package main

import (
	"encoding/json"
	"fmt"
	"net"
	"os"
	"os/exec"
	"strings"
	"time"

	"bet24.com/log"
	"bet24.com/utils"
	"github.com/gorilla/websocket"
)

type serverconfig struct {
	ServerPort int
}

type multipleServerconfig struct {
	PortStart int
}

type roomconfig struct {
	ServerPort int
}

type room struct {
	Config roomconfig
}

type rooms struct {
	Rooms []room
}

func waitInput() {
	for {
		var endInput string
		fmt.Scanln(&endInput)
		switch endInput {
		case "list":
			fmt.Println(modules)
		default:
			fmt.Println("unknown command")
		}
	}
}

func tryRestart(name string) {
	cmd := exec.Command("cmd.exe", "/c", fmt.Sprintf("start cmd.exe /k %v", name))
	cmd.Start()
}

func doMonitorConfigServer(name string) {
	var sc serverconfig
	configFile := fmt.Sprintf("%s/server.json", name)
	data, err := os.ReadFile(configFile)
	if err != nil {
		log.Debug("doMonitorConfigServer read config[%s] failed", configFile)
	}

	err = json.Unmarshal(data, &sc)
	if err != nil {
		log.Debug("Unmarshal config failed err:%v", err)
		return
	}

	// 百人游戏，端口在rooms
	if sc.ServerPort == 0 {
		doMonitorBacServer(name)
		return
	}

	doMonitorTcpServer(fmt.Sprintf("localhost:%d", sc.ServerPort+100), name)
}

func doMonitorBacServer(name string) {
	var sc rooms
	configFile := fmt.Sprintf("%s/rooms.json", name)
	data, err := os.ReadFile(configFile)
	if err != nil {
		log.Debug("doMonitorBacServer read config[%s] failed", configFile)
	}

	err = json.Unmarshal(data, &sc)
	if err != nil {
		log.Debug("doMonitorBacServer Unmarshal config failed err:%v", err)
		return
	}
	for _, v := range sc.Rooms {
		doMonitorTcpServer(fmt.Sprintf("localhost:%d", v.Config.ServerPort+100), name)
	}
}

func doMonitorMultiServer(name string, count int) {
	var sc multipleServerconfig
	configFile := fmt.Sprintf("%s/server.json", name)
	data, err := os.ReadFile(configFile)
	if err != nil {
		log.Debug("doMonitorMultiServer read config[%s] failed", configFile)
	}

	err = json.Unmarshal(data, &sc)
	if err != nil {
		log.Debug("doMonitorMultiServer Unmarshal config failed err:%v", err)
		return
	}
	for i := 0; i < count; i++ {
		doMonitorTcpServer(fmt.Sprintf("localhost:%d", sc.PortStart+100+i), fmt.Sprintf("%s %d", name, i+1))
	}

}

func doMonitorWsServer(url string, name string) {
	wsServer := url
	//wsServer := "ws://192.168.0.18:3563"
	c, _, err := websocket.DefaultDialer.Dial(wsServer, nil)
	if err != nil {
		log.Debug("连接[%s,%s]服务器失败 %v time = %v", name, url, err, time.Now().Format("2006-01-02 15:04:05"))
		tryRestart(name)
		return
	}
	log.Debug("连接[%s]成功 time = %v", name, time.Now().Format("2006-01-02 15:04:05"))
	c.Close()
}

func doMonitorTcpServer(url string, name string) {
	tcpAddr, err := net.ResolveTCPAddr("tcp4", url)
	if err != nil {
		log.Debug("doTcpServer[%v] Fatal error: %v", name, err.Error())
		return
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		log.Debug("连接[%v]服务器失败 %v time = %v", name, err, time.Now().Format("2006-01-02 15:04:05"))
		tryRestart(name)
		return
	}
	log.Debug("连接[%v]成功 time = %v", name, time.Now().Format("2006-01-02 15:04:05"))
	conn.Close()
}

type module struct {
	Name  string
	Addr  string
	Count int
}

var modules []module

func run() {
	// 读取配置文件
	data, err := os.ReadFile("conf/servermonitor.json")
	if err != nil {
		log.Debug("read config failed")
		time.AfterFunc(time.Second*10, run)
		return
	}
	err = json.Unmarshal(data, &modules)
	if err != nil {
		log.Debug("Unmarshal config failed err:%v", err)
		time.AfterFunc(time.Second*10, run)
		return
	}

	utils.SetConsoleTitle(fmt.Sprintf("ServerMonitor %d apps", len(modules)))

	fmt.Println(string(data))
	doMonitor()
}

func doMonitor() {
	for _, v := range modules {
		if v.Count > 0 {
			doMonitorMultiServer(v.Name, v.Count)
		} else if v.Addr == "" {
			doMonitorConfigServer(v.Name)
		} else if strings.Index(v.Addr, "ws://") >= 0 {
			doMonitorWsServer(v.Addr, v.Name)
		} else {
			doMonitorTcpServer(v.Addr, v.Name)
		}
	}

	time.AfterFunc(time.Second*10, doMonitor)
}

func main() {
	defer waitInput()
	go run()
}
