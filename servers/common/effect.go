package common

const (
	_                        = iota
	SpecialEffect_Frozen     // 冰冻效果
	SpecialEffect_BatchCatch // 产生范围攻击
	SpecialEffect_Slot       // 产生slot
	SpecialEffect_Prizewheel // 转盘
)

const (
	_                      = iota
	SpecialType_Screw      // 螺旋鱼
	SpecialType_Bomb       // 炸弹鱼
	SpecialType_Lightening // 闪电
	SpecialType_Trident    // 三叉戟
	SpecialType_Slot       // slot
	SpecialType_Blackhole  // 黑洞
	SpecialType_Laser      // 激光
)

/*
	击中特殊鱼产生范围攻击效果
	客户端在特殊鱼被击中后，将炮台设置成特殊效果
	客户端发射特殊子弹 special_shoot ，这个指令服务器只转发，客户端自定义所需的数据
	客户端发送特殊捕获命令
		special_catch
		type SpecialCatch struct {
			OriginFishKey int // 获得炮弹的鱼的fishkey
			TargetFishKey int // 目标鱼的fishkey
		}
	后续捕获成功和失败信息跟普通捕获一样
*/

type EffectInfo struct {
	SpecialEffect int    `json:",omitempty"`
	SpecialType   int    `json:",omitempty"`
	Param1        int    `json:",omitempty"`
	Param2        int    `json:",omitempty"`
	Extra         string `json:",omitempty"`
}

const ESSENCE_ITEMID = 620200
