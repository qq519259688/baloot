package common

const (
	LOGTYPE_SHOPBUY              = 1  // 商城购买
	LOGTYPE_EXCHANGE_GOLD        = 2  // 元宝兑换金币
	LOGTYPE_RECHARGE_CARD        = 3  // 充值卡
	LOGTYPE_EXCHANGE_CARD        = 4  // 兑换卡
	LOGTYPE_SEND_REG             = 5  // 注册赠送
	LOGTYPE_GOLD_EXCHANGE        = 6  // 金币提现
	LOGTYPE_ADMIN_REDUCE         = 7  // 后台扣减
	LOGTYPE_CHANGE_NICKNAME      = 9  // 修改昵称
	LOGTYPE_PACK_FIRST           = 10 // 首充礼包
	LOGTYPE_PACK_EVERYDAY        = 11 // 每日特惠礼包
	LOGTYPE_PACK_TIMELIMITED     = 12 // 限时礼包
	LOGTYPE_SEND_SIGN            = 13 // 签到
	LOGTYPE_SEND_MONTHLYCARD     = 14 // 购买月卡赠送
	LOGTYPE_PACK_GROWTH          = 15 // 成长礼包
	LOGTYPE_PACK_VIP             = 16 // vip特惠礼包
	LOGTYPE_EXCHANGE             = 17 // 福卡兑换
	LOGTYPE_VIP_UPGRADE          = 18 // vip升级
	LOGTYPE_SEND_REVIEW          = 19 // 评论赠送
	LOGTYPE_TASK_AWARD           = 20 // 任务奖励
	LOGTYPE_ATTACHMENT_GIFT      = 21 // 附件领取
	LOGTYPE_GIVING               = 22 // 赠送礼物
	LOGTYPE_PRIZEWHEEL_CONSUME   = 24 // 幸运抽奖消耗
	LOGTYPE_PRIZEWHEEL_GIFT      = 25 // 彩票刮奖
	LOGTYPE_SUBSIDY_GIFT         = 27 // 救助金
	LOGTYPE_REALNAME             = 28 // 实名认证
	LOGTYPE_SHARE                = 29 // 分享
	LOGTYPE_CHAT                 = 30 // 聊天
	LOGTYPE_FRIEND               = 31 // 好友赠送
	LOGTYPE_SPREAD               = 32 // 邀请奖励
	LOGTYPE_VIDEO_GAME_RETURN    = 33 // 看视频游戏结算输返还
	LOGTYPE_BIND_TEL             = 34 // 绑定手机
	LOGTYPE_BIND_FACEBOOK        = 35 // 绑定facebook
	LOGTYPE_BIND_TEACHER         = 36 // 推广绑定
	LOGTYPE_TEACHER_DOUBLE       = 37 // 收益翻倍
	LOGTYPE_TEACHER_CONTINUE     = 38 // 徒弟连续登录
	LOGTYPE_RANK_AWARD           = 39 // 榜单奖励
	LOGTYPE_TASK_COUPON          = 40 // 红包券任务
	LOGTYPE_TASK_COUPON_MULTIPLE = 41 // 红包券任务多倍
	LOGTYPE_RETURN               = 42 // 回归奖励
	LOGTYPE_PURCHASE             = 43 // 100K购
	LOGTYPE_TRANSFER             = 44 // 赠送返额
	LOGTYPE_GOLD_WHEEL           = 45 // 消耗转盘
	LOGTYPE_SLOT_SCORE           = 46 // SLOT兑换
	LOGTYPE_AWARD_ANSWER         = 47 // 奖励模块（问卷）
	LOGTYPE_SIGNIN_WHEEL         = 48 // 签到转盘
	LOGTYPE_EXCHANGE_TOCASH      = 49 // 兑换现金
	LOGTYPE_EXCHANGE_TOGOLD      = 50 // 兑换金币

	LOGTYPE_TOOL_FRAGMENT_PATCH = 51 //碎片合成
	LOGTYPE_TOOL_CONVERT        = 52 //道具折算
	LOGTYPE_LEVEL_UPGRADE       = 53 // 用户升级
	LOGTYPE_LOGIN_AWARD         = 54 // 登录奖励

	// 私人场
	LOGTYPE_PRIVATEROOM_ENTER = 55
	LOGTYPE_PRIVATEROOM_OWNER = 56
	LOGTYPE_PRIVATEROOM_PRIZE = 57

	LOGTYPE_SIMPLEMATCH_CREATE        = 58 // 创建费用
	LOGTYPE_SIMPLEMATCH_CREATE_RETURN = 59 // 创建返还
	LOGTYPE_SIMPLEMATCH_ENTER         = 60 // 进入费用
	LOGTYPE_SIMPLEMATCH_ENTER_RETURN  = 61 // 退出返还
	LOGTYPE_SIMPLEMATCH_OWNER_COLLECT = 62 // 房主收报名费
	LOGTYPE_SIMPLEMATCH_PRIZE         = 63 // 奖金

	LOGTYPE_EXCHANGE_TOTEL = 64 // 兑换话费

	LOGTYPE_SNGMATCH_ENTER        = 65 // 进入费用
	LOGTYPE_SNGMATCH_ENTER_RETURN = 66 // 退出返还
	LOGTYPE_SNGMATCH_PRIZE        = 67 // 奖金
	LOGTYPE_AWARD_AGENT_SHARE     = 68 // 奖励模块(代理分享)
	LOGTYPE_TRANSFER_CONSUME      = 69 // 赠送扣减
	LOGTYPE_TRANSFER_ADD          = 70 // 赠送增加
	LOGTYPE_TOOL_SELL             = 71 // 道具出售

	LOGTYPE_POINTMATCH_CREATE        = 72 // 创建费用
	LOGTYPE_POINTMATCH_CREATE_RETURN = 73 // 创建返还
	LOGTYPE_POINTMATCH_ENTER         = 74 // 进入费用
	LOGTYPE_POINTMATCH_ENTER_RETURN  = 75 // 退出返还
	LOGTYPE_POINTMATCH_OWNER_COLLECT = 76 // 房主收报名费
	LOGTYPE_POINTMATCH_PRIZE         = 77 // 奖金

	LOGTYPE_COMBOMATCH_ENTER        = 78 // 进入费用
	LOGTYPE_COMBOMATCH_ENTER_RETURN = 79 // 退出返还
	LOGTYPE_COMBOMATCH_PRIZE        = 80 // 奖金

	LOGTYPE_CASHCARD_TOCASH     = 81 // 现金卡提现
	LOGTYPE_AUDIOROOM_JOIN_DEL  = 82 // 语聊房加入会费
	LOGTYPE_AUDIOROOM_JOIN_SEND = 83 // 语聊房加入收益
	LOGTYPE_AUDIOROOM_COLLECT   = 84 // 语聊房收集奖励

	LOGTYPE_GIFT_SEND            = 85 // 赠送礼物
	LOGTYPE_GIFT_RECEIVE         = 86 // 领取礼物
	LOGTYPE_AUDIOROOM_GAME_START = 87 // 语聊房游戏开始消耗

	LOGTYPE_NOVICE_WELFARE_AWARD    = 88 // 新手福利奖励
	LOGTYPE_NOVICE_WELFARE_GIFTPACK = 89 // 新手礼包
	LOGTYPE_AUDIOROOM_INCOME        = 90 // 语聊房收益

	LOGTYPE_DAILY_WHEEL = 91 // 每日转盘
	LOGTYPE_LEVEL_PACK  = 92 // 等级礼包

	LOGTYPE_LADDER_CONSECUTIVE_CARD = 93 // 使用连胜卡

	LOGTYPE_GUESS_BET   = 95 // 竞猜下注
	LOGTYPE_GUESS_AWARD = 96 // 竞猜派奖

	LOGTYPE_SINGLEMATCH_TICKET = 97 // 锦标赛门票
	LOGTYPE_SINGLEMATCH_PRIZE  = 98 // 锦标赛奖励
	LOGTYPE_SINGLEMATCH_REVIVE = 99 // 复活

	LOGTYPE_HIGHLY_PROFITABLE_AWARD = 100 // 一本万利奖励

	LOGTYPE_FISH_DROP = 7081 //捕鱼掉落
)

func GetLogTypeName(logType int) string {
	switch logType {
	case LOGTYPE_SHOPBUY: // 1=商城购买
		return "商城购买,"
	case LOGTYPE_RECHARGE_CARD: // 3=充值卡
		return "充值卡,"
	case LOGTYPE_EXCHANGE_CARD: // 4=兑换卡
		return "兑换卡,"
	case LOGTYPE_GOLD_EXCHANGE: // 6=金币提现
		return "金币提现"
	case LOGTYPE_EXCHANGE_GOLD: // 17=福卡兑换
		return "元宝兑换金币,"
	case LOGTYPE_SEND_REG: // 5=注册赠送
		return "注册赠送,"
	case LOGTYPE_ADMIN_REDUCE: // 7=后台扣减
		return "后台扣减,"
	case LOGTYPE_CHANGE_NICKNAME: // 9=修改昵称
		return "修改昵称,"
	case LOGTYPE_PACK_FIRST: // 10=首充礼包
		return "首充礼包,"
	case LOGTYPE_PACK_EVERYDAY: // 11=每日特惠礼包
		return "每日特惠礼包,"
	case LOGTYPE_PACK_TIMELIMITED: // 12=限时礼包
		return "限时礼包,"
	case LOGTYPE_SEND_SIGN: // 13=签到
		return "签到,"
	case LOGTYPE_SEND_MONTHLYCARD: // 14=购买月卡赠送
		return "购买月卡赠送,"
	case LOGTYPE_PACK_GROWTH: // 15=成长礼包
		return "成长礼包,"
	case LOGTYPE_PACK_VIP: // 16=vip特惠礼包
		return "vip特惠礼包,"
	case LOGTYPE_EXCHANGE: // 17=福卡兑换
		return "福卡兑换,"
	case LOGTYPE_VIP_UPGRADE: // 18=vip升级
		return "vip升级"
	case LOGTYPE_SEND_REVIEW: // 19=评论赠送
		return "评论赠送"
	case LOGTYPE_TASK_AWARD: // 20=任务奖励
		return "任务奖励,"
	case LOGTYPE_ATTACHMENT_GIFT: // 21=附件领取
		return "附件领取,"
	case LOGTYPE_PRIZEWHEEL_CONSUME: // 24=幸运抽奖消耗
		return "转盘消耗,"
	case LOGTYPE_PRIZEWHEEL_GIFT: // 25=彩票刮奖
		return "彩票刮奖,"
	case LOGTYPE_SUBSIDY_GIFT: // 27=救助金
		return "救助金,"
	case LOGTYPE_REALNAME: // 28=实名认证
		return "实名认证,"
	case LOGTYPE_SHARE: // 29=分享
		return "分享,"
	case LOGTYPE_CHAT: // 30=聊天
		return "聊天,"
	case LOGTYPE_FRIEND: // 31=好友赠送
		return "好友赠送,"
	case LOGTYPE_SPREAD: // 32=邀请奖励
		return "邀请奖励,"
	case LOGTYPE_VIDEO_GAME_RETURN: // 33=看视频游戏结算输返还
		return "看视频游戏结算返还,"
	case LOGTYPE_BIND_TEL: // 34=绑定手机
		return "绑定手机"
	case LOGTYPE_BIND_FACEBOOK: // 35=绑定facebook
		return "绑定Facebook"
	case LOGTYPE_BIND_TEACHER: // 36=推广绑定
		return "推广绑定"
	case LOGTYPE_TEACHER_DOUBLE: // 37=收益翻倍
		return "收益翻倍"
	case LOGTYPE_TEACHER_CONTINUE: // 38=连续登录
		return "徒弟连续登录"
	case LOGTYPE_RANK_AWARD: // 39=榜单奖励
		return "榜单奖励"
	case LOGTYPE_TASK_COUPON: // 40=红包券任务
		return "红包券任务"
	case LOGTYPE_TASK_COUPON_MULTIPLE: // 41=红包券多倍
		return "红包券多倍"
	case LOGTYPE_RETURN: // 42=回归奖励
		return "回归奖励"
	case LOGTYPE_PURCHASE: // 43=100K购
		return "100K购"
	case LOGTYPE_TRANSFER: // 44=赠送返额
		return "赠送返额"
	case LOGTYPE_GOLD_WHEEL: // 45=消耗转盘
		return "消耗转盘"
	case LOGTYPE_SLOT_SCORE: // 46=SLOT兑换
		return "Slot兑换"
	case LOGTYPE_AWARD_ANSWER: // 47=奖励模块
		return "奖励模块(问卷),"
	case LOGTYPE_EXCHANGE_TOCASH: // 49=兑换cash
		return "兑换CASH"
	case LOGTYPE_EXCHANGE_TOGOLD: // 50=兑换金币
		return "兑换金币"
	case LOGTYPE_TOOL_FRAGMENT_PATCH: // 51=碎片合成
		return "碎片合成,"
	case LOGTYPE_TOOL_CONVERT: // 52=道具折算
		return "道具折算,"
	case LOGTYPE_LEVEL_UPGRADE: // 53=用户升级
		return "用户升级,"
	case LOGTYPE_LOGIN_AWARD: // 54=登录奖励
		return "登录奖励,"
	case LOGTYPE_EXCHANGE_TOTEL: // 63=兑换话费
		return "兑换话费,"
	case LOGTYPE_AWARD_AGENT_SHARE: // 68=奖励模块(代理分享)
		return "奖励模块(代理分享),"
	case LOGTYPE_TRANSFER_CONSUME: // 69=赠送扣减
		return "赠送扣减"
	case LOGTYPE_TRANSFER_ADD: // 70=赠送增加
		return "赠送增加"
	case LOGTYPE_TOOL_SELL: // 71=出售道具
		return "道具出售"
	case LOGTYPE_CASHCARD_TOCASH: // 81=现金卡提现
		return "现金卡提现"
	case LOGTYPE_AUDIOROOM_JOIN_DEL: // 82=语聊房加入会费
		return "语聊房加入会费"
	case LOGTYPE_AUDIOROOM_JOIN_SEND: // 83=语聊房加入收益
		return "语聊房加入收益"
	case LOGTYPE_AUDIOROOM_COLLECT: // 84=语聊房收集奖励
		return "语聊房收集奖励"
	case LOGTYPE_AUDIOROOM_INCOME: // 90=语聊房收益
		return "语聊房收益"
	case LOGTYPE_GUESS_BET: // 95=竞猜投注
		return "竞猜投注"
	case LOGTYPE_GUESS_AWARD: // 96=竞猜派奖
		return "竞猜派奖"
	case LOGTYPE_FISH_DROP: // 7081=捕鱼掉落
		return "捕鱼掉落,"

	}
	return ""
}
