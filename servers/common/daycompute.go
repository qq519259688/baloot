package common

import (
	"bet24.com/log"
	"runtime/debug"
	"strings"
	"time"
)

const SecondsOfDay = 86400        // 每天秒数
const FirstWeek_days = 4          // 1970.1.1为周四
const FirstWeek_Secondes = 432000 // 周日为每周起点，减去5天
const SecondsOfWeek = 607800
const Layout = "2006-01-02 15:04:05"

// 获取当前时间
func GetNowTime() time.Time {
	return time.Now()
}

// 获取当前时间串
func GetNowTimeStr() string {
	return time.Now().Format(Layout)
}

// 时间字符串转换时间格式
func ParseTime(value string) time.Time {
	if value == "" {
		value = "1970-01-01 00:00:00"
	}
	t, err := time.ParseInLocation(Layout, value, time.Now().Location())
	if err != nil {
		log.Error("daycompute.ParseTime error %+v", err)
		log.Error("%s", debug.Stack())
	}
	return t
}

// 获取时间戳
func GetTimeStamp() int {
	return int(time.Now().Unix())
}

// 从数据库读取的 time 已经加有时区,这里不需要再处理 Timezone_add
func GetStamp(t time.Time) int {
	return int(t.Unix())
}

func timeStampToLocal(timeStamp int) int {
	nowStr := time.Unix(int64(timeStamp), 0).Format(Layout)
	u, err := time.Parse(Layout, nowStr)
	if err != nil {
		log.Error("daycompute.timeStampToLocal error %+v", err)
		log.Error("%s", debug.Stack())
		return timeStamp
	}
	return int(u.Unix())
}

func TimeStampToString(timeStamp int64) string {
	nowStr := time.Unix(timeStamp, 0).Format(Layout)
	u, err := time.Parse(Layout, nowStr)
	if err != nil {
		return nowStr
	}
	return u.Format(Layout)
}

func TimeStampToShortString(timeStamp int64) string {
	ret := TimeStampToString(timeStamp)
	ret = strings.ReplaceAll(ret, "2023-", "")
	return strings.ReplaceAll(ret, "2024-", "")
}

func TimeStampToTime(timeStamp int64) time.Time {
	now := time.Unix(timeStamp, 0)
	nowStr := now.Format(Layout)
	u, err := time.Parse(Layout, nowStr)
	if err != nil {
		return now
	}
	return u
}

func GetDayIndex(timeStamp int) int {
	return timeStampToLocal(timeStamp) / SecondsOfDay
}

func GetNowDayIndex() int {
	return GetDayIndex(int(time.Now().Unix()))
}

func GetWeekIndex(timeStamp int) int {
	return (timeStampToLocal(timeStamp)-FirstWeek_Secondes)/SecondsOfWeek + 1
}

func GetMonthIndex(timeStamp int) int {
	t := time.Unix(int64(timeStamp), 0)
	// 5 + 202300（年份加月份解决唯一且不重复的问题）
	return int(t.Month()) + t.Year()*100
}

func IsContinueDay(timeStamp1, timeStamp2 int) bool {
	day1 := GetDayIndex(timeStamp1)
	day2 := GetDayIndex(timeStamp2)
	if day1 == day2-1 || day1 == day2+1 {
		return true
	}
	return false
}

func IsSameDay(timeStamp1, timeStamp2 int) bool {
	return GetDayIndex(timeStamp1) == GetDayIndex(timeStamp2)
}

func IsSameWeek(timeStamp1, timeStamp2 int) bool {
	return GetWeekIndex(timeStamp1) == GetWeekIndex(timeStamp2)
}
