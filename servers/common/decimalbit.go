package common

func pow(x, n int) int {
	if x == 0 {
		return 0
	}
	if n == 0 {
		return 1
	}
	ret := x
	for i := 1; i < n; i++ {
		ret *= x
	}
	return ret
}

func decimalOr(left, right, bit int) int {
	if left == 0 && right == 0 {
		return 0
	}
	ret := 0
	if left%10 > 0 || right%10 > 0 {
		ret = pow(10, bit)
	}

	return ret + decimalOr(left/10, right/10, bit+1)
}

func decimalAnd(left, right, bit int) int {
	if left == 0 || right == 0 {
		return 0
	}
	ret := 0
	if left%10 > 0 && right%10 > 0 {
		ret = pow(10, bit)
	}

	return ret + decimalAnd(left/10, right/10, bit+1)
}

func DecimalOr(left, right int) int {
	return decimalOr(left, right, 0)
}

func DecimalAnd(left, right int) int {
	return decimalAnd(left, right, 0)
}
