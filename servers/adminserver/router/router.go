package router

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bet24.com/servers/adminserver/audioroom"
	"bet24.com/servers/adminserver/guess"
	"bet24.com/servers/adminserver/matchdata"
	"bet24.com/servers/adminserver/rank"

	"bet24.com/servers/adminserver/label"

	"bet24.com/servers/adminserver/dot"

	"bet24.com/servers/adminserver/agent"
	"bet24.com/servers/adminserver/announce"
	"bet24.com/servers/adminserver/card"
	"bet24.com/servers/adminserver/chat"
	"bet24.com/servers/adminserver/config"
	"bet24.com/servers/adminserver/controller"
	"bet24.com/servers/adminserver/coupon"
	"bet24.com/servers/adminserver/game"
	"bet24.com/servers/adminserver/middleware"
	"bet24.com/servers/adminserver/platformconfig"
	"bet24.com/servers/adminserver/privateroom"
	"bet24.com/servers/adminserver/register"
	"bet24.com/servers/adminserver/report"
	"bet24.com/servers/adminserver/roi"
	"bet24.com/servers/adminserver/slot"
	"bet24.com/servers/adminserver/teacher"
	"bet24.com/servers/adminserver/video"
	"github.com/gin-gonic/gin"
	"github.com/mattn/go-colorable"
)

func Run() {

	// 强制日志颜色化
	gin.ForceConsoleColor()

	// 设置颜色输出,识别 console 色值
	gin.DefaultWriter = colorable.NewColorableStdout()

	// 设置日志模式
	gin.SetMode(gin.DebugMode)

	// 创建一个默认的路由
	r := gin.Default()
	r.Use(middleware.CheckValid(), middleware.Cors())

	// 创建一个默认的路由(HTTPS)
	rTls := gin.Default()
	rTls.Use(middleware.TlsHandler(), middleware.CheckValid(), middleware.Cors()) // 处理SSL的中间件

	// ------------------------------玩家------------------------------
	userGroup := rTls.Group("/user")
	{
		// 用户列表
		userGroup.POST("/allUserList", controller.AllUserList)
		// 查询用户详情信息
		userGroup.POST("/userDetail", controller.UserDetail)
		// 查询被封用户列表
		userGroup.POST("/forbidUserList", controller.ForbidUserList)
		// 封杀玩家
		userGroup.POST("/forbidUserAdd", controller.ForbidUserAdd)
		// 解除封杀
		userGroup.POST("/forbidUserDel", controller.ForbidUserDel)
		// 加金
		userGroup.POST("/cashSend", controller.CashSend)
		// 扣金
		userGroup.POST("/cashDel", controller.CashDel)
		// 用户简单信息
		userGroup.POST("/userSimpleInfo", controller.UserSimpleInfo)
		// 金币场解锁
		userGroup.POST("/cashUnlock", controller.CashUnlock)
		// 金币锁信息
		userGroup.POST("/cashLockInfo", controller.CashLockInfo)
		// 实时在线
		userGroup.POST("/casinoOnlineList", controller.CasinoOnlineList)
		// 货币统计信息
		userGroup.POST("/moneyStat", controller.MoneyStat)
		// 货币排行
		userGroup.POST("/currencyTop", controller.CurrencyTop)
		// 获取用户ID(根据昵称、手机号)
		userGroup.POST("/getUserID", controller.GetUserID)
		// 删除账号（后台）
		userGroup.POST("/delUser", controller.DelUser)
		// 后台扣减钻石
		userGroup.POST("/diamondDel", controller.DiamondWebReduce)
		// 绑定facebook
		userGroup.POST("/bindFacebook", controller.BindFacebook)
		// 个性签名列表
		userGroup.POST("/userWordsList", controller.UserWordsList)
		// 个性签名审核
		userGroup.POST("/userWordsSet", controller.UserWordsSet)
		// 银行信息
		userGroup.POST("/bankInfo", controller.BankInfo)
		// 元宝积分榜
		userGroup.POST("/scoreRankList", controller.ScoreRankList)
		// 添加元宝积分
		userGroup.POST("/addRankScore", controller.AddRankScore)
		// 用户标签列表
		userGroup.POST("/tags", controller.GetUserTagList)
		// 白名单列表
		userGroup.POST("/whiteList", controller.WhiteList)
		// 添加白名单
		userGroup.POST("/whiteAdd", controller.WhiteAdd)
		// 删除白名单
		userGroup.POST("/whiteDel", controller.WhiteDel)
		// 修改昵称
		userGroup.POST("/changeNickName", controller.ChangeNickName)
		// 个人奖池
		userGroup.POST("/prizePool", controller.GetUserPrizePool)
		// 奖池日志列表
		userGroup.POST("/prizePoolList", controller.GetPrizePoolList)
		// 发放个人奖池
		userGroup.POST("/sendPrizePool", controller.SendPrizePool)
	}

	// ------------------------------统计------------------------------
	statGroup := rTls.Group("/stat")
	{
		// 日常统计
		statGroup.POST("/dailyStat", controller.DailyStat)
		// 注册统计
		statGroup.POST("/regStat", register.StatList)
		// 注册统计(小时)
		statGroup.POST("/regStatByHour", register.StatListByHour)
		// 注册来源统计
		statGroup.POST("/regStatSourceList", register.SourceStatList)
		// 在线统计(每天)
		statGroup.POST("/onlineStatListByDay", controller.OnlineStatListByDay)
		// 在线统计报表(小时)
		statGroup.POST("/onlineStatReport", controller.OnlineStatReport)
		// 同时在线统计报表(小时)
		statGroup.POST("/onlineUserReport", controller.OnlineUserReport)
		// 游戏统计(小时)
		statGroup.POST("/gameStatHour", controller.GameStatHour)
		// 存量统计
		statGroup.POST("/moneyStatTotalList", controller.MoneyStatTotalList)
		// 存量统计(每天)
		statGroup.POST("/moneyStatTotalListByDay", controller.MoneyStatTotalListByDay)
		// 金币变化
		statGroup.POST("/getUserMoneyStatList", controller.GetUserMoneyStatList)
		// 金币变化详情
		statGroup.POST("/getMoneyStatDetail", controller.GetMoneyStatDetail)
		// 游戏日常统计
		statGroup.POST("/gameDailyStat", controller.GameDailyStat)
		// 后台管理--导出金币变化
		statGroup.POST("/moneyStatExport", controller.MoneyStatExport)
		// 用户日常统计
		statGroup.POST("/userDailyStat", controller.UserDailyStat)
		// 税收总计
		statGroup.POST("/taxStatList", controller.TaxStatList)
		// 游戏记录报表(局数、时长)
		statGroup.POST("/gameRecordReport", game.RecordIndexStat)
		// 游戏记录报表详情
		statGroup.POST("/gameRecordReportDetail", controller.GameRecordReportDetail)
		// 注册用户转化报表
		statGroup.POST("/regConvertReport", controller.RegConvertReport)
		// 登录用户转化报表
		statGroup.POST("/loginConvertReport", controller.LoginConvertReport)
		// 留存统计
		statGroup.POST("/liveStatList", controller.LiveStatList)
		// 渠道留存统计
		statGroup.POST("/liveStatListByPartner", controller.LiveStatListByPartner)
		// 游戏金币流量统计
		statGroup.POST("/moneyFlowStat", controller.MoneyFlowStat)
		// Slot 统计列表
		statGroup.POST("/getSlotStat", slot.GetStat)
		// Slot 统计详情列表
		statGroup.POST("/getSlotDetail", slot.GetStatDetail)
		// 渠道登录统计
		statGroup.POST("/loginByPartner", controller.LoginStatByPartner)
		// 在线统计报表(小时)
		statGroup.POST("/onlineStatReportChip", controller.OnlineStatReportChip)
		// 同时在线统计报表(小时)
		statGroup.POST("/onlineUserReportChip", controller.OnlineUserReportChip)
		// 游戏统计(小时)
		statGroup.POST("/gameStatHourChip", controller.GameStatHourChip)
		// 在线统计(每天)
		statGroup.POST("/onlineStatListByDayChip", controller.OnlineStatListByDayChip)
		// 新用户注册统计
		statGroup.POST("/newUserStatList", register.NewUserStatList)
		// 留存统计
		statGroup.POST("/retentionStatList", controller.RetentionStatList)
		// 获取牌局统计
		statGroup.POST("/cardStatList", game.GetCardStatList)
		// 中途退出统计
		statGroup.POST("/midwayStatList", game.GetMidwayStatList)
		// 水池统计
		statGroup.POST("/waterPoolStatList", game.GetWaterPoolStatList)
	}

	// ------------------------------统计------------------------------
	trackGroup := rTls.Group("/track")
	{
		// 游戏轨迹记录
		trackGroup.POST("/userTrackList", controller.UserTrackList)
		// 游戏轨迹统计
		trackGroup.POST("/userTrackStat", controller.UserTrackStat)
		// 用户足迹
		trackGroup.POST("/trackList", controller.TrackList)
	}

	// ------------------------------投入产出------------------------------
	roiGroup := rTls.Group("/roi")
	{
		// 录入
		roiGroup.POST("/add", roi.Insert)
		// 删除
		roiGroup.POST("/del", roi.Del)
		// 列表
		roiGroup.POST("/list", roi.GetList)
		// 月列表
		roiGroup.POST("/listByMonth", roi.GetListByMonth)
	}

	// ------------------------------每日报表------------------------------
	reportGroup := rTls.Group("/report")
	{
		// 录入日报
		reportGroup.POST("/addDaily", report.InsertDaily)
		// 删除日报
		reportGroup.POST("/delDaily", report.DelDaily)
		// 日报列表
		reportGroup.POST("/dailyList", report.GetDailyList)
		// 挑战赛列表
		reportGroup.POST("/sngMatchList", report.GetSNGMatchList)
		// 游戏列表
		reportGroup.POST("/gameList", report.GetGameList)
		// 发行日报
		reportGroup.POST("/issue", report.GetIssueStatList)
		// 时段统计报表
		reportGroup.POST("/timePeriod", report.TimePeriodReport)
		// 时段统计列表
		reportGroup.POST("/timePeriodStat", report.TimePeriodStat)
		// 时段用户列表
		reportGroup.POST("/timePeriodUsers", report.TimePeriodUsers)
	}

	// ------------------------------后台账号------------------------------
	adminGroup := rTls.Group("/admin")
	{
		// 后台登陆
		adminGroup.POST("/login", controller.Login)
		// 获取页面列表
		adminGroup.POST("/pageList", controller.PageList)
		// 获取管理用户信息
		adminGroup.POST("/getInfo", controller.GetInfo)
		// 修改密码
		adminGroup.POST("/updatePassword", controller.UpdatePassword)
		// 校验页面
		adminGroup.POST("/verifyPage", controller.VerifyPage)
		// 管理用户列表
		adminGroup.POST("/userList", controller.UserList)
		// 删除后台管理用户
		adminGroup.POST("/userDel", controller.UserDel)
		// 修改备注
		adminGroup.POST("/updateMemo", controller.UpdateMemo)
		// 添加管理账号
		adminGroup.POST("/userAdd", controller.UserAdd)
		// 获取后台账号密码
		adminGroup.POST("/getPassword", controller.GetPassword)
	}

	// ------------------------------视频广告------------------------------
	videoGroup := rTls.Group("/video")
	{
		// 广告播放统计(按天)
		videoGroup.POST("/playStat", video.PlayStat)

		// 广告播放统计(按时段)
		videoGroup.POST("/playStatByHour", video.PlayStatByHour)

		// 广告来源统计
		videoGroup.POST("/sourceList", video.SourceList)

		// 广告分布
		videoGroup.POST("/regionList", video.RegionList)

		// 广告指标
		videoGroup.POST("/indexList", video.IndexList)

		// 广告用户统计(按天)
		videoGroup.POST("/userStat", video.UserStat)

		// 广告用户统计(按时段)
		videoGroup.POST("/userStatByHour", video.UserStatByHour)
	}

	// ------------------------------代理------------------------------
	agentGroup := rTls.Group("/agent")
	{
		// 配置信息
		agentGroup.POST("/config", agent.GetConfig)
		// 修改配置
		agentGroup.POST("/configUp", agent.UpdateConfig)
		// 代理列表
		agentGroup.POST("/list", agent.GetList)
		// 会员列表
		agentGroup.POST("/memberList", agent.GetMemberList)
		// 佣金日志
		agentGroup.POST("/commissionLog", agent.GetCommissionLog)
		// 佣金排行榜
		agentGroup.POST("/commissionRank", agent.GetCommissionRankList)
		// 创建代理
		agentGroup.POST("/create", agent.Create)
		// 申请列表
		agentGroup.POST("/applyList", agent.ApplyList)
		// 处理申请
		agentGroup.POST("/dealApply", agent.DealApply)
		// 设置代理状态
		agentGroup.POST("/setStatus", agent.SetStatus)
	}

	// ------------------------------师徒------------------------------
	teacherGroup := rTls.Group("/teacher")
	{
		// 收益排名
		teacherGroup.POST("/topProfit", teacher.GetTopProfit)
		// 收益列表
		teacherGroup.POST("/profitList", teacher.GetProfitList)
	}

	// ------------------------------渠道------------------------------
	partnerGroup := rTls.Group("/partner")
	{
		// 渠道列表
		partnerGroup.POST("/partnerList", controller.PartnerList)
		// 添加渠道
		partnerGroup.POST("/partnerAdd", controller.PartnerAdd)
		// 删除渠道
		partnerGroup.POST("/partnerDel", controller.PartnerDel)
		// 平台信息列表
		partnerGroup.POST("/platformInfoList", controller.PlatformInfoList)
		// 平台信息修改
		partnerGroup.POST("/platformInfoUp", controller.PlatformInfoUp)
		// 平台配置信息
		partnerGroup.POST("/platformConfig", controller.PlatformConfig)
		// 修改平台配置信息
		partnerGroup.POST("/platformConfigUp", controller.PlatformConfigUp)
		// 流量渠道
		partnerGroup.POST("/utmSource", controller.UTMSourceList)
	}

	// ------------------------------充值订单------------------------------
	payGroup := rTls.Group("/pay")
	{
		// 订单列表
		payGroup.POST("/orderList", controller.OrderList)
		// 获取各渠道充值列表
		payGroup.POST("/payList", controller.PayList)
		// 登录渠道充值统计
		payGroup.POST("/payListByPartner", controller.PayListByPartner)
		// 日充值总额
		payGroup.POST("/payListByDay", controller.PayListByDay)
		// 充值统计列表
		payGroup.POST("/payStatList", controller.PayStatList)
		// 苹果充值日志
		payGroup.POST("/appleLog", controller.AppleLog)
		// 苹果充值错误日志
		payGroup.POST("/appleErrorLog", controller.AppleErrorLog)
		// 充值日志
		payGroup.POST("/payLog", controller.PayLog)
		// 充值排行
		payGroup.POST("/payRank", controller.PayRank)
		// googlePay
		payGroup.POST("/googleLog", controller.GoogleLog)
		// 提现日志
		payGroup.POST("/withdrawLog", controller.WithdrawLog)
		// 提现统计
		payGroup.POST("/withdrawStatList", controller.WithdrawStatList)
		// 提现排行
		payGroup.POST("/withdrawRank", controller.WithdrawRank)
		// 提现审核列表
		payGroup.POST("/withdrawAuditList", controller.WithdrawAuditList)
		// 手动充值
		payGroup.POST("/manualPay", controller.ManualPay)
		// 手动充值列表
		payGroup.POST("/manualPayList", controller.ManualPayList)
		// 提现日志
		payGroup.POST("/withdrawFlashLog", controller.WithdrawFlashLog)
		// 充值日志
		payGroup.POST("/payChipLog", controller.PayChipLog)
		// 日充值总额
		payGroup.POST("/payChipListByDay", controller.PayChipListByDay)
		// 充值卡记录
		payGroup.POST("/rechargeCard/list", card.GetRechargeCardList)
		// 兑换卡列表
		payGroup.POST("/exchangeCard/list", card.GetExchangeCardList)
		// 生成兑换卡
		payGroup.POST("/exchangeCard/gen", card.GenExchangeCard)
		// 修改兑换卡
		payGroup.POST("/exchangeCard/update", card.UpdateExchangeCard)
		// 兑换卡日志
		payGroup.POST("/exchangeCard/log", card.GetExchangeCardLog)
		// 礼品卡列表
		payGroup.POST("/giftCard/list", controller.GiftCardList)
		// 礼品卡处理
		payGroup.POST("/giftCard/deal", controller.GiftCardDeal)
		// 付费用户列表
		payGroup.POST("/userList", controller.PayUserList)
	}

	// ------------------------------卡类------------------------------
	cardGroup := rTls.Group("/card")
	{
		// 充值卡记录
		cardGroup.POST("/recharge/list", card.GetRechargeCardList)
		// 兑换卡列表
		cardGroup.POST("/exchange/list", card.GetExchangeCardList)
		// 生成兑换卡
		cardGroup.POST("/exchange/gen", card.GenExchangeCard)
		// 修改兑换卡
		cardGroup.POST("/exchange/update", card.UpdateExchangeCard)
		// 兑换卡日志
		cardGroup.POST("/exchange/log", card.GetExchangeCardLog)
		// 比赛卡列表
		cardGroup.POST("/match/list", card.GetMatchCardList)
		// 生成兑换卡
		cardGroup.POST("/match/gen", card.GenMatchCard)
		// 修改兑换卡
		cardGroup.POST("/match/update", card.UpdateMatchCard)
	}

	// ------------------------------私人场------------------------------
	roomGroup := rTls.Group("/room")
	{
		// 历史房间
		roomGroup.POST("/roomHistory", privateroom.RoomHistory)
		// 历史椅子
		roomGroup.POST("/chairHistory", privateroom.ChairHistory)
	}

	// ------------------------------邮件------------------------------
	mailGroup := rTls.Group("/mail")
	{
		// 用户邮件列表
		mailGroup.POST("/mailList", controller.ServiceMessageList)
		// 获取客服留言信息
		mailGroup.POST("/mailInfo", controller.ServiceMessageInfo)
		// 客服留言翻译
		mailGroup.POST("/mailTranslate", controller.MsgTranslate)
		// 用户留言详细信息
		mailGroup.POST("/mailDetailList", controller.ServiceDetailList)
		// 客服回复
		mailGroup.POST("/serviceSend", controller.ServiceSend)
		// 留言标签列表
		mailGroup.POST("/msgTagList", controller.MsgTagList)
		// 留言标签统计
		mailGroup.POST("/msgTagStat", controller.MsgTagStat)
		// 根据标签获取列表
		mailGroup.POST("/mailListByTag", controller.ServiceMessageListByTag)
		// 用户邮件列表
		mailGroup.POST("/timeBroadcastList", controller.TimeBroadcastList)
		// 用户留言详细信息
		mailGroup.POST("/timeBroadcastSend", controller.TimeBroadcastSend)
		// 客服回复
		mailGroup.POST("/timeBroadcastDel", controller.TimeBroadcastDel)
		// 客服留言小红点提示
		mailGroup.POST("/tip", controller.Tip)
		// 发送系统消息
		mailGroup.POST("/sysMessageSend", controller.SysMessageSend)
		// 系统消息列表
		mailGroup.POST("/sysMessageList", controller.SysMessageList)
		// 道具列表
		mailGroup.POST("/tools", controller.GetItems)
		// 用户道具列表
		mailGroup.POST("/userTools", controller.GetUserItemList)
		// 删减用户道具
		mailGroup.POST("/reduceUserTools", controller.ReduceUserItem)
		// 标签统计
		mailGroup.POST("/TagStat", controller.ServiceMessageTagStat)
		// 客服反馈列表
		mailGroup.POST("/feedbackList", controller.FeedbackList)
		// 客服反馈修改
		mailGroup.POST("/feedbackUpdate", controller.FeedbackUpdate)
	}

	// ------------------------------游戏------------------------------
	gameGroup := rTls.Group("/game")
	{
		// 获取简单游戏信息(GameID、ChineseName)
		gameGroup.POST("/getSimpleGames", controller.GetSimpleGames)
		// 审核游戏列表
		gameGroup.POST("/gameRequestList", controller.GameRequestList)
		// 添加审核
		gameGroup.POST("/gameRequestAdd", controller.GameRequestAdd)
		// 删除审核
		gameGroup.POST("/gameRequestDel", controller.GameRequestDel)
		// 所有游戏列表
		gameGroup.POST("/getAllGames", controller.GetAllGames)
		// 游戏信息修改
		gameGroup.POST("/allGameUpd", controller.AllGameUpd)
		// 获取系统返还比率
		gameGroup.POST("/getSysOdds", controller.GetSysOdds)
		// 设置系统返还比率
		gameGroup.POST("/setSysOdds", controller.SetSysOdds)
		// 获取游戏库存列表
		gameGroup.POST("/inventoryList", controller.GetGameInventoryList)
		// 更新或者修改单个库存列表
		gameGroup.POST("/inventoryUpdate", controller.UpdateGameInventory)
		// 排行榜榜单列表
		gameGroup.POST("/getRankingList", rank.GetRankingList)
	}

	// ------------------------------兑换------------------------------
	exchangeGroup := rTls.Group("/exchange")
	{
		// 兑换数值
		exchangeGroup.POST("/sys", controller.SysExchange)
		// 兑换历史
		exchangeGroup.POST("/history", controller.ExchangeHistory)
		// 兑换历史
		exchangeGroup.POST("/cash", controller.ExchangeCash)
		// 修改兑换历史
		exchangeGroup.POST("/history/update", controller.ExchangeHistoryUpdate)
	}

	// ------------------------------角色------------------------------
	roleGroup := rTls.Group("/role")
	{
		// 角色列表
		roleGroup.POST("/roleList", controller.RoleList)
		// 添加角色用户
		roleGroup.POST("/roleUserAdd", controller.RoleUserAdd)
		// 用户角色列表
		roleGroup.POST("/roleListByAdmin", controller.RoleListByAdmin)
		// 角色设置页面
		roleGroup.POST("/roleSetPage", controller.RoleSetPage)
		// 角色页面列表
		roleGroup.POST("/rolePageList", controller.RolePageList)
		// 角色用户列表gameStatHour
		roleGroup.POST("/roleUserList", controller.RoleUserList)
	}

	// ------------------------------日志------------------------------
	logGroup := rTls.Group("/log")
	{
		// 日志列表
		logGroup.POST("/logList", controller.LogList)
		// 系统日志列表
		logGroup.POST("/sysLogList", controller.SysLogList)
		// 登录日志列表
		logGroup.POST("/loginLogList", controller.LoginLogList)
		// 注册日志列表
		logGroup.POST("/registerLogList", controller.RegisterLogList)
		// 注册日志列表
		logGroup.POST("/registerLogList_V2", controller.RegisterLogList_V2)
		// 金币日志
		logGroup.POST("/getCashLog", controller.GetCashLog)
		// 钻石日志
		logGroup.POST("/getDiamondLog", controller.GetDiamondLog)
		// 后台钻石日志
		logGroup.POST("/getDiamondAdminLog", controller.GetDiamondAdminLog)
		// 投注日志
		logGroup.POST("/getBetLog", controller.GetBetLog)
		// 投注详情
		logGroup.POST("/getBetDetail", controller.GetBetDetail)
		// 流水排行
		logGroup.POST("/dailyBetRank", controller.DailyBetRank)
		// 流水列表
		logGroup.POST("/dailyBetList", controller.DailyBetList)
		// 贈送日誌
		logGroup.POST("/sendLog", controller.SendLog)
		// 道具日志
		logGroup.POST("/itemLog", controller.ItemLog)
		// 签到日志
		logGroup.POST("/signList", controller.SignList)
		// 赠送日志
		logGroup.POST("/transferLog", controller.CashTransferLog)
	}

	// ------------------------------筹码------------------------------
	chipGroup := rTls.Group("/chip")
	{
		// 筹码统计
		chipGroup.POST("/stat", controller.ChipStat)
		// 金币变化
		chipGroup.POST("/getUserStatList", controller.GetUserChipStatList)
		// 金币变化详情
		chipGroup.POST("/getStatDetail", controller.GetChipStatDetail)
		// 后台管理--导出金币变化
		chipGroup.POST("/statExport", controller.ChipStatExport)
		// 金币日志
		chipGroup.POST("/getLog", controller.GetChipLog)
		// 税收总计
		chipGroup.POST("/taxStatList", controller.ChipTaxStatList)
		// 投注日志
		chipGroup.POST("/getBetLog", controller.GetChipBetLog)
		// 发放筹码
		chipGroup.POST("/send", controller.ChipSend)
		// 扣减筹码
		chipGroup.POST("/del", controller.ChipDel)
		// 货币排行
		chipGroup.POST("/top", controller.ChipTop)
		// 存量统计
		chipGroup.POST("/statTotalList", controller.ChipStatTotalList)
		// 存量统计(每天)
		chipGroup.POST("/statTotalListByDay", controller.ChipStatTotalListByDay)
		// 赠送日志
		chipGroup.POST("/transferLog", controller.ChipTransferLog)
		// Slot 统计列表
		chipGroup.POST("/getSlotStat", slot.GetChipStat)
		// Slot 统计详情列表
		chipGroup.POST("/getSlotDetail", slot.GetChipStatDetail)
		// 元宝大厅登录统计
		chipGroup.POST("/loginStat", controller.ChipLoginStat)
		// 行为列表
		chipGroup.POST("/behaviorList", controller.BehaviorList)
	}

	// ------------------------------红包券(任务)------------------------------
	couponGroup := rTls.Group("/coupon")
	{
		// 红包券排行榜
		couponGroup.POST("/rank", coupon.CouponRank)
		// 红包券日志
		couponGroup.POST("/log", coupon.CouponLog)
		// 天统计
		couponGroup.POST("/dayStat", coupon.DayStat)
		// 小时统计
		couponGroup.POST("/hourStat", coupon.HourStat)
	}

	// ------------------------------公告------------------------------
	announceGroup := rTls.Group("/announce")
	{
		// 公告列表
		announceGroup.POST("/list", announce.GetList)
		// 发送公告
		announceGroup.POST("/send", announce.Send)
		// 修改公告
		announceGroup.POST("/update", announce.Update)
		// 删除公告
		announceGroup.POST("/del", announce.Del)
	}

	// ------------------------------配置------------------------------
	cfgGroup := rTls.Group("/cfg")
	{
		// 获取配置列表
		cfgGroup.POST("/list", platformconfig.GetConfigList)
		// 获取配置信息
		cfgGroup.POST("/info", platformconfig.GetConfig)
		// 设置配置
		cfgGroup.POST("/set", platformconfig.SetConfig)
		// 获取操作记录
		cfgGroup.POST("/log", platformconfig.GetConfigLog)
	}

	// ------------------------------聊天------------------------------
	chatGroup := rTls.Group("/chat")
	{
		// 获取机器人聊天列表
		chatGroup.POST("/list", chat.GetList)
		// 获取机器人聊天信息
		chatGroup.POST("/info", chat.GetInfo)
		// 添加机器人聊天信息
		chatGroup.POST("/add", chat.AddInfo)
		// 修改机器人聊天信息
		chatGroup.POST("/update", chat.UpdateInfo)
		// 删除机器人聊天信息
		chatGroup.POST("/del", chat.DelInfo)
	}

	// ------------------------------打点统计------------------------------
	dotGroup := rTls.Group("/dot")
	{
		// 打点列表
		dotGroup.POST("/list", dot.GetConfigListNotTask)
		// 获取打点统计
		dotGroup.POST("/statList", dot.GetStatList)
		// 获取任务打点统计
		dotGroup.POST("/taskStatList", dot.GetTaskStatList)
	}

	// ------------------------------标签------------------------------
	labelGroup := rTls.Group("/label")
	{
		// 配置列表
		labelGroup.POST("/configList", label.GetConfigList)

		// 留存统计
		labelGroup.POST("/liveStatList", label.GetLiveStatList)
	}

	// ------------------------------语聊房------------------------------
	audioRoomGroup := rTls.Group("/audioRoom")
	{
		// 获取语聊房列表
		audioRoomGroup.POST("/getRoomList", audioroom.GetRoomList)
		// 获取语聊房详细信息
		audioRoomGroup.POST("/getRoomDetail", audioroom.GetRoomDetail)
		// 获取语聊房成员
		audioRoomGroup.POST("/getRoomMember", audioroom.GetRoomMember)
		// 获取语聊房房间的在线用户
		audioRoomGroup.POST("/getRoomOnlineUsers", audioroom.GetRoomOnlineUsers)
		// 获取语聊房麦位
		audioRoomGroup.POST("/getRoomMic", audioroom.GetRoomMic)
		// 获取房间黑名单列表
		audioRoomGroup.POST("/getBlackList", audioroom.GetBlackList)
		// 获取房内操作日志
		audioRoomGroup.POST("/getRoomLogRecord", audioroom.GetRoomLogRecord)
		// 获取房间任务列表
		audioRoomGroup.POST("/getRoomTask", audioroom.GetRoomTask)
		// 获取用户的房间任务
		audioRoomGroup.POST("/getUserRoomTask", audioroom.GetUserRoomTask)
		// 获取房间上下麦日志
		audioRoomGroup.POST("/getRoomMicLog", audioroom.GetRoomMicLog)
	}

	// ------------------------------赛事数据管理------------------------------
	matchData := rTls.Group("/matchdata")
	{
		// 获取赛事列表
		matchData.POST("/getMatchList", matchdata.GetMatchList)
		// 获取加入的名单
		matchData.POST("/getJoinList", matchdata.GetJoinList)
		// 根据类型获取名单
		matchData.POST("/getTypeList", matchdata.GetTypeList)
		// 获取加入的总费用
		matchData.POST("/getJoinTotalFee", matchdata.GetJoinTotalFee)

		// 竞猜 获取赛事列表
		matchData.POST("/guess/getMatchList", guess.GetGuessMatchList)
		// 竞猜 获取赛事信息（添加成功后，获取赛事写入内存 或 查数据）
		matchData.POST("/guess/getMatchInfo", guess.GetGuessMatchInfo)
		// 竞猜 刷新赛事信息（添加成功后，获取赛事写入内存）
		matchData.POST("/guess/refreshMatchInfo", guess.RefreshGuessMatchInfo)
		// 竞猜 添加赛事
		matchData.POST("/guess/addMatch", guess.AddGuessMatch)
		// 竞猜 编辑赛事
		matchData.POST("/guess/editMatch", guess.EditGuessMatch)
		// 竞猜 设置赛事状态
		matchData.POST("/guess/setMatchOpen", guess.SetGuessMatchOpen)
		// 竞猜 派奖
		matchData.POST("/guess/award", guess.GuessMatchAward)
		// 竞猜 设置结果
		matchData.POST("/guess/setResult", guess.GuessSetResult)

		// 竞猜 添加赛事球队
		matchData.POST("/guess/addMatchTeam", guess.AddGuessMatchTeam)
		// 竞猜 修改赛事球队
		matchData.POST("/guess/editMatchTeam", guess.EditGuessMatchTeam)

		// 竞猜 添加赛事投注选项
		matchData.POST("/guess/addMatchBet", guess.AddGuessMatchBet)
		// 竞猜 修改赛事投注选项
		matchData.POST("/guess/editMatchBet", guess.EditGuessMatchBet)

		// 竞猜 获取球队列表
		matchData.POST("/guess/getTeamList", guess.GetGuessTeamList)
		// 竞猜 添加球队
		matchData.POST("/guess/addTeam", guess.AddGuessTeam)
		// 竞猜 获取球队信息
		matchData.POST("/guess/getTeamInfo", guess.GetGuessTeamInfo)
		// 竞猜 编辑球队
		matchData.POST("/guess/editTeam", guess.EditGuessTeam)
	}

	// ------------------------------作业------------------------------
	jobGroup := rTls.Group("/job")
	{
		// 作业列表
		jobGroup.POST("/jobList", controller.JobList)
		// 作业简单列表
		jobGroup.POST("/jobSimpleList", controller.JobSimpleList)
		// 作业历史列表
		jobGroup.POST("/jobHistoryList", controller.JobHistoryList)
	}

	// ------------------------------404错误------------------------------
	// 404错误
	r.NoRoute(controller.NotFoundRoute)
	r.NoMethod(controller.NotFoundMethod)

	rTls.NoRoute(controller.NotFoundRoute)
	rTls.NoMethod(controller.NotFoundMethod)

	// 启动HTTPS服务(gin默认启动服务)
	go rTls.RunTLS(fmt.Sprintf(":%d", config.Server.TlsPort), config.Server.TlsCert, config.Server.TlsKey)

	// 启动HTTP服务(gin默认启动服务)
	r.Run(fmt.Sprintf(":%d", config.Server.WebPort))

	// ------------------------------优雅关机------------------------------
	// 实现优雅的关机
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", config.Server.WebPort),
		Handler: r,
	}

	go func() {

		fmt.Printf("Listening and serving HTTP on %v\n", srv.Addr)

		// 启动HTTP服务
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// 等待中断信号来优雅地关闭服务器，为关闭服务器操作设置一个超时时长
	quit := make(chan os.Signal, 1) // 创建一个接收信号的通道

	// kill 默认会发送 syscall.SIGTERM 信号
	// kill -2 发送 syscall.SIGINT 信号,我们常用的 Ctrl+C 就是触发系统SIGINT信号
	// kill -9 发送 syscall.SIGKILL 信号，但是不能被捕获，所以不需要添加它
	// signal.Notify 把收到的 syscall.SIGINT 或 syscall.SIGTERM 信号转发给 quit
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM) // 此处不会阻塞

	<-quit // 阻塞在此，当接收到上述两种信号时才会往下执行

	fmt.Println("Shutdown Server!")

	// 创建一个30秒超时的context
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	// 优雅关闭服务（将未处理完的请求处理完再关闭服务），超时就退出
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	fmt.Println("adminserver server closed ...")
}
