package chat

import (
	"encoding/json"
	"net/http"

	"bet24.com/log"
	coreClient "bet24.com/servers/coreservice/client"
	"github.com/gin-gonic/gin"
)

// 获取机器人聊天列表
func GetList(c *gin.Context) {
	var ret struct {
		RecordCount int
		List        []robot_chat
	}
	resp := coreClient.GetRobotChatList()
	if resp.Data != "" {
		if err := json.Unmarshal([]byte(resp.Data), &ret.List); err != nil {
			log.Error("chat.GetList json unmarshal error %v", err)
		}
	}
	c.JSON(http.StatusOK, ret)
	return
}

// 获取机器人聊天信息
func GetInfo(c *gin.Context) {
	var req robot_chat
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "chat.GetInfo", err)
		return
	}

	var info robot_chat
	resp := coreClient.GetRobotChatInfo(req.UserId)
	if resp.Data != "" {
		if err := json.Unmarshal([]byte(resp.Data), &info); err != nil {
			log.Error("chat.GetInfo json unmarshal error %v", err)
		}
	}

	var ret struct {
		RecordCount int
		List        []robot_chat
	}
	ret.List = append(ret.List, info)

	c.JSON(http.StatusOK, ret)
	return
}

// 添加机器人聊天信息
func AddInfo(c *gin.Context) {
	var req robot_chat
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "chat.AddInfo", err)
		return
	}
	info := coreClient.AddRobotChatInfo(req.UserId, req.Msg, req.Seconds, req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, info)
	return
}

// 修改机器人聊天信息
func UpdateInfo(c *gin.Context) {
	var req robot_chat
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "chat.UpdateInfo", err)
		return
	}
	info := coreClient.UpdateRobotChatInfo(req.UserId, req.Msg, req.Seconds, req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, info)
	return
}

// 删除机器人聊天信息
func DelInfo(c *gin.Context) {
	var req robot_chat
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "chat.DelInfo", err)
		return
	}
	info := coreClient.DelRobotChatInfo(req.UserId)
	c.JSON(http.StatusOK, info)
	return
}
