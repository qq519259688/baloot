package chat

type robot_chat struct {
	UserId    int    // 用户id
	Msg       string // 消息
	Seconds   int    // 间隔时间(秒)
	BeginTime string // 开始时间
	EndTime   string // 截止时间
	SendTime  string // 发送时间
}
