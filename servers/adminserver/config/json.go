package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	slog "bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	uuid "github.com/satori/go.uuid"
)

type conf_game struct {
	GameID     int
	Login      string
	Password   string
	Database   string
	Datasource string
}

var Server struct {
	LogLevel        string
	FileLevel       string
	LogPath         string
	WSAddr          string
	CertFile        string
	KeyFile         string
	MaxConnNum      int
	Login           string
	Password        string
	Database        string
	Datasource      string
	ChannelUrl      string
	ChannelPassword string
	Games           []conf_game //游戏库
	AdminserverID   string
	MonitorPort     int
	WebPort         int
	WebKey          string
	MSDBDatabase    string

	TlsPort int    //HTTPS 端口
	TlsCert string //HTTPS 证书FILE
	TlsKey  string //HTTPS 证书KEY

	RedisDB     int
	ServiceAddr string

	GOOGLE_APPLICATION_CREDENTIALS string // 谷歌凭证
	GOOGLE_ZH_LANGUAGE             string // 中文
	GOOGLE_OTHER_LANGUAGE          string // 其他语言

	Exchange_Broadcast_Phone string `json:"exchange.broadcast.phone"`
	Exchange_Broadcast_Cash  string `json:"exchange.broadcast.cash"`
	Exchange_Title           string `json:"exchange.title"`
	Exchange_Content         string `json:"exchange.content"`
}

func init() {
	data, err := os.ReadFile("fishconf/adminserver.json")
	if err != nil {
		log.Fatalf("read config failed fishconf/adminserver.json ==> %v", err)
	}
	fmt.Println(string(data))
	err = json.Unmarshal(data, &Server)
	if err != nil {
		log.Fatalf("Unmarshal config failed fishconf/adminserver.json err:%v", err)
	}

	logger, err := slog.New(Server.LogLevel, Server.FileLevel, Server.LogPath, log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	// 游戏库
	for i, v := range Server.Games {
		fmt.Printf("gameid=%d database=%s\n", v.GameID, v.Database)
		if v.Login == "" {
			Server.Games[i].Login = Server.Login
		}
		if v.Password == "" {
			Server.Games[i].Password = Server.Password
		}
		if v.Datasource == "" {
			Server.Games[i].Datasource = Server.Datasource
		}
	}

	id, _ := uuid.NewV4()
	Server.AdminserverID = id.String()
	slog.Debug("Server.AdminserverID  = %v", Server.AdminserverID)
	if Server.MonitorPort == 0 {
		Server.MonitorPort = Server.WebPort + 100
	}

	loadData()
}

const config_key = "admin_config"

var AdminConfig struct {
	Phones          []string `json:"phones"`
	SMSPostUrl      string   `json:"sms.postUrl"`
	SMSAppId        string   `json:"sms.appId"`
	SMSApiKey       string   `json:"sms.apiKey"`
	SMSSendMsg      string   `json:"sms.sendMsg"`
	MoneyValue      int      `json:"warning.moneyValue"`
	MoneyMinutes    int      `json:"warning.moneyMinutes"`
	LotteryValue    int      `json:"warning.lotteryValue"`
	LotteryMinutes  int      `json:"warning.lotteryMinutes"`
	RegValue        int      `json:"warning.regValue"`
	RegMinutes      int      `json:"warning.regMinutes"`
	TransferValue   int      `json:"warning.transferValue"`
	TransferMinutes int      `json:"warning.transferMinutes"`
}

func loadData() {
	loadRedisConfig()
	go time.AfterFunc(5*time.Minute, loadData)
}

func loadRedisConfig() {
	if data := coreservice.GetPlatformConfig(config_key); data != "" {
		err := json.Unmarshal([]byte(data), &AdminConfig)
		if err == nil {
			return
		}
		slog.Release("Unmarshal config [%s] err:%v", string(data), err)
		return
	}

	if data, err := os.ReadFile("fishconf/adminConfig.json"); err == nil {
		err = json.Unmarshal([]byte(data), &AdminConfig)
		if err == nil {
			coreservice.SetPlatformConfig(config_key, string(data))
			return
		}
		slog.Release("Unmarshal config [%s] err:%v", string(data), err)
	} else {
		slog.Release("read config failed fishconf/adminConfig.json %v", err)
	}

	return
}
