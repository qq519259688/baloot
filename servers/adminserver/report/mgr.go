package report

var mgr *reportManager

type reportManager struct {
	list map[string][]*info
}

func Run() {
	mgr = new(reportManager)
	mgr.list = make(map[string][]*info)
}

// 录入日报数据
func (this *reportManager) insertDaily(info *info) {
	defer this.clear()
	insertDaily(info)
}

// 删除日报数据
func (this *reportManager) delDaily(id int) int {
	defer this.clear()
	return delDaily(id)
}

// 获取日报列表
func (this *reportManager) getDailyList(beginTime, endTime string) []*info {
	return getDailyList(beginTime, endTime)
}

// 获取挑战赛列表
func (this *reportManager) getSNGMatchList(beginTime, endTime string) []*info {
	return getSNGMatchList(beginTime, endTime)
}

// 获取游戏列表
func (this *reportManager) getGameList(beginTime, endTime string) []*info {
	return getGameList(beginTime, endTime)
}

// 清空缓存
func (this *reportManager) clear() {
	this.list = make(map[string][]*info)
	return
}

// 获取发行统计列表
func (this *reportManager) getIssueStatList(beginTime, endTime string) []*info {
	return getIssueStatList(beginTime, endTime)
}

// 时段统计报表
func (this *reportManager) timePeriodReport(beginTime, endTime string) []timePeriodInfo {
	return timePeriodReport(beginTime, endTime)
}

// 时段统计
func (this *reportManager) timePeriodStat(beginTime, endTime string) []timePeriodInfo {
	return timePeriodStat(beginTime, endTime)
}

// 时段用户列表
func (this *reportManager) timePeriodUsers(timePeriod string, beginTime, endTime string, pageIndex, pageSize int) (int, []timePeriodUser) {
	var (
		minSeconds int
		maxSeconds int
	)

	switch timePeriod {
	case "0~5分钟":
		minSeconds, maxSeconds = 0, 300
	case "5~15分钟":
		minSeconds, maxSeconds = 301, 900
	case "15~30分钟":
		minSeconds, maxSeconds = 901, 1800
	case "30~45分钟":
		minSeconds, maxSeconds = 1801, 2700
	case "45~60分钟":
		minSeconds, maxSeconds = 2701, 3600
	case "60~75分钟":
		minSeconds, maxSeconds = 3601, 4500
	case "75~90分钟":
		minSeconds, maxSeconds = 4501, 5400
	case "90~105分钟":
		minSeconds, maxSeconds = 5401, 6300
	case "105~120分钟":
		minSeconds, maxSeconds = 6301, 7200
	case "2小时以上":
		minSeconds, maxSeconds = 7201, 2000000000
	}

	return timePeriodUsers(minSeconds, maxSeconds, beginTime, endTime, pageIndex, pageSize)
}
