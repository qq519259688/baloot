package report

type req_base struct {
	BeginTime string
	EndTime   string
}

type info struct {
	Rid       int    // 标识
	DateFlag  string // 日期
	ItemType  int    // 项类型(1=计数  2=计价)
	ItemName  string // 项名称
	ItemValue string // 项值
	ItemTag   int    // 项标签
}

type timePeriodInfo struct {
	DateFlag   string
	TimePeriod string // 时段
	UserCount  int    // 人数
}

type req_timePeriodUser struct {
	TimePeriod string // 时段
	BeginTime  string // 开始时间
	EndTime    string // 截止时间
	PageIndex  int    // 页索引
	PageSize   int    // 页大小
}

type timePeriodUser struct {
	Rid       int    // 序号
	UserID    int    // 用户id
	NickName  string // 昵称
	RegTime   string // 注册时间
	LoginTime string // 登录时间
	Seconds   int    // 时长
	IpAddress string // ip地址
}
