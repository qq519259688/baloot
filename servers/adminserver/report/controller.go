package report

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 录入日报
func InsertDaily(c *gin.Context) {
	var req *info
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "report.InsertDaily", err)
		return
	}

	mgr.insertDaily(req)
	c.JSON(http.StatusOK, nil)
	return
}

// 删除日报
func DelDaily(c *gin.Context) {
	var req *info
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "report.DelDaily", err)
		return
	}

	retCode := mgr.delDaily(req.Rid)
	c.JSON(http.StatusOK, retCode)
	return
}

// 日报列表
func GetDailyList(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "report.GetDailyList", err)
		return
	}

	list := mgr.getDailyList(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 挑战赛列表
func GetSNGMatchList(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "report.GetSNGMatchList", err)
		return
	}

	list := mgr.getSNGMatchList(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 游戏列表
func GetGameList(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "report.GetGameList", err)
		return
	}

	list := mgr.getGameList(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 发行统计列表
func GetIssueStatList(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "report.GetIssueStatList", err)
		return
	}

	list := mgr.getIssueStatList(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 时段统计报表
func TimePeriodReport(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "report.TimePeriodReport", err)
		return
	}

	list := mgr.timePeriodReport(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 时段统计
func TimePeriodStat(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "report.TimePeriodStat", err)
		return
	}

	list := mgr.timePeriodStat(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 时段用户列表
func TimePeriodUsers(c *gin.Context) {
	var req req_timePeriodUser
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "report.TimePeriodUsers", err)
		return
	}

	recordCount, list := mgr.timePeriodUsers(req.TimePeriod, req.BeginTime, req.EndTime, req.PageIndex, req.PageSize)
	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: recordCount,
		List:        list,
	})
	return
}
