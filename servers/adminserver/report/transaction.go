package report

import (
	"bet24.com/servers/adminserver/ip"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
)

// 日报表录入
func insertDaily(req *info) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_DailyReport_Insert")
	statement.AddParamter("@DateFlag", database.AdParamInput, database.AdVarChar, 20, req.DateFlag)
	statement.AddParamter("@ItemType", database.AdParamInput, database.AdInteger, 4, req.ItemType)
	statement.AddParamter("@ItemName", database.AdParamInput, database.AdNVarChar, 32, req.ItemName)
	statement.AddParamter("@ItemValue", database.AdParamInput, database.AdFloat, 20, req.ItemValue)
	statement.AddParamter("@ItemTag", database.AdParamInput, database.AdInteger, 4, req.ItemTag)
	sqlString := statement.GenSql()
	dao.CenterDB.ExecSql(sqlString)
}

// 日报表删除
func delDaily(id int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_DailyReport_Del")
	statement.AddParamter("@Rid", database.AdParamInput, database.AdInteger, 4, id)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return 0
	}
	return int((*retRows[0][0].(*interface{})).(int64))
}

// 日报表列表
func getDailyList(beginTime, endTime string) []*info {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_DailyReport_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}
	var list []*info
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out info

		out.Rid = int((*ret[0].(*interface{})).(int64))
		out.DateFlag = (*ret[1].(*interface{})).(string)
		out.ItemType = int((*ret[2].(*interface{})).(int64))
		out.ItemName = (*ret[3].(*interface{})).(string)
		out.ItemValue = string((*ret[4].(*interface{})).([]byte))
		out.ItemTag = int((*ret[5].(*interface{})).(int64))
		// out.ItemValue, _ = strconv.ParseFloat(valueStr, 64)
		list = append(list, &out)
	}
	return list
}

// 挑战赛报表
func getSNGMatchList(beginTime, endTime string) []*info {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_SNGMatchReport_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}
	var list []*info
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out info

		out.Rid = int((*ret[0].(*interface{})).(int64))
		out.DateFlag = (*ret[1].(*interface{})).(string)
		out.ItemType = int((*ret[2].(*interface{})).(int64))
		out.ItemName = (*ret[3].(*interface{})).(string)
		out.ItemValue = string((*ret[4].(*interface{})).([]byte))
		out.ItemTag = int((*ret[5].(*interface{})).(int64))
		list = append(list, &out)
	}
	return list
}

// 游戏报表
func getGameList(beginTime, endTime string) []*info {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_GameReport_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}
	var list []*info
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out info

		out.Rid = int((*ret[0].(*interface{})).(int64))
		out.DateFlag = (*ret[1].(*interface{})).(string)
		out.ItemType = int((*ret[2].(*interface{})).(int64))
		out.ItemName = (*ret[3].(*interface{})).(string)
		out.ItemValue = string((*ret[4].(*interface{})).([]byte))
		out.ItemTag = int((*ret[5].(*interface{})).(int64))
		list = append(list, &out)
	}
	return list
}

// 发行统计列表
func getIssueStatList(beginTime, endTime string) []*info {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Issue_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}
	var list []*info
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out info

		out.Rid = int((*ret[0].(*interface{})).(int64))
		out.DateFlag = (*ret[1].(*interface{})).(string)
		out.ItemType = int((*ret[2].(*interface{})).(int64))
		out.ItemName = (*ret[3].(*interface{})).(string)
		out.ItemValue = string((*ret[4].(*interface{})).([]byte))
		// out.ItemValue, _ = strconv.ParseFloat(valueStr, 64)
		list = append(list, &out)
	}
	return list
}

// 时段统计报表
func timePeriodReport(beginTime, endTime string) []timePeriodInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_TimePeriod_Report")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}
	var list []timePeriodInfo
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out timePeriodInfo

		out.TimePeriod = (*ret[0].(*interface{})).(string)
		out.UserCount = int((*ret[1].(*interface{})).(int64))
		list = append(list, out)
	}
	return list
}

// 时段统计
func timePeriodStat(beginTime, endTime string) []timePeriodInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_TimePeriod_GetStatList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}
	var list []timePeriodInfo
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out timePeriodInfo

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.TimePeriod = (*ret[1].(*interface{})).(string)
		out.UserCount = int((*ret[2].(*interface{})).(int64))
		list = append(list, out)
	}
	return list
}

// 时段用户列表
func timePeriodUsers(minSeconds, maxSeconds int, beginTime, endTime string, pageIndex, pageSize int) (int, []timePeriodUser) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var (
		recordCount int
		list        []timePeriodUser
	)
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_TimePeriod_GetUserList")
	statement.AddParamter("@MinSeconds", database.AdParamInput, database.AdInteger, 4, minSeconds)
	statement.AddParamter("@MaxSeconds", database.AdParamInput, database.AdInteger, 4, maxSeconds)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return recordCount, list
	}
	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var out timePeriodUser

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			out.RegTime = (*ret[3].(*interface{})).(string)
			out.LoginTime = (*ret[4].(*interface{})).(string)
			out.Seconds = int((*ret[5].(*interface{})).(int64))
			out.IpAddress = (*ret[6].(*interface{})).(string)
			out.IpAddress = ip.GetCountryAndRegion(out.IpAddress, false)
			list = append(list, out)
		}
	}

	recordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return recordCount, list
}
