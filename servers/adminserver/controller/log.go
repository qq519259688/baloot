package controller

import (
	"bet24.com/servers/adminserver/character"
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

// 日志列表
func LogList(c *gin.Context) {
	obj := dao.NewLogList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "logList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 系统日志列表
func SysLogList(c *gin.Context) {
	obj := dao.NewSysLogList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "sysLogList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 登录日志列表
func LoginLogList(c *gin.Context) {
	obj := dao.NewLoginLogList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "loginLogList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 注册日志列表
func RegisterLogList(c *gin.Context) {
	obj := dao.NewRegisterLogList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "registerLogList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 注册日志列表
func RegisterLogList_V2(c *gin.Context) {
	obj := dao.NewRegisterLogList_v2()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "RegisterLogList_V2", err)
		return
	}

	obj.In.UTMSources = character.GetSpecialCharacter(obj.In.UTMSources)
	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 流水排行
func DailyBetRank(c *gin.Context) {
	obj := dao.NewDailyBetRank()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "dailyBetRank", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 流水列表
func DailyBetList(c *gin.Context) {
	obj := dao.NewDailyBetList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "dailyBetList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 贈送日誌
func SendLog(c *gin.Context) {
	obj := dao.NewSendLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "sendLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}
