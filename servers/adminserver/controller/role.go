package controller

import (
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

//角色列表
func RoleList(c *gin.Context) {
	obj := dao.NewRoleList()
	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

//添加角色用户
func RoleUserAdd(c *gin.Context) {
	obj := dao.NewRoleUserAdd()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "roleUserAdd", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, nil)
	return
}

//用户角色列表
func RoleListByAdmin(c *gin.Context) {
	obj := dao.NewRoleListByAdmin()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "roleListByAdmin", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

//角色设置页面
func RoleSetPage(c *gin.Context) {
	obj := dao.NewRoleSetPage()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "roleSetPage", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

//角色页面列表
func RolePageList(c *gin.Context) {
	obj := dao.NewRolePageList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "rolePageList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

//角色用户列表
func RoleUserList(c *gin.Context) {
	obj := dao.NewRoleUserList()
	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}
