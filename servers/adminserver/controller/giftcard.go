package controller

import (
	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"bet24.com/servers/adminserver/item"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 礼品卡列表
func GiftCardList(c *gin.Context) {
	obj := dao.NewGiftCardList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GiftCardList", err)
		return
	}

	obj.DoAction(nil)

	if len(obj.Out.List) > 0 {
		items := item.GetSysItems()
		for i, v := range obj.Out.List {
			value, ok := items[v.ItemID]
			if !ok {
				continue
			}
			obj.Out.List[i].ItemName = value.Name
			obj.Out.List[i].ItemDesc = value.Desc
		}
	}

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 礼品卡处理
func GiftCardDeal(c *gin.Context) {
	obj := dao.NewGiftCardDeal()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GiftCardDeal", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}
