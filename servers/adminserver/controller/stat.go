package controller

import (
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

// 日常统计
func DailyStat(c *gin.Context) {
	obj := dao.NewDailyStat()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "dailyStat", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 在线统计
func OnlineStat(c *gin.Context) {
	obj := dao.NewOnlineStat()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "onlineStat", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 在线统计(每天)
func OnlineStatListByDay(c *gin.Context) {
	obj := dao.NewOnlineStatListByDay()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "onlineStatListByDay", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 在线统计报表(小时)
func OnlineStatReport(c *gin.Context) {
	obj := dao.NewOnlineStatReport()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "onlineStatReport", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 同时在线统计报表(小时)
func OnlineUserReport(c *gin.Context) {
	obj := dao.NewOnlineUserReport()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "OnlineUserReport", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 游戏统计(小时)
func GameStatHour(c *gin.Context) {
	obj := dao.NewGameStatHour()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "gameStatHour", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 存量统计
func MoneyStatTotalList(c *gin.Context) {
	obj := dao.NewMoneyStatTotalList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "moneyStatTotalList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 存量统计(每天)
func MoneyStatTotalListByDay(c *gin.Context) {
	obj := dao.NewMoneyStatTotalListByDay()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "moneyStatTotalListByDay", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 货币排行
func CurrencyTop(c *gin.Context) {
	obj := dao.NewCurrencyTop()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "currencyTop", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 货币变化
func GetUserMoneyStatList(c *gin.Context) {
	obj := dao.NewGetUserMoneyStatList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getUserMoneyStatList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 用户货币变化详情
func GetMoneyStatDetail(c *gin.Context) {
	obj := dao.NewGetMoneyStatDetail()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getMoneyStatDetail", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 游戏计
func GameDailyStat(c *gin.Context) {
	obj := dao.NewGameDailyStat()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "gameDailyStat", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 后台管理--导出金币变化
func MoneyStatExport(c *gin.Context) {
	obj := dao.NewMoneyStatExport()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "moneyStatExport", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 用户日常统计
func UserDailyStat(c *gin.Context) {
	obj := dao.NewUserDailyStat()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "userDailyStat", err)
		return
	}
	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 税收总计
func TaxStatList(c *gin.Context) {
	obj := dao.NewTaxStatList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "TaxStatList", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 游戏记录详情报表
func GameRecordReportDetail(c *gin.Context) {
	obj := dao.NewGameRecordReportDetail()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GameRecordReportDetail", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 注册用户转化报表
func RegConvertReport(c *gin.Context) {
	obj := dao.NewRegConvertReport()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "RegConvertReport", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 登录用户转化报表
func LoginConvertReport(c *gin.Context) {
	obj := dao.NewLoginConvertReport()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "LoginConvertReport", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 留存统计
func LiveStatList(c *gin.Context) {
	obj := dao.NewLiveStatList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "LiveStatList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 渠道留存统计
func LiveStatListByPartner(c *gin.Context) {
	obj := dao.NewLiveStatListByPartner()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "LiveStatListByPartner", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 渠道登录统计
func LoginStatByPartner(c *gin.Context) {
	obj := dao.NewLoginStatPartner()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "LoginStatByPartner", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 在线统计报表(小时)
func OnlineStatReportChip(c *gin.Context) {
	obj := dao.NewOnlineStatReportChip()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "onlineStatReportChip", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 同时在线统计报表(小时)
func OnlineUserReportChip(c *gin.Context) {
	obj := dao.NewOnlineUserReportChip()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "OnlineUserReportChip", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 游戏统计(小时)
func GameStatHourChip(c *gin.Context) {
	obj := dao.NewGameStatHourChip()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "gameStatHourChip", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 在线统计(每天)
func OnlineStatListByDayChip(c *gin.Context) {
	obj := dao.NewOnlineStatListByDayChip()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "onlineStatListByDay", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 留存统计
func RetentionStatList(c *gin.Context) {
	obj := dao.NewRetentionStatList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "RetentionStatList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}
