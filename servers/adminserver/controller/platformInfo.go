package controller

import (
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

//平台信息列表
func PlatformInfoList(c *gin.Context) {
	obj := dao.NewPlatformInfoList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "platformInfoList", err)
		return
	}
	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

//平台信息修改
func PlatformInfoUp(c *gin.Context) {
	obj := dao.NewPlatformInfoUp()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "platformInfoUp", err)
		return
	}
	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

//平台配置信息
func PlatformConfig(c *gin.Context) {
	obj := dao.NewPlatformConfig()
	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

//修改平台配置信息
func PlatformConfigUp(c *gin.Context) {
	obj := dao.NewPlatformConfigUp()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "platformConfigUp", err)
		return
	}
	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}
