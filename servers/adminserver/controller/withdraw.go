package controller

import (
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

// 提现日志
func WithdrawLog(c *gin.Context) {
	obj := dao.NewWithdrawLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "WithdrawLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 提现统计
func WithdrawStatList(c *gin.Context) {
	obj := dao.NewWithdrawStatList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "WithdrawStatList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 提现排行
func WithdrawRank(c *gin.Context) {
	obj := dao.NewWithdrawRank()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "WithdrawRank", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 提现审核列表
func WithdrawAuditList(c *gin.Context) {
	obj := dao.NewWithdrawAuditList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "WithdrawAuditList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 提现日志
func WithdrawFlashLog(c *gin.Context) {
	obj := dao.NewWithdrawFlashLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "WithdrawFlashLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}
