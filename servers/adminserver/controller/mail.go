package controller

import (
	"bet24.com/log"
	"bet24.com/servers/adminserver/character"
	"bet24.com/servers/adminserver/config"
	"bet24.com/servers/adminserver/dao"
	"bet24.com/servers/adminserver/google"
	"bet24.com/servers/adminserver/item"
	"bet24.com/servers/adminserver/serverdata"
	item_inventory "bet24.com/servers/micros/item_inventory/proto"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 留言板列表
func ServiceMessageList(c *gin.Context) {
	obj := dao.NewServiceMessageList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "serviceMessageList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 用户留言详细信息
func ServiceDetailList(c *gin.Context) {
	obj := dao.NewServiceDetailList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "serviceDetailList", err)
		return
	}

	obj.DoAction(nil)

	// 是否开启翻译
	if ok, _ := google.IsTranslate(); ok {
		for idx, v := range obj.Out.List {
			if v.Msg == "" || v.TransMsg != "" {
				continue
			}

			transMsg, err := google.Translate(config.Server.GOOGLE_OTHER_LANGUAGE, config.Server.GOOGLE_ZH_LANGUAGE, v.Msg)
			if err != nil {
				log.Error("controller.ServiceDetailList google.Translate error %+v --> %s", err, v.Msg)
				continue
			}

			obj.Out.List[idx].TransMsg = transMsg

			transObj := dao.NewMsgTranslate()
			transObj.In.MessageID = v.MessageID
			transObj.In.OpUserID = -1
			transObj.In.OpUserName = "System"
			transObj.In.TransMsg = transMsg
			go transObj.DoAction()
		}
	}

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 客服回复
func ServiceSend(c *gin.Context) {
	obj := dao.NewServiceSend()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "serviceSend", err)
		return
	}

	obj.In.Msg = character.GetSpecialCharacter(obj.In.Msg)
	obj.In.TransMsg = character.GetSpecialCharacter(obj.In.TransMsg)

	// 是否开启翻译
	if ok, _ := google.IsTranslate(); ok {

		log.Debug("ServiceSend obj.In 2 ==> %+v", obj.In)

		if obj.In.Msg == "" && obj.In.TransMsg != "" {
			msg, err := google.Translate(config.Server.GOOGLE_ZH_LANGUAGE, config.Server.GOOGLE_OTHER_LANGUAGE, obj.In.TransMsg)
			if err != nil {
				log.Error("controller.ServiceDetailList google.Translate error %+v --> %s", err, obj.In.TransMsg)
			}

			obj.In.Msg = msg
		}
	} else if obj.In.TransMsg != "" {
		obj.In.Msg = obj.In.TransMsg
		obj.In.TransMsg = ""
	}

	// 没有消息，不允许发送
	if obj.In.Msg == "" {
		c.JSON(http.StatusOK, nil)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, nil)
	return
}

// 系统广播列表
func TimeBroadcastList(c *gin.Context) {
	obj := dao.NewTimeBroadcastList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "timeBroadcastList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 定时广播发送
func TimeBroadcastSend(c *gin.Context) {
	obj := dao.NewTimeBroadcastSend()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "TimeBroadcastSend", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, nil)
	return
}

// 定时广播删除
func TimeBroadcastDel(c *gin.Context) {
	obj := dao.NewTimeBroadcastDel()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "timeBroadcastDel", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, nil)
	return
}

// 客服留言小红点提示
func Tip(c *gin.Context) {
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: serverdata.Tip.GetList(),
	})
	return
}

// 发送系统消息
func SysMessageSend(c *gin.Context) {
	obj := dao.NewSysMessageSend()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "sysMessageSend", err)
		return
	}

	var items []item_inventory.ItemPack
	items = append(items, item.AddItems(obj.In.ItemID_1, obj.In.ItemCount_1)...)
	items = append(items, item.AddItems(obj.In.ItemID_2, obj.In.ItemCount_2)...)
	items = append(items, item.AddItems(obj.In.ItemID_3, obj.In.ItemCount_3)...)
	items = append(items, item.AddItems(obj.In.ItemID_4, obj.In.ItemCount_4)...)
	items = append(items, item.AddItems(obj.In.ItemID_5, obj.In.ItemCount_5)...)

	if len(items) > 0 {
		buff, _ := json.Marshal(items)
		obj.In.Tools = string(buff)
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 获取系统消息列表
func SysMessageList(c *gin.Context) {
	obj := dao.NewSysMessageList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "sysMessageList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 客服留言翻译
func MsgTranslate(c *gin.Context) {
	obj := dao.NewMsgTranslate()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "MsgTranslate", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, "")
	return
}

// 留言标签列表
func MsgTagList(c *gin.Context) {
	obj := dao.NewMsgTagList()
	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 留言标签统计
func MsgTagStat(c *gin.Context) {
	obj := dao.NewMsgTagStat()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "MsgTagStat", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 根据标签获取列表
func ServiceMessageListByTag(c *gin.Context) {
	obj := dao.NewServiceMessageListByTag()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ServiceMessageListByTag", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 获取客服留言信息
func ServiceMessageInfo(c *gin.Context) {
	obj := dao.NewServiceMessageInfo()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ServiceMessageInfo", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 标签统计
func ServiceMessageTagStat(c *gin.Context) {
	obj := dao.NewServiceMessageTagStat()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ServiceMessageTagStat", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}
