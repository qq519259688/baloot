package controller

import (
	"bet24.com/servers/adminserver/item"
	item_inventory "bet24.com/servers/micros/item_inventory/proto"
	"net/http"
	"sort"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

func ItemLog(c *gin.Context) {
	obj := dao.NewUserItemLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "itemLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

func GetItems(c *gin.Context) {
	type tool struct {
		Id   int    //道具ID
		Type int    //道具类型
		Name string //道具名称
		Desc string //道具描述
	}

	items := item.GetSysItems()

	var tools []tool
	for _, v := range items {
		tools = append(tools, tool{
			Id:   v.Id,
			Type: v.Type,
			Name: v.Name,
			Desc: v.Desc,
		})
	}

	//道具ID排序
	sort.SliceStable(tools, func(i, j int) bool {
		return tools[i].Id < tools[j].Id
	})

	//道具类型排序
	sort.SliceStable(tools, func(i, j int) bool {
		return tools[i].Type < tools[j].Type
	})

	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        []tool
	}{
		RecordCount: len(tools),
		List:        tools,
	})
	return
}

func GetUserItemList(c *gin.Context) {
	obj := dao.NewUserItemList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getUserItemList", err)
		return
	}

	obj.DoAction(nil)

	if len(obj.Out.List) > 0 {
		items := item.GetSysItems()
		for i, v := range obj.Out.List {
			value, ok := items[v.ItemID]
			if !ok {
				continue
			}
			obj.Out.List[i].ItemName = value.Name
		}
	}

	c.JSON(http.StatusOK, obj.Out)
	return
}

func ReduceUserItem(c *gin.Context) {
	var req struct {
		UserId     int
		ItemId     int
		Count      int
		OpUserID   int
		OpUserName string
	}

	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "reduceUserItem", err)
		return
	}

	_, errorMsg := item_inventory.ReduceItemByAdmin(req.OpUserID, req.OpUserName, req.UserId, req.ItemId, req.Count)
	c.String(http.StatusOK, errorMsg)
	return
}
