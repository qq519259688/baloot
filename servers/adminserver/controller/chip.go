package controller

import (
	"encoding/json"
	"net/http"

	"bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/adminserver/dao"
	notification "bet24.com/servers/micros/notification/proto"
	"github.com/gin-gonic/gin"
)

// 筹码日志
func GetChipLog(c *gin.Context) {
	obj := dao.NewGetChipLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GetChipLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 税收总计
func ChipTaxStatList(c *gin.Context) {
	obj := dao.NewChipTaxStatList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChipTaxStatList", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 筹码统计
func ChipStat(c *gin.Context) {
	obj := dao.NewChipStat()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChipStat", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 筹码变化
func GetUserChipStatList(c *gin.Context) {
	obj := dao.NewGetUserChipStatList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GetUserChipStatList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 用户筹码变化详情
func GetChipStatDetail(c *gin.Context) {
	obj := dao.NewGetChipStatDetail()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GetChipStatDetail", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 后台管理--导出金币变化
func ChipStatExport(c *gin.Context) {
	obj := dao.NewChipStatExport()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChipStatExport", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 投注日志
func GetChipBetLog(c *gin.Context) {
	obj := dao.NewGetChipBetLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GetChipBetLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 发放筹码
func ChipSend(c *gin.Context) {
	obj := dao.NewChipSend()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChipSend", err)
		return
	}

	obj.DoAction(nil)

	if obj.Out.RetCode == 1 {
		// 通知客户端
		notification.AddNotification(obj.In.UserID, notification.Notification_Chip, "")
		notifyChanged(obj.In.UserID)
	}

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 扣减筹码
func ChipDel(c *gin.Context) {
	obj := dao.NewChipDel()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChipDel", err)
		return
	}

	obj.DoAction(nil)

	if obj.Out.RetCode == 1 {
		// 通知客户端
		notification.AddNotification(obj.In.UserID, notification.Notification_Chip, "")
		notifyChanged(obj.In.UserID)
	}

	c.JSON(http.StatusOK, obj.Out)
	return
}

func notifyChanged(userId int) {
	var d redis.Channel_msg
	d.Message = "RefreshChip"
	d.UserID = userId
	js, _ := json.Marshal(d)
	redis.Publish(string(js))
}

// 筹码排行
func ChipTop(c *gin.Context) {
	obj := dao.NewChipTop()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChipTop", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 存量统计
func ChipStatTotalList(c *gin.Context) {
	obj := dao.NewChipStatTotalList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChipStatTotalList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 存量统计(每天)
func ChipStatTotalListByDay(c *gin.Context) {
	obj := dao.NewChipStatTotalListByDay()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChipStatTotalListByDay", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 赠送日志
func ChipTransferLog(c *gin.Context) {
	obj := dao.NewTransferLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChipTransferLog", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

func ChipLoginStat(c *gin.Context) {
	obj := dao.NewChipLoginStat()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChipLoginStat", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

func BehaviorList(c *gin.Context) {
	obj := dao.NewBehaviorList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "BehaviorList", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}
