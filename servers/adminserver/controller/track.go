package controller

import (
	"bet24.com/servers/adminserver/badge"
	"bet24.com/servers/adminserver/gift"
	"bet24.com/servers/adminserver/item"
	"bet24.com/servers/adminserver/shop"
	"bet24.com/servers/adminserver/task"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

// 获取轨迹产品id、道具id等信息
func getTrackId(trackLevel, trackItem string) (oldItem, name string) {
	var (
		itemId    string
		productId string
		giftId    string
		taskId    string
		badgeId   string
	)

	switch trackLevel {
	case "充值成功":
		productId = trackItem

	case "商城":
		if strings.Contains(trackItem, "道具 --> 点击") {
			oldItem = "道具 --> 点击"
		} else if strings.Contains(trackItem, "购买弹窗 --> 打开shop/") {
			oldItem = "购买弹窗 --> 打开shop/"
		} else if strings.Contains(trackItem, "购买弹窗 --> 打开") {
			oldItem = "购买弹窗 --> 打开"
		} else if strings.Contains(trackItem, "购买弹窗 --> 点击购买") {
			oldItem = "购买弹窗 --> 点击购买"
		} else if strings.Contains(trackItem, "金币或钻石 --> 点击") {
			oldItem = "金币或钻石 --> 点击"
		}
		if oldItem == "" {
			log.Debug("track.getTrackId is empty ==>level=%s item=%s", trackLevel, trackItem)
			return
		}

		switch oldItem {
		case "道具 --> 点击":
			fallthrough
		case "购买弹窗 --> 打开":
			itemId = strings.ReplaceAll(trackItem, oldItem, "")
		default:
			productId = strings.ReplaceAll(trackItem, oldItem, "")
		}

	case "新手充值":
		if strings.Contains(trackItem, "点击") {
			oldItem = "点击"
		}
		if oldItem == "" {
			log.Debug("track.getTrackId is empty ==>level=%s item=%s", trackLevel, trackItem)
			return
		}

		productId = strings.ReplaceAll(trackItem, oldItem, "")

	case "首冲":
		if strings.Contains(trackItem, "购买 --> ") {
			oldItem = "购买 --> "
		}
		if oldItem == "" {
			log.Debug("track.getTrackId is empty ==>level=%s item=%s", trackLevel, trackItem)
			return
		}

		productId = strings.ReplaceAll(trackItem, oldItem, "")

	case "背包":
		if strings.Contains(trackItem, "道具 --> 点击") {
			oldItem = "道具 --> 点击"
		} else if strings.Contains(trackItem, "礼物 --> 点击") {
			oldItem = "礼物 --> 点击"
		}

		if oldItem == "" {
			log.Debug("track.getTrackId is empty ==>level=%s item=%s", trackLevel, trackItem)
			return
		}

		switch oldItem {
		case "道具 --> 点击":
			fallthrough
		case "礼物 --> 点击":
			itemId = strings.ReplaceAll(trackItem, oldItem, "")
		default:
			productId = strings.ReplaceAll(trackItem, oldItem, "")
		}

	case "礼物赠送":
		if strings.Contains(trackItem, "礼物Id:") {
			oldItem = "礼物Id:"
		}
		if oldItem == "" {
			log.Debug("track.getTrackId is empty ==>level=%s item=%s", trackLevel, trackItem)
			return
		}

		giftId = strings.ReplaceAll(trackItem, oldItem, "")

	case "送礼物":
		if strings.Contains(trackItem, "赠送 --> 点击") {
			oldItem = "赠送 --> 点击"
		}

		if oldItem == "" {
			log.Debug("track.getTrackId is empty ==>level=%s item=%s", trackLevel, trackItem)
			return
		}

		giftId = strings.ReplaceAll(trackItem, oldItem, "")

	case "vip":
		if strings.Contains(trackItem, "vip卡 --> 点击") {
			oldItem = "vip卡 --> 点击"
		}

		if oldItem == "" {
			log.Debug("track.getTrackId is empty ==>level=%s item=%s", trackLevel, trackItem)
			return
		}

		itemId = strings.ReplaceAll(trackItem, oldItem, "")

	case "任务":
		if strings.Contains(trackItem, "领取 --> ") {
			oldItem = "领取 --> "
		} else if strings.Contains(trackItem, "前往 --> ") {
			oldItem = "前往 --> "
		}

		if oldItem == "" {
			log.Debug("track.getTrackId is empty ==>level=%s item=%s", trackLevel, trackItem)
			return
		}

		taskId = strings.ReplaceAll(trackItem, oldItem, "")

	case "徽章佩戴":
		//if strings.Contains(trackItem, "Id:") {
		//	oldItem = "Id:"
		//}

		if oldItem == "" {
			log.Debug("track.getTrackId is empty ==>level=%s item=%s", trackLevel, trackItem)
			return
		}
	}

	if productId != "" {
		if name = shop.GetProductName(productId); name == "" {
			return
		}
		name = fmt.Sprintf("%s （商品id:%s）", name, productId)
	} else if itemId != "" {
		id, err := strconv.Atoi(itemId)
		if err != nil {
			log.Error("track.getTrackId itemId=%s err=%+v", itemId, err)
			return
		}

		if name = item.GetItemName(id); name == "" {
			return
		}
		name = fmt.Sprintf("%s （道具id:%d）", name, id)
	} else if giftId != "" {
		id, err := strconv.Atoi(giftId)
		if err != nil {
			log.Error("track.getTrackId giftId=%s err=%+v", giftId, err)
			return
		}
		if name = gift.GetGiftName(id); name == "" {
			return
		}
		name = fmt.Sprintf("%s （礼物id:%d）", name, id)
	} else if taskId != "" {
		id, err := strconv.Atoi(taskId)
		if err != nil {
			log.Error("track.getTrackId giftId=%s err=%+v", taskId, err)
			return
		}
		if name = task.GetTaskName(id); name == "" {
			return
		}
		name = fmt.Sprintf("%s （任务id:%d）", name, id)
	} else if badgeId != "" {
		id, err := strconv.Atoi(badgeId)
		if err != nil {
			log.Error("track.getTrackId giftId=%s err=%+v", badgeId, err)
			return
		}
		if name = badge.GetBadgeName(id); name == "" {
			return
		}
		name = fmt.Sprintf("%s （徽章id:%d）", name, id)
	}

	return
}

// 游戏轨迹记录
func UserTrackList(c *gin.Context) {
	obj := dao.NewUserTrackList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "userTrackList", err)
		return
	}
	obj.DoAction()

	for i, v := range obj.Out.List {
		oldItem, name := getTrackId(v.Level, v.Item)
		if name == "" {
			continue
		}

		obj.Out.List[i].Item = fmt.Sprintf("%s %s", oldItem, name)
	}

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 游戏轨迹统计
func UserTrackStat(c *gin.Context) {
	obj := dao.NewUserTrackStat()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "userTrackStat", err)
		return
	}
	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 用户足迹
func TrackList(c *gin.Context) {
	obj := dao.NewTrackList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "TrackList", err)
		return
	}
	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}
