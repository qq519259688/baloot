package controller

import (
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

func ScoreRankList(c *gin.Context) {
	obj := dao.NewScoreRankList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ScoreRankList", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

func AddRankScore(c *gin.Context) {
	obj := dao.NewAddScore()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "AddRankScore", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, "")
	return
}
