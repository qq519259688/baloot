package controller

import (
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

//签到列表
func SignList(c *gin.Context) {
	obj := dao.NewSignList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "signList", err)
		return
	}
	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}
