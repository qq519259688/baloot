package controller

import (
	"net/http"

	"bet24.com/servers/adminserver/serverdata"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

//渠道列表
func PartnerList(c *gin.Context) {
	list := serverdata.Partners.GetPartners()
	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: len(list),
		List:        list,
	})
	return
}

//添加渠道
func PartnerAdd(c *gin.Context) {
	obj := dao.NewPartnerAdd()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "partnerAdd", err)
		return
	}

	obj.DoAction(nil)

	serverdata.Partners.Refresh()

	c.JSON(http.StatusOK, obj.Out)
	return
}

//删除渠道
func PartnerDel(c *gin.Context) {
	obj := dao.NewPartnerDel()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "partnerDel", err)
		return
	}

	obj.DoAction(nil)

	serverdata.Partners.Refresh()

	c.JSON(http.StatusOK, obj.Out)
	return
}
