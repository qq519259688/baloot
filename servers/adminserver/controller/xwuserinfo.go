package controller

import (
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

//闲玩统计
func XwInfoStat(c *gin.Context) {
	obj := dao.NewXwInfoStat()
	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
}

//闲玩用户信息
func XwUserInfoList(c *gin.Context) {
	obj := dao.NewXwUserInfoList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "xwUserInfoList", err)
		return
	}
	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
}
