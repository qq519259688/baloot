package controller

import (
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

// 白名单列表
func WhiteList(c *gin.Context) {
	obj := dao.NewWhiteList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "WhiteList", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 添加白名单
func WhiteAdd(c *gin.Context) {
	obj := dao.NewWhiteAdd()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "WhiteAdd", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, nil)
	return
}

// 删除白名单
func WhiteDel(c *gin.Context) {
	obj := dao.NewWhiteDel()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "WhiteDel", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, nil)
	return
}
