package controller

import (
	"bet24.com/servers/adminserver/shop"
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"

	coreClient "bet24.com/servers/coreservice/client"
)

// 订单列表
func OrderList(c *gin.Context) {
	obj := dao.NewOrderList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "orderList", err)
		return
	}

	obj.DoAction(nil)
	for i, v := range obj.Out.List {
		productName := shop.GetProductFormatStr(v.ProductID)
		if len(productName) <= 0 {
			continue
		}
		obj.Out.List[i].ProductName = productName
	}

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 获取各渠道充值列表
func PayList(c *gin.Context) {
	obj := dao.NewPayList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "payList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 充值统计列表
func PayStatList(c *gin.Context) {
	obj := dao.NewPayStatList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "payStatList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 登录渠道充值统计
func PayListByPartner(c *gin.Context) {
	obj := dao.NewPayListByPartner()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "payListByPartner", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 日充值总额
func PayListByDay(c *gin.Context) {
	obj := dao.NewPayListByDay()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "payListByDay", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 充值日志
func PayLog(c *gin.Context) {
	obj := dao.NewPayLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "payLog", err)
		return
	}

	obj.DoAction(nil)
	for i, v := range obj.Out.List {
		productName := shop.GetProductFormatStr(v.ProductID)
		if len(productName) <= 0 {
			continue
		}
		obj.Out.List[i].ProductName = productName
	}

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 充值排行
func PayRank(c *gin.Context) {
	obj := dao.NewPayRank()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "payRank", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 苹果日志
func AppleLog(c *gin.Context) {
	obj := dao.NewAppleLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "appleLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 苹果错误日志
func AppleErrorLog(c *gin.Context) {
	obj := dao.NewAppleErrorLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "appleErrorLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// Google日志
func GoogleLog(c *gin.Context) {
	obj := dao.NewGoogleLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "appleErrorLog", err)
		return
	}

	obj.DoAction()
	for i, v := range obj.Out.List {
		productName := shop.GetProductFormatStr(v.ProductID)
		if len(productName) <= 0 {
			continue
		}
		obj.Out.List[i].ProductName = productName
	}
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 手动充值
func ManualPay(c *gin.Context) {
	obj := dao.NewManualPay()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "manualPay", err)
		return
	}
	obj.DoAction()
	if obj.Out.RetCode == 1 {
		// 充值
		resp := coreClient.Recharge(obj.In.UserID, obj.In.ProductID)
		log.Debug("%s 充值成功 %+v", "manualPay", resp)
	}
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 手动充值列表
func ManualPayList(c *gin.Context) {
	obj := dao.NewManualPayList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "manualPayList", err)
		return
	}
	obj.DoAction()
	for i, v := range obj.Out.List {
		productName := shop.GetProductFormatStr(v.ProductID)
		if len(productName) <= 0 {
			continue
		}
		obj.Out.List[i].ProductName = productName
	}
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 充值日志
func PayChipLog(c *gin.Context) {
	obj := dao.NewPayChipLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "PayChipLog", err)
		return
	}

	obj.DoAction(nil)
	for i, v := range obj.Out.List {
		productName := shop.GetProductFormatStr(v.ProductID)
		if len(productName) <= 0 {
			continue
		}
		obj.Out.List[i].ProductName = productName
	}

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 日充值总额
func PayChipListByDay(c *gin.Context) {
	obj := dao.NewPayChipListByDay()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "PayChipListByDay", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 付费用户列表
func PayUserList(c *gin.Context) {
	obj := dao.NewPayUserList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "PayUserList", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}
