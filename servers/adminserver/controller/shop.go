package controller

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"encoding/json"
)

// 汇率
type exchangeRate struct {
	Currency    string  // 币种
	CountryName string  // 国家名称
	CountryCode string  // 国家编码
	Rate        float64 // 汇率
}

func GetExchangeRate(currency string) *exchangeRate {
	var list []*exchangeRate

	respExchange := client.GetExchangeRateList()
	if respExchange.RetCode != 1 {
		log.Error("%s PayRequest client ==> %+v", "GetExchangeRate.GetExchangeRateList", respExchange)
		return nil
	}

	if err := json.Unmarshal([]byte(respExchange.Data), &list); err != nil {
		log.Error("%s PayRequest json unmarshal err %v", "GetExchangeRate.GetExchangeRateList", err)
		return nil
	}

	var info *exchangeRate

	for _, v := range list {
		info = v
		if info.Currency == currency {
			return info
		}
	}

	return info
}
