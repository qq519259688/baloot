package controller

import (
	"net/http"

	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

// 流量渠道列表
func UTMSourceList(c *gin.Context) {
	list := dao.GetUTMSource()
	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: len(list),
		List:        list,
	})
	return
}
