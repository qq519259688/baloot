package controller

import (
	waterPool "bet24.com/servers/micros/waterpool/proto"
	"net/http"

	"bet24.com/servers/adminserver/dao"

	"bet24.com/log"
	"bet24.com/servers/adminserver/serverdata"
	"github.com/gin-gonic/gin"
)

// 获取简单游戏信息(GameID、ChineseName)
func GetSimpleGames(c *gin.Context) {
	list := serverdata.Games.GetSimpleGames()
	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: len(list),
		List:        list,
	})
	return
}

// 审核游戏列表
func GameRequestList(c *gin.Context) {
	obj := dao.NewGetGameRequestList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "gameRequestList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 添加审核游戏
func GameRequestAdd(c *gin.Context) {
	obj := dao.NewGameRequestAdd()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "gameRequestAdd", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 删除审核游戏
func GameRequestDel(c *gin.Context) {
	obj := dao.NewGameRequestDel()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "gameRequestDel", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 所有游戏列表
func GetAllGames(c *gin.Context) {
	var info struct {
		GameID int
	}

	if err := c.ShouldBind(&info); err != nil {
		log.Debug("%s shouldBind err %v", "getAllGames", err)
		return
	}

	if info.GameID > 0 {
		var list []*dao.AllGameInfo
		list = append(list, serverdata.Games.GetGameInfo(info.GameID))
		c.JSON(http.StatusOK, struct {
			RecordCount int
			List        interface{}
		}{
			RecordCount: len(list),
			List:        list,
		})
		return
	}

	list := serverdata.Games.GetGames()
	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: len(*list),
		List:        list,
	})
	return
}

// 游戏信息修改
func AllGameUpd(c *gin.Context) {
	obj := dao.NewAllGameUpd()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "allGameUpd", err)
		return
	}

	obj.DoAction(nil)

	serverdata.Games.Refresh()

	c.JSON(http.StatusOK, nil)
	return
}

// 获取系统返还比率
func GetSysOdds(c *gin.Context) {
	obj := dao.NewSysOdds()
	if err := c.ShouldBind(&obj); err != nil {
		log.Debug("%s shouldBind err %v", "getSysOdds", err)
		return
	}
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: obj.Get(),
	})
	return
}

// 设置系统返还比率
func SetSysOdds(c *gin.Context) {
	obj := dao.NewSysOdds()
	if err := c.ShouldBind(&obj); err != nil {
		log.Debug("%s shouldBind err %v", "setSysOdds", err)
		return
	}
	log.Debug("SetSysOdds ==> %+v", obj)
	b := obj.Set()
	c.JSON(http.StatusOK, b)
	return
}

type GetGameInventoryList_in struct {
	GameID int
}

type GameInventoryInfo struct {
	GameName          string  // 游戏名称
	GameID            int     // 游戏ID
	RoomName          string  // 房间名称
	RoomType          int     // 房间类型
	InventoryValue    int     // 实时库存值
	ControlRate       float64 // 实时控制率
	MinInventoryValue int     // 库存取值下限
	MaxInventoryValue int     // 库存取值上限
	MaxControlRate    int     // 控制率上限
}

type GetGameInventoryList_out struct {
	RecordCount int
	List        []GameInventoryInfo
}

// 获取游戏库存列表
func GetGameInventoryList(c *gin.Context) {
	var in GetGameInventoryList_in
	if err := c.ShouldBind(&in); err != nil {
		log.Debug("%s shouldBind err %v", "GetGameInventoryList", err)
		return
	}

	resp := waterPool.GetInventoryList(in.GameID)

	var out GetGameInventoryList_out
	out.RecordCount = resp.Count
	for _, v := range resp.List {
		g := serverdata.Games.GetGameInfo(v.GameID)
		if g == nil {
			continue
		}
		out.List = append(out.List, GameInventoryInfo{
			GameName:          g.GameName,
			GameID:            v.GameID,
			RoomName:          v.RoomName,
			RoomType:          v.RoomType,
			InventoryValue:    v.InventoryValue,
			ControlRate:       v.ControlRate,
			MinInventoryValue: v.MinInventoryValue,
			MaxInventoryValue: v.MaxInventoryValue,
			MaxControlRate:    v.MaxControlRate,
		})
	}

	c.JSON(http.StatusOK, out)
	return
}

// 更新或者修改单个库存
func UpdateGameInventory(c *gin.Context) {
	var in GameInventoryInfo
	if err := c.ShouldBind(&in); err != nil {
		log.Debug("%s shouldBind err %v", "UpdateGameInventory", err)
		return
	}

	// 修改库存配置
	waterPool.UpdateInventoryList(in.GameID,
		in.RoomName,
		in.RoomType,
		in.InventoryValue,
		in.MinInventoryValue,
		in.MaxInventoryValue,
		in.MaxControlRate)

	c.JSON(http.StatusOK, "")
	return
}
