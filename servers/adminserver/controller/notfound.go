package controller

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

//404错误
func NotFoundRoute(c *gin.Context) {
	_ = c.Request.ParseForm()
	log.Debug("%v %v not found route ip[%v]",
		c.Request.URL.Path, c.Request.Form, c.Request.RemoteAddr)

	c.JSON(http.StatusNotFound, struct {
		RetCode  int
		ErrorMsg string
	}{
		RetCode:  -1,
		ErrorMsg: "Not Found Page (404)",
	})
}

func NotFoundMethod(c *gin.Context) {

	_ = c.Request.ParseForm()
	log.Debug("%v %v not found route ip[%v]",
		c.Request.URL.Path, c.Request.Form, c.Request.RemoteAddr)

	c.JSON(http.StatusNotFound, struct {
		RetCode  int
		ErrorMsg string
	}{
		RetCode:  -1,
		ErrorMsg: "Not Found Page (404)",
	})
}
