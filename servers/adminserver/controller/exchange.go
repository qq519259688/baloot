package controller

import (
	"bet24.com/log"
	"bet24.com/servers/adminserver/character"
	"bet24.com/servers/adminserver/dao"
	"bet24.com/servers/adminserver/item"
	"bet24.com/servers/coreservice/client"
	item_inventory "bet24.com/servers/micros/item_inventory/proto"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func SysExchange(c *gin.Context) {
	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: 0,
		List:        client.GetExchangeType(),
	})
	return
}

// 兑换历史
func ExchangeHistory(c *gin.Context) {
	obj := dao.NewExchangeHistory()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ExchangeHistory", err)
		return
	}

	obj.DoAction()

	tools := item.GetSysItems()
	for i := 0; i < len(obj.Out.List); i++ {
		list := &obj.Out.List[i]
		for j := 0; j < len(list.ItemPack); j++ {
			history := &list.ItemPack[j]
			sysItem, ok := tools[history.ItemId]
			if !ok {
				continue
			}
			obj.Out.List[i].Item = fmt.Sprintf("<b>%s</b>（Num：%d）", sysItem.Name, history.Count)
			if sysItem.Type == item_inventory.Item_Physical || sysItem.Type == item_inventory.Item_GiftCard {
				obj.Out.List[i].IsMust = 1
			}
		}
	}

	c.JSON(http.StatusOK, obj.Out)
}

// 兑换历史
func ExchangeCash(c *gin.Context) {
	obj := dao.NewExchangeCash()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ExchangeCash", err)
		return
	}

	obj.DoAction()

	tools := item.GetSysItems()
	for i := 0; i < len(obj.Out.List); i++ {
		list := &obj.Out.List[i]
		for j := 0; j < len(list.ItemPack); j++ {
			history := &list.ItemPack[j]
			item, ok := tools[history.ItemId]
			if !ok {
				continue
			}
			obj.Out.List[i].Item = fmt.Sprintf("<b>%s</b>（Num：%d）", item.Name, history.Count)
			if item.Type == item_inventory.Item_Physical || item.Type == item_inventory.Item_GiftCard {
				obj.Out.List[i].IsMust = 1
			}
		}
	}

	c.JSON(http.StatusOK, obj.Out)
}

// 修改兑换历史
func ExchangeHistoryUpdate(c *gin.Context) {
	obj := dao.NewExchangeHistoryUpdate()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ExchangeHistoryUpdate", err)
		return
	}

	if obj.In.Status == 0 {
		obj.Out.RetCode = 11
		obj.Out.ErrorMsg = "修改失败"
		c.JSON(http.StatusOK, obj.Out)
		return
	}

	obj.In.Items = character.GetSpecialCharacter(obj.In.Items)
	obj.DoAction()

	/*
		// 兑换成功
		if obj.Out.RetCode != 1 {
			c.JSON(http.StatusOK, obj.Out)
			return
		}

		// 拒绝,返还
		if obj.In.Status == 2 {
			itemId := 0
			switch obj.In.ExchangeType {
			case 1: // 奖券
				itemId = item_inventory.Item_GiftCard
			case 5: // 金币提现
				itemId = item_inventory.Item_Gold
			}

			// 返还道具
			if itemId > 0 && obj.In.Price > 0 {
				var tools []item_inventory.ItemPack

				tools = append(tools, item_inventory.ItemPack{
					ItemId: itemId,
					Count:  obj.In.Price,
				})

				client.SendSysMail(obj.In.UserID, mail.SysMail{
					Id:         0,
					Title:      config.Server.Exchange_Title,
					Content:    config.Server.Exchange_Content,
					Status:     0,
					SourceName: "admin exchange",
					Crdate:     common.GetTimeStamp(),
					Tools:      tools,
				})
			}

			c.JSON(http.StatusOK, obj.Out)
			return
		}

		// 查询用户信息
		resp := client.GetUserInfo(obj.In.UserID)
		if resp.Data == "" {
			c.JSON(http.StatusOK, obj.Out)
			return
		}

		// 用户信息
		var UserHotInfo struct {
			UserId       int
			NickName     string
			Vip          int
			FaceUrl      string
			FaceId       int
			Sex          int
			UserWords    string
			IsRobot      int   // 是否机器人
			Achievements []int // 成就列表
		}

		if err := json.Unmarshal([]byte(resp.Data), &UserHotInfo); err != nil {
			c.JSON(http.StatusOK, obj.Out)
			return
		}

		tools := item.GetSysItems()

		var items []item_inventory.ItemPack
		if err := json.Unmarshal([]byte(obj.In.Items), &items); err != nil {
			c.JSON(http.StatusOK, obj.Out)
			return
		}

		for _, v := range items {
			tmpItem, ok := tools[v.ItemId]
			if !ok {
				continue
			}

			broadcastMsg := ""

			// 8=话费
			if tmpItem.Type == 8 {
				// SNG赛 Cash 类处理
				if splitRemarks := strings.Split(obj.Out.Remark, ","); len(splitRemarks) > 0 {
					if rid, err := strconv.Atoi(splitRemarks[0]); err != nil {
						log.Error("ExchangeHistoryUpdate SNG 赛 Cash 处理 obj.In=%+v obj.Out=%+v ，err ==>%v", obj.In, obj.Out, err)
					} else {
						log.Debug("ExchangeHistoryUpdate SNG 赛 Cash 处理调用 rid=%d obj.In=%+v obj.Out=%+v", rid, obj.In, obj.Out)
						go gameHistory.MyMatch_UpdateStatus(obj.In.UserID, rid, gameHistory.MyMatch_Status_Received)
					}

					continue
				}

				broadcastMsg = config.Server.Exchange_Broadcast_Phone
			} else if tmpItem.Type == 11 { // 11=Cash
				broadcastMsg = config.Server.Exchange_Broadcast_Cash
			}

			broadcastMsg = strings.ReplaceAll(broadcastMsg, "数量", fmt.Sprintf("<color=#04ff0a>%d</color>", v.Count))
			broadcastMsg = strings.ReplaceAll(broadcastMsg, "玩家昵称", fmt.Sprintf("<color=#ffafd8>%s</color>", UserHotInfo.NickName))

			go client.SendBroadcast(obj.In.UserID, 0, 0, broadcastMsg, "")
		}
	*/

	c.JSON(http.StatusOK, obj.Out)
	return
}
