package controller

import (
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	notification "bet24.com/servers/micros/notification/proto"
	"github.com/gin-gonic/gin"
)

// 金币日志
func GetCashLog(c *gin.Context) {
	obj := dao.NewGetCashLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getCashLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 钻石日志
func GetDiamondLog(c *gin.Context) {
	obj := dao.NewGetDiamondLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getDiamondLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 投注日志
func GetBetLog(c *gin.Context) {
	obj := dao.NewGetBetLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getBetLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 投注详情
func GetBetDetail(c *gin.Context) {
	obj := dao.NewGetBetDetail()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getBetDetail", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 加金
func CashSend(c *gin.Context) {
	obj := dao.NewCashSend()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "cashSend", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 扣金
func CashDel(c *gin.Context) {
	obj := dao.NewCashDel()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "cashDel", err)
		return
	}

	obj.DoAction(nil)

	// 通知客户端
	go notification.AddNotification(obj.In.UserID, notification.Notification_Gold, "")

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 金币场解锁
func CashUnlock(c *gin.Context) {
	obj := dao.NewCashUnlock()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "cashUnlock", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 金币锁信息
func CashLockInfo(c *gin.Context) {
	obj := dao.NewCashLockInfo()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "cashLockInfo", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 货币统计
func MoneyStat(c *gin.Context) {
	obj := dao.NewMoneyStat()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "moneyStat", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 后台扣减钻石
func DiamondWebReduce(c *gin.Context) {
	obj := dao.NewDiamondWebReduce()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "diamondWebReduce", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 后台钻石日志
func GetDiamondAdminLog(c *gin.Context) {
	obj := dao.NewGetDiamondAdminLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getDiamondAdminLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 游戏金币流量统计
func MoneyFlowStat(c *gin.Context) {
	obj := dao.NewMoneyFlowStat()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "MoneyFlowStat", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 赠送日志
func CashTransferLog(c *gin.Context) {
	obj := dao.NewTransferLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChipTransferLog", err)
		return
	}

	obj.GetCashTransferLog()
	c.JSON(http.StatusOK, obj.Out)
	return
}
