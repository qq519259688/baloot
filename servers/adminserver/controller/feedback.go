package controller

import (
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

// 客服反馈列表
func FeedbackList(c *gin.Context) {
	obj := dao.NewFeedbackList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "FeedbackList", err)
		return
	}
	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
}

// 客服反馈修改
func FeedbackUpdate(c *gin.Context) {
	obj := dao.NewFeedbackUp()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "FeedbackUpdate", err)
		return
	}
	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
}
