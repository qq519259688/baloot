package controller

import (
	waterPool "bet24.com/servers/micros/waterpool/proto"
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

// 后台登陆
func Login(c *gin.Context) {
	obj := dao.NewUserLogin()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "login", err)
		return
	}

	obj.DoAction(nil)
	obj.Out.ErrorMsg = ""
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 获取页面列表
func PageList(c *gin.Context) {
	obj := dao.NewPageList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "pageList", err)
		return
	}

	obj.DoAction(nil)
	obj.Out.ErrorMsg = ""
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 获取管理用户信息
func GetInfo(c *gin.Context) {
	obj := dao.NewGetInfo()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getInfo", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 修改密码
func UpdatePassword(c *gin.Context) {
	obj := dao.NewUpdatePassword()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "updatePassword", err)
		return
	}

	//判断2次密码是否一致
	if obj.In.NewPassword != obj.In.NewPassword2 {
		obj.Out.RetCode = 11
		obj.Out.ErrorMsg = ""
		c.JSON(http.StatusOK, obj.Out)
		return
	}

	obj.DoAction(nil)
	obj.Out.ErrorMsg = ""
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 校验页面
func VerifyPage(c *gin.Context) {
	obj := dao.NewVerifyPage()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "verifyPage", err)
		return
	}

	obj.DoAction(nil)
	obj.Out.ErrorMsg = ""
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 管理用户列表
func UserList(c *gin.Context) {
	obj := dao.NewUserList()
	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 删除后台管理用户
func UserDel(c *gin.Context) {
	obj := dao.NewUserDel()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "userDel", err)
		return
	}
	obj.DoAction(nil)
	obj.Out.ErrorMsg = ""
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 添加备注
func UpdateMemo(c *gin.Context) {
	obj := dao.NewUpdateMemo()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "updateMemo", err)
		return
	}
	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 添加管理用户
func UserAdd(c *gin.Context) {
	obj := dao.NewUserAdd()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "userAdd", err)
		return
	}
	obj.DoAction(nil)
	obj.Out.ErrorMsg = ""
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 获取后台登录密码
func GetPassword(c *gin.Context) {
	obj := dao.NewGetPassword()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getPassword", err)
		return
	}
	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 获取个人奖池
func GetUserPrizePool(c *gin.Context) {
	var req struct {
		UserId int
	}
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "getUserPrizePool", err)
		return
	}

	value := waterPool.GetUserWaterPool(req.UserId)
	c.JSON(http.StatusOK, value)
	return
}

// 获取奖池日志列表
func GetPrizePoolList(c *gin.Context) {
	obj := dao.NewGetPrizePoolList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getPrizePoolList", err)
		return
	}

	obj.Out.Data = waterPool.GetUserWaterPoolGrantRecords(obj.In.UserId, obj.In.BeginTime,
		obj.In.EndTime, obj.In.PageIndex, obj.In.PageSize)
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 发放个人奖池
func SendPrizePool(c *gin.Context) {
	obj := dao.NewSendPrizePool()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "sendPrizePool", err)
		return
	}

	waterPool.GrantUserNewWaterPool(obj.In.UserId, obj.In.Value, obj.In.GenType)
	c.JSON(http.StatusOK, "ok")
	return
}
