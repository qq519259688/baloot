package controller

import (
	"net/http"
	"strconv"

	userlabel "bet24.com/servers/micros/userlabel/proto"

	"bet24.com/servers/adminserver/config"
	"bet24.com/servers/adminserver/google"

	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"github.com/gin-gonic/gin"
)

// 查询用户列表
func AllUserList(c *gin.Context) {
	obj := dao.NewAllUserList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "allUserList", err)
		return
	}

	var (
		userID   int
		nickName string
	)

	nickName = obj.In.Data
	if value, err := strconv.Atoi(obj.In.Data); err == nil {
		userID = value
	}

	obj.In.UserID = userID
	obj.In.NickName = nickName
	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 查询用户详情信息
func UserDetail(c *gin.Context) {
	obj := dao.NewUserDetail()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "userDetail", err)
		return
	}
	obj.DoAction(nil)

	// 用户标签
	obj.Out.Labels = userlabel.GetLabel(obj.In.UserID, 0)

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 查询被封用户列表
func ForbidUserList(c *gin.Context) {
	obj := dao.NewForbidUserList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "forbidUserList", err)
		return
	}

	obj.DoAction(nil)

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 封杀玩家
func ForbidUserAdd(c *gin.Context) {
	obj := dao.NewForbidUserAdd()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "forbidUserAdd", err)
		return
	}

	obj.DoAction(nil)
	obj.Out.ErrorMsg = ""
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 解除封杀
func ForbidUserDel(c *gin.Context) {
	obj := dao.NewForbidUserDel()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "forbidUserDel", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 玩家简单信息
func UserSimpleInfo(c *gin.Context) {
	obj := dao.NewUserSimpleInfo()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "userSimpleInfo", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 实时在线
func CasinoOnlineList(c *gin.Context) {
	obj := dao.NewCasinoOnlineList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "casinoOnlineList", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 获取用户ID(根据昵称)
func GetUserID(c *gin.Context) {
	var info struct {
		Data string
	}
	if err := c.ShouldBind(&info); err != nil {
		log.Debug("%s shouldBind err %v", "getUserID", err)
		return
	}

	var (
		userID   int
		nickName string
	)

	if value, err := strconv.Atoi(info.Data); err != nil {
		nickName = info.Data
	} else {
		userID = value
	}

	//log.Debug("userID:%d nickName:%s tel:%s", userID, nickName, tel)

	obj := dao.NewGetUserID()
	obj.In.UserID = userID
	obj.In.NickName = nickName
	obj.DoAction(nil)

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 删除账号（后台）
func DelUser(c *gin.Context) {
	obj := dao.NewDelUser()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "delUser", err)
		return
	}
	obj.DoAction(nil)
	obj.Out.ErrorMsg = ""
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 绑定facebook
func BindFacebook(c *gin.Context) {
	obj := dao.NewBindFacebook()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "BindFacebook", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 个性签名列表
func UserWordsList(c *gin.Context) {
	obj := dao.NewUserWordsList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "UserWordsList", err)
		return
	}

	obj.DoAction()

	// 是否开启翻译
	if ok, _ := google.IsTranslate(); ok {
		for idx, v := range obj.Out.List {
			if v.UserWords == "" || v.TransMsg != "" {
				continue
			}

			transMsg, err := google.Translate(config.Server.GOOGLE_OTHER_LANGUAGE, config.Server.GOOGLE_ZH_LANGUAGE, v.UserWords)
			if err != nil {
				log.Error("controller.UserWordsList google.Translate error %+v --> %s", err, v.UserWords)
				continue
			}

			obj.Out.List[idx].TransMsg = transMsg

			transObj := dao.NewUserWordsTranslate()
			transObj.In.ApplyID = v.ApplyID
			transObj.In.TransMsg = transMsg
			go transObj.DoAction()
		}
	}

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 个性签名审批
func UserWordsSet(c *gin.Context) {
	obj := dao.NewUserWordsSet()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "UserWordsSet", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 银行信息
func BankInfo(c *gin.Context) {
	obj := dao.NewBankInfo()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "BankInfo", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 用户标签列表
func GetUserTagList(c *gin.Context) {
	var (
		req struct {
			PageIndex  int
			PageSize   int
			ColorValue int
		}
		resp struct {
			RecordCount int
			List        interface{}
		}
	)
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "GetUserTagList", err)
		return
	}

	resp.RecordCount, resp.List = dao.GetUserTagsList(req.ColorValue, req.PageIndex, req.PageSize)

	c.JSON(http.StatusOK, resp)
	return
}

// 修改昵称
func ChangeNickName(c *gin.Context) {
	obj := dao.NewChangeNickName()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "ChangeNickName", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}
