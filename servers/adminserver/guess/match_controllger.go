package guess

import (
	"bet24.com/log"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 获取赛事列表
func GetGuessMatchList(c *gin.Context) {
	obj := NewGetGuessMatchList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GetGuessMatchList", err)
		return
	}
	obj.Out.List, obj.Out.RecordCount = mgr.getGuessMatchList(obj.In.MatchType)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 获取赛事信息（添加成功后，获取赛事写入内存 或 查数据）
func GetGuessMatchInfo(c *gin.Context) {
	obj := NewGetGuessMatchInfo()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GetGuessMatchInfo", err)
		return
	}
	obj.Out.Data, obj.Out.Code = mgr.getGuessMatchInfo(obj.In.SerialNumber)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 刷新赛事信息（添加成功后，获取赛事写入内存）
func RefreshGuessMatchInfo(c *gin.Context) {
	obj := NewRefreshGuessMatchInfo()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "RefreshGuessMatchInfo", err)
		return
	}
	mgr.guessGetMatchInfo(obj.In.SerialNumber)
	c.JSON(http.StatusOK, "")
	return
}

// 添加赛事
func AddGuessMatch(c *gin.Context) {
	obj := NewAddGuessMatch()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "AddGuessMatch", err)
		return
	}
	obj.Out.Data = mgr.addGuessMatch(obj.In)
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 编辑赛事
func EditGuessMatch(c *gin.Context) {
	obj := NewEditGuessMatch()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "EditGuessMatch", err)
		return
	}
	obj.Out.RetCode = mgr.editGuessMatch(obj.In)
	c.JSON(http.StatusOK, obj.Out.RetCode)
	return
}

// 设置赛事状态
func SetGuessMatchOpen(c *gin.Context) {
	obj := NewSetGuessMatchOpen()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "SetGuessMatchOpen", err)
		return
	}
	obj.Out.RetCode = mgr.setGuessMatchOpen(obj.In)
	c.JSON(http.StatusOK, obj.Out.RetCode)
	return
}

// 派奖
func GuessMatchAward(c *gin.Context) {
	obj := NewGuessMatchAward()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GuessMatchAward", err)
		return
	}
	obj.Out.Data = mgr.guessMatchAward(obj.In)
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 设置结果
func GuessSetResult(c *gin.Context) {
	obj := NewGuessSetResult()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GuessSetResult", err)
		return
	}
	obj.Out.RetCode = mgr.guessSetResult(obj.In)
	c.JSON(http.StatusOK, obj.Out.RetCode)
	return
}

// 添加赛事球队
func AddGuessMatchTeam(c *gin.Context) {
	obj := NewAddGuessMatchTeam()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "AddGuessMatchTeam", err)
		return
	}
	obj.Out.RetCode = mgr.addGuessMatchTeam(obj.In)
	c.JSON(http.StatusOK, obj.Out.RetCode)
	return
}

// 修改赛事球队
func EditGuessMatchTeam(c *gin.Context) {
	obj := NewEditGuessMatchTeam()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "EditGuessMatchTeam", err)
		return
	}
	obj.Out.RetCode = mgr.editGuessMatchTeam(obj.In)
	c.JSON(http.StatusOK, obj.Out.RetCode)
	return
}

// 添加赛事投注
func AddGuessMatchBet(c *gin.Context) {
	obj := NewAddGuessMatchBet()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "AddGuessMatchBet", err)
		return
	}
	obj.Out.RetCode = mgr.addGuessMatchBet(obj.In)
	c.JSON(http.StatusOK, obj.Out.RetCode)
	return
}

// 修改赛事投注选项
func EditGuessMatchBet(c *gin.Context) {
	obj := NewEditGuessMatchBet()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "EditGuessMatchBet", err)
		return
	}
	obj.Out.RetCode = mgr.editGuessMatchBet(obj.In)
	c.JSON(http.StatusOK, obj.Out.RetCode)
	return
}
