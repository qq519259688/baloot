package guess

import (
	"bet24.com/log"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 获取球队列表
func GetGuessTeamList(c *gin.Context) {
	obj := NewGetGuessTeamList()
	obj.Out.List = mgr.getGuessTeamList()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 添加球队
func AddGuessTeam(c *gin.Context) {
	obj := NewAddGuessTeam()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "AddGuessTeam", err)
		return
	}
	obj.Out.Data = mgr.addGuessTeam(obj.In)
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 获取球队信息
func GetGuessTeamInfo(c *gin.Context) {
	obj := NewGetGuessTeamInfo()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GetGuessTeamInfo", err)
		return
	}
	obj.Out.Data, obj.Out.RetCode = mgr.getGuessTeamInfo(obj.In.Id)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 编辑球队
func EditGuessTeam(c *gin.Context) {
	obj := NewEditGuessTeam()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "EditGuessTeam", err)
		return
	}
	obj.Out.Data = mgr.editGuessTeam(obj.In)
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}
