package guess

import (
	"bet24.com/log"
	"bet24.com/servers/adminserver/character"
	guess "bet24.com/servers/micros/guess/proto"
	"encoding/json"
	"fmt"
)

// 获取球队列表
func (m *Manager) getGuessTeamList() []guess.Team {
	var list []guess.Team
	resp := guess.OnGuessMsg(0, "guessGetTeamList", "")
	err := json.Unmarshal([]byte(resp), &list)
	if err != nil {
		log.Debug("guess.guessGetTeamList json.Unmarshal error. err[%+v]", err)
		return nil
	}
	return list
}

// 添加球队
func (m *Manager) addGuessTeam(in addTeam_in) string {
	in.Icon = character.GetSpecialCharacter(in.Icon)
	params, err := json.Marshal(in)
	if err != nil {
		log.Debug("manager.addGuessTeam json.Marshal error. err[%+v]", err)
		return ""
	}
	return guess.OnGuessMsg(0, "guessAddTeam", string(params))
}

// 获取球队信息
func (m *Manager) getGuessTeamInfo(id int) (guess.Team, int) {
	var resp guess.Team
	data := guess.OnGuessMsg(0, "guessGetTeam", fmt.Sprintf(`{"Id": %d}`, id))
	err := json.Unmarshal([]byte(data), &resp)
	if err != nil {
		log.Debug("Manager.guessGetTeam json.Unmarshal error. err[%+v]", err)
		return resp, GuessTeam_Fail
	}
	if resp.Id == 0 {
		return resp, GuessTeam_NotExist
	}
	return resp, GuessTeam_Success
}

// 主客场拆分
func (m *Manager) homeAwaySplit(teams []guess.Team) guessTeam {
	var team guessTeam
	for k, v := range teams {
		if k == 0 {
			team.HomeRid = v.Rid
			team.HomeTeamId = v.Id
			team.HomeTeamName = v.Name
			team.HomeTeamIcon = v.Icon
			team.HomeTeamShortName = v.ShortName
			continue
		}
		team.AwayRid = v.Rid
		team.AwayTeamId = v.Id
		team.AwayTeamName = v.Name
		team.AwayTeamIcon = v.Icon
		team.AwayTeamShortName = v.ShortName
		break
	}
	return team
}

// 编辑球队
func (m *Manager) editGuessTeam(in editTeam_in) string {
	in.Icon = character.GetSpecialCharacter(in.Icon)
	params, err := json.Marshal(in)
	if err != nil {
		log.Debug("manager.editGuessTeam json.Marshal error. err[%+v]", err)
		return ""
	}
	return guess.OnGuessMsg(0, "guessUpdateTeam", string(params))
}
