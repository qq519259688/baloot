package guess

import (
	"bet24.com/database"
	guess "bet24.com/servers/micros/guess/proto"
)

const (
	GuessMatch_Fail = iota
	GuessMatch_Success
)

// 赛事队伍
type MatchTeams struct {
	Rid    int // 记录id
	TeamId int // 队伍id
}

// 赛事投注
type MatchBet struct {
	HomeBetId     int     // 主队 投注id
	HomeBetName   string  // 主队 投注名称
	HomeBetOdds   float64 // 主队 投注赔率
	HomeBetAmount int     // 主队 投注金额
	HomeBetIsWin  bool    // 主队 是否胜
	DrawBetId     int     // 平 投注id
	DrawBetName   string  // 平 投注名称
	DrawBetOdds   float64 // 平 投注赔率
	DrawBetAmount int     // 平 投注金额
	DrawBetIsWin  bool    // 平 是否胜
	AwayBetId     int     // 客队 投注id
	AwayBetName   string  // 客队 投注名称
	AwayBetOdds   float64 // 客队 投注赔率
	AwayBetAmount int     // 客队 投注金额
	AwayBetIsWin  bool    // 客队 是否胜
}

// 获取球队列表
type (
	getMatchList_in struct {
		SearchKey string // 搜索的内容
		MatchType int    // 赛事类型 (0=所有赛事  1=预热的赛事(不可投注)  2=正在进行的赛事(可以投注)  3=结束的赛事(已出结果))
		BeginTime string // 开始时间
		EndTime   string // 结束时间
		PageIndex int    // 第几页
		PageSize  int    // 请求的个数
	}

	guessMatchInfo struct {
		SerialNumber string // 流水号
		Title        string // 标题(如：让球)
		Status       int    // 状态（0=无效状态 1=开启状态  2=结束状态）
		guessTeam
		Result      string // 赛事结果
		StartAt     string `json:",omitempty"` // 开始时间戳
		EndAt       string `json:",omitempty"` // 结束时间戳
		ShowStartAt string `json:",omitempty"` // 展示开始时间戳
		ShowEndAt   string `json:",omitempty"` // 展示结束时间戳
	}

	getMatchList_out struct {
		RecordCount int `json:",omitempty"`
		List        []guessMatchInfo
	}

	getMatchList struct {
		database.Trans_base
		In  getMatchList_in
		Out getMatchList_out
	}
)

func NewGetGuessMatchList() *getMatchList {
	return &getMatchList{}
}

// 获取赛事信息（添加成功后，获取赛事写入内存 或 查数据）
type (
	getMatchInfo_in struct {
		SerialNumber string // 赛事流水号
	}

	guessMatch struct {
		guessMatchInfo
		MatchBet
	}

	getMatchInfo_out struct {
		Code int `json:",omitempty"`
		Data guessMatch
	}

	getMatchInfo struct {
		database.Trans_base
		In  getMatchInfo_in
		Out getMatchInfo_out
	}
)

func NewGetGuessMatchInfo() *getMatchInfo {
	return &getMatchInfo{}
}

// 刷新赛事信息（添加成功后，获取赛事写入内存）
type (
	refreshMatchInfo_in struct {
		SerialNumber string // 赛事流水号
	}

	refreshMatchInfo_out struct {
	}

	refreshMatchInfo struct {
		database.Trans_base
		In  refreshMatchInfo_in
		Out refreshMatchInfo_out
	}
)

func NewRefreshGuessMatchInfo() *refreshMatchInfo {
	return &refreshMatchInfo{}
}

// 添加赛事
type (
	addMatch_in struct {
		guess.OpUser
		Title       string // 标题（让、受球）
		StartAt     string // 开始时间
		EndAt       string // 结束时间
		ShowStartAt string // 展示开始时间
		ShowEndAt   string // 展示结束时间
	}

	addMatch_out struct {
		Data string
	}

	addMatch struct {
		database.Trans_base
		In  addMatch_in
		Out addMatch_out
	}
)

func NewAddGuessMatch() *addMatch {
	return &addMatch{}
}

// 编辑赛事
type (
	editMatch_in struct {
		guess.OpUser
		SerialNumber string // 流水号
		Title        string // 标题（让、受球）
		StartAt      string // 开始时间
		EndAt        string // 结束时间
		ShowStartAt  string // 展示开始时间
		ShowEndAt    string // 展示结束时间
	}

	editMatch_out struct {
		RetCode int
	}

	editMatch struct {
		database.Trans_base
		In  editMatch_in
		Out editMatch_out
	}
)

func NewEditGuessMatch() *editMatch {
	return &editMatch{}
}

// 设置赛事状态
type (
	setMatchOpen_in struct {
		guess.OpUser
		SerialNumber string // 流水号
		Status       int    // 状态（0：无效，1：开启，2：关闭）
	}

	setMatchOpen_out struct {
		RetCode int `json:",omitempty"`
	}

	setMatchOpen struct {
		database.Trans_base
		In  setMatchOpen_in
		Out setMatchOpen_out
	}
)

func NewSetGuessMatchOpen() *setMatchOpen {
	return &setMatchOpen{}
}

// 派奖
type (
	matchAward_in struct {
		guess.OpUser
		SerialNumber string // 流水号
	}

	matchAward_out struct {
		Data string
	}

	matchAward struct {
		database.Trans_base
		In  matchAward_in
		Out matchAward_out
	}
)

func NewGuessMatchAward() *matchAward {
	return &matchAward{}
}

// 设置结果
type (
	setResult_in struct {
		guess.OpUser
		SerialNumber string // 流水号
		BetId        int    // 投注id
		Result       string // 结果输入
	}

	setResult_out struct {
		RetCode int
	}

	setResult struct {
		database.Trans_base
		In  setResult_in
		Out setResult_out
	}
)

func NewGuessSetResult() *setResult {
	return &setResult{}
}

// 添加赛事球队
type (
	addMatchTeam_in struct {
		guess.OpUser
		SerialNumber string // 流水号
		TeamId       int    // 球队ID
	}

	addMatchTeam_out struct {
		RetCode int `json:",omitempty"`
	}

	addMatchTeam struct {
		database.Trans_base
		In  addMatchTeam_in
		Out addMatchTeam_out
	}
)

func NewAddGuessMatchTeam() *addMatchTeam {
	return &addMatchTeam{}
}

// 修改赛事球队
type (
	editMatchTeam_in struct {
		guess.OpUser
		Rid          int    // 记录id
		SerialNumber string // 流水号
		TeamID       int    // 球队id
	}

	editMatchTeam_out struct {
		RetCode int `json:",omitempty"`
	}

	editMatchTeam struct {
		database.Trans_base
		In  editMatchTeam_in
		Out editMatchTeam_out
	}
)

func NewEditGuessMatchTeam() *editMatchTeam {
	return &editMatchTeam{}
}

// 添加赛事投注
type (
	addMatchBet_in struct {
		guess.OpUser
		SerialNumber string  // 流水号w
		BetName      string  // 投注名称
		BetOdds      float64 // 投注赔率
	}

	addMatchBet_out struct {
		RetCode int `json:",omitempty"`
	}

	addMatchBet struct {
		database.Trans_base
		In  addMatchBet_in
		Out addMatchBet_out
	}
)

func NewAddGuessMatchBet() *addMatchBet {
	return &addMatchBet{}
}

// 修改赛事投注选项
type (
	editMatchBet_in struct {
		guess.OpUser
		BetId        int     // 投注id
		SerialNumber string  // 流水号
		BetName      string  // 投注标题
		BetOdds      float64 // 投注赔率
	}

	editMatchBet_out struct {
		RetCode int `json:",omitempty"`
	}

	editMatchBet struct {
		database.Trans_base
		In  editMatchBet_in
		Out editMatchBet_out
	}
)

func NewEditGuessMatchBet() *editMatchBet {
	return &editMatchBet{}
}
