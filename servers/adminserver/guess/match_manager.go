package guess

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	guess "bet24.com/servers/micros/guess/proto"
	"encoding/json"
	"fmt"
	"sort"
)

// 获取赛事列表
func (m *Manager) getGuessMatchList(matchType int) ([]guessMatchInfo, int) {
	var list []guess.Match
	params := fmt.Sprintf(`{"Type":%d}`, matchType)
	match := guess.OnGuessMsg(0, "guessGetMatchList", params)
	err := json.Unmarshal([]byte(match), &list)
	if err != nil {
		log.Debug("guess.guessGetMatchList json.Unmarshal error. err[%+v]", err)
		return nil, GuessMatch_Fail
	}
	var resp []guessMatchInfo
	for _, v := range list {
		rs := guessMatchInfo{
			SerialNumber: v.SerialNumber,
			Title:        v.Title,
			Status:       v.Status,
			guessTeam:    m.homeAwaySplit(v.Teams),
			Result:       v.Result,
			StartAt:      common.TimeStampToString(int64(v.StartAt)),
			EndAt:        common.TimeStampToString(int64(v.EndAt)),
			ShowStartAt:  common.TimeStampToString(int64(v.ShowStartAt)),
			ShowEndAt:    common.TimeStampToString(int64(v.ShowEndAt)),
		}
		resp = append(resp, rs)
	}
	sort.SliceStable(resp, func(i, j int) bool {
		if resp[i].StartAt == resp[j].StartAt {
			return resp[i].EndAt < resp[j].EndAt
		}
		return resp[i].StartAt > resp[j].StartAt
	})
	return resp, GuessMatch_Success
}

// 获取竞猜赛事
func (m *Manager) guessGetMatchInfo(serialNumber string) string {
	params := fmt.Sprintf(`{"SerialNumber": "%s"}`, serialNumber)
	return guess.OnGuessMsg(0, "guessGetMatchInfo", params)
}

// 获取赛事信息（添加成功后，获取赛事写入内存 或 查数据）
func (m *Manager) getGuessMatchInfo(serialNumber string) (guessMatch, int) {
	var data guess.Match
	var resp guessMatch
	info := m.guessGetMatchInfo(serialNumber)
	err := json.Unmarshal([]byte(info), &data)
	if err != nil {
		log.Debug("guess.guessGetMatchInfo json.Unmarshal error. err[%+v]", err)
		return resp, GuessMatch_Fail
	}
	resp.SerialNumber = data.SerialNumber
	resp.Title = data.Title
	resp.Status = data.Status
	resp.guessTeam = m.homeAwaySplit(data.Teams)
	resp.Result = data.Result
	resp.StartAt = common.TimeStampToString(int64(data.StartAt))
	resp.EndAt = common.TimeStampToString(int64(data.EndAt))
	resp.ShowStartAt = common.TimeStampToString(int64(data.ShowStartAt))
	resp.ShowEndAt = common.TimeStampToString(int64(data.ShowEndAt))
	resp.MatchBet = m.matchBet(data.Bets)
	return resp, GuessMatch_Success
}

// 赛事投注
func (m *Manager) matchBet(bets []guess.Bet) MatchBet {
	var bet MatchBet
	for k, v := range bets {
		// 主队
		if k == 0 {
			bet.HomeBetId = v.Id
			bet.HomeBetName = v.Name
			bet.HomeBetOdds = v.Odds
			bet.HomeBetAmount = v.Amount
			bet.HomeBetIsWin = v.IsWin
		}
		// 平
		if k == 1 {
			bet.DrawBetId = v.Id
			bet.DrawBetName = v.Name
			bet.DrawBetOdds = v.Odds
			bet.DrawBetAmount = v.Amount
			bet.DrawBetIsWin = v.IsWin
		}
		// 客队
		if k == 2 {
			bet.AwayBetId = v.Id
			bet.AwayBetName = v.Name
			bet.AwayBetOdds = v.Odds
			bet.AwayBetAmount = v.Amount
			bet.AwayBetIsWin = v.IsWin
		}
	}
	return bet
}

// 添加赛事
func (m *Manager) addGuessMatch(in addMatch_in) string {
	params, err := json.Marshal(in)
	if err != nil {
		log.Debug("manager.addGuessMatch json.Marshal error. err[%+v]", err)
		return ""
	}
	msg := guess.RetMsg{}
	data := guess.OnGuessMsg(0, "guessAddMatch", string(params))
	err = json.Unmarshal([]byte(data), &msg)
	if err != nil {
		log.Debug("guess.guessAddMatch json.Unmarshal error. err[%+v]", err)
		return ""
	}
	return msg.Message
}

// 编辑赛事
func (m *Manager) editGuessMatch(in editMatch_in) int {
	params, err := json.Marshal(in)
	if err != nil {
		log.Debug("manager.editGuessMatch json.Marshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	msg := guess.RetMsg{}
	data := guess.OnGuessMsg(0, "guessUpdateMatch", string(params))
	err = json.Unmarshal([]byte(data), &msg)
	if err != nil {
		log.Debug("guess.guessUpdateMatch json.Unmarshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	return msg.RetCode
}

// 设置赛事状态
func (m *Manager) setGuessMatchOpen(in setMatchOpen_in) int {
	params, err := json.Marshal(in)
	if err != nil {
		log.Debug("manager.setGuessMatchOpen json.Marshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	msg := guess.RetMsg{}
	data := guess.OnGuessMsg(0, "guessSetMatchOpen", string(params))
	err = json.Unmarshal([]byte(data), &msg)
	if err != nil {
		log.Debug("guess.guessSetMatchOpen json.Unmarshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	return msg.RetCode
}

// 派奖
func (m *Manager) guessMatchAward(in matchAward_in) string {
	params, err := json.Marshal(in)
	if err != nil {
		log.Debug("manager.guessMatchAward json.Marshal error. err[%+v]", err)
		return ""
	}
	msg := guess.RetMsg{}
	data := guess.OnGuessMsg(0, "guessAward", string(params))
	err = json.Unmarshal([]byte(data), &msg)
	if err != nil {
		log.Debug("guess.guessAward json.Unmarshal error. err[%+v]", err)
		return ""
	}
	return msg.Message
}

// 设置结果
func (m *Manager) guessSetResult(in setResult_in) int {
	params, err := json.Marshal(in)
	if err != nil {
		log.Debug("manager.guessSetResult json.Marshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	msg := guess.RetMsg{}
	data := guess.OnGuessMsg(0, "guessSetResult", string(params))
	err = json.Unmarshal([]byte(data), &msg)
	if err != nil {
		log.Debug("guess.guessSetResult json.Unmarshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	return msg.RetCode
}

// 添加赛事球队
func (m *Manager) addGuessMatchTeam(in addMatchTeam_in) int {
	params, err := json.Marshal(in)
	if err != nil {
		log.Debug("manager.addGuessMatchTeam json.Marshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	msg := guess.RetMsg{}
	data := guess.OnGuessMsg(0, "guessAddMatchTeam", string(params))
	err = json.Unmarshal([]byte(data), &msg)
	if err != nil {
		log.Debug("guess.guessAddMatchTeam json.Unmarshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	return msg.RetCode
}

// 修改赛事球队
func (m *Manager) editGuessMatchTeam(in editMatchTeam_in) int {
	params, err := json.Marshal(in)
	if err != nil {
		log.Debug("manager.editGuessMatchTeam json.Marshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	msg := guess.RetMsg{}
	data := guess.OnGuessMsg(0, "guessUpdateMatchTeam", string(params))
	err = json.Unmarshal([]byte(data), &msg)
	if err != nil {
		log.Debug("guess.guessUpdateMatchTeam json.Unmarshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	return msg.RetCode
}

// 添加赛事投注
func (m *Manager) addGuessMatchBet(in addMatchBet_in) int {
	params, err := json.Marshal(in)
	if err != nil {
		log.Debug("manager.addGuessMatchBet json.Marshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	msg := guess.RetMsg{}
	data := guess.OnGuessMsg(0, "guessAddMatchBet", string(params))
	err = json.Unmarshal([]byte(data), &msg)
	if err != nil {
		log.Debug("guess.guessAddMatchBet json.Unmarshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	return msg.RetCode
}

// 修改赛事投注选项
func (m *Manager) editGuessMatchBet(in editMatchBet_in) int {
	params, err := json.Marshal(in)
	if err != nil {
		log.Debug("manager.editGuessMatchBet json.Marshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	msg := guess.RetMsg{}
	data := guess.OnGuessMsg(0, "guessUpdateMatchBet", string(params))
	err = json.Unmarshal([]byte(data), &msg)
	if err != nil {
		log.Debug("guess.guessUpdateMatchBet json.Unmarshal error. err[%+v]", err)
		return GuessMatch_Fail
	}
	return msg.RetCode
}
