package guess

import (
	"bet24.com/database"
	guess "bet24.com/servers/micros/guess/proto"
)

const (
	GuessTeam_Fail = iota
	GuessTeam_Success
	GuessTeam_NotExist
)

// 竞猜球队
type guessTeam struct {
	HomeRid           int    // 主队记录ID
	HomeTeamId        int    `json:",omitempty"` // 主队ID
	HomeTeamName      string // 主队名称
	HomeTeamShortName string // 主队别名
	HomeTeamIcon      string `json:",omitempty"` // 主队队徽
	AwayRid           int    // 客队记录ID
	AwayTeamId        int    `json:",omitempty"` // 客队ID
	AwayTeamName      string // 客队名称
	AwayTeamShortName string // 客队别名
	AwayTeamIcon      string `json:",omitempty"` // 客队队徽
}

// 获取球队列表
type (
	getTeamList_in struct {
	}

	getTeamList_out struct {
		RecordCount int // 总记录数
		List        []guess.Team
	}

	getTeamList struct {
		database.Trans_base
		In  getTeamList_in
		Out getTeamList_out
	}
)

func NewGetGuessTeamList() *getTeamList {
	return &getTeamList{}
}

// 添加球队
type (
	addTeam_in struct {
		guess.OpUser
		Name      string
		Icon      string
		ShortName string
	}

	addTeam_out struct {
		Data string
	}

	addTeam struct {
		database.Trans_base
		In  addTeam_in
		Out addTeam_out
	}
)

func NewAddGuessTeam() *addTeam {
	return &addTeam{}
}

// 获取球队信息
type (
	getTeamInfo_in struct {
		Id int
	}

	getTeamInfo_out struct {
		RetCode int // 总记录数
		Data    guess.Team
	}

	getTeamInfo struct {
		database.Trans_base
		In  getTeamInfo_in
		Out getTeamInfo_out
	}
)

func NewGetGuessTeamInfo() *getTeamInfo {
	return &getTeamInfo{}
}

// 编辑球队
type (
	editTeam_in struct {
		guess.Team
	}

	editTeam_out struct {
		Data string
	}

	editTeam struct {
		database.Trans_base
		In  editTeam_in
		Out editTeam_out
	}
)

func NewEditGuessTeam() *editTeam {
	return &editTeam{}
}
