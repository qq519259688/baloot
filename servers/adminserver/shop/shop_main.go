package shop

import "fmt"

func GetProductName(productId string) string {
	return getShopManager().getProductName(productId)
}

func GetProductFormatStr(productId string) string {
	item := getShopManager().getProduct(productId)
	if item == nil {
		return ""
	}
	return fmt.Sprintf("%s&nbsp;<font color='gray'>(id:%s)</font>", item.ProductDesc, productId)
}
