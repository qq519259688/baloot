package shop

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	item "bet24.com/servers/micros/item_inventory/proto"
	"encoding/json"
	"time"
)

var shopmgr *shopManager

type shopManager struct {
	shop_list []*Shop_Item
}

type Shop_Item struct {
	ProductId   string          //产品ID
	ProductName string          //产品名称
	Amount      int             //数量
	Price       float64         //价格(分或钻石)
	PayType     int             //支付类型(1=金币  0=钻石)
	ShopType    int             //产品类型(1=金币  2=钻石  3=炮台  4=礼包)
	IsHot       int             //是否热销 1=热销
	Bonus       int             // 额外赠送(多送百分比)
	Extra       []item.ItemPack // 扩展信息
	ProductDesc string          // 产品描述
}

func getShopManager() *shopManager {
	if shopmgr == nil {
		shopmgr = new(shopManager)
		shopmgr.refreshData()
	}
	return shopmgr
}

func (this *shopManager) refreshData() {
	this.load()
	time.AfterFunc(1*time.Minute, this.refreshData)
}

func (this *shopManager) load() {

	//// 获取汇率
	//info := GetExchangeRate("")
	//if info == nil {
	//	log.Error("%s 获取汇率失败 %+v", "getShopList", info)
	//	return items
	//}

	resp := client.GetShopList(0, 0, "")
	if resp.RetCode != 1 {
		return
	}

	if resp.Data == "" {
		return
	}

	if err := json.Unmarshal([]byte(resp.Data), &this.shop_list); err != nil {
		log.Error("shop.load unmarshal fail %v", err)
		return
	}

	return
}

func (this *shopManager) getProduct(productId string) *Shop_Item {
	for _, v := range this.shop_list {
		if v.ProductId == productId {
			return v
		}
	}
	return nil
}

func (this *shopManager) getProductName(productId string) string {
	for _, v := range this.shop_list {
		if v.ProductId == productId {
			return v.ProductDesc
		}
	}

	return ""
}
