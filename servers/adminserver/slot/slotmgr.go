package slot

import (
	"sort"
)

var mgr *slotManager

type slotManager struct {
	statDetail_list map[string][]*statInfo
}

func Run() {
	mgr = new(slotManager)
	mgr.statDetail_list = make(map[string][]*statInfo)
}

// 统计列表
func (this *slotManager) getStat(req req_base) (*statInfo, []*statInfo) {
	var list []*statInfo
	info := new(statInfo)

	for _, v := range getStat(req) {
		info.InvestAmount += v.InvestAmount
		if v.InvestAmount%100 == 0 {
			info.BetAmount += v.BetAmount
			info.BetTimes += v.BetTimes
			info.WinTimes += v.WinTimes
		}

		info.ResultAmount += v.ResultAmount
		info.WinAmount += v.WinAmount
		info.Tax += v.Tax

		list = append(list, v)
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].InvestAmount < list[j].InvestAmount
	})

	return info, list
}

// 统计详情
func (this *slotManager) getStatDetail(req req_base) []*statInfo {
	return getStatDetail(req)
}

// 统计列表
func (this *slotManager) getChipStat(req req_base) (*statInfo, []*statInfo) {
	var list []*statInfo
	info := new(statInfo)

	for _, v := range getChipStat(req) {
		info.InvestAmount += v.InvestAmount
		if v.InvestAmount%100 == 0 {
			info.BetAmount += v.BetAmount
			info.BetTimes += v.BetTimes
			info.WinTimes += v.WinTimes
		}

		info.ResultAmount += v.ResultAmount
		info.WinAmount += v.WinAmount
		info.Tax += v.Tax

		list = append(list, v)
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].InvestAmount < list[j].InvestAmount
	})

	return info, list
}

// 统计详情
func (this *slotManager) getChipStatDetail(req req_base) []*statInfo {
	return getChipStatDetail(req)
}
