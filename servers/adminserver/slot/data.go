package slot

const dateFormat = "2006-01-02 15:04:05"

type req_base struct {
	GameID       int    // 游戏ID
	InvestAmount int    // 下注金额
	BeginTime    string // 开始时间
	EndTime      string // 截止时间
}

// 统计信息
type statInfo struct {
	DateFlag        string `json:"DateFlag,omitempty"` // 日期标识
	InvestAmount    int    // 下注金额
	BetAmount       int    // 投注额
	BetTimes        int    // 投注次数
	ResultAmount    int    // 返还额
	MaxResultAmount int    // 最大返还额
	WinTimes        int    // 赢次数
	WinAmount       int    // 赢金额
	Tax             int    // 台费
}
