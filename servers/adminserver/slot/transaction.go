package slot

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
)

// 统计列表
func getStat(req req_base) []*statInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*statInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Slot_GetStat")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, req.GameID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out statInfo
		out.InvestAmount = int((*ret[0].(*interface{})).(int64))
		out.BetAmount = int((*ret[1].(*interface{})).(int64))
		out.BetTimes = int((*ret[2].(*interface{})).(int64))
		out.ResultAmount = int((*ret[3].(*interface{})).(int64))
		out.WinTimes = int((*ret[4].(*interface{})).(int64))
		out.WinAmount = int((*ret[5].(*interface{})).(int64))
		out.Tax = int((*ret[6].(*interface{})).(int64))
		out.MaxResultAmount = int((*ret[7].(*interface{})).(int64))
		list = append(list, &out)
	}

	return list
}

// 统计详情
func getStatDetail(req req_base) []*statInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Slot_GetStatDetail")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, req.GameID)
	statement.AddParamter("@InvestAmount", database.AdParamInput, database.AdInteger, 8, req.InvestAmount)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*statInfo

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out statInfo
		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.BetAmount = int((*ret[1].(*interface{})).(int64))
		out.BetTimes = int((*ret[2].(*interface{})).(int64))
		out.ResultAmount = int((*ret[3].(*interface{})).(int64))
		out.WinTimes = int((*ret[4].(*interface{})).(int64))
		out.WinAmount = int((*ret[5].(*interface{})).(int64))
		out.Tax = int((*ret[6].(*interface{})).(int64))
		out.MaxResultAmount = int((*ret[7].(*interface{})).(int64))
		list = append(list, &out)
	}

	return list
}

// 统计列表
func getChipStat(req req_base) []*statInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*statInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_SlotChip_GetStat")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, req.GameID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out statInfo
		out.InvestAmount = int((*ret[0].(*interface{})).(int64))
		out.BetAmount = int((*ret[1].(*interface{})).(int64))
		out.BetTimes = int((*ret[2].(*interface{})).(int64))
		out.ResultAmount = int((*ret[3].(*interface{})).(int64))
		out.WinTimes = int((*ret[4].(*interface{})).(int64))
		out.WinAmount = int((*ret[5].(*interface{})).(int64))
		out.Tax = int((*ret[6].(*interface{})).(int64))
		out.MaxResultAmount = int((*ret[7].(*interface{})).(int64))
		list = append(list, &out)
	}

	return list
}

// 统计详情
func getChipStatDetail(req req_base) []*statInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_SlotChip_GetStatDetail")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, req.GameID)
	statement.AddParamter("@InvestAmount", database.AdParamInput, database.AdInteger, 8, req.InvestAmount)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*statInfo

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out statInfo
		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.BetAmount = int((*ret[1].(*interface{})).(int64))
		out.BetTimes = int((*ret[2].(*interface{})).(int64))
		out.ResultAmount = int((*ret[3].(*interface{})).(int64))
		out.WinTimes = int((*ret[4].(*interface{})).(int64))
		out.WinAmount = int((*ret[5].(*interface{})).(int64))
		out.Tax = int((*ret[6].(*interface{})).(int64))
		out.MaxResultAmount = int((*ret[7].(*interface{})).(int64))
		list = append(list, &out)
	}

	return list
}
