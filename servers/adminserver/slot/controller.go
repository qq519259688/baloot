package slot

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 统计列表
func GetStat(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "slot.GetStat", err)
		return
	}

	var resp struct {
		Info *statInfo
		List []*statInfo
	}

	resp.Info, resp.List = mgr.getStat(req)
	c.JSON(http.StatusOK, resp)
	return
}

// 统计详情
func GetStatDetail(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "slot.GetStatDetail", err)
		return
	}

	var resp struct {
		List []*statInfo
	}

	resp.List = mgr.getStatDetail(req)
	c.JSON(http.StatusOK, resp)
	return
}

// 统计列表
func GetChipStat(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "slot.GetChipStat", err)
		return
	}

	var resp struct {
		Info *statInfo
		List []*statInfo
	}

	resp.Info, resp.List = mgr.getChipStat(req)
	c.JSON(http.StatusOK, resp)
	return
}

// 统计详情
func GetChipStatDetail(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "slot.GetChipStatDetail", err)
		return
	}

	var resp struct {
		List []*statInfo
	}

	resp.List = mgr.getChipStatDetail(req)
	c.JSON(http.StatusOK, resp)
	return
}
