package dao

import (
	"bet24.com/servers/adminserver/ip"
	"runtime/debug"
	"strconv"

	"bet24.com/database"
	"bet24.com/log"
)

// 金币日志
type (
	getChipLog_in struct {
		UserID     int    // 用户ID
		PartnerID  int    // 渠道ID
		SourceName string // 应用名称
		BeginTime  string // 开始时间
		EndTime    string // 截止时间
		PageIndex  int    // 页索引
		PageSize   int    // 页大小
	}

	getChipLogInfo struct {
		LogID         int    // 标识
		SourceName    string // 源名称
		MoneyType     string // 货币类型
		UserID        int    // 用户ID
		NickName      string // 昵称
		CurrentAmount int    // 当前金额
		ModifyAmount  int    // 修改金额
		StillAmount   int    // 剩余金额
		Remark        string // 备注
		IPAddress     string // IP地址
		Crdate        string // 时间
		PartnerName   string // 渠道名称
		TaxAmount     int    // 台费
	}

	getChipLog_out struct {
		RecordCount int // 记录数
		List        []getChipLogInfo
	}

	getChipLog struct {
		database.Trans_base
		In  getChipLog_in
		Out getChipLog_out
	}
)

func NewGetChipLog() *getChipLog {
	return &getChipLog{}
}

func (this *getChipLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Chip_GetCashLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 32, this.In.SourceName)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]getChipLogInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.LogID = int((*ret[0].(*interface{})).(int64))
			out.SourceName = (*ret[1].(*interface{})).(string)
			out.MoneyType = (*ret[2].(*interface{})).(string)
			out.UserID = int((*ret[3].(*interface{})).(int64))
			out.NickName = (*ret[4].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}

			out.CurrentAmount = int((*ret[5].(*interface{})).(int64))
			out.ModifyAmount = int((*ret[6].(*interface{})).(int64))
			out.StillAmount = int((*ret[7].(*interface{})).(int64))

			out.Remark = (*ret[8].(*interface{})).(string)
			out.IPAddress = (*ret[9].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[10].(*interface{})).(string)
			out.PartnerName = (*ret[11].(*interface{})).(string)

			out.TaxAmount = int((*ret[12].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 加金
type (
	chipSend_in struct {
		OpUserID   int    // 操作员ID
		OpUserName string // 操作员名称
		UserID     int    // 用户ID
		SourceName string // 应用名称
		Reason     string // 原因
		WantAmount int    // 操作金额
		WantBank   int    // 操作保险柜
		IpAddress  string // IP地址
	}

	chipSend_out struct {
		RetCode  int    // 操作结果
		ErrorMsg string // 操作描述
	}

	chipSend struct {
		database.Trans_base
		In  chipSend_in
		Out chipSend_out
	}
)

func NewChipSend() *chipSend {
	return &chipSend{}
}

func (this *chipSend) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Chip_SendCoin")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 32, this.In.SourceName)
	statement.AddParamter("@Reason", database.AdParamInput, database.AdNVarChar, 128, this.In.Reason)
	statement.AddParamter("@WantAmount", database.AdParamInput, database.AdBigint, 8, this.In.WantAmount)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	statement.AddParamter("@ErrorMsg", database.AdParamOutput, database.AdNVarChar, 64, this.Out.ErrorMsg)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
	this.Out.ErrorMsg = (*retRows[0][1].(*interface{})).(string)
}

// 扣金
type (
	chipDel_in struct {
		OpUserID   int    // 操作员ID
		OpUserName string // 操作员名称
		UserID     int    // 用户ID
		SourceName string // 应用名称
		Reason     string // 原因
		WantAmount int    // 操作金额
		WantBank   int    // 操作保险柜
		IpAddress  string // IP地址
	}

	chipDel_out struct {
		RetCode  int    // 操作结果
		ErrorMsg string // 操作描述
	}

	chipDel struct {
		database.Trans_base
		In  chipDel_in
		Out chipDel_out
	}
)

func NewChipDel() *chipDel {
	return &chipDel{}
}

func (this *chipDel) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Chip_DelCoin")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdVarChar, 32, this.In.SourceName)
	statement.AddParamter("@Reason", database.AdParamInput, database.AdVarChar, 128, this.In.Reason)
	statement.AddParamter("@WantAmount", database.AdParamInput, database.AdBigint, 8, this.In.WantAmount)
	statement.AddParamter("@WantBank", database.AdParamInput, database.AdBigint, 8, this.In.WantBank)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	statement.AddParamter("@ErrorMsg", database.AdParamOutput, database.AdNVarChar, 128, this.Out.ErrorMsg)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
	this.Out.ErrorMsg = (*retRows[0][1].(*interface{})).(string)
}

// 筹码统计
type (
	chipStat_in struct {
		BeginTime string // 开始时间
		EndTime   string // 截止时间
	}

	chipStatInfo struct {
		LogType     int    // 日志类型
		LogTypeName string // 日志类型名称
		GameID      int    // 游戏ID
		ChineseName string // 游戏名
		Amount      int    // 金额
		SendAmount  int    // 发行金额
		DelAmount   int    // 消耗金额
		DiffAmount  int    // 差额
	}

	chipStat_out struct {
		Send    int // 总发行
		Consume int // 总消耗

		GameSend    int // 游戏发行
		GameConsume int // 游戏消耗
		GameDiff    int // 游戏差额

		Cash            int // 金币总额
		Bank            int // 保险柜总额
		YesterdayAmount int // 前日总额
		CurrAmount      int // 当前总额

		List []chipStatInfo
	}

	chipStat struct {
		database.Trans_base
		In  chipStat_in
		Out chipStat_out
	}
)

func NewChipStat() *chipStat {
	return &chipStat{}
}

func (this *chipStat) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ChipStat_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)

	statement.AddParamter("@Send", database.AdParamOutput, database.AdBigint, 8, this.Out.Send)
	statement.AddParamter("@Consume", database.AdParamOutput, database.AdBigint, 8, this.Out.Consume)

	statement.AddParamter("@GameSend", database.AdParamOutput, database.AdBigint, 8, this.Out.GameSend)
	statement.AddParamter("@GameConsume", database.AdParamOutput, database.AdBigint, 8, this.Out.GameConsume)
	statement.AddParamter("@GameDiff", database.AdParamOutput, database.AdBigint, 8, this.Out.GameDiff)

	statement.AddParamter("@Cash", database.AdParamOutput, database.AdBigint, 8, this.Out.Cash)
	statement.AddParamter("@Bank", database.AdParamOutput, database.AdBigint, 8, this.Out.Bank)
	statement.AddParamter("@YesterdayAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.YesterdayAmount)
	statement.AddParamter("@CurrAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.CurrAmount)

	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]chipStatInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.LogType = int((*ret[0].(*interface{})).(int64))
			out.LogTypeName = (*ret[1].(*interface{})).(string)
			out.GameID = int((*ret[2].(*interface{})).(int64))
			out.Amount = int((*ret[3].(*interface{})).(int64))
			out.ChineseName = (*ret[4].(*interface{})).(string)
			out.SendAmount = int((*ret[5].(*interface{})).(int64))
			out.DelAmount = int((*ret[6].(*interface{})).(int64))
			out.DiffAmount = int((*ret[7].(*interface{})).(int64))
		}
	}

	ret := retRows[rowLen-1]

	this.Out.Send = int((*ret[0].(*interface{})).(int64))
	this.Out.Consume = int((*ret[1].(*interface{})).(int64))
	this.Out.GameSend = int((*ret[2].(*interface{})).(int64))
	this.Out.GameConsume = int((*ret[3].(*interface{})).(int64))
	this.Out.GameDiff = int((*ret[4].(*interface{})).(int64))
	this.Out.Cash = int((*ret[5].(*interface{})).(int64))
	this.Out.Bank = int((*ret[6].(*interface{})).(int64))
	this.Out.YesterdayAmount = int((*ret[7].(*interface{})).(int64))
	this.Out.CurrAmount = int((*ret[8].(*interface{})).(int64))
}

// 筹码变化
type (
	getUserChipStatList_in struct {
		UserID    int    // 用户ID
		IsGameWin int    // 是否输赢
		BeginTime string // 起始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	getUserChipStatListModel struct {
		RowNumber int    // 行号
		UserID    int    // UserID
		NickName  string // 昵称
		MoneySum  int    // 金币变化
	}

	getUserChipStatList_out struct {
		RecordCount int // 记录数
		TotalAmount int // 总计
		List        []getUserChipStatListModel
	}

	getUserChipStatList struct {
		database.Trans_base
		In  getUserChipStatList_in
		Out getUserChipStatList_out
	}
)

func NewGetUserChipStatList() *getUserChipStatList {
	return &getUserChipStatList{}
}

func (this *getUserChipStatList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetChipStatList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@IsGameWin", database.AdParamInput, database.AdInteger, 4, this.In.IsGameWin)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.TotalAmount)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]getUserChipStatListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RowNumber = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.MoneySum = int((*ret[3].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.TotalAmount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
}

// 用户筹码变化详情
type (
	getChipStatDetail_in struct {
		UserID    int    // 用户ID
		StartTime string // 开始时间
		EndTime   string // 截止时间
		IsGameWin int    // 是否游戏输入
	}

	getChipStatDetailModel struct {
		DataFlag    string `json:"dateFlag"`    // 日期
		ChineseName string `json:"chineseName"` // 游戏名
		LogTypeName string `json:"logTypeName"` // 日志类型
		MoneySum    string `json:"moneySum"`    // 数量
		UserID      int    `json:"userID"`      // 用户ID
		NickName    string `json:"nickName"`    // 昵称
	}

	getChipStatDetail_out struct {
		RecordCount int
		List        []getChipStatDetailModel
	}

	getChipStatDetail struct {
		database.Trans_base
		In  getChipStatDetail_in
		Out getChipStatDetail_out
	}
)

func NewGetChipStatDetail() *getChipStatDetail {
	return &getChipStatDetail{}
}

func (this *getChipStatDetail) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetChipStatDetail")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@StartTime", database.AdParamInput, database.AdVarChar, 20, this.In.StartTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@IsGameWin", database.AdParamInput, database.AdInteger, 4, this.In.IsGameWin)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
	this.State = true
	this.Out.List = make([]getChipStatDetailModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DataFlag = (*ret[0].(*interface{})).(string)
		out.ChineseName = (*ret[1].(*interface{})).(string)
		out.LogTypeName = (*ret[2].(*interface{})).(string)
		moneySum := int((*ret[3].(*interface{})).(int64))
		out.MoneySum = strconv.Itoa(moneySum)
		out.UserID = int((*ret[4].(*interface{})).(int64))
		out.NickName = (*ret[5].(*interface{})).(string)
		if info := tagMgr.getInfo(out.UserID); info != nil {
			out.NickName = info.NickName
		}
	}
}

// 后台管理--导出金币变化
type (
	chipStatExport_in struct {
		BeginTime string // 开始时间
		EndTime   string // 截止时间
	}

	chipStatExportModel struct {
		UserID       int    // 用户ID
		NickName     string // 昵称
		PartnerName  string // 渠道名称
		InvestAmount int    // 投注金额
		WinAmount    int    // 赢金额
		SubAmount    int    // 变化金额
	}

	chipStatExport_out struct {
		RecordCount int // 记录数
		List        []chipStatExportModel
	}

	chipStatExport struct {
		database.Trans_base
		In  chipStatExport_in
		Out chipStatExport_out
	}
)

func NewChipStatExport() *chipStatExport {
	return &chipStatExport{}
}

func (this *chipStatExport) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_ExportChipStat")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}
	this.Out.List = make([]chipStatExportModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.UserID = int((*ret[0].(*interface{})).(int64))
		out.NickName = (*ret[1].(*interface{})).(string)
		if info := tagMgr.getInfo(out.UserID); info != nil {
			out.NickName = info.NickName
		}
		out.PartnerName = (*ret[2].(*interface{})).(string)
		out.InvestAmount = int((*ret[3].(*interface{})).(int64))
		out.WinAmount = int((*ret[4].(*interface{})).(int64))
		out.SubAmount = out.InvestAmount + out.WinAmount
	}
}

// 税收总计
type (
	chipTaxStatList_in struct {
		BeginTime string
		EndTime   string
	}

	chipTaxStatListModel struct {
		ServerName string
		TaxSum     int
	}

	chipTaxStatList_out struct {
		TotalTaxAmount int
		List           []chipTaxStatListModel
	}

	chipTaxStatList struct {
		database.Trans_base
		In  chipTaxStatList_in
		Out chipTaxStatList_out
	}
)

func NewChipTaxStatList() *chipTaxStatList {
	return &chipTaxStatList{}
}

func (this *chipTaxStatList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ChipTaxStatTotal_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@TotalTaxAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.TotalTaxAmount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]chipTaxStatListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.ServerName = (*ret[0].(*interface{})).(string)
			out.TaxSum = int((*ret[1].(*interface{})).(int64))
		}
	}

	this.Out.TotalTaxAmount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 投注日志
type (
	getChipBetLog_in struct {
		UserID    int    // 用户ID
		GameID    int    // 游戏ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	getChipBetLogInfo struct {
		RecordID     string // 标识
		UserID       int    // 用户ID
		NickName     string // 昵称
		ChineseName  string // 游戏名
		BetAmount    int    // 下注金额
		BetTime      string // 下注时间
		BetZone      string // 下注区域
		Odds         string // 赔率
		ResultAmount int    // 结算金额
		ResultTime   string // 结算时间
		ResultZone   string // 结算区域
		WinAmount    int    // 输赢金额
		StillAmount  int    // 剩余金额
		Status       int    // 状态 0=下注  1=结算
		Tax          int    // 台费
		SubSeconds   int    // 时间差
	}

	getChipBetLog_out struct {
		RecordCount int // 记录数
		WinAmount   int // 输赢金额
		List        []getChipBetLogInfo
	}

	getChipBetLog struct {
		database.Trans_base
		In  getChipBetLog_in
		Out getChipBetLog_out
	}
)

func NewGetChipBetLog() *getChipBetLog {
	return &getChipBetLog{}
}

func (this *getChipBetLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Game_GetChipBetLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, this.In.GameID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@WinAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.WinAmount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]getChipBetLogInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RecordID = (*ret[0].(*interface{})).(string)
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ChineseName = (*ret[3].(*interface{})).(string)
			out.BetAmount = int((*ret[4].(*interface{})).(int64))
			out.BetTime = (*ret[5].(*interface{})).(string)
			out.BetZone = (*ret[6].(*interface{})).(string)
			out.Odds = string((*ret[7].(*interface{})).([]byte))
			out.ResultAmount = int((*ret[8].(*interface{})).(int64))
			out.ResultTime = (*ret[9].(*interface{})).(string)
			out.ResultZone = (*ret[10].(*interface{})).(string)
			out.WinAmount = int((*ret[11].(*interface{})).(int64))
			out.StillAmount = int((*ret[12].(*interface{})).(int64))
			out.Status = int((*ret[13].(*interface{})).(int64))
			out.Tax = int((*ret[14].(*interface{})).(int64))
			out.SubSeconds = int((*ret[15].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.WinAmount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
}

// 筹码排行
type (
	chipTop_in struct {
		PartnerID int // 渠道ID
		PageIndex int // 页索引
		PageSize  int // 页大小
	}

	chipTopInfo struct {
		Rank        int     // 名次
		PartnerName string  // 渠道名称
		UserID      int     // 用户ID
		NickName    string  // 昵称
		GoldAmount  int     // 金币
		BankAmount  int     // 保险柜
		PayMoney    float64 // 充值金额
	}

	chipTop_out struct {
		RecordCount int // 总记录数
		List        []chipTopInfo
	}

	chipTop struct {
		database.Trans_base
		In  chipTop_in
		Out chipTop_out
	}
)

func NewChipTop() *chipTop {
	return &chipTop{}
}

func (this *chipTop) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Chip_GetTop")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]chipTopInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rank = int((*ret[0].(*interface{})).(int64))
			out.PartnerName = (*ret[1].(*interface{})).(string)
			out.UserID = int((*ret[2].(*interface{})).(int64))
			out.NickName = (*ret[3].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.GoldAmount = int((*ret[4].(*interface{})).(int64))
			out.BankAmount = int((*ret[5].(*interface{})).(int64))

			payMoney := string((*ret[6].(*interface{})).([]byte))
			out.PayMoney, _ = strconv.ParseFloat(payMoney, 64)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 存量统计
type (
	chipStatTotalList_in struct {
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	chipStatTotalInfo struct {
		RowNumber          int    // 序号
		Crdate             string // 时间
		TotalCurrentAmount int64  // 金币存量
		TotalBankAmount    int64  // 保险柜存量
		TotalAmount        int64  // 总存量
	}

	chipStatTotalList_out struct {
		RecordCount int // 总记录数
		List        []chipStatTotalInfo
	}

	chipStatTotalList struct {
		database.Trans_base
		In  chipStatTotalList_in
		Out chipStatTotalList_out
	}
)

func NewChipStatTotalList() *chipStatTotalList {
	return &chipStatTotalList{}
}

func (this *chipStatTotalList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ChipStatTotal_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]chipStatTotalInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RowNumber = int((*ret[0].(*interface{})).(int64))
			out.Crdate = (*ret[1].(*interface{})).(string)
			out.TotalCurrentAmount = (*ret[2].(*interface{})).(int64)
			out.TotalBankAmount = (*ret[3].(*interface{})).(int64)
			out.TotalAmount = out.TotalCurrentAmount + out.TotalBankAmount
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 存量统计(每天)
type (
	chipStatTotalListByDay_in struct {
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	chipStatTotalByDayInfo struct {
		RowNumber          int    // 序号
		Crdate             string // 时间
		TotalAmount        int64  // 总存量
		TotalBankAmount    int64  // 保险柜存量
		TotalCurrentAmount int64  // 金币存量
	}

	chipStatTotalListByDay_out struct {
		RecordCount int // 总记录数
		List        []chipStatTotalByDayInfo
	}

	chipStatTotalListByDay struct {
		database.Trans_base
		In  chipStatTotalListByDay_in
		Out chipStatTotalListByDay_out
	}
)

func NewChipStatTotalListByDay() *chipStatTotalListByDay {
	return &chipStatTotalListByDay{}
}

func (this *chipStatTotalListByDay) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ChipStatTotal_GetListByDay")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]chipStatTotalByDayInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RowNumber = int((*ret[0].(*interface{})).(int64))
			out.Crdate = (*ret[1].(*interface{})).(string)
			out.TotalAmount = (*ret[2].(*interface{})).(int64)
			out.TotalBankAmount = (*ret[3].(*interface{})).(int64)
			out.TotalCurrentAmount = (*ret[4].(*interface{})).(int64)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 赠送日志
type (
	transferLog_in struct {
		UserID       int    // 账号ID
		BeginTime    string // 开始时间
		EndTime      string // 截止时间
		PageIndex    int    // 页索引
		PageSize     int    // 页大小
		AcceptUserID int    // 接受账号ID
	}

	transferLogModel struct {
		Rid                int    // 标识
		UserID             int    // 用户ID
		NickName           string // 昵称
		CurrAmount         int    // 操作前
		ModifyAmount       int    // 操作
		StillAmount        int    // 剩余
		Action             string // 1=转入  -1=转出
		AcceptUserID       int    // 接收方ID
		AcceptNickName     string // 接收方昵称
		AcceptCurrAmount   int    // 接收方操作前
		AcceptModifyAmount int    // 接收方操作
		AcceptStillAmount  int    // 接收方剩余
		TaxAmount          int    // 台费
		Remark             string // 备注
		IPAddress          string // IP地址
		Crdate             string // 时间
	}

	transferLog_out struct {
		RecordCount    int // 记录数
		OutTotalAmount int // 总转出
		InTotalAmount  int // 总转入
		TotalTaxAmount int // 总台费
		List           []transferLogModel
	}

	transferLog struct {
		database.Trans_base
		In  transferLog_in
		Out transferLog_out
	}
)

func NewTransferLog() *transferLog {
	return &transferLog{}
}

func (this *transferLog) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Chip_GetTransferLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@OutTotalAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.OutTotalAmount)
	statement.AddParamter("@InTotalAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.InTotalAmount)
	statement.AddParamter("@TotalTaxAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.TotalTaxAmount)
	statement.AddParamter("@AcceptUserID", database.AdParamInput, database.AdInteger, 4, this.In.AcceptUserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]transferLogModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.CurrAmount = int((*ret[3].(*interface{})).(int64))
			out.ModifyAmount = int((*ret[4].(*interface{})).(int64))
			out.StillAmount = int((*ret[5].(*interface{})).(int64))
			out.Action = (*ret[6].(*interface{})).(string)
			out.AcceptUserID = int((*ret[7].(*interface{})).(int64))
			out.AcceptNickName = (*ret[8].(*interface{})).(string)
			out.AcceptCurrAmount = int((*ret[9].(*interface{})).(int64))
			out.AcceptModifyAmount = int((*ret[10].(*interface{})).(int64))
			out.AcceptStillAmount = int((*ret[11].(*interface{})).(int64))
			out.TaxAmount = int((*ret[12].(*interface{})).(int64))
			out.Remark = (*ret[13].(*interface{})).(string)
			out.IPAddress = (*ret[14].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[15].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.OutTotalAmount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
	this.Out.InTotalAmount = int((*retRows[rowLen-1][2].(*interface{})).(int64))
	this.Out.TotalTaxAmount = int((*retRows[rowLen-1][3].(*interface{})).(int64))
}

type (
	chipLoginStat_in struct {
		BeginTime string
		EndTime   string
	}

	chipLoginStatItem struct {
		DateFlag   string
		LoginCount int
	}

	chipLoginStat_out struct {
		RecordCount int
		List        []chipLoginStatItem
	}

	chipLoginStat struct {
		database.Trans_base
		In  chipLoginStat_in
		Out chipLoginStat_out
	}
)

func NewChipLoginStat() *chipLoginStat {
	return &chipLoginStat{}
}

func (this *chipLoginStat) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_ChipLoginStat_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	this.Out.List = make([]chipLoginStatItem, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.LoginCount = int((*ret[1].(*interface{})).(int64))
	}
}

type (
	behaviorList_in struct {
		UserID    int
		BeginTime string
		EndTime   string
		PageIndex int
		PageSize  int
	}

	behaviorListModel struct {
		UserID      int
		SceneName   string
		ActTime     string
		Amount      int
		EndTime     string
		StaySeconds int
		StillAmount int
		WinAmount   int
		GameCount   int
		Status      int
	}

	behaviorList_out struct {
		RecordCount int
		List        []behaviorListModel
	}

	behaviorList struct {
		database.Trans_base
		In  behaviorList_in
		Out behaviorList_out
	}
)

func NewBehaviorList() *behaviorList {
	return &behaviorList{}
}

func (this *behaviorList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserBehavior_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdParamInput, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]behaviorListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.UserID = int((*ret[0].(*interface{})).(int64))
			out.SceneName = (*ret[1].(*interface{})).(string)
			out.ActTime = (*ret[2].(*interface{})).(string)
			out.Amount = int((*ret[3].(*interface{})).(int64))
			out.EndTime = (*ret[4].(*interface{})).(string)
			out.StaySeconds = int((*ret[5].(*interface{})).(int64))
			out.StillAmount = int((*ret[6].(*interface{})).(int64))
			out.WinAmount = int((*ret[7].(*interface{})).(int64))
			out.GameCount = int((*ret[8].(*interface{})).(int64))
			out.Status = int((*ret[9].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}
