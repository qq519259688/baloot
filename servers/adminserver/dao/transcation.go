package dao

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/adminserver/config"
)

var (
	CenterDB *database.DataBase
	MSDB     *database.DataBase
	Games    []*Transaction_Game
)

func Run() {
	CenterDB = database.NewDataBase(
		config.Server.Datasource, config.Server.Database,
		public.DecryptDBString(config.Server.Login),
		public.DecryptDBString(config.Server.Password))

	MSDB = database.NewDataBase(
		config.Server.Datasource, config.Server.MSDBDatabase,
		public.DecryptDBString(config.Server.Login),
		public.DecryptDBString(config.Server.Password))

	// 创建游戏库
	gameCount := len(config.Server.Games)
	Games = make([]*Transaction_Game, gameCount)
	for i, v := range config.Server.Games {
		log.Debug("v.GameID=%d v.Database=%s", v.GameID, v.Database)
		Games[i] = NewTransactionGame(v.GameID, v.Datasource, v.Database, v.Login, v.Password)
	}
}

// 获取游戏库
func GetGameDB(gameID int) *database.DataBase {
	for _, v := range Games {
		if gameID == v.GameID {
			return v.GameDB
		}
	}
	//log.Release("GetGameDB failed GameID = %d", gameID)
	return nil
}
