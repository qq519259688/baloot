package dao

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 平台信息列表
type (
	platformInfoList_in struct {
		PartnerID int //渠道ID
	}

	platformInfo struct {
		PartnerID int    //合作商ID
		Version   string //版本
		Download  string //下载
	}

	platformInfoList_out struct {
		RecordCount int //记录数
		List        []platformInfo
	}

	platformInfoList struct {
		database.Trans_base
		In  platformInfoList_in
		Out platformInfoList_out
	}
)

func NewPlatformInfoList() *platformInfoList {
	return &platformInfoList{}
}

func (this *platformInfoList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_PlatformInfo_GetList")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]platformInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.PartnerID = int((*ret[0].(*interface{})).(int64))
		out.Version = (*ret[1].(*interface{})).(string)
		out.Download = (*ret[2].(*interface{})).(string)
	}
}

// 平台信息修改
type (
	platformInfoUp_in struct {
		OpUserID   int    //操作员ID
		OpUserName string //操作员名称
		PartnerID  int    //渠道ID
		Version    string //本版
		IpAddress  string //IP地址
		Download   string //下注地址
	}

	platformInfoUp_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	platformInfoUp struct {
		database.Trans_base
		In  platformInfoUp_in
		Out platformInfoUp_out
	}
)

func NewPlatformInfoUp() *platformInfoUp {
	return &platformInfoUp{}
}

func (this *platformInfoUp) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_PlatformInfo_Update")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@Version", database.AdParamInput, database.AdVarChar, 16, this.In.Version)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@Download", database.AdParamInput, database.AdVarChar, 128, this.In.Download)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.Out.RetCode = 1
}

// 平台配置信息
type (
	platformConfig_out struct {
		RegGoldSend         int // 注册赠送金币
		DailyTransferAmount int // 日转账限额
		BindFacebook        int // 绑定Facebook
		BindMobile          int // 绑定手机号
		IsCouponTask        int // 是否开启红包券任务
		UpgradeAPK          int // 升级APK
		SurchargeRate       int // 提现手续费比率
		FreeSurchargeTimes  int // 免手续费提取次数
		TotalAmount         int // 可提取总金额
		DailyNoAuditAmount  int // 日无审核提现金额
		DailyNoAuditTimes   int // 日无审核提现次数
		DailyMaxTimes       int // 日最多提现次数
		MinWithdrawAmount   int // 提现最小金额
	}

	platformConfig struct {
		database.Trans_base
		Out platformConfig_out
	}
)

func NewPlatformConfig() *platformConfig {
	return &platformConfig{}
}

func (this *platformConfig) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_PlatformConfig_GetInfo")
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}
	this.State = true
	ret := retRows[0]
	this.Out.RegGoldSend = int((*ret[0].(*interface{})).(int64))
	this.Out.DailyTransferAmount = int((*ret[1].(*interface{})).(int64))
	this.Out.BindFacebook = int((*ret[2].(*interface{})).(int64))
	this.Out.BindMobile = int((*ret[3].(*interface{})).(int64))
	this.Out.UpgradeAPK = int((*ret[4].(*interface{})).(int64))
	this.Out.IsCouponTask = int((*ret[5].(*interface{})).(int64))
}

// 修改平台配置
type (
	platformConfigUp_in struct {
		OpUserID            int    // 操作员ID
		OpUserName          string // 操作员名称
		RegGoldSend         int    // 注册赠送金币
		DailyTransferAmount int    // 日转账限额
		BindFacebook        int    // 绑定Facebook
		BindMobile          int    // 绑定手机号
		UpgradeAPK          int    // 升级APK

		IpAddress string
	}

	platformConfigUp_out struct {
		RetCode  int
		ErrorMsg string
	}

	platformConfigUp struct {
		database.Trans_base
		In  platformConfigUp_in
		Out platformConfigUp_out
	}
)

func NewPlatformConfigUp() *platformConfigUp {
	return &platformConfigUp{}
}

func (this *platformConfigUp) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_PlatformConfig_Update")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@RegGoldSend", database.AdParamInput, database.AdInteger, 4, this.In.RegGoldSend)
	statement.AddParamter("@DailyTransferAmount", database.AdParamInput, database.AdInteger, 4, this.In.DailyTransferAmount)
	statement.AddParamter("@BindFacebook", database.AdParamInput, database.AdInteger, 4, this.In.BindFacebook)
	statement.AddParamter("@BindMobile", database.AdParamInput, database.AdInteger, 4, this.In.BindMobile)
	statement.AddParamter("@UpgradeAPK", database.AdParamInput, database.AdInteger, 4, this.In.UpgradeAPK)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}
	this.State = true
	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
	if this.Out.RetCode == 1 {
		this.Out.ErrorMsg = "修改配置成功"
	} else {
		this.Out.ErrorMsg = "修改配置失败"
	}
}

// 添加平台配置log操作记录
type (
	addLog_in struct {
		OpUserID   int    // 管理员用户ID（AdminUserID）
		OpUserName string // 管理员名称（AdminUserName）
		Memo       string // 管理员备注（Memo）
		IPAddress  string // IP地址（IPAddress）
		KeyName    string // 配置的key
		Eq         int    // 修改的下标位置（Eq）
	}

	platformConfigLog struct {
		database.Trans_base
		In addLog_in
	}
)

func NewPlatformConfigLog() *platformConfigLog {
	return &platformConfigLog{}
}

func (this *platformConfigLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("PlatformConfig.addLog transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Manage_PlatformConfig_Log")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@Memo", database.AdParamInput, database.AdNVarChar, 128, this.In.Memo)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 15, this.In.IPAddress)
	statement.AddParamter("@KeyName", database.AdParamInput, database.AdVarChar, 64, this.In.KeyName)
	statement.AddParamter("@Eq", database.AdParamInput, database.AdInteger, 4, this.In.Eq)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	CenterDB.ExecSql(sqlstring)
	this.State = true
}

// 获取平台配置的操作记录列表
type (
	getList_in struct {
		OpUserID int    // 操作用户ID
		KeyName  string // 配置的key
	}

	getList_out struct {
		AdminUserName string // 管理员名称（AdminUserName）
		Memo          string // 管理员备注（Memo）
		KeyName       string // 配置的key
		Eq            int    // 修改的下标位置（Eq）
		Crdate        string // 创建时间（Crdate）
	}

	platformConfigGetList struct {
		database.Trans_base
		In  getList_in
		Out []getList_out
	}
)

func NewPlatformConfigGetList() *platformConfigGetList {
	return &platformConfigGetList{}
}

func (this *platformConfigGetList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("PlatformConfig.GetList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Manage_PlatformConfig_GetList")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@KeyName", database.AdParamInput, database.AdVarChar, 64, this.In.KeyName)
	sqlString := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out = make([]getList_out, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out[i]

		out.AdminUserName = (*ret[0].(*interface{})).(string)
		out.Memo = (*ret[1].(*interface{})).(string)
		out.KeyName = (*ret[2].(*interface{})).(string)
		out.Eq = int((*ret[3].(*interface{})).(int64))
		out.Crdate = (*ret[4].(*interface{})).(string)
	}
}
