package dao

import (
	"encoding/json"
	"fmt"
	"runtime/debug"
	"strconv"

	"bet24.com/database"
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

// 兑换历史
type (
	exchangeHistory_in struct {
		Id        int    // 标识
		UserID    int    // 用户ID
		Memo      string // 备注
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
		Status    int    // -1=所有  0=待发货  1=已发货
	}

	exchangeHistoryModel struct {
		Id           int    // 标识
		UserID       int    // 用户ID
		NickName     string // 昵称
		ExchangeID   int    // 兑换ID
		Price        string // 价格
		Items        string
		ItemPack     []item.ItemPack `json:"-"` // 物品
		Item         string          // 物品
		Remark       string          // 备注
		IsMust       int             // 是否必须 1=必须  其他否
		Status       int             // 状态
		Crdate       string          // 时间
		ExchangeType int             // 兑换类型(1=奖券兑换金币  2=代理兑换元宝 3=代理兑换电话卡  4=代理兑换RP)
	}

	exchangeHistory_out struct {
		RecordCount int // 记录数
		List        []exchangeHistoryModel
	}

	exchangeHistory struct {
		database.Trans_base
		In  exchangeHistory_in
		Out exchangeHistory_out
	}
)

func NewExchangeHistory() *exchangeHistory {
	return &exchangeHistory{}
}

func (this *exchangeHistory) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Exchange_GetHistoryList")
	statement.AddParamter("@Id", database.AdParamInput, database.AdInteger, 4, this.In.Id)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@Memo", database.AdParamInput, database.AdVarChar, 1024, this.In.Memo)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, this.In.Status)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]exchangeHistoryModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Id = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ExchangeID = int((*ret[3].(*interface{})).(int64))
			price := string((*ret[4].(*interface{})).([]byte))
			fPrice, _ := strconv.ParseFloat(price, 64)
			out.Price = fmt.Sprintf("%.2f", fPrice)
			out.Items = (*ret[5].(*interface{})).(string)
			if err := json.Unmarshal([]byte(out.Items), &out.ItemPack); err != nil {
				log.Error("ExchangeHistory json unmarshal fail %v", err)
			}
			out.Remark = (*ret[6].(*interface{})).(string)
			out.Status = int((*ret[7].(*interface{})).(int64))
			out.Crdate = (*ret[8].(*interface{})).(string)
			out.ExchangeType = int((*ret[9].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 兑换历史
type (
	exchangeCash_in struct {
		UserID    int    // 用户ID
		Memo      string // 备注
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
		Status    int    // -1=所有  0=待发货  1=已发货
	}

	exchangeCash_out struct {
		RecordCount int // 记录数
		List        []exchangeHistoryModel
	}

	exchangeCash struct {
		database.Trans_base
		In  exchangeCash_in
		Out exchangeCash_out
	}
)

func NewExchangeCash() *exchangeCash {
	return &exchangeCash{}
}

func (this *exchangeCash) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Exchange_GetCashList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@Memo", database.AdParamInput, database.AdVarChar, 1024, this.In.Memo)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, this.In.Status)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]exchangeHistoryModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Id = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ExchangeID = int((*ret[3].(*interface{})).(int64))
			price := string((*ret[4].(*interface{})).([]byte))
			fPrice, _ := strconv.ParseFloat(price, 64)
			out.Price = fmt.Sprintf("%.2f", fPrice)
			out.Items = (*ret[5].(*interface{})).(string)
			if err := json.Unmarshal([]byte(out.Items), &out.ItemPack); err != nil {
				log.Error("ExchangeHistory json unmarshal fail %v", err)
			}
			out.Remark = (*ret[6].(*interface{})).(string)
			out.Status = int((*ret[7].(*interface{})).(int64))
			out.Crdate = (*ret[8].(*interface{})).(string)
			out.ExchangeType = int((*ret[9].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 修改兑换历史
type (
	exchangeHistoryUpdate_in struct {
		OpUserID     int    // 操作员ID
		OpUserName   string // 操作员名称
		Id           int    // 兑换历史标识
		Status       int    // 状态（0=待发货  1=已发货）
		Remark       string // 备注(格式：SNG比赛ID,手机号)
		IpAddress    string // ip地址
		UserID       int    // 用户ID
		Items        string // 道具
		Price        int    // 价格
		ExchangeType int    // 兑换类型(1=奖券兑换金币  2=代理兑换元宝 3=代理兑换电话卡  4=代理兑换RP  5=金币提现)
	}

	exchangeHistoryUpdate_out struct {
		RetCode  int    // 操作结果
		ErrorMsg string // 操作描述
		Remark   string // 备注
	}

	exchangeHistoryUpdate struct {
		database.Trans_base
		In  exchangeHistoryUpdate_in
		Out exchangeHistoryUpdate_out
	}
)

func NewExchangeHistoryUpdate() *exchangeHistoryUpdate {
	return &exchangeHistoryUpdate{}
}

func (this *exchangeHistoryUpdate) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Exchange_UpdateHistory")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@Id", database.AdParamInput, database.AdInteger, 4, this.In.Id)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, this.In.Status)
	statement.AddParamter("@Remark", database.AdParamInput, database.AdVarChar, 128, this.In.Remark)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	statement.AddParamter("@NewRemark", database.AdParamOutput, database.AdNVarChar, 1024, this.Out.Remark)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
	this.Out.Remark = (*retRows[0][1].(*interface{})).(string)
	if this.Out.RetCode == 1 {
		this.Out.ErrorMsg = "修改成功"
	} else {
		this.Out.ErrorMsg = "修改失败"
	}
}
