package dao

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

//作业列表
type (
	jobList_in struct {
		DataBaseName string //数据库名称
	}

	jobInfo struct {
		Database_name    string //数据库名称
		Name             string //作业名称
		JobEnabled       string //作业是否启用
		Step_name        string //作业步骤名称
		Command          string //执行命令
		Last_run_time    string //上一次执行间
		ScheduleEnabled  string //步骤是否启用
		Freq_type        string //作业运行的频率
		Freq_subday_type string //每天频率类型
		Description      string //作业描述
	}

	jobList_out struct {
		RecordCount int //记录数
		List        []jobInfo
	}

	jobList struct {
		database.Trans_base
		In  jobList_in
		Out jobList_out
	}
)

func NewJobList() *jobList {
	return &jobList{}
}

func (this *jobList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("MSDB_SysJobs_GetList")
	statement.AddParamter("@DataBaseName", database.AdParamInput, database.AdVarChar, 32, this.In.DataBaseName)
	sqlstring := statement.GenSql()
	retRows := MSDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]jobInfo, rowLen)

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.Database_name = (*ret[0].(*interface{})).(string)
		out.Name = (*ret[1].(*interface{})).(string)
		out.JobEnabled = (*ret[2].(*interface{})).(string)
		out.Step_name = (*ret[3].(*interface{})).(string)
		out.Command = (*ret[4].(*interface{})).(string)
		out.Last_run_time = (*ret[5].(*interface{})).(string)
		out.ScheduleEnabled = (*ret[6].(*interface{})).(string)
		out.Freq_type = (*ret[7].(*interface{})).(string)
		out.Freq_subday_type = (*ret[8].(*interface{})).(string)
		out.Description = (*ret[9].(*interface{})).(string)
	}
}

//作业简单列表
type (
	jobSimpleList_in struct {
		DataBaseName string //数据库名称
	}

	jobSimpleInfo struct {
		Name string //作业名称
	}

	jobSimpleList_out struct {
		RecordCount int //记录数
		List        []jobSimpleInfo
	}

	jobSimpleList struct {
		database.Trans_base
		In  jobList_in
		Out jobList_out
	}
)

func NewJobSimpleList() *jobSimpleList {
	return &jobSimpleList{}
}

func (this *jobSimpleList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("MSDB_SysJobs_GetSimplyList")
	statement.AddParamter("@DataBaseName", database.AdParamInput, database.AdVarChar, 32, this.In.DataBaseName)
	sqlstring := statement.GenSql()
	retRows := MSDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]jobInfo, rowLen)

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.Name = (*ret[0].(*interface{})).(string)
	}
}

//作业历史列表
type (
	jobHistoryList_in struct {
		DataBaseName string //数据库名称
		JobName      string //作业名称
		RunStatus    int    //运行失败的作业 -1=所有对象
		PageIndex    int    //页索引
		PageSize     int    //页大小
	}

	jobHistoryInfo struct {
		Database_name     string //数据库名称
		Job_name          string //作业名称
		Message           string //消息
		Step_id           int    //作业中步骤的ID
		Run_status        bool   //执行状态
		Command           string //执行命令
		Run_duration      string //运行时间
		Retries_attempted int    //重试次数
		Run_time          string //运行时间
	}

	jobHistoryList_out struct {
		RecordCount int //记录数
		List        []jobHistoryInfo
	}

	jobHistoryList struct {
		database.Trans_base
		In  jobHistoryList_in
		Out jobHistoryList_out
	}
)

func NewJobHistoryList() *jobHistoryList {
	return &jobHistoryList{}
}

func (this *jobHistoryList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("MSDB_SysJobHistory_GetList")
	statement.AddParamter("@DataBaseName", database.AdParamInput, database.AdVarChar, 32, this.In.DataBaseName)
	statement.AddParamter("@JobName", database.AdParamInput, database.AdVarChar, 32, this.In.JobName)
	statement.AddParamter("@RunStatus", database.AdParamInput, database.AdInteger, 4, this.In.RunStatus)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := MSDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true

	if rowLen > 1 {
		this.Out.List = make([]jobHistoryInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Database_name = (*ret[0].(*interface{})).(string)
			out.Job_name = (*ret[1].(*interface{})).(string)
			out.Message = (*ret[2].(*interface{})).(string)
			out.Step_id = int((*ret[3].(*interface{})).(int64))
			out.Run_status = (*ret[4].(*interface{})).(bool)
			out.Command = (*ret[5].(*interface{})).(string)
			out.Run_duration = (*ret[6].(*interface{})).(string)
			out.Retries_attempted = int((*ret[7].(*interface{})).(int64))
			out.Run_time = (*ret[8].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}
