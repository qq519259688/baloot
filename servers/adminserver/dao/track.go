package dao

import (
	"runtime/debug"
	"time"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
)

// 游戏轨迹记录
type (
	userTrackList_in struct {
		BeginTime string
		EndTime   string
	}

	userTrackListModel struct {
		Level string //1级类目
		Item  string //类目
		Times int    //次数
		Users int    //用户数
	}

	userTrackList_out struct {
		RecordCount int
		List        []userTrackListModel
	}

	userTrackList struct {
		database.Trans_base
		In  userTrackList_in
		Out userTrackList_out
	}
)

func NewUserTrackList() *userTrackList {
	return &userTrackList{}
}

func (this *userTrackList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserTrack_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}
	this.Out.List = make([]userTrackListModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.Level = (*ret[0].(*interface{})).(string)
		out.Item = (*ret[1].(*interface{})).(string)
		out.Times = int((*ret[2].(*interface{})).(int64))
		out.Users = int((*ret[3].(*interface{})).(int64))
	}

	return
}

// 游戏轨迹统计
type (
	userTrackStat_in struct {
		BeginTime string
		EndTime   string
	}

	userTrackStatModel struct {
		Level_1 string //1级类目
		Level_2 string //2级类目
		Level_3 string //3级类目
		Times   int    //次数
		Users   int    //用户数
	}

	userTrackStat_out struct {
		RecordCount int
		List        []userTrackStatModel
	}

	userTrackStat struct {
		database.Trans_base
		In  userTrackStat_in
		Out userTrackStat_out
	}
)

func NewUserTrackStat() *userTrackStat {
	return &userTrackStat{}
}

func (this *userTrackStat) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserTrack_GetStat")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}
	this.Out.List = make([]userTrackStatModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.Level_1 = (*ret[0].(*interface{})).(string)
		out.Level_2 = (*ret[1].(*interface{})).(string)
		out.Level_3 = (*ret[2].(*interface{})).(string)
		out.Times = int((*ret[3].(*interface{})).(int64))
		out.Users = int((*ret[4].(*interface{})).(int64))
	}

	return
}

// 用户足迹
type (
	trackList_in struct {
		UserID    int    //用户ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	trackListModel struct {
		Rid      int
		UserID   int
		Crdate   string
		Level_1  string
		Level_2  string
		Level_3  string
		Seconds  float64
		currTime time.Time
	}

	trackList_out struct {
		RecordCount int
		List        []trackListModel
	}

	trackList struct {
		database.Trans_base
		In  trackList_in
		Out trackList_out
	}
)

func NewTrackList() *trackList {
	return &trackList{}
}

func (this *trackList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetTrack")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	if rowLen > 1 {
		this.Out.List = make([]trackListModel, rowLen-1)

		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.UserID = this.In.UserID
			out.Crdate = (*ret[1].(*interface{})).(string)
			out.Level_1 = (*ret[2].(*interface{})).(string)
			out.Level_2 = (*ret[3].(*interface{})).(string)
			out.Level_3 = (*ret[4].(*interface{})).(string)
			out.currTime, _ = time.Parse(common.Layout, out.Crdate)

			if i > 0 {
				out.Seconds = this.Out.List[i].currTime.Sub(this.Out.List[i-1].currTime).Seconds()
			}
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}
