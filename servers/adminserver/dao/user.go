package dao

import (
	"bet24.com/servers/adminserver/ip"
	waterpool "bet24.com/servers/micros/waterpool/proto"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 管理员登陆
type (
	userLogin_in struct {
		AdminUserID   int    //管理员ID
		AdminUserName string //管理员名称
		LoginPassword string //登陆密码
		IpAddress     string //IP地址
	}

	userLoginModel struct {
		RoleID int //角色ID
	}

	userLogin_out struct {
		CurrAdminUserID   int    //当前用户ID
		CurrAdminUserName string //当前用户名
		RetCode           int    //操作结果
		ErrorMsg          string //操作描述
	}

	userLogin struct {
		database.Trans_base
		In  userLogin_in
		Out userLogin_out
	}
)

func NewUserLogin() *userLogin {
	return &userLogin{}
}

func (this *userLogin) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminUser_Login")
	statement.AddParamter("@AdminUserID", database.AdParamInput, database.AdInteger, 4, this.In.AdminUserID)
	statement.AddParamter("@AdminUserName", database.AdParamInput, database.AdVarChar, 32, this.In.AdminUserName)
	statement.AddParamter("@LoginPassword", database.AdParamInput, database.AdVarChar, 32, this.In.LoginPassword)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@CurrAdminUserID", database.AdParamOutput, database.AdInteger, 4, this.Out.CurrAdminUserID)
	statement.AddParamter("@CurrAdminUserName", database.AdParamOutput, database.AdVarChar, 32, this.Out.CurrAdminUserName)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.CurrAdminUserID = int((*retRows[0][0].(*interface{})).(int64))
	this.Out.CurrAdminUserName = (*retRows[0][1].(*interface{})).(string)
	this.Out.RetCode = int((*retRows[0][2].(*interface{})).(int64))
}

// 获取页面列表
type (
	pageList_in struct {
		AdminUserID int //管理员ID
	}

	pageInfo struct {
		PageID   int    //页ID
		PageName string //页名称
		PageUrl  string //页路径
		IsMenu   int    //是否菜单
	}

	pageList_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
		List     []pageInfo
	}

	pageList struct {
		database.Trans_base
		In  pageList_in
		Out pageList_out
	}
)

func NewPageList() *pageList {
	return &pageList{}
}

func (this *pageList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminUser_GetPageList")
	statement.AddParamter("@AdminUserID", database.AdParamInput, database.AdInteger, 4, this.In.AdminUserID)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]pageInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]
			out.PageID = int((*ret[0].(*interface{})).(int64))
			out.PageName = (*ret[1].(*interface{})).(string)
			out.PageUrl = (*ret[2].(*interface{})).(string)
			out.IsMenu = int((*ret[3].(*interface{})).(int64))
		}
	}

	this.Out.RetCode = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 获取管理用户信息
type (
	getInfo_in struct {
		AdminUserID int //管理员ID
	}

	getInfo_out struct {
		AdminUserID  int    //管理员ID
		IPAddress    string //IP地址
		FailureTimes int    //失败次数
		UpdateTime   string //更新时间
		Crdate       string //创建时间
		RoleNames    string //角色名称
		Memo         string //管理员备注
	}

	getInfo struct {
		database.Trans_base
		In  getInfo_in
		Out getInfo_out
	}
)

func NewGetInfo() *getInfo {
	return &getInfo{}
}

func (this *getInfo) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminUser_GetInfo")
	statement.AddParamter("@AdminUserID", database.AdParamInput, database.AdInteger, 4, this.In.AdminUserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	ret := retRows[0]
	this.Out.AdminUserID = int((*ret[0].(*interface{})).(int64))
	this.Out.IPAddress = (*ret[1].(*interface{})).(string)
	this.Out.IPAddress = ip.GetCountryAndRegion(this.Out.IPAddress, false)
	this.Out.FailureTimes = int((*ret[2].(*interface{})).(int64))
	this.Out.UpdateTime = (*ret[3].(*interface{})).(string)
	this.Out.Crdate = (*ret[4].(*interface{})).(string)
	this.Out.RoleNames = (*ret[5].(*interface{})).(string)
	this.Out.Memo = (*ret[6].(*interface{})).(string)
}

// 修改密码
type (
	updatePassword_in struct {
		AdminUserID  int    //管理用户ID
		OldPassword  string //旧密码(MD5加密)
		NewPassword  string //新密码(MD5加密)
		NewPassword2 string //新密码(MD5加密)
		IpAddress    string //IP地址
	}

	updatePassword_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	updatePassword struct {
		database.Trans_base
		In  updatePassword_in
		Out updatePassword_out
	}
)

func NewUpdatePassword() *updatePassword {
	return &updatePassword{}
}

func (this *updatePassword) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminUser_UpdatePassword")
	statement.AddParamter("@AdminUserID", database.AdParamInput, database.AdInteger, 4, this.In.AdminUserID)
	statement.AddParamter("@OldPassword", database.AdParamInput, database.AdVarChar, 32, this.In.OldPassword)
	statement.AddParamter("@NewPassword", database.AdParamInput, database.AdVarChar, 32, this.In.NewPassword)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	ret := retRows[0]
	this.Out.RetCode = int((*ret[0].(*interface{})).(int64))
}

// 校验页面
type (
	verifyPage_in struct {
		AdminUserID int //管理用户ID
		AdminPageID int //管理页面ID
	}

	verifyPage_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	verifyPage struct {
		database.Trans_base
		In  verifyPage_in
		Out verifyPage_out
	}
)

func NewVerifyPage() *verifyPage {
	return &verifyPage{}
}

func (this *verifyPage) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminUser_VerifyPage")
	statement.AddParamter("@AdminUserID", database.AdParamInput, database.AdInteger, 4, this.In.AdminUserID)
	statement.AddParamter("@AdminPageID", database.AdParamInput, database.AdInteger, 4, this.In.AdminPageID)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	ret := retRows[0]
	this.Out.RetCode = int((*ret[0].(*interface{})).(int64))
}

// 管理用户列表
type (
	userInfo struct {
		AdminUserID   int    //管理员ID
		AdminUserName string //管理员名称
		IPAddress     string //IP地址
		FailureTimes  int    //失败次数
		UpdateTime    string //更新时间
		Memo          string //备注
		Crdate        string //创建时间
	}

	userList_out struct {
		RecordCount int
		List        []userInfo
	}

	userList struct {
		database.Trans_base
		Out userList_out
	}
)

func NewUserList() *userList {
	return &userList{}
}

func (this *userList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminUser_GetList")
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]userInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.AdminUserID = int((*ret[0].(*interface{})).(int64))
		out.AdminUserName = (*ret[1].(*interface{})).(string)
		out.IPAddress = (*ret[2].(*interface{})).(string)
		out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
		out.FailureTimes = int((*ret[3].(*interface{})).(int64))
		out.UpdateTime = (*ret[4].(*interface{})).(string)
		out.Memo = (*ret[5].(*interface{})).(string)
		out.Crdate = (*ret[6].(*interface{})).(string)
	}
}

// 删除后台管理用户
type (
	userDel_in struct {
		OpUserID      int    //操作员ID
		OpUserName    string //操作员名称
		AdminUserName string //管理员名称
		IpAddress     string //IP地址
	}

	userDel_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	userDel struct {
		database.Trans_base
		In  userDel_in
		Out userDel_out
	}
)

func NewUserDel() *userDel {
	return &userDel{}
}

func (this *userDel) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminUser_Del")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@AdminUserName", database.AdParamInput, database.AdVarChar, 32, this.In.AdminUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 16, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	ret := retRows[0]
	this.Out.RetCode = int((*ret[0].(*interface{})).(int64))
}

// 添加备注
type (
	updateMemo_in struct {
		OpUserID      int    //操作员ID
		OpUserName    string //操作员名称
		AdminUserName string //管理员名称
		Memo          string //备注
		IpAddress     string //IP地址
	}

	updateMemo_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	updateMemo struct {
		database.Trans_base
		In  updateMemo_in
		Out updateMemo_out
	}
)

func NewUpdateMemo() *updateMemo {
	return &updateMemo{}
}

func (this *updateMemo) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminUser_UpdateMemo")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@AdminUserName", database.AdParamInput, database.AdVarChar, 32, this.In.AdminUserName)
	statement.AddParamter("@Memo", database.AdParamInput, database.AdNVarChar, 128, this.In.Memo)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
}

// 添加管理用户
type (
	userAdd_in struct {
		OpUserID      int    //操作员ID
		OpUserName    string //操作员名称
		AdminUserName string //管理员名称
		LoginPassword string //登陆密码
		IpAddress     string //IP地址
	}

	userAdd_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	userAdd struct {
		database.Trans_base
		In  userAdd_in
		Out userAdd_out
	}
)

func NewUserAdd() *userAdd {
	return &userAdd{}
}

func (this *userAdd) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminUser_Add")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@AdminUserName", database.AdParamInput, database.AdVarChar, 32, this.In.AdminUserName)
	statement.AddParamter("@LoginPassword", database.AdParamInput, database.AdVarChar, 128, this.In.LoginPassword)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 16, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
}

// 获取后台登录密码
type (
	getPassword_in struct {
		AdminUserName string //账号名称
		IpAddress     string //IP地址
	}

	getPassword_out struct {
		AdminUserID   int    //后台账号ID
		LoginPassword string //登录密码(MD5加密)
	}

	getPassword struct {
		database.Trans_base
		In  getPassword_in
		Out getPassword_out
	}
)

func NewGetPassword() *getPassword {
	return &getPassword{}
}

func (this *getPassword) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminUser_GetPassword")
	statement.AddParamter("@AdminUserName", database.AdParamInput, database.AdVarChar, 32, this.In.AdminUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@AdminUserID", database.AdParamOutput, database.AdInteger, 4, this.Out.AdminUserID)
	statement.AddParamter("@LoginPassword", database.AdParamOutput, database.AdVarChar, 32, this.Out.LoginPassword)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}
	ret := retRows[0]
	this.Out.AdminUserID = int((*ret[0].(*interface{})).(int64))
	this.Out.LoginPassword = (*ret[1].(*interface{})).(string)
}

// 获取奖池日志列表
type (
	getPrizePoolList_in struct {
		UserId    int // 用户id
		BeginTime int // 开始时间
		EndTime   int // 结束时间
		PageIndex int // 第几页
		PageSize  int // 请求的个数
	}

	getPrizePoolList_out struct {
		Data waterpool.GrantRecords
	}

	getPrizePoolList struct {
		database.Trans_base
		In  getPrizePoolList_in
		Out getPrizePoolList_out
	}
)

func NewGetPrizePoolList() *getPrizePoolList {
	return &getPrizePoolList{}
}

// 发放个人奖池
type (
	sendPrizePool_in struct {
		UserId  int    // 用户id
		Value   int    // 奖池数值
		GenType string // 操作原因
	}

	sendPrizePool_out struct {
		Data string // 返回的数据
	}

	sendPrizePool struct {
		database.Trans_base
		In  sendPrizePool_in
		Out sendPrizePool_out
	}
)

func NewSendPrizePool() *sendPrizePool {
	return &sendPrizePool{}
}
