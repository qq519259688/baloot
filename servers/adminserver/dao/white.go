package dao

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 白名单列表
type (
	whiteList_in struct {
		UserID    int
		PageIndex int
		PageSize  int
	}

	whiteInfo struct {
		UserID    int
		NickName  string
		WhiteType int
		Crdate    string
	}

	whiteList_out struct {
		RecordCount int
		List        []whiteInfo
	}

	whiteList struct {
		database.Trans_base
		In  whiteList_in
		Out whiteList_out
	}
)

func NewWhiteList() *whiteList {
	return &whiteList{}
}

func (this *whiteList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_WhiteList_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlString := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]whiteInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.UserID = int((*ret[0].(*interface{})).(int64))
			out.NickName = (*ret[1].(*interface{})).(string)
			out.WhiteType = int((*ret[2].(*interface{})).(int64))
			out.Crdate = (*ret[3].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 添加白名单
type (
	whiteAdd_in struct {
		OpUserID   int
		OpUserName string
		UserID     int
		WhiteType  int
		IpAddress  string
	}

	whiteAdd struct {
		database.Trans_base
		In whiteAdd_in
	}
)

func NewWhiteAdd() *whiteAdd {
	return &whiteAdd{}
}

func (this *whiteAdd) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_WhiteList_Add")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@WhiteType", database.AdParamInput, database.AdInteger, 4, this.In.WhiteType)
	statement.AddParamter("@IpAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	sqlString := statement.GenSql()
	CenterDB.ExecSql(sqlString)
}

// 删除白名单
type (
	whiteDel_in struct {
		OpUserID   int
		OpUserName string
		UserID     int
		WhiteType  int
		IpAddress  string
	}

	whiteDel struct {
		database.Trans_base
		In whiteDel_in
	}
)

func NewWhiteDel() *whiteDel {
	return &whiteDel{}
}

func (this *whiteDel) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_WhiteList_Del")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@WhiteType", database.AdParamInput, database.AdInteger, 4, this.In.WhiteType)
	statement.AddParamter("@IpAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	sqlString := statement.GenSql()
	CenterDB.ExecSql(sqlString)
}
