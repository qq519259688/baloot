package dao

import (
	"bet24.com/servers/adminserver/ip"
	"fmt"
	"runtime/debug"
	"strconv"

	"bet24.com/database"
	"bet24.com/log"
)

// 后台日志列表
type (
	logList_in struct {
		AdminUserID int    //管理员ID
		StartTime   string //开始时间
		EndTime     string //截止时间
		PageIndex   int    //页索引
		PageSize    int    //页大小
	}

	logInfo struct {
		AdminLogID    int    //日志ID
		AdminUserID   int    //管理员ID
		AdminUserName string //管理员名称
		IPAddress     string //IP地址
		Msg           string //消息
		Crdate        string //时间
	}

	logList_out struct {
		RecordCount int //记录数
		List        []logInfo
	}

	logList struct {
		database.Trans_base
		In  logList_in
		Out logList_out
	}
)

func NewLogList() *logList {
	return &logList{}
}

func (this *logList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminUser_GetLogList")
	statement.AddParamter("@AdminUserID", database.AdParamInput, database.AdInteger, 4, this.In.AdminUserID)
	statement.AddParamter("@StartTime", database.AdParamInput, database.AdVarChar, 20, this.In.StartTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]logInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.AdminLogID = int((*ret[0].(*interface{})).(int64))
			out.AdminUserID = int((*ret[1].(*interface{})).(int64))
			out.AdminUserName = (*ret[2].(*interface{})).(string)
			out.IPAddress = (*ret[3].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Msg = (*ret[4].(*interface{})).(string)
			out.Crdate = (*ret[5].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 系统日志列表
type (
	sysLogList_in struct {
		OpUserID  int    //管理员ID
		StartTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	sysLogInfo struct {
		SysLogID   int    //日志ID
		OpUserID   int    //管理员ID
		OpUserName string //管理员名称
		IPAddress  string //IP地址
		Msg        string //消息
		Crdate     string //时间
	}

	sysLogList_out struct {
		RecordCount int //记录数
		List        []sysLogInfo
	}

	sysLogList struct {
		database.Trans_base
		In  sysLogList_in
		Out sysLogList_out
	}
)

func NewSysLogList() *sysLogList {
	return &sysLogList{}
}

func (this *sysLogList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_SysLog_GetList")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@StartTime", database.AdParamInput, database.AdVarChar, 20, this.In.StartTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]sysLogInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.SysLogID = int((*ret[0].(*interface{})).(int64))
			out.OpUserID = int((*ret[1].(*interface{})).(int64))
			out.OpUserName = (*ret[2].(*interface{})).(string)
			out.IPAddress = (*ret[3].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Msg = (*ret[4].(*interface{})).(string)
			out.Crdate = (*ret[5].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 登录日志列表
type (
	loginLogList_in struct {
		UserID    int    //用户ID
		NickName  string //昵称
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	loginLogInfo struct {
		LogID       int    //标识
		UserID      int    //用户ID
		NickName    string //昵称
		IMei        string //IP地址
		PartnerName string //渠道
		Version     int    //版本号
		LoginType   string //登录方式
		IPAddress   string //IP地址
		Crdate      string //时间
		DeviceName  string // 设备名称
	}

	loginLogList_out struct {
		RecordCount int //记录数
		List        []loginLogInfo
	}

	loginLogList struct {
		database.Trans_base
		In  loginLogList_in
		Out loginLogList_out
	}
)

func NewLoginLogList() *loginLogList {
	return &loginLogList{}
}

func (this *loginLogList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserLoginLog_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, this.In.NickName)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]loginLogInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.LogID = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.IMei = (*ret[3].(*interface{})).(string)
			out.PartnerName = (*ret[4].(*interface{})).(string)
			out.Version = int((*ret[5].(*interface{})).(int64))
			out.LoginType = (*ret[6].(*interface{})).(string)
			out.IPAddress = (*ret[7].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[8].(*interface{})).(string)
			out.DeviceName = (*ret[9].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 注册日志列表
type (
	registerLogList_in struct {
		PartnerIDs string //合作商ID
		SourceIDs  string
		Lv         int
		Exp        int
		BeginTime  string //开始时间
		EndTime    string //截止时间
		PageIndex  int    //页索引
		PageSize   int    //页大小
	}

	registerLogInfo struct {
		RowNumber   int    //标识
		PartnerName string //合作商名称
		UserID      int    //用户ID
		NickName    string //昵称
		IMei        string //IP地址
		RegIP       string //注册IP
		RegTime     string //注册时间
		Deviceid    string //设备号
		Lv          int    // 等级
		Exp         int    // 经验值
		GuideStep   int    // 完成指引
		UTMSource   string // 流量渠道
		LoginTime   string // 登录时间
		Amount      int    // 金币
		DeviceName  string // 设备型号
		PayMoney    float64
	}

	registerLogList_out struct {
		RecordCount int //记录数
		List        []registerLogInfo
	}

	registerLogList struct {
		database.Trans_base
		In  registerLogList_in
		Out registerLogList_out
	}
)

func NewRegisterLogList() *registerLogList {
	return &registerLogList{}
}

func (this *registerLogList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetRegLogList")
	statement.AddParamter("@PartnerIDs", database.AdParamInput, database.AdVarChar, 256, this.In.PartnerIDs)
	statement.AddParamter("@SourceIDs", database.AdParamInput, database.AdVarChar, 256, this.In.SourceIDs)
	statement.AddParamter("@Lv", database.AdParamInput, database.AdInteger, 4, this.In.Lv)
	statement.AddParamter("@Exp", database.AdParamInput, database.AdInteger, 4, this.In.Exp)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]registerLogInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RowNumber = int((*ret[0].(*interface{})).(int64))
			out.PartnerName = (*ret[1].(*interface{})).(string)
			out.UserID = int((*ret[2].(*interface{})).(int64))
			out.NickName = (*ret[3].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.IMei = (*ret[4].(*interface{})).(string)
			out.RegIP = (*ret[5].(*interface{})).(string)
			out.RegIP = ip.GetCountryAndRegion(out.RegIP, true)
			out.RegTime = (*ret[6].(*interface{})).(string)
			out.Deviceid = (*ret[7].(*interface{})).(string)
			out.Lv = int((*ret[8].(*interface{})).(int64))
			out.Exp = int((*ret[9].(*interface{})).(int64))
			out.GuideStep = int((*ret[10].(*interface{})).(int64))
			out.UTMSource = (*ret[11].(*interface{})).(string)
			out.LoginTime = (*ret[12].(*interface{})).(string)
			out.Amount = int((*ret[13].(*interface{})).(int64))
			out.DeviceName = (*ret[14].(*interface{})).(string)
			payMoney := string((*ret[15].(*interface{})).([]byte))
			out.PayMoney, _ = strconv.ParseFloat(payMoney, 64)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 流水排行
type (
	dailyBetRank_in struct {
		BeginTime string //开始时间
		EndTime   string //截止时间
	}

	dailyBetRankInfo struct {
		Rid       int    //标识
		UserID    int    //用户ID
		NickName  string //昵称
		BetAmount string //流水
	}

	dailyBetRank_out struct {
		RecordCount int //记录数
		List        []dailyBetRankInfo
	}

	dailyBetRank struct {
		database.Trans_base
		In  dailyBetRank_in
		Out dailyBetRank_out
	}
)

func NewDailyBetRank() *dailyBetRank {
	return &dailyBetRank{}
}

func (this *dailyBetRank) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_DailyBet_RankList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]dailyBetRankInfo, rowLen)

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.Rid = int((*ret[0].(*interface{})).(int64))
		out.UserID = int((*ret[1].(*interface{})).(int64))
		out.NickName = (*ret[2].(*interface{})).(string)
		if info := tagMgr.getInfo(out.UserID); info != nil {
			out.NickName = info.NickName
		}
		out.BetAmount = string((*ret[3].(*interface{})).([]byte))
	}
}

// 玩家流水
type (
	dailyBetList_in struct {
		UserID    int    //用户ID
		BeginTime string //开始时间
		EndTime   string //截止时间
	}

	dailyBetInfo struct {
		DateFlag     string `json:"dateFlag"`     //日期
		UserID       int    `json:"userID"`       //用户ID
		ChineseName  string `json:"chineseName"`  //游戏名称
		BetAmount    string `json:"betAmount"`    //流水金额
		ResultAmount string `json:"resultAmount"` //结算金额
		WinAmount    string `json:"winAmount"`    //输赢金额
	}

	dailyBetList_out struct {
		RecordCount int //记录数
		List        []dailyBetInfo
	}

	dailyBetList struct {
		database.Trans_base
		In  dailyBetList_in
		Out dailyBetList_out
	}
)

func NewDailyBetList() *dailyBetList {
	return &dailyBetList{}
}

func (this *dailyBetList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_DailyBet_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]dailyBetInfo, rowLen)

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.UserID = int((*ret[1].(*interface{})).(int64))
		out.ChineseName = (*ret[2].(*interface{})).(string)
		out.BetAmount = string((*ret[3].(*interface{})).([]byte))
		out.ResultAmount = string((*ret[4].(*interface{})).([]byte))
		out.WinAmount = string((*ret[5].(*interface{})).([]byte))
	}
}

// 充值日志
type (
	payLog_in struct {
		PayID     int    //支付渠道ID
		UserID    int    //用户ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	payLogInfo struct {
		OrderID     string //订单
		PayChannel  string //支付渠道
		UserID      int    //用户ID
		NickName    string //昵称
		ProductID   string //产品ID
		ProductName string //产品名称
		Price       string //价格
		Crdate      string //时间
		IpAddress   string //IP地址
	}

	payLog_out struct {
		RecordCount int     //记录数
		TotalAmount float64 //充值总金币
		List        []payLogInfo
	}

	payLog struct {
		database.Trans_base
		In  payLog_in
		Out payLog_out
	}
)

func NewPayLog() *payLog {
	return &payLog{}
}

func (this *payLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Pay_GetLog")
	statement.AddParamter("@PayID", database.AdParamInput, database.AdInteger, 4, this.In.PayID)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalAmount", database.AdParamOutput, database.AdFloat, 20, this.Out.TotalAmount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true

	if rowLen > 1 {
		this.Out.List = make([]payLogInfo, rowLen-1)

		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.OrderID = (*ret[0].(*interface{})).(string)
			out.PayChannel = (*ret[1].(*interface{})).(string)
			out.UserID = int((*ret[2].(*interface{})).(int64))
			out.NickName = (*ret[3].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ProductID = (*ret[4].(*interface{})).(string)
			price := string((*ret[5].(*interface{})).([]byte))
			fPrice, _ := strconv.ParseFloat(price, 64)
			out.Price = fmt.Sprintf("%.2f", fPrice)
			out.Crdate = (*ret[6].(*interface{})).(string)
			out.IpAddress = (*ret[7].(*interface{})).(string)
			out.IpAddress = ip.GetCountryAndRegion(out.IpAddress, false)
			out.ProductName = out.ProductID
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))

	totalAmount := string((*retRows[rowLen-1][1].(*interface{})).([]byte))
	this.Out.TotalAmount, _ = strconv.ParseFloat(totalAmount, 64)
}

// 充值排行
type (
	payRank_in struct {
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	payRankInfo struct {
		Rid         int    //排序
		UserID      int    //用户ID
		NickName    string //昵称
		TotalAmount string //金额
	}

	payRank_out struct {
		RecordCount int //记录数
		List        []payRankInfo
	}

	payRank struct {
		database.Trans_base
		In  payRank_in
		Out payRank_out
	}
)

func NewPayRank() *payRank {
	return &payRank{}
}

func (this *payRank) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Pay_GetRank")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true

	if rowLen > 1 {
		this.Out.List = make([]payRankInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			totalAmount := string((*ret[3].(*interface{})).([]byte))
			fTotalAmount, _ := strconv.ParseFloat(totalAmount, 64)
			out.TotalAmount = fmt.Sprintf("%.2f", fTotalAmount)

		}
	}
	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 贈送日誌
type (
	sendLog_in struct {
		UserID    int    //用戶ID
		IpAddress string //IP地址
		BeginTime string //開始時間
		EndTime   string //截止時間
		PageIndex int    //頁索引
		PageSize  int    //頁大小
	}

	sendLogModel struct {
		Rid       int    //標識
		UserID    int    //用戶ID
		NickName  string //暱稱
		IpAddress string //IP
		Crdate    string //時間
	}

	sendLog_out struct {
		RecordCount int //記錄數
		List        []sendLogModel
	}

	sendLog struct {
		database.Trans_base
		In  sendLog_in
		Out sendLog_out
	}
)

func NewSendLog() *sendLog {
	return &sendLog{}
}

func (this *sendLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_UserSendLog_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowsLen := len(retRows)
	if rowsLen <= 0 {
		this.State = false
		return
	}
	this.State = true
	if rowsLen > 1 {
		this.Out.List = make([]sendLogModel, rowsLen-1)
		for i := 0; i < rowsLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.IpAddress = (*ret[3].(*interface{})).(string)
			out.IpAddress = ip.GetCountryAndRegion(out.IpAddress, false)
			out.Crdate = (*ret[4].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowsLen-1][0].(*interface{})).(int64))
}

// 充值日志
type (
	payChipLog_in struct {
		PayID     int    //支付渠道ID
		UserID    int    //用户ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	payChipLogInfo struct {
		OrderID     string //订单
		PayChannel  string //支付渠道
		UserID      int    //用户ID
		NickName    string //昵称
		ProductID   string //产品ID
		ProductName string //产品名称
		Price       int    //价格
		Crdate      string //时间
		IpAddress   string //IP地址
	}

	payChipLog_out struct {
		RecordCount int //记录数
		TotalAmount int //充值总金币
		List        []payChipLogInfo
	}

	payChipLog struct {
		database.Trans_base
		In  payChipLog_in
		Out payChipLog_out
	}
)

func NewPayChipLog() *payChipLog {
	return &payChipLog{}
}

func (this *payChipLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Pay_GetChipLog")
	statement.AddParamter("@PayID", database.AdParamInput, database.AdInteger, 4, this.In.PayID)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.TotalAmount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true

	if rowLen > 1 {
		this.Out.List = make([]payChipLogInfo, rowLen-1)

		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.OrderID = (*ret[0].(*interface{})).(string)
			out.PayChannel = (*ret[1].(*interface{})).(string)
			out.UserID = int((*ret[2].(*interface{})).(int64))
			out.NickName = (*ret[3].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ProductID = (*ret[4].(*interface{})).(string)
			out.Price = int((*ret[5].(*interface{})).(int64))
			out.Crdate = (*ret[6].(*interface{})).(string)
			out.IpAddress = (*ret[7].(*interface{})).(string)
			out.IpAddress = ip.GetCountryAndRegion(out.IpAddress, false)
			out.ProductName = out.ProductID
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.TotalAmount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
}

// 注册日志列表
type (
	registerLogList_v2_in struct {
		UTMSources string //渠道源ID
		Lv         int
		Exp        int
		BeginTime  string //开始时间
		EndTime    string //截止时间
		PageIndex  int    //页索引
		PageSize   int    //页大小
	}

	registerLogInfo_v2 struct {
		RowNumber   int    //标识
		PartnerName string //合作商名称
		UserID      int    //用户ID
		NickName    string //昵称
		IMei        string //IP地址
		RegIP       string //注册IP
		RegTime     string //注册时间
		Deviceid    string //设备号
		Lv          int    // 等级
		Exp         int    // 经验值
		GuideStep   int    // 完成指引
		UTMSource   string // 流量渠道
		DeviceName  string // 设备型号
		PayMoney    float64
		LoginTime   string // 登录时间
		Amount      int    // 金币
		Version     int    // 版本号
	}

	registerLogList_v2_out struct {
		RecordCount int //记录数
		List        []registerLogInfo_v2
	}

	registerLogList_v2 struct {
		database.Trans_base
		In  registerLogList_v2_in
		Out registerLogList_v2_out
	}
)

func NewRegisterLogList_v2() *registerLogList_v2 {
	return &registerLogList_v2{}
}

func (this *registerLogList_v2) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetRegLogList_V2")
	statement.AddParamter("@UTMSources", database.AdParamInput, database.AdVarChar, 256, this.In.UTMSources)
	statement.AddParamter("@Lv", database.AdParamInput, database.AdInteger, 4, this.In.Lv)
	statement.AddParamter("@Exp", database.AdParamInput, database.AdInteger, 4, this.In.Exp)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]registerLogInfo_v2, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RowNumber = int((*ret[0].(*interface{})).(int64))
			out.PartnerName = (*ret[1].(*interface{})).(string)
			out.UserID = int((*ret[2].(*interface{})).(int64))
			out.NickName = (*ret[3].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.IMei = (*ret[4].(*interface{})).(string)
			out.RegIP = (*ret[5].(*interface{})).(string)
			out.RegIP = ip.GetCountryAndRegion(out.RegIP, true)
			out.RegTime = (*ret[6].(*interface{})).(string)
			out.Deviceid = (*ret[7].(*interface{})).(string)
			out.Lv = int((*ret[8].(*interface{})).(int64))
			out.Exp = int((*ret[9].(*interface{})).(int64))
			out.GuideStep = int((*ret[10].(*interface{})).(int64))
			out.UTMSource = (*ret[11].(*interface{})).(string)
			out.DeviceName = (*ret[12].(*interface{})).(string)
			payMoney := string((*ret[13].(*interface{})).([]byte))
			out.PayMoney, _ = strconv.ParseFloat(payMoney, 64)
			out.LoginTime = (*ret[14].(*interface{})).(string)
			out.Amount = int((*ret[15].(*interface{})).(int64))
			out.Version = int((*ret[16].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}
