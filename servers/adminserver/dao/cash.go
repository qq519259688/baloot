package dao

import (
	"bet24.com/servers/adminserver/ip"
	"encoding/json"
	"runtime/debug"
	"sort"

	"bet24.com/database"
	"bet24.com/log"
)

// 金币日志
type (
	getCashLog_in struct {
		UserID     int    //用户ID
		PartnerID  int    //渠道ID
		SourceName string //应用名称
		BeginTime  string //开始时间
		EndTime    string //截止时间
		PageIndex  int    //页索引
		PageSize   int    //页大小
	}

	getCashLogInfo struct {
		LogID         int    //标识
		SourceName    string //源名称
		MoneyType     string //货币类型
		UserID        int    //用户ID
		NickName      string //昵称
		CurrentAmount int    //当前金额
		ModifyAmount  int    //修改金额
		StillAmount   int    //剩余金额
		Remark        string //备注
		IPAddress     string //IP地址
		Crdate        string //时间
		PartnerName   string //渠道名称
		TaxAmount     int    //台费
	}

	getCashLog_out struct {
		RecordCount int //记录数
		List        []getCashLogInfo
	}

	getCashLog struct {
		database.Trans_base
		In  getCashLog_in
		Out getCashLog_out
	}
)

func NewGetCashLog() *getCashLog {
	return &getCashLog{}
}

func (this *getCashLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Money_GetCashLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 32, this.In.SourceName)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]getCashLogInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.LogID = int((*ret[0].(*interface{})).(int64))
			out.SourceName = (*ret[1].(*interface{})).(string)
			out.MoneyType = (*ret[2].(*interface{})).(string)
			out.UserID = int((*ret[3].(*interface{})).(int64))
			out.NickName = (*ret[4].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}

			out.CurrentAmount = int((*ret[5].(*interface{})).(int64))
			out.ModifyAmount = int((*ret[6].(*interface{})).(int64))
			out.StillAmount = int((*ret[7].(*interface{})).(int64))

			out.Remark = (*ret[8].(*interface{})).(string)
			out.IPAddress = (*ret[9].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[10].(*interface{})).(string)
			out.PartnerName = (*ret[11].(*interface{})).(string)

			out.TaxAmount = int((*ret[12].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 钻石日志
type (
	getDiamondLog_in struct {
		UserID    int    //用户ID
		PartnerID int    //渠道ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	getDiamondLogInfo struct {
		LogID         int    //标识
		UserID        int    //用户ID
		NickName      string //昵称
		CurrentAmount int    //当前金额
		ModifyAmount  int    //修改金额
		StillAmount   int    //剩余金额
		Remark        string //备注
		IPAddress     string //IP地址
		Crdate        string //时间
		PartnerName   string //渠道名称
	}

	getDiamondLog_out struct {
		RecordCount int //记录数
		List        []getDiamondLogInfo
	}

	getDiamondLog struct {
		database.Trans_base
		In  getDiamondLog_in
		Out getDiamondLog_out
	}
)

func NewGetDiamondLog() *getDiamondLog {
	return &getDiamondLog{}
}

func (this *getDiamondLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Diamond_GetLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]getDiamondLogInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.LogID = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}

			out.CurrentAmount = int((*ret[3].(*interface{})).(int64))
			out.ModifyAmount = int((*ret[4].(*interface{})).(int64))
			out.StillAmount = int((*ret[5].(*interface{})).(int64))

			out.Remark = (*ret[6].(*interface{})).(string)
			out.IPAddress = (*ret[7].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[8].(*interface{})).(string)
			out.PartnerName = (*ret[9].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 投注日志
type (
	getBetLog_in struct {
		UserID    int    //用户ID
		GameID    int    //游戏ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	getBetLogInfo struct {
		RecordID     string //标识
		UserID       int    //用户ID
		NickName     string //昵称
		ChineseName  string //游戏名
		BetAmount    int    //下注金额
		BetTime      string //下注时间
		BetZone      string //下注区域
		Odds         string //赔率
		ResultAmount int    //结算金额
		ResultTime   string //结算时间
		ResultZone   string //结算区域
		WinAmount    int    //输赢金额
		StillAmount  int    //剩余金额
		Status       int    //状态 0=下注  1=结算
		Tax          int    //台费
		SubSeconds   int    // 时间差
	}

	getBetLog_out struct {
		RecordCount int //记录数
		WinAmount   int //输赢金额
		List        []getBetLogInfo
	}

	getBetLog struct {
		database.Trans_base
		In  getBetLog_in
		Out getBetLog_out
	}
)

func NewGetBetLog() *getBetLog {
	return &getBetLog{}
}

func (this *getBetLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Game_GetBetLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, this.In.GameID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@WinAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.WinAmount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]getBetLogInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RecordID = (*ret[0].(*interface{})).(string)
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ChineseName = (*ret[3].(*interface{})).(string)
			out.BetAmount = int((*ret[4].(*interface{})).(int64))
			out.BetTime = (*ret[5].(*interface{})).(string)
			out.BetZone = (*ret[6].(*interface{})).(string)
			out.Odds = string((*ret[7].(*interface{})).([]byte))
			out.ResultAmount = int((*ret[8].(*interface{})).(int64))
			out.ResultTime = (*ret[9].(*interface{})).(string)
			out.ResultZone = (*ret[10].(*interface{})).(string)
			out.WinAmount = int((*ret[11].(*interface{})).(int64))
			out.StillAmount = int((*ret[12].(*interface{})).(int64))
			out.Status = int((*ret[13].(*interface{})).(int64))
			out.Tax = int((*ret[14].(*interface{})).(int64))
			out.SubSeconds = int((*ret[15].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.WinAmount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
}

// 投注详情
type (
	getBetDetail_in struct {
		UserID    int    //用户ID
		RecordID  string //记录ID
		BeginTime string //开始时间
		EndTime   string //截止时间
	}

	recordInfo struct {
		FishID   int     // 鱼ID
		FishOdds int     // 鱼的倍率
		Catched  int     // 捕获数量
		Consume  int     // 消耗金币
		Return   int     // 返还金币
		Settle   int     //结算金币
		ROI      float64 //投入产出率
	}

	getBetDetail_out struct {
		RecordID     int
		TotalConsume int     //总消耗
		TotalReturn  int     //总返还
		TotalSettle  int     //总结算
		TotalROI     float64 //投入产出率
		List         []recordInfo
	}

	getBetDetail struct {
		database.Trans_base
		In  getBetDetail_in
		Out getBetDetail_out
	}
)

func NewGetBetDetail() *getBetDetail {
	return &getBetDetail{}
}

func (this *getBetDetail) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Game_GetBetDetail")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@RecordID", database.AdParamInput, database.AdVarChar, 32, this.In.RecordID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		//recordId := (*ret[0].(*interface{})).(string)
		resultZone := (*ret[1].(*interface{})).(string)

		var records [][]int
		if err := json.Unmarshal([]byte(resultZone), &records); err != nil {
			log.Error("getBetDetail resultZone unmarshal fail %v", err)
			return
		}

		for _, record := range records {
			var info recordInfo
			for idx, val := range record {
				switch idx {
				case 0: //鱼ID
					info.FishID = val
				case 1: //鱼的倍率
					info.FishOdds = val
				case 2: //捕获数量
					info.Catched = val
				case 3: //消耗金币
					info.Consume = val
					info.Settle += -info.Consume
				case 4: //返还金币
					info.Return = val
					info.Settle += info.Return
				}
			}

			this.Out.TotalConsume += info.Consume
			this.Out.TotalReturn += info.Return
			this.Out.TotalSettle += info.Settle

			info.ROI = float64(info.Return) / float64(info.Consume)

			//log.Debug("info ==> %+v", info)

			addFlag := true

			for idx, val := range this.Out.List {
				if val.FishID != info.FishID {
					continue
				}

				addFlag = false

				out := &this.Out.List[idx]

				out.Catched += info.Catched
				out.Consume += info.Consume
				out.Return += info.Return
				out.Settle += info.Settle
				out.ROI = float64(out.Return) / float64(out.Consume)
			}

			if !addFlag {
				continue
			}

			this.Out.List = append(this.Out.List, info)
		}
	}

	this.Out.TotalROI = float64(this.Out.TotalReturn) / float64(this.Out.TotalConsume)
	//sort.SliceStable(this.Out.List, func(i, j int) bool {
	//	return this.Out.List[i].Catched > this.Out.List[j].Catched
	//})

	sort.SliceStable(this.Out.List, func(i, j int) bool {
		return this.Out.List[i].ROI > this.Out.List[j].ROI
	})

	//log.Debug("getBetDetail this.Out.List %+v", this.Out.List)
}

// 加金
type (
	cashSend_in struct {
		OpUserID   int    //操作员ID
		OpUserName string //操作员名称
		UserID     int    //用户ID
		SourceName string //应用名称
		Reason     string //原因
		WantAmount int    //操作金额
		WantBank   int    //操作保险柜
		IpAddress  string //IP地址
	}

	cashSend_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	cashSend struct {
		database.Trans_base
		In  cashSend_in
		Out cashSend_out
	}
)

func NewCashSend() *cashSend {
	return &cashSend{}
}

func (this *cashSend) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("prMoney_WebSendCoin")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 32, this.In.SourceName)
	statement.AddParamter("@Reason", database.AdParamInput, database.AdNVarChar, 128, this.In.Reason)
	statement.AddParamter("@WantAmount", database.AdParamInput, database.AdInteger, 4, this.In.WantAmount)
	statement.AddParamter("@WantBank", database.AdParamInput, database.AdInteger, 4, this.In.WantBank)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	statement.AddParamter("@ErrorMsg", database.AdParamOutput, database.AdNVarChar, 128, this.Out.ErrorMsg)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
	this.Out.ErrorMsg = (*retRows[0][1].(*interface{})).(string)
}

// 扣金
type (
	cashDel_in struct {
		OpUserID   int    //操作员ID
		OpUserName string //操作员名称
		UserID     int    //用户ID
		SourceName string //应用名称
		Reason     string //原因
		WantAmount int    //操作金额
		WantBank   int    //操作保险柜
		IpAddress  string //IP地址
	}

	cashDel_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	cashDel struct {
		database.Trans_base
		In  cashDel_in
		Out cashDel_out
	}
)

func NewCashDel() *cashDel {
	return &cashDel{}
}

func (this *cashDel) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("prMoney_WebDelCoin_v2")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 32, this.In.SourceName)
	statement.AddParamter("@Reason", database.AdParamInput, database.AdNVarChar, 128, this.In.Reason)
	statement.AddParamter("@WantAmount", database.AdParamInput, database.AdBigint, 8, this.In.WantAmount)
	statement.AddParamter("@WantBank", database.AdParamInput, database.AdBigint, 8, this.In.WantBank)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	statement.AddParamter("@ErrorMsg", database.AdParamOutput, database.AdNVarChar, 128, this.Out.ErrorMsg)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
	this.Out.ErrorMsg = (*retRows[0][1].(*interface{})).(string)
}

// 金币场解锁
type (
	cashUnlock_in struct {
		OpUserID   int    //操作员ID
		OpUserName string //操作员名称
		UserID     int    //用户ID
		IpAddress  string //IP地址
	}

	cashUnlock_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	cashUnlock struct {
		database.Trans_base
		In  cashUnlock_in
		Out cashUnlock_out
	}
)

func NewCashUnlock() *cashUnlock {
	return &cashUnlock{}
}

func (this *cashUnlock) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_CasinoOnline_Unlock")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
}

// 金币锁信息
type (
	cashLockInfo_in struct {
		UserID   int    //用户ID
		NickName string //昵称
	}

	cashLockInfo_out struct {
		UserID     int    //用户ID
		NickName   string //昵称
		LoginTime  string //登陆时间
		ServerName string //服务器名称
	}

	cashLockInfo struct {
		database.Trans_base
		In  cashLockInfo_in
		Out cashLockInfo_out
	}
)

func NewCashLockInfo() *cashLockInfo {
	return &cashLockInfo{}
}

func (this *cashLockInfo) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_CasinoOnline_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, this.In.NickName)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	ret := retRows[0]
	this.Out.UserID = int((*ret[0].(*interface{})).(int64))
	this.Out.NickName = (*ret[1].(*interface{})).(string)
	if info := tagMgr.getInfo(this.Out.UserID); info != nil {
		this.Out.NickName = info.NickName
	}
	this.Out.LoginTime = (*ret[2].(*interface{})).(string)
	this.Out.ServerName = (*ret[3].(*interface{})).(string)
}

// 货币统计
type (
	moneyStat_in struct {
		BeginTime string //开始时间
		EndTime   string //截止时间
	}

	moneyStatInfo struct {
		LogType     int    //日志类型
		LogTypeName string //日志类型名称
		GameID      int    //游戏ID
		ChineseName string //游戏名
		Amount      int    //金额
		SendAmount  int    //发行金额
		DelAmount   int    //消耗金额
		DiffAmount  int    //差额
	}

	moneyStat_out struct {
		Send    int //总发行
		Consume int //总消耗

		GameSend    int //游戏发行
		GameConsume int //游戏消耗
		GameDiff    int //游戏差额

		Cash            int //金币总额
		Bank            int //保险柜总额
		YesterdayAmount int //前日总额
		CurrAmount      int //当前总额

		List []moneyStatInfo
	}

	moneyStat struct {
		database.Trans_base
		In  moneyStat_in
		Out moneyStat_out
	}
)

func NewMoneyStat() *moneyStat {
	return &moneyStat{}
}

func (this *moneyStat) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MoneyStat_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)

	statement.AddParamter("@Send", database.AdParamOutput, database.AdBigint, 8, this.Out.Send)
	statement.AddParamter("@Consume", database.AdParamOutput, database.AdBigint, 8, this.Out.Consume)

	statement.AddParamter("@GameSend", database.AdParamOutput, database.AdBigint, 8, this.Out.GameSend)
	statement.AddParamter("@GameConsume", database.AdParamOutput, database.AdBigint, 8, this.Out.GameConsume)
	statement.AddParamter("@GameDiff", database.AdParamOutput, database.AdBigint, 8, this.Out.GameDiff)

	statement.AddParamter("@Cash", database.AdParamOutput, database.AdBigint, 8, this.Out.Cash)
	statement.AddParamter("@Bank", database.AdParamOutput, database.AdBigint, 8, this.Out.Bank)
	statement.AddParamter("@YesterdayAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.YesterdayAmount)
	statement.AddParamter("@CurrAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.CurrAmount)

	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]moneyStatInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.LogType = int((*ret[0].(*interface{})).(int64))
			out.LogTypeName = (*ret[1].(*interface{})).(string)
			out.GameID = int((*ret[2].(*interface{})).(int64))
			out.Amount = int((*ret[3].(*interface{})).(int64))
			out.ChineseName = (*ret[4].(*interface{})).(string)
			out.SendAmount = int((*ret[5].(*interface{})).(int64))
			out.DelAmount = int((*ret[6].(*interface{})).(int64))
			out.DiffAmount = int((*ret[7].(*interface{})).(int64))
		}
	}

	ret := retRows[rowLen-1]

	this.Out.Send = int((*ret[0].(*interface{})).(int64))
	this.Out.Consume = int((*ret[1].(*interface{})).(int64))
	this.Out.GameSend = int((*ret[2].(*interface{})).(int64))
	this.Out.GameConsume = int((*ret[3].(*interface{})).(int64))
	this.Out.GameDiff = int((*ret[4].(*interface{})).(int64))
	this.Out.Cash = int((*ret[5].(*interface{})).(int64))
	this.Out.Bank = int((*ret[6].(*interface{})).(int64))
	this.Out.YesterdayAmount = int((*ret[7].(*interface{})).(int64))
	this.Out.CurrAmount = int((*ret[8].(*interface{})).(int64))
}

// 后台扣减钻石
type (
	diamondWebReduce_in struct {
		OpUserID   int    //操作员ID
		OpUserName string //操作员名称
		UserID     int    //用户ID
		WantAmount int    //钻石数
		Memo       string //描述
		IpAddress  string //IP地址
	}

	diamondWebReduce_out struct {
		RetCode       int    //操作结果
		ErrorMsg      string //操作描述
		DiamondAmount int    //钻石数
	}

	diamondWebReduce struct {
		database.Trans_base
		In  diamondWebReduce_in
		Out diamondWebReduce_out
	}
)

func NewDiamondWebReduce() *diamondWebReduce {
	return &diamondWebReduce{}
}

func (this *diamondWebReduce) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Diamond_WebReduce")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@WantAmount", database.AdParamInput, database.AdInteger, 4, this.In.WantAmount)
	statement.AddParamter("@Memo", database.AdParamInput, database.AdVarChar, 128, this.In.Memo)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@DiamondAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.DiamondAmount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.Out.RetCode = 11
		this.Out.ErrorMsg = "操作失败"
		return
	}

	this.Out.DiamondAmount = int((*retRows[0][0].(*interface{})).(int64))
	this.Out.RetCode = 1
	this.Out.ErrorMsg = "操作成功"
}

// 后台钻石日志
type (
	getDiamondAdminLog_in struct {
		UserID    int    //用户ID
		PartnerID int    //渠道ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	getDiamondAdminLogInfo struct {
		LogID         int    //标识
		UserID        int    //用户ID
		NickName      string //昵称
		CurrentAmount int    //当前金额
		ModifyAmount  int    //修改金额
		StillAmount   int    //剩余金额
		Remark        string //备注
		IPAddress     string //IP地址
		Crdate        string //时间
	}

	getDiamondAdminLog_out struct {
		RecordCount int //记录数
		List        []getDiamondAdminLogInfo
	}

	getDiamondAdminLog struct {
		database.Trans_base
		In  getDiamondAdminLog_in
		Out getDiamondAdminLog_out
	}
)

func NewGetDiamondAdminLog() *getDiamondAdminLog {
	return &getDiamondAdminLog{}
}

func (this *getDiamondAdminLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Diamond_GetAdminLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]getDiamondAdminLogInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.LogID = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}

			out.CurrentAmount = int((*ret[3].(*interface{})).(int64))
			out.ModifyAmount = int((*ret[4].(*interface{})).(int64))
			out.StillAmount = int((*ret[5].(*interface{})).(int64))

			out.Remark = (*ret[6].(*interface{})).(string)
			out.IPAddress = (*ret[7].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[8].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 游戏金币流量统计
type (
	moneyFlowStat_in struct {
		GameID    int    //游戏ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	moneyFlowStatModel struct {
		DateFlag    string //时间
		ChineseName string //游戏名称
		Amount      int    //金币
		Tax         int    //台费
	}

	moneyFlowStat_out struct {
		RecordCount int //记录数
		List        []moneyFlowStatModel
	}

	moneyFlowStat struct {
		database.Trans_base
		In  moneyFlowStat_in
		Out moneyFlowStat_out
	}
)

func NewMoneyFlowStat() *moneyFlowStat {
	return &moneyFlowStat{}
}

func (this *moneyFlowStat) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MoneyFlow_GetList")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, this.In.GameID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]moneyFlowStatModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.ChineseName = (*ret[1].(*interface{})).(string)
			out.Amount = int((*ret[2].(*interface{})).(int64))
			out.Tax = int((*ret[3].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 赠送日志
func (this *transferLog) GetCashTransferLog() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Cash_GetTransferLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@OutTotalAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.OutTotalAmount)
	statement.AddParamter("@InTotalAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.InTotalAmount)
	statement.AddParamter("@TotalTaxAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.TotalTaxAmount)
	statement.AddParamter("@AcceptUserID", database.AdParamInput, database.AdInteger, 4, this.In.AcceptUserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]transferLogModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.CurrAmount = int((*ret[3].(*interface{})).(int64))
			out.ModifyAmount = int((*ret[4].(*interface{})).(int64))
			out.StillAmount = int((*ret[5].(*interface{})).(int64))
			out.Action = (*ret[6].(*interface{})).(string)
			out.AcceptUserID = int((*ret[7].(*interface{})).(int64))
			out.AcceptNickName = (*ret[8].(*interface{})).(string)
			out.AcceptCurrAmount = int((*ret[9].(*interface{})).(int64))
			out.AcceptModifyAmount = int((*ret[10].(*interface{})).(int64))
			out.AcceptStillAmount = int((*ret[11].(*interface{})).(int64))
			out.TaxAmount = int((*ret[12].(*interface{})).(int64))
			out.Remark = (*ret[13].(*interface{})).(string)
			out.IPAddress = (*ret[14].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[15].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.OutTotalAmount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
	this.Out.InTotalAmount = int((*retRows[rowLen-1][2].(*interface{})).(int64))
	this.Out.TotalTaxAmount = int((*retRows[rowLen-1][3].(*interface{})).(int64))
}
