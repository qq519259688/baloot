package dao

import (
	"bet24.com/servers/adminserver/ip"
	"runtime/debug"
	"strconv"

	"bet24.com/database"
	"bet24.com/log"
)

// 用户道具日志
type (
	userItemLog_in struct {
		UserID    int    //用户ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
		ItemID    int    // 道具ID
	}

	userItemLogModel struct {
		LogID      int    //日志ID
		UserID     int    //用户ID
		NickName   string //昵称
		ItemID     int    //道具ID
		ItemName   string //道具名称
		CurrCount  int    //当前数量
		WantCount  int    //修改数量
		StillCount int    //剩余数量
		Remark     string //备注
		IPAddress  string //IP地址
		Crdate     string //时间
	}

	userItemLog_out struct {
		RecordCount int
		List        []userItemLogModel
	}

	userItemLog struct {
		database.Trans_base
		In  userItemLog_in
		Out userItemLog_out
	}
)

func NewUserItemLog() *userItemLog {
	return &userItemLog{}
}

func (this *userItemLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserItem_GetLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@ItemID", database.AdParamInput, database.AdInteger, 4, this.In.ItemID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]userItemLogModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.LogID = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ItemID = int((*ret[3].(*interface{})).(int64))
			out.CurrCount = int((*ret[4].(*interface{})).(int64))
			out.WantCount = int((*ret[5].(*interface{})).(int64))
			out.StillCount = int((*ret[6].(*interface{})).(int64))
			out.Remark = (*ret[7].(*interface{})).(string)
			out.IPAddress = (*ret[8].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[9].(*interface{})).(string)
			out.ItemName = (*ret[10].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 用户道具列表
type (
	userItemList_in struct {
		UserID int //用户ID
	}

	userItemListModel struct {
		UserItemID int    //用户道具ID
		ItemID     int    //道具ID
		ItemName   string //道具名称
		ItemCount  int    //道具数
		Start      string //开始时间
		Duration   int    //持续时间(叠加)
		EnegyPoint int    //道具能量
	}

	userItemList_out struct {
		RecordCount int
		List        []userItemListModel
	}

	userItemList struct {
		database.Trans_base
		In  userItemList_in
		Out userItemList_out
	}
)

func NewUserItemList() *userItemList {
	return &userItemList{}
}

func (this *userItemList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserItem_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
	this.State = true
	this.Out.List = make([]userItemListModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.UserItemID = int((*ret[0].(*interface{})).(int64))
		out.ItemID = int((*ret[1].(*interface{})).(int64))
		out.ItemCount = int((*ret[2].(*interface{})).(int64))
		out.Start = (*ret[3].(*interface{})).(string)
		out.Duration = int((*ret[4].(*interface{})).(int64))
		out.EnegyPoint = int((*ret[5].(*interface{})).(int64))
		out.ItemName = strconv.Itoa(out.ItemID)
	}
	this.Out.RecordCount = rowLen
}
