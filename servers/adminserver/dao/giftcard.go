package dao

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	"runtime/debug"
)

// 礼品卡列表
type (
	giftCardList_in struct {
		UserID    int    //用户ID
		Status    int    // 状态
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	giftCardListModel struct {
		Rid           int    // 标识
		UserID        int    // 用户ID
		NickName      string // 昵称
		ItemID        int    // 道具id
		ItemName      string // 道具名称
		ItemDesc      string // 道具描述
		Key           string // 兑换码
		ActivateTime  string // 激活时间
		OperateStatus int    // 操作状态
		Crdate        string // 创建时间
	}

	giftCardList_out struct {
		RecordCount int
		List        []giftCardListModel
	}

	giftCardList struct {
		database.Trans_base
		In  giftCardList_in
		Out giftCardList_out
	}
)

func NewGiftCardList() *giftCardList {
	return &giftCardList{}
}

func (this *giftCardList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_GiftCard_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@OperateStatus", database.AdParamInput, database.AdInteger, 4, this.In.Status)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlString := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]giftCardListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			out.ItemID = int((*ret[3].(*interface{})).(int64))
			out.Key = (*ret[4].(*interface{})).(string)
			activateTime := (*ret[5].(*interface{})).(int64)
			if activateTime > 0 {
				out.ActivateTime = common.TimeStampToString(activateTime)
			} else {
				out.ActivateTime = "<font color=\"#ccc\">未激活</font>"
			}
			out.OperateStatus = int((*ret[6].(*interface{})).(int64))
			out.Crdate = (*ret[7].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 礼品卡处理
type (
	giftCardDeal_in struct {
		OpUserID   int    // 操作员ID
		OpUserName string // 操作员名称
		Rid        int    // 标识
		Status     int    // 操作状态(0=待处理  1=已兑换   2=不予兑换)
		IpAddress  string // IP地址
	}

	giftCardDeal_out struct {
		RetCode int
	}

	giftCardDeal struct {
		database.Trans_base
		In  giftCardDeal_in
		Out giftCardDeal_out
	}
)

func NewGiftCardDeal() *giftCardDeal {
	return &giftCardDeal{}
}

func (this *giftCardDeal) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_GiftCard_Deal")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 20, this.In.OpUserName)
	statement.AddParamter("@Rid", database.AdParamInput, database.AdInteger, 4, this.In.Rid)
	statement.AddParamter("@OperateStatus", database.AdParamInput, database.AdInteger, 4, this.In.Status)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	sqlString := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
}
