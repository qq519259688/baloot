package dao

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

//渠道列表
type (
	PartnerInfo struct {
		PartnerID    int    //合作商ID
		PartnerName  string //合作商名称
		PrePartnerID int    //父合作商ID
		EnglishName  string //英文名
		ChannelLink  string //渠道链接
	}

	partnerList_out struct {
		RecordCount int //记录数
		List        []PartnerInfo
	}

	partnerList struct {
		database.Trans_base
		Out partnerList_out
	}
)

func NewPartnerList() *partnerList {
	return &partnerList{}
}

func (this *partnerList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Partner_GetList")
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]PartnerInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.PartnerID = int((*ret[0].(*interface{})).(int64))
		out.PartnerName = (*ret[1].(*interface{})).(string)
		out.PrePartnerID = int((*ret[2].(*interface{})).(int64))
		out.EnglishName = (*ret[3].(*interface{})).(string)
	}
}

//添加渠道
type (
	partnerAdd_in struct {
		OpUserID     int    //操作员ID
		OpUserName   string //操作员名称
		PartnerID    int    //渠道ID
		PartnerName  string //渠道名称
		PrePartnerID int    //父渠道ID
		IpAddress    string //IP地址
		EnglishName  string //游戏英文名
	}

	partnerAdd_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	partnerAdd struct {
		database.Trans_base
		In  partnerAdd_in
		Out partnerAdd_out
	}
)

func NewPartnerAdd() *partnerAdd {
	return &partnerAdd{}
}

func (this *partnerAdd) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Partner_Add")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@PartnerName", database.AdParamInput, database.AdNVarChar, 32, this.In.PartnerName)
	statement.AddParamter("@PrePartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PrePartnerID)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@EnglishName", database.AdParamInput, database.AdVarChar, 16, this.In.EnglishName)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
}

//删除渠道
type (
	partnerDel_in struct {
		OpUserID   int    //操作员ID
		OpUserName string //操作员名称
		PartnerID  int    //渠道ID
		IpAddress  string //IP地址
	}

	partnerDel_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	partnerDel struct {
		database.Trans_base
		In  partnerDel_in
		Out partnerDel_out
	}
)

func NewPartnerDel() *partnerDel {
	return &partnerDel{}
}

func (this *partnerDel) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Partner_Del")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 20, this.In.PartnerID)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
}
