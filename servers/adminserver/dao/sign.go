package dao

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

//签到列表
type (
	signList_in struct {
		UserID    int    //用户ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	signListModel struct {
		PartnerName string //渠道名称
		UserID      int    //用户ID
		NickName    string //昵称
		SignID      int    //签到ID
		SignTime    string //签到时间
		Day         int    //连签天数
		DaySet      string //已领奖励
	}

	signList_out struct {
		RecordCount int
		List        []signListModel
	}

	signList struct {
		database.Trans_base
		In  signList_in
		Out signList_out
	}
)

func NewSignList() *signList {
	return &signList{}
}

func (this *signList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserSign_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]signListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.UserID = int((*ret[0].(*interface{})).(int64))
			out.NickName = (*ret[1].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.SignID = int((*ret[2].(*interface{})).(int64))
			out.SignTime = (*ret[3].(*interface{})).(string)
			out.Day = int((*ret[4].(*interface{})).(int64))
			out.DaySet = (*ret[5].(*interface{})).(string)
			out.PartnerName = (*ret[6].(*interface{})).(string)
		}
	}
	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}
