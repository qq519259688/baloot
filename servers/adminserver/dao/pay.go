package dao

import (
	"bet24.com/servers/adminserver/ip"
	"fmt"
	"runtime/debug"
	"strconv"

	"bet24.com/database"
	"bet24.com/log"
)

const (
	ORDER_WEIXIN   = "Manage_WeixinOrder_GetList"   // 微信订单列表
	ORDER_OPPO     = "Manage_OppoOrder_GetList"     // Oppo订单列表
	ORDER_QIHOO    = "Manage_QihooOrder_GetList"    // 360订单列表
	ORDER_VIVO     = "Manage_VivoOrder_GetList"     // Vivo订单列表
	ORDER_VMALL    = "Manage_VMallOrder_GetList"    // 华为订单生成
	ORDER_CROPAY   = "Manage_CroOrder_GetList"      // Cro訂單列表
	ORDER_LIU      = "Manage_LiuOrder_GetList"      // Liu訂單列表
	ORDER_KAYA     = "Manage_KayaOrder_GetList"     // Kaya訂單列表
	ORDER_FLASH    = "Manage_FlashOrder_GetList"    // Flash訂單列表
	ORDER_DINGPEI  = "Manage_DingpeiOrder_GetList"  // Dingpei訂單列表
	ORDER_CRUSH    = "Manage_CrushOrder_GetList"    // Crush訂單列表
	ORDER_PAYERMAX = "Manage_PayerMaxOrder_GetList" // PayerMax訂單列表
	ORDER_OPAY     = "Manage_OOrder_GetList"        // OPay訂單列表
)

// 订单列表
type (
	orderList_in struct {
		UserID     int    //用户id
		BeginTime  string //开始时间
		EndTime    string //截止时间
		PageIndex  int    //页索引
		PageSize   int    //页大小
		PayChannel string //充值渠道
	}

	orderInfo struct {
		OrderID     string //订单号
		TradeID     string //流水号
		UserID      int    //用户ID
		NickName    string //昵称
		ProductID   string //产品ID
		ProductName string //产品名称
		Price       string //价格
		GetStatus   int    //状态
		PartnerName string //渠道名称
		IPAddress   string //IP地址
		Crdate      string //时间
	}

	orderList_out struct {
		RecordCount int     //记录数
		TotalPrice  float64 //总金额
		UserCount   int     //用户数
		List        []orderInfo
	}

	orderList struct {
		database.Trans_base
		In  orderList_in
		Out orderList_out
	}
)

func NewOrderList() *orderList {
	return &orderList{}
}

func (this *orderList) getProcName() string {
	switch this.In.PayChannel {
	case "weixin":
		return ORDER_WEIXIN
	case "oppo":
		return ORDER_OPPO
	case "qihoo":
		return ORDER_QIHOO
	case "vivo":
		return ORDER_VIVO
	case "vmall":
		return ORDER_VMALL
	case "cropay":
		return ORDER_CROPAY
	case "liupay":
		return ORDER_LIU
	case "kayapay":
		return ORDER_KAYA
	case "flashpay":
		return ORDER_FLASH
	case "dingpeipay":
		return ORDER_DINGPEI
	case "crushPay":
		return ORDER_CROPAY
	case "payerMax":
		return ORDER_PAYERMAX
	case "oPay":
		return ORDER_OPAY
	}
	return ""
}

func (this *orderList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	procName := this.getProcName()
	if len(procName) <= 0 {
		log.Error("orderList procName %s is not exist", procName)
		return
	}

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName(procName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalPrice", database.AdParamOutput, database.AdFloat, 20, this.Out.TotalPrice)
	statement.AddParamter("@UserCount", database.AdParamOutput, database.AdInteger, 4, this.Out.UserCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]orderInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.OrderID = (*ret[0].(*interface{})).(string)
			out.TradeID = (*ret[1].(*interface{})).(string)
			out.UserID = int((*ret[2].(*interface{})).(int64))
			out.NickName = (*ret[3].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ProductID = (*ret[4].(*interface{})).(string)

			price := string((*ret[5].(*interface{})).([]byte))
			fPrice, _ := strconv.ParseFloat(price, 64)
			out.Price = fmt.Sprintf("%.2f", fPrice)

			out.GetStatus = int((*ret[6].(*interface{})).(int64))
			out.PartnerName = (*ret[7].(*interface{})).(string)
			out.IPAddress = (*ret[8].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[9].(*interface{})).(string)
			out.ProductName = out.ProductID
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))

	totalPrice := string((*retRows[rowLen-1][1].(*interface{})).([]byte))
	this.Out.TotalPrice, _ = strconv.ParseFloat(totalPrice, 64)

	this.Out.UserCount = int((*retRows[rowLen-1][2].(*interface{})).(int64))
}

// 获取各渠道充值列表
type (
	payList_in struct {
		PayID     int    //充值渠道ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	payListModel struct {
		PayName      string //充值渠道
		DateFlag     string //日期标志
		RMBMoney     string //充值总额
		PayUserCount int    //充值用户数(除去重复的)
		ARPU         string //ARPU
	}

	payList_out struct {
		RecordCount   int     //记录数
		TotalRMBMoney float64 //成功充值数
		UserCount     int     //充值用户数
		CurrRMBMoney  float64 //今天充值总额
		List          []payListModel
	}

	payList struct {
		database.Trans_base
		In  payList_in
		Out payList_out
	}
)

func NewPayList() *payList {
	return &payList{}
}

func (this *payList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Pay_GetList")
	statement.AddParamter("@PayID", database.AdParamInput, database.AdInteger, 4, this.In.PayID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalRMBMoney", database.AdParamOutput, database.AdFloat, 20, this.Out.TotalRMBMoney)
	statement.AddParamter("@UserCount", database.AdParamOutput, database.AdInteger, 4, this.Out.UserCount)
	statement.AddParamter("@CurrRMBMoney", database.AdParamOutput, database.AdFloat, 20, this.Out.CurrRMBMoney)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]payListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.PayName = (*ret[0].(*interface{})).(string)
			out.DateFlag = (*ret[1].(*interface{})).(string)

			rmbMoney := string((*ret[2].(*interface{})).([]byte))
			fRMBMoney, _ := strconv.ParseFloat(rmbMoney, 64)
			out.RMBMoney = fmt.Sprintf("%.2f", fRMBMoney)

			out.PayUserCount = int((*ret[3].(*interface{})).(int64))
			ARPURet := string((*ret[4].(*interface{})).([]byte))
			fARPU, _ := strconv.ParseFloat(ARPURet, 64)
			out.ARPU = fmt.Sprintf("%.2f", fARPU)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))

	totalRMBMoney := string((*retRows[rowLen-1][1].(*interface{})).([]byte))
	this.Out.TotalRMBMoney, _ = strconv.ParseFloat(totalRMBMoney, 64)

	this.Out.UserCount = int((*retRows[rowLen-1][2].(*interface{})).(int64))

	currRMBMoney := string((*retRows[rowLen-1][3].(*interface{})).([]byte))
	this.Out.CurrRMBMoney, _ = strconv.ParseFloat(currRMBMoney, 64)
}

// 充值统计列表
type (
	payStatList_in struct {
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	payStatListModel struct {
		DateFlag      string //日期标识
		RMBMoney      string //充值金额
		UserCount     int    //充值用户数(除去重复的)
		LastRMBMoney  string //昨天充值金额
		LastUserCount int    //昨天充值用户数(除去重复的)
	}

	payStatList_out struct {
		RecordCount int //记录数
		List        []payStatListModel
	}

	payStatList struct {
		database.Trans_base
		In  payStatList_in
		Out payStatList_out
	}
)

func NewPayStatList() *payStatList {
	return &payStatList{}
}

func (this *payStatList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaciton recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_PayStat_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]payStatListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]
			out.DateFlag = (*ret[0].(*interface{})).(string)

			rmbMoney := string((*ret[1].(*interface{})).([]byte))
			fRMBMoney, _ := strconv.ParseFloat(rmbMoney, 64)
			out.RMBMoney = fmt.Sprintf("%.2f", fRMBMoney)

			out.UserCount = int((*ret[2].(*interface{})).(int64))

			lastRMBMoney := string((*ret[3].(*interface{})).([]byte))
			fLastRMBMoney, _ := strconv.ParseFloat(lastRMBMoney, 64)
			out.LastRMBMoney = fmt.Sprintf("%.2f", fLastRMBMoney)

			out.LastUserCount = int((*ret[4].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 登录渠道充值统计
type (
	payListByPartner_in struct {
		PartnerID int    //渠道ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	payListByPartnerModel struct {
		DateFlag       string //日期标识
		RMBMoney       string //充值金额
		TotalUserCount int    //充值用户数(除去重复的)
		ARPU           string //arpu
		PartnerName    string //渠道名称
	}

	payListByPartner_out struct {
		RecordCount   int     //记录数
		TotalRMBMoney float64 //成功充值数
		UserCount     int     //充值用户数
		CurrRMBMoney  float64 //今天充值总额
		List          []payListByPartnerModel
	}

	payListByPartner struct {
		database.Trans_base
		In  payListByPartner_in
		Out payListByPartner_out
	}
)

func NewPayListByPartner() *payListByPartner {
	return &payListByPartner{}
}

func (this *payListByPartner) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaciton recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Pay_GetChannelList")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalRMBMoney", database.AdParamOutput, database.AdFloat, 20, this.Out.TotalRMBMoney)
	statement.AddParamter("@UserCount", database.AdParamOutput, database.AdInteger, 4, this.Out.UserCount)
	statement.AddParamter("@CurrRMBMoney", database.AdParamOutput, database.AdFloat, 20, this.Out.CurrRMBMoney)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]payListByPartnerModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			rmbMoneyRet := string((*ret[1].(*interface{})).([]byte))
			fRMBMoney, _ := strconv.ParseFloat(rmbMoneyRet, 64)
			out.RMBMoney = fmt.Sprintf("%.2f", fRMBMoney)
			out.TotalUserCount = int((*ret[2].(*interface{})).(int64))
			ARPURet := string((*ret[3].(*interface{})).([]byte))
			fARPU, _ := strconv.ParseFloat(ARPURet, 64)
			out.ARPU = fmt.Sprintf("%.2f", fARPU)
			out.PartnerName = (*ret[4].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))

	totalRMBMoneyRet := string((*retRows[rowLen-1][1].(*interface{})).([]byte))
	this.Out.TotalRMBMoney, _ = strconv.ParseFloat(totalRMBMoneyRet, 64)

	this.Out.UserCount = int((*retRows[rowLen-1][2].(*interface{})).(int64))

	currRMBMoneyRet := string((*retRows[rowLen-1][3].(*interface{})).([]byte))
	this.Out.CurrRMBMoney, _ = strconv.ParseFloat(currRMBMoneyRet, 64)
}

// 日充值总额
type (
	payListByDay_in struct {
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	payListByDayModel struct {
		DateFlag     string //日期标志
		RMBMoney     string //充值总额
		PayUserCount int    //充值用户数(除去重复的)
		ARPU         string //ARPU值
	}

	payListByDay_out struct {
		RecordCount   int     //记录数
		TotalRMBMoney float64 //成功充值数
		UserCount     int     //充值用户数
		CurrRMBMoney  float64 //今天充值总额
		List          []payListByDayModel
	}

	payListByDay struct {
		database.Trans_base
		In  payListByDay_in
		Out payListByDay_out
	}
)

func NewPayListByDay() *payListByDay {
	return &payListByDay{}
}

func (this *payListByDay) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Pay_GetListByDay")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalRMBMoney", database.AdParamOutput, database.AdFloat, 20, this.Out.TotalRMBMoney)
	statement.AddParamter("@UserCount", database.AdParamOutput, database.AdInteger, 4, this.Out.UserCount)
	statement.AddParamter("@CurrRMBMoney", database.AdParamOutput, database.AdFloat, 20, this.Out.CurrRMBMoney)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]payListByDayModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)

			rmbMoney := string((*ret[1].(*interface{})).([]byte))
			fRMBMoney, _ := strconv.ParseFloat(rmbMoney, 64)
			out.RMBMoney = fmt.Sprintf("%.2f", fRMBMoney)

			out.PayUserCount = int((*ret[2].(*interface{})).(int64))
			ARPURet := string((*ret[3].(*interface{})).([]byte))
			fARPU, _ := strconv.ParseFloat(ARPURet, 64)
			out.ARPU = fmt.Sprintf("%.2f", fARPU)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))

	totalRMBMoney := string((*retRows[rowLen-1][1].(*interface{})).([]byte))
	this.Out.TotalRMBMoney, _ = strconv.ParseFloat(totalRMBMoney, 64)

	this.Out.UserCount = int((*retRows[rowLen-1][2].(*interface{})).(int64))

	currRMBMoney := string((*retRows[rowLen-1][3].(*interface{})).([]byte))
	this.Out.CurrRMBMoney, _ = strconv.ParseFloat(currRMBMoney, 64)
}

// 苹果充值记录
type (
	appleLog_in struct {
		UserID    int    //用户ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	appleLogModel struct {
		UserID        int    //用户ID
		NickName      string //昵称
		ReceiptBuffer string //内容
		Crdate        string //时间
		PartnerName   string //渠道名称
		IPAddress     string //IP地址
	}

	appleLog_out struct {
		RecordCount int //记录数
		List        []appleLogModel
	}

	appleLog struct {
		database.Trans_base
		In  appleLog_in
		Out appleLog_out
	}
)

func NewAppleLog() *appleLog {
	return &appleLog{}
}

func (this *appleLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Apple_GetLogList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]appleLogModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.UserID = int((*ret[0].(*interface{})).(int64))
			out.NickName = (*ret[1].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ReceiptBuffer = (*ret[2].(*interface{})).(string)
			out.Crdate = (*ret[3].(*interface{})).(string)
			out.PartnerName = (*ret[4].(*interface{})).(string)
			out.IPAddress = (*ret[5].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
		}
	}
	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 苹果充值错误记录
type (
	appleErrorLog_in struct {
		UserID    int    //用户ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	appleErrorLogModel struct {
		UserID      int    //用户ID
		NickName    string //昵称
		ErrorMsg    string //错误消息
		Crdate      string //时间
		PartnerName string //渠道名称
		IPAddress   string //IP地址
	}

	appleErrorLog_out struct {
		RecordCount int //记录数
		List        []appleErrorLogModel
	}

	appleErrorLog struct {
		database.Trans_base
		In  appleErrorLog_in
		Out appleErrorLog_out
	}
)

func NewAppleErrorLog() *appleErrorLog {
	return &appleErrorLog{}
}

func (this *appleErrorLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Apple_GetErrorList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]appleErrorLogModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.UserID = int((*ret[0].(*interface{})).(int64))
			out.NickName = (*ret[1].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ErrorMsg = (*ret[2].(*interface{})).(string)
			out.Crdate = (*ret[3].(*interface{})).(string)
			out.PartnerName = (*ret[4].(*interface{})).(string)
			out.IPAddress = (*ret[5].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
		}
	}
	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// google 訂單
type (
	googleLog_in struct {
		UserID    int    //用戶ID
		BeginTime string //開始時間
		EndTime   string //截至時間
		PageIndex int    //頁索引
		PageSize  int    //頁大小
	}

	googleLogModel struct {
		UserID      int    //用戶ID
		NickName    string //昵稱
		ProductID   string //產品ID
		ProductName string //產品名稱
		Price       string //價格
		PartnerName string //渠道名稱
		Token       string //token
		IPAddress   string //IP地址
		Crdate      string //時間
	}

	googleLog_out struct {
		RecordCount int
		List        []googleLogModel
	}

	googleLog struct {
		database.Trans_base
		In  googleLog_in
		Out googleLog_out
	}
)

func NewGoogleLog() *googleLog {
	return &googleLog{}
}

func (this *googleLog) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_GoogleOrder_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]googleLogModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.UserID = int((*ret[0].(*interface{})).(int64))
			out.NickName = (*ret[1].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ProductID = (*ret[2].(*interface{})).(string)
			price := string((*ret[3].(*interface{})).([]byte))
			fPrice, _ := strconv.ParseFloat(price, 64)
			out.Price = fmt.Sprintf("%.2f", fPrice)
			out.PartnerName = (*ret[4].(*interface{})).(string)
			out.Token = (*ret[5].(*interface{})).(string)
			out.IPAddress = (*ret[6].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[7].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 手动充值
type (
	manualPay_in struct {
		OpUserID   int     // 操作员ID
		OpUserName string  // 操作员名称
		OrderID    string  // 订单ID
		UserID     int     // 用户ID
		ProductID  string  // 产品ID
		Price      float64 // 价格
		IpAddress  string  // IP地址
	}

	manualPay_out struct {
		RetCode int // 操作结果
	}

	manualPay struct {
		database.Trans_base
		In  manualPay_in
		Out manualPay_out
	}
)

func NewManualPay() *manualPay {
	return &manualPay{}
}

func (this *manualPay) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_Manual_OrderReq")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@OrderID", database.AdParamInput, database.AdVarChar, 32, this.In.OrderID)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@ProductID", database.AdParamInput, database.AdVarChar, 32, this.In.ProductID)
	statement.AddParamter("@Price", database.AdParamInput, database.AdFloat, 20, this.In.Price)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
}

// 手动充值列表
type (
	manualPayList_in struct {
		UserID    int    // 用户ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	manualPayModel struct {
		OrderID     string // 订单号
		UserID      int    // 用户ID
		NickName    string // 昵称
		ProductID   string // 产品ID
		ProductName string // 产品名称
		Price       string // 价格
		OpUserID    int    // 操作员ID
		OpUserName  string // 操作员名称
		Crdate      string // 时间
	}

	manualPayList_out struct {
		RecordCount   int     // 记录数
		TotalRMBMoney float64 // 成功充值
		UserCount     int     // 用户数
		List          []manualPayModel
	}

	manualPayList struct {
		database.Trans_base
		In  manualPayList_in
		Out manualPayList_out
	}
)

func NewManualPayList() *manualPayList {
	return &manualPayList{}
}

func (this *manualPayList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ManualPay_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalRMBMoney", database.AdParamOutput, database.AdFloat, 20, this.Out.TotalRMBMoney)
	statement.AddParamter("@UserCount", database.AdParamOutput, database.AdInteger, 4, this.Out.UserCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]manualPayModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.OrderID = (*ret[0].(*interface{})).(string)
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.ProductID = (*ret[3].(*interface{})).(string)

			price := string((*ret[4].(*interface{})).([]byte))
			fPrice, _ := strconv.ParseFloat(price, 64)
			out.Price = fmt.Sprintf("%.2f", fPrice)

			out.OpUserID = int((*ret[5].(*interface{})).(int64))
			out.OpUserName = (*ret[6].(*interface{})).(string)
			out.Crdate = (*ret[7].(*interface{})).(string)
			out.ProductName = out.ProductID
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))

	totalRMBMoney := string((*retRows[rowLen-1][1].(*interface{})).([]byte))
	this.Out.TotalRMBMoney, _ = strconv.ParseFloat(totalRMBMoney, 64)

	this.Out.UserCount = int((*retRows[rowLen-1][2].(*interface{})).(int64))
}

// 日充值总额
type (
	payChipListByDay_in struct {
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	payChipListByDayModel struct {
		DateFlag     string //日期标志
		RMBMoney     string //充值总额
		PayUserCount int    //充值用户数(除去重复的)
		ARPU         string //ARPU值
	}

	payChipListByDay_out struct {
		RecordCount   int     //记录数
		TotalRMBMoney float64 //成功充值数
		UserCount     int     //充值用户数
		CurrRMBMoney  float64 //今天充值总额
		List          []payChipListByDayModel
	}

	payChipListByDay struct {
		database.Trans_base
		In  payChipListByDay_in
		Out payChipListByDay_out
	}
)

func NewPayChipListByDay() *payChipListByDay {
	return &payChipListByDay{}
}

func (this *payChipListByDay) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Pay_GetChipListByDay")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalRMBMoney", database.AdParamOutput, database.AdFloat, 20, this.Out.TotalRMBMoney)
	statement.AddParamter("@UserCount", database.AdParamOutput, database.AdInteger, 4, this.Out.UserCount)
	statement.AddParamter("@CurrRMBMoney", database.AdParamOutput, database.AdFloat, 20, this.Out.CurrRMBMoney)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]payChipListByDayModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)

			rmbMoney := string((*ret[1].(*interface{})).([]byte))
			fRMBMoney, _ := strconv.ParseFloat(rmbMoney, 64)
			out.RMBMoney = fmt.Sprintf("%.2f", fRMBMoney)

			out.PayUserCount = int((*ret[2].(*interface{})).(int64))
			ARPURet := string((*ret[3].(*interface{})).([]byte))
			fARPU, _ := strconv.ParseFloat(ARPURet, 64)
			out.ARPU = fmt.Sprintf("%.2f", fARPU)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))

	totalRMBMoney := string((*retRows[rowLen-1][1].(*interface{})).([]byte))
	this.Out.TotalRMBMoney, _ = strconv.ParseFloat(totalRMBMoney, 64)

	this.Out.UserCount = int((*retRows[rowLen-1][2].(*interface{})).(int64))

	currRMBMoney := string((*retRows[rowLen-1][3].(*interface{})).([]byte))
	this.Out.CurrRMBMoney, _ = strconv.ParseFloat(currRMBMoney, 64)
}

type (
	payUserList_in struct {
		DateFlag  string
		PageIndex int
		PageSize  int
	}

	payUserInfo struct {
		Rid       int
		UserID    int
		NickName  string
		Amount    int
		VipLevel  int
		RMBMoney  string
		PayMoney  string
		RegTime   string
		RegIP     string
		LoginTime string
	}

	payUserList_out struct {
		RecordCount int
		List        []payUserInfo
	}

	payUserList struct {
		database.Trans_base
		In  payUserList_in
		Out payUserList_out
	}
)

func NewPayUserList() *payUserList {
	return &payUserList{}
}

func (this *payUserList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Pay_GetUserList")
	statement.AddParamter("@DateFlag", database.AdParamInput, database.AdVarChar, 20, this.In.DateFlag)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlString := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]payUserInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			out.Amount = int((*ret[3].(*interface{})).(int64))
			out.VipLevel = int((*ret[4].(*interface{})).(int64))

			rmbMoney := string((*ret[5].(*interface{})).([]byte))
			fRMBMoney, _ := strconv.ParseFloat(rmbMoney, 64)
			out.RMBMoney = fmt.Sprintf("%.2f", fRMBMoney)

			payMoney := string((*ret[6].(*interface{})).([]byte))
			fPayMoney, _ := strconv.ParseFloat(payMoney, 64)
			out.PayMoney = fmt.Sprintf("%.2f", fPayMoney)

			out.RegTime = (*ret[7].(*interface{})).(string)
			out.RegIP = (*ret[8].(*interface{})).(string)
			out.RegIP = ip.GetCountryAndRegion(out.RegIP, true)
			out.LoginTime = (*ret[9].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}
