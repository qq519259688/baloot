package dao

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 提现日志
type (
	withdrawLog_in struct {
		UserID    int    // 用户ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	withdrawLogInfo struct {
		OrderID         string // 订单号
		TradeID         string // 交易号
		UserID          int    // 用户ID
		NickName        string // 用户昵称
		Amount          int    // 金额
		RealAmount      int    // 实际扣除金额
		DfTransactionId string // 平台代付单号，32字符以内
		GetStatus       int    // 状态 (0=下单(无审核)  1=待审核  2=已审核  3=提现成功(审核)  4=提现成功(无审核) 13=Pending(审核)  14=Pending(无审核) 11=拒绝
		// 		251=提现失败(无审核)  252=提现失败(审核)  255=提现失败)
		DfDesc     string // 代付状态描述
		Balance    int    // 余额
		NotifyTime string // 通知时间
		Crdate     string // 下单时间
	}

	withdrawLog_out struct {
		RecordCount int //记录数
		TotalAmount int //总金币
		List        []withdrawLogInfo
	}

	withdrawLog struct {
		database.Trans_base
		In  withdrawLog_in
		Out withdrawLog_out
	}
)

func NewWithdrawLog() *withdrawLog {
	return &withdrawLog{}
}

func (this *withdrawLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Withdraw_GetLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.TotalAmount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true

	if rowLen > 1 {
		this.Out.List = make([]withdrawLogInfo, rowLen-1)

		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.OrderID = (*ret[0].(*interface{})).(string)
			out.TradeID = (*ret[1].(*interface{})).(string)
			out.UserID = int((*ret[2].(*interface{})).(int64))
			out.NickName = (*ret[3].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.Amount = int((*ret[4].(*interface{})).(int64))
			out.RealAmount = int((*ret[5].(*interface{})).(int64))
			out.DfTransactionId = (*ret[6].(*interface{})).(string)
			out.GetStatus = int((*ret[7].(*interface{})).(int64))
			out.DfDesc = (*ret[8].(*interface{})).(string)
			out.Balance = int((*ret[9].(*interface{})).(int64))
			out.NotifyTime = (*ret[10].(*interface{})).(string)
			out.Crdate = (*ret[11].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.TotalAmount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
}

// 提现统计
type (
	withdrawStatList_in struct {
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	withdrawStatListModel struct {
		DateFlag       string // 日期标识
		WithdrawAmount int    // 提现金额
		SuccessAmount  int    // 成功提现金额
		UserCount      int    // 提现用户数(除去重复的)
	}

	withdrawStatList_out struct {
		RecordCount       int // 记录数
		WithdrawAmount    int // 提现金额
		SuccessAmount     int // 成功提现金额
		CurrWithdraw      int // 今天提现金额
		CurrSuccessAmount int // 今天成功提现金额
		List              []withdrawStatListModel
	}

	withdrawStatList struct {
		database.Trans_base
		In  withdrawStatList_in
		Out withdrawStatList_out
	}
)

func NewWithdrawStatList() *withdrawStatList {
	return &withdrawStatList{}
}

func (this *withdrawStatList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Withdraw_GetStatList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@WithdrawAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.WithdrawAmount)
	statement.AddParamter("@SuccessAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.SuccessAmount)
	statement.AddParamter("@CurrWithdraw", database.AdParamOutput, database.AdBigint, 8, this.Out.CurrWithdraw)
	statement.AddParamter("@CurrSuccessAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.CurrSuccessAmount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]withdrawStatListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.WithdrawAmount = int((*ret[1].(*interface{})).(int64))
			out.UserCount = int((*ret[2].(*interface{})).(int64))
			out.SuccessAmount = int((*ret[3].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.WithdrawAmount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
	this.Out.SuccessAmount = int((*retRows[rowLen-1][2].(*interface{})).(int64))
	this.Out.CurrWithdraw = int((*retRows[rowLen-1][3].(*interface{})).(int64))
	this.Out.CurrSuccessAmount = int((*retRows[rowLen-1][4].(*interface{})).(int64))
}

// 提现排行
type (
	withdrawRank_in struct {
		BeginTime string // 开始时间
		EndTime   string // 截止时间
	}

	withdrawRankInfo struct {
		Rid         int    // 排序
		UserID      int    // 用户ID
		NickName    string // 昵称
		TotalAmount int    // 金额
	}

	withdrawRank_out struct {
		RecordCount int // 记录数
		List        []withdrawRankInfo
	}

	withdrawRank struct {
		database.Trans_base
		In  withdrawRank_in
		Out withdrawRank_out
	}
)

func NewWithdrawRank() *withdrawRank {
	return &withdrawRank{}
}

func (this *withdrawRank) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Withdraw_GetRank")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true

	this.Out.List = make([]withdrawRankInfo, rowLen)

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.Rid = int((*ret[0].(*interface{})).(int64))
		out.UserID = int((*ret[1].(*interface{})).(int64))
		out.NickName = (*ret[2].(*interface{})).(string)
		if info := tagMgr.getInfo(out.UserID); info != nil {
			out.NickName = info.NickName
		}
		out.TotalAmount = int((*ret[3].(*interface{})).(int64))
	}
}

// 提现审核列表
type (
	withdrawAuditList_in struct {
		BeginTime string // 起始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	withdrawAuditList_out struct {
		RecordCount int // 记录数
		List        []withdrawAuditModel
	}

	withdrawAuditModel struct {
		OrderID        string // 订单号
		UserID         int    // 用户ID
		NickName       string // 昵称
		BetAmount      int    // 流水
		WithdrawTimes  int    // 提现次数
		WithdrawAmount int    // 提现金额
		Amount         int    // 申请金额
		GetStatus      int    // 状态 (0=下单(无审核)  1=待审核  2=已审核  3=提现成功(审核)  4=提现成功(无审核)  11=拒绝)
		Crdate         string // 创建时间(申请时间)
		NotifyTime     string // 提现时间
		RealName       string // 真实姓名
		BankCard       string // 银行卡
		BankName       string // 银行名称
		Mobile         string // 电话
		EMail          string // email
		Address        string // 地址
		SourceName     string // 源
	}

	withdrawAuditList struct {
		database.Trans_base
		In  withdrawAuditList_in
		Out withdrawAuditList_out
	}
)

func NewWithdrawAuditList() *withdrawAuditList {
	return &withdrawAuditList{}
}

func (this *withdrawAuditList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Withdraw_GetAuditList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRow := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRow)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true

	if rowLen > 1 {
		this.Out.List = make([]withdrawAuditModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRow[i]
			out := &this.Out.List[i]

			out.OrderID = (*ret[0].(*interface{})).(string)
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.BetAmount = int((*ret[3].(*interface{})).(int64))
			out.WithdrawTimes = int((*ret[4].(*interface{})).(int64))
			out.WithdrawAmount = int((*ret[5].(*interface{})).(int64))
			out.Amount = int((*ret[6].(*interface{})).(int64))
			out.GetStatus = int((*ret[7].(*interface{})).(int64))
			out.Crdate = (*ret[8].(*interface{})).(string)
			out.NotifyTime = (*ret[9].(*interface{})).(string)
			out.RealName = (*ret[10].(*interface{})).(string)
			out.BankCard = (*ret[11].(*interface{})).(string)
			out.BankName = (*ret[12].(*interface{})).(string)
			out.Mobile = (*ret[13].(*interface{})).(string)
			out.EMail = (*ret[14].(*interface{})).(string)
			out.Address = (*ret[15].(*interface{})).(string)
			out.SourceName = (*ret[16].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRow[rowLen-1][0].(*interface{})).(int64))
}

// 提现日志
type (
	withdrawFlashLog_in struct {
		UserID    int    // 用户ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	withdrawFlashLogInfo struct {
		OrderID         string // 订单号
		TradeID         string // 交易号
		UserID          int    // 用户ID
		NickName        string // 用户昵称
		Amount          int    // 金额
		RealAmount      int    // 实际扣除金额
		DfTransactionId string // 平台代付单号，32字符以内
		GetStatus       int    // 状态 (0=下单(无审核)  1=待审核  2=已审核  3=提现成功(审核)  4=提现成功(无审核) 13=Pending(审核)  14=Pending(无审核) 11=拒绝
		// 		251=提现失败(无审核)  252=提现失败(审核)  255=提现失败)
		DfDesc     string // 代付状态描述
		Balance    int    // 余额
		NotifyTime string // 通知时间
		Crdate     string // 下单时间
	}

	withdrawFlashLog_out struct {
		RecordCount int //记录数
		TotalAmount int //总金币
		List        []withdrawFlashLogInfo
	}

	withdrawFlashLog struct {
		database.Trans_base
		In  withdrawFlashLog_in
		Out withdrawFlashLog_out
	}
)

func NewWithdrawFlashLog() *withdrawFlashLog {
	return &withdrawFlashLog{}
}

func (this *withdrawFlashLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_FlashWithdraw_GetLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.TotalAmount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true

	if rowLen > 1 {
		this.Out.List = make([]withdrawFlashLogInfo, rowLen-1)

		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.OrderID = (*ret[0].(*interface{})).(string)
			out.TradeID = (*ret[1].(*interface{})).(string)
			out.UserID = int((*ret[2].(*interface{})).(int64))
			out.NickName = (*ret[3].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.Amount = int((*ret[4].(*interface{})).(int64))
			out.RealAmount = int((*ret[5].(*interface{})).(int64))
			out.DfTransactionId = (*ret[6].(*interface{})).(string)
			out.GetStatus = int((*ret[7].(*interface{})).(int64))
			out.DfDesc = (*ret[8].(*interface{})).(string)
			out.Balance = int((*ret[9].(*interface{})).(int64))
			out.NotifyTime = (*ret[10].(*interface{})).(string)
			out.Crdate = (*ret[11].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.TotalAmount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
}
