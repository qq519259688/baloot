package dao

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

type (
	scoreRankList_in struct {
		RankType int
	}

	scoreRankItem struct {
		DateFlag string
		Rank     int
		UserID   int
		NickName string
		Amount   int
		Crdate   string
	}

	scoreRankList_out struct {
		RecordCount int
		List        []scoreRankItem
	}

	scoreRankList struct {
		database.Trans_base
		In  scoreRankList_in
		Out scoreRankList_out
	}
)

func NewScoreRankList() *scoreRankList {
	return &scoreRankList{}
}

func (this *scoreRankList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ChipScoreRank_GetList")
	statement.AddParamter("@RankType", database.AdParamInput, database.AdInteger, 4, this.In.RankType)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	this.Out.List = make([]scoreRankItem, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.Rank = int((*ret[1].(*interface{})).(int64))
		out.UserID = int((*ret[2].(*interface{})).(int64))
		out.NickName = (*ret[3].(*interface{})).(string)
		if info := tagMgr.getInfo(out.UserID); info != nil {
			out.NickName = info.NickName
		}
		out.Amount = int((*ret[4].(*interface{})).(int64))
		out.Crdate = (*ret[5].(*interface{})).(string)
	}
}

type (
	addScore_in struct {
		OpUserID   int    // 操作员ID
		OpUserName string // 操作员名称
		UserID     int    // 用户ID
		Score      int    // 积分
		IpAddress  string // IP地址
	}

	addScore struct {
		database.Trans_base
		In addScore_in
	}
)

func NewAddScore() *addScore {
	return &addScore{}
}

func (this *addScore) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserChipScore_AddRobot")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@Score", database.AdParamInput, database.AdInteger, 4, this.In.Score)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	sqlstring := statement.GenSql()
	CenterDB.ExecSql(sqlstring)
	return
}
