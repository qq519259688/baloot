package dao

import (
	"bet24.com/servers/adminserver/ip"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 反馈列表
type (
	feedbackList_in struct {
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	feedbackListDetail struct {
		Rid        int    // 标识
		EMail      string // 邮箱
		Msg        string // 消息
		Status     int    // 状态
		OpUserID   int    // 操作员ID
		OpUserName string // 操作员名称
		IPAddress  string // ip地址
		Crdate     string // 时间
		ToUserId   int    // 关联的用户ID
		ToNickName string // 关联的用户昵称
	}

	feedbackList_out struct {
		RecordCount int // 记录数
		List        []feedbackListDetail
	}

	feedbackList struct {
		database.Trans_base
		In  feedbackList_in
		Out feedbackList_out
	}
)

func NewFeedbackList() *feedbackList {
	return &feedbackList{}
}

func (this *feedbackList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Feedback_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]feedbackListDetail, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.EMail = (*ret[1].(*interface{})).(string)
			out.Msg = (*ret[2].(*interface{})).(string)
			out.Status = int((*ret[3].(*interface{})).(int64))
			out.OpUserID = int((*ret[4].(*interface{})).(int64))
			out.OpUserName = (*ret[5].(*interface{})).(string)
			out.IPAddress = (*ret[6].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[7].(*interface{})).(string)
			out.ToUserId = int((*ret[8].(*interface{})).(int64))
			out.ToNickName = (*ret[9].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 反馈修改
type (
	feedbackUp_in struct {
		OpUserID   int    // 操作员ID
		OpUserName string // 操作员名称
		Rid        int    // 标识
		Status     int    // 状态
	}

	feedbackUp_out struct {
		RetCode int // 操作结果
	}

	feedbackUp struct {
		database.Trans_base
		In  feedbackUp_in
		Out feedbackUp_out
	}
)

func NewFeedbackUp() *feedbackUp {
	return &feedbackUp{}
}

func (this *feedbackUp) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Feedback_Update")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@Rid", database.AdParamInput, database.AdInteger, 4, this.In.Rid)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, this.In.Status)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}
	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
	return
}
