package dao

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

//闲玩统计
type (
	xwInfoStatModel struct {
		InfoID         int //信息ID
		InfoType       int //类型 1=赢金  2=充值  3=冲级(每档前5名)
		Amount         int //数额
		RMBMoney       int //单价(人民币:分)
		UserCountLimit int //用户数上限 -1=无限制  >0 限制
		UserCount      int //用户数
		TotalRMBMoney  int //消耗(人民币:分)
	}

	xwInfoStat_out struct {
		RecordCount     int
		PayRMBMoney     int //充值消耗
		WinRMBMoney     int //赢金消耗
		WinRankRMBMoney int //冲金消耗
		List            []xwInfoStatModel
	}

	xwInfoStat struct {
		database.Trans_base
		Out xwInfoStat_out
	}
)

func NewXwInfoStat() *xwInfoStat {
	return &xwInfoStat{}
}

func (this *xwInfoStat) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_XWInfoStat_GetList")
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	this.Out.RecordCount = rowLen
	this.Out.List = make([]xwInfoStatModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.InfoID = int((*ret[0].(*interface{})).(int64))
		out.InfoType = int((*ret[1].(*interface{})).(int64))
		out.Amount = int((*ret[2].(*interface{})).(int64))
		out.RMBMoney = int((*ret[3].(*interface{})).(int64))
		out.UserCountLimit = int((*ret[4].(*interface{})).(int64))
		out.UserCount = int((*ret[5].(*interface{})).(int64))
		userCount := out.UserCount
		if out.UserCountLimit > 0 && out.UserCount >= out.UserCountLimit {
			userCount = out.UserCountLimit
		}
		out.TotalRMBMoney = out.RMBMoney * userCount
		switch out.InfoType {
		case 1: //充值
			this.Out.PayRMBMoney = this.Out.PayRMBMoney + out.TotalRMBMoney
		case 2: //冲金
			this.Out.WinRankRMBMoney = this.Out.WinRankRMBMoney + out.TotalRMBMoney
		case 3: //赢金
			this.Out.WinRMBMoney = this.Out.WinRMBMoney + out.TotalRMBMoney
		}
	}
}

//闲玩用户信息
type (
	xwUserInfoList_in struct {
		InfoID    int //信息ID
		PageIndex int //页索引
		PageSize  int //页大小
	}

	xwUserInfoListModel struct {
		RowNumber int    //序号
		UserID    int    //用户ID
		NickName  string //昵称
		Deviceid  string //设备号
		Win_gold  int    //赢金
		Payamount int    //充值
		Crdate    string //时间
	}

	xwUserInfoList_out struct {
		RecordCount int
		List        []xwUserInfoListModel
	}
	xwUserInfoList struct {
		database.Trans_base
		In  xwUserInfoList_in
		Out xwUserInfoList_out
	}
)

func NewXwUserInfoList() *xwUserInfoList {
	return &xwUserInfoList{}
}

func (this *xwUserInfoList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_XWInfoStat_GetUserList")
	statement.AddParamter("@InfoID", database.AdParamInput, database.AdInteger, 4, this.In.InfoID)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}
	if rowLen > 1 {
		this.Out.List = make([]xwUserInfoListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RowNumber = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.Deviceid = (*ret[3].(*interface{})).(string)
			out.Win_gold = int((*ret[4].(*interface{})).(int64))
			out.Payamount = int((*ret[5].(*interface{})).(int64))
			out.Crdate = (*ret[6].(*interface{})).(string)
		}
	}
	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}
