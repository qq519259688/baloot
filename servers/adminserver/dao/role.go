package dao

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

//角色列表
type (
	roleInfo struct {
		RoleID   int    //角色ID
		RoleName string //角色名称
	}

	roleList_out struct {
		List []roleInfo
	}

	roleList struct {
		database.Trans_base
		Out roleList_out
	}
)

func NewRoleList() *roleList {
	return &roleList{}
}

func (this *roleList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminRole_GetList")
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]roleInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.RoleID = int((*ret[0].(*interface{})).(int64))
		out.RoleName = (*ret[1].(*interface{})).(string)
	}
}

//添加角色用户
type (
	roleUserAdd_in struct {
		OpUserID     int    //操作员ID
		OpUserName   string //操作员名称
		AdminUserID  int    //管理员ID
		AdminRoleIDs string //管理角色ID集 (格式如：1,2,3,)
		IpAddress    string //IP地址
	}

	roleUserAdd struct {
		database.Trans_base
		In roleUserAdd_in
	}
)

func NewRoleUserAdd() *roleUserAdd {
	return &roleUserAdd{}
}

func (this *roleUserAdd) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_RoleUser_AddUser")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@AdminUserID", database.AdParamInput, database.AdVarChar, 32, this.In.AdminUserID)
	statement.AddParamter("@AdminRoleIDs", database.AdParamInput, database.AdVarChar, 1024, this.In.AdminRoleIDs)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
}

//用户角色列表
type (
	roleListByAdmin_in struct {
		AdminUserID int //管理员ID
	}

	roleAdminInfo struct {
		RoleID      int    //角色ID
		RoleName    string //角色名称
		AdminUserID int    //管理员ID
	}

	roleListByAdmin_out struct {
		List []roleAdminInfo
	}

	roleListByAdmin struct {
		database.Trans_base
		In  roleListByAdmin_in
		Out roleListByAdmin_out
	}
)

func NewRoleListByAdmin() *roleListByAdmin {
	return &roleListByAdmin{}
}

func (this *roleListByAdmin) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminRole_GetListByAdmin")
	statement.AddParamter("@AdminUserID", database.AdParamInput, database.AdVarChar, 32, this.In.AdminUserID)

	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]roleAdminInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.RoleID = int((*ret[0].(*interface{})).(int64))
		out.RoleName = (*ret[1].(*interface{})).(string)
		out.AdminUserID = int((*ret[2].(*interface{})).(int64))
	}
}

//角色设置页面
type (
	roleSetPage_in struct {
		OpUserID     int    //操作员ID
		OpUserName   string //操作员名称
		AdminRoleID  int    //角色ID
		AdminPageIDs string //页面ID集(格式如：1,2,3,)
		IpAddress    string //IP地址
	}

	roleSetPage_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	roleSetPage struct {
		database.Trans_base
		In  roleSetPage_in
		Out roleSetPage_out
	}
)

func NewRoleSetPage() *roleSetPage {
	return &roleSetPage{}
}

func (this *roleSetPage) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminRole_SetPage")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@AdminRoleID", database.AdParamInput, database.AdInteger, 4, this.In.AdminRoleID)
	statement.AddParamter("@AdminPageIDs", database.AdParamInput, database.AdVarChar, 2048, this.In.AdminPageIDs)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
}

//角色页面列表
type (
	rolePageList_in struct {
		AdminRoleID int //角色ID
	}

	rolePageInfo struct {
		AdminRoleID int    //角色ID
		AdminPageID int    //页面ID
		PageName    string //页面名称
	}

	rolePageList_out struct {
		AdminRoleName string //角色名称
		List          []rolePageInfo
	}

	rolePageList struct {
		database.Trans_base
		In  rolePageList_in
		Out rolePageList_out
	}
)

func NewRolePageList() *rolePageList {
	return &rolePageList{}
}

func (this *rolePageList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AdminRole_GetPageList")
	statement.AddParamter("@AdminRoleID", database.AdParamInput, database.AdInteger, 4, this.In.AdminRoleID)
	statement.AddParamter("@AdminRoleName", database.AdParamOutput, database.AdNVarChar, 32, this.Out.AdminRoleName)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]rolePageInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.AdminRoleID = int((*ret[0].(*interface{})).(int64))
			out.AdminPageID = int((*ret[1].(*interface{})).(int64))
			out.PageName = (*ret[2].(*interface{})).(string)
		}
	}

	this.Out.AdminRoleName = (*retRows[rowLen-1][0].(*interface{})).(string)
}

//角色用户列表
type (
	roleUserInfo struct {
		AdminUserID   int    //管理员ID
		AdminUserName string //管理员名称
		AdminRoleID   int    //角色ID
		AdminRoleName string //角色名称
		Crdate        string //时间
	}

	roleUserList_out struct {
		List []roleUserInfo
	}

	roleUserList struct {
		database.Trans_base
		Out roleUserList_out
	}
)

func NewRoleUserList() *roleUserList {
	return &roleUserList{}
}

func (this *roleUserList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_RoleUser_GetList")
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]roleUserInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.AdminUserID = int((*ret[0].(*interface{})).(int64))
		out.AdminUserName = (*ret[1].(*interface{})).(string)
		out.AdminRoleID = int((*ret[2].(*interface{})).(int64))
		out.AdminRoleName = (*ret[3].(*interface{})).(string)
		out.Crdate = (*ret[4].(*interface{})).(string)
	}
}
