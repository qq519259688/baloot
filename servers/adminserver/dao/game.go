package dao

import (
	"runtime/debug"
	"strconv"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/redis"
)

// 所有游戏
type (
	getAllGames_in struct {
		GameID int
	}

	AllGameInfo struct {
		GameID      int    //游戏ID
		ChineseName string //中文名
		EnglishName string //英文名
		DisplaySort int    //显示顺序
		Enabled     int    //是否启用
		DBName      string //数据库名
		GameName    string //游戏名
		GameType    int    // 游戏类型 (0=无限制  1=金币游戏  2=元宝游戏)
		NeedLevel   int    // 所需等级
	}

	getAllGames struct {
		database.Trans_base
		In  getAllGames_in
		Out []AllGameInfo
	}
)

func NewGetAllGames() *getAllGames {
	return &getAllGames{}
}

func (this *getAllGames) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllGame_GetList")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, this.In.GameID)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out = make([]AllGameInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out[i]

		out.GameID = int((*ret[0].(*interface{})).(int64))
		out.ChineseName = (*ret[1].(*interface{})).(string)
		out.EnglishName = (*ret[2].(*interface{})).(string)
		out.DisplaySort = int((*ret[3].(*interface{})).(int64))
		out.Enabled = int((*ret[4].(*interface{})).(int64))
		out.DBName = (*ret[5].(*interface{})).(string)
		out.GameName = (*ret[6].(*interface{})).(string)
		out.GameType = int((*ret[7].(*interface{})).(int64))
		out.NeedLevel = int((*ret[8].(*interface{})).(int64))
	}
}

// 审核游戏列表
type (
	gameRequestList_in struct {
		PartnerID int
	}

	gameRequestInfo struct {
		PartnerID   int    //渠道ID
		PartnerName string //渠道名称
		VersionCode int    //版本
		GameName    string //游戏名称
		ChineseName string //中文名称
	}

	gameRequestList_out struct {
		RecordCount int //记录数
		List        []gameRequestInfo
	}

	getGameRequestList struct {
		database.Trans_base
		In  gameRequestList_in
		Out gameRequestList_out
	}
)

func NewGetGameRequestList() *getGameRequestList {
	return &getGameRequestList{}
}

func (this *getGameRequestList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_GameRequest_GetList")
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]gameRequestInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.PartnerID = int((*ret[0].(*interface{})).(int64))
		out.PartnerName = (*ret[1].(*interface{})).(string)
		out.VersionCode = int((*ret[2].(*interface{})).(int64))
		out.GameName = (*ret[3].(*interface{})).(string)
		out.ChineseName = (*ret[4].(*interface{})).(string)
	}
}

// 添加审核
type (
	gameRequestAdd_in struct {
		OpUserID    int    //操作员ID
		OpUserName  string //操作员名称
		PartnerID   int    //渠道ID
		PartnerName string //渠道名称
		VersionCode int    //版本号
		GameName    string //游戏名称
		IpAddress   string //IP地址
	}

	gameRequestAdd_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	gameRequestAdd struct {
		database.Trans_base
		In  gameRequestAdd_in
		Out gameRequestAdd_out
	}
)

func NewGameRequestAdd() *gameRequestAdd {
	return &gameRequestAdd{}
}

func (this *gameRequestAdd) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_GameRequest_Add")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 20, this.In.PartnerID)
	statement.AddParamter("@PartnerName", database.AdParamInput, database.AdNVarChar, 32, this.In.PartnerName)
	statement.AddParamter("@VersionCode", database.AdParamInput, database.AdInteger, 20, this.In.VersionCode)
	statement.AddParamter("@GameName", database.AdParamInput, database.AdVarChar, 32, this.In.GameName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
}

// 删除审核
type (
	gameRequestDel_in struct {
		OpUserID   int    //操作员ID
		OpUserName string //操作员名称
		PartnerID  int    //渠道ID
		IpAddress  string //IP地址
	}

	gameRequestDel_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	gameRequestDel struct {
		database.Trans_base
		In  gameRequestDel_in
		Out gameRequestDel_out
	}
)

func NewGameRequestDel() *gameRequestDel {
	return &gameRequestDel{}
}

func (this *gameRequestDel) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_GameRequest_Del")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 20, this.In.PartnerID)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
}

// 游戏信息修改
type (
	allGameUpd_in struct {
		OpUserID    int    //操作员ID
		OpUserName  string //操作员名称
		GameID      int    //渠道ID
		IpAddress   string //IP地址
		Enabled     int    //是否启用
		DisplaySort int    //显示顺序
		GameType    int    //游戏类型 (0=无限制  1=金币游戏  2=元宝游戏)
		NeedLevel   int    // 所需等级
	}

	allGameUpd struct {
		database.Trans_base
		In allGameUpd_in
	}
)

func NewAllGameUpd() *allGameUpd {
	return &allGameUpd{}
}

func (this *allGameUpd) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllGame_Update")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, this.In.GameID)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@Enabled", database.AdParamInput, database.AdInteger, 4, this.In.Enabled)
	statement.AddParamter("@DisplaySort", database.AdParamInput, database.AdInteger, 4, this.In.DisplaySort)
	statement.AddParamter("@GameType", database.AdParamInput, database.AdInteger, 4, this.In.GameType)
	statement.AddParamter("@NeedLevel", database.AdParamInput, database.AdInteger, 4, this.In.NeedLevel)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
}

const (
	_                = iota
	Odds_Sys         //系统返还比率
	Odds_RoomOdds_EC //EC：浅水滩（初级）
	Odds_RoomOdds_AO //AO：深海寻宝（中级）
	Odds_RoomOdds_AC //AC：海底密藏（高级）
	Odds_RoomOdds_DM //DM：深海沉船（大师）
	Odds_RoomOdds_MAX
)

// 系统返还率
type sysOdds struct {
	Type int `json:"Type"` //类型
	Odd  int `json:"Odd"`  //比率
}

func NewSysOdds() *sysOdds {
	return &sysOdds{}
}

func (s *sysOdds) getKey(oddType int) string {
	switch oddType {
	case Odds_Sys:
		return "fish:sysodds" //系统返还比率
	case Odds_RoomOdds_EC:
		return "fish:roomodds:EC" //EC：浅水滩（初级）
	case Odds_RoomOdds_AO:
		return "fish:roomodds:AO" //AO：深海寻宝（中级）
	case Odds_RoomOdds_AC:
		return "fish:roomodds:AC" //AC：海底密藏（高级）
	case Odds_RoomOdds_DM:
		return "fish:roomodds:DM" //DM：深海沉船（大师）
	default:
		log.Error("NewSysOdds getKey type=%d is not exist", oddType)
	}
	return ""
}

// 获取系统返还比率
func (s *sysOdds) Get() []*sysOdds {
	var odds []*sysOdds
	for oddType := 1; oddType < Odds_RoomOdds_MAX; oddType++ {
		key := s.getKey(oddType)
		odds = append(odds, &sysOdds{
			Type: oddType,
			Odd:  redis.String_GetInt(key),
		})
	}
	return odds
}

// 设置系统返还比率
func (s *sysOdds) Set() bool {
	if s.Odd < 0 {
		log.Error("SetSysOdds odds(%d) 参数有误", s.Odd)
		return false
	}

	key := s.getKey(s.Type)
	if key == "" {
		return false
	}
	return redis.String_Set(key, strconv.Itoa(s.Odd))
}
