package dao

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

type utmSource struct {
	Source string
}

func GetUTMSource() []*utmSource {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UTMSource_GetList")
	sqlString := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*utmSource
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info utmSource
		info.Source = (*ret[0].(*interface{})).(string)
		list = append(list, &info)
	}

	return list
}
