package dao

import (
	"fmt"
	"runtime/debug"
	"sort"
	"strconv"
	"sync"
	"time"

	"bet24.com/database"
	"bet24.com/log"
)

var tagMgr *usertagManager

type info struct {
	UserId     int     // 用户id
	NickName   string  // 昵称
	VipLevel   int     // vip等级
	PayMoney   float64 // 累计充值额度
	ColorValue int     // 色值
}

type usertagManager struct {
	list []*info
	lock *sync.RWMutex
}

func RunTagManager() {
	tagMgr = new(usertagManager)
	tagMgr.lock = &sync.RWMutex{}
	go tagMgr.doRefresh()
}

func (this *usertagManager) doRefresh() {
	this.loadData()

	ticker := time.NewTicker(3 * time.Minute)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				this.loadData()
			}
		}
	}(ticker)
}

func (this *usertagManager) loadData() {
	list := this.doAction()
	this.lock.Lock()
	defer this.lock.Unlock()
	this.list = list
}

func (this *usertagManager) getInfo(userId int) *info {
	this.lock.RLock()
	defer this.lock.RUnlock()
	for _, v := range this.list {
		if v.UserId == userId {
			return v
		}
	}
	return nil
}

func GetUserTagsList(colorValue, pageIndex, pageSize int) (int, []*info) {
	return tagMgr.getList(colorValue, pageIndex, pageSize)
}

func (this *usertagManager) getList(colorValue, pageIndex, pageSize int) (int, []*info) {
	var list []*info
	this.lock.RLock()
	for _, v := range this.list {
		if v.ColorValue == colorValue {
			list = append(list, v)
		}
	}
	this.lock.RUnlock()
	total := len(list)
	if total <= 0 {
		return total, nil
	}
	start := (pageIndex - 1) * pageSize
	end := pageIndex * pageSize

	if start < 0 || start > end {
		return 0, nil
	}

	if end > total {
		end = total
	}

	return total, list[start:end]
}

func (this *usertagManager) getColor(nickName string, colorValue, vipLevel int) string {
	colorName := "black"
	switch colorValue {
	case 3:
		colorName = "red"
	case 2:
		colorName = "orange"
	case 1:
		colorName = "#3bb4f2"
	}

	return fmt.Sprintf("<font color='%s'><b>%s</b></font>(%d)", colorName, nickName, vipLevel)
}

func (this *usertagManager) doAction() []*info {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*info

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetTags")
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out info
		out.UserId = int((*ret[0].(*interface{})).(int64))
		out.VipLevel = int((*ret[1].(*interface{})).(int64))

		payMoney := string((*ret[2].(*interface{})).([]byte))
		out.PayMoney, _ = strconv.ParseFloat(payMoney, 64)

		out.ColorValue = int((*ret[3].(*interface{})).(int64))
		out.NickName = (*ret[4].(*interface{})).(string)
		out.NickName = this.getColor(out.NickName, out.ColorValue, out.VipLevel)
		list = append(list, &out)
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].PayMoney > list[j].PayMoney
	})
	sort.SliceStable(list, func(i, j int) bool {
		return list[i].VipLevel > list[j].VipLevel
	})
	sort.SliceStable(list, func(i, j int) bool {
		return list[i].ColorValue > list[j].ColorValue
	})
	return list
}
