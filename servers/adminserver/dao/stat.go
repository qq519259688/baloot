package dao

import (
	"runtime/debug"
	"strconv"

	"bet24.com/database"
	"bet24.com/log"
)

// 日常统计
type (
	dailyStat_in struct {
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	dailyStatInfo struct {
		DateFlag        string  //日期标志
		AvgOnlineCount  int     //平均在线
		MaxOnlineCount  int     //最高在线
		RegCount        int     //注册用户
		LoginCount      int     //登陆用户
		ActiveCount     int     //活跃用户(活跃用户是指参与过游戏的用户)
		PayMoney        float64 //充值
		CosumeAmount    int     //消耗货币(消耗货币是指昨日的货币总额变化数量)
		PayUserCount    int     //付费人数
		AvgPayMoney     float64 //人均付费
		LiveUsers_1     int     //次日留存
		LiveUsers_3     int     //3日留存
		LiveUsers_7     int     //7日留存
		ReturnUserCount int     //回归用户(回归用户是指这天登录，但此前超过10天没有登录过的用户)
		VideoPlayTimes  int     //视频广告播放次数（游戏）
		VideoSendAmount int     //视频广告赠送金币（游戏）
	}

	dailyStat_out struct {
		RecordCount int //记录数
		List        []dailyStatInfo
	}

	dailyStat struct {
		database.Trans_base
		In  dailyStat_in
		Out dailyStat_out
	}
)

func NewDailyStat() *dailyStat {
	return &dailyStat{}
}

func (this *dailyStat) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_DailyStat_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]dailyStatInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.AvgOnlineCount = int((*ret[1].(*interface{})).(int64))
			out.MaxOnlineCount = int((*ret[2].(*interface{})).(int64))
			out.RegCount = int((*ret[3].(*interface{})).(int64))
			out.LoginCount = int((*ret[4].(*interface{})).(int64))
			out.ActiveCount = int((*ret[5].(*interface{})).(int64))

			payAmount := string((*ret[6].(*interface{})).([]byte))
			out.PayMoney, _ = strconv.ParseFloat(payAmount, 64)

			out.CosumeAmount = int((*ret[7].(*interface{})).(int64))
			out.PayUserCount = int((*ret[8].(*interface{})).(int64))

			avgPayMoney := string((*ret[9].(*interface{})).([]byte))
			out.AvgPayMoney, _ = strconv.ParseFloat(avgPayMoney, 64)

			out.LiveUsers_1 = int((*ret[10].(*interface{})).(int64))
			out.LiveUsers_3 = int((*ret[11].(*interface{})).(int64))
			out.LiveUsers_7 = int((*ret[12].(*interface{})).(int64))
			out.ReturnUserCount = int((*ret[13].(*interface{})).(int64))
			out.VideoPlayTimes = int((*ret[14].(*interface{})).(int64))
			out.VideoSendAmount = int((*ret[15].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 在线统计
type (
	onlineStat_in struct {
		PartnerID int    //合作商ID
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	onlineStatInfo struct {
		DateFlag            string //日期
		OnlineUserCount     int    //在线用户数
		PartnerName         string //渠道名称
		LastOnlineUserCount int    //昨天在线人数
	}

	onlineStat_out struct {
		RecordCount int //记录数
		MaxCount    int //最高数
		MinCount    int //最低数
		AvgCount    int //平均数
		List        []onlineStatInfo
	}

	onlineStat struct {
		database.Trans_base
		In  onlineStat_in
		Out onlineStat_out
	}
)

func NewOnlineStat() *onlineStat {
	return &onlineStat{}
}

func (this *onlineStat) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_OnlineStat_GetList")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@MaxCount", database.AdParamOutput, database.AdInteger, 4, this.Out.MaxCount)
	statement.AddParamter("@MinCount", database.AdParamOutput, database.AdInteger, 4, this.Out.MinCount)
	statement.AddParamter("@AvgCount", database.AdParamOutput, database.AdInteger, 4, this.Out.AvgCount)

	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]onlineStatInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.OnlineUserCount = int((*ret[1].(*interface{})).(int64))
			out.PartnerName = (*ret[2].(*interface{})).(string)
			out.LastOnlineUserCount = int((*ret[3].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.MaxCount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
	this.Out.MinCount = int((*retRows[rowLen-1][2].(*interface{})).(int64))
	this.Out.AvgCount = int((*retRows[rowLen-1][3].(*interface{})).(int64))
}

// 在线统计(每天)
type (
	onlineStatListByDay_in struct {
		PartnerID int    //合作商ID
		BeginTime string //开始时间
		EndTime   string //结束时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	onlineStatListByDayModel struct {
		DateFlag        string //日期标识
		OnlineUserCount int    //在线用户数
		PartnerName     string //渠道名称
		TimeFlag        string //时间标识
	}

	onlineStatListByDay_out struct {
		RecordCount int //记录数
		MaxCount    int //最高数
		MinCount    int //最低数
		AvgCount    int //平均数
		List        []onlineStatListByDayModel
	}

	onlineStatListByDay struct {
		database.Trans_base
		In  onlineStatListByDay_in
		Out onlineStatListByDay_out
	}
)

func NewOnlineStatListByDay() *onlineStatListByDay {
	return &onlineStatListByDay{}
}

func (this *onlineStatListByDay) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_OnlineStat_GetListByDay")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@MaxCount", database.AdParamOutput, database.AdInteger, 4, this.Out.MaxCount)
	statement.AddParamter("@MinCount", database.AdParamOutput, database.AdInteger, 4, this.Out.MinCount)
	statement.AddParamter("@AvgCount", database.AdParamOutput, database.AdInteger, 4, this.Out.AvgCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]onlineStatListByDayModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.OnlineUserCount = int((*ret[1].(*interface{})).(int64))
			out.PartnerName = (*ret[2].(*interface{})).(string)
			out.TimeFlag = (*ret[3].(*interface{})).(string)
		}
	}
	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.MaxCount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
	this.Out.MinCount = int((*retRows[rowLen-1][2].(*interface{})).(int64))
	this.Out.AvgCount = int((*retRows[rowLen-1][3].(*interface{})).(int64))
}

// 在线统计报表(小时)
type (
	onlineStatReport_in struct {
		GameID    int    //游戏ID
		BeginTime string //开始时间
		EndTime   string //截止时间
	}

	onlineStatReportModel struct {
		DateFlag string //日期标志

		Hour_0  int //0点
		Hour_1  int //1点
		Hour_2  int //2点
		Hour_3  int //3点
		Hour_4  int //4点
		Hour_5  int //5点
		Hour_6  int //6点
		Hour_7  int //7点
		Hour_8  int //8点
		Hour_9  int //9点
		Hour_10 int //10点
		Hour_11 int //11点
		Hour_12 int //12点
		Hour_13 int //13点
		Hour_14 int //14点
		Hour_15 int //15点
		Hour_16 int //16点
		Hour_17 int //17点
		Hour_18 int //18点
		Hour_19 int //19点
		Hour_20 int //20点
		Hour_21 int //21点
		Hour_22 int //22点
		Hour_23 int //23点
	}

	onlineStatReport_out struct {
		List []onlineStatReportModel
	}

	onlineStatReport struct {
		database.Trans_base
		In  onlineStatReport_in
		Out onlineStatReport_out
	}
)

func NewOnlineStatReport() *onlineStatReport {
	return &onlineStatReport{}
}

func (this *onlineStatReport) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_OnlineStat_GetReport")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]onlineStatReportModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.Hour_0 = int((*ret[1].(*interface{})).(int64))
		out.Hour_1 = int((*ret[2].(*interface{})).(int64))
		out.Hour_2 = int((*ret[3].(*interface{})).(int64))
		out.Hour_3 = int((*ret[4].(*interface{})).(int64))
		out.Hour_4 = int((*ret[5].(*interface{})).(int64))
		out.Hour_5 = int((*ret[6].(*interface{})).(int64))
		out.Hour_6 = int((*ret[7].(*interface{})).(int64))
		out.Hour_7 = int((*ret[8].(*interface{})).(int64))
		out.Hour_8 = int((*ret[9].(*interface{})).(int64))
		out.Hour_9 = int((*ret[10].(*interface{})).(int64))
		out.Hour_10 = int((*ret[11].(*interface{})).(int64))
		out.Hour_11 = int((*ret[12].(*interface{})).(int64))
		out.Hour_12 = int((*ret[13].(*interface{})).(int64))
		out.Hour_13 = int((*ret[14].(*interface{})).(int64))
		out.Hour_14 = int((*ret[15].(*interface{})).(int64))
		out.Hour_15 = int((*ret[16].(*interface{})).(int64))
		out.Hour_16 = int((*ret[17].(*interface{})).(int64))
		out.Hour_17 = int((*ret[18].(*interface{})).(int64))
		out.Hour_18 = int((*ret[19].(*interface{})).(int64))
		out.Hour_19 = int((*ret[20].(*interface{})).(int64))
		out.Hour_20 = int((*ret[21].(*interface{})).(int64))
		out.Hour_21 = int((*ret[22].(*interface{})).(int64))
		out.Hour_22 = int((*ret[23].(*interface{})).(int64))
		out.Hour_23 = int((*ret[24].(*interface{})).(int64))
	}
}

// 同时在线统计报表(小时)
type (
	onlineUserReport_in struct {
		BeginTime string // 开始时间
		EndTime   string // 截止时间
	}

	onlineUserReportModel struct {
		DateFlag string // 日期标志

		Hour_0000 int // 00:00
		Hour_0030 int // 00:30

		Hour_0100 int // 01:00
		Hour_0130 int // 01:30

		Hour_0200 int // 02:00
		Hour_0230 int // 02:30

		Hour_0300 int // 03:00
		Hour_0330 int // 03:30

		Hour_0400 int // 04:00
		Hour_0430 int // 04:30

		Hour_0500 int // 05:00
		Hour_0530 int // 05:30

		Hour_0600 int // 06:00
		Hour_0630 int // 06:30

		Hour_0700 int // 07:00
		Hour_0730 int // 07:30

		Hour_0800 int // 08:00
		Hour_0830 int // 08:30

		Hour_0900 int // 09:00
		Hour_0930 int // 09:30

		Hour_1000 int // 10:00
		Hour_1030 int // 10:30

		Hour_1100 int // 11:00
		Hour_1130 int // 11:30

		Hour_1200 int // 12:00
		Hour_1230 int // 12:30

		Hour_1300 int // 13:00
		Hour_1330 int // 13:30

		Hour_1400 int // 14:00
		Hour_1430 int // 14:30

		Hour_1500 int // 15:00
		Hour_1530 int // 15:30

		Hour_1600 int // 16:00
		Hour_1630 int // 16:30

		Hour_1700 int // 17:00
		Hour_1730 int // 17:30

		Hour_1800 int // 18:00
		Hour_1830 int // 18:30

		Hour_1900 int // 19:00
		Hour_1930 int // 19:30

		Hour_2000 int // 20:00
		Hour_2030 int // 20:30

		Hour_2100 int // 21:00
		Hour_2130 int // 21:30

		Hour_2200 int // 22:00
		Hour_2230 int // 22:30

		Hour_2300 int // 23:00
		Hour_2330 int // 23:30
	}

	onlineUserReport_out struct {
		List []onlineUserReportModel
	}

	onlineUserReport struct {
		database.Trans_base
		In  onlineUserReport_in
		Out onlineUserReport_out
	}
)

func NewOnlineUserReport() *onlineUserReport {
	return &onlineUserReport{}
}

func (this *onlineUserReport) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_OnlineUser_GetReport")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]onlineUserReportModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.Hour_0000 = int((*ret[1].(*interface{})).(int64))
		out.Hour_0030 = int((*ret[2].(*interface{})).(int64))
		out.Hour_0100 = int((*ret[3].(*interface{})).(int64))
		out.Hour_0130 = int((*ret[4].(*interface{})).(int64))
		out.Hour_0200 = int((*ret[5].(*interface{})).(int64))
		out.Hour_0230 = int((*ret[6].(*interface{})).(int64))
		out.Hour_0300 = int((*ret[7].(*interface{})).(int64))
		out.Hour_0330 = int((*ret[8].(*interface{})).(int64))
		out.Hour_0400 = int((*ret[9].(*interface{})).(int64))
		out.Hour_0430 = int((*ret[10].(*interface{})).(int64))
		out.Hour_0500 = int((*ret[11].(*interface{})).(int64))
		out.Hour_0530 = int((*ret[12].(*interface{})).(int64))
		out.Hour_0600 = int((*ret[13].(*interface{})).(int64))
		out.Hour_0630 = int((*ret[14].(*interface{})).(int64))
		out.Hour_0700 = int((*ret[15].(*interface{})).(int64))
		out.Hour_0730 = int((*ret[16].(*interface{})).(int64))
		out.Hour_0800 = int((*ret[17].(*interface{})).(int64))
		out.Hour_0830 = int((*ret[18].(*interface{})).(int64))
		out.Hour_0900 = int((*ret[19].(*interface{})).(int64))
		out.Hour_0930 = int((*ret[20].(*interface{})).(int64))
		out.Hour_1000 = int((*ret[21].(*interface{})).(int64))
		out.Hour_1030 = int((*ret[22].(*interface{})).(int64))
		out.Hour_1100 = int((*ret[23].(*interface{})).(int64))
		out.Hour_1130 = int((*ret[24].(*interface{})).(int64))
		out.Hour_1200 = int((*ret[25].(*interface{})).(int64))
		out.Hour_1230 = int((*ret[26].(*interface{})).(int64))
		out.Hour_1300 = int((*ret[27].(*interface{})).(int64))
		out.Hour_1330 = int((*ret[28].(*interface{})).(int64))
		out.Hour_1400 = int((*ret[29].(*interface{})).(int64))
		out.Hour_1430 = int((*ret[30].(*interface{})).(int64))
		out.Hour_1500 = int((*ret[31].(*interface{})).(int64))
		out.Hour_1530 = int((*ret[32].(*interface{})).(int64))
		out.Hour_1600 = int((*ret[33].(*interface{})).(int64))
		out.Hour_1630 = int((*ret[34].(*interface{})).(int64))
		out.Hour_1700 = int((*ret[35].(*interface{})).(int64))
		out.Hour_1730 = int((*ret[36].(*interface{})).(int64))
		out.Hour_1800 = int((*ret[37].(*interface{})).(int64))
		out.Hour_1830 = int((*ret[38].(*interface{})).(int64))
		out.Hour_1900 = int((*ret[39].(*interface{})).(int64))
		out.Hour_1930 = int((*ret[40].(*interface{})).(int64))
		out.Hour_2000 = int((*ret[41].(*interface{})).(int64))
		out.Hour_2030 = int((*ret[42].(*interface{})).(int64))
		out.Hour_2100 = int((*ret[43].(*interface{})).(int64))
		out.Hour_2130 = int((*ret[44].(*interface{})).(int64))
		out.Hour_2200 = int((*ret[45].(*interface{})).(int64))
		out.Hour_2230 = int((*ret[46].(*interface{})).(int64))
		out.Hour_2300 = int((*ret[47].(*interface{})).(int64))
		out.Hour_2330 = int((*ret[48].(*interface{})).(int64))
	}
}

// 游戏统计(小时)
type (
	gameStatHour_in struct {
		GameID    int    //游戏ID
		BeginTime string //开始时间
		EndTime   string //截止时间
	}

	gameStatHourInfo struct {
		ChineseName string // 游戏名称
		Hour_0      int    // 0点
		Hour_1      int    // 1点
		Hour_2      int    // 2点
		Hour_3      int    // 3点
		Hour_4      int    // 4点
		Hour_5      int    // 5点
		Hour_6      int    // 6点
		Hour_7      int    // 7点
		Hour_8      int    // 8点
		Hour_9      int    // 9点
		Hour_10     int    // 10点
		Hour_11     int    // 11点
		Hour_12     int    // 12点
		Hour_13     int    // 13点
		Hour_14     int    // 14点
		Hour_15     int    // 15点
		Hour_16     int    // 16点
		Hour_17     int    // 17点
		Hour_18     int    // 18点
		Hour_19     int    // 19点
		Hour_20     int    // 20点
		Hour_21     int    // 21点
		Hour_22     int    // 22点
		Hour_23     int    // 23点
	}

	gameStatHour_out struct {
		RecordCount int //总注册数
		List        []gameStatHourInfo
	}

	gameStatHour struct {
		database.Trans_base
		In  gameStatHour_in
		Out gameStatHour_out
	}
)

func NewGameStatHour() *gameStatHour {
	return &gameStatHour{}
}

func (this *gameStatHour) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_OnlineStat_GetHourList")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, this.In.GameID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]gameStatHourInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.ChineseName = (*ret[0].(*interface{})).(string)
		out.Hour_0 = int((*ret[1].(*interface{})).(int64))
		out.Hour_1 = int((*ret[2].(*interface{})).(int64))
		out.Hour_2 = int((*ret[3].(*interface{})).(int64))
		out.Hour_3 = int((*ret[4].(*interface{})).(int64))
		out.Hour_4 = int((*ret[5].(*interface{})).(int64))
		out.Hour_5 = int((*ret[6].(*interface{})).(int64))
		out.Hour_6 = int((*ret[7].(*interface{})).(int64))
		out.Hour_7 = int((*ret[8].(*interface{})).(int64))
		out.Hour_8 = int((*ret[9].(*interface{})).(int64))
		out.Hour_9 = int((*ret[10].(*interface{})).(int64))
		out.Hour_10 = int((*ret[11].(*interface{})).(int64))
		out.Hour_11 = int((*ret[12].(*interface{})).(int64))
		out.Hour_12 = int((*ret[13].(*interface{})).(int64))
		out.Hour_13 = int((*ret[14].(*interface{})).(int64))
		out.Hour_14 = int((*ret[15].(*interface{})).(int64))
		out.Hour_15 = int((*ret[16].(*interface{})).(int64))
		out.Hour_16 = int((*ret[17].(*interface{})).(int64))
		out.Hour_17 = int((*ret[18].(*interface{})).(int64))
		out.Hour_18 = int((*ret[19].(*interface{})).(int64))
		out.Hour_19 = int((*ret[20].(*interface{})).(int64))
		out.Hour_20 = int((*ret[21].(*interface{})).(int64))
		out.Hour_21 = int((*ret[22].(*interface{})).(int64))
		out.Hour_22 = int((*ret[23].(*interface{})).(int64))
		out.Hour_23 = int((*ret[24].(*interface{})).(int64))
	}
}

// 存量统计
type (
	moneyStatTotalList_in struct {
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	moneyStatTotalInfo struct {
		RowNumber          int    //序号
		Crdate             string //时间
		TotalCurrentAmount int64  //金币存量
		TotalBankAmount    int64  //保险柜存量
		TotalAmount        int64  //总存量
	}

	moneyStatTotalList_out struct {
		RecordCount int //总记录数
		List        []moneyStatTotalInfo
	}

	moneyStatTotalList struct {
		database.Trans_base
		In  moneyStatTotalList_in
		Out moneyStatTotalList_out
	}
)

func NewMoneyStatTotalList() *moneyStatTotalList {
	return &moneyStatTotalList{}
}

func (this *moneyStatTotalList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MoneyStatTotal_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]moneyStatTotalInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RowNumber = int((*ret[0].(*interface{})).(int64))
			out.Crdate = (*ret[1].(*interface{})).(string)
			out.TotalCurrentAmount = (*ret[2].(*interface{})).(int64)
			out.TotalBankAmount = (*ret[3].(*interface{})).(int64)
			out.TotalAmount = out.TotalCurrentAmount + out.TotalBankAmount
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 存量统计(每天)
type (
	moneyStatTotalListByDay_in struct {
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	moneyStatTotalByDayInfo struct {
		RowNumber          int    //序号
		Crdate             string //时间
		TotalAmount        int64  //总存量
		TotalBankAmount    int64  //保险柜存量
		TotalCurrentAmount int64  //金币存量
	}

	moneyStatTotalListByDay_out struct {
		RecordCount int //总记录数
		List        []moneyStatTotalByDayInfo
	}

	moneyStatTotalListByDay struct {
		database.Trans_base
		In  moneyStatTotalListByDay_in
		Out moneyStatTotalListByDay_out
	}
)

func NewMoneyStatTotalListByDay() *moneyStatTotalListByDay {
	return &moneyStatTotalListByDay{}
}

func (this *moneyStatTotalListByDay) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MoneyStatTotal_GetListByDay")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]moneyStatTotalByDayInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RowNumber = int((*ret[0].(*interface{})).(int64))
			out.Crdate = (*ret[1].(*interface{})).(string)
			out.TotalAmount = (*ret[2].(*interface{})).(int64)
			out.TotalBankAmount = (*ret[3].(*interface{})).(int64)
			out.TotalCurrentAmount = (*ret[4].(*interface{})).(int64)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 货币排行
type (
	currencyTop_in struct {
		PartnerID int // 渠道ID
		PageIndex int // 页索引
		PageSize  int // 页大小
	}

	currencyTopInfo struct {
		Rank        int     // 名次
		PartnerName string  // 渠道名称
		UserID      int     // 用户ID
		NickName    string  // 昵称
		GoldAmount  int     // 金币
		BankAmount  int     // 保险柜
		PayMoney    float64 // 充值金额
	}

	currencyTop_out struct {
		RecordCount int // 总记录数
		List        []currencyTopInfo
	}

	currencyTop struct {
		database.Trans_base
		In  currencyTop_in
		Out currencyTop_out
	}
)

func NewCurrencyTop() *currencyTop {
	return &currencyTop{}
}

func (this *currencyTop) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Currency_GetTop")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]currencyTopInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rank = int((*ret[0].(*interface{})).(int64))
			out.PartnerName = (*ret[1].(*interface{})).(string)
			out.UserID = int((*ret[2].(*interface{})).(int64))
			out.NickName = (*ret[3].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.GoldAmount = int((*ret[4].(*interface{})).(int64))
			out.BankAmount = int((*ret[5].(*interface{})).(int64))

			payMoney := string((*ret[6].(*interface{})).([]byte))
			out.PayMoney, _ = strconv.ParseFloat(payMoney, 64)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 货币变化
type (
	getUserMoneyStatList_in struct {
		UserID    int    //用户ID
		IsGameWin int    // 是否输赢
		BeginTime string //起始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	getUserMoneyStatListModel struct {
		RowNumber int    //行号
		UserID    int    //UserID
		NickName  string //昵称
		MoneySum  int    //金币变化
	}

	getUserMoneyStatList_out struct {
		RecordCount int //记录数
		TotalAmount int //总计
		List        []getUserMoneyStatListModel
	}

	getUserMoneyStatList struct {
		database.Trans_base
		In  getUserMoneyStatList_in
		Out getUserMoneyStatList_out
	}
)

func NewGetUserMoneyStatList() *getUserMoneyStatList {
	return &getUserMoneyStatList{}
}

func (this *getUserMoneyStatList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetMoneyStatList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@IsGameWin", database.AdParamInput, database.AdInteger, 4, this.In.IsGameWin)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@TotalAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.TotalAmount)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]getUserMoneyStatListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RowNumber = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.MoneySum = int((*ret[3].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.TotalAmount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
}

// 用户货币变化详情
type (
	getMoneyStatDetail_in struct {
		UserID    int    // 用户ID
		StartTime string // 开始时间
		EndTime   string // 截止时间
		IsGameWin int    // 是否游戏输入
	}

	getMoneyStatDetailModel struct {
		DataFlag    string `json:"dateFlag"`    //日期
		ChineseName string `json:"chineseName"` //游戏名
		LogTypeName string `json:"logTypeName"` //日志类型
		MoneySum    string `json:"moneySum"`    //数量
		UserID      int    `json:"userID"`      //用户ID
		NickName    string `json:"nickName"`    //昵称
	}

	getMoneyStatDetail_out struct {
		RecordCount int
		List        []getMoneyStatDetailModel
	}

	getMoneyStatDetail struct {
		database.Trans_base
		In  getMoneyStatDetail_in
		Out getMoneyStatDetail_out
	}
)

func NewGetMoneyStatDetail() *getMoneyStatDetail {
	return &getMoneyStatDetail{}
}

func (this *getMoneyStatDetail) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetMoneyStatDetail")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@StartTime", database.AdParamInput, database.AdVarChar, 20, this.In.StartTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@IsGameWin", database.AdParamInput, database.AdInteger, 4, this.In.IsGameWin)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
	this.State = true
	this.Out.List = make([]getMoneyStatDetailModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DataFlag = (*ret[0].(*interface{})).(string)
		out.ChineseName = (*ret[1].(*interface{})).(string)
		out.LogTypeName = (*ret[2].(*interface{})).(string)
		moneySum := int((*ret[3].(*interface{})).(int64))
		out.MoneySum = strconv.Itoa(moneySum)
		out.UserID = int((*ret[4].(*interface{})).(int64))
		out.NickName = (*ret[5].(*interface{})).(string)
		if info := tagMgr.getInfo(out.UserID); info != nil {
			out.NickName = info.NickName
		}
	}
}

// 游戏日常统计
type (
	gameDailyStat_in struct {
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	gameDailyStatModel struct {
		DateFlag       string //时间
		ServerName     string //服务器名称
		AvgOnlineCount int    //平均在线人数
		MaxOnlineCount int    //最高在线人数
		BetAmount      int    //消耗金币
		ResultAmount   int    //产生金币
		ActiveCount    int    //活跃用户(指有消耗的用户数)
	}

	gameDailyStat_out struct {
		RecordCount int //记录数
		List        []gameDailyStatModel
	}

	gameDailyStat struct {
		database.Trans_base
		In  gameDailyStat_in
		Out gameDailyStat_out
	}
)

func NewGameDailyStat() *gameDailyStat {
	return &gameDailyStat{}
}

func (this *gameDailyStat) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_GameDailyStat_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]gameDailyStatModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.ServerName = (*ret[1].(*interface{})).(string)
			out.AvgOnlineCount = int((*ret[2].(*interface{})).(int64))
			out.MaxOnlineCount = int((*ret[3].(*interface{})).(int64))
			out.BetAmount = int((*ret[4].(*interface{})).(int64))
			out.ResultAmount = int((*ret[5].(*interface{})).(int64))
			out.ActiveCount = int((*ret[6].(*interface{})).(int64))
		}
	}
	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 后台管理--导出金币变化
type (
	moneyStatExport_in struct {
		BeginTime string //开始时间
		EndTime   string //截止时间
	}

	moneyStatExportModel struct {
		UserID       int    //用户ID
		NickName     string //昵称
		PartnerName  string //渠道名称
		InvestAmount int    //投注金额
		WinAmount    int    //赢金额
		SubAmount    int    //变化金额
	}

	moneyStatExport_out struct {
		RecordCount int //记录数
		List        []moneyStatExportModel
	}

	moneyStatExport struct {
		database.Trans_base
		In  moneyStatExport_in
		Out moneyStatExport_out
	}
)

func NewMoneyStatExport() *moneyStatExport {
	return &moneyStatExport{}
}

func (this *moneyStatExport) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_ExportMoneyStat")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}
	this.Out.List = make([]moneyStatExportModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.UserID = int((*ret[0].(*interface{})).(int64))
		out.NickName = (*ret[1].(*interface{})).(string)
		if info := tagMgr.getInfo(out.UserID); info != nil {
			out.NickName = info.NickName
		}
		out.PartnerName = (*ret[2].(*interface{})).(string)
		out.InvestAmount = int((*ret[3].(*interface{})).(int64))
		out.WinAmount = int((*ret[4].(*interface{})).(int64))
		out.SubAmount = out.InvestAmount + out.WinAmount
	}
}

// 用户日常统计列表
type (
	userDailyStat_in struct {
		UserID    int    //用户ID
		IsSleep   int    //是否沉睡用户 (1=沉睡用户)
		BeginTime string //开始时间
		EndTime   string //截止时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	userDailyStatModel struct {
		Rid             int    //标识
		DateFlag        string //日期标识
		UserID          int    //用户ID
		NickName        string //昵称
		WinAmount       int    //赢金币
		LoseAmount      int    //输金币
		StillAmount     int    //剩余金额
		SubAmount       int
		GameStart       string  //游戏开始时间
		GameEnd         string  //最后游戏时间
		PlaySeconds     int     //游戏时长
		PayMoney        float64 //充值金额
		PartnerName     string  //渠道名称
		LoginCount      int     //登录数
		RegTime         string  //注册时间
		VideoPlayTimes  int     //视频广告播放次数
		VideoSendAmount int     //视频广告赠送金币
	}

	userDailyStat_out struct {
		RecordCount int //记录数
		List        []userDailyStatModel
	}

	userDailyStat struct {
		database.Trans_base
		In  userDailyStat_in
		Out userDailyStat_out
	}
)

func NewUserDailyStat() *userDailyStat {
	return &userDailyStat{}
}

func (this *userDailyStat) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserDailyStat_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@IsSleep", database.AdParamInput, database.AdInteger, 4, this.In.IsSleep)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}
	if rowLen > 1 {
		this.Out.List = make([]userDailyStatModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.DateFlag = (*ret[1].(*interface{})).(string)
			out.UserID = int((*ret[2].(*interface{})).(int64))
			out.NickName = (*ret[3].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.WinAmount = int((*ret[4].(*interface{})).(int64))
			out.LoseAmount = int((*ret[5].(*interface{})).(int64))
			out.StillAmount = int((*ret[6].(*interface{})).(int64))
			out.GameStart = (*ret[7].(*interface{})).(string)
			out.GameEnd = (*ret[8].(*interface{})).(string)
			out.PlaySeconds = int((*ret[9].(*interface{})).(int64))

			payMoney := string((*ret[10].(*interface{})).([]byte))
			out.PayMoney, _ = strconv.ParseFloat(payMoney, 64)

			out.PartnerName = (*ret[11].(*interface{})).(string)
			out.LoginCount = int((*ret[12].(*interface{})).(int64))
			out.RegTime = (*ret[13].(*interface{})).(string)
			out.SubAmount = out.WinAmount + out.LoseAmount
			if out.GameStart == "1970-01-01 00:00" {
				out.GameStart = ""
			}
			if out.GameEnd == "1970-01-01 00:00" {
				out.GameEnd = ""
			}
			out.VideoPlayTimes = int((*ret[14].(*interface{})).(int64))
			out.VideoSendAmount = int((*ret[15].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 税收总计
type (
	taxStatList_in struct {
		BeginTime string
		EndTime   string
	}

	taxStatListModel struct {
		ServerName string
		TaxSum     int
	}

	taxStatList_out struct {
		TotalTaxAmount int
		List           []taxStatListModel
	}

	taxStatList struct {
		database.Trans_base
		In  taxStatList_in
		Out taxStatList_out
	}
)

func NewTaxStatList() *taxStatList {
	return &taxStatList{}
}

func (this *taxStatList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_TaxStatTotal_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@TotalTaxAmount", database.AdParamOutput, database.AdBigint, 8, this.Out.TotalTaxAmount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]taxStatListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.ServerName = (*ret[0].(*interface{})).(string)
			out.TaxSum = int((*ret[1].(*interface{})).(int64))
		}
	}

	this.Out.TotalTaxAmount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 游戏记录报表详情
type (
	gameRecordReportDetail_in struct {
		BeginTime string
		EndTime   string
	}

	gameRecordReportDetailModel struct {
		GameID      int
		ChineseName string
		ServerName  string
		MoneySum    int
		TotalCount  int
		WinCount    int
		PlaySeconds int
		Players     int
	}

	gameRecordReportDetail_out struct {
		RecordCount int
		List        []gameRecordReportDetailModel
	}

	gameRecordReportDetail struct {
		database.Trans_base
		In  gameRecordReportDetail_in
		Out gameRecordReportDetail_out
	}
)

func NewGameRecordReportDetail() *gameRecordReportDetail {
	return &gameRecordReportDetail{}
}

func (this *gameRecordReportDetail) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_GameRecord_ReportDetail")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	this.Out.List = make([]gameRecordReportDetailModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.GameID = int((*ret[0].(*interface{})).(int64))
		out.ChineseName = (*ret[1].(*interface{})).(string)
		out.ServerName = (*ret[2].(*interface{})).(string)
		out.MoneySum = int((*ret[3].(*interface{})).(int64))
		out.TotalCount = int((*ret[4].(*interface{})).(int64))
		out.WinCount = int((*ret[5].(*interface{})).(int64))
		out.PlaySeconds = int((*ret[6].(*interface{})).(int64))
		out.Players = int((*ret[7].(*interface{})).(int64))
	}

	return
}

// 注册用户转化报表
type (
	regConvertReport_in struct {
		BeginTime string
		EndTime   string
	}

	regConvertReport_out struct {
		RegUsers       int //注册用户数
		LoginUsers     int //登录用户数
		PlayUsers      int //游戏用户数
		VideoReqUsers  int //广告请求用户数
		PayUsers       int //付费用户数
		VideoPlayUsers int //广告播放用户数
	}

	regConvertReport struct {
		database.Trans_base
		In  regConvertReport_in
		Out regConvertReport_out
	}
)

func NewRegConvertReport() *regConvertReport {
	return &regConvertReport{}
}

func (this *regConvertReport) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_NewUser_ConvertReport")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}
	ret := retRows[0]
	this.Out.RegUsers = int((*ret[0].(*interface{})).(int64))
	this.Out.LoginUsers = int((*ret[1].(*interface{})).(int64))
	this.Out.PlayUsers = int((*ret[2].(*interface{})).(int64))
	this.Out.VideoReqUsers = int((*ret[3].(*interface{})).(int64))
	this.Out.PayUsers = int((*ret[4].(*interface{})).(int64))
	this.Out.VideoPlayUsers = int((*ret[5].(*interface{})).(int64))
}

// 登录用户转化报表
type (
	loginConvertReport_in struct {
		DateFlag string
		IsReg    int
	}

	loginConvertReport_out struct {
		LoginUsers     int //登录用户数
		PlayUsers      int //游戏用户数
		VideoReqUsers  int //广告请求用户数
		PayUsers       int //付费用户数
		VideoPlayUsers int //广告播放用户数
	}

	loginConvertReport struct {
		database.Trans_base
		In  loginConvertReport_in
		Out loginConvertReport_out
	}
)

func NewLoginConvertReport() *loginConvertReport {
	return &loginConvertReport{}
}

func (this *loginConvertReport) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_ConvertReport")
	statement.AddParamter("@DateFlag", database.AdParamInput, database.AdVarChar, 20, this.In.DateFlag)
	statement.AddParamter("@IsReg", database.AdParamInput, database.AdInteger, 4, this.In.IsReg)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}
	ret := retRows[0]
	this.Out.LoginUsers = int((*ret[0].(*interface{})).(int64))
	this.Out.PlayUsers = int((*ret[1].(*interface{})).(int64))
	this.Out.VideoReqUsers = int((*ret[2].(*interface{})).(int64))
	this.Out.PayUsers = int((*ret[3].(*interface{})).(int64))
	this.Out.VideoPlayUsers = int((*ret[4].(*interface{})).(int64))
}

// 留存统计
type (
	liveStatList_in struct {
		PartnerID int    // 渠道ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	liveStatListModel struct {
		DateFlag     string // 日期标识
		PartnerName  string // 渠道名称
		RegCount     int    // 注册用户数
		LiveUsers_1  int    // 次日留存
		LiveUsers_3  int    // 3日留存
		LiveUsers_5  int    // 5日留存
		LiveUsers_7  int    // 7日留存
		LiveUsers_15 int    // 15日留存
		LiveUsers_30 int    // 30日留存
	}

	liveStatList_out struct {
		RecordCount int // 记录数
		List        []liveStatListModel
	}

	liveStatList struct {
		database.Trans_base
		In  liveStatList_in
		Out liveStatList_out
	}
)

func NewLiveStatList() *liveStatList {
	return &liveStatList{}
}

func (this *liveStatList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_LiveStat_GetList")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]liveStatListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.PartnerName = (*ret[1].(*interface{})).(string)
			out.RegCount = int((*ret[2].(*interface{})).(int64))
			out.LiveUsers_1 = int((*ret[3].(*interface{})).(int64))
			out.LiveUsers_3 = int((*ret[4].(*interface{})).(int64))
			out.LiveUsers_5 = int((*ret[5].(*interface{})).(int64))
			out.LiveUsers_7 = int((*ret[6].(*interface{})).(int64))
			out.LiveUsers_15 = int((*ret[7].(*interface{})).(int64))
			out.LiveUsers_30 = int((*ret[8].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 渠道留存统计
type (
	liveStatListByPartner_in struct {
		PartnerID int    // 渠道ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	liveStatListByPartnerModel struct {
		DateFlag    string // 日期标识
		PartnerName string // 渠道名称

		CreateUsers_1 int    // 昨天创建用户数
		LiveUsers_1   int    // 昨天创建,今天还登录的用户数
		Ratio_1       string // 昨天留存率

		CreateUsers_3 int    // 3天创建用户数
		LiveUsers_3   int    // 3天创建,今天还登录的用户数
		Ratio_3       string // 3天留存率

		CreateUsers_7 int    // 7天创建用户数
		LiveUsers_7   int    // 7天创建,今天还登录的用户数
		Ratio_7       string // 7天留存率

		CreateUsers_15 int    // 15天创建用户数
		LiveUsers_15   int    // 15天创建,今天还登录的用户数
		Ratio_15       string // 15天留存率

		CreateUsers_30 int    // 30天创建用户数
		LiveUsers_30   int    // 30天创建,今天还登录的用户数
		Ratio_30       string // 30天留存率
	}

	liveStatListByPartner_out struct {
		RecordCount int // 记录数
		List        []liveStatListByPartnerModel
	}

	liveStatListByPartner struct {
		database.Trans_base
		In  liveStatListByPartner_in
		Out liveStatListByPartner_out
	}
)

func NewLiveStatListByPartner() *liveStatListByPartner {
	return &liveStatListByPartner{}
}

func (this *liveStatListByPartner) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_LiveStat_GetListByPartner")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]liveStatListByPartnerModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)

			out.CreateUsers_1 = int((*ret[1].(*interface{})).(int64))
			out.LiveUsers_1 = int((*ret[2].(*interface{})).(int64))
			out.Ratio_1 = (*ret[3].(*interface{})).(string)

			out.CreateUsers_3 = int((*ret[4].(*interface{})).(int64))
			out.LiveUsers_3 = int((*ret[5].(*interface{})).(int64))
			out.Ratio_3 = (*ret[6].(*interface{})).(string)

			out.CreateUsers_7 = int((*ret[7].(*interface{})).(int64))
			out.LiveUsers_7 = int((*ret[8].(*interface{})).(int64))
			out.Ratio_7 = (*ret[9].(*interface{})).(string)

			out.CreateUsers_15 = int((*ret[10].(*interface{})).(int64))
			out.LiveUsers_15 = int((*ret[11].(*interface{})).(int64))
			out.Ratio_15 = (*ret[12].(*interface{})).(string)

			out.CreateUsers_30 = int((*ret[13].(*interface{})).(int64))
			out.LiveUsers_30 = int((*ret[14].(*interface{})).(int64))
			out.Ratio_30 = (*ret[15].(*interface{})).(string)

			out.PartnerName = (*ret[16].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 渠道登录统计
type (
	loginStatPartner_in struct {
		PartnerID int    // 渠道ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	loginStatPartnerModel struct {
		DateFlag    string // 日期
		PartnerName string // 渠道名称
		LoginCount  int    // 登录数
		PartnerID   int    // 渠道ID
	}

	loginStatPartner_out struct {
		RecordCount int
		List        []loginStatPartnerModel
	}

	loginStatPartner struct {
		database.Trans_base
		In  loginStatPartner_in
		Out loginStatPartner_out
	}
)

func NewLoginStatPartner() *loginStatPartner {
	return &loginStatPartner{}
}

func (this *loginStatPartner) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserLoginLog_GetStatListByPartner")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]loginStatPartnerModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.PartnerName = (*ret[1].(*interface{})).(string)
			out.LoginCount = int((*ret[2].(*interface{})).(int64))
			out.PartnerID = int((*ret[3].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 在线统计报表(小时)
type (
	onlineStatReportChip_in struct {
		GameID    int    //游戏ID
		BeginTime string //开始时间
		EndTime   string //截止时间
	}

	onlineStatReportChipModel struct {
		DateFlag string //日期标志

		Hour_0  int //0点
		Hour_1  int //1点
		Hour_2  int //2点
		Hour_3  int //3点
		Hour_4  int //4点
		Hour_5  int //5点
		Hour_6  int //6点
		Hour_7  int //7点
		Hour_8  int //8点
		Hour_9  int //9点
		Hour_10 int //10点
		Hour_11 int //11点
		Hour_12 int //12点
		Hour_13 int //13点
		Hour_14 int //14点
		Hour_15 int //15点
		Hour_16 int //16点
		Hour_17 int //17点
		Hour_18 int //18点
		Hour_19 int //19点
		Hour_20 int //20点
		Hour_21 int //21点
		Hour_22 int //22点
		Hour_23 int //23点
	}

	onlineStatReportChip_out struct {
		List []onlineStatReportChipModel
	}

	onlineStatReportChip struct {
		database.Trans_base
		In  onlineStatReportChip_in
		Out onlineStatReportChip_out
	}
)

func NewOnlineStatReportChip() *onlineStatReportChip {
	return &onlineStatReportChip{}
}

func (this *onlineStatReportChip) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_OnlineStatChip_GetReport")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]onlineStatReportChipModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.Hour_0 = int((*ret[1].(*interface{})).(int64))
		out.Hour_1 = int((*ret[2].(*interface{})).(int64))
		out.Hour_2 = int((*ret[3].(*interface{})).(int64))
		out.Hour_3 = int((*ret[4].(*interface{})).(int64))
		out.Hour_4 = int((*ret[5].(*interface{})).(int64))
		out.Hour_5 = int((*ret[6].(*interface{})).(int64))
		out.Hour_6 = int((*ret[7].(*interface{})).(int64))
		out.Hour_7 = int((*ret[8].(*interface{})).(int64))
		out.Hour_8 = int((*ret[9].(*interface{})).(int64))
		out.Hour_9 = int((*ret[10].(*interface{})).(int64))
		out.Hour_10 = int((*ret[11].(*interface{})).(int64))
		out.Hour_11 = int((*ret[12].(*interface{})).(int64))
		out.Hour_12 = int((*ret[13].(*interface{})).(int64))
		out.Hour_13 = int((*ret[14].(*interface{})).(int64))
		out.Hour_14 = int((*ret[15].(*interface{})).(int64))
		out.Hour_15 = int((*ret[16].(*interface{})).(int64))
		out.Hour_16 = int((*ret[17].(*interface{})).(int64))
		out.Hour_17 = int((*ret[18].(*interface{})).(int64))
		out.Hour_18 = int((*ret[19].(*interface{})).(int64))
		out.Hour_19 = int((*ret[20].(*interface{})).(int64))
		out.Hour_20 = int((*ret[21].(*interface{})).(int64))
		out.Hour_21 = int((*ret[22].(*interface{})).(int64))
		out.Hour_22 = int((*ret[23].(*interface{})).(int64))
		out.Hour_23 = int((*ret[24].(*interface{})).(int64))
	}
}

// 同时在线统计报表(小时)
type (
	onlineUserReportChip_in struct {
		BeginTime string // 开始时间
		EndTime   string // 截止时间
	}

	onlineUserReportChipModel struct {
		DateFlag string // 日期标志

		Hour_0000 int // 00:00
		Hour_0030 int // 00:30

		Hour_0100 int // 01:00
		Hour_0130 int // 01:30

		Hour_0200 int // 02:00
		Hour_0230 int // 02:30

		Hour_0300 int // 03:00
		Hour_0330 int // 03:30

		Hour_0400 int // 04:00
		Hour_0430 int // 04:30

		Hour_0500 int // 05:00
		Hour_0530 int // 05:30

		Hour_0600 int // 06:00
		Hour_0630 int // 06:30

		Hour_0700 int // 07:00
		Hour_0730 int // 07:30

		Hour_0800 int // 08:00
		Hour_0830 int // 08:30

		Hour_0900 int // 09:00
		Hour_0930 int // 09:30

		Hour_1000 int // 10:00
		Hour_1030 int // 10:30

		Hour_1100 int // 11:00
		Hour_1130 int // 11:30

		Hour_1200 int // 12:00
		Hour_1230 int // 12:30

		Hour_1300 int // 13:00
		Hour_1330 int // 13:30

		Hour_1400 int // 14:00
		Hour_1430 int // 14:30

		Hour_1500 int // 15:00
		Hour_1530 int // 15:30

		Hour_1600 int // 16:00
		Hour_1630 int // 16:30

		Hour_1700 int // 17:00
		Hour_1730 int // 17:30

		Hour_1800 int // 18:00
		Hour_1830 int // 18:30

		Hour_1900 int // 19:00
		Hour_1930 int // 19:30

		Hour_2000 int // 20:00
		Hour_2030 int // 20:30

		Hour_2100 int // 21:00
		Hour_2130 int // 21:30

		Hour_2200 int // 22:00
		Hour_2230 int // 22:30

		Hour_2300 int // 23:00
		Hour_2330 int // 23:30
	}

	onlineUserReportChip_out struct {
		List []onlineUserReportChipModel
	}

	onlineUserReportChip struct {
		database.Trans_base
		In  onlineUserReportChip_in
		Out onlineUserReportChip_out
	}
)

func NewOnlineUserReportChip() *onlineUserReportChip {
	return &onlineUserReportChip{}
}

func (this *onlineUserReportChip) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_OnlineUserChip_GetReport")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]onlineUserReportChipModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.Hour_0000 = int((*ret[1].(*interface{})).(int64))
		out.Hour_0030 = int((*ret[2].(*interface{})).(int64))
		out.Hour_0100 = int((*ret[3].(*interface{})).(int64))
		out.Hour_0130 = int((*ret[4].(*interface{})).(int64))
		out.Hour_0200 = int((*ret[5].(*interface{})).(int64))
		out.Hour_0230 = int((*ret[6].(*interface{})).(int64))
		out.Hour_0300 = int((*ret[7].(*interface{})).(int64))
		out.Hour_0330 = int((*ret[8].(*interface{})).(int64))
		out.Hour_0400 = int((*ret[9].(*interface{})).(int64))
		out.Hour_0430 = int((*ret[10].(*interface{})).(int64))
		out.Hour_0500 = int((*ret[11].(*interface{})).(int64))
		out.Hour_0530 = int((*ret[12].(*interface{})).(int64))
		out.Hour_0600 = int((*ret[13].(*interface{})).(int64))
		out.Hour_0630 = int((*ret[14].(*interface{})).(int64))
		out.Hour_0700 = int((*ret[15].(*interface{})).(int64))
		out.Hour_0730 = int((*ret[16].(*interface{})).(int64))
		out.Hour_0800 = int((*ret[17].(*interface{})).(int64))
		out.Hour_0830 = int((*ret[18].(*interface{})).(int64))
		out.Hour_0900 = int((*ret[19].(*interface{})).(int64))
		out.Hour_0930 = int((*ret[20].(*interface{})).(int64))
		out.Hour_1000 = int((*ret[21].(*interface{})).(int64))
		out.Hour_1030 = int((*ret[22].(*interface{})).(int64))
		out.Hour_1100 = int((*ret[23].(*interface{})).(int64))
		out.Hour_1130 = int((*ret[24].(*interface{})).(int64))
		out.Hour_1200 = int((*ret[25].(*interface{})).(int64))
		out.Hour_1230 = int((*ret[26].(*interface{})).(int64))
		out.Hour_1300 = int((*ret[27].(*interface{})).(int64))
		out.Hour_1330 = int((*ret[28].(*interface{})).(int64))
		out.Hour_1400 = int((*ret[29].(*interface{})).(int64))
		out.Hour_1430 = int((*ret[30].(*interface{})).(int64))
		out.Hour_1500 = int((*ret[31].(*interface{})).(int64))
		out.Hour_1530 = int((*ret[32].(*interface{})).(int64))
		out.Hour_1600 = int((*ret[33].(*interface{})).(int64))
		out.Hour_1630 = int((*ret[34].(*interface{})).(int64))
		out.Hour_1700 = int((*ret[35].(*interface{})).(int64))
		out.Hour_1730 = int((*ret[36].(*interface{})).(int64))
		out.Hour_1800 = int((*ret[37].(*interface{})).(int64))
		out.Hour_1830 = int((*ret[38].(*interface{})).(int64))
		out.Hour_1900 = int((*ret[39].(*interface{})).(int64))
		out.Hour_1930 = int((*ret[40].(*interface{})).(int64))
		out.Hour_2000 = int((*ret[41].(*interface{})).(int64))
		out.Hour_2030 = int((*ret[42].(*interface{})).(int64))
		out.Hour_2100 = int((*ret[43].(*interface{})).(int64))
		out.Hour_2130 = int((*ret[44].(*interface{})).(int64))
		out.Hour_2200 = int((*ret[45].(*interface{})).(int64))
		out.Hour_2230 = int((*ret[46].(*interface{})).(int64))
		out.Hour_2300 = int((*ret[47].(*interface{})).(int64))
		out.Hour_2330 = int((*ret[48].(*interface{})).(int64))
	}
}

// 游戏统计(小时)
type (
	gameStatHourChip_in struct {
		GameID    int    //游戏ID
		BeginTime string //开始时间
		EndTime   string //截止时间
	}

	gameStatHourChipInfo struct {
		ChineseName string // 游戏名称
		Hour_0      int    // 0点
		Hour_1      int    // 1点
		Hour_2      int    // 2点
		Hour_3      int    // 3点
		Hour_4      int    // 4点
		Hour_5      int    // 5点
		Hour_6      int    // 6点
		Hour_7      int    // 7点
		Hour_8      int    // 8点
		Hour_9      int    // 9点
		Hour_10     int    // 10点
		Hour_11     int    // 11点
		Hour_12     int    // 12点
		Hour_13     int    // 13点
		Hour_14     int    // 14点
		Hour_15     int    // 15点
		Hour_16     int    // 16点
		Hour_17     int    // 17点
		Hour_18     int    // 18点
		Hour_19     int    // 19点
		Hour_20     int    // 20点
		Hour_21     int    // 21点
		Hour_22     int    // 22点
		Hour_23     int    // 23点
	}

	gameStatHourChip_out struct {
		RecordCount int //总注册数
		List        []gameStatHourChipInfo
	}

	gameStatHourChip struct {
		database.Trans_base
		In  gameStatHourChip_in
		Out gameStatHourChip_out
	}
)

func NewGameStatHourChip() *gameStatHourChip {
	return &gameStatHourChip{}
}

func (this *gameStatHourChip) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_OnlineStatChip_GetHourList")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, this.In.GameID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]gameStatHourChipInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.ChineseName = (*ret[0].(*interface{})).(string)
		out.Hour_0 = int((*ret[1].(*interface{})).(int64))
		out.Hour_1 = int((*ret[2].(*interface{})).(int64))
		out.Hour_2 = int((*ret[3].(*interface{})).(int64))
		out.Hour_3 = int((*ret[4].(*interface{})).(int64))
		out.Hour_4 = int((*ret[5].(*interface{})).(int64))
		out.Hour_5 = int((*ret[6].(*interface{})).(int64))
		out.Hour_6 = int((*ret[7].(*interface{})).(int64))
		out.Hour_7 = int((*ret[8].(*interface{})).(int64))
		out.Hour_8 = int((*ret[9].(*interface{})).(int64))
		out.Hour_9 = int((*ret[10].(*interface{})).(int64))
		out.Hour_10 = int((*ret[11].(*interface{})).(int64))
		out.Hour_11 = int((*ret[12].(*interface{})).(int64))
		out.Hour_12 = int((*ret[13].(*interface{})).(int64))
		out.Hour_13 = int((*ret[14].(*interface{})).(int64))
		out.Hour_14 = int((*ret[15].(*interface{})).(int64))
		out.Hour_15 = int((*ret[16].(*interface{})).(int64))
		out.Hour_16 = int((*ret[17].(*interface{})).(int64))
		out.Hour_17 = int((*ret[18].(*interface{})).(int64))
		out.Hour_18 = int((*ret[19].(*interface{})).(int64))
		out.Hour_19 = int((*ret[20].(*interface{})).(int64))
		out.Hour_20 = int((*ret[21].(*interface{})).(int64))
		out.Hour_21 = int((*ret[22].(*interface{})).(int64))
		out.Hour_22 = int((*ret[23].(*interface{})).(int64))
		out.Hour_23 = int((*ret[24].(*interface{})).(int64))
	}
}

// 在线统计(每天)
type (
	onlineStatListByDayChip_in struct {
		PartnerID int    //合作商ID
		BeginTime string //开始时间
		EndTime   string //结束时间
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	onlineStatListByDayChipModel struct {
		DateFlag        string //日期标识
		OnlineUserCount int    //在线用户数
		PartnerName     string //渠道名称
		TimeFlag        string //时间标识
	}

	onlineStatListByDayChip_out struct {
		RecordCount int //记录数
		MaxCount    int //最高数
		MinCount    int //最低数
		AvgCount    int //平均数
		List        []onlineStatListByDayChipModel
	}

	onlineStatListByDayChip struct {
		database.Trans_base
		In  onlineStatListByDayChip_in
		Out onlineStatListByDayChip_out
	}
)

func NewOnlineStatListByDayChip() *onlineStatListByDayChip {
	return &onlineStatListByDayChip{}
}

func (this *onlineStatListByDayChip) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_OnlineStatChip_GetListByDay")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@MaxCount", database.AdParamOutput, database.AdInteger, 4, this.Out.MaxCount)
	statement.AddParamter("@MinCount", database.AdParamOutput, database.AdInteger, 4, this.Out.MinCount)
	statement.AddParamter("@AvgCount", database.AdParamOutput, database.AdInteger, 4, this.Out.AvgCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]onlineStatListByDayChipModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.OnlineUserCount = int((*ret[1].(*interface{})).(int64))
			out.PartnerName = (*ret[2].(*interface{})).(string)
			out.TimeFlag = (*ret[3].(*interface{})).(string)
		}
	}
	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	this.Out.MaxCount = int((*retRows[rowLen-1][1].(*interface{})).(int64))
	this.Out.MinCount = int((*retRows[rowLen-1][2].(*interface{})).(int64))
	this.Out.AvgCount = int((*retRows[rowLen-1][3].(*interface{})).(int64))
}

// 留存统计
type (
	retentionStatList_in struct {
		Item      string // 事项
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	retentionStatListModel struct {
		DateFlag     string // 日期标识
		Item         string // 事项
		UserCount    int    // 用户数
		LiveUsers_1  int    // 次日留存
		LiveUsers_3  int    // 3日留存
		LiveUsers_5  int    // 5日留存
		LiveUsers_7  int    // 7日留存
		LiveUsers_15 int    // 15日留存
		LiveUsers_30 int    // 30日留存
	}

	retentionStatList_out struct {
		RecordCount int // 记录数
		List        []retentionStatListModel
	}

	retentionStatList struct {
		database.Trans_base
		In  retentionStatList_in
		Out retentionStatList_out
	}
)

func NewRetentionStatList() *retentionStatList {
	return &retentionStatList{}
}

func (this *retentionStatList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_RetentionStat_GetList")
	statement.AddParamter("@Item", database.AdParamInput, database.AdVarChar, 32, this.In.Item)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlString := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]retentionStatListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.Item = (*ret[1].(*interface{})).(string)
			out.UserCount = int((*ret[2].(*interface{})).(int64))
			out.LiveUsers_1 = int((*ret[3].(*interface{})).(int64))
			out.LiveUsers_3 = int((*ret[4].(*interface{})).(int64))
			out.LiveUsers_5 = int((*ret[5].(*interface{})).(int64))
			out.LiveUsers_7 = int((*ret[6].(*interface{})).(int64))
			out.LiveUsers_15 = int((*ret[7].(*interface{})).(int64))
			out.LiveUsers_30 = int((*ret[8].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}
