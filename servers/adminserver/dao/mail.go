package dao

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

//留言板列表
type (
	serviceMessageList_in struct {
		UserID    int    // 用户ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
		Sort      int    // 排序(0=最新排序   1=最早排序)
	}

	serviceMessageListModel struct {
		RowNumber int    // 序号
		UserID    int    // 用户ID
		NickName  string // 昵称
		MailCount int    // 邮件数
		Crdate    string // 时间
		Msg       string // 邮件内容
	}

	serviceMessageList_out struct {
		RecordCount int // 记录数
		List        []serviceMessageListModel
	}

	serviceMessageList struct {
		database.Trans_base
		In  serviceMessageList_in
		Out serviceMessageList_out
	}
)

func NewServiceMessageList() *serviceMessageList {
	return &serviceMessageList{}
}

func (this *serviceMessageList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ServiceMessage_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@Sort", database.AdParamInput, database.AdInteger, 4, this.In.Sort)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]serviceMessageListModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RowNumber = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.MailCount = int((*ret[3].(*interface{})).(int64))
			out.Crdate = (*ret[4].(*interface{})).(string)
			out.Msg = (*ret[5].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

//后台获取用户留言详细信息
type (
	serviceDetailList_in struct {
		UserID    int    // 用户ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
		OpUserID  int    // 操作员ID
	}

	serviceDetailInfo struct {
		MessageID int    // 用户邮件ID
		UserID    int    // 用户ID
		NickName  string // 昵称
		Msg       string // 邮件内容
		IsRead    int    // 是否已读
		Crdate    string // 创建时间
		FaceUrl   string // 头像名次
		TransMsg  string // 译文
		TagName   string // 标签名称
	}

	serviceDetailList_out struct {
		RecordCount int // 记录数
		List        []serviceDetailInfo
	}

	serviceDetailList struct {
		database.Trans_base
		In  serviceDetailList_in
		Out serviceDetailList_out
	}
)

func NewServiceDetailList() *serviceDetailList {
	return &serviceDetailList{}
}

func (this *serviceDetailList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_serviceMessage_GetDetailList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]serviceDetailInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.MessageID = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.Msg = (*ret[3].(*interface{})).(string)
			out.IsRead = int((*ret[4].(*interface{})).(int64))
			out.Crdate = (*ret[5].(*interface{})).(string)
			out.FaceUrl = (*ret[6].(*interface{})).(string)
			out.TransMsg = (*ret[7].(*interface{})).(string)
			out.TagName = (*ret[8].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

//后台客服回复
type (
	serviceSend_in struct {
		OpUserID   int    // 用户ID
		OpUserName string // 开始时间
		ToUserID   int    // 截止时间
		Msg        string // 内容
		TransMsg   string // 译文
		IpAddress  string // IP
	}

	serviceSend struct {
		database.Trans_base
		In serviceSend_in
	}
)

func NewServiceSend() *serviceSend {
	return &serviceSend{}
}

func (this *serviceSend) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_serviceMessage_Send")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 20, this.In.ToUserID)
	statement.AddParamter("@Msg", database.AdParamInput, database.AdNVarChar, 512, this.In.Msg)
	statement.AddParamter("@TransMsg", database.AdParamInput, database.AdNVarChar, 512, this.In.TransMsg)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
}

//系统广播列表
type (
	timeBroadcastList_in struct {
		PageIndex int //页索引
		PageSize  int //页大小
	}

	timeBroadcastInfo struct {
		Rid          int    //标识
		OpUserID     int    //操作员名称
		OpUserName   string //操作员名称
		BeginDate    string //开始日期
		BeginTime    string //截止日期
		EndDate      string //截止时间
		EndTime      string //截止时间
		InterMinutes int    //间隔分钟
		Msg          string //消息
		UpdateTime   string //更新时间
		Crdate       string //创建时间
	}

	timeBroadcastList_out struct {
		RecordCount int //记录数
		List        []timeBroadcastInfo
	}

	timeBroadcastList struct {
		database.Trans_base
		In  timeBroadcastList_in
		Out timeBroadcastList_out
	}
)

func NewTimeBroadcastList() *timeBroadcastList {
	return &timeBroadcastList{}
}

func (this *timeBroadcastList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_TimeBroadcast_GetList")
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]timeBroadcastInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.OpUserID = int((*ret[1].(*interface{})).(int64))
			out.OpUserName = (*ret[2].(*interface{})).(string)
			out.BeginDate = (*ret[3].(*interface{})).(string)
			out.BeginTime = (*ret[4].(*interface{})).(string)
			out.EndDate = (*ret[5].(*interface{})).(string)
			out.EndTime = (*ret[6].(*interface{})).(string)
			out.InterMinutes = int((*ret[7].(*interface{})).(int64))
			out.Msg = (*ret[8].(*interface{})).(string)
			out.UpdateTime = (*ret[9].(*interface{})).(string)
			out.Crdate = (*ret[10].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

//定时广播发送
type (
	timeBroadcastSend_in struct {
		OpUserID     int    //操作员ID
		OpUserName   string //操作员名称
		BeginDate    string //开始日期
		BeginTime    string //开始时间
		EndDate      string //截止日期
		EndTime      string //截止时间
		InterMinutes int    //备注
		Msg          string //消息
	}

	timeBroadcastSend struct {
		database.Trans_base
		In timeBroadcastSend_in
	}
)

func NewTimeBroadcastSend() *timeBroadcastSend {
	return &timeBroadcastSend{}
}

func (this *timeBroadcastSend) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_TimeBroadcast_Send")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@BeginDate", database.AdParamInput, database.AdVarChar, 16, this.In.BeginDate)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 16, this.In.BeginTime)
	statement.AddParamter("@EndDate", database.AdParamInput, database.AdVarChar, 16, this.In.EndDate)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 16, this.In.EndTime)
	statement.AddParamter("@InterMinutes", database.AdParamInput, database.AdInteger, 4, this.In.InterMinutes)
	statement.AddParamter("@Msg", database.AdParamInput, database.AdVarChar, 512, this.In.Msg)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
}

//定时广播删除
type (
	timeBroadcastDel_in struct {
		Rid int //标识
	}

	timeBroadcastDel struct {
		database.Trans_base
		In timeBroadcastDel_in
	}
)

func NewTimeBroadcastDel() *timeBroadcastDel {
	return &timeBroadcastDel{}
}

func (this *timeBroadcastDel) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_TimeBroadcast_Del")
	statement.AddParamter("@Rid", database.AdParamInput, database.AdInteger, 4, this.In.Rid)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}
}

//小红点提示
type (
	TipInfo struct {
		MessageCount  int // 消息数
		AuditCount    int // 提现审核条数
		FailCount     int // 提现失败条数
		AlertCount    int // 警报条数
		ExchangeCount int // 兑换数
		UserWordCount int // 个性签名数
		FeedbackCount int // 反馈数
	}

	tip_out struct {
		List []TipInfo
	}

	tip struct {
		database.Trans_base
		Out tip_out
	}
)

func NewTip() *tip {
	return &tip{}
}

func (this *tip) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Tip_GetInfo")
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.List = make([]TipInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.MessageCount = int((*ret[0].(*interface{})).(int64))
		out.ExchangeCount = int((*ret[1].(*interface{})).(int64))
		out.UserWordCount = int((*ret[2].(*interface{})).(int64))
		out.AuditCount = int((*ret[3].(*interface{})).(int64))
		out.FailCount = int((*ret[4].(*interface{})).(int64))
		out.FeedbackCount = int((*ret[5].(*interface{})).(int64))
	}
}

//发送系统消息
type (
	sysMessageSend_in struct {
		OpUserID    int    //操作员ID
		OpUserName  string //操作员名称
		Title       string //标题
		Content     string //内容
		UserID      int    //用户ID
		ItemID_1    int    //道具ID1
		ItemCount_1 int    //道具数1
		ItemID_2    int    //道具ID2
		ItemCount_2 int    //道具数2
		ItemID_3    int    //道具ID3
		ItemCount_3 int    //道具数3
		ItemID_4    int    //道具ID4
		ItemCount_4 int    //道具数4
		ItemID_5    int    //道具ID5
		ItemCount_5 int    //道具数5
		Tools       string //附件(json格式)
		IpAddress   string //IP地址
		MsgType     int    //邮件类型(0=普通邮件  1=重要邮件)
	}

	sysMessageSend_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	sysMessageSend struct {
		database.Trans_base
		In  sysMessageSend_in
		Out sysMessageSend_out
	}
)

func NewSysMessageSend() *sysMessageSend {
	return &sysMessageSend{}
}

func (this *sysMessageSend) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover error %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Manage_SysMessage_Send")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@Title", database.AdParamInput, database.AdNVarChar, 32, this.In.Title)
	statement.AddParamter("@Content", database.AdParamInput, database.AdNVarChar, 256, this.In.Content)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@Tools", database.AdParamInput, database.AdVarChar, 1024, this.In.Tools)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@MsgType", database.AdParamInput, database.AdInteger, 4, this.In.MsgType)
	sqlstring := statement.GenSql()
	CenterDB.ExecSql(sqlstring)
	this.State = true
	this.Out.RetCode = 1
	this.Out.ErrorMsg = "发送成功"
	return
}

//获取系统消息列表
type (
	sysMessageList_in struct {
		UserID     int    //用户ID
		ToolTypeID int    //道具类型ID
		BeginTime  string //开始时间
		EndTime    string //截止时间
		PageIndex  int    //页索引
		PageSize   int    //页大小
	}

	sysMessageInfo struct {
		SysMsgID   int    //系统消息ID
		UserID     int    //用户ID
		NickName   string //昵称
		Title      string //标题
		Content    string //内容
		Status     int    //状态
		SourceName string //源名称
		Tools      string //道具(json格式)
		Crdate     string //时间
		//ToolID     int    //道具ID
		//ToolName   string //道具名称
		//ToolNum    int    //道具数
	}

	sysMessageList_out struct {
		RecordCount int //记录数
		List        []sysMessageInfo
	}

	sysMessageList struct {
		database.Trans_base
		In  sysMessageList_in
		Out sysMessageList_out
	}
)

func NewSysMessageList() *sysMessageList {
	return &sysMessageList{}
}

func (this *sysMessageList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_SysMessage_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]sysMessageInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.SysMsgID = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.Title = (*ret[3].(*interface{})).(string)
			out.Content = (*ret[4].(*interface{})).(string)
			out.Status = int((*ret[5].(*interface{})).(int64))
			out.SourceName = (*ret[6].(*interface{})).(string)
			out.Tools = (*ret[7].(*interface{})).(string)
			out.Crdate = (*ret[8].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 客服留言翻译
type (
	msgTranslate_in struct {
		OpUserID   int    // 操作员ID
		OpUserName string // 操作员名称
		MessageID  int    // 留言ID
		TransMsg   string // 译文
		TagID      int    // 标签ID
	}

	msgTranslate struct {
		database.Trans_base
		In msgTranslate_in
	}
)

func NewMsgTranslate() *msgTranslate {
	return &msgTranslate{}
}

func (this *msgTranslate) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ServiceMessage_Translate")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@MessageID", database.AdParamInput, database.AdInteger, 4, this.In.MessageID)
	statement.AddParamter("@TransMsg", database.AdParamInput, database.AdVarChar, 512, this.In.TransMsg)
	statement.AddParamter("@TagID", database.AdParamInput, database.AdInteger, 4, this.In.TagID)
	sqlstring := statement.GenSql()
	CenterDB.ExecSql(sqlstring)
}

// 留言标签列表
type (
	msgTagListModel struct {
		TagID   int    // 标签ID
		TagName string // 标签名称
	}

	msgTagList_out struct {
		RecordCount int // 记录数
		List        []msgTagListModel
	}

	msgTagList struct {
		database.Trans_base
		Out msgTagList_out
	}
)

func NewMsgTagList() *msgTagList {
	return &msgTagList{}
}

func (this *msgTagList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ServiceMessage_GetTagList")
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	this.Out.List = make([]msgTagListModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.TagID = int((*ret[0].(*interface{})).(int64))
		out.TagName = (*ret[1].(*interface{})).(string)
	}
}

// 留言标签统计
type (
	msgTagStat_in struct {
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	msgTagStatModel struct {
		DateFlag string // 日期标识
		TagID    int    // 标签ID
		TagName  string // 标签名称
		MsgCount int    // 留言数
	}

	msgTagStat_out struct {
		RecordCount int // 记录数
		List        []msgTagStatModel
	}

	msgTagStat struct {
		database.Trans_base
		In  msgTagStat_in
		Out msgTagStat_out
	}
)

func NewMsgTagStat() *msgTagStat {
	return &msgTagStat{}
}

func (this *msgTagStat) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ServiceMessage_TagStat")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]msgTagStatModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.TagID = int((*ret[1].(*interface{})).(int64))
			out.TagName = (*ret[2].(*interface{})).(string)
			out.MsgCount = int((*ret[3].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 根据标签获取列表
type (
	serviceMessageListByTag_in struct {
		TagID     int    // 标签ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	serviceMessageListByTagModel struct {
		MessageID int    // 留言ID
		UserID    int    // 用户ID
		NickName  string // 昵称
		Msg       string // 留言内容
		Crdate    string // 时间
		FaceUrl   string // 头像url
		TransMsg  string // 译文
	}

	serviceMessageListByTag_out struct {
		RecordCount int
		List        []serviceMessageListByTagModel
	}

	serviceMessageListByTag struct {
		database.Trans_base
		In  serviceMessageListByTag_in
		Out serviceMessageListByTag_out
	}
)

func NewServiceMessageListByTag() *serviceMessageListByTag {
	return &serviceMessageListByTag{}
}

func (this *serviceMessageListByTag) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ServiceMessage_GetListByTag")
	statement.AddParamter("@TagID", database.AdParamInput, database.AdInteger, 4, this.In.TagID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]serviceMessageListByTagModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.MessageID = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.Msg = (*ret[3].(*interface{})).(string)
			out.Crdate = (*ret[4].(*interface{})).(string)
			out.FaceUrl = (*ret[5].(*interface{})).(string)
			out.TransMsg = (*ret[6].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 客服留言信息
type (
	serviceMessageInfo_in struct {
		MessageID int // 留言ID
	}

	serviceMessageInfo_out struct {
		UserID   int    // 用户ID
		NickName string // 昵称
		Msg      string // 内容
		Crdate   string // 时间
		TransMsg string // 译文
		TagID    int    // 标签ID
	}

	serviceMessageInfo struct {
		database.Trans_base
		In  serviceMessageInfo_in
		Out serviceMessageInfo_out
	}
)

func NewServiceMessageInfo() *serviceMessageInfo {
	return &serviceMessageInfo{}
}

func (this *serviceMessageInfo) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ServiceMessage_GetInfo")
	statement.AddParamter("@MessageID", database.AdParamInput, database.AdInteger, 4, this.In.MessageID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	ret := retRows[0]
	this.Out.UserID = int((*ret[0].(*interface{})).(int64))
	this.Out.NickName = (*ret[1].(*interface{})).(string)
	if info := tagMgr.getInfo(this.Out.UserID); info != nil {
		this.Out.NickName = info.NickName
	}
	this.Out.Msg = (*ret[2].(*interface{})).(string)
	this.Out.Crdate = (*ret[3].(*interface{})).(string)
	this.Out.TransMsg = (*ret[4].(*interface{})).(string)
	this.Out.TagID = int((*ret[5].(*interface{})).(int64))
}

// 标签统计
type (
	serviceMessageTagStat_in struct {
		BeginTime string
		EndTime   string
	}

	serviceMessageTagStatModel struct {
		DateFlag string // 日期标识
		TagID    int    // 标签ID
		TagName  string // 标签名称
		MsgCount int    // 消息数
	}

	serviceMessageTagStat_out struct {
		RecordCount int
		List        []serviceMessageTagStatModel
	}

	serviceMessageTagStat struct {
		database.Trans_base
		In  serviceMessageTagStat_in
		Out serviceMessageTagStat_out
	}
)

func NewServiceMessageTagStat() *serviceMessageTagStat {
	return &serviceMessageTagStat{}
}

func (this *serviceMessageTagStat) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ServiceMessage_GetTagStat")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	this.Out.List = make([]serviceMessageTagStatModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.TagID = int((*ret[1].(*interface{})).(int64))
		out.TagName = (*ret[2].(*interface{})).(string)
		out.MsgCount = int((*ret[3].(*interface{})).(int64))
	}
}
