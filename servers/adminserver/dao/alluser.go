package dao

import (
	"bet24.com/servers/adminserver/ip"
	"fmt"
	"runtime/debug"
	"strconv"

	"bet24.com/database"
	"bet24.com/log"
)

// 查询用户信息
type (
	allUserList_in struct {
		Data      string //数据
		PartnerID int    //渠道ID
		UserID    int    //用户ID
		NickName  string //昵称
		PageIndex int    //页索引
		PageSize  int    //页大小
	}

	allUserInfo struct {
		PartnerName string //渠道名称
		UserID      int    //用户ID
		NickName    string //昵称
		Amount      int    //现金
		VipLevel    int    //Vip等级
		LoginTime   string //登陆时间
		PayMoney    string // 充值累计
		RegTime     string // 注册时间
		RegIP       string // 注册ip
	}

	allUserList_out struct {
		RecordCount int //记录数
		List        []allUserInfo
	}

	allUserLit struct {
		database.Trans_base
		In  allUserList_in
		Out allUserList_out
	}
)

func NewAllUserList() *allUserLit {
	return &allUserLit{}
}

func (this *allUserLit) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetList")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, this.In.NickName)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]allUserInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.PartnerName = (*ret[0].(*interface{})).(string)
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.Amount = int((*ret[3].(*interface{})).(int64))
			out.VipLevel = int((*ret[4].(*interface{})).(int64))
			out.LoginTime = (*ret[5].(*interface{})).(string)
			payMoney := string((*ret[6].(*interface{})).([]byte))
			fPayMoney, _ := strconv.ParseFloat(payMoney, 64)
			out.PayMoney = fmt.Sprintf("%.2f", fPayMoney)
			out.RegTime = (*ret[7].(*interface{})).(string)
			out.RegIP = (*ret[8].(*interface{})).(string)
			out.RegIP = ip.GetCountryAndRegion(out.RegIP, false)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 查询用户详情信息
type (
	userDetail_in struct {
		UserID int
	}

	userDetail_out struct {
		NickName       string      // 昵称
		PartnerName    string      // 渠道名称
		Amount         int         // 金币
		VipLevel       int         // VIP等级
		PayMoney       float64     // 累计充值
		RegTime        string      // 注册时间
		RegIP          string      // 注册IP
		IMei           string      // IMei
		LoginTime      string      // 登录时间
		LoginIP        string      // 登录IP
		FaceURL        string      // 头像URL
		IsMonthCard    string      // 月卡
		UserWords      string      // 个性签名
		Tel            string      // 手机号
		Deviceid       string      // 设备号
		ChipAmount     int         // 筹码数额
		ChipBank       int         // 筹码保险柜
		Lv             int         // 等级
		Exp            int         // 经验值
		TotalSeconds   int         // 总在线时长
		TodaySeconds   int         // 今天在线时长
		HigherUserID   int         // 代理ID
		HigherNickName string      // 代理昵称
		Source         string      // 广告系列来源。用来标识搜索引擎、简报名称或其他来源。示例： utm_source=google
		Medium         string      // 广告系列媒介。用来标识媒介，比如电子邮件或每次点击费用。示例：utm_medium=cpc
		Labels         interface{} // 用户标签
		DeviceName     string      // 设备型号
	}

	userDetail struct {
		database.Trans_base
		In  userDetail_in
		Out userDetail_out
	}
)

func NewUserDetail() *userDetail {
	return &userDetail{}
}

func (this *userDetail) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	this.State = true
	ret := retRows[0]
	this.Out.NickName = (*ret[0].(*interface{})).(string)
	this.Out.PartnerName = (*ret[1].(*interface{})).(string)
	this.Out.Amount = int((*ret[2].(*interface{})).(int64))
	this.Out.VipLevel = int((*ret[3].(*interface{})).(int64))

	payMoney := string((*ret[4].(*interface{})).([]byte))
	this.Out.PayMoney, _ = strconv.ParseFloat(payMoney, 64)

	this.Out.RegTime = (*ret[5].(*interface{})).(string)
	this.Out.RegIP = (*ret[6].(*interface{})).(string)
	this.Out.RegIP = ip.GetCountryAndRegion(this.Out.RegIP, false)
	this.Out.IMei = (*ret[7].(*interface{})).(string)
	this.Out.LoginTime = (*ret[8].(*interface{})).(string)
	this.Out.LoginIP = (*ret[9].(*interface{})).(string)
	this.Out.LoginIP = ip.GetCountryAndRegion(this.Out.LoginIP, false)
	this.Out.FaceURL = (*ret[10].(*interface{})).(string)
	this.Out.IsMonthCard = (*ret[11].(*interface{})).(string)
	if this.Out.IsMonthCard == "2021-01-01" {
		this.Out.IsMonthCard = "无"
	}
	this.Out.UserWords = (*ret[12].(*interface{})).(string)
	this.Out.Tel = (*ret[13].(*interface{})).(string)
	this.Out.Deviceid = (*ret[14].(*interface{})).(string)
	this.Out.ChipAmount = int((*ret[15].(*interface{})).(int64))
	this.Out.ChipBank = int((*ret[16].(*interface{})).(int64))
	this.Out.Lv = int((*ret[17].(*interface{})).(int64))
	this.Out.Exp = int((*ret[18].(*interface{})).(int64))
	this.Out.TotalSeconds = int((*ret[19].(*interface{})).(int64))
	this.Out.TodaySeconds = int((*ret[20].(*interface{})).(int64))
	this.Out.HigherUserID = int((*ret[21].(*interface{})).(int64))
	this.Out.HigherNickName = (*ret[22].(*interface{})).(string)
	this.Out.Source = (*ret[23].(*interface{})).(string)
	this.Out.Medium = (*ret[24].(*interface{})).(string)
	this.Out.DeviceName = (*ret[25].(*interface{})).(string)

	return
}

// 查询被封用户列表
type (
	forbidUserList_in struct {
		UserID    int //用户ID
		PageIndex int //页索引
		PageSize  int //页大小
	}

	forbidUserInfo struct {
		Rid       int    //标识
		UserID    int    //用户ID
		NickName  string //昵称
		Memo      string //备注
		BeginTime string //开始时间
		EndTime   string //结束时间

		GameIDs string //游戏ID集
	}

	forbidUserList_out struct {
		RecordCount int //记录数
		List        []forbidUserInfo
	}

	forbidUserList struct {
		database.Trans_base
		In  forbidUserList_in
		Out forbidUserList_out
	}
)

func NewForbidUserList() *forbidUserList {
	return &forbidUserList{}
}

func (this *forbidUserList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ForbidUser_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]forbidUserInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.Memo = (*ret[3].(*interface{})).(string)
			out.BeginTime = (*ret[4].(*interface{})).(string)
			out.EndTime = (*ret[5].(*interface{})).(string)
			out.GameIDs = (*ret[6].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 封杀玩家
type (
	forbidUserAdd_in struct {
		OpUserID   int    //操作员ID
		OpUserName string //操作员名称
		UserID     int    //用户ID
		Memo       string //备注
		Days       int    //封杀天数
		IpAddress  string //IP地址
		GameIDs    string //游戏ID集(以逗号分隔)
	}

	forbidUserAdd_out struct {
		RetCode  int    //操作结果1成功 2已经封杀
		ErrorMsg string //操作描述
	}

	forbidUserAdd struct {
		database.Trans_base
		In  forbidUserAdd_in
		Out forbidUserAdd_out
	}
)

func NewForbidUserAdd() *forbidUserAdd {
	return &forbidUserAdd{}
}

func (this *forbidUserAdd) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ForbidUser_Add")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 20, this.In.UserID)
	statement.AddParamter("@Memo", database.AdParamInput, database.AdNVarChar, 256, this.In.Memo)
	statement.AddParamter("@Days", database.AdParamInput, database.AdInteger, 4, this.In.Days)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@GameIDs", database.AdParamInput, database.AdVarChar, 128, this.In.GameIDs)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 16, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	ret := retRows[0]
	this.Out.RetCode = int((*ret[0].(*interface{})).(int64))
}

// 解除封杀
type (
	forbidUserDel_in struct {
		OpUserID   int    //操作员ID
		OpUserName string //操作员名称
		UserID     int    //用户ID
		Memo       string //备注
		IpAddress  string //IP地址
	}

	forbidUserDel_out struct {
		GameIDs string //游戏ID集(以逗号分隔)
	}

	forbidUserDel struct {
		database.Trans_base
		In  forbidUserDel_in
		Out forbidUserDel_out
	}
)

func NewForbidUserDel() *forbidUserDel {
	return &forbidUserDel{}
}

func (this *forbidUserDel) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ForbidUser_Del")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 20, this.In.UserID)
	statement.AddParamter("@Memo", database.AdParamInput, database.AdVarChar, 256, this.In.Memo)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@GameIDs", database.AdParamOutput, database.AdVarChar, 128, this.Out.GameIDs)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.GameIDs = (*retRows[0][0].(*interface{})).(string)
}

// 绑码日志
type (
	userSimpleInfo_in struct {
		UserID   int    //用户ID
		NickName string //昵称
	}

	userSimpleInfo_out struct {
		UserID     int    //用户ID
		NickName   string //昵称
		Amount     int    //金额
		Bank       int    //保险柜
		ChipAmount int    // 筹码金额
		ChipBank   int    // 筹码保险柜
	}

	userSimpleInfo struct {
		database.Trans_base
		In  userSimpleInfo_in
		Out userSimpleInfo_out
	}
)

func NewUserSimpleInfo() *userSimpleInfo {
	return &userSimpleInfo{}
}

func (this *userSimpleInfo) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetSimpleInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, this.In.NickName)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true

	ret := retRows[0]

	this.Out.UserID = int((*ret[0].(*interface{})).(int64))
	this.Out.NickName = (*ret[1].(*interface{})).(string)
	if info := tagMgr.getInfo(this.Out.UserID); info != nil {
		this.Out.NickName = info.NickName
	}
	this.Out.Amount = int((*ret[2].(*interface{})).(int64))
	this.Out.Bank = int((*ret[3].(*interface{})).(int64))
	this.Out.ChipAmount = int((*ret[4].(*interface{})).(int64))
	this.Out.ChipBank = int((*ret[5].(*interface{})).(int64))
}

// 实时在线
type (
	casinoOnlineList_in struct {
		UserID    int // 用户ID
		GameID    int // 游戏ID
		PartnerID int // 渠道ID
		PageIndex int // 页索引
		PageSize  int // 页大小
		IsChip    int // 是否元宝(-1=所有 0=休闲 1=元宝)
	}

	casinoOnlineInfo struct {
		UserID      int    // 用户ID
		NickName    string // 昵称
		ChineseName string // 游戏名称
		LoginTime   string // 登陆时间
		ServerName  string // 服务器名称
		IPAddress   string // IP地址
		PartnerName string // 渠道名称
		GameType    string // 游戏类型
		IsChip      int    // 是否元宝
	}

	casinoOnlineList_out struct {
		RecordCount int // 记录数
		List        []casinoOnlineInfo
	}

	casinoOnlineList struct {
		database.Trans_base
		In  casinoOnlineList_in
		Out casinoOnlineList_out
	}
)

func NewCasinoOnlineList() *casinoOnlineList {
	return &casinoOnlineList{}
}

func (this *casinoOnlineList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_CasinoOnline_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, this.In.GameID)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@IsChip", database.AdParamInput, database.AdInteger, 4, this.In.IsChip)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true

	if rowLen > 1 {
		this.Out.List = make([]casinoOnlineInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.UserID = int((*ret[0].(*interface{})).(int64))
			out.NickName = (*ret[1].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.LoginTime = (*ret[2].(*interface{})).(string)
			out.ServerName = (*ret[3].(*interface{})).(string)
			out.IPAddress = (*ret[4].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.PartnerName = (*ret[5].(*interface{})).(string)
			out.GameType = (*ret[6].(*interface{})).(string)
			out.ChineseName = (*ret[7].(*interface{})).(string)
			out.IsChip = int((*ret[8].(*interface{})).(int64))
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 获取用户ID(根据昵称、手机号)
type (
	getUserID_in struct {
		UserID   int    //用户ID
		NickName string //昵称
	}

	getUserID_out struct {
		NewUserID int //用户ID
	}

	getUserID struct {
		database.Trans_base
		In  getUserID_in
		Out getUserID_out
	}
)

func NewGetUserID() *getUserID {
	return &getUserID{}
}

func (this *getUserID) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover error %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetUserID")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, this.In.NickName)
	statement.AddParamter("@NewUserID", database.AdParamOutput, database.AdInteger, 4, this.Out.NewUserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.NewUserID = int((*retRows[0][0].(*interface{})).(int64))
}

// 删除账号（后台）
type (
	delUser_in struct {
		OpUserID   int    //操作员ID
		OpUserName string //操作员名称
		UserID     int    //用户ID
		IpAddress  string //IP地址
	}

	delUser_out struct {
		RetCode  int    //操作结果
		ErrorMsg string //操作描述
	}

	delUser struct {
		database.Trans_base
		In  delUser_in
		Out delUser_out
	}
)

func NewDelUser() *delUser {
	return &delUser{}
}

func (this *delUser) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_Del")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 32, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
	return
}

// 绑定facebook
type (
	bindFacebook_in struct {
		OpUserID   int    // 操作员ID
		OpUserName string // 操作员名称
		UserID_1   int    // 用户ID_1
		UserID_2   int    // 用户ID_2
		IpAddress  string // IP地址
	}

	bindFacebook_out struct {
		RetCode int
	}

	bindFacebook struct {
		database.Trans_base
		In  bindFacebook_in
		Out bindFacebook_out
	}
)

func NewBindFacebook() *bindFacebook {
	return &bindFacebook{}
}

func (this *bindFacebook) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_BindFacebook")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@UserID_1", database.AdParamInput, database.AdInteger, 4, this.In.UserID_1)
	statement.AddParamter("@UserID_2", database.AdParamInput, database.AdInteger, 4, this.In.UserID_2)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
	return
}

// 个性签名列表
type (
	userWordsList_in struct {
		UserID    int    // 用户ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	userWordsModel struct {
		ApplyID    int    // 申请ID
		UserID     int    // 用户ID
		NickName   string // 昵称
		UserWords  string // 个性签名
		TransMsg   string // 译文
		Status     int    // 状态 1=同意  2=拒绝
		OpUserID   int    // 操作员ID
		OpUserName string // 操作员名称
		DealTime   string // 处理时间
		ApplyTime  string // 申请时间
		OpUser     string // 操作员
	}

	userWordsList_out struct {
		RecordCount int // 记录数
		List        []userWordsModel
	}

	userWordsList struct {
		database.Trans_base
		In  userWordsList_in
		Out userWordsList_out
	}
)

func NewUserWordsList() *userWordsList {
	return &userWordsList{}
}

func (this *userWordsList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserWords_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]userWordsModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.ApplyID = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			if info := tagMgr.getInfo(out.UserID); info != nil {
				out.NickName = info.NickName
			}
			out.UserWords = (*ret[3].(*interface{})).(string)
			out.TransMsg = (*ret[4].(*interface{})).(string)
			out.Status = int((*ret[5].(*interface{})).(int64))
			out.OpUserID = int((*ret[6].(*interface{})).(int64))
			out.OpUserName = (*ret[7].(*interface{})).(string)
			out.DealTime = (*ret[8].(*interface{})).(string)
			out.ApplyTime = (*ret[9].(*interface{})).(string)
			if out.OpUserID > 0 {
				out.OpUser = fmt.Sprintf("%s（id：%d）", out.OpUserName, out.OpUserID)
			}
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return
}

// 个性签名审批
type (
	userWordsSet_in struct {
		OpUserID   int    // 操作员ID
		OpUserName string // 操作员名称
		ApplyID    int    // 申请ID
		Status     int    // 状态 1=同意  其他拒绝
	}

	userWordsSet_out struct {
		RetCode int // 操作结果
	}

	userWordsSet struct {
		database.Trans_base
		In  userWordsSet_in
		Out userWordsSet_out
	}
)

func NewUserWordsSet() *userWordsSet {
	return &userWordsSet{}
}

func (this *userWordsSet) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserWords_SetStatus")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@ApplyID", database.AdParamInput, database.AdInteger, 4, this.In.ApplyID)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, this.In.Status)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
	return
}

// 个性签名翻译
type (
	userWordsTranslate_in struct {
		ApplyID  int    // 申请ID
		TransMsg string // 译文
	}

	userWordsTranslate struct {
		database.Trans_base
		In userWordsTranslate_in
	}
)

func NewUserWordsTranslate() *userWordsTranslate {
	return &userWordsTranslate{}
}

func (this *userWordsTranslate) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserWords_Translate")
	statement.AddParamter("@ApplyID", database.AdParamInput, database.AdInteger, 4, this.In.ApplyID)
	statement.AddParamter("@TransMsg", database.AdParamInput, database.AdNVarChar, 256, this.In.TransMsg)
	sqlstring := statement.GenSql()
	CenterDB.ExecSql(sqlstring)
	return
}

type (
	bankInfo_in struct {
		UserID int
	}

	bankInfoItem struct {
		RealName string
		BankName string
		BankCode string
		BankCard string
		Mobile   string
		Crdate   string
	}

	bankInfo_out struct {
		RecordCount int
		List        []bankInfoItem
	}

	bankInfo struct {
		database.Trans_base
		In  bankInfo_in
		Out bankInfo_out
	}
)

func NewBankInfo() *bankInfo {
	return &bankInfo{}
}

func (this *bankInfo) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetBankInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	this.Out.List = make([]bankInfoItem, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.RealName = (*ret[0].(*interface{})).(string)
		out.BankName = (*ret[1].(*interface{})).(string)
		out.BankCode = (*ret[2].(*interface{})).(string)
		out.BankCard = (*ret[3].(*interface{})).(string)
		out.Mobile = (*ret[4].(*interface{})).(string)
		out.Crdate = (*ret[5].(*interface{})).(string)
	}
}

type (
	changeNickName_in struct {
		OpUserID   int    // 操作员ID
		OpUserName string // 操作员名称
		UserID     int    // 用户ID
		NickName   string // 昵称
		IpAddress  string // IP地址
	}

	changeNickName_out struct {
		RetCode int
	}

	changeNickName struct {
		database.Trans_base
		In  changeNickName_in
		Out changeNickName_out
	}
)

func NewChangeNickName() *changeNickName {
	return &changeNickName{}
}

func (this *changeNickName) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_ChangeNickName")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, this.In.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, this.In.OpUserName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, this.In.NickName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	sqlString := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	this.Out.RetCode = int((*retRows[0][0].(*interface{})).(int64))
}
