package main

import (
	"fmt"
	"time"

	"bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/adminserver/config"
	"bet24.com/servers/adminserver/crons"
	"bet24.com/servers/adminserver/dao"
	"bet24.com/servers/adminserver/router"
	"bet24.com/servers/adminserver/serverdata"
	"bet24.com/servers/adminserver/service"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/monitor"
	"bet24.com/utils"
)

func main() {
	//defer log.PanicHandler("webserver")

	//redis
	redis.InitPool(config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB)
	coreservice.SetServiceAddr(config.Server.ServiceAddr)

	//数据库
	dao.Run()
	serverdata.Run()
	go service.Run()
	dao.RunTagManager()

	// 子弹配置
	// bullet.Run()

	//监控
	monitor.Run(config.Server.MonitorPort, config.Server.LogPath)
	utils.SetConsoleTitle(fmt.Sprintf("adminserver port:%d monitor:%d", config.Server.TlsPort, config.Server.MonitorPort))
	//定时器
	go crons.Run()

	log.Release("Server started %v", time.Now())

	//gin 路由
	router.Run()
}
