package register

import (
	"fmt"
	"sort"
	"time"

	"bet24.com/log"
)

var mgr *regManager

type regManager struct {
	stat_list        map[string]*regStatInfo
	statHour_list    map[string]*regStatByHourInfo
	statSource_list  map[string][]*regStatSourceInfo
	newUserStat_list map[string][]*newUserStatInfo
}

func Run() {
	mgr = new(regManager)
	mgr.stat_list = make(map[string]*regStatInfo)
	mgr.statHour_list = make(map[string]*regStatByHourInfo)
	mgr.statSource_list = make(map[string][]*regStatSourceInfo)
	mgr.newUserStat_list = make(map[string][]*newUserStatInfo)
}

func (this *regManager) statList(partnerId int, beginTime, endTime string) (int, []*regStatInfo) {
	var (
		totalRegCount int
		list          []*regStatInfo
	)

	end, _ := time.Parse(dateFormat, endTime)

	for begin, _ := time.Parse(dateFormat, beginTime); !begin.After(end); begin = begin.AddDate(0, 0, 1) {
		dateStr := begin.Format(dateFormat)
		key := fmt.Sprintf("%s_%d", dateStr, partnerId)
		v, ok := this.stat_list[key]
		if ok {
			totalRegCount += v.RegUserCount
			list = append(list, v)
			continue
		}

		for _, v := range regStatList(partnerId, dateStr, dateStr) {

			log.Debug("regmgr.statList ==> %+v", v)

			totalRegCount += v.RegUserCount

			if begin.Format(dateFormat) == time.Now().Format(dateFormat) {
				list = append(list, v)
				continue
			}

			key = fmt.Sprintf("%s_%d", v.DateFlag, partnerId)
			this.stat_list[key] = v
			list = append(list, v)
		}
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].DateFlag > list[j].DateFlag
	})

	return totalRegCount, list
}

func (this *regManager) statListByHour(beginTime, endTime string) []*regStatByHourInfo {
	var list []*regStatByHourInfo

	end, _ := time.Parse(dateFormat, endTime)

	for begin, _ := time.Parse(dateFormat, beginTime); !begin.After(end); begin = begin.AddDate(0, 0, 1) {
		dateStr := begin.Format(dateFormat)
		v, ok := this.statHour_list[dateStr]
		if ok {
			list = append(list, v)
			continue
		}

		for _, v := range regStatListByHour(dateStr, dateStr) {

			log.Debug("regmgr.statListByHour ==> %+v", v)

			if begin.Format(dateFormat) == time.Now().Format(dateFormat) {
				list = append(list, v)
				continue
			}

			this.statHour_list[v.DateFlag] = v
			list = append(list, v)
		}
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].DateFlag < list[j].DateFlag
	})

	return list
}

func (this *regManager) sourceStatList(beginTime, endTime string) []*regStatSourceInfo {
	var list []*regStatSourceInfo

	end, _ := time.Parse(dateFormat, endTime)

	for begin, _ := time.Parse(dateFormat, beginTime); !begin.After(end); begin = begin.AddDate(0, 0, 1) {
		dateStr := begin.Format(dateFormat)
		v, ok := this.statSource_list[dateStr]
		if ok {
			list = append(list, v...)
			continue
		}

		for _, v := range regStatSourceList(dateStr, dateStr) {

			log.Debug("regmgr.sourceStatList ==> %+v", v)

			if begin.Format(dateFormat) == time.Now().Format(dateFormat) {
				list = append(list, v)
				continue
			}

			this.statSource_list[v.DateFlag] = append(this.statSource_list[v.DateFlag], v)
			list = append(list, v)
		}
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].DateFlag > list[j].DateFlag
	})

	return list
}

func (this *regManager) newUserStatList(req *newUserStatInf_req) *newUserStatInfo_resp {
	return getNewUserStat(req)
}
