package register

import (
	"net/http"
	"strings"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 注册统计
func StatList(c *gin.Context) {
	var req reg_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "register.StatList", err)
		return
	}

	recordCount, list := mgr.statList(req.PartnerID, req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: recordCount,
		List:        list,
	})
	return
}

// 注册统计(小时)
func StatListByHour(c *gin.Context) {
	var req reg_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "register.StatListByHour", err)
		return
	}

	list := mgr.statListByHour(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 注册来源统计
func SourceStatList(c *gin.Context) {
	var req reg_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "register.StatSourceList", err)
		return
	}

	list := mgr.sourceStatList(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 注册来源统计
func NewUserStatList(c *gin.Context) {
	var req newUserStatInf_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "register.NewUserStatList", err)
		return
	}

	req.Source = strings.ReplaceAll(req.Source, "0x5B", "[")
	req.Source = strings.ReplaceAll(req.Source, "0x5D", "]")
	req.Source = strings.ReplaceAll(req.Source, "0x7B", "{")
	req.Source = strings.ReplaceAll(req.Source, "0x7D", "}")
	req.Source = strings.ReplaceAll(req.Source, "0x25", "%")
	req.Source = strings.ReplaceAll(req.Source, "0x28", "(")
	req.Source = strings.ReplaceAll(req.Source, "0x29", ")")

	resp := mgr.newUserStatList(&req)
	c.JSON(http.StatusOK, resp)
	return
}
