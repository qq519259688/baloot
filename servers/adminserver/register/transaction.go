package register

import (
	"fmt"
	"runtime/debug"
	"strconv"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
)

// 注册统计
func regStatList(partnerId int, beginTime, endTime string) []*regStatInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover fail %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	var (
		totalRegCount int
		list          []*regStatInfo
	)

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_RegStat_GetSubsistList")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, partnerId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@TotalRegCount", database.AdParamOutput, database.AdInteger, 4, totalRegCount)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var out regStatInfo

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.RegUserCount = int((*ret[1].(*interface{})).(int64))

			payMoney := string((*ret[2].(*interface{})).([]byte))
			payMoneyStr, _ := strconv.ParseFloat(payMoney, 64)
			out.PayMoney = fmt.Sprintf("%.2f", payMoneyStr)

			out.PartnerName = (*ret[3].(*interface{})).(string)
			out.TotalUserCount = int((*ret[4].(*interface{})).(int64))

			list = append(list, &out)
		}
	}

	totalRegCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return list
}

// 注册统计(小时)
func regStatListByHour(beginTime, endTime string) []*regStatByHourInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*regStatByHourInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_RegStat_GetListByHour")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@UserType", database.AdParamInput, database.AdInteger, 4, 0)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out regStatByHourInfo

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.UserCount = int((*ret[1].(*interface{})).(int64))
		out.Hour_0 = int((*ret[2].(*interface{})).(int64))
		out.Hour_1 = int((*ret[3].(*interface{})).(int64))
		out.Hour_2 = int((*ret[4].(*interface{})).(int64))
		out.Hour_3 = int((*ret[5].(*interface{})).(int64))
		out.Hour_4 = int((*ret[6].(*interface{})).(int64))
		out.Hour_5 = int((*ret[7].(*interface{})).(int64))
		out.Hour_6 = int((*ret[8].(*interface{})).(int64))
		out.Hour_7 = int((*ret[9].(*interface{})).(int64))
		out.Hour_8 = int((*ret[10].(*interface{})).(int64))
		out.Hour_9 = int((*ret[11].(*interface{})).(int64))
		out.Hour_10 = int((*ret[12].(*interface{})).(int64))
		out.Hour_11 = int((*ret[13].(*interface{})).(int64))
		out.Hour_12 = int((*ret[14].(*interface{})).(int64))
		out.Hour_13 = int((*ret[15].(*interface{})).(int64))
		out.Hour_14 = int((*ret[16].(*interface{})).(int64))
		out.Hour_15 = int((*ret[17].(*interface{})).(int64))
		out.Hour_16 = int((*ret[18].(*interface{})).(int64))
		out.Hour_17 = int((*ret[19].(*interface{})).(int64))
		out.Hour_18 = int((*ret[20].(*interface{})).(int64))
		out.Hour_19 = int((*ret[21].(*interface{})).(int64))
		out.Hour_20 = int((*ret[22].(*interface{})).(int64))
		out.Hour_21 = int((*ret[23].(*interface{})).(int64))
		out.Hour_22 = int((*ret[24].(*interface{})).(int64))
		out.Hour_23 = int((*ret[25].(*interface{})).(int64))

		list = append(list, &out)
	}

	return list
}

// 注册来源统计
func regStatSourceList(beginTime, endTime string) []*regStatSourceInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*regStatSourceInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_RegStat_GetSourceList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out regStatSourceInfo

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.ServerName = (*ret[1].(*interface{})).(string)
		out.UserCount = int((*ret[2].(*interface{})).(int64))

		list = append(list, &out)
	}

	return list
}

func getNewUserStat(req *newUserStatInf_req) *newUserStatInfo_resp {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	resp := new(newUserStatInfo_resp)

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AllUser_GetNewUserStat")
	statement.AddParamter("@Source", database.AdParamInput, database.AdNVarChar, 128, req.Source)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, resp.RecordCount)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var out newUserStatInfo

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.Source = (*ret[1].(*interface{})).(string)
			out.RegCount = int((*ret[2].(*interface{})).(int64))
			out.LiveUsers_2 = int((*ret[3].(*interface{})).(int64))
			out.PayUsers_1 = int((*ret[4].(*interface{})).(int64))

			payMoney_1 := string((*ret[5].(*interface{})).([]byte))
			out.PayMoney_1, _ = strconv.ParseFloat(payMoney_1, 64)

			payMoney_2 := string((*ret[6].(*interface{})).([]byte))
			out.PayMoney_2, _ = strconv.ParseFloat(payMoney_2, 64)

			payMoney_3 := string((*ret[7].(*interface{})).([]byte))
			out.PayMoney_3, _ = strconv.ParseFloat(payMoney_3, 64)

			payMoney_7 := string((*ret[8].(*interface{})).([]byte))
			out.PayMoney_7, _ = strconv.ParseFloat(payMoney_7, 64)

			payMoney_14 := string((*ret[9].(*interface{})).([]byte))
			out.PayMoney_14, _ = strconv.ParseFloat(payMoney_14, 64)

			payMoney_30 := string((*ret[10].(*interface{})).([]byte))
			out.PayMoney_30, _ = strconv.ParseFloat(payMoney_30, 64)

			resp.List = append(resp.List, &out)
		}
	}

	resp.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return resp
}
