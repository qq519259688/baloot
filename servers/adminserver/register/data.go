package register

const dateFormat = "2006-01-02"

type reg_base struct {
	PartnerID int    // 渠道ID
	BeginTime string // 开始时间
	EndTime   string // 截止时间
}

type regStatInfo struct {
	DateFlag       string // 日期标志
	RegUserCount   int    // 注册用户数
	TotalUserCount int    // 总注册人数
	PayMoney       string // 充值
	PartnerName    string // 渠道名称
}

type regStatByHourInfo struct {
	DateFlag  string
	UserCount int
	Hour_0    int
	Hour_1    int
	Hour_2    int
	Hour_3    int
	Hour_4    int
	Hour_5    int
	Hour_6    int
	Hour_7    int
	Hour_8    int
	Hour_9    int
	Hour_10   int
	Hour_11   int
	Hour_12   int
	Hour_13   int
	Hour_14   int
	Hour_15   int
	Hour_16   int
	Hour_17   int
	Hour_18   int
	Hour_19   int
	Hour_20   int
	Hour_21   int
	Hour_22   int
	Hour_23   int
}

type regStatSourceInfo struct {
	DateFlag   string
	ServerName string
	UserCount  int
}

type (
	newUserStatInf_req struct {
		Source    string
		BeginTime string
		EndTime   string
		PageIndex int
		PageSize  int
	}

	newUserStatInfo_resp struct {
		RecordCount int
		List        []*newUserStatInfo
	}

	newUserStatInfo struct {
		DateFlag    string  // 日期标识
		Source      string  // UTM源
		RegCount    int     // 注册人数
		LiveUsers_2 int     // 次日留存
		PayUsers_1  int     // 第1天充值人数
		PayMoney_1  float64 // 第1天充值金额
		PayMoney_2  float64 // 第2天充值金额
		PayMoney_3  float64 // 第3天充值金额
		PayMoney_7  float64 // 第7天充值金额
		PayMoney_14 float64 // 第14天充值金额
		PayMoney_30 float64 // 第30天充值金额
	}
)
