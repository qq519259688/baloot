package service

import (
	"bet24.com/servers/adminserver/agent"
	"bet24.com/servers/adminserver/announce"
	"bet24.com/servers/adminserver/audioroom"
	"bet24.com/servers/adminserver/card"
	"bet24.com/servers/adminserver/coupon"
	"bet24.com/servers/adminserver/game"
	"bet24.com/servers/adminserver/guess"
	"bet24.com/servers/adminserver/label"
	"bet24.com/servers/adminserver/matchdata"
	"bet24.com/servers/adminserver/rank"
	"bet24.com/servers/adminserver/register"
	"bet24.com/servers/adminserver/report"
	"bet24.com/servers/adminserver/roi"
	"bet24.com/servers/adminserver/slot"
	"bet24.com/servers/adminserver/teacher"
	"bet24.com/servers/adminserver/video"
	"bet24.com/servers/adminserver/warning"
)

func Run() {
	// 视频相关
	go video.Run()

	// 注册相关
	go register.Run()

	// 游戏相关
	go game.Run()

	// 代理相关
	go agent.Run()

	// slot 监控
	go slot.Run()

	// roi 投入产出
	go roi.Run()

	// 红包相关
	go coupon.Run()

	// 师徒相关
	go teacher.Run()

	// 系统公告
	go announce.Run()

	// 充值卡
	go card.Run()

	// 预警
	go warning.Run()

	// 每日报表
	go report.Run()

	// 标签
	go label.Run()

	// 语聊房
	go audioroom.Run()

	// 赛事数据相关
	go matchdata.Run()

	// 排行榜
	go rank.Run()

	// 赛事竞猜
	go guess.Run()
}
