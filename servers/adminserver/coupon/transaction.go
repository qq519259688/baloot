package coupon

import (
	"bet24.com/servers/adminserver/ip"
	"fmt"
	"runtime/debug"
	"strconv"

	"bet24.com/servers/adminserver/dao"

	"bet24.com/database"

	"bet24.com/log"
)

// 红包券排行
type (
	userCouponRank_in struct {
		PageIndex int // 页索引
		PageSize  int // 页大小
	}

	userCouponRankModel struct {
		RowNumber   int    // 序号
		UserID      int    // 账号ID
		NickName    string // 昵称
		ItemCount   int    // 道具数
		PayMoney    string // 充值额度
		PartnerName string // 渠道名称
	}

	userCouponRank_out struct {
		RecordCount int
		List        []userCouponRankModel
	}

	userCouponRank struct {
		database.Trans_base
		In  userCouponRank_in
		Out userCouponRank_out
	}
)

func NewUserCouponRank() *userCouponRank {
	return &userCouponRank{}
}

func (this *userCouponRank) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_CouponTask_GetRank")
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		this.Out.List = make([]userCouponRankModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.RowNumber = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			out.ItemCount = int((*ret[3].(*interface{})).(int64))

			payMoney := string((*ret[4].(*interface{})).([]byte))
			fPayMoney, _ := strconv.ParseFloat(payMoney, 64)
			out.PayMoney = fmt.Sprintf("%.2f", fPayMoney)

			out.PartnerName = (*ret[5].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return
}

// 红包券日志
type (
	userCouponLog_in struct {
		UserID    int    // 用户ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	userCouponLogModel struct {
		LogID      int    // 日志ID
		UserID     int    // 用户ID
		NickName   string // 昵称
		ItemID     int    // 道具ID
		ItemName   string // 道具名称
		CurrCount  int    // 当前数量
		WantCount  int    // 修改数量
		StillCount int    // 剩余数量
		Remark     string // 备注
		IPAddress  string // IP地址
		Crdate     string // 时间
	}

	userCouponLog_out struct {
		RecordCount int
		List        []userCouponLogModel
	}

	userCouponLog struct {
		database.Trans_base
		In  userCouponLog_in
		Out userCouponLog_out
	}
)

func NewUserCouponLog() *userCouponLog {
	return &userCouponLog{}
}

func (this *userCouponLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserCouponTask_GetLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, this.In.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, this.In.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, this.Out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	if rowLen > 1 {
		this.Out.List = make([]userCouponLogModel, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			out := &this.Out.List[i]

			out.LogID = int((*ret[0].(*interface{})).(int64))
			out.UserID = int((*ret[1].(*interface{})).(int64))
			out.NickName = (*ret[2].(*interface{})).(string)
			out.ItemID = int((*ret[3].(*interface{})).(int64))
			out.CurrCount = int((*ret[4].(*interface{})).(int64))
			out.WantCount = int((*ret[5].(*interface{})).(int64))
			out.StillCount = int((*ret[6].(*interface{})).(int64))
			out.Remark = (*ret[7].(*interface{})).(string)
			out.IPAddress = (*ret[8].(*interface{})).(string)
			out.IPAddress = ip.GetCountryAndRegion(out.IPAddress, false)
			out.Crdate = (*ret[9].(*interface{})).(string)
			out.ItemName = (*ret[10].(*interface{})).(string)
		}
	}

	this.Out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
}

// 红包券统计(每天)
func statByDay(dateFlag string) []*statInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_CouponTask_GetStatByDay")
	statement.AddParamter("@DateFlag", database.AdParamInput, database.AdVarChar, 20, dateFlag)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}
	var list []*statInfo
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out statInfo

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.CouponCount = int((*ret[1].(*interface{})).(int64))
		out.UserCount = int((*ret[2].(*interface{})).(int64))
		list = append(list, &out)
	}

	return list
}

// 红包券统计(每小时)
func statByHour(dateFlag string) []*statInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_CouponTask_GetStatByHour")
	statement.AddParamter("@DateFlag", database.AdParamInput, database.AdVarChar, 20, dateFlag)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}
	var list []*statInfo
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out statInfo

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.Remark = (*ret[1].(*interface{})).(string)
		out.CouponCount = int((*ret[2].(*interface{})).(int64))
		out.UserCount = int((*ret[3].(*interface{})).(int64))
		list = append(list, &out)
	}

	return list
}
