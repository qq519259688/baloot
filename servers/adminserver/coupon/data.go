package coupon

const dateFormat = "2006-01-02"

type req_base struct {
	BeginTime string // 开始时间
	EndTime   string // 截止时间
}

// 统计信息
type statInfo struct {
	DateFlag    string // 日期标识
	Remark      string // 备注
	CouponCount int    // 红包券数
	UserCount   int    // 用户数
}
