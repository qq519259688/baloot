package coupon

import (
	"fmt"
	"sort"
	"time"

	"bet24.com/log"
)

var mgr *couponManager

type couponManager struct {
	day_list  map[string]*statInfo
	hour_list map[string][]*statInfo
}

func Run() {
	mgr = new(couponManager)
	mgr.day_list = make(map[string]*statInfo)
	mgr.hour_list = make(map[string][]*statInfo)
}

func (this *couponManager) dayStat(beginTime, endTime string) []*statInfo {
	var list []*statInfo
	end, _ := time.Parse(dateFormat, endTime)
	for begin, _ := time.Parse(dateFormat, beginTime); !begin.After(end); begin = begin.AddDate(0, 0, 1) {
		dateStr := begin.Format(dateFormat)
		key := fmt.Sprintf("%s", dateStr)
		v, ok := this.day_list[key]
		if ok {
			list = append(list, v)
			continue
		}

		for _, v := range statByDay(dateStr) {
			log.Debug("couponmgr.dayStat ==> %+v", v)
			if begin.Format(dateFormat) == time.Now().Format(dateFormat) {
				list = append(list, v)
				continue
			}

			key = fmt.Sprintf("%s", v.DateFlag)
			this.day_list[key] = v
			list = append(list, v)
		}
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].DateFlag > list[j].DateFlag
	})

	return list
}

func (this *couponManager) hourStat(beginTime, endTime string) []*statInfo {
	var list []*statInfo
	end, _ := time.Parse(dateFormat, endTime)

	for begin, _ := time.Parse(dateFormat, beginTime); !begin.After(end); begin = begin.AddDate(0, 0, 1) {
		dateStr := begin.Format(dateFormat)
		key := fmt.Sprintf("%s", dateStr)
		v, ok := this.hour_list[key]
		if ok {
			list = append(list, v...)
			continue
		}

		for _, v := range statByHour(dateStr) {
			log.Debug("couponmgr.hourStat ==> %+v", v)
			if begin.Format(dateFormat) == time.Now().Format(dateFormat) {
				list = append(list, v)
				continue
			}

			key = fmt.Sprintf("%s", v.DateFlag)
			this.hour_list[key] = append(this.hour_list[key], v)
			list = append(list, v)
		}
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].DateFlag > list[j].DateFlag
	})

	return list
}
