package coupon

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

func CouponRank(c *gin.Context) {
	obj := NewUserCouponRank()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "CouponRank", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
}

func CouponLog(c *gin.Context) {
	obj := NewUserCouponLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "CouponLog", err)
		return
	}

	obj.DoAction(nil)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 天统计
func DayStat(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "coupon.DayStat", err)
		return
	}

	list := mgr.dayStat(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 小时统计
func HourStat(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "coupon.HourStat", err)
		return
	}

	list := mgr.hourStat(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}
