package serverdata

import (
	"sync"
	"time"

	"bet24.com/servers/adminserver/dao"
)

type PartnerList struct {
	partners []dao.PartnerInfo
	lock     *sync.RWMutex
}

func NewPartnerList() *PartnerList {
	obj := new(PartnerList)
	obj.lock = &sync.RWMutex{}
	return obj
}

func (this *PartnerList) GetPartners() []dao.PartnerInfo {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.partners
}

func (this *PartnerList) run() {
	this.refreshData()
}

func (this *PartnerList) refreshData() {
	go this.doRefresh()
	time.AfterFunc(5*time.Minute, this.refreshData)
}

func (this *PartnerList) doRefresh() {
	obj := dao.NewPartnerList()
	obj.DoAction(nil)
	this.lock.Lock()
	this.partners = obj.Out.List
	this.lock.Unlock()
}

func (this *PartnerList) Refresh() {
	this.doRefresh()
}
