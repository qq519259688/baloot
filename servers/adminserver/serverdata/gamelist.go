package serverdata

import (
	"encoding/json"
	"sync"
	"time"

	"bet24.com/servers/adminserver/dao"

	"bet24.com/log"
)

func NewGameList() *GameList {
	obj := new(GameList)
	obj.lock = &sync.Mutex{}
	return obj
}

type GameList struct {
	games []dao.AllGameInfo
	lock  *sync.Mutex
}

func (g *GameList) GetGames() *[]dao.AllGameInfo {
	g.lock.Lock()
	defer g.lock.Unlock()
	return &g.games
}

func (g *GameList) GetGameIds() []int {
	g.lock.Lock()
	defer g.lock.Unlock()
	gameIDList := []int{}

	for i := 0; i < len(g.games); i++ {
		gameIDList = append(gameIDList, g.games[i].GameID)
	}

	return gameIDList
}

//GetGameInfo 获取游戏信息
func (g *GameList) GetGameInfo(GameID int) *dao.AllGameInfo {
	for i := 0; i < len(g.games); i++ {
		if g.games[i].GameID == GameID {
			return &g.games[i]
		}
	}

	return nil
}

type gameInfo struct {
	GameID      int
	ChineseName string
	EnglishName string
}

//获取简单游戏信息(GameID、ChineseName)
func (g *GameList) GetSimpleGames() []*gameInfo {
	var infoList []*gameInfo
	for _, val := range g.games {
		if val.Enabled != 1 && val.Enabled != 2 {
			continue
		}

		infoList = append(infoList, &gameInfo{
			GameID:      val.GameID,
			ChineseName: val.EnglishName,
			EnglishName: val.EnglishName,
		})
	}

	return infoList
}

func (g *GameList) GetJson() string {
	g.lock.Lock()
	defer g.lock.Unlock()
	if len(g.games) <= 0 {
		return ""
	}
	retData, err := json.Marshal(g.games)
	if err != nil {
		log.Release("GameList GetJson Marshal failed")
		return ""
	}
	return string(retData)
}

func (g *GameList) run() {
	g.refreshData()
}

func (g *GameList) refreshData() {
	go g.doRefresh()
	time.AfterFunc(5*time.Minute, g.refreshData)
}

func (g *GameList) doRefresh() {
	getGameList := dao.NewGetAllGames()
	getGameList.In.GameID = 0
	getGameList.DoAction(nil)
	if len(getGameList.Out) > 0 {
		g.lock.Lock()
		g.games = getGameList.Out
		g.lock.Unlock()
	}
}

//立即刷新
func (g *GameList) Refresh() {
	g.doRefresh()
}
