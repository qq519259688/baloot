package serverdata

var (
	Games    *GameList
	Partners *PartnerList
	Tip      *tip
)

func Run() {
	Games = NewGameList()
	Games.run()

	Partners = NewPartnerList()
	Partners.run()

	Tip = NewTip()
	Tip.run()
}
