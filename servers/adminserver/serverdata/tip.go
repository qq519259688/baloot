package serverdata

import (
	"sync"
	"time"

	"bet24.com/servers/adminserver/dao"
)

type tip struct {
	tips []dao.TipInfo
	lock *sync.RWMutex
}

func NewTip() *tip {
	obj := new(tip)
	obj.lock = &sync.RWMutex{}
	return obj
}

func (this *tip) GetList() []dao.TipInfo {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.tips
}

func (this *tip) run() {
	ticker := time.NewTicker(10 * time.Second)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				this.search()
			}
		}
	}(ticker)
}

func (this *tip) search() {
	obj := dao.NewTip()
	obj.DoAction(nil)
	this.lock.Lock()
	defer this.lock.Unlock()
	this.tips = obj.Out.List
}
