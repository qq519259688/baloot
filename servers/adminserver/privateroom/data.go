package privateroom

// 历史房间
type (
	roomHistory_req struct {
		UserID      int    // 用户ID
		OwnerUserID int    // 创建者
		GameID      int    // 游戏ID
		RoomNo      string // 房间号
		BeginTime   string // 开始时间
		EndTime     string // 截止时间
		PageIndex   int    // 页索引
		PageSize    int    // 页大小
	}

	roomHistory_resp struct {
		RecordCount int
		List        []*roomHistoryInfo
	}

	roomHistoryInfo struct {
		RoomID           int    // 房间ID
		RoomNo           string // 房间号
		GameID           int    // 游戏ID
		EnglishName      string // 游戏名称
		OwnerUserID      int    // 创建者ID
		OwnerNickName    string // 创建者昵称
		BeginTime        string // 开始时间
		EndTime          string // 结束时间
		TotalAwardAmount int    // 奖金
		TotalFee         int    // 总费用(报名)
		TotalTax         int    // 台费
		ExtInfo          string // 扩展信息
		PlaySeconds      int    // 游戏时长
		WaitSeconds      int    // 等待时长
		Crdate           string // 创建时间
	}
)

// 历史椅子
type (
	chairHistory_req struct {
		RoomID int // 房间ID
	}

	chairHistory_resp struct {
		RecordCount int // 记录数
		List        []*chairHistoryInfo
	}

	chairHistoryInfo struct {
		ChairNo     int    // 椅子号
		UserID      int    // 用户ID
		NickName    string // 昵称
		Fee         int    // 费用(报名)
		Tax         int    // 台费
		AwardAmount int    // 奖金
		Score       int    // 积分
	}
)
