package privateroom

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 历史房间
func RoomHistory(c *gin.Context) {
	var req roomHistory_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "privateRoom.RoomHistory", err)
		return
	}

	resp := mgr.roomHistory(&req)
	c.JSON(http.StatusOK, resp)
	return
}

// 历史椅子
func ChairHistory(c *gin.Context) {
	var req chairHistory_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "privateRoom.ChairHistory", err)
		return
	}

	resp := mgr.chairHistory(&req)
	c.JSON(http.StatusOK, resp)
}
