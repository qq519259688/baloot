package privateroom

import (
	"runtime/debug"

	"bet24.com/servers/adminserver/dao"

	"bet24.com/database"

	"bet24.com/log"
)

// 历史房间
func roomHistory(req *roomHistory_req) roomHistory_resp {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var resp roomHistory_resp

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_PrivateRoom_GetHistoryList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, req.UserID)
	statement.AddParamter("@OwnerUserID", database.AdParamInput, database.AdInteger, 4, req.OwnerUserID)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, req.GameID)
	statement.AddParamter("@RoomNo", database.AdParamInput, database.AdVarChar, 32, req.RoomNo)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, resp.RecordCount)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return resp
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info roomHistoryInfo
			info.RoomID = int((*ret[0].(*interface{})).(int64))
			info.RoomNo = (*ret[1].(*interface{})).(string)
			info.GameID = int((*ret[2].(*interface{})).(int64))
			info.EnglishName = (*ret[3].(*interface{})).(string)
			info.OwnerUserID = int((*ret[4].(*interface{})).(int64))
			info.OwnerNickName = (*ret[5].(*interface{})).(string)
			info.BeginTime = (*ret[6].(*interface{})).(string)
			info.EndTime = (*ret[7].(*interface{})).(string)
			info.TotalAwardAmount = int((*ret[8].(*interface{})).(int64))
			info.TotalFee = int((*ret[9].(*interface{})).(int64))
			info.TotalTax = int((*ret[10].(*interface{})).(int64))
			info.ExtInfo = (*ret[11].(*interface{})).(string)
			info.Crdate = (*ret[12].(*interface{})).(string)
			info.PlaySeconds = int((*ret[13].(*interface{})).(int64))
			info.WaitSeconds = int((*ret[14].(*interface{})).(int64))
			resp.List = append(resp.List, &info)
		}
	}

	resp.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return resp
}

// 历史椅子
func chairHistory(req *chairHistory_req) chairHistory_resp {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var resp chairHistory_resp
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_PrivateRoom_GetChairHistory")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, req.RoomID)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return resp
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info chairHistoryInfo
		info.ChairNo = int((*ret[0].(*interface{})).(int64))
		info.UserID = int((*ret[1].(*interface{})).(int64))
		info.NickName = (*ret[2].(*interface{})).(string)
		info.Fee = int((*ret[3].(*interface{})).(int64))
		info.Tax = int((*ret[4].(*interface{})).(int64))
		info.AwardAmount = int((*ret[5].(*interface{})).(int64))
		info.Score = int((*ret[6].(*interface{})).(int64))
		resp.List = append(resp.List, &info)
	}
	return resp
}
