package privateroom

var mgr *roomManager

type roomManager struct {
}

func Run() {
	mgr = new(roomManager)
}

func (this *roomManager) roomHistory(req *roomHistory_req) roomHistory_resp {
	return roomHistory(req)
}

func (this *roomManager) chairHistory(req *chairHistory_req) chairHistory_resp {
	return chairHistory(req)
}
