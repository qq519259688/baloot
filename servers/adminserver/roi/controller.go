package roi

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 录入
func Insert(c *gin.Context) {
	var req *roiInfo
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "roi.Insert", err)
		return
	}

	mgr.insert(req)
	c.JSON(http.StatusOK, nil)
	return
}

// 删除
func Del(c *gin.Context) {
	var req *roiInfo
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "roi.Del", err)
		return
	}

	retCode := mgr.del(req.Rid)
	c.JSON(http.StatusOK, retCode)
	return
}

// 列表
func GetList(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "roi.GetList", err)
		return
	}

	list := mgr.getList(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 列表
func GetListByMonth(c *gin.Context) {
	list := mgr.getListByMonth()
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}
