package roi

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
)

// 投入产出录入
func insert(info *roiInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ROI_Insert")
	statement.AddParamter("@DateFlag", database.AdParamInput, database.AdVarChar, 20, info.DateFlag)
	statement.AddParamter("@ItemType", database.AdParamInput, database.AdInteger, 4, info.ItemType)
	statement.AddParamter("@ItemName", database.AdParamInput, database.AdNVarChar, 32, info.ItemName)
	statement.AddParamter("@ItemValue", database.AdParamInput, database.AdFloat, 20, info.ItemValue)
	sqlstring := statement.GenSql()
	dao.CenterDB.ExecSql(sqlstring)
}

// 投入产出删除
func del(id int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ROI_Del")
	statement.AddParamter("@Rid", database.AdParamInput, database.AdInteger, 4, id)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}
	return int((*retRows[0][0].(*interface{})).(int64))
}

// 投入产出列表
func getList(dateFlag string) []*roiInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ROI_GetList")
	statement.AddParamter("@DateFlag", database.AdParamInput, database.AdVarChar, 20, dateFlag)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}
	var list []*roiInfo
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out roiInfo

		out.Rid = int((*ret[0].(*interface{})).(int64))
		out.DateFlag = (*ret[1].(*interface{})).(string)
		out.ItemType = int((*ret[2].(*interface{})).(int64))
		out.ItemName = (*ret[3].(*interface{})).(string)
		out.ItemValue = string((*ret[4].(*interface{})).([]byte))
		// out.ItemValue, _ = strconv.ParseFloat(valueStr, 64)
		list = append(list, &out)
	}
	return list
}

// 投入产出列表
func getListByMonth() []*roiInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ROI_GetListByMonth")
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}
	var list []*roiInfo
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out roiInfo

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.ItemType = int((*ret[1].(*interface{})).(int64))
		out.ItemName = (*ret[2].(*interface{})).(string)
		out.ItemValue = string((*ret[3].(*interface{})).([]byte))
		list = append(list, &out)
	}
	return list
}
