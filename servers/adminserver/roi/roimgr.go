package roi

import (
	"fmt"
	"sort"
	"time"

	"bet24.com/log"
)

var mgr *roiManager

type roiManager struct {
	list map[string][]*roiInfo
}

func Run() {
	mgr = new(roiManager)
	mgr.list = make(map[string][]*roiInfo)
}

// 录入数据
func (this *roiManager) insert(info *roiInfo) {
	defer this.clear()
	insert(info)
}

// 删除数据
func (this *roiManager) del(id int) int {
	defer this.clear()
	return del(id)
}

// 获取列表
func (this *roiManager) getList(beginTime, endTime string) []*roiInfo {
	var list []*roiInfo

	end, _ := time.Parse(dateFormat, endTime)

	for begin, _ := time.Parse(dateFormat, beginTime); !begin.After(end); begin = begin.AddDate(0, 0, 1) {
		dateStr := begin.Format(dateFormat)
		key := fmt.Sprintf("%s", dateStr)
		v, ok := this.list[key]
		if ok {
			list = append(list, v...)
			continue
		}

		for _, v := range getList(dateStr) {
			log.Debug("roimgr.getList ==> %+v", v)
			if time.Now().Sub(begin).Hours() < 72 {
				list = append(list, v)
				continue
			}

			key = fmt.Sprintf("%s", v.DateFlag)
			this.list[key] = append(this.list[key], v)
			list = append(list, v)
		}
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].DateFlag > list[j].DateFlag
	})

	return list
}

// 获取列表
func (this *roiManager) getListByMonth() []*roiInfo {
	return getListByMonth()
}

// 清空缓存
func (this *roiManager) clear() {
	this.list = make(map[string][]*roiInfo)
	return
}
