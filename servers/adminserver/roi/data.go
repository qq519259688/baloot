package roi

const dateFormat = "2006-01-02"

type req_base struct {
	BeginTime string
	EndTime   string
	PartnerID int
}

type roiInfo struct {
	Rid       int    // 标识
	DateFlag  string // 日期
	ItemType  int    // 项类型(1=计数  2=计价)
	ItemName  string // 项名称
	ItemValue string // 项值
}
