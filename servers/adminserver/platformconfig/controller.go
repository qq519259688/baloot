package platformconfig

import (
	"bet24.com/servers/adminserver/dao"
	"net/http"
	"sort"
	"strings"

	"bet24.com/log"
	coreClient "bet24.com/servers/coreservice/client"
	"github.com/gin-gonic/gin"
)

type (
	configInfo struct {
		CfgKey string
	}
	configList struct {
		RecordCount int
		List        []configInfo
	}
)

// 获取配置列表
func GetConfigList(c *gin.Context) {
	var resp configList
	list := coreClient.GetPlatformConfigList()
	for _, v := range list {
		resp.List = append(resp.List, configInfo{CfgKey: v})
	}
	sort.SliceStable(resp.List, func(i, j int) bool {
		return resp.List[i].CfgKey <= resp.List[j].CfgKey
	})
	c.JSON(http.StatusOK, resp)
	return
}

// 获取配置信息
func GetConfig(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "platformConfig.GetConfig", err)
		return
	}

	ret := coreClient.GetPlatformConfig(req.CfgKey)
	c.String(http.StatusOK, ret)
	return
}

// 设置配置
func SetConfig(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "platformConfig.SetConfig", err)
		return
	}

	req.CfgValue = strings.ReplaceAll(req.CfgValue, "[7B]", "{")
	req.CfgValue = strings.ReplaceAll(req.CfgValue, "[7D]", "}")
	req.CfgValue = strings.ReplaceAll(req.CfgValue, "[2A]", "*")
	req.CfgValue = strings.ReplaceAll(req.CfgValue, "[25]", "%")
	req.CfgValue = strings.ReplaceAll(req.CfgValue, "[43]", "+")
	coreClient.SetPlatformConfig(req.CfgKey, req.CfgValue)
	addOperationLog(req)
	log.Release("platformConfig.SetConfig %+v", req)
	c.JSON(http.StatusOK, "")
	return
}

// 添加操作日志
func addOperationLog(req req_base) {
	if req.Eq == 0 {
		return
	}
	user := dao.NewGetInfo()
	user.In.AdminUserID = req.OpUserID
	user.DoAction(nil)

	objLog := dao.NewPlatformConfigLog()
	objLog.In.OpUserID = req.OpUserID
	objLog.In.OpUserName = req.OpUserName
	objLog.In.Memo = user.Out.Memo
	objLog.In.IPAddress = req.IpAddress
	objLog.In.KeyName = req.CfgKey
	objLog.In.Eq = req.Eq
	objLog.DoAction(nil)
}

// 获取操作记录
func GetConfigLog(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "platformConfig.GetConfigLog", err)
		return
	}
	objLog := dao.NewPlatformConfigGetList()
	objLog.In.OpUserID = req.OpUserID
	objLog.In.KeyName = req.CfgKey
	objLog.DoAction(nil)
	c.JSON(http.StatusOK, objLog.Out)
	return
}
