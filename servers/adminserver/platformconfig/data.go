package platformconfig

type req_base struct {
	CfgKey     string
	CfgValue   string
	Eq         int
	OpUserID   int
	OpUserName string
	IpAddress  string
}
