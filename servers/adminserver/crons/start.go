package crons

import (
	"fmt"
	"time"

	"bet24.com/profile"
	"github.com/robfig/cron"
)

var (
	cpuProfile interface {
		Stop()
	}
	memProfile interface {
		Stop()
	}
)

func Run() {
	c := cron.New()

	//秒(Seconds)  0-59
	//分(Minutes)  0-59
	//小时(Hours)  0-23
	//一个月中的某一天(Day of month)  1-31
	//月(Month)  1-12 or JAN-DEC
	//星期几(Day of week)  0-6 or SUN-SAT
	c.AddFunc("0 0/30 * * * ? ", profSample)

	//c.AddFunc("0/5 * * * * ?",public_transaction.CheckLonghuTax)

	c.Start()
}

/**
生成性能分析图，没30分钟执行一次
*/
func profSample() {
	if cpuProfile != nil {
		cpuProfile.Stop()
	}
	if memProfile != nil {
		memProfile.Stop()
	}

	//路径可以改为从配置文件获取，   ./2019-05-18/17.30/
	path := "./dump/"
	now := time.Now()
	date := now.Format("2006-01-02")
	path = fmt.Sprintf("%s/%d.%d", path+date, now.Hour(), now.Minute())

	cpuProfile = profile.Start(profile.CPUProfile, profile.ProfilePath(path), profile.NamePrefix("webcenter_"))
	memProfile = profile.Start(profile.MemProfile, profile.ProfilePath(path), profile.NamePrefix("webcenter_"))
}
