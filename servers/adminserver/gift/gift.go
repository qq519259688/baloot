package gift

import (
	giftservice "bet24.com/servers/micros/giftservice/proto"
	"time"
)

var mgr *giftManager

type giftManager struct {
	gift_list []giftservice.Gift
}

func getGiftManager() *giftManager {
	if mgr == nil {
		mgr = new(giftManager)
		mgr.refreshData()
	}
	return mgr
}

func (this *giftManager) refreshData() {
	this.load()
	time.AfterFunc(1*time.Minute, this.refreshData)
}

func (this *giftManager) load() {
	list := giftservice.GetGiftList(0)
	if len(list) == 0 {
		return
	}

	this.gift_list = list
}

func (this *giftManager) getGiftName(giftId int) string {
	for _, v := range this.gift_list {
		if v.GiftId == giftId {
			return v.Desc
		}
	}
	return ""
}
