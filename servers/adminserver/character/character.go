package character

import "strings"

// 获取特殊字符
func GetSpecialCharacter(character string) string {
	character = strings.ReplaceAll(character, "0x22", "\"")
	character = strings.ReplaceAll(character, "0x5B", "[")
	character = strings.ReplaceAll(character, "0x5D", "]")
	character = strings.ReplaceAll(character, "0x7B", "{")
	character = strings.ReplaceAll(character, "0x7D", "}")
	character = strings.ReplaceAll(character, "0x23", "#")
	character = strings.ReplaceAll(character, "0x24", "$")
	character = strings.ReplaceAll(character, "0x25", "%")
	character = strings.ReplaceAll(character, "0x26", "&")
	character = strings.ReplaceAll(character, "0x28", "(")
	character = strings.ReplaceAll(character, "0x29", ")")
	character = strings.ReplaceAll(character, "0x2A", "*")
	return character
}
