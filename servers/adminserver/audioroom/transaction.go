package audioroom

import (
	"bet24.com/database"
	"bet24.com/log"
	audioroomPb "bet24.com/servers/micros/audioroom/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"encoding/json"
	"runtime/debug"
)

// 获取所有房间
func getRoomList(roomName, beginTime, endTime, sortName, sortType string, roomId, pageIndex, pageSize int) (int, []audioroomPb.AdminRoomInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var recordCount int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AudioRoom_GetAllRoom")
	statement.AddParamter("@RoomId", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdNVarChar, 32, roomName)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@SortName", database.AdParamInput, database.AdVarChar, 16, sortName)
	statement.AddParamter("@SortType", database.AdParamInput, database.AdChar, 4, sortType)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	var records []audioroomPb.AdminRoomInfo
	if rowLen <= 0 {
		return 0, records
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var data audioroomPb.AdminRoomInfo
			data.RoomId = int(ret[0].(int64))
			data.RoomName = ret[1].(string)
			data.UserId = int(ret[2].(int64))
			data.NickName = ret[3].(string)
			data.Level = int(ret[4].(int64))
			data.Exps = int(ret[5].(int64))
			data.MemberCount = int(ret[6].(int64))
			data.CollectDiamond = int(ret[7].(int64))
			data.DiamondAmount = int(ret[8].(int64))
			data.CollectGold = int(ret[9].(int64))
			data.GoldAmount = int(ret[10].(int64))
			data.Crdate = ret[11].(string)
			records = append(records, data)
		}
	}

	if records == nil {
		records = make([]audioroomPb.AdminRoomInfo, 0)
	}
	recordCount = int(retRows[rowLen-1][0].(int64))
	return recordCount, records
}

// 获取语聊房详细信息
func getRoomDetail(roomId int) audioroomPb.AdminRoomDetail {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AudioRoom_GetExtraInfo")
	statement.AddParamter("@RoomId", database.AdParamInput, database.AdInteger, 4, roomId)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	jsonData := dbengine.Execute(sqlString)
	var ret []audioroomPb.AdminRoomDetail
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("transaction.getRoomDetail json unmarshal err %v", err)
	}
	if len(ret) <= 0 {
		return audioroomPb.AdminRoomDetail{}
	}
	return ret[0]
}

// 获取语聊房成员
func getRoomMember(beginTime, endTime string, roomId, pageIndex, pageSize int) (int, []audioroomPb.AdminRoomMember) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var recordCount int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AudioRoom_GetRoomMember")
	statement.AddParamter("@RoomId", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	var records []audioroomPb.AdminRoomMember
	if rowLen <= 0 {
		return 0, records
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var data audioroomPb.AdminRoomMember
			data.RoomID = int(ret[0].(int64))
			data.UserID = int(ret[1].(int64))
			data.NickName = ret[2].(string)
			data.RoleID = int(ret[3].(int64))
			data.Exps = int(ret[4].(int64))
			data.Level = int(ret[5].(int64))
			data.Crdate = ret[6].(string)
			records = append(records, data)
		}
	}

	if records == nil {
		records = make([]audioroomPb.AdminRoomMember, 0)
	}
	recordCount = int(retRows[rowLen-1][0].(int64))
	return recordCount, records
}

// 获取语聊房详细信息
func getBlackList(roomId, blackType, pageIndex, pageSize int, beginTime, endTime string) (int, []audioroomPb.AdminRoomBlackList) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var recordCount int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AudioRoom_BlackIdentity")
	statement.AddParamter("@RoomId", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@BlackType", database.AdParamInput, database.AdInteger, 4, blackType)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	var records []audioroomPb.AdminRoomBlackList
	if rowLen <= 0 {
		return 0, records
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var data audioroomPb.AdminRoomBlackList
			data.UserID = int(ret[0].(int64))
			data.NickName = ret[1].(string)
			data.RoleID = int(ret[2].(int64))
			data.Level = int(ret[3].(int64))
			data.ExpireTime = int(ret[4].(int64))
			data.Crdate = ret[5].(string)
			records = append(records, data)
		}
	}

	if records == nil {
		records = make([]audioroomPb.AdminRoomBlackList, 0)
	}
	recordCount = int(retRows[rowLen-1][0].(int64))
	return recordCount, records
}

// 获取语聊房详细信息
func getRoomLogRecord(roomId, pageIndex, pageSize int, beginTime, endTime string) (int, []audioroomPb.AdminRoomLogRecord) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var recordCount int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AudioRoom_GetRoomLogRecord")
	statement.AddParamter("@RoomId", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	var records []audioroomPb.AdminRoomLogRecord
	if rowLen <= 0 {
		return 0, records
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var data audioroomPb.AdminRoomLogRecord
			data.RoomID = int(ret[0].(int64))
			data.UserID = int(ret[1].(int64))
			data.ToUserID = int(ret[2].(int64))
			data.OperateType = int(ret[3].(int64))
			data.Crdate = ret[4].(string)
			records = append(records, data)
		}
	}

	if records == nil {
		records = make([]audioroomPb.AdminRoomLogRecord, 0)
	}
	recordCount = int(retRows[rowLen-1][0].(int64))
	return recordCount, records
}

// 获取房间任务列表
func getRoomTask(roomId, pageIndex, pageSize int, beginTime, endTime string) (int, []audioroomPb.AdminRoomTask) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var recordCount int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AudioRoom_GetRoomTaskList")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	var records []audioroomPb.AdminRoomTask
	if rowLen <= 0 {
		return 0, records
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var data audioroomPb.AdminRoomTask
			data.RoomID = int(ret[0].(int64))
			data.TaskID = int(ret[1].(int64))
			data.Schedule = int(ret[2].(int64))
			data.CurrNum = int(ret[3].(int64))
			data.UpdateTime = ret[4].(string)
			records = append(records, data)
		}
	}

	if records == nil {
		records = make([]audioroomPb.AdminRoomTask, 0)
	}
	recordCount = int(retRows[rowLen-1][0].(int64))
	return recordCount, records
}

// 获取用户的房间任务
func getUserRoomTask(roomId, userId, pageIndex, pageSize int) (int, []audioroomPb.AdminUserRoomTask) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var recordCount int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AudioRoom_GetUserRoomTaskList")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	var records []audioroomPb.AdminUserRoomTask
	if rowLen <= 0 {
		return 0, records
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var data audioroomPb.AdminUserRoomTask
			data.UserID = int(ret[0].(int64))
			data.RoomID = int(ret[1].(int64))
			data.TaskID = int(ret[2].(int64))
			data.Schedule = int(ret[3].(int64))
			data.CurrNum = int(ret[4].(int64))
			data.Status = int(ret[5].(int64))
			data.UpdateTime = ret[6].(string)
			records = append(records, data)
		}
	}

	if records == nil {
		records = make([]audioroomPb.AdminUserRoomTask, 0)
	}
	recordCount = int(retRows[rowLen-1][0].(int64))
	return recordCount, records
}

// 获取房间上下麦日志
func getRoomMicLog(roomId, userId, pageIndex, pageSize int, roomName, nickName, beginTime, endTime string) (int, []audioroomPb.AdminRoomMicLog) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var recordCount int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AudioRoom_GetRoomMicLog")
	statement.AddParamter("@RoomId", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdNVarChar, 32, roomName)
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, nickName)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	var records []audioroomPb.AdminRoomMicLog
	if rowLen <= 0 {
		return 0, records
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var data audioroomPb.AdminRoomMicLog
			data.RoomID = int(ret[0].(int64))
			data.RoomName = ret[1].(string)
			data.UserID = int(ret[2].(int64))
			data.NickName = ret[3].(string)
			data.RoleID = int(ret[4].(int64))
			data.MicMode = int(ret[5].(int64))
			data.OnMicTime = ret[6].(string)
			data.OffMicTime = ret[7].(string)
			data.Seconds = int(ret[8].(int64))
			data.OpUserID = int(ret[9].(int64))
			data.OpUserRoleID = int(ret[10].(int64))
			records = append(records, data)
		}
	}

	if records == nil {
		records = make([]audioroomPb.AdminRoomMicLog, 0)
	}
	recordCount = int(retRows[rowLen-1][0].(int64))
	return recordCount, records
}
