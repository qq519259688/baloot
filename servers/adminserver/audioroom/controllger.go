package audioroom

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 获取语聊房列表
func GetRoomList(c *gin.Context) {
	obj := NewGetList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getRoomList", err)
		return
	}
	count, list := mgr.getRoomList(obj.In.SearchKey, obj.In.BeginTime, obj.In.EndTime,
		obj.In.SortName, obj.In.SortType, obj.In.PageIndex, obj.In.PageSize, obj.In.IsRealTime)
	obj.Out.Data.RecordCount = count
	obj.Out.Data.List = list
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 获取语聊房详细信息
func GetRoomDetail(c *gin.Context) {
	obj := NewRoomDetail()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getRoomDetail", err)
		return
	}
	obj.Out.Data.List = mgr.getRoomDetail(obj.In.RoomId, obj.In.IsRealTime)
	c.JSON(http.StatusOK, obj.Out.Data.List)
	return
}

// 获取语聊房成员
func GetRoomMember(c *gin.Context) {
	obj := NewRoomMember()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GetRoomMember", err)
		return
	}
	count, list := mgr.getRoomMember(obj.In.BeginTime, obj.In.EndTime, obj.In.RoomId,
		obj.In.PageIndex, obj.In.PageSize, obj.In.IsRealTime)
	obj.Out.Data.RecordCount = count
	obj.Out.Data.List = list
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 获取语聊房房间的在线用户
func GetRoomOnlineUsers(c *gin.Context) {
	obj := NewRoomMember()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "GetRoomOnlineUsers", err)
		return
	}
	count, list := mgr.getRoomOnlineUsers(obj.In.RoomId, obj.In.IsRealTime, obj.In.PageIndex, obj.In.PageSize)
	obj.Out.Data.RecordCount = count
	obj.Out.Data.List = list
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 获取语聊房麦位
func GetRoomMic(c *gin.Context) {
	obj := NewRoomDetail()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getRoomMic", err)
		return
	}
	count, list := mgr.getRoomMic(obj.In.RoomId, obj.In.IsRealTime)
	obj.Out.Data.RecordCount = count
	obj.Out.Data.List = list
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 获取黑名单列表
func GetBlackList(c *gin.Context) {
	obj := NewBlackList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getBlackList", err)
		return
	}
	count, list := mgr.getBlackList(obj.In.RoomId, obj.In.BlackType, obj.In.PageIndex,
		obj.In.PageSize, obj.In.IsRealTime, obj.In.BeginTime, obj.In.EndTime)
	obj.Out.Data.RecordCount = count
	obj.Out.Data.List = list
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 获取房内操作日志
func GetRoomLogRecord(c *gin.Context) {
	obj := NewRoomLogRecord()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getRoomLogRecord", err)
		return
	}
	count, list := mgr.getRoomLogRecord(obj.In.RoomId, obj.In.PageIndex, obj.In.PageSize, obj.In.BeginTime, obj.In.EndTime)
	obj.Out.Data.RecordCount = count
	obj.Out.Data.List = list
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 获取房间任务列表
func GetRoomTask(c *gin.Context) {
	obj := NewRoomTask()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getRoomTask", err)
		return
	}
	count, list := mgr.getRoomTask(obj.In.IsRealTime, obj.In.RoomId, obj.In.PageIndex, obj.In.PageSize, obj.In.BeginTime, obj.In.EndTime)
	obj.Out.Data.RecordCount = count
	obj.Out.Data.List = list
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 获取用户的房间任务
func GetUserRoomTask(c *gin.Context) {
	obj := NewUserRoomTask()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getUserRoomTask", err)
		return
	}
	count, list := mgr.getUserRoomTask(obj.In.RoomId, obj.In.UserId, obj.In.PageIndex, obj.In.PageSize)
	obj.Out.Data.RecordCount = count
	obj.Out.Data.List = list
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}

// 获取房间上下麦日志
func GetRoomMicLog(c *gin.Context) {
	obj := NewRoomMicLog()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getRoomMicLog", err)
		return
	}
	count, list := mgr.getRoomMicLog(obj.In.PageIndex, obj.In.PageSize, obj.In.SearchRoom,
		obj.In.SearchUser, obj.In.BeginTime, obj.In.EndTime)
	obj.Out.Data.RecordCount = count
	obj.Out.Data.List = list
	c.JSON(http.StatusOK, obj.Out.Data)
	return
}
