package audioroom

import (
	"bet24.com/database"
	audioroomPb "bet24.com/servers/micros/audioroom/proto"
)

// 是否实时（1：是，0：否）
func isRealTime(realTime int) bool {
	return realTime == 1
}

// 获取房间列表
type (
	getList_in struct {
		SearchKey  string // 搜索的内容
		BeginTime  string // 开始时间
		EndTime    string // 结束时间
		IsRealTime int    // 是否实时数据（1：是，0：否）
		PageIndex  int    // 第几页
		PageSize   int    // 请求的个数
		SortName   string // 排序名称
		SortType   string // 排序类型
	}

	getList_out struct {
		Data audioroomPb.AdminResponse
	}

	getList struct {
		database.Trans_base
		In  getList_in
		Out getList_out
	}
)

func NewGetList() *getList {
	return &getList{}
}

// 语聊房详细信息
type (
	roomDetail_in struct {
		RoomId     int // 房间id
		IsRealTime int // 是否实时数据（1：是，0：否）
	}

	roomDetail_out struct {
		Data audioroomPb.AdminResponse
	}

	roomDetail struct {
		database.Trans_base
		In  roomDetail_in
		Out roomDetail_out
	}
)

func NewRoomDetail() *roomDetail {
	return &roomDetail{}
}

// 语聊房内的成员信息
type (
	roomMember_in struct {
		RoomId     int    // 房间id
		BeginTime  string // 开始时间
		EndTime    string // 结束时间
		PageIndex  int    // 第几页
		PageSize   int    // 请求的个数
		IsRealTime int    // 是否实时数据（1：是，0：否）
	}

	roomMember_out struct {
		Data audioroomPb.AdminResponse
	}

	roomMember struct {
		database.Trans_base
		In  roomMember_in
		Out roomMember_out
	}
)

func NewRoomMember() *roomMember {
	return &roomMember{}
}

// 获取黑名单列表
type (
	blackList_in struct {
		RoomId     int    // 房间id
		BlackType  int    // 黑名单的类型（1：房间，2：麦位）
		BeginTime  string // 开始时间
		EndTime    string // 结束时间
		PageIndex  int    // 第几页
		PageSize   int    // 请求的个数
		IsRealTime int    // 是否实时数据（1：是，0：否）
	}

	blackList_out struct {
		Data audioroomPb.AdminResponse
	}

	blackList struct {
		database.Trans_base
		In  blackList_in
		Out blackList_out
	}
)

func NewBlackList() *blackList {
	return &blackList{}
}

// 获取房内操作日志
type (
	roomLogRecord_in struct {
		RoomId    int    // 房间id
		BeginTime string // 开始时间
		EndTime   string // 结束时间
		PageIndex int    // 第几页
		PageSize  int    // 请求的个数
	}

	roomLogRecord_out struct {
		Data audioroomPb.AdminResponse
	}

	roomLogRecord struct {
		database.Trans_base
		In  roomLogRecord_in
		Out roomLogRecord_out
	}
)

func NewRoomLogRecord() *roomLogRecord {
	return &roomLogRecord{}
}

// 获取房间任务列表
type (
	roomTask_in struct {
		IsRealTime int    // 是否实时数据（1：是，0：否）
		RoomId     int    // 房间id
		PageIndex  int    // 第几页
		PageSize   int    // 请求的个数
		BeginTime  string // 开始时间
		EndTime    string // 结束时间
	}

	roomTask_out struct {
		Data audioroomPb.AdminResponse
	}

	roomTask struct {
		database.Trans_base
		In  roomTask_in
		Out roomTask_out
	}
)

func NewRoomTask() *roomTask {
	return &roomTask{}
}

// 获取用户的房间任务
type (
	userRoomTask_in struct {
		RoomId    int // 房间id
		UserId    int // 用户id
		PageIndex int // 第几页
		PageSize  int // 请求的个数
	}

	userRoomTask_out struct {
		Data audioroomPb.AdminResponse
	}

	userRoomTask struct {
		database.Trans_base
		In  userRoomTask_in
		Out userRoomTask_out
	}
)

func NewUserRoomTask() *userRoomTask {
	return &userRoomTask{}
}

// 获取房间上下麦日志
type (
	roomMicLog_in struct {
		SearchRoom string // 搜索房间
		SearchUser string // 搜索用户
		BeginTime  string // 开始时间
		EndTime    string // 结束时间
		PageIndex  int    // 第几页
		PageSize   int    // 请求的个数
	}

	roomMicLog_out struct {
		Data audioroomPb.AdminResponse
	}

	roomMicLog struct {
		database.Trans_base
		In  roomMicLog_in
		Out roomMicLog_out
	}
)

func NewRoomMicLog() *roomMicLog {
	return &roomMicLog{}
}
