package audioroom

import (
	"bet24.com/log"
	audioroomPb "bet24.com/servers/micros/audioroom/proto"
	user "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"strconv"
)

var mgr *Manager

type Manager struct {
}

func Run() {
	mgr = new(Manager)
}

// 获取语聊房列表
func (this *Manager) getRoomList(searchKey, beginTime, endTime, sortName, sortType string,
	pageIndex, pageSize, realTime int) (int, interface{}) {
	var roomId int
	// 如果输入的是数字则直接替换成房间id
	if num, err := strconv.ParseInt(searchKey, 10, 64); err == nil {
		roomId = int(num)
		searchKey = ""
	}
	if isRealTime(realTime) {
		return audioroomPb.AdminGetRoomList(searchKey, sortName, sortType, roomId, pageIndex, pageSize)
	}
	return getRoomList(searchKey, beginTime, endTime, sortName, sortType, roomId, pageIndex, pageSize)
}

// 获取语聊房详细信息
func (this *Manager) getRoomDetail(roomId, realTime int) interface{} {
	if roomId <= 0 {
		return nil
	}
	if isRealTime(realTime) {
		return audioroomPb.AdminGetRoomDetail(roomId)
	}
	resp := getRoomDetail(roomId)
	var mics []audioroomPb.MicInfo
	if err := json.Unmarshal([]byte(resp.MicInfo), &mics); err != nil {
		log.Debug("Manager.getRoomDetail json.Unmarshal error. err[%+v]", err)
		return resp
	}
	resp.MicInfo = ""
	resp.UpMicCount = audioroomPb.AdminGetUpMicCount(mics)
	return resp
}

// 获取语聊房成员
func (this *Manager) getRoomMember(beginTime, endTime string, roomId, pageIndex, pageSize, realTime int) (int, interface{}) {
	if isRealTime(realTime) {
		return audioroomPb.AdminGetRoomMember(roomId, pageIndex, pageSize)
	}
	return getRoomMember(beginTime, endTime, roomId, pageIndex, pageSize)
}

// 获取语聊房房间的在线用户
func (this *Manager) getRoomOnlineUsers(roomId, realTime, pageIndex, pageSize int) (int, interface{}) {
	if !isRealTime(realTime) {
		return 0, nil
	}
	return audioroomPb.AdminGetRoomOnlineUsers(roomId, pageIndex, pageSize)
}

// 获取语聊房麦位
func (this *Manager) getRoomMic(roomId, realTime int) (int, interface{}) {
	if !isRealTime(realTime) {
		return 0, nil
	}
	return audioroomPb.AdminGetRoomMic(roomId)
}

// 获取黑名单列表
func (this *Manager) getBlackList(roomId, blackType, pageIndex, pageSize, realTime int, beginTime, endTime string) (int, interface{}) {
	if isRealTime(realTime) {
		return audioroomPb.AdminGetBlackList(roomId, blackType, pageIndex, pageSize)
	}
	return getBlackList(roomId, blackType, pageIndex, pageSize, beginTime, endTime)
}

// 获取房内操作日志
func (this *Manager) getRoomLogRecord(roomId, pageIndex, pageSize int, beginTime, endTime string) (int, interface{}) {
	count, list := getRoomLogRecord(roomId, pageIndex, pageSize, beginTime, endTime)
	for k, v := range list {
		userInfo := user.GetUserInfo(v.UserID)
		if userInfo == nil {
			continue
		}
		list[k].NickName = userInfo.NickName
		toUserInfo := user.GetUserInfo(v.ToUserID)
		if toUserInfo == nil {
			continue
		}
		list[k].ToNickName = toUserInfo.NickName
	}
	return count, list
}

// 获取房间任务列表
func (this *Manager) getRoomTask(realTime, roomId, pageIndex, pageSize int, beginTime, endTime string) (int, interface{}) {
	if isRealTime(realTime) {
		return audioroomPb.AdminGetRoomTask(roomId, pageIndex, pageSize)
	}

	sysTask := audioroomPb.GenerateSysTaskMap(true)
	var sysTaskMap map[int]*audioroomPb.TaskConfig
	if err := json.Unmarshal([]byte(sysTask), &sysTaskMap); err != nil {
		log.Debug("Manager.getRoomTask GenerateSysTaskMap json Unmarshal fail. err:%+v", err)
		return 0, nil
	}

	count, list := getRoomTask(roomId, pageIndex, pageSize, beginTime, endTime)
	for k, v := range list {
		task, ok := sysTaskMap[v.TaskID]
		if ok {
			list[k].TaskName = task.Name
		}
	}
	return count, list
}

// 获取用户的房间任务
func (this *Manager) getUserRoomTask(roomId, userId, pageIndex, pageSize int) (int, interface{}) {
	sysTask := audioroomPb.GenerateSysTaskMap(false)
	var sysTaskMap map[int]*audioroomPb.TaskConfig
	if err := json.Unmarshal([]byte(sysTask), &sysTaskMap); err != nil {
		log.Debug("Manager.getUserRoomTask GenerateSysTaskMap json Unmarshal fail. err:%+v", err)
		return 0, []audioroomPb.AdminUserRoomTask{}
	}

	count, list := getUserRoomTask(roomId, userId, pageIndex, pageSize)
	for k, v := range list {
		task, ok := sysTaskMap[v.TaskID]
		if ok {
			list[k].TaskName = task.Name
		}
	}
	return count, list
}

// 获取房间上下麦日志
func (this *Manager) getRoomMicLog(pageIndex, pageSize int, searchRoom, searchUser, beginTime, endTime string) (int, interface{}) {
	var roomId, userId int
	// 如果输入的是数字则直接替换成房间id
	if num, err := strconv.ParseInt(searchRoom, 10, 64); err == nil {
		roomId = int(num)
		searchRoom = ""
	}
	// 如果输入的是数字则直接替换成用户id
	if num, err := strconv.ParseInt(searchUser, 10, 64); err == nil {
		userId = int(num)
		searchUser = ""
	}
	return getRoomMicLog(roomId, userId, pageIndex, pageSize, searchRoom, searchUser, beginTime, endTime)
}
