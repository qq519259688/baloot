package card

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
)

func getRechargeCardList(req *req_base) resp_base {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var resp resp_base

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_RechargeCard_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, req.UserID)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, req.Status)
	statement.AddParamter("@CardNo", database.AdParamInput, database.AdVarChar, 32, req.CardNo)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, resp.RecordCount)
	statement.AddParamter("@ErrorTimes", database.AdParamOutput, database.AdInteger, 4, resp.ErrorTimes)
	statement.AddParamter("@Crdate", database.AdParamOutput, database.AdVarChar, 32, resp.Crdate)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return resp
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var out info
			out.Rid = int((*ret[0].(*interface{})).(int64))
			out.CardNo = (*ret[1].(*interface{})).(string)
			out.GoldAmount = int((*ret[2].(*interface{})).(int64))
			out.UserID = int((*ret[3].(*interface{})).(int64))
			out.NickName = (*ret[4].(*interface{})).(string)
			out.UseTime = (*ret[5].(*interface{})).(string)
			out.Crdate = (*ret[6].(*interface{})).(string)
			resp.List = append(resp.List, &out)
		}
	}

	resp.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	resp.ErrorTimes = int((*retRows[rowLen-1][1].(*interface{})).(int64))
	resp.Crdate = (*retRows[rowLen-1][2].(*interface{})).(string)
	return resp
}

// 生成兑换卡
func genExchangeCard(req exchangeCard_req) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ExchangeCard_Gen")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, req.OpUserID)
	statement.AddParamter("@OpNickName", database.AdParamInput, database.AdVarChar, 32, req.OpUserName)
	statement.AddParamter("@CardNo", database.AdParamInput, database.AdVarChar, 32, req.CardNo)
	statement.AddParamter("@PerDays", database.AdParamInput, database.AdInteger, 4, req.PerDays-1)
	statement.AddParamter("@UseTimes", database.AdParamInput, database.AdBigint, 8, req.UseTimes)
	statement.AddParamter("@OpenTime", database.AdParamInput, database.AdVarChar, 32, req.OpenTime)
	statement.AddParamter("@ExpireTime", database.AdParamInput, database.AdVarChar, 32, req.ExpireTime)
	statement.AddParamter("@Items", database.AdParamInput, database.AdVarChar, 128, req.Items)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, req.IpAddress)
	sqlString := statement.GenSql()
	dao.CenterDB.ExecSql(sqlString)
}

// 修改兑换卡
func updateExchangeCard(req exchangeCard_req) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ExchangeCard_Update")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, req.OpUserID)
	statement.AddParamter("@OpNickName", database.AdParamInput, database.AdVarChar, 32, req.OpUserName)
	statement.AddParamter("@CardNo", database.AdParamInput, database.AdVarChar, 32, req.CardNo)
	statement.AddParamter("@PerDays", database.AdParamInput, database.AdInteger, 4, req.PerDays-1)
	statement.AddParamter("@UseTimes", database.AdParamInput, database.AdBigint, 8, req.UseTimes)
	statement.AddParamter("@OpenTime", database.AdParamInput, database.AdVarChar, 32, req.OpenTime)
	statement.AddParamter("@ExpireTime", database.AdParamInput, database.AdVarChar, 32, req.ExpireTime)
	statement.AddParamter("@Items", database.AdParamInput, database.AdVarChar, 128, req.Items)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, req.IpAddress)
	sqlString := statement.GenSql()
	dao.CenterDB.ExecSql(sqlString)
}

// 获取兑换卡列表
func getExchangeCardList(cardNo string) []*exchangeCard {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ExchangeCard_GetList")
	statement.AddParamter("@CardNo", database.AdParamInput, database.AdVarChar, 32, cardNo)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*exchangeCard
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info exchangeCard
		info.CardNo = (*ret[0].(*interface{})).(string)
		info.PerDays = int((*ret[1].(*interface{})).(int64)) + 1
		info.UseTimes = int((*ret[2].(*interface{})).(int64))
		info.OpenTime = (*ret[3].(*interface{})).(string)
		info.ExpireTime = (*ret[4].(*interface{})).(string)
		info.Items = (*ret[5].(*interface{})).(string)
		info.Crdate = (*ret[6].(*interface{})).(string)

		list = append(list, &info)
	}
	return list
}

// 兑换日志
func getExchangeCardLog(req exchangeCardLog_req) exchangeCardLog_resp {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var resp exchangeCardLog_resp

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_ExchangeCard_GetLogList")
	statement.AddParamter("@CardNo", database.AdParamInput, database.AdVarChar, 32, req.CardNo)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, req.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, resp.RecordCount)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return resp
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info exchangeCardLog
			info.Rid = int((*ret[0].(*interface{})).(int64))
			info.UserID = int((*ret[1].(*interface{})).(int64))
			info.NickName = (*ret[2].(*interface{})).(string)
			info.CardNo = (*ret[3].(*interface{})).(string)
			info.Crdate = (*ret[4].(*interface{})).(string)
			resp.List = append(resp.List, &info)
		}
	}
	resp.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return resp
}

// 生成比赛卡
func genMatchCard(req matchCard_req) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MatchCard_Gen")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, req.OpUserID)
	statement.AddParamter("@OpNickName", database.AdParamInput, database.AdVarChar, 32, req.OpUserName)
	statement.AddParamter("@OpenTime", database.AdParamInput, database.AdVarChar, 32, req.OpenTime)
	statement.AddParamter("@ExpireTime", database.AdParamInput, database.AdVarChar, 32, req.ExpireTime)
	statement.AddParamter("@TimeMemo", database.AdParamInput, database.AdNVarChar, 1024, req.TimeMemo)
	statement.AddParamter("@SceneMemo", database.AdParamInput, database.AdNVarChar, 2048, req.SceneMemo)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, req.IpAddress)
	sqlString := statement.GenSql()
	dao.CenterDB.ExecSql(sqlString)
}

// 修改比赛卡
func updateMatchCard(req matchCard_req) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MatchCard_Update")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, req.OpUserID)
	statement.AddParamter("@OpNickName", database.AdParamInput, database.AdVarChar, 32, req.OpUserName)
	statement.AddParamter("@CardNo", database.AdParamInput, database.AdVarChar, 32, req.CardNo)
	statement.AddParamter("@OpenTime", database.AdParamInput, database.AdVarChar, 32, req.OpenTime)
	statement.AddParamter("@ExpireTime", database.AdParamInput, database.AdVarChar, 32, req.ExpireTime)
	statement.AddParamter("@TimeMemo", database.AdParamInput, database.AdNVarChar, 1024, req.TimeMemo)
	statement.AddParamter("@SceneMemo", database.AdParamInput, database.AdNVarChar, 2048, req.SceneMemo)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, req.IpAddress)
	sqlString := statement.GenSql()
	dao.CenterDB.ExecSql(sqlString)
}

// 获取比赛卡列表
func getMatchCardList(cardNo string) []*matchCard {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MatchCard_GetList")
	statement.AddParamter("@CardNo", database.AdParamInput, database.AdVarChar, 32, cardNo)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*matchCard
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info matchCard
		info.CardNo = (*ret[0].(*interface{})).(string)
		info.UseTimes = int((*ret[1].(*interface{})).(int64))
		info.OpenTime = (*ret[2].(*interface{})).(string)
		info.ExpireTime = (*ret[3].(*interface{})).(string)
		info.Crdate = (*ret[4].(*interface{})).(string)
		info.TimeMemo = (*ret[5].(*interface{})).(string)
		info.SceneMemo = (*ret[6].(*interface{})).(string)

		list = append(list, &info)
	}
	return list
}
