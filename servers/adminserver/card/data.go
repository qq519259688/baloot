package card

type req_base struct {
	UserID    int    // 用户ID
	Status    int    // 状态 (-1=所有  0=未使用  1=使用)
	CardNo    string // 卡号
	PageIndex int    // 页索引
	PageSize  int    // 页大小
}

type resp_base struct {
	RecordCount int    // 记录数
	ErrorTimes  int    // 错误次数
	Crdate      string // 时间
	List        []*info
}

type info struct {
	Rid        int    // 序号
	CardNo     string // 卡号
	GoldAmount int    // 金币
	UserID     int    // 用户ID
	NickName   string // 昵称
	UseTime    string // 使用时间
	Crdate     string // 生成时间
}

type exchangeCard_req struct {
	OpUserID   int
	OpUserName string
	exchangeCard
	IpAddress string
}

type exchangeCard struct {
	CardNo     string // 卡号
	PerDays    int    // 每天(0=无限制  1=每1天)
	UseTimes   int    // 可使用次数
	OpenTime   string `json:"StartTime" form:"StartTime"` // 开放时间
	ExpireTime string // 过期时间
	Items      string // 物品(json格式)
	Crdate     string // 创建时间
}

type exchangeCardLog_req struct {
	CardNo    string // 卡号
	UserID    int    // 用户id
	BeginTime string // 开始时间
	EndTime   string // 截止时间
	PageIndex int    // 页索引
	PageSize  int    // 页大小
}

type exchangeCardLog_resp struct {
	RecordCount int
	List        []*exchangeCardLog
}

type exchangeCardLog struct {
	Rid      int    // 标识
	UserID   int    // 用户ID
	NickName string // 昵称
	CardNo   string // 卡号
	Crdate   string // 时间
}

type matchCard_req struct {
	OpUserID   int
	OpUserName string
	matchCard
	IpAddress string
}

type matchCard struct {
	CardNo     string // 卡号
	OpenTime   string `json:"StartTime" form:"StartTime"` // 开放时间
	ExpireTime string // 过期时间
	TimeMemo   string // 时间描述
	SceneMemo  string // 场地描述
	UseTimes   int    // 累计使用次数
	Crdate     string // 创建时间
}
