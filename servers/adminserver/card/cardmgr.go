package card

import (
	"bet24.com/log"
)

var mgr *cardManager

type cardManager struct {
}

func Run() {
	mgr = new(cardManager)
	log.Debug("card Manager running...")
}

// 充值卡列表
func (this *cardManager) getRechargeCardList(req *req_base) resp_base {
	return getRechargeCardList(req)
}

// 生成兑换卡
func (this *cardManager) genExchangeCard(req exchangeCard_req) {
	genExchangeCard(req)
}

// 修改兑换卡
func (this *cardManager) updateExchangeCard(req exchangeCard_req) {
	updateExchangeCard(req)
}

// 兑换卡列表
func (this *cardManager) getExchangeCardList(cardNo string) []*exchangeCard {
	return getExchangeCardList(cardNo)
}

// 兑换日志
func (this *cardManager) getExchangeCardLog(req exchangeCardLog_req) exchangeCardLog_resp {
	return getExchangeCardLog(req)
}

// 生成比赛卡
func (this *cardManager) genMatchCard(req matchCard_req) {
	genMatchCard(req)
}

// 修改比赛卡
func (this *cardManager) updateMatchCard(req matchCard_req) {
	updateMatchCard(req)
}

// 比赛卡列表
func (this *cardManager) getMatchCardList(cardNo string) []*matchCard {
	return getMatchCardList(cardNo)
}
