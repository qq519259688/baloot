package card

import (
	"bet24.com/log"
	"bet24.com/servers/adminserver/character"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

// 充值卡列表
func GetRechargeCardList(c *gin.Context) {
	var req *req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "GetRechargeCardList", err)
		return
	}

	resp := mgr.getRechargeCardList(req)
	c.JSON(http.StatusOK, resp)
	return
}

// 生成兑换卡
func GenExchangeCard(c *gin.Context) {
	var req exchangeCard_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "GenExchangeCard", err)
		return
	}

	// 判断是否已E开头
	if req.CardNo != "" && !strings.HasPrefix(req.CardNo, "E") {
		req.CardNo = "E" + req.CardNo
	}

	req.Items = character.GetSpecialCharacter(req.Items)
	mgr.genExchangeCard(req)
	c.JSON(http.StatusOK, nil)
	return
}

// 修改兑换卡
func UpdateExchangeCard(c *gin.Context) {
	var req exchangeCard_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "UpdateExchangeCard", err)
		return
	}
	req.Items = character.GetSpecialCharacter(req.Items)
	mgr.updateExchangeCard(req)
	c.JSON(http.StatusOK, nil)
	return
}

// 兑换卡列表
func GetExchangeCardList(c *gin.Context) {
	var req exchangeCard_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "GetExchangeCardList", err)
		return
	}
	list := mgr.getExchangeCardList(req.CardNo)
	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: len(list),
		List:        list,
	})
	return
}

// 兑换日志
func GetExchangeCardLog(c *gin.Context) {
	var req exchangeCardLog_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "GetExchangeCardLog", err)
		return
	}
	resp := mgr.getExchangeCardLog(req)
	c.JSON(http.StatusOK, resp)
	return
}

// 生成比赛卡
func GenMatchCard(c *gin.Context) {
	var req matchCard_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "GenMatchCard", err)
		return
	}
	mgr.genMatchCard(req)
	c.JSON(http.StatusOK, nil)
	return
}

// 修改比赛卡
func UpdateMatchCard(c *gin.Context) {
	var req matchCard_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "UpdateMatchCard", err)
		return
	}
	mgr.updateMatchCard(req)
	c.JSON(http.StatusOK, nil)
	return
}

// 比赛卡列表
func GetMatchCardList(c *gin.Context) {
	var req matchCard_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "GetMatchCardList", err)
		return
	}
	list := mgr.getMatchCardList(req.CardNo)
	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: len(list),
		List:        list,
	})
	return
}
