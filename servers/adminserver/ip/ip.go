package ip

import (
	game "bet24.com/servers/micros/game/proto"
	"fmt"
	"strings"
)

// 根据IP获取国家地区地理位置
func GetCountryAndRegion(ipAddress string, isWrap bool) string {
	switch ipAddress {
	case "":
		return "未知"
	case "::1":
		return fmt.Sprintf("%s%s", ipAddress, "(未知)")
	}

	// 获取IP地理位置
	country, region := game.GetCountryAndRegion(ipAddress)
	//log.Debug("GetCountryAndRegion ipAddress=%s isWrap=%v country=%s region=%s", ipAddress, isWrap, country, region)
	var build strings.Builder
	build.WriteString(ipAddress)
	if isWrap {
		build.WriteString("<br/>")
	}
	build.WriteString("(")
	build.WriteString(country)

	region = strings.ReplaceAll(region, "未知", "")
	if len(region) > 0 {
		build.WriteString("|")
		build.WriteString(region)
	}

	build.WriteString(")")

	return build.String()
}
