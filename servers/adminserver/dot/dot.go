package dot

import (
	"net/http"

	dotservice "bet24.com/servers/micros/dotservice/proto"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 打点列表
func GetConfigListNotTask(c *gin.Context) {
	jsonData := dotservice.GetConfigListNotTask()
	c.String(http.StatusOK, jsonData)
	return
}

// 获取打点统计
func GetStatList(c *gin.Context) {
	var req dotservice.Request_List
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "GetStatList", err)
		return
	}

	jsonData := dotservice.GetStatList(req.BeginTime, req.EndTime, req.Event)
	c.String(http.StatusOK, jsonData)
	return
}

// 获取打点任务统计
func GetTaskStatList(c *gin.Context) {
	var req dotservice.Request_List
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "GetTaskStatList", err)
		return
	}

	jsonData := dotservice.GetTaskStatList(req.BeginTime, req.EndTime)
	c.String(http.StatusOK, jsonData)
	return
}
