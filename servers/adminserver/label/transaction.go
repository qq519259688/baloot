package label

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
)

// 获取留存统计
func trans_liveStatList(req request_liveStatList) response_liveStatList {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var result response_liveStatList

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_LabelLiveStat_GetList")
	statement.AddParamter("@LabelID", database.AdParamInput, database.AdVarChar, 32, req.LabelID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, result.RecordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return result
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var out liveStatListModel

			out.DateFlag = (*ret[0].(*interface{})).(string)
			out.LabelID = (*ret[1].(*interface{})).(string)
			out.RegCount = int((*ret[2].(*interface{})).(int64))
			out.LiveUsers_1 = int((*ret[3].(*interface{})).(int64))
			out.LiveUsers_3 = int((*ret[4].(*interface{})).(int64))
			out.LiveUsers_5 = int((*ret[5].(*interface{})).(int64))
			out.LiveUsers_7 = int((*ret[6].(*interface{})).(int64))
			out.LiveUsers_15 = int((*ret[7].(*interface{})).(int64))
			out.LiveUsers_30 = int((*ret[8].(*interface{})).(int64))

			result.List = append(result.List, out)
		}
	}

	result.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return result
}
