package label

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 获取标签配置
func GetConfigList(c *gin.Context) {
	configList := mgr.getConfigList()
	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: 0,
		List:        configList,
	})
	return
}

// 留存统计
func GetLiveStatList(c *gin.Context) {
	var req request_liveStatList
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "label.GetLiveStatList", err)
		return
	}

	resp := mgr.getLiveStatList(req)
	c.JSON(http.StatusOK, resp)
	return
}
