package label

import (
	userlabel "bet24.com/servers/micros/userlabel/proto"
)

var mgr *Manager

type Manager struct {
}

func Run() {
	mgr = new(Manager)
}

// 获取标签配置
func (this *Manager) getConfigList() []userlabel.ConfigSimpleInfo {
	return userlabel.GetConfigSimpleInfo()
}

// 留存统计
func (this *Manager) getLiveStatList(req request_liveStatList) response_liveStatList {
	// 标签配置
	configList := userlabel.GetConfigSimpleInfo()

	// 留存
	resp := trans_liveStatList(req)
	for i := 0; i < len(resp.List); i++ {
		for _, v := range configList {
			if resp.List[i].LabelID != v.LabelId {
				continue
			}
			resp.List[i].LabelName = v.LabelName
			resp.List[i].Color = v.Color
			break
		}
	}

	return resp
}
