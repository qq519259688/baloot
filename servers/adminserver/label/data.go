package label

// 留存统计
type (
	request_liveStatList struct {
		LabelID   string // 标签ID
		BeginTime string // 开始时间
		EndTime   string // 截止时间
		PageIndex int    // 页索引
		PageSize  int    // 页大小
	}

	liveStatListModel struct {
		DateFlag     string // 日期标识
		LabelID      string // 标签ID
		LabelName    string // 标签名称
		Color        string // 颜色
		RegCount     int    // 注册用户数
		LiveUsers_1  int    // 次日留存
		LiveUsers_3  int    // 3日留存
		LiveUsers_5  int    // 5日留存
		LiveUsers_7  int    // 7日留存
		LiveUsers_15 int    // 15日留存
		LiveUsers_30 int    // 30日留存
	}

	response_liveStatList struct {
		RecordCount int // 记录数
		List        []liveStatListModel
	}
)
