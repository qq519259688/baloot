package errormsg

import (
	"encoding/json"
	_ "fmt"
	"log"
	"os"
	"sync"
	"time"
)

var Instance *errormsg

func Run() {
	Instance = new(errormsg)
	Instance.lock = &sync.RWMutex{}
	Instance.run()
}

func GetErrorMsg(msg string, ret_code int) string {
	Instance.lock.RLock()
	defer Instance.lock.RUnlock()
	for _, v := range Instance.Msgs {
		if v.Msg == msg {
			return v.getMsg(ret_code)
		}
	}
	return ""
}

type error_code struct {
	RetCode  int
	ErrorMsg string
}

type msg struct {
	Msg        string
	ErrorCodes []error_code
}

func (m *msg) getMsg(ret_code int) string {
	for _, v := range m.ErrorCodes {
		if v.RetCode == ret_code {
			return v.ErrorMsg
		}
	}
	return ""
}

type errormsg struct {
	Msgs []msg
	lock *sync.RWMutex
}

func (e *errormsg) run() {
	e.refreshData()
}

func (e *errormsg) refreshData() {
	go e.doRefresh()
	time.AfterFunc(5*time.Minute, e.refreshData)
}

func (e *errormsg) doRefresh() {
	e.lock.Lock()
	defer e.lock.Unlock()
	data, err := os.ReadFile("fishconf/errormsg.json")
	if err != nil {
		log.Fatalf("read config failed fishconf/errormsg.json %v", err)
	}
	//fmt.Println(string(data))
	err = json.Unmarshal(data, &e.Msgs)
	if err != nil {
		log.Fatalf("Unmarshal config failed fishconf/errormsg.json err:%v", err)
	}

	//fmt.Printf("Msgs count = %d\n", len(e.Msgs))
}
