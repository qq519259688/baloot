package matchdata

import (
	"bet24.com/database"
)

// 获取赛事列表
type (
	getMatchList_in struct {
		SerialNumber string // 赛事场次
		BeginTime    string // 开始时间
		EndTime      string // 结束时间
		PageIndex    int    // 请求的页数
		PageSize     int    // 请求的个数
	}

	matchStatistics struct {
		SerialNumber   string // 比赛场次
		MatchName      string // 比赛名称
		EnrollFee      string // 报名费（存在多种报名方式）[ItemId, Count]
		MaxUserCount   int    // 最多报名人数
		TableUserCount int    // 每张桌子人数
		JoinUserCount  int    // 加入的用户数
		StartTime      string // 开始时间
		EndTime        string // 结束时间
	}

	getMatchList_out struct {
		RecordCount int // 总记录数
		List        []matchStatistics
	}

	getMatchList struct {
		database.Trans_base
		In  getMatchList_in
		Out getMatchList_out
	}
)

func NewGetMatchList() *getMatchList {
	return &getMatchList{}
}

// 获取加入的名单
type (
	getJoinList_in struct {
		SerialNumber string // 赛事场次
		UserID       int    // 用户ID
		IsAward      int    // 是否获奖（-1=全部，0=否，1=是）
		BeginTime    string // 开始时间
		EndTime      string // 结束时间
		PageIndex    int    // 请求的页数
		PageSize     int    // 请求的个数
	}

	joinList struct {
		SerialNumber string // 赛事场次
		Rank         int    // 排名
		UserID       int    // 用户ID
		Nickname     string // 用户昵称
		ItemID       int    // 道具ID（报名道具）
		ItemCount    int    // 报名道具的数量
		MatchName    string // 赛事名称
		EnrollTime   string // 报名时间
		Score        int    // 分数
		Prize        string // 奖品
		RobotID      int    // 机器人ID
	}

	getJoinList_out struct {
		RecordCount int // 总记录数
		List        []joinList
	}

	getJoinList struct {
		database.Trans_base
		In  getJoinList_in
		Out getJoinList_out
	}
)

func NewGetJoinList() *getJoinList {
	return &getJoinList{}
}

// 获取加入的名单
type (
	getTypeList_in struct {
		SerialNumber string // 赛事场次
	}

	typeList struct {
		SerialNumber string // 赛事场次
		Rank         int    // 排名
		UserID       int    // 用户ID
		NickName     string // 用户昵称
		ItemID       int    // 道具ID（报名道具）
		ItemCount    int    // 报名道具的数量
		EnrollTime   string // 报名时间
		Score        int    // 分数
		Prize        string // 奖品
		RobotID      int    // 机器人ID
	}

	getTypeList_out struct {
		RecordCount int // 总记录数
		List        []typeList
	}

	getTypeList struct {
		database.Trans_base
		In  getTypeList_in
		Out getTypeList_out
	}
)

func NewGetTypeList() *getTypeList {
	return &getTypeList{}
}

// 获取加入的总费用
type (
	getJoinTotalFee_in struct {
		SerialNumber string // 赛事场次
	}

	joinTotalFee struct {
		ItemID    int
		ItemCount int
	}

	getJoinTotalFee_out struct {
		List []joinTotalFee
	}

	getJoinTotalFee struct {
		database.Trans_base
		In  getJoinTotalFee_in
		Out getJoinTotalFee_out
	}
)

func NewGetJoinTotalFee() *getJoinTotalFee {
	return &getJoinTotalFee{}
}
