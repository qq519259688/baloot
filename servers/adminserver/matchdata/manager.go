package matchdata

var mgr *Manager

type Manager struct {
}

func Run() {
	mgr = new(Manager)
}

// 获取赛事列表
func (this *Manager) getMatchList(pageIndex, pageSize int, serialNumber, beginTime, endTime string) (int, []matchStatistics) {
	return transGetMatchList(pageIndex, pageSize, serialNumber, beginTime, endTime)
}

// 获取加入的名单
func (this *Manager) getJoinList(userID, isAward, pageIndex, pageSize int, serialNumber, beginTime, endTime string) (int, []joinList) {
	return transGetJoinList(userID, isAward, pageIndex, pageSize, serialNumber, beginTime, endTime)
}

// 根据类型获取名单
func (this *Manager) getTypeList(serialNumber string) []typeList {
	return transGetTypeList(serialNumber)
}

// 获取加入的总费用
func (this *Manager) getJoinTotalFee(serialNumber string) []joinTotalFee {
	return transGetJoinTotalFee(serialNumber)
}
