package matchdata

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 获取赛事列表
func GetMatchList(c *gin.Context) {
	obj := NewGetMatchList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getMatchList", err)
		return
	}
	count, list := mgr.getMatchList(obj.In.PageIndex, obj.In.PageSize,
		obj.In.SerialNumber, obj.In.BeginTime, obj.In.EndTime)
	obj.Out.RecordCount = count
	obj.Out.List = list
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 获取加入的名单
func GetJoinList(c *gin.Context) {
	obj := NewGetJoinList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getJoinList", err)
		return
	}
	count, list := mgr.getJoinList(obj.In.UserID, obj.In.IsAward, obj.In.PageIndex, obj.In.PageSize,
		obj.In.SerialNumber, obj.In.BeginTime, obj.In.EndTime)
	obj.Out.RecordCount = count
	obj.Out.List = list
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 根据类型获取名单
func GetTypeList(c *gin.Context) {
	obj := NewGetTypeList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getTypeList", err)
		return
	}
	obj.Out.List = mgr.getTypeList(obj.In.SerialNumber)
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 获取加入的总费用
func GetJoinTotalFee(c *gin.Context) {
	obj := NewGetJoinTotalFee()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getJoinTotalFee", err)
		return
	}
	obj.Out.List = mgr.getJoinTotalFee(obj.In.SerialNumber)
	c.JSON(http.StatusOK, obj.Out)
	return
}
