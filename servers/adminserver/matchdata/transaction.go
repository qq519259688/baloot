package matchdata

import (
	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"encoding/json"
	"runtime/debug"
)

// 获取赛事列表
func transGetMatchList(pageIndex, pageSize int, serialNumber, beginTime, endTime string) (int, []matchStatistics) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var recordCount int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MatchData_GetStatistics")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	var list []matchStatistics
	if rowLen <= 0 {
		return 0, list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var data matchStatistics

			data.SerialNumber = ret[0].(string)
			data.MatchName = ret[1].(string)
			data.EnrollFee = ret[2].(string)
			data.MaxUserCount = int(ret[3].(int64))
			data.TableUserCount = int(ret[4].(int64))
			data.JoinUserCount = int(ret[5].(int64))
			data.StartTime = ret[6].(string)
			data.EndTime = ret[7].(string)
			list = append(list, data)
		}
	}

	if list == nil {
		list = make([]matchStatistics, 0)
	}
	recordCount = int(retRows[rowLen-1][0].(int64))
	return recordCount, list
}

// 获取加入的名单
func transGetJoinList(userID, isAward, pageIndex, pageSize int, serialNumber, beginTime, endTime string) (int, []joinList) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var recordCount int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MatchData_GetJoinList")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userID)
	statement.AddParamter("@IsAward", database.AdParamInput, database.AdInteger, 4, isAward)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	var list []joinList
	if rowLen <= 0 {
		return 0, list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var data joinList

			data.SerialNumber = ret[0].(string)
			data.Rank = int(ret[1].(int64))
			data.UserID = int(ret[2].(int64))
			data.Nickname = ret[3].(string)
			data.ItemID = int(ret[4].(int64))
			data.ItemCount = int(ret[5].(int64))
			data.MatchName = ret[6].(string)
			data.EnrollTime = ret[7].(string)
			data.Score = int(ret[8].(int64))
			data.Prize = ret[9].(string)
			data.RobotID = int(ret[10].(int64))
			list = append(list, data)
		}
	}

	if list == nil {
		list = make([]joinList, 0)
	}
	recordCount = int(retRows[rowLen-1][0].(int64))
	return recordCount, list
}

// 根据类型获取名单
func transGetTypeList(serialNumber string) []typeList {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MatchData_GetTypeList")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	jsonData := dbengine.Execute(sqlString)

	var list []typeList
	if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
		log.Error("transaction.transGetJoinTotalFee json unmarshal serialNumber=%s err %v", serialNumber, err)
	}
	return list
}

// 获取加入的总费用
func transGetJoinTotalFee(serialNumber string) []joinTotalFee {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MatchData_GetJoinTotalFee")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	jsonData := dbengine.Execute(sqlString)

	var out []joinTotalFee
	if err := json.Unmarshal([]byte(jsonData), &out); err != nil {
		log.Error("transaction.transGetJoinTotalFee json unmarshal serialNumber=%s err %v", serialNumber, err)
	}
	return out
}
