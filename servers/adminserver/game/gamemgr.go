package game

import (
	"fmt"
	"sort"
	"time"

	"bet24.com/log"
)

var mgr *gameManager

type gameManager struct {
	index_list map[string]*gameIndexInfo
}

func Run() {
	mgr = new(gameManager)
	mgr.index_list = make(map[string]*gameIndexInfo)
}

func (this *gameManager) recordIndexStat(gameId, partnerId int, beginTime, endTime string) []*gameIndexInfo {
	var list []*gameIndexInfo

	end, _ := time.Parse(dateFormat, endTime)

	for begin, _ := time.Parse(dateFormat, beginTime); !begin.After(end); begin = begin.AddDate(0, 0, 1) {
		dateStr := begin.Format(dateFormat)
		key := fmt.Sprintf("%s_%d_%d", dateStr, gameId, partnerId)
		v, ok := this.index_list[key]
		if ok {
			list = append(list, v)
			continue
		}

		for _, v := range gameRecordIndex(gameId, partnerId, dateStr, dateStr) {

			log.Debug("gamemgr.recordIndexStat ==> %+v", v)

			if begin.Format(dateFormat) == time.Now().Format(dateFormat) {
				list = append(list, v)
				continue
			}

			key = fmt.Sprintf("%s_%d_%d", v.DateFlag, gameId, partnerId)
			this.index_list[key] = v
			list = append(list, v)
		}
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].DateFlag < list[j].DateFlag
	})

	return list
}

// 获取牌局统计
func (this *gameManager) getCardStatList(req requestInfo) (int, []cardStatInfo) {
	return trans_getCardStatList(req)
}

// 中途退出统计
func (this *gameManager) getMidwayStatList(req requestInfo) (int, []midwayStatInfo) {
	return trans_getMidwayStatList(req)
}

// 水池统计
func (this *gameManager) getWaterPoolStatList(req requestInfo) []*info {
	return trans_getWaterPoolStatList(req.BeginTime, req.EndTime)
}
