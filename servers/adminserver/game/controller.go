package game

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 游戏记录报表(局数、时长)
func RecordIndexStat(c *gin.Context) {
	var req struct {
		GameID    int
		PartnerID int
		BeginTime string
		EndTime   string
	}
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "game.RecordIndexStat", err)
		return
	}

	list := mgr.recordIndexStat(req.GameID, req.PartnerID, req.BeginTime, req.EndTime)

	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 获取牌局统计
func GetCardStatList(c *gin.Context) {
	var req requestInfo
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "game.GetCardStatList", err)
		return
	}

	recordCount, list := mgr.getCardStatList(req)

	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: recordCount,
		List:        list,
	})
	return
}

// 中途退出统计
func GetMidwayStatList(c *gin.Context) {
	var req requestInfo
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "game.GetMidwayStatList", err)
		return
	}

	recordCount, list := mgr.getMidwayStatList(req)

	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: recordCount,
		List:        list,
	})
	return
}

// 水池统计
func GetWaterPoolStatList(c *gin.Context) {
	var req requestInfo
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "game.GetWaterPoolStatList", err)
		return
	}

	list := mgr.getWaterPoolStatList(req)

	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: 0,
		List:        list,
	})
	return
}
