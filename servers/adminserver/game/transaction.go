package game

import (
	"runtime/debug"
	"strconv"

	"bet24.com/servers/adminserver/dao"

	"bet24.com/database"
	"bet24.com/log"
)

// 游戏记录报表(局数、时长)
func gameRecordIndex(gameId, partnerId int, beginTime, endTime string) []*gameIndexInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*gameIndexInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_GameRecord_GetReport")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, partnerId)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out gameIndexInfo

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.TotalCount = int((*ret[1].(*interface{})).(int64))
		out.WinCount = int((*ret[2].(*interface{})).(int64))
		out.PlaySeconds = int((*ret[3].(*interface{})).(int64))
		out.Players = int((*ret[4].(*interface{})).(int64))
		out.TaxAmount = int((*ret[5].(*interface{})).(int64))

		list = append(list, &out)
	}

	return list
}

// 获取牌局统计
func trans_getCardStatList(req requestInfo) (int, []cardStatInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var recordCount int
	var list []cardStatInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_CardStat_GetList")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, req.GameId)
	statement.AddParamter("@TypeID", database.AdParamInput, database.AdInteger, 4, req.TypeId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return recordCount, list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			var info cardStatInfo
			ret := retRows[i]
			info.DateFlag = (*ret[0].(*interface{})).(string)
			info.GameID = int((*ret[1].(*interface{})).(int64))
			info.EnglishName = (*ret[2].(*interface{})).(string)
			info.PlayUserCount = int((*ret[3].(*interface{})).(int64))
			info.SendAmount = int((*ret[4].(*interface{})).(int64))
			info.ConsumeAmount = int((*ret[5].(*interface{})).(int64))
			info.BreakUserCount = int((*ret[6].(*interface{})).(int64))
			info.BreakTimes = int((*ret[7].(*interface{})).(int64))
			info.PlayCount = int((*ret[8].(*interface{})).(int64))
			info.PlaySeconds = int((*ret[9].(*interface{})).(int64))
			info.MidwayUserCount = int((*ret[10].(*interface{})).(int64))
			info.MidwayTimes = int((*ret[11].(*interface{})).(int64))
			list = append(list, info)
		}
	}

	recordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return recordCount, list
}

// 中途退出统计
func trans_getMidwayStatList(req requestInfo) (int, []midwayStatInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var recordCount int
	var list []midwayStatInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_MidwayStat_GetList")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, req.GameId)
	statement.AddParamter("@TypeID", database.AdParamInput, database.AdInteger, 4, req.TypeId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return recordCount, list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			var info midwayStatInfo
			ret := retRows[i]
			info.DateFlag = (*ret[0].(*interface{})).(string)
			info.GameID = int((*ret[1].(*interface{})).(int64))
			info.EnglishName = (*ret[2].(*interface{})).(string)
			info.SourceName = (*ret[3].(*interface{})).(string)
			info.PlayUserCount = int((*ret[4].(*interface{})).(int64))
			info.PlayCount = int((*ret[5].(*interface{})).(int64))
			info.MidwayUserCount = int((*ret[6].(*interface{})).(int64))
			info.MidwayTimes = int((*ret[7].(*interface{})).(int64))
			list = append(list, info)
		}
	}

	recordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return recordCount, list
}

// 水池统计
func trans_getWaterPoolStatList(beginTime, endTime string) []*info {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_WaterPoolStat_GetList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*info
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out info

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.ItemName = (*ret[1].(*interface{})).(string)
		out.ItemValue = strconv.Itoa(int((*ret[2].(*interface{})).(int64)))
		list = append(list, &out)
	}
	return list
}
