package game

const dateFormat = "2006-01-02"

type gameIndexInfo struct {
	DateFlag    string
	TotalCount  int
	WinCount    int
	PlaySeconds int
	Players     int
	TaxAmount   int
}

type requestInfo struct {
	GameId    int    // 游戏ID
	TypeId    int    //  标签类型
	BeginTime string // 开始时间
	EndTime   string // 截止时间
	PageIndex int    // 页索引
	PageSize  int    // 页大小
}

// 牌局统计信息
type cardStatInfo struct {
	DateFlag        string // 日期标志
	GameID          int    // 游戏ID
	EnglishName     string // 游戏名称
	PlayUserCount   int    // 游戏人数
	SendAmount      int    // 发放
	ConsumeAmount   int    // 消耗
	BreakUserCount  int    // 破产人数
	BreakTimes      int    // 破产次数
	PlayCount       int    // 游戏局数
	PlaySeconds     int    // 游戏时长
	MidwayUserCount int    // 中途退出人数

	MidwayTimes int // 中途退出次数
}

// 中途退出统计信息
type midwayStatInfo struct {
	DateFlag        string // 日期标识
	GameID          int    // 游戏ID
	EnglishName     string // 游戏名称
	SourceName      string // 服务器名称
	PlayUserCount   int    // 游戏人数
	PlayCount       int    // 游戏局数
	MidwayUserCount int    // 中途退出人数
	MidwayTimes     int    // 中途退出次数
}

// 水池统计信息
type info struct {
	Rid       int    // 标识
	DateFlag  string // 日期
	ItemType  int    `json:",omitempty"` // 项类型
	ItemName  string // 项名称
	ItemValue string // 项值
	ItemTag   int    `json:",omitempty"` // 项标签
}
