package rank

import (
	"strconv"
)

var mgr *Manager

type Manager struct {
}

func Run() {
	mgr = new(Manager)
}

func (r *Manager) getRankingList(searchKey, beginTime, endTime string, rankType, pageIndex, pageSize int) (int, []RankingList) {
	var userId int
	// 如果输入的是数字则直接替换成用户ID
	if num, err := strconv.ParseInt(searchKey, 10, 64); err == nil {
		userId = int(num)
		searchKey = ""
	}
	return getRankingList(searchKey, beginTime, endTime, rankType, userId, pageIndex, pageSize)
}
