package rank

import (
	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"runtime/debug"
)

// 获取榜单列表
func getRankingList(nickname, beginTime, endTime string, rankType, userId, pageIndex, pageSize int) (int, []RankingList) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var recordCount int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Rank_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Nickname", database.AdParamInput, database.AdNVarChar, 32, nickname)
	statement.AddParamter("@RankType", database.AdParamInput, database.AdInteger, 4, rankType)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	var records []RankingList
	if rowLen <= 0 {
		return 0, records
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var data RankingList
			data.RankType = int(ret[0].(int64))
			data.Rank = int(ret[1].(int64))
			data.UserID = int(ret[2].(int64))
			data.Amount = int(ret[3].(int64))
			data.Crdate = ret[4].(string)
			data.IsGift = int(ret[5].(int64))
			data.NickName = ret[6].(string)
			records = append(records, data)
		}
	}

	if records == nil {
		records = make([]RankingList, 0)
	}
	recordCount = int(retRows[rowLen-1][0].(int64))
	return recordCount, records
}
