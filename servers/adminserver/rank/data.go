package rank

import (
	"bet24.com/database"
)

// 获取排行榜列表
type (
	getRankingList_in struct {
		SearchKey string // 搜索的内容
		RankType  int    // 榜单类型【具体类型查看 coreservice\rank\rankItem.go 文件】
		BeginTime string // 开始时间
		EndTime   string // 结束时间
		PageIndex int    // 第几页
		PageSize  int    // 请求的个数
	}

	RankingList struct {
		RankType int    // 榜单类型【具体类型查看 coreservice\rank\rankItem.go 文件】
		Rank     int    // 名次
		UserID   int    // 用户ID
		Amount   int    // 数量
		Crdate   string // 创建时间
		IsGift   int    // 是否领取
		NickName string // 用户昵称
	}

	getRankingList_out struct {
		RecordCount int `json:",omitempty"`
		List        []RankingList
	}

	getRankList struct {
		database.Trans_base
		In  getRankingList_in
		Out getRankingList_out
	}
)

func NewGetRankingList() *getRankList {
	return &getRankList{}
}
