package rank

import (
	"bet24.com/log"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 获取排行榜列表
func GetRankingList(c *gin.Context) {
	obj := NewGetRankingList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "getRankingList", err)
		return
	}
	count, list := mgr.getRankingList(obj.In.SearchKey, obj.In.BeginTime, obj.In.EndTime, obj.In.RankType,
		obj.In.PageIndex, obj.In.PageSize)
	obj.Out.RecordCount = count
	obj.Out.List = list
	c.JSON(http.StatusOK, obj.Out)
	return
}
