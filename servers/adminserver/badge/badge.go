package badge

import (
	"bet24.com/log"
	badge "bet24.com/servers/micros/badge/proto"
	"encoding/json"
	"time"
)

var mgr *badgeManager

type badgeManager struct {
	badge_list []badge.Badge
}

func getBadgeManager() *badgeManager {
	if mgr == nil {
		mgr = new(badgeManager)
		mgr.refreshData()
	}
	return mgr
}

func (this *badgeManager) refreshData() {
	this.load()
	time.AfterFunc(1*time.Minute, this.refreshData)
}

func (this *badgeManager) load() {
	data := badge.GetSysBadgeList()

	if err := json.Unmarshal([]byte(data), &this.badge_list); err != nil {
		log.Error("badge.load data unmarshal fail %v", err)
	}

	return
}

func (this *badgeManager) getBadgeName(badgeId int) string {
	for _, v := range this.badge_list {
		if v.BadgeId == badgeId {
			return v.BadgeName
		}
	}
	return ""
}
