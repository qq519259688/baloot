package announce

import (
	"encoding/json"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
)

// 获取公告列表
func getList(rid int) []*info {
	var list []*info
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Announce_GetList")
	statement.AddParamter("@Rid", database.AdParamInput, database.AdInteger, 4, rid)
	sqlString := statement.GenSql()
	jsonData := dao.CenterDB.ExecSqlJson(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
		log.Error("announce.transaction load json unmarshal err %v", err)
	}
	return list
}

// 发布公告
func send(r *Req_Info) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Announce_Send")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, r.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, r.OpUserName)
	statement.AddParamter("@Title_En", database.AdParamInput, database.AdNVarChar, 32, r.Title_En)
	statement.AddParamter("@Content_En", database.AdParamInput, database.AdNVarChar, 4000, r.Content_En)
	statement.AddParamter("@Title_Eg", database.AdParamInput, database.AdNVarChar, 32, r.Title_Eg)
	statement.AddParamter("@Content_Eg", database.AdParamInput, database.AdNVarChar, 4000, r.Content_Eg)
	statement.AddParamter("@Priority", database.AdParamInput, database.AdInteger, 4, r.Priority)
	statement.AddParamter("@Version", database.AdParamInput, database.AdInteger, 4, r.Version)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, r.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, r.EndTime)
	statement.AddParamter("@Enabled", database.AdParamInput, database.AdInteger, 4, r.Enabled)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, r.IpAddress)
	sqlString := statement.GenSql()
	dao.CenterDB.ExecSql(sqlString)
	return
}

// 修改公告
func update(r *Req_Info) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Announce_Update")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, r.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, r.OpUserName)
	statement.AddParamter("@Rid", database.AdParamInput, database.AdInteger, 4, r.Rid)
	statement.AddParamter("@Title_En", database.AdParamInput, database.AdNVarChar, 32, r.Title_En)
	statement.AddParamter("@Content_En", database.AdParamInput, database.AdNVarChar, 4000, r.Content_En)
	statement.AddParamter("@Title_Eg", database.AdParamInput, database.AdNVarChar, 32, r.Title_Eg)
	statement.AddParamter("@Content_Eg", database.AdParamInput, database.AdNVarChar, 4000, r.Content_Eg)
	statement.AddParamter("@Priority", database.AdParamInput, database.AdInteger, 4, r.Priority)
	statement.AddParamter("@Version", database.AdParamInput, database.AdInteger, 4, r.Version)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, r.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, r.EndTime)
	statement.AddParamter("@Enabled", database.AdParamInput, database.AdInteger, 4, r.Enabled)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, r.IpAddress)
	sqlString := statement.GenSql()
	dao.CenterDB.ExecSql(sqlString)
	return
}

// 删除公告
func del(r *Req_Info) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Announce_Del")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, r.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, r.OpUserName)
	statement.AddParamter("@Rid", database.AdParamInput, database.AdInteger, 4, r.Rid)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, r.IpAddress)
	sqlString := statement.GenSql()
	dao.CenterDB.ExecSql(sqlString)
	return
}
