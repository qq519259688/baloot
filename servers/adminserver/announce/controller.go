package announce

import (
	"net/http"

	"bet24.com/log"

	"github.com/gin-gonic/gin"
)

// 公告列表
func GetList(c *gin.Context) {
	var req *Req_Info
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "announce.GetList", err)
		return
	}

	var resp struct {
		RecordCount int
		List        interface{}
	}

	resp.List = mgr.getList(req.Rid)
	c.JSON(http.StatusOK, resp)
	return
}

// 发布公告
func Send(c *gin.Context) {
	var req *Req_Info
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "announce.Send", err)
		return
	}
	mgr.send(req)
	c.JSON(http.StatusOK, "success")
	return
}

// 修改公告
func Update(c *gin.Context) {
	var req *Req_Info
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "announce.Update", err)
		return
	}
	mgr.update(req)
	c.JSON(http.StatusOK, "success")
	return
}

// 删除公告
func Del(c *gin.Context) {
	var req *Req_Info
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "announce.Del", err)
		return
	}
	mgr.del(req)
	c.JSON(http.StatusOK, "success")
	return
}
