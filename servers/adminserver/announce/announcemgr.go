package announce

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
)

var mgr *announceManager

type announceManager struct {
}

func Run() {
	mgr = new(announceManager)
	log.Debug("announce manager running...")
}

// 获取列表
func (this *announceManager) getList(rid int) []*info {
	return getList(rid)
}

// 发布公告
func (this *announceManager) send(r *Req_Info) {
	send(r)
	this.refresh()
}

// 修改公告
func (this *announceManager) update(r *Req_Info) {
	update(r)
	this.refresh()
}

// 删除公告
func (this *announceManager) del(r *Req_Info) {
	del(r)
	this.refresh()
}

// 刷新数据
func (this *announceManager) refresh() {
	client.AnnounceRefresh()
}
