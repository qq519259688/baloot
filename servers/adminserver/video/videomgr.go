package video

import (
	"fmt"
	"sort"
	"time"

	"bet24.com/log"
)

var mgr *videoManager

type videoManager struct {
	playStat_list map[string]*playStatInfo
	source_list   map[string][]*sourceInfo
	index_list    map[string]*indexInfo
	userStat_list map[string]*userStatInfo
}

func Run() {
	mgr = new(videoManager)
	mgr.playStat_list = make(map[string]*playStatInfo)
	mgr.source_list = make(map[string][]*sourceInfo)
	mgr.index_list = make(map[string]*indexInfo)
	mgr.userStat_list = make(map[string]*userStatInfo)
}

func (this *videoManager) playStat(beginTime, endTime string, partnerId int) (int, int, int, []*playStatInfo) {
	var (
		list           []*playStatInfo
		totalReqTimes  int
		totalPlayTimes int
		totalFailTimes int
	)

	end, _ := time.Parse(dateFormat, endTime)

	for begin, _ := time.Parse(dateFormat, beginTime); !begin.After(end); begin = begin.AddDate(0, 0, 1) {
		dateStr := begin.Format(dateFormat)
		key := fmt.Sprintf("%s_%d", dateStr, partnerId)
		v, ok := this.playStat_list[key]
		if ok {
			totalReqTimes += v.ReqTimes
			totalPlayTimes += v.PlayTimes
			totalFailTimes += v.FailTimes
			list = append(list, v)
			continue
		}

		for _, v := range playStat(dateStr, dateStr, partnerId) {

			log.Debug("videomgr.playStat ==> %+v", v)

			totalReqTimes += v.ReqTimes
			totalPlayTimes += v.PlayTimes
			totalFailTimes += v.FailTimes

			if begin.Format(dateFormat) == time.Now().Format(dateFormat) {
				list = append(list, v)
				continue
			}

			key = fmt.Sprintf("%s_%d", v.DateFlag, partnerId)
			this.playStat_list[key] = v
			list = append(list, v)
		}
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].DateFlag > list[j].DateFlag
	})

	return totalReqTimes, totalPlayTimes, totalFailTimes, list
}

func (this *videoManager) sourceStat(beginTime, endTime string, partnerId int) []*sourceInfo {
	var list []*sourceInfo
	end, _ := time.Parse(dateFormat, endTime)

	for begin, _ := time.Parse(dateFormat, beginTime); !begin.After(end); begin = begin.AddDate(0, 0, 1) {
		dateStr := begin.Format(dateFormat)
		key := fmt.Sprintf("%s_%d", dateStr, partnerId)
		v, ok := this.source_list[key]
		if ok {
			list = append(list, v...)
			continue
		}

		for _, v := range source(dateStr, dateStr, partnerId) {

			log.Debug("videomgr.sourceStat ==> %+v", v)

			if begin.Format(dateFormat) == time.Now().Format(dateFormat) {
				list = append(list, v)
				continue
			}

			key = fmt.Sprintf("%s_%d", v.DateFlag, partnerId)
			this.source_list[key] = append(this.source_list[key], v)
			list = append(list, v)
		}
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].DateFlag > list[j].DateFlag
	})

	return list
}

func (this *videoManager) indexStat(beginTime, endTime string) []*indexInfo {
	var list []*indexInfo
	end, _ := time.Parse(dateFormat, endTime)

	for begin, _ := time.Parse(dateFormat, beginTime); !begin.After(end); begin = begin.AddDate(0, 0, 1) {
		dateStr := begin.Format(dateFormat)
		v, ok := this.index_list[dateStr]
		if ok {
			list = append(list, v)
			continue
		}

		for _, v := range index(dateStr, dateStr) {

			log.Debug("videomgr.indexStat ==> %+v", v)

			if begin.Format(dateFormat) == time.Now().Format(dateFormat) {
				list = append(list, v)
				continue
			}

			this.index_list[v.DateFlag] = v
			list = append(list, v)
		}
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].DateFlag < list[j].DateFlag
	})

	return list
}

func (this *videoManager) userStat(beginTime, endTime string) []*userStatInfo {
	var list []*userStatInfo

	end, _ := time.Parse(dateFormat, endTime)

	for begin, _ := time.Parse(dateFormat, beginTime); !begin.After(end); begin = begin.AddDate(0, 0, 1) {
		dateStr := begin.Format(dateFormat)
		v, ok := this.userStat_list[dateStr]
		if ok {
			list = append(list, v)
			continue
		}

		for _, v := range userStat(dateStr, dateStr) {

			log.Debug("videomgr.userStat ==> %+v", v)

			if begin.Format(dateFormat) == time.Now().Format(dateFormat) {
				list = append(list, v)
				continue
			}

			this.userStat_list[v.DateFlag] = v
			list = append(list, v)
		}
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].DateFlag > list[j].DateFlag
	})

	return list
}
