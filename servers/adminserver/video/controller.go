package video

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 广告播放统计(按天)
func PlayStat(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "video.PlayStat", err)
		return
	}

	var resp struct {
		List           interface{}
		TotalReqTimes  int
		TotalPlayTimes int
		TotalFailTimes int
	}

	resp.TotalReqTimes, resp.TotalPlayTimes, resp.TotalFailTimes, resp.List = mgr.playStat(req.BeginTime, req.EndTime, req.PartnerID)
	c.JSON(http.StatusOK, resp)
	return
}

// 广告播放统计(按时段)
func PlayStatByHour(c *gin.Context) {
	obj := NewPlayStatByHour()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "video.PlayStatByHour", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 广告来源统计
func SourceList(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "video.SourceList", err)
		return
	}

	list := mgr.sourceStat(req.BeginTime, req.EndTime, req.PartnerID)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 广告分布
func RegionList(c *gin.Context) {
	obj := NewRegionList()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "video.RegionList", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}

// 广告指标
func IndexList(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "video.IndexList", err)
		return
	}

	list := mgr.indexStat(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 广告用户统计(按天)
func UserStat(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "video.UserStat", err)
		return
	}

	list := mgr.userStat(req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		List interface{}
	}{
		List: list,
	})
	return
}

// 广告用户统计(按时段)
func UserStatByHour(c *gin.Context) {
	obj := NewUserStatByHour()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "video.UserStatByHour", err)
		return
	}

	obj.DoAction()
	c.JSON(http.StatusOK, obj.Out)
	return
}
