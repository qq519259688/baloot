package video

const dateFormat = "2006-01-02"

type req_base struct {
	BeginTime string
	EndTime   string
	PartnerID int
}

// 播放统计
type playStatInfo struct {
	DateFlag  string // 日期
	ReqTimes  int    // 请求次数
	PlayTimes int    // 播放次数
	FailTimes int    // 失败次数
}

// 播放来源
type sourceInfo struct {
	DateFlag  string // 日期
	VideoName string // 视频名称
	PlayTimes int    // 播放次数
}

// 播放指标
type indexInfo struct {
	DateFlag      string // 日期
	ReqTimes      int    // 请求次数
	PlayTimes     int    // 播放次数
	UserCount     int    // 用户数
	PlayUserCount int    // 播放用户数
	GoldTimes     int    // 金币次数
	GoldGiftTimes int    // 领取金币次数
}

// 播放用户统计
type userStatInfo struct {
	DateFlag  string // 日期
	ReqUsers  int    // 请求用户数
	PlayUsers int    // 播放用户数
	FailUsers int    // 失败用户数
}
