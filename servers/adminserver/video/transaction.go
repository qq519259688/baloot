package video

import (
	"runtime/debug"

	"bet24.com/servers/adminserver/dao"

	"bet24.com/database"
	"bet24.com/log"
)

// 广告播放统计(按天)
func playStat(beginTime, endTime string, partnerId int) []*playStatInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var list []*playStatInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserVideo_GetPlayStat")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, partnerId)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out playStatInfo

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.ReqTimes = int((*ret[1].(*interface{})).(int64))
		out.PlayTimes = int((*ret[2].(*interface{})).(int64))
		out.FailTimes = int((*ret[3].(*interface{})).(int64))

		list = append(list, &out)
	}

	return list
}

// 广告播放统计(按时段)
type (
	playStatByHour_in struct {
		BeginTime string
		EndTime   string
		PartnerID int
	}

	playStatByHourModel struct {
		DateFlag string

		Hour_0000 int // 00:00
		Hour_0030 int // 00:30

		Hour_0100 int // 01:00
		Hour_0130 int // 01:30

		Hour_0200 int // 02:00
		Hour_0230 int // 02:30

		Hour_0300 int // 03:00
		Hour_0330 int // 03:30

		Hour_0400 int // 04:00
		Hour_0430 int // 04:30

		Hour_0500 int // 05:00
		Hour_0530 int // 05:30

		Hour_0600 int // 06:00
		Hour_0630 int // 06:30

		Hour_0700 int // 07:00
		Hour_0730 int // 07:30

		Hour_0800 int // 08:00
		Hour_0830 int // 08:30

		Hour_0900 int // 09:00
		Hour_0930 int // 09:30

		Hour_1000 int // 10:00
		Hour_1030 int // 10:30

		Hour_1100 int // 11:00
		Hour_1130 int // 11:30

		Hour_1200 int // 12:00
		Hour_1230 int // 12:30

		Hour_1300 int // 13:00
		Hour_1330 int // 13:30

		Hour_1400 int // 14:00
		Hour_1430 int // 14:30

		Hour_1500 int // 15:00
		Hour_1530 int // 15:30

		Hour_1600 int // 16:00
		Hour_1630 int // 16:30

		Hour_1700 int // 17:00
		Hour_1730 int // 17:30

		Hour_1800 int // 18:00
		Hour_1830 int // 18:30

		Hour_1900 int // 19:00
		Hour_1930 int // 19:30

		Hour_2000 int // 20:00
		Hour_2030 int // 20:30

		Hour_2100 int // 21:00
		Hour_2130 int // 21:30

		Hour_2200 int // 22:00
		Hour_2230 int // 22:30

		Hour_2300 int // 23:00
		Hour_2330 int // 23:30
	}

	playStatByHour_out struct {
		RecordCount int
		List        []playStatByHourModel
	}

	playStatByHour struct {
		database.Trans_base
		In  playStatByHour_in
		Out playStatByHour_out
	}
)

func NewPlayStatByHour() *playStatByHour {
	return &playStatByHour{}
}

func (this *playStatByHour) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserVideo_GetPlayStatByHour")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}
	this.Out.List = make([]playStatByHourModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.Hour_0000 = int((*ret[1].(*interface{})).(int64))
		out.Hour_0030 = int((*ret[2].(*interface{})).(int64))
		out.Hour_0100 = int((*ret[3].(*interface{})).(int64))
		out.Hour_0130 = int((*ret[4].(*interface{})).(int64))
		out.Hour_0200 = int((*ret[5].(*interface{})).(int64))
		out.Hour_0230 = int((*ret[6].(*interface{})).(int64))
		out.Hour_0300 = int((*ret[7].(*interface{})).(int64))
		out.Hour_0330 = int((*ret[8].(*interface{})).(int64))
		out.Hour_0400 = int((*ret[9].(*interface{})).(int64))
		out.Hour_0430 = int((*ret[10].(*interface{})).(int64))
		out.Hour_0500 = int((*ret[11].(*interface{})).(int64))
		out.Hour_0530 = int((*ret[12].(*interface{})).(int64))
		out.Hour_0600 = int((*ret[13].(*interface{})).(int64))
		out.Hour_0630 = int((*ret[14].(*interface{})).(int64))
		out.Hour_0700 = int((*ret[15].(*interface{})).(int64))
		out.Hour_0730 = int((*ret[16].(*interface{})).(int64))
		out.Hour_0800 = int((*ret[17].(*interface{})).(int64))
		out.Hour_0830 = int((*ret[18].(*interface{})).(int64))
		out.Hour_0900 = int((*ret[19].(*interface{})).(int64))
		out.Hour_0930 = int((*ret[20].(*interface{})).(int64))
		out.Hour_1000 = int((*ret[21].(*interface{})).(int64))
		out.Hour_1030 = int((*ret[22].(*interface{})).(int64))
		out.Hour_1100 = int((*ret[23].(*interface{})).(int64))
		out.Hour_1130 = int((*ret[24].(*interface{})).(int64))
		out.Hour_1200 = int((*ret[25].(*interface{})).(int64))
		out.Hour_1230 = int((*ret[26].(*interface{})).(int64))
		out.Hour_1300 = int((*ret[27].(*interface{})).(int64))
		out.Hour_1330 = int((*ret[28].(*interface{})).(int64))
		out.Hour_1400 = int((*ret[29].(*interface{})).(int64))
		out.Hour_1430 = int((*ret[30].(*interface{})).(int64))
		out.Hour_1500 = int((*ret[31].(*interface{})).(int64))
		out.Hour_1530 = int((*ret[32].(*interface{})).(int64))
		out.Hour_1600 = int((*ret[33].(*interface{})).(int64))
		out.Hour_1630 = int((*ret[34].(*interface{})).(int64))
		out.Hour_1700 = int((*ret[35].(*interface{})).(int64))
		out.Hour_1730 = int((*ret[36].(*interface{})).(int64))
		out.Hour_1800 = int((*ret[37].(*interface{})).(int64))
		out.Hour_1830 = int((*ret[38].(*interface{})).(int64))
		out.Hour_1900 = int((*ret[39].(*interface{})).(int64))
		out.Hour_1930 = int((*ret[40].(*interface{})).(int64))
		out.Hour_2000 = int((*ret[41].(*interface{})).(int64))
		out.Hour_2030 = int((*ret[42].(*interface{})).(int64))
		out.Hour_2100 = int((*ret[43].(*interface{})).(int64))
		out.Hour_2130 = int((*ret[44].(*interface{})).(int64))
		out.Hour_2200 = int((*ret[45].(*interface{})).(int64))
		out.Hour_2230 = int((*ret[46].(*interface{})).(int64))
		out.Hour_2300 = int((*ret[47].(*interface{})).(int64))
		out.Hour_2330 = int((*ret[48].(*interface{})).(int64))
	}
}

// 广告来源统计
func source(beginTime, endTime string, partnerId int) []*sourceInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*sourceInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserVideo_GetSourceList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, partnerId)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out sourceInfo

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.VideoName = (*ret[1].(*interface{})).(string)
		out.PlayTimes = int((*ret[2].(*interface{})).(int64))

		list = append(list, &out)
	}

	return list
}

//广告分布
type (
	regionList_in struct {
		BeginTime string
		EndTime   string
	}

	regionListModel struct {
		VideoName  string
		ModuleName string
		ReqTimes   int
		PlayTimes  int
	}

	regionList_out struct {
		RecordCount int
		List        []regionListModel
	}

	regionList struct {
		database.Trans_base
		In  regionList_in
		Out regionList_out
	}
)

func NewRegionList() *regionList {
	return &regionList{}
}

func (this *regionList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserVideo_GetRegionList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	this.Out.List = make([]regionListModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.VideoName = (*ret[0].(*interface{})).(string)
		out.ModuleName = (*ret[1].(*interface{})).(string)
		out.ReqTimes = int((*ret[2].(*interface{})).(int64))
		out.PlayTimes = int((*ret[3].(*interface{})).(int64))
	}
}

// 广告指标
func index(beginTime, endTime string) []*indexInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*indexInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserVideo_GetIndexList")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out indexInfo

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.ReqTimes = int((*ret[1].(*interface{})).(int64))
		out.PlayTimes = int((*ret[2].(*interface{})).(int64))
		out.UserCount = int((*ret[3].(*interface{})).(int64))
		out.GoldTimes = int((*ret[4].(*interface{})).(int64))
		out.GoldGiftTimes = int((*ret[5].(*interface{})).(int64))
		out.PlayUserCount = int((*ret[6].(*interface{})).(int64))

		list = append(list, &out)
	}

	return list
}

// 广告用户统计(按天)
func userStat(beginTime, endTime string) []*userStatInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*userStatInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserVideo_GetUserStat")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out userStatInfo
		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.ReqUsers = int((*ret[1].(*interface{})).(int64))
		out.PlayUsers = int((*ret[2].(*interface{})).(int64))
		out.FailUsers = int((*ret[3].(*interface{})).(int64))
		list = append(list, &out)
	}
	return list
}

//广告用户统计(按时段)
type (
	userStatByHour_in struct {
		BeginTime string
		EndTime   string
	}

	userStatByHourModel struct {
		DateFlag  string
		Hour_0000 int // 00:00
		Hour_0030 int // 00:30

		Hour_0100 int // 01:00
		Hour_0130 int // 01:30

		Hour_0200 int // 02:00
		Hour_0230 int // 02:30

		Hour_0300 int // 03:00
		Hour_0330 int // 03:30

		Hour_0400 int // 04:00
		Hour_0430 int // 04:30

		Hour_0500 int // 05:00
		Hour_0530 int // 05:30

		Hour_0600 int // 06:00
		Hour_0630 int // 06:30

		Hour_0700 int // 07:00
		Hour_0730 int // 07:30

		Hour_0800 int // 08:00
		Hour_0830 int // 08:30

		Hour_0900 int // 09:00
		Hour_0930 int // 09:30

		Hour_1000 int // 10:00
		Hour_1030 int // 10:30

		Hour_1100 int // 11:00
		Hour_1130 int // 11:30

		Hour_1200 int // 12:00
		Hour_1230 int // 12:30

		Hour_1300 int // 13:00
		Hour_1330 int // 13:30

		Hour_1400 int // 14:00
		Hour_1430 int // 14:30

		Hour_1500 int // 15:00
		Hour_1530 int // 15:30

		Hour_1600 int // 16:00
		Hour_1630 int // 16:30

		Hour_1700 int // 17:00
		Hour_1730 int // 17:30

		Hour_1800 int // 18:00
		Hour_1830 int // 18:30

		Hour_1900 int // 19:00
		Hour_1930 int // 19:30

		Hour_2000 int // 20:00
		Hour_2030 int // 20:30

		Hour_2100 int // 21:00
		Hour_2130 int // 21:30

		Hour_2200 int // 22:00
		Hour_2230 int // 22:30

		Hour_2300 int // 23:00
		Hour_2330 int // 23:30
	}

	userStatByHour_out struct {
		RecordCount int
		List        []userStatByHourModel
	}

	userStatByHour struct {
		database.Trans_base
		In  userStatByHour_in
		Out userStatByHour_out
	}
)

func NewUserStatByHour() *userStatByHour {
	return &userStatByHour{}
}

func (this *userStatByHour) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserVideo_GetUserStatByHour")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, this.In.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, this.In.EndTime)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}
	this.Out.List = make([]userStatByHourModel, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.DateFlag = (*ret[0].(*interface{})).(string)
		out.Hour_0000 = int((*ret[1].(*interface{})).(int64))
		out.Hour_0030 = int((*ret[2].(*interface{})).(int64))
		out.Hour_0100 = int((*ret[3].(*interface{})).(int64))
		out.Hour_0130 = int((*ret[4].(*interface{})).(int64))
		out.Hour_0200 = int((*ret[5].(*interface{})).(int64))
		out.Hour_0230 = int((*ret[6].(*interface{})).(int64))
		out.Hour_0300 = int((*ret[7].(*interface{})).(int64))
		out.Hour_0330 = int((*ret[8].(*interface{})).(int64))
		out.Hour_0400 = int((*ret[9].(*interface{})).(int64))
		out.Hour_0430 = int((*ret[10].(*interface{})).(int64))
		out.Hour_0500 = int((*ret[11].(*interface{})).(int64))
		out.Hour_0530 = int((*ret[12].(*interface{})).(int64))
		out.Hour_0600 = int((*ret[13].(*interface{})).(int64))
		out.Hour_0630 = int((*ret[14].(*interface{})).(int64))
		out.Hour_0700 = int((*ret[15].(*interface{})).(int64))
		out.Hour_0730 = int((*ret[16].(*interface{})).(int64))
		out.Hour_0800 = int((*ret[17].(*interface{})).(int64))
		out.Hour_0830 = int((*ret[18].(*interface{})).(int64))
		out.Hour_0900 = int((*ret[19].(*interface{})).(int64))
		out.Hour_0930 = int((*ret[20].(*interface{})).(int64))
		out.Hour_1000 = int((*ret[21].(*interface{})).(int64))
		out.Hour_1030 = int((*ret[22].(*interface{})).(int64))
		out.Hour_1100 = int((*ret[23].(*interface{})).(int64))
		out.Hour_1130 = int((*ret[24].(*interface{})).(int64))
		out.Hour_1200 = int((*ret[25].(*interface{})).(int64))
		out.Hour_1230 = int((*ret[26].(*interface{})).(int64))
		out.Hour_1300 = int((*ret[27].(*interface{})).(int64))
		out.Hour_1330 = int((*ret[28].(*interface{})).(int64))
		out.Hour_1400 = int((*ret[29].(*interface{})).(int64))
		out.Hour_1430 = int((*ret[30].(*interface{})).(int64))
		out.Hour_1500 = int((*ret[31].(*interface{})).(int64))
		out.Hour_1530 = int((*ret[32].(*interface{})).(int64))
		out.Hour_1600 = int((*ret[33].(*interface{})).(int64))
		out.Hour_1630 = int((*ret[34].(*interface{})).(int64))
		out.Hour_1700 = int((*ret[35].(*interface{})).(int64))
		out.Hour_1730 = int((*ret[36].(*interface{})).(int64))
		out.Hour_1800 = int((*ret[37].(*interface{})).(int64))
		out.Hour_1830 = int((*ret[38].(*interface{})).(int64))
		out.Hour_1900 = int((*ret[39].(*interface{})).(int64))
		out.Hour_1930 = int((*ret[40].(*interface{})).(int64))
		out.Hour_2000 = int((*ret[41].(*interface{})).(int64))
		out.Hour_2030 = int((*ret[42].(*interface{})).(int64))
		out.Hour_2100 = int((*ret[43].(*interface{})).(int64))
		out.Hour_2130 = int((*ret[44].(*interface{})).(int64))
		out.Hour_2200 = int((*ret[45].(*interface{})).(int64))
		out.Hour_2230 = int((*ret[46].(*interface{})).(int64))
		out.Hour_2300 = int((*ret[47].(*interface{})).(int64))
		out.Hour_2330 = int((*ret[48].(*interface{})).(int64))
	}
}
