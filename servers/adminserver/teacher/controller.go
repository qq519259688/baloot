package teacher

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

func GetTopProfit(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "teacher.GetTopProfit", err)
		return
	}

	ret := mgr.getTopProfit(&req)
	c.JSON(http.StatusOK, ret)
	return
}

func GetProfitList(c *gin.Context) {
	var req req_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "teacher.GetProfitList", err)
		return
	}

	ret := mgr.getProfitList(&req)
	c.JSON(http.StatusOK, ret)
	return
}
