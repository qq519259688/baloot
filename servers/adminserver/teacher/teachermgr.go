package teacher

var mgr *teacherManager

type teacherManager struct {
}

func Run() {
	mgr = new(teacherManager)
}

func (this *teacherManager) getTopProfit(req *req_base) topProfitList {
	return getTopProfit(req)
}

func (this *teacherManager) getProfitList(req *req_base) profitList {
	return getProfitList(req)
}
