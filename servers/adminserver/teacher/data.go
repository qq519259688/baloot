package teacher

type req_base struct {
	TeacherID int    // 师父ID
	StudentID int    // 徒弟ID
	BeginTime string // 开始时间
	EndTime   string // 截止时间
	PageIndex int    // 页索引
	PageSize  int    // 页大小

}

type topProfitInfo struct {
	RowNumber   int
	TeacherID   int
	TeacherName string
	Profit      int
	Price       int
}

type profitInfo struct {
	RowNumber   int
	TeacherID   int
	TeacherName string
	StudentID   int
	StudentName string
	ProfitType  int
	Profit      int
	Price       int
	Remark      string
	Crdate      string
}

type topProfitList struct {
	RecordCount int
	List        []*topProfitInfo
}

type profitList struct {
	RecordCount int
	List        []*profitInfo
}
