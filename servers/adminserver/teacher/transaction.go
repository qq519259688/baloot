package teacher

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
)

func getTopProfit(req *req_base) topProfitList {
	var out topProfitList
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserTeacher_TopProfit")
	statement.AddParamter("@TeacherID", database.AdParamInput, database.AdInteger, 4, req.TeacherID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, out.RecordCount)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return out
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info topProfitInfo
			info.RowNumber = int((*ret[0].(*interface{})).(int64))
			info.TeacherID = int((*ret[1].(*interface{})).(int64))
			info.TeacherName = (*ret[2].(*interface{})).(string)
			info.Profit = int((*ret[3].(*interface{})).(int64))
			info.Price = int((*ret[4].(*interface{})).(int64))
			out.List = append(out.List, &info)
		}
	}
	out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return out
}

func getProfitList(req *req_base) profitList {
	var out profitList
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserTeacher_GetProfitList")
	statement.AddParamter("@TeacherID", database.AdParamInput, database.AdInteger, 4, req.TeacherID)
	statement.AddParamter("@StudentID", database.AdParamInput, database.AdInteger, 4, req.StudentID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, out.RecordCount)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return out
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info profitInfo
			info.RowNumber = int((*ret[0].(*interface{})).(int64))
			info.TeacherID = int((*ret[1].(*interface{})).(int64))
			info.TeacherName = (*ret[2].(*interface{})).(string)
			info.StudentID = int((*ret[3].(*interface{})).(int64))
			info.StudentName = (*ret[4].(*interface{})).(string)
			info.ProfitType = int((*ret[5].(*interface{})).(int64))
			info.Profit = int((*ret[6].(*interface{})).(int64))
			info.Price = int((*ret[7].(*interface{})).(int64))
			info.Remark = (*ret[8].(*interface{})).(string)
			info.Crdate = (*ret[9].(*interface{})).(string)
			out.List = append(out.List, &info)
		}
	}
	out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return out
}
