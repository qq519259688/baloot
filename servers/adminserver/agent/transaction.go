package agent

import (
	"runtime/debug"
	"strings"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
)

// 代理配置
func getConfig() *configInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AgentConfig_GetInfo")
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return nil
	}

	ret := retRows[0]
	return &configInfo{
		IsOpen:       int((*ret[0].(*interface{})).(int64)),
		BindSend:     int((*ret[1].(*interface{})).(int64)),
		BetOneDirect: int((*ret[2].(*interface{})).(int64)),
		BetTwoDirect: int((*ret[3].(*interface{})).(int64)),
		TaxOneDirect: int((*ret[4].(*interface{})).(int64)),
		TaxTwoDirect: int((*ret[5].(*interface{})).(int64)),
		PayOneDirect: int((*ret[6].(*interface{})).(int64)),
		PayTwoDirect: int((*ret[7].(*interface{})).(int64)),
		AuditEnabled: int((*ret[8].(*interface{})).(int64)),
	}
}

// 修改代理配置
func updateConfig(req *configUp_in) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var out agentList_out
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AgentConfig_Update")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, req.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, req.OpUserName)
	statement.AddParamter("@IsOpen", database.AdParamInput, database.AdInteger, 4, req.IsOpen)
	statement.AddParamter("@BindSend", database.AdParamInput, database.AdInteger, 4, req.BindSend)
	statement.AddParamter("@OneDirect", database.AdParamInput, database.AdInteger, 4, req.BetOneDirect)
	statement.AddParamter("@TwoDirect", database.AdParamInput, database.AdInteger, 4, req.BetTwoDirect)
	statement.AddParamter("@TaxOneDirect", database.AdParamInput, database.AdInteger, 4, req.TaxOneDirect)
	statement.AddParamter("@TaxTwoDirect", database.AdParamInput, database.AdInteger, 4, req.TaxTwoDirect)
	statement.AddParamter("@PayOneDirect", database.AdParamInput, database.AdInteger, 4, req.PayOneDirect)
	statement.AddParamter("@PayTwoDirect", database.AdParamInput, database.AdInteger, 4, req.PayTwoDirect)
	statement.AddParamter("@AuditEnabled", database.AdParamInput, database.AdInteger, 4, req.AuditEnabled)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, req.IpAddress)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, out.RecordCount)
	sqlString := statement.GenSql()
	dao.CenterDB.ExecSql(sqlString)
}

// 代理列表
func getAgentList(req *request_base) *agentList_out {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var out agentList_out
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserAgent_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, req.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, out.RecordCount)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info agentInfo
			info.UserID = int((*ret[0].(*interface{})).(int64))
			info.NickName = (*ret[1].(*interface{})).(string)
			info.Commission = int((*ret[2].(*interface{})).(int64))
			info.Profit = int((*ret[3].(*interface{})).(int64))
			info.Members = int((*ret[4].(*interface{})).(int64))
			info.Enabled = int((*ret[5].(*interface{})).(int64))
			info.Crdate = (*ret[6].(*interface{})).(string)
			out.List = append(out.List, &info)
		}
	}

	out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return &out
}

// 申请列表
func getAgentApplyList(req *request_base) *applyList_out {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var out applyList_out
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserAgent_GetApplyList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, req.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, out.RecordCount)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info applyInfo
			info.ApplyID = int((*ret[0].(*interface{})).(int64))
			info.UserID = int((*ret[1].(*interface{})).(int64))
			info.NickName = (*ret[2].(*interface{})).(string)
			info.HigherUserID = int((*ret[3].(*interface{})).(int64))
			info.HigherNickName = (*ret[4].(*interface{})).(string)
			info.ApplyStatus = int((*ret[5].(*interface{})).(int64))
			memo := (*ret[6].(*interface{})).(string)
			mList := strings.Split(memo, "，操作员：")
			info.Memo = mList[0]
			if len(mList) == 2 {
				info.OpUser = mList[1]
			}
			info.DealTime = (*ret[7].(*interface{})).(string)
			info.Crdate = (*ret[8].(*interface{})).(string)

			out.List = append(out.List, &info)
		}
	}

	out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return &out
}

// 处理申请
func dealApply(req *dealApply_req) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserAgent_DealApply")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, req.OpUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, req.OpUserName)
	statement.AddParamter("@ApplyID", database.AdParamInput, database.AdInteger, 4, req.ApplyID)
	statement.AddParamter("@ApplyStatus", database.AdParamInput, database.AdInteger, 4, req.ApplyStatus)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, req.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlString := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return retCode
	}
	retCode = int((*retRows[0][0].(*interface{})).(int64))
	return retCode
}

// 会员列表
func getMemberList(req *request_base) *memberInfo_out {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	out := new(memberInfo_out)

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserAgent_GetMemberList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, req.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, out.RecordCount)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return out
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info memberInfo

			info.UserID = int((*ret[0].(*interface{})).(int64))
			info.NickName = (*ret[1].(*interface{})).(string)
			info.Commission = int((*ret[2].(*interface{})).(int64))
			info.NewCommission = int((*ret[3].(*interface{})).(int64))
			info.BindTime = (*ret[4].(*interface{})).(string)
			info.LoginTime = (*ret[5].(*interface{})).(string)
			info.HigherUserID = int((*ret[6].(*interface{})).(int64))
			info.HigherNickName = (*ret[7].(*interface{})).(string)

			out.List = append(out.List, &info)
		}
	}

	out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	return out
}

// 佣金日志
func getCommissionLog(req *request_base) *commissionInfo_out {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	out := new(commissionInfo_out)

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserAgent_GetCommissionLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, req.UserID)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, req.BeginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, req.EndTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, req.PageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, req.PageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, out.RecordCount)
	statement.AddParamter("@TotalCommission", database.AdParamOutput, database.AdInteger, 8, out.TotalCommission)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return out
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info commissionInfo

			info.Rid = int((*ret[0].(*interface{})).(int64))
			info.UserID = int((*ret[1].(*interface{})).(int64))
			info.NickName = (*ret[2].(*interface{})).(string)
			info.FromUserID = int((*ret[3].(*interface{})).(int64))
			info.FromNickName = (*ret[4].(*interface{})).(string)
			info.Tax = int((*ret[5].(*interface{})).(int64))
			info.WantCommission = int((*ret[6].(*interface{})).(int64))
			info.StillCommission = int((*ret[7].(*interface{})).(int64))
			info.Direct = int((*ret[8].(*interface{})).(int64))
			info.Memo = (*ret[9].(*interface{})).(string)
			info.Crdate = (*ret[10].(*interface{})).(string)
			info.ChineseName = (*ret[11].(*interface{})).(string)

			out.List = append(out.List, &info)
		}
	}

	out.RecordCount = int((*retRows[rowLen-1][0].(*interface{})).(int64))
	out.TotalCommission = int((*retRows[rowLen-1][1].(*interface{})).(int64))
	return out
}

// 佣金排行榜
func getCommissionRankList(userId int, beginTime, endTime string) []*commissionRankInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_CommissionStat_RankList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var out []*commissionRankInfo
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info commissionRankInfo

		info.Rid = int((*ret[0].(*interface{})).(int64))
		info.UserID = int((*ret[1].(*interface{})).(int64))
		info.NickName = (*ret[2].(*interface{})).(string)
		info.BetAmount = int((*ret[3].(*interface{})).(int64))
		info.Commission = int((*ret[4].(*interface{})).(int64))
		info.Grade = int((*ret[5].(*interface{})).(int64))

		out = append(out, &info)
	}
	return out
}

// 创建代理
func createAgent(opUserID int, opUserName string, userId, grade int, ipAddress string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Agent_Create")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, opUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, opUserName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Grade", database.AdParamInput, database.AdInteger, 4, grade)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	retRows := dao.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return retCode
	}

	retCode = int((*retRows[0][0].(*interface{})).(int64))
	return retCode
}

// 设置代理状态
func setStatus(opUserID int, opUserName string, userId, enabled int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserAgent_SetStatus")
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, opUserID)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, opUserName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Enabled", database.AdParamInput, database.AdInteger, 4, enabled)
	sqlString := statement.GenSql()
	dao.CenterDB.ExecSql(sqlString)
}
