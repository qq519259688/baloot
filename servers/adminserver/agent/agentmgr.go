package agent

var mgr *agentManager

type agentManager struct {
}

func Run() {
	mgr = new(agentManager)
}

// 配置信息
func (this *agentManager) getConfig() *configInfo {
	return getConfig()
}

// 修改配置
func (this *agentManager) updateConfig(req *configUp_in) {
	updateConfig(req)
}

// 代理列表
func (this *agentManager) getAgentList(req *request_base, sortType int) *agentList_out {
	return getAgentList(req)
}

// 申请列表
func (this *agentManager) getApplyList(req *request_base) *applyList_out {
	return getAgentApplyList(req)
}

// 处理申请
func (this *agentManager) dealApply(req *dealApply_req) int {
	return dealApply(req)
}

// 会员列表
func (this *agentManager) getMemberList(req *request_base) *memberInfo_out {
	return getMemberList(req)
}

// 佣金日志
func (this *agentManager) getCommissionLog(req *request_base) *commissionInfo_out {
	return getCommissionLog(req)
}

// 佣金排行榜
func (this *agentManager) getCommissionRankList(userId int, beginTime, endTime string) []*commissionRankInfo {
	return getCommissionRankList(userId, beginTime, endTime)
}

// 创建代理
func (this *agentManager) createAgent(opUserID int, opUserName string, userId, grade int, ipAddress string) int {
	return createAgent(opUserID, opUserName, userId, grade, ipAddress)
}

// 设置代理状态
func (this *agentManager) setStatus(opUserID int, opUserName string, userId, enabled int) {
	setStatus(opUserID, opUserName, userId, enabled)
}
