package agent

// 配置
type configInfo struct {
	IsOpen       int // 是否开启(1=开启  其他关闭)
	BindSend     int // 绑码赠送
	BetOneDirect int // 流水1级代理返佣(以万为基数)
	BetTwoDirect int // 流水2级代理返佣(以万为基数)
	TaxOneDirect int // 服务费1级代理返佣(以万为基数)
	TaxTwoDirect int // 服务费2级代理返佣(以万为基数)
	PayOneDirect int // 充值1级代理返佣(以万为基数)
	PayTwoDirect int // 充值2级代理返佣(以万为基数)
	AuditEnabled int // 审核是否启用
}

type configUp_in struct {
	OpUserID   int    // 操作员ID
	OpUserName string // 操作员名称
	IpAddress  string // IP地址
	configInfo
}

// 基础请求
type request_base struct {
	UserID    int    // 用户ID
	BeginTime string // 开始时间
	EndTime   string // 截止时间
	PageIndex int    // 页索引
	PageSize  int    // 页大小
}

// 代理信息
type agentInfo struct {
	UserID     int    // 用户ID
	NickName   string // 昵称
	Commission int    // 佣金
	Profit     int    // 收益
	Members    int    // 会员数
	Enabled    int    // 是否启用
	Crdate     string // 注册时间
}

// 代理列表
type agentList_out struct {
	RecordCount int // 记录数
	List        []*agentInfo
}

// 会员信息
type (
	memberInfo struct {
		HigherUserID   int    // 代理ID
		HigherNickName string // 代理名称
		UserID         int    // 用户ID
		NickName       string // 昵称
		Commission     int    // 佣金
		NewCommission  int    // 新增佣金
		BindTime       string // 绑码时间
		LoginTime      string // 登录时间
	}

	memberInfo_out struct {
		RecordCount int // 记录数
		List        []*memberInfo
	}
)

// 佣金信息
type (
	commissionInfo struct {
		Rid             int    // 标识
		UserID          int    // 用户ID
		NickName        string // 昵称
		FromUserID      int    // 源用户ID
		FromNickName    string // 源用户昵称
		Tax             int    // 台费
		WantCommission  int    // 操作佣金
		StillCommission int    // 剩余佣金
		Direct          int    // 属级(1=直属  2=非直属)
		Memo            string // 备注
		Crdate          string // 时间
		ChineseName     string // 游戏名称
	}

	commissionInfo_out struct {
		RecordCount     int // 记录数
		TotalCommission int // 总佣金
		List            []*commissionInfo
	}
)

// 佣金排行榜
type commissionRankInfo struct {
	Rid        int    // 序号
	UserID     int    // 用户ID
	NickName   string // 昵称
	Grade      int    // 代理级别
	BetAmount  int    // 下注金额
	Commission int    // 佣金
}

// 申请列表
type applyList_out struct {
	RecordCount int // 记录数
	List        []*applyInfo
}

// 申请列表
type applyInfo struct {
	ApplyID        int    // 申请ID
	UserID         int    // 用户ID
	NickName       string // 昵称
	HigherUserID   int    // 上级用户ID
	HigherNickName string // 上级用户昵称
	ApplyStatus    int    // 申请状态(0=待审核 1=已通过 2=拒绝)
	Memo           string // 备注(联系信息，如：电话)
	DealTime       string // 处理时间
	Crdate         string // 申请时间
	OpUser         string // 操作员
}

// 处理申请
type dealApply_req struct {
	OpUserID    int    // 操作员ID
	OpUserName  string // 操作员名称
	IpAddress   string // IP地址
	ApplyID     int    // 申请ID
	ApplyStatus int    // 状态(0=审核 1=已通过 2=拒绝)
}
