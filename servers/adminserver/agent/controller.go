package agent

import (
	"net/http"

	"bet24.com/log"
	"github.com/gin-gonic/gin"
)

// 配置信息
func GetConfig(c *gin.Context) {
	resp := mgr.getConfig()
	c.JSON(http.StatusOK, resp)
}

// 修改配置
func UpdateConfig(c *gin.Context) {
	var req configUp_in
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "agent.UpdateConfig", err)
		return
	}

	mgr.updateConfig(&req)
	c.JSON(http.StatusOK, "")
	return
}

// 代理列表
func GetList(c *gin.Context) {
	var req struct {
		Info     *request_base
		SortType int
	}
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "agent.GetList", err)
		return
	}

	resp := mgr.getAgentList(req.Info, req.SortType)
	c.JSON(http.StatusOK, resp)
	return
}

// 申请列表
func ApplyList(c *gin.Context) {
	var req *request_base
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "agent.ApplyList", err)
		return
	}
	resp := mgr.getApplyList(req)
	c.JSON(http.StatusOK, resp)
	return
}

// 处理申请
func DealApply(c *gin.Context) {
	var req *dealApply_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "agent.DealApply", err)
		return
	}
	retCode := mgr.dealApply(req)
	c.JSON(http.StatusOK, struct {
		RetCode int
	}{
		RetCode: retCode,
	})
	return
}

// 会员列表
func GetMemberList(c *gin.Context) {
	req := new(request_base)
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "agent.GetMemberList", err)
		return
	}

	resp := mgr.getMemberList(req)
	c.JSON(http.StatusOK, resp)
	return
}

// 佣金日志
func GetCommissionLog(c *gin.Context) {
	req := new(request_base)
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "agent.GetCommissionLog", err)
		return
	}

	resp := mgr.getCommissionLog(req)
	c.JSON(http.StatusOK, resp)
	return
}

// 佣金排行榜
func GetCommissionRankList(c *gin.Context) {
	req := new(request_base)
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "agent.GetCommissionRankList", err)
		return
	}

	list := mgr.getCommissionRankList(req.UserID, req.BeginTime, req.EndTime)
	c.JSON(http.StatusOK, struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: len(list),
		List:        list,
	})
	return
}

// 创建代理
func Create(c *gin.Context) {
	var req struct {
		OpUserID   int
		OpUserName string
		UserId     int
		Grade      int
		IpAddress  string
	}
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "agent.Create", err)
		return
	}

	retCode := mgr.createAgent(req.OpUserID, req.OpUserName, req.UserId, req.Grade, req.IpAddress)
	c.JSON(http.StatusOK, struct {
		RetCode int
	}{
		RetCode: retCode,
	})
	return
}

// 设置代理状态
func SetStatus(c *gin.Context) {
	var req struct {
		OpUserID   int
		OpUserName string
		UserId     int
		Enabled    int
	}
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "agent.SetStatus", err)
		return
	}

	mgr.setStatus(req.OpUserID, req.OpUserName, req.UserId, req.Enabled)
	c.JSON(http.StatusOK, nil)
	return
}
