package warning

import (
	"fmt"
	"time"

	"bet24.com/public"
	"bet24.com/servers/adminserver/config"
)

var w *warning

type (
	warning struct {
		moneyWarning    // 金币预警
		lotteryWarning  // 话费券预警
		regWarning      // 注册预警
		transferWarning // 赠送预警
	}

	moneyWarning struct {
		stock      int       // 金币存量
		searchTime time.Time // 金币查询时间
	}

	lotteryWarning struct {
		stock      int       // 话费券存量
		searchTime time.Time // 话费券查询时间
	}

	regWarning struct {
		searchTime time.Time // 注册查询时间
		regTime    time.Time // 注册时间
	}

	transferWarning struct {
		searchTime   time.Time // 赠送查询时间
		transferTime time.Time // 赠送时间
	}

	warningItem struct {
		moneyStock    int // 金币存量
		lotteryStock  int // 话费券存量
		regCount      int // 注册用户数
		transferTimes int // 赠送次数
	}
)

func Run() {
	w = new(warning)
	w.moneyWarning.searchTime = time.Now()
	w.lotteryWarning.searchTime = time.Now()
	w.regWarning.searchTime = time.Now()
	w.regWarning.regTime = time.Now()
	w.transferWarning.searchTime = time.Now()
	w.transferWarning.transferTime = time.Now()

	go w.doCheck()
}

func (this *warning) doCheck() {
	ticker := time.NewTicker(10 * time.Second)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				this.check()
			}
		}
	}(ticker)
}

// 发送短信
func (this *warning) sendSMS(msg string) {
	for _, tel := range config.AdminConfig.Phones {
		if !public.CheckMobile(tel) {
			continue
		}
		go public.HttpSMSSend(tel, msg, config.AdminConfig.SMSAppId, config.AdminConfig.SMSApiKey, config.AdminConfig.SMSPostUrl)
	}
	return
}

func (this *warning) check() {
	info := warningCheck(this.moneyWarning.searchTime, this.lotteryWarning.searchTime,
		this.regWarning.regTime, this.transferWarning.transferTime)
	if info == nil {
		return
	}

	// log.Debug("check from DBInfo ==>%+v moneyWarning.stock=%d lotteryWarning.stock=%d regSearchTime=%v regTime=%v transferSearchTime=%v transferTime=%v",
	// 	info, this.moneyWarning.stock, this.lotteryWarning.stock, this.regWarning.searchTime, this.regWarning.regTime, this.transferWarning.searchTime, this.transferWarning.transferTime)

	// 触发金币预警
	if info.moneyStock-this.moneyWarning.stock >= config.AdminConfig.MoneyValue && this.moneyWarning.stock > 0 {
		// 触发了警报系统！【变量】可能存在数据异常，当前值为【变量】（正常值上限【变量】，下限【变量】）
		msg := fmt.Sprintf(config.AdminConfig.SMSSendMsg, "YXB", info.moneyStock-this.moneyWarning.stock, config.AdminConfig.MoneyValue)

		// 发送短信
		this.sendSMS(msg)
	}
	if this.moneyWarning.searchTime.Before(time.Now()) {
		this.moneyWarning.stock = info.moneyStock
		this.moneyWarning.searchTime = time.Now().Add(time.Duration(config.AdminConfig.MoneyMinutes) * time.Minute)
	}

	// 触发话费券预警
	if info.lotteryStock-this.lotteryWarning.stock >= config.AdminConfig.LotteryValue && this.lotteryWarning.stock > 0 {
		// 触发了警报系统！【变量】可能存在数据异常，当前值为【变量】（正常值上限【变量】，下限【变量】）
		msg := fmt.Sprintf(config.AdminConfig.SMSSendMsg, "HFQ", info.lotteryStock-this.lotteryWarning.stock, config.AdminConfig.LotteryValue)
		// 发送短信
		this.sendSMS(msg)
	}
	if this.lotteryWarning.searchTime.Before(time.Now()) {
		this.lotteryWarning.stock = info.lotteryStock
		this.lotteryWarning.searchTime = time.Now().Add(time.Duration(config.AdminConfig.LotteryMinutes) * time.Minute)
	}

	// 触发注册预警
	if info.regCount >= config.AdminConfig.RegValue {
		// 触发了警报系统！【变量】可能存在数据异常，当前值为【变量】（正常值上限【变量】，下限【变量】）
		msg := fmt.Sprintf(config.AdminConfig.SMSSendMsg, "NewRegister", info.regCount, config.AdminConfig.RegValue)
		// 发送短信
		this.sendSMS(msg)
	}
	if this.regWarning.searchTime.Before(time.Now()) {
		this.regWarning.searchTime = time.Now().Add(time.Duration(config.AdminConfig.RegMinutes) * time.Minute)
		this.regWarning.regTime = time.Now().Add(time.Duration(config.AdminConfig.RegMinutes*-1) * time.Minute)
	} else {
		this.regWarning.regTime = time.Now().Add(time.Duration(config.AdminConfig.RegMinutes) * time.Minute)
	}

	// 触发赠送预警
	if info.transferTimes >= config.AdminConfig.TransferValue {
		// 触发了警报系统！【变量】可能存在数据异常，当前值为【变量】（正常值上限【变量】，下限【变量】）
		msg := fmt.Sprintf(config.AdminConfig.SMSSendMsg, "ZS", info.transferTimes, config.AdminConfig.TransferValue)
		// 发送短信
		this.sendSMS(msg)
	}
	if this.transferWarning.searchTime.Before(time.Now()) {
		this.transferWarning.searchTime = time.Now().Add(time.Duration(config.AdminConfig.TransferMinutes) * time.Minute)
		this.transferWarning.transferTime = time.Now().Add(time.Duration(config.AdminConfig.TransferMinutes*-1) * time.Minute)
	} else {
		this.transferWarning.transferTime = time.Now().Add(time.Duration(config.AdminConfig.TransferMinutes) * time.Minute)
	}

	return
}
