package warning

import (
	"runtime/debug"
	"time"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/adminserver/dao"
	"bet24.com/servers/common"
)

// 预警检查
func warningCheck(moneySearchTime, lotterySearchTime, regTime, transferTime time.Time) *warningItem {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Warning_Search")
	statement.AddParamter("@MoneySearchTime", database.AdParamInput, database.AdVarChar, 32, moneySearchTime.Format(common.Layout))
	statement.AddParamter("@LotterySearchTime", database.AdParamInput, database.AdVarChar, 32, lotterySearchTime.Format(common.Layout))
	statement.AddParamter("@RegTime", database.AdParamInput, database.AdVarChar, 32, regTime.Format(common.Layout))
	statement.AddParamter("@TransferTime", database.AdParamInput, database.AdVarChar, 32, transferTime.Format(common.Layout))
	sqlString := statement.GenSql()
	// log.Debug(sqlString)
	retRows := dao.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	ret := retRows[0]
	var info warningItem
	info.moneyStock = int((*ret[0].(*interface{})).(int64))
	info.lotteryStock = int((*ret[1].(*interface{})).(int64))
	info.regCount = int((*ret[2].(*interface{})).(int64))
	info.transferTimes = int((*ret[3].(*interface{})).(int64))
	return &info
}
