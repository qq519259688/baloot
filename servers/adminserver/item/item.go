package item

import (
	item_inventory "bet24.com/servers/micros/item_inventory/proto"
	"time"
)

var itemmgr *itemManager

type itemManager struct {
	item_list map[int]*item_inventory.Item
}

func getItemManager() *itemManager {
	if itemmgr == nil {
		itemmgr = new(itemManager)
		itemmgr.item_list = make(map[int]*item_inventory.Item)
		itemmgr.refreshData()
	}

	return itemmgr
}

func (this *itemManager) refreshData() {
	this.load()
	time.AfterFunc(1*time.Minute, this.refreshData)
}

func (this *itemManager) load() {
	items := item_inventory.GetItems()
	this.item_list = items

	// 删除筹码道具
	// delete(items, Item_Chip)
	return
}

func (this *itemManager) getSysItems() map[int]*item_inventory.Item {
	return this.item_list
}

func (this *itemManager) addItems(itemId, count int) []item_inventory.ItemPack {
	var items []item_inventory.ItemPack

	if itemId <= 0 || count <= 0 {
		return items
	}

	items = append(items, item_inventory.ItemPack{
		ItemId: itemId,
		Count:  count,
	})

	return items
}
