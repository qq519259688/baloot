package item

import item_inventory "bet24.com/servers/micros/item_inventory/proto"

func GetSysItems() map[int]*item_inventory.Item {
	return getItemManager().getSysItems()
}

func GetItemName(itemId int) string {
	list := getItemManager().getSysItems()
	if item, ok := list[itemId]; ok {
		return item.Name
	}
	return ""
}

func AddItems(itemId, count int) []item_inventory.ItemPack {
	return getItemManager().addItems(itemId, count)
}
