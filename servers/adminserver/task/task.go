package task

import (
	task "bet24.com/servers/micros/task/proto"
	"time"
)

var mgr *taskManager

type taskManager struct {
	task_list []task.Task
}

func getTaskManager() *taskManager {
	if mgr == nil {
		mgr = new(taskManager)
		mgr.refreshData()
	}
	return mgr
}

func (this *taskManager) refreshData() {
	this.load()
	time.AfterFunc(1*time.Minute, this.refreshData)
}

func (this *taskManager) load() {
	list := task.GetSysTaskList()
	if len(list) == 0 {
		return
	}

	for _, v := range list {
		this.task_list = append(this.task_list, *v)
	}
	return
}

func (this *taskManager) getTaskName(taskId int) string {
	for _, v := range this.task_list {
		if v.Id == taskId {
			return v.Desc
		}
	}
	return ""
}
