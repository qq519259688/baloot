package user

import (
	badge "bet24.com/servers/micros/badge/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
)

const (
	UserStatus_NotLogin = iota // 还没发送登陆
	UserStatus_Free            // 已登录
	UserStatus_Sit
	UserStatus_Ready
	UserStatus_Play
	UserStatus_Watch
	UserStatus_Offline
)

type UserInfo_Base struct {
	UserId      int
	Gold        int
	Vip         int
	NickName    string
	FaceUrl     string
	FaceId      int
	Sex         int
	UserWords   string
	Chip        int // 筹码
	Decorations []userservices.UserDecoration
	Charm       int // 魅力值
	Level       int
	VipExpire   int
}

// 发给自己的登录信息
type UserInfo_Login struct {
	UserInfo_Base
	BankAmount       int
	Session          string
	PayAmount        float64
	Code             int
	IsGuest          int
	TeacherId        int
	HigherUserID     int
	Grade            int
	ChipBank         int    // 筹码保险柜
	ChipSend         int    // 筹码赠送
	Experience       int    // 人物经验
	LoginAward       int    // 元宝大厅登录奖励(0=无效  1=可领取  2=已领取)
	IsWhite          int    // 是否白名单(赠送功能是否展示 true=展示  其他隐藏)
	AutoLoginChip    int    // 是否自动登录元宝大厅
	MailVipTip       bool   // 重要邮件提醒
	IsRegister       bool   // 是否新注册用户
	Currency         string // 币种
	CurrencyIsModify int    // 币种是否允许修改
	VipPoint         int
	Badges           []badge.BadgePosition
}

// 广播给其他人的游戏信息
type UserInfo_Table struct {
	UserInfo_Base
	Status  int
	TableId int
	ChairId int
}
