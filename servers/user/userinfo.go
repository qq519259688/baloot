package user

import (
	badge "bet24.com/servers/micros/badge/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
	"fmt"
	"sync"
)

func NewUserInfo(userIndex int32) *UserInfo {
	u := new(UserInfo)
	u.userIndex = userIndex
	u.lock = &sync.RWMutex{}
	u.Clear()
	return u
}

type UserInfo struct {
	lock             *sync.RWMutex
	userIndex        int32   // 网络索引，只读，如果需要改变，需另外创建一个对象
	userId           int     // 用户ID
	gold             int     // 金币
	bankamount       int     // 保险柜数量
	nickName         string  // 昵称
	faceUrl          string  // 头像地址
	faceId           int     // 默认头像ID
	sex              int     //性别
	session          string  // 登录session或token
	payamount        float64 // 充值金额
	viplevel         int     // vip等级
	status           int     // 状态
	tableId          int     // 所在桌子
	chairId          int     // 所在椅子
	userIp           string  // ip地址
	userWords        string  //个性签名
	isRobot          int     //是否机器人
	loginTime        int64
	teacherid        int //师父ID
	higherUserID     int // 上级ID
	grade            int // 代理等级
	chip             int // 筹码
	chipBank         int // 筹码保险柜
	level            int
	experience       int
	autoLoginChip    int
	mailVipTip       bool // 重要邮件(含附件)
	lastTableId      int
	currency         string // 币种
	currencyIsModify int    // 是否允许修改(1=允许修改  其他=禁止修改)
	utmSource        string // 流量渠道
	isGuest          bool
	decorations      []userservices.UserDecoration
	badges           []badge.BadgePosition // 徽章
	// 比赛带入的信息
	score     int // 当前分数
	setCount  int // 当前局数
	baseScore int // 当前底分
	vipPoint  int
	vipExpire int

	partnerId   int
	versionCode int
	charm       int
}

func (u *UserInfo) Clear() {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.userId = 0
	u.gold = 0
	u.bankamount = 0
	u.nickName = ""
	u.faceId = 0
	u.faceUrl = ""
	u.sex = 0
	u.session = ""
	u.payamount = 0
	u.viplevel = 0
	u.status = UserStatus_NotLogin
	u.tableId = -1
	u.chairId = -1
	u.userWords = ""
	u.isRobot = 0
	u.loginTime = 0
	u.teacherid = 0
	u.higherUserID = 0
	u.grade = 0
	u.chip = 0
	u.chipBank = 0
	u.level = 0
	u.experience = 0
	u.autoLoginChip = 0
	u.mailVipTip = false
	u.lastTableId = -1
	u.currency = ""
	u.currencyIsModify = 0
	u.utmSource = ""
	u.isGuest = false
}

func (u *UserInfo) GetLogonTime() int64 {
	return u.loginTime
}

func (u *UserInfo) SetLogonTime(t int64) {
	u.loginTime = t
}

func (u *UserInfo) GetUserIndex() int32 {
	return u.userIndex
}

func (u *UserInfo) GetUserId() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.userId
}

func (u *UserInfo) SetUserId(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.userId = value
}

func (u *UserInfo) IsRobot() bool {
	u.lock.Lock()
	defer u.lock.Unlock()
	if u.isRobot == 1 {
		return true
	}
	return false
}

func (u *UserInfo) SetRobot() {
	u.isRobot = 1
}

func (u *UserInfo) GetUserIp() string {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.userIp
}

func (u *UserInfo) SetUserIp(value string) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.userIp = value
}

func (u *UserInfo) GetUserStatus() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.status
}

func (u *UserInfo) SetUserStatus(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.status = value
}

func (u *UserInfo) IsPlayer() bool {
	u.lock.Lock()
	defer u.lock.Unlock()
	return u.status >= UserStatus_Sit && u.status != UserStatus_Watch
}

func (u *UserInfo) GetUserGold() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.gold
}

func (u *UserInfo) SetUserGold(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.gold = value
}

func (u *UserInfo) GetUserBankamount() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.bankamount
}

func (u *UserInfo) SetUserBankamount(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.bankamount = value
}

func (u *UserInfo) GetUserSession() string {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.session
}

func (u *UserInfo) SetUserSession(value string) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.session = value
}

func (u *UserInfo) GetUserNickName() string {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.nickName
}

func (u *UserInfo) SetUserNickName(value string) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.nickName = value
}

func (u *UserInfo) GetUserFaceUrl() string {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.faceUrl
}

func (u *UserInfo) SetUserFaceUrl(value string) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.faceUrl = value
}

func (u *UserInfo) GetUserFaceId() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.faceId
}

func (u *UserInfo) SetUserFaceId(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.faceId = value
}

func (u *UserInfo) GetUserSex() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.sex
}

func (u *UserInfo) SetUserSex(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.sex = value
}

func (u *UserInfo) GetUserPayAmount() float64 {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.payamount
}

func (u *UserInfo) SetUserPayAmount(value float64) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.payamount = value
}

func (u *UserInfo) GetUserVipLevel() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.viplevel
}

func (u *UserInfo) GetUserVipPoint() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.vipPoint
}

func (u *UserInfo) GetUserVipExpire() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.vipExpire
}

func (u *UserInfo) SetUserVipLevel(level int, point, expire int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.viplevel = level
	u.vipPoint = point
	u.vipExpire = expire
}

func (u *UserInfo) GetUserTableId() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.tableId
}

func (u *UserInfo) SetUserTableId(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.tableId = value
}

func (u *UserInfo) GetUserChairId() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.chairId
}

func (u *UserInfo) SetUserChairId(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.chairId = value
}

func (u *UserInfo) GetUserLastTableId() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.lastTableId
}

func (u *UserInfo) SetUserLastTableId(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.lastTableId = value
}

func (u *UserInfo) GetAutoLoginChip() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.autoLoginChip
}

func (u *UserInfo) SetAutoLoginChip(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.autoLoginChip = value
}

func (u *UserInfo) GetMailVipTip() bool {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.mailVipTip
}

func (u *UserInfo) SetMailVipTip(value bool) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.mailVipTip = value
}

func (u *UserInfo) GetUserInfo_Login(code, isGuest, chipSend, loginAward, isWhite int, isRegister bool) UserInfo_Login {
	u.lock.RLock()
	defer u.lock.RUnlock()
	u.isGuest = isGuest == 1
	return UserInfo_Login{
		UserInfo_Base: UserInfo_Base{
			UserId:      u.userId,
			Gold:        u.gold,
			Vip:         u.viplevel,
			NickName:    u.nickName,
			FaceUrl:     u.faceUrl,
			FaceId:      u.faceId,
			Sex:         u.sex,
			UserWords:   u.userWords,
			Chip:        u.chip,
			Decorations: u.decorations,
			Charm:       u.charm,
			Level:       u.level,
			VipExpire:   u.vipExpire,
		},
		BankAmount:       u.bankamount,
		Session:          u.session,
		PayAmount:        u.payamount,
		Code:             code,
		IsGuest:          isGuest,
		TeacherId:        u.teacherid,
		HigherUserID:     u.higherUserID,
		Grade:            u.grade,
		ChipBank:         u.chipBank,
		ChipSend:         chipSend,
		Experience:       u.experience,
		LoginAward:       loginAward,
		IsWhite:          isWhite,
		AutoLoginChip:    u.autoLoginChip,
		MailVipTip:       u.mailVipTip,
		IsRegister:       isRegister,
		Currency:         u.currency,
		CurrencyIsModify: u.currencyIsModify,
		VipPoint:         u.vipPoint,
		Badges:           u.badges,
	}
}

func (u *UserInfo) GetUserInfo_Table(self bool) UserInfo_Table {
	u.lock.RLock()
	defer u.lock.RUnlock()
	ret := UserInfo_Table{
		UserInfo_Base: UserInfo_Base{
			UserId:      u.userId,
			Gold:        u.gold,
			Vip:         u.viplevel,
			NickName:    u.nickName,
			FaceUrl:     u.faceUrl,
			FaceId:      u.faceId,
			Sex:         u.sex,
			UserWords:   u.userWords,
			Chip:        u.chip,
			Decorations: u.decorations,
			Charm:       u.charm,
			Level:       u.level,
			VipExpire:   u.vipExpire,
		},
		Status:  u.status,
		TableId: u.tableId,
		ChairId: u.chairId,
	}

	//if !self && int(time.Now().Unix()) > u.vipExpire {
	//	ret.Vip = 0
	//}

	return ret
}

func (u *UserInfo) SetTableChairId(tableId, chairId int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.tableId = tableId
	u.chairId = chairId
	if tableId != -1 {
		u.lastTableId = tableId
	}
}

func (u *UserInfo) GetUserWords() string {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.userWords
}

func (u *UserInfo) SetUserWords(value string) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.userWords = value
}

func (u *UserInfo) GetUserTeacher() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.teacherid
}

func (u *UserInfo) SetUserTeacher(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.teacherid = value
}

func (u *UserInfo) GetHigherUserID() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.higherUserID
}

func (u *UserInfo) SetHigherUserID(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.higherUserID = value
}

func (u *UserInfo) GetGrade() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.grade
}

func (u *UserInfo) SetGrade(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.grade = value
}

func (u *UserInfo) GetChip() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.chip
}

func (u *UserInfo) SetChip(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.chip = value
}

func (u *UserInfo) GetChipBank() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.chipBank
}

func (u *UserInfo) SetChipBank(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.chipBank = value
}

func (u *UserInfo) DumpUserInfo() string {
	u.lock.RLock()
	u.lock.RUnlock()
	return fmt.Sprintf("UserInfo[%d:%d] %s Gold:%d Status:%d TableChair:%d.%d Robot[%d] Level:%d(%d) Chip:%d",
		u.userIndex, u.userId, u.nickName, u.gold, u.status, u.tableId, u.chairId, u.isRobot, u.level, u.experience, u.chip)
}

func (u *UserInfo) GetCopy() *UserInfo {
	userInfo := *u
	return &userInfo
}

func (u *UserInfo) GetUserLevel() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.level
}

func (u *UserInfo) SetUserLevel(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.level = value
}

func (u *UserInfo) GetUserExperience() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.experience
}

func (u *UserInfo) SetUserExperience(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.experience = value
}

func (u *UserInfo) GetCurrency() string {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.currency
}

func (u *UserInfo) SetCurrency(value string) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.currency = value
}

func (u *UserInfo) GetCurrencyIsModify() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.currencyIsModify
}

func (u *UserInfo) SetCurrencyIsModify(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.currencyIsModify = value
}

func (u *UserInfo) GetUTMSource() string {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.utmSource
}

func (u *UserInfo) SetUTMSource(value string) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.utmSource = value
}

func (u *UserInfo) IsGuest() bool {
	return u.isGuest
}

func (u *UserInfo) GetDecorations() []userservices.UserDecoration {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.decorations
}

func (u *UserInfo) SetDecorations(value []userservices.UserDecoration) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.decorations = value
}

func (u *UserInfo) GetBadges() []badge.BadgePosition {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.badges
}

func (u *UserInfo) SetBadges(value []badge.BadgePosition) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.badges = value
}

func (u *UserInfo) GetScore() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.score
}

func (u *UserInfo) SetScore(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.score = value
}

func (u *UserInfo) GetBaseScore() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.baseScore
}

func (u *UserInfo) SetBaseScore(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.baseScore = value
}

func (u *UserInfo) GetSetCount() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.setCount
}

func (u *UserInfo) SetSetCount(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.setCount = value
}

func (u *UserInfo) GetPartnerId() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.partnerId
}

func (u *UserInfo) SetPartnerId(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.partnerId = value
}

func (u *UserInfo) GetVersionCode() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.versionCode
}

func (u *UserInfo) SetVersionCode(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.versionCode = value
}

func (u *UserInfo) GetUserCharm() int {
	u.lock.RLock()
	defer u.lock.RUnlock()
	return u.charm
}

func (u *UserInfo) SetUserCharm(value int) {
	u.lock.Lock()
	defer u.lock.Unlock()
	u.charm = value
}
