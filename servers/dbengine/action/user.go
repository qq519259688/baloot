package action

import (
	"math/rand"

	"bet24.com/log"
	"bet24.com/servers/dbengine/tables"
	_ "github.com/go-sql-driver/mysql"
	uuid "github.com/satori/go.uuid"
)

func Login(imei string, nickName string, faceUrl string, partnerId int, version string, ip string) *tables.User {
	// isExist?
	rows, err := CenterDB.Query(
		"select * from userinfo where Imei = ?", imei)
	if err != nil {
		log.Debug("action.Login failed to select user:%v", err)
		return nil
	}
	if !rows.Next() {
		return registerUser(imei, nickName, faceUrl, partnerId, version, ip)
	}

	var ret tables.User
	if err := rows.Scan(&ret.UserId,
		&ret.Imei,
		&ret.Password,
		&ret.NickName,
		&ret.FaceId,
		&ret.FaceUrl,
		&ret.ChangeIndex,
		&ret.VipLevel,
		&ret.VipPoint,
		&ret.PayAmount,
		&ret.Gold,
		&ret.Diamond,
		&ret.CannonId,
		&ret.BulletId,
		&ret.PartnerId,
		&ret.Version,
	); err != nil {
		log.Debug("user.Login failed to scan %v", err)
		return nil
	}
	inertLoginLog(ret.UserId, partnerId, version, ip)
	return &ret
}

func registerUser(imei string, nickName string, faceUrl string, partnerId int, version string, ip string) *tables.User {
	for i := 0; i < 5; i++ {
		userId := genUserId()
		if userId == 0 {
			log.Debug("user.registerUser failed to genUserId")
			return nil
		}
		password, _ := uuid.NewV4()
		_, err := CenterDB.Exec(
			"INSERT INTO userinfo (UserId,Imei,Password,NickName,FaceUrl) VALUES (?,?,?,?,?)",
			userId, imei, password, nickName, faceUrl)
		if err != nil {
			log.Debug("user.registerUser %v", err)
			continue
		}
		break
	}
	return Login(imei, nickName, faceUrl, partnerId, version, ip)
}

func inertLoginLog(userId int, partnerId int, version string, ip string) {
	_, err := CenterDB.Exec(
		"INSERT INTO userlogin_log (UserId,PartnerId,Version,Ip,LoginTime) VALUES (?,?,?,?,?)",
		userId, partnerId, version, ip, getNow())
	if err != nil {
		log.Debug("%v", err)
		return
	}
}

func genUserId() int {
	userId := rand.Intn(90000) + 10000
	rows, err := CenterDB.Query(
		"select * from userinfo where UserId = ?", userId)
	if err != nil {
		log.Debug("user.Login failed to select user:%V", err)
		return 0
	}
	if !rows.Next() {
		return userId
	}
	return genUserId()
}
