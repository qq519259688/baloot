package action

import (
	"bet24.com/log"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

var (
	CenterDB *sql.DB
)

func Run(server string, port int, user, password, centerDB string) bool {
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", user, password, server, port, centerDB))
	if err != nil {
		log.Debug("%v", err)
		return false
	}
	CenterDB = db
	testLogin()
	return true
}

func getNow() int64 {
	now := time.Now()
	return now.UnixNano() / 1e6
}

func testLogin() {
	Login("testImei", "testNickname", "testFace", 10001, "1.0.1", "127.0.0.1")
}
