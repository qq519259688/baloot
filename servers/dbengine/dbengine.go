package dbengine

import (
	"bet24.com/servers/dbengine/action"
)

func Run(server string, port int, user, password, centerDB string) bool {
	return action.Run(server, port, user, password, centerDB)
}
