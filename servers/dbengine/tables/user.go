package tables

type User_identity struct {
	UserId   int
	Imei     string
	Password string
	NickName string
}

type User_face struct {
	FaceId      int
	FaceUrl     string
	ChangeIndex int
}

type User_vip struct {
	VipLevel int
	VipPoint int
}

type User_pay struct {
	PayAmount int // 充值金额
	Gold      int
	Diamond   int
}

type User_game struct {
	CannonId int
	BulletId int
}

type User_source struct {
	PartnerId int
	Version   string
}

type User struct {
	User_identity
	User_face
	User_vip
	User_pay
	User_game
	User_source
}
