package tables

type Shop_item struct {
	ProductId   string
	ProductName string
	Amount      int
	Price       int // 单位为钻石数量或者人民币分
	IsHot       int
	Bonus       int    // 多送百分比
	Extra       string // 扩展信息
}

type Shop_gold struct {
	Shop_item
}

type Shop_diamond struct {
	Shop_gold
}

type Shop_cannon struct {
	Shop_gold
}

type Shop_gift struct {
	Shop_gold
}
