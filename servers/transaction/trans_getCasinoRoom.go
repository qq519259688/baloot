package transaction

import (
	"bet24.com/database"
	"bet24.com/log"
	"runtime/debug"
)

type trans_getCasinoRoom_out struct {
	Room   string
	GameID int
}

type trans_getCasinoRoom struct {
	database.Trans_base
	Out    trans_getCasinoRoom_out
	UserID int
}

func NewTransGetCasinoRoom() *trans_getCasinoRoom {
	obj := new(trans_getCasinoRoom)
	return obj
}

func (this *trans_getCasinoRoom) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}

	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_CasinoOnline_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.UserID)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	// 这里带输出和结果集，最后一行是输出参数
	rowCount := len(retRows)
	if rowCount > 0 {
		ret := retRows[0]
		this.Out.GameID = int((ret[0]).(int64))
		this.Out.Room = (ret[1]).(string)
	}
	this.State = true
}
