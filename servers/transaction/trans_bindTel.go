package transaction

import (
	"runtime/debug"

	item "bet24.com/servers/micros/item_inventory/proto"

	"bet24.com/database"
	"bet24.com/log"
)

// 绑定手机号
type (
	trans_bindTel_in struct {
		UserID  int    // 用户ID
		Tel     string // 手机号
		SMSCode string // 验证码
	}

	trans_bindTel_out struct {
		RetCode int // 操作结果
		Items   []item.ItemPack
	}

	trans_bindTel struct {
		database.Trans_base
		In  trans_bindTel_in
		Out trans_bindTel_out
	}
)

func NewTransBindTel() *trans_bindTel {
	return &trans_bindTel{}
}

func (this *trans_bindTel) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserTel_Bind")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@Tel", database.AdParamInput, database.AdVarChar, 32, this.In.Tel)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}
	this.Out.RetCode = int((retRows[0][0]).(int64))
}
