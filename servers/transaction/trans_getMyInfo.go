package transaction

import (
	"encoding/json"

	"bet24.com/database"
	"bet24.com/log"
)

// 获取自己的大厅基本信息
type (
	trans_getMyInfo_in struct {
		UserID int `binding:"required"`
	}

	trans_getMyInfo_out struct {
		UserID           int     //用户ID
		NickName         string  //昵称
		ServerName       string  //服务器名
		FaceID           int     //头像ID
		FaceUrl          string  //头像URL
		Sex              int     //性别
		Amount           int     //金额
		Bank             int     //保险柜
		EPassword        string  //登陆密码(md5密文)
		PartnerID        int     //渠道ID
		PayMoney         float64 //充值额度
		VipLevel         int     //Vip等级
		UserWords        string  //个性签名
		Code             int     //推荐码
		IsGuest          int     //是否游客
		TeacherId        int     //师父ID
		HigherUserID     int     // 上级用户ID
		Grade            int     // 代理等级
		ChipAmount       int     // 筹码
		ChipBank         int     // 筹码保险柜
		Currency         string  // 币种
		CurrencyIsModify int     // 是否允许修改(1=允许修改  其他=禁止修改)
		UTMSource        string  // 注册流量渠道
		Charm            int     // 魅力值
	}

	trans_getMyInfo struct {
		database.Trans_base
		In  trans_getMyInfo_in
		Out trans_getMyInfo_out
	}
)

func NewTransGetMyInfo() *trans_getMyInfo {
	obj := new(trans_getMyInfo)
	return obj
}

func (this *trans_getMyInfo) DoAction(ch chan<- interface{}) {
	this.State = false
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_GetMyInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	jsonData := CenterDB.ExecSqlJson(sqlstring)
	if jsonData == "" {
		log.Debug("trans_getMyInfo.DoAction failed data == null")
		return
	}

	var resultSet []trans_getMyInfo_out
	err := json.Unmarshal([]byte(jsonData), &resultSet)
	if err != nil || len(resultSet) < 1 {
		log.Release("trans_getMyInfo.DoAction Unmarshal failed err = %v,data = %s", err, jsonData)
		return
	}
	this.State = true
	this.Out = resultSet[0]
}
