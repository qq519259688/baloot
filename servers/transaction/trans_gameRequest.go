package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

type GameRequestInfo struct {
	PartnerID   int
	VersionCode int
	GameName    string
}

type trans_gameRequest struct {
	database.Trans_base
	OUT []GameRequestInfo
}

func NewTransGameRequest() *trans_gameRequest {
	return &trans_gameRequest{}
}

func (this *trans_gameRequest) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Request_GetInfo")
	sqlstring := statement.GenSql()
	//log.Debug("%v", sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	rowsLen := len(retRows)
	if rowsLen <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.OUT = make([]GameRequestInfo, rowsLen)

	for i := 0; i < rowsLen; i++ {
		ret := retRows[i]
		out := &this.OUT[i]

		out.PartnerID = int((ret[0]).(int64))
		out.VersionCode = int((ret[1]).(int64))
		out.GameName = (ret[2]).(string)
	}
}
