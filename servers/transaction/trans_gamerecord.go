package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

func WriteGameRecord(userId int, gameId int, roomName string, record string) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Game_GameData_WriteRecord")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdVarChar, 32, roomName)
	statement.AddParamter("@Record", database.AdParamInput, database.AdVarChar, 512, record)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	CenterDB.ExecSql(sqlstring)
}

func GetGameRecord(userId int, gameId int, roomName string, recordCount int) []string {
	defer func() {
		if err := recover(); err != nil {
			log.Release("GetGameRecord recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Game_GameData_GetRecordList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdVarChar, 32, roomName)
	statement.AddParamter("@RecordCount", database.AdParamInput, database.AdInteger, 4, recordCount)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowsLen := len(retRows)
	if rowsLen <= 0 {
		return []string{}
	}

	result := make([]string, rowsLen)
	for i := 0; i < rowsLen; i++ {
		ret := retRows[i]
		result[i] = (ret[0]).(string)
	}
	return result
}
