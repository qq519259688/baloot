package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

func Trans_SetGameStatus(userID, gameID, isOnline int, serverName string, isChipRoom bool) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()
	if userID == 0 {
		log.Release("%s", debug.Stack())
		return
	}
	isChip := 0
	if isChipRoom {
		isChip = 1
	}
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Game_AllUser_SetGameStatus")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userID)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdVarChar, 64, gameID)
	statement.AddParamter("@ServerName", database.AdParamInput, database.AdNVarChar, 32, serverName)
	statement.AddParamter("@IsOnline", database.AdParamInput, database.AdTinyInt, 1, isOnline)
	statement.AddParamter("@IsChip", database.AdParamInput, database.AdInteger, 4, isChip)
	sqlstring := statement.GenSql()
	CenterDB.ExecSql(sqlstring)
}
