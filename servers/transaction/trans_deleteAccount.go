package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 删除应用账号
func DeleteAccount(userId int) bool {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_DelAccount")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return false
	}

	return int(retRows[0][0].(int64)) == 1
}
