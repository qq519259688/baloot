package transaction

import (
	"fmt"
	"runtime/debug"
	"strconv"
	"strings"

	"bet24.com/database"
	"bet24.com/log"
)

type trans_login_in struct {
	IMei         string //IMei
	NickName     string //昵称
	PartnerID    int    //渠道ID
	Version      int    //版本号
	IpAddress    string //IP
	FaceUrl      string //头像url
	Deviceid     string //设备号
	DeviceName   string // 设备名称
	Referrer     string // 流量渠道追踪全解析
	MessageToken string // 推送消息token
}

type trans_login_out struct {
	RetCode  int
	ErrorMsg string

	UserID           int     //用户ID
	NickName         string  //昵称
	FaceID           int     //头像ID
	FaceUrl          string  //头像url
	Password         string  //登陆密码(md5密文)
	Money            int     //金币数
	Bank             int     //保险柜
	PayAmount        float64 //充值额度
	Sex              int     //性别
	VipLevel         int     //Vip等级
	ForbidTime       string  //封号时间
	SectionToken     string
	UserWords        string //个性签名
	Code             int    //推荐码
	IsGuest          int    //是否游客
	TeacherId        int    // 师父ID
	HigherUserID     int    // 上级ID
	Grade            int    // 代理等级
	Chip             int    // 筹码
	ChipBank         int    // 筹码保险柜
	ChipSend         int    // 筹码赠送
	AutoLoginChip    int    // 自动登录元宝大厅
	IsReturn         bool   // 是否回归用户,超过10天没有登录
	IsRegister       bool   // 是否新注册用户
	Currency         string // 币种
	CurrencyIsModify int    // 是否允许修改(1=允许修改  其他=禁止修改)
	UTMSource        string // 注册流量渠道
	Charm            int    // 魅力值
}

type trans_login struct {
	database.Trans_base
	In  trans_login_in
	Out trans_login_out
}

func NewTransLogin() *trans_login {
	return &trans_login{}
}

func (this *trans_login) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	//游客登陆
	statement.SetProcName("WS_AllUser_LoginByIMei")
	statement.AddParamter("@IMei", database.AdParamInput, database.AdVarChar, 64, this.In.IMei)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, this.In.NickName)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@Version", database.AdParamInput, database.AdInteger, 4, this.In.Version)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 32, this.In.IpAddress)
	statement.AddParamter("@deviceid", database.AdParamInput, database.AdVarChar, 64, this.In.Deviceid)
	statement.AddParamter("@DeviceName", database.AdParamInput, database.AdNVarChar, 32, this.In.DeviceName)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	this.State = true
	ret := retRows[0]
	this.Out.RetCode = int((ret[0]).(int64))
	this.Out.UserID = int((ret[1]).(int64))
	this.Out.NickName = (ret[2]).(string)
	this.Out.FaceID = int((ret[3]).(int64))
	this.Out.FaceUrl = (ret[4]).(string)
	this.Out.Password = (ret[5]).(string)
	this.Out.Money = int((ret[6]).(int64))
	payAmount := string(ret[7].([]byte))
	this.Out.PayAmount, _ = strconv.ParseFloat(payAmount, 64)
	this.Out.VipLevel = int((ret[8]).(int64))
	this.Out.ForbidTime = (ret[9]).(string)
	this.Out.Bank = int((ret[10]).(int64))
	this.Out.Sex = int((ret[11]).(int64))
	this.Out.UserWords = (ret[12]).(string)
	this.Out.Code = int((ret[13]).(int64))
	this.Out.IsGuest = int((ret[14]).(int64))
	this.Out.TeacherId = int((ret[15]).(int64))
	this.Out.HigherUserID = int((ret[16]).(int64))
	this.Out.Grade = int((ret[17]).(int64))
	this.Out.Chip = int((ret[18]).(int64))
	this.Out.ChipBank = int((ret[19]).(int64))
	this.Out.ChipSend = int((ret[20]).(int64))
	this.Out.AutoLoginChip = int((ret[21]).(int64))
	this.Out.IsReturn = int((ret[22]).(int64)) == 1
	this.Out.IsRegister = int((ret[23]).(int64)) == 1
	this.Out.Currency = (ret[24]).(string)
	this.Out.CurrencyIsModify = int((ret[25]).(int64))
	this.Out.UTMSource = (ret[26]).(string)
	if len(ret) > 27 {
		this.Out.Charm = int((ret[27]).(int64))
	}
}

// 流量渠道追踪全解析
func (this *trans_login) UTMInsert() (string, string) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	// 没有渠道跟踪解析信息
	if this.In.Referrer == "" {
		return "", ""
	}

	// 流量渠道追踪全解析
	var (
		source   string // 广告系列来源。用来标识搜索引擎、简报名称或其他来源。示例： utm_source=google
		medium   string // 广告系列媒介。用来标识媒介，比如电子邮件或每次点击费用。示例：utm_medium=cpc
		term     string // 广告系列字词。用于付费搜索，用来注明此广告的关键字。示例：utm_term=running+shoes
		content  string // 广告系列内容。用于A/B测试和按内容进行定位的广告。区分指向同一网址的广告或链接。示例：utm_content=logolink或utm_content=textlink
		campaign string // 广告系列名称。用于关键字分析。用来标识特定的产品促销活动或战略性广告系列。示例：utm_campaign=spring_sale
	)

	referrers := strings.Split(this.In.Referrer, "&")
	for _, v := range referrers {
		params := strings.Split(v, "=")
		if len(params) != 2 {
			log.Release("loginByIMei.UTMInsert userId=%d keys=%s is invalid", this.Out.UserID, v)
			continue
		}

		name := params[0]
		value := params[1]
		switch name {
		case "gclid":
			source = "GoogleAd"
			content = params[1]
		case "adjust_external_click_id":
			source = "Adjust"
			content = value
			ss := strings.Split(content, "_")
			if len(ss) > 1 {
				source = fmt.Sprintf("%s_%s", source, ss[0])
			}
		case "utm_source":
			source = value
		case "utm_medium":
			medium = value
		case "utm_term":
			term = value
		case "utm_content":
			content = value
		case "utm_campaign":
			campaign = value
		default:
			source = name
			content = value
			log.Release("loginByIMei.UTMInsert userId=%d [%s]=[%s] not exist", this.Out.UserID, name, value)
		}
	}

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UTM_Insert")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.Out.UserID)
	statement.AddParamter("@Source", database.AdParamInput, database.AdNVarChar, 128, source)
	statement.AddParamter("@Medium", database.AdParamInput, database.AdNVarChar, 128, medium)
	statement.AddParamter("@Term", database.AdParamInput, database.AdNVarChar, 128, term)
	statement.AddParamter("@Content", database.AdParamInput, database.AdNVarChar, 256, content)
	statement.AddParamter("@Campaign", database.AdParamInput, database.AdNVarChar, 128, campaign)
	sqlString := statement.GenSql()
	CenterDB.ExecSql(sqlString)
	return source, content
}
