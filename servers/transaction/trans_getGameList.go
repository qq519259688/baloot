package transaction

import (
	"encoding/json"
	"runtime/debug"
	"sort"

	"bet24.com/database"
	"bet24.com/log"
)

const (
	GameType_All  = iota // 所有类型
	GameType_Gold        // 金币类型
	GameType_Chip        // 元宝类型
)

type trans_getGameList_in struct {
	PartnerID int
}

// GameID, ChineseName, EnglishName, IconVersion,
// ServerIP, Port, Version, DownloadRoot, WebRoot,
// 0 AS PartnerSort
type GameInfo struct {
	GameID      int
	ChineseName string
	EnglishName string
	ServerAddr  string
	GameType    int // 游戏类型
	DisplaySort string
	goldSort    string
	chipSort    string
	NeedLevel   int // 所需等级
}

type trans_getGameList struct {
	database.Trans_base
	In  *trans_getGameList_in
	Out []GameInfo
}

func newTransGetGameList() *trans_getGameList {
	return &trans_getGameList{
		In: &trans_getGameList_in{},
	}
}

func (this *trans_getGameList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllGame_GetList")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	jsonData := CenterDB.ExecSqlJson(sqlstring)
	json.Unmarshal([]byte(jsonData), &this.Out)
	for i := 0; i < len(this.Out); i++ {
		displaySort := this.Out[i].DisplaySort
		if len(displaySort) >= 5 {
			this.Out[i].goldSort = displaySort[1:3]
			this.Out[i].chipSort = displaySort[3:]
		}
	}
}

func GetGameListFromDB(isChipRoom bool) []GameInfo {
	act := newTransGetGameList()
	act.DoAction(nil)
	var ret []GameInfo
	for _, v := range act.Out {
		if isChipRoom && v.GameType == GameType_Gold {
			continue
		}
		if !isChipRoom && v.GameType == GameType_Chip {
			continue
		}
		ret = append(ret, v)
	}

	if isChipRoom {
		sort.SliceStable(ret, func(i, j int) bool {
			return ret[i].chipSort < ret[j].chipSort
		})
	} else {
		sort.SliceStable(ret, func(i, j int) bool {
			return ret[i].goldSort < ret[j].goldSort
		})
	}

	return ret
}
