package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

type trans_gameroom_ping struct {
	database.Trans_base
	GameID     int
	ServerName string
	Type       int
}

func NewGameRoomPing() *trans_gameroom_ping {
	obj := new(trans_gameroom_ping)
	return obj
}

func (this *trans_gameroom_ping) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_CasinoOnline_Verify")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, this.GameID)
	statement.AddParamter("@ServerName", database.AdParamInput, database.AdNVarChar, 32, this.ServerName)
	statement.AddParamter("@Type", database.AdParamInput, database.AdTinyInt, 1, this.Type)
	sqlstring := statement.GenSql()
	CenterDB.ExecSql(sqlstring)
	this.State = true
}

func DoGameRoomPing(gameID int, pingType int, serverName string) {
	obj := NewGameRoomPing()
	obj.GameID = gameID
	obj.Type = pingType
	obj.ServerName = serverName
	go obj.DoAction(nil)
}
