package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

// 绑定 facebook
type (
	trans_bindFacebook_in struct {
		UserID   int    // 用户ID
		OpenID   string // openid
		NickName string // 昵称
		FaceUrl  string // 头像URL
	}

	trans_bindFacebook_out struct {
		RetCode  int    // 操作结果
		NickName string // 昵称
		FaceId   int    // 头像Id
		FaceUrl  string // 头像URL
		Items    []item.ItemPack
	}

	trans_bindFacebook struct {
		database.Trans_base
		In  trans_bindFacebook_in
		Out trans_bindFacebook_out
	}
)

func NewTransBindFacebook() *trans_bindFacebook {
	return &trans_bindFacebook{}
}

func (this *trans_bindFacebook) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserFacebook_Bind")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@OpenID", database.AdParamInput, database.AdVarChar, 64, this.In.OpenID)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, this.In.NickName)
	statement.AddParamter("@FaceUrl", database.AdParamInput, database.AdVarChar, 1024, this.In.FaceUrl)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}
	this.Out.RetCode = int((retRows[0][0]).(int64))
	if this.Out.RetCode == 1 {
		this.Out.NickName = this.In.NickName
		this.Out.FaceId = 0
		this.Out.FaceUrl = this.In.FaceUrl
		this.Out.Items = append(this.Out.Items, item.ItemPack{
			ItemId: 1,
			Count:  int((retRows[0][1]).(int64)),
		})
	}
}
