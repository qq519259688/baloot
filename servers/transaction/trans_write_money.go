package transaction

import (
	"bet24.com/database"
	"bet24.com/log"
	"runtime/debug"
)

func WriteMoneySync(userID, gameID, amount, tax, status, scoreType int, srcName, userIP string) (bool, int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("DoAction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Money_WriteMoney")

	var balance int
	var retCode int
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userID)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 64, srcName)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdVarChar, 64, gameID)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@Tax", database.AdParamInput, database.AdBigint, 8, tax)
	statement.AddParamter("@Status", database.AdParamInput, database.AdTinyInt, 1, status)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, userIP)
	statement.AddParamter("@Type", database.AdParamInput, database.AdInteger, 4, scoreType)
	statement.AddParamter("@Balance", database.AdParamOutput, database.AdBigint, 8, balance)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdTinyInt, 1, retCode)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return false, 0, 0
	}
	ret := retRows[0]
	balance = int((ret[0]).(int64))
	retCode = int((ret[1]).(int64))
	return true, retCode, balance
}

func WriteMoneySyncWithModifyAmount(userID, gameID, amount, tax, status, scoreType int, srcName, userIP string) (int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("DoAction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Money_WriteMoney_GetModifyAmount")

	var balance int
	var modifyAmount int
	var retCode int
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userID)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 64, srcName)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdVarChar, 64, gameID)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@Tax", database.AdParamInput, database.AdBigint, 8, tax)
	statement.AddParamter("@Status", database.AdParamInput, database.AdTinyInt, 1, status)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, userIP)
	statement.AddParamter("@Type", database.AdParamInput, database.AdInteger, 4, scoreType)
	statement.AddParamter("@Balance", database.AdParamOutput, database.AdBigint, 8, balance)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdTinyInt, 1, retCode)
	statement.AddParamter("@ModifyAmount", database.AdParamOutput, database.AdBigint, 8, modifyAmount)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0, 0
	}
	ret := retRows[0]
	balance = int((ret[0]).(int64))
	retCode = int((ret[1]).(int64))
	modifyAmount = int((ret[2]).(int64))
	return balance, modifyAmount
}

func WriteChipSync(userID, gameID, amount, tax, status, scoreType int, srcName, userIP string) (bool, int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("DoAction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Chip_Write")

	var balance int
	var retCode int
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userID)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 64, srcName)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdVarChar, 64, gameID)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@Tax", database.AdParamInput, database.AdBigint, 8, tax)
	statement.AddParamter("@Status", database.AdParamInput, database.AdTinyInt, 1, status)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, userIP)
	statement.AddParamter("@Type", database.AdParamInput, database.AdInteger, 4, scoreType)
	statement.AddParamter("@Balance", database.AdParamOutput, database.AdBigint, 8, balance)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdTinyInt, 1, retCode)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return false, 0, 0
	}
	ret := retRows[0]
	balance = int((ret[0]).(int64))
	retCode = int((ret[1]).(int64))
	return true, retCode, balance
}
