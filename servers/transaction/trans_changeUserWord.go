package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

type changeUserWord_in struct {
	UserID    int
	UserWords string
}

type changeUserWord_out struct {
	RetCode int
}

type changeUserWord struct {
	database.Trans_base
	IN  changeUserWord_in
	OUT changeUserWord_out
}

func NewChangeUserWord() *changeUserWord {
	return &changeUserWord{}
}

func (this *changeUserWord) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_ChangeUserWord")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.IN.UserID)
	statement.AddParamter("@UserWords", database.AdParamInput, database.AdNVarChar, 256, this.IN.UserWords)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.OUT.RetCode)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	ret := retRows[0]
	this.OUT.RetCode = int((ret[0]).(int64))
	this.State = true
}
