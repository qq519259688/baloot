package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

type trans_serviceMsg_in struct {
	UserID    int `binding:"required"`
	MessageID int `binding:"required"`
}

type serviceMsg struct {
	MessageID int
	UserID    int
	ToUserID  int
	Msg       string
	Crdate    int
}

type serviceMsgList struct {
	MessageID   int
	ServiceMsgs []serviceMsg
}

type trans_getServiceMsgList struct {
	database.Trans_base
	IN  trans_serviceMsg_in
	OUT serviceMsgList
}

func NewTransServiceMsgList() *trans_getServiceMsgList {
	return &trans_getServiceMsgList{}
}

func (this *trans_getServiceMsgList) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_ServiceMessage_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.IN.UserID)
	statement.AddParamter("@MessageID", database.AdParamInput, database.AdInteger, 4, this.IN.MessageID)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	rowsLen := len(retRows)
	this.State = true
	if rowsLen <= 0 {
		return
	}

	this.OUT.ServiceMsgs = make([]serviceMsg, rowsLen)
	for i := 0; i < rowsLen; i++ {
		ret := retRows[i]
		out := &this.OUT.ServiceMsgs[i]
		out.MessageID = int((ret[0]).(int64))
		out.UserID = int((ret[1]).(int64))
		out.ToUserID = int((ret[2]).(int64))
		out.Msg = (ret[3]).(string)
		out.Crdate = int((ret[4]).(int64))

		// 把最大值赋给上层读取
		if out.MessageID > this.OUT.MessageID {
			this.OUT.MessageID = out.MessageID
		}
	}
}
