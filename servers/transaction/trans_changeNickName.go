package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 修改昵称
type (
	trans_changeNickName_in struct {
		UserID      int    `binding:"required"`
		NewNickName string `binding:"required"`
	}

	trans_changeNickName_out struct {
		RetCode  int
		ErrorMsg string
	}

	trans_changeNickName struct {
		database.Trans_base
		In  trans_changeNickName_in
		Out trans_changeNickName_out
	}
)

func NewTransChangeNickName() *trans_changeNickName {
	obj := new(trans_changeNickName)
	return obj
}

func (this *trans_changeNickName) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}

	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_AllUser_ChangeNickName")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, this.In.NewNickName)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	ret := retRows[0]
	this.Out.RetCode = int((ret[0]).(int64))
	this.State = true
}

// 修改昵称次数
func ChangeNameTimes(userId int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_ChangeNameTimes")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return -1
	}

	return int((retRows[0][0]).(int64))
}
