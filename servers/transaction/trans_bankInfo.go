package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 银行信息列表
type (
	bankInfoList_in struct {
		UserID int
	}

	// 银行卡信息
	bankInfo struct {
		RealName string // 真实姓名
		BankCard string // 银行卡
		BankName string // 银行名称
		Mobile   string // 手机号
		EMail    string // 邮箱
		Address  string // 地址
	}

	bankInfoList_out struct {
		List []bankInfo
	}

	bankInfoList struct {
		database.Trans_base
		In  bankInfoList_in
		Out bankInfoList_out
	}
)

func NewBankInfoList() *bankInfoList {
	return &bankInfoList{}
}

func (this *bankInfoList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_BankInfo_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	this.Out.List = make([]bankInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.RealName = (ret[0]).(string)
		out.BankCard = (ret[1]).(string)
		out.BankName = (ret[2]).(string)
		out.Mobile = (ret[3]).(string)
		out.EMail = (ret[4]).(string)
		out.Address = (ret[5]).(string)
	}
}
