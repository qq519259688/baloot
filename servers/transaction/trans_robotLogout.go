package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

type trans_robot_logout_in struct {
	GameID     int
	ServerName string
	UserID     int
}

type trans_robot_logout struct {
	database.Trans_base
	In trans_robot_logout_in
	// Out trans_robot_login_out
}

func NewTransRobotLogout() *trans_robot_logout {
	return &trans_robot_logout{}
}

func (this *trans_robot_logout) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	//游客登陆
	statement.SetProcName("prGS_MachineUserLogout")
	statement.AddParamter("@intGameID", database.AdParamInput, database.AdInteger, 4, this.In.GameID)
	statement.AddParamter("@chvServerName", database.AdParamInput, database.AdNVarChar, 128, this.In.ServerName)
	statement.AddParamter("@intUserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)

	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	CenterDB.ExecSql(sqlstring)
	this.State = true
}
