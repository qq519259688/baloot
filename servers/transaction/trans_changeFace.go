package transaction

import (
	"bet24.com/database"
	"bet24.com/log"
	"runtime/debug"
)

type changeFace_in struct {
	UserID  int    `binding:"required"` //用户ID
	Sex     int    //性别
	FaceID  int    //头像ID
	FaceUrl string //头像url地址
}

type changeFace_out struct {
	RetCode  int
	ErrorMsg string
}

type changeFace struct {
	database.Trans_base
	IN  changeFace_in
	Out changeFace_out
}

func NewChangeFace() *changeFace {
	return &changeFace{}
}

func (this *changeFace) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(true)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_AllUser_ChangeFace")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.IN.UserID)
	statement.AddParamter("@Sex", database.AdParamInput, database.AdInteger, 4, this.IN.Sex)
	statement.AddParamter("@FaceID", database.AdParamInput, database.AdInteger, 4, this.IN.FaceID)
	statement.AddParamter("@FaceUrl", database.AdParamInput, database.AdVarChar, 1024, this.IN.FaceUrl)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	ret := retRows[0]
	this.Out.RetCode = int((ret[0]).(int64))
	this.State = true
}

func ChangeUserFace(userId int, faceId int, faceUrl string) bool {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
			return
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(true)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_AllUser_ChangeFace")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Sex", database.AdParamInput, database.AdInteger, 4, 0)
	statement.AddParamter("@FaceID", database.AdParamInput, database.AdInteger, 4, faceId)
	statement.AddParamter("@FaceUrl", database.AdParamInput, database.AdVarChar, 1024, faceUrl)
	sqlstring := statement.GenSql()

	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return false
	}

	ret := retRows[0]
	retCode := int((ret[0]).(int64))
	return retCode == 1
}
