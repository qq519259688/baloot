package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 获取绑定信息
type (
	trans_bindingInfo_in struct {
		UserID int // 用户ID
	}

	trans_bindingInfo_out struct {
		IsFacebook int // 是否绑定facebook
		IsTel      int // 是否绑定手机
	}

	trans_bindingInfo struct {
		database.Trans_base
		In  trans_bindingInfo_in
		Out trans_bindingInfo_out
	}
)

func NewTransBindingInfo() *trans_bindingInfo {
	return &trans_bindingInfo{}
}

func (this *trans_bindingInfo) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_GetBindInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}
	this.Out.IsFacebook = int((retRows[0][0]).(int64))
	this.Out.IsTel = int((retRows[0][1]).(int64))
}
