package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 客服反馈
type (
	feedback_in struct {
		Email     string // 邮箱
		ToUserId  int    // 关联的用户ID
		Msg       string // 内容
		IPAddress string // IP地址
	}

	feedback_out struct {
		RetCode int
	}

	feedback struct {
		database.Trans_base
		In  feedback_in
		Out feedback_out
	}
)

func NewFeedback() *feedback {
	return &feedback{}
}

func (this *feedback) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Feedback_Send")
	statement.AddParamter("@EMail", database.AdParamInput, database.AdNVarChar, 64, this.In.Email)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, this.In.ToUserId)
	statement.AddParamter("@Msg", database.AdParamInput, database.AdNVarChar, 512, this.In.Msg)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IPAddress)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	this.Out.RetCode = int((retRows[0][0]).(int64))
	return
}
