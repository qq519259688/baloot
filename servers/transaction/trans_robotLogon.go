package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

type trans_robot_login_in struct {
	GameID     int
	ServerName string
	MaxBeans   int
	MinBeans   int
	IsChip     bool
}

type trans_robot_login_out struct {
	UserID   int    //用户ID
	Password string //登陆密码(md5密文)
	Levels1  int
	Levels2  int
	Levels3  int
}

type trans_robot_login struct {
	database.Trans_base
	In  trans_robot_login_in
	Out trans_robot_login_out
}

func NewTransRobotLogin() *trans_robot_login {
	return &trans_robot_login{}
}

func (this *trans_robot_login) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	//游客登陆
	statement.SetProcName("prGS_MachineUserLogin")
	statement.AddParamter("@intGameID", database.AdParamInput, database.AdInteger, 4, this.In.GameID)
	statement.AddParamter("@chvServerName", database.AdParamInput, database.AdNVarChar, 128, this.In.ServerName)
	statement.AddParamter("@intMaxBeans", database.AdParamInput, database.AdBigint, 8, this.In.MaxBeans)
	statement.AddParamter("@intMinBeans", database.AdParamInput, database.AdBigint, 8, this.In.MinBeans)

	statement.AddParamter("@intUserID", database.AdParamOutput, database.AdInteger, 4, this.Out.UserID)
	statement.AddParamter("@chvPwd", database.AdParamOutput, database.AdVarChar, 32, this.Out.Password)
	statement.AddParamter("@intLevels1", database.AdParamOutput, database.AdInteger, 4, this.Out.Levels1)
	statement.AddParamter("@intLevels2", database.AdParamOutput, database.AdInteger, 4, this.Out.Levels2)
	statement.AddParamter("@intLevels3", database.AdParamOutput, database.AdInteger, 4, this.Out.Levels3)

	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	this.State = true
	ret := retRows[0]
	this.Out.UserID = int((ret[0]).(int64))
	this.Out.Password = (ret[1]).(string)
	this.Out.Levels1 = int((ret[2]).(int64))
	this.Out.Levels2 = int((ret[3]).(int64))
	this.Out.Levels3 = int((ret[4]).(int64))
}
