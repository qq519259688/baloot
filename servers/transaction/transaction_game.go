package transaction

import (
	"bet24.com/database"
	"bet24.com/public"
)

type Transaction_Game struct {
	GameDB     *database.DataBase
	GameID     int
	DataSource string
	DataBase   string
	Login      string
	Password   string
}

func NewTransactionGame(gameID int, ds, db, login, password string) *Transaction_Game {
	obj := new(Transaction_Game)
	obj.GameID = gameID
	obj.DataSource = ds
	obj.DataBase = db
	obj.Login = login
	obj.Password = password
	obj.Run()
	return obj
}

func (this *Transaction_Game) Run() {
	this.GameDB = database.NewDataBase(
		this.DataSource,
		this.DataBase,
		public.DecryptDBString(this.Login),
		public.DecryptDBString(this.Password))
}
