package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

type changeSex_in struct {
	UserID int
	Sex    int //性别 0=默认(无)  1=男   2=女
}

type changeSex_out struct {
	RetCode  int
	FaceID   int //头像ID
	ErrorMsg string
}

type changeSex struct {
	database.Trans_base
	IN  changeSex_in
	Out changeSex_out
}

func NewTransChangeSex() *changeSex {
	return &changeSex{}
}

func (this *changeSex) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_ChangeSex")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.IN.UserID)
	statement.AddParamter("@Sex", database.AdParamInput, database.AdInteger, 4, this.IN.Sex)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	this.State = true
	ret := retRows[0]
	this.Out.RetCode = int((ret[0]).(int64))
	this.Out.FaceID = int((ret[1]).(int64))
}
