package transaction

import (
	"bet24.com/database"
)

func WriteFreeChipRecord(userID, gameID, faceValue, amount int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_GameFreeChip_AddRecord")

	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userID)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameID)
	statement.AddParamter("@FaceValue", database.AdParamInput, database.AdBigint, 8, faceValue)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdBigint, 8, amount)

	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	CenterDB.ExecSql(sqlstring)
	return
}
