package transaction

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

// isReward: 0 表示查询 1 表示领取，需要写数据库
func GetShareItems(userId int, isReward int) []item.ItemPack {
	var rets []item.ItemPack
	defer func() {
		if err := recover(); err != nil {
			log.Release("GetShareItems err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Share_GetItems")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Reward", database.AdParamInput, database.AdInteger, 4, isReward)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return rets
	}

	items := (retRows[0][0]).(string)
	if len(items) <= 0 {
		return rets
	}

	if err := json.Unmarshal([]byte(items), &rets); err != nil {
		log.Error("GetShareItems json unmarshal fail %v", err)
	}

	return rets
}
