package transaction

import (
	"bet24.com/database"
	"bet24.com/log"
)

type trans_write_bet_record_in struct {
	UserID       int     //用户ID
	GameID       int     //游戏ID
	BetSecBefore int     //结算前几秒下注的
	BetAmount    int     //下注金额
	BetDesc      string  //下注描述
	WinAmount    int     //输赢
	ResultDesc   string  //结果描述
	Tax          int     //产生税收
	WinRate      float64 //赔率(百为基数)
	TransType    string  //
	IpAddress    string  //IP地址
	TimeStamp    int
	RoomName     string
	IsChip       bool
}

type trans_write_bet_record struct {
	database.Trans_base
	IN trans_write_bet_record_in
}

func WriteBetRecordAction(userID, gameID int, userIP string, betAmount, winAmount, tax int, winRate float64,
	betDesc, resultDesc string, betSecBefore int, transType string, roomName string) {
	writeBetRecord := newTransWriteBetRecord()
	writeBetRecord.IN.UserID = userID
	writeBetRecord.IN.GameID = gameID
	writeBetRecord.IN.BetSecBefore = betSecBefore
	writeBetRecord.IN.BetAmount = betAmount
	writeBetRecord.IN.BetDesc = betDesc
	writeBetRecord.IN.WinAmount = winAmount
	writeBetRecord.IN.ResultDesc = resultDesc
	writeBetRecord.IN.Tax = tax
	writeBetRecord.IN.WinRate = winRate
	writeBetRecord.IN.IpAddress = userIP
	writeBetRecord.IN.TransType = transType
	writeBetRecord.IN.RoomName = roomName

	go writeBetRecord.DoAction(nil)
}

func WriteChipBetRecordAction(userID, gameID int, userIP string, betAmount, winAmount, tax int, winRate float64,
	betDesc, resultDesc string, betSecBefore int, transType string, roomName string) {
	writeBetRecord := newTransWriteBetRecord()
	writeBetRecord.IN.UserID = userID
	writeBetRecord.IN.GameID = gameID
	writeBetRecord.IN.BetSecBefore = betSecBefore
	writeBetRecord.IN.BetAmount = betAmount
	writeBetRecord.IN.BetDesc = betDesc
	writeBetRecord.IN.WinAmount = winAmount
	writeBetRecord.IN.ResultDesc = resultDesc
	writeBetRecord.IN.Tax = tax
	writeBetRecord.IN.WinRate = winRate
	writeBetRecord.IN.IpAddress = userIP
	writeBetRecord.IN.TransType = transType
	writeBetRecord.IN.RoomName = roomName
	writeBetRecord.IN.IsChip = true

	go writeBetRecord.DoAction(nil)
}

func newTransWriteBetRecord() *trans_write_bet_record {
	return &trans_write_bet_record{}
}
func (this *trans_write_bet_record) DoAction(ch chan<- interface{}) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	if this.IN.IsChip {
		statement.SetProcName("Game_ChipBetRecord_Write")
	} else {
		statement.SetProcName("Game_BetRecord_Write")
	}
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.IN.UserID)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, this.IN.GameID)
	statement.AddParamter("@BetSecBefore", database.AdParamInput, database.AdInteger, 4, this.IN.BetSecBefore)
	statement.AddParamter("@BetAmount", database.AdParamInput, database.AdBigint, 8, this.IN.BetAmount)
	statement.AddParamter("@BetZone", database.AdParamInput, database.AdVarChar, 512, this.IN.BetDesc)
	statement.AddParamter("@ResultAmount", database.AdParamInput, database.AdBigint, 8, this.IN.WinAmount)
	statement.AddParamter("@ResultZone", database.AdParamInput, database.AdVarChar, 1024, this.IN.ResultDesc)
	statement.AddParamter("@Tax", database.AdParamInput, database.AdBigint, 8, this.IN.Tax)
	statement.AddParamter("@Odds", database.AdParamInput, database.AdFloat, 8, this.IN.WinRate)
	statement.AddParamter("@TransType", database.AdParamInput, database.AdVarChar, 32, this.IN.TransType)
	statement.AddParamter("@IpAddress", database.AdParamInput, database.AdVarChar, 16, this.IN.IpAddress)
	statement.AddParamter("@ServerName", database.AdParamInput, database.AdNVarChar, 32, this.IN.RoomName)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	CenterDB.ExecSql(sqlstring)
}

func WriteBetRecordSetCount(userId int, gameId int, secBefore int, setCount int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Game_BetCount_Write")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	statement.AddParamter("@BetSecBefore", database.AdParamInput, database.AdInteger, 4, secBefore)
	statement.AddParamter("@PlayCount", database.AdParamInput, database.AdInteger, 4, setCount)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	CenterDB.ExecSql(sqlstring)
}
