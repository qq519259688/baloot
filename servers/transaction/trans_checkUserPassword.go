package transaction

import (
	"bet24.com/database"
	"bet24.com/log"
	"encoding/json"
	"runtime/debug"
)

type checkUserPassword_in struct {
	UserID    int
	Password  string
	PartnerId int
	Version   int
}

type checkUserPassword_out struct {
	RetCode          int    //结果
	Amount           int    //金币
	Bank             int    //保险柜
	NickName         string //昵称
	FaceID           int    //头像ID
	FaceUrl          string //头像URL
	VipLevel         int    //Vip等级
	Sex              int    //性别
	UserWords        string //个性签名
	Code             int    //推荐码
	IsGuest          int    //是否游客
	TeacherID        int    //师父ID
	HigherUserID     int    // 上级用户ID
	Grade            int    // 代理等级
	ChipAmount       int    // 筹码
	ChipBank         int    // 筹码保险柜
	Currency         string // 币种
	CurrencyIsModify int    // 是否允许修改(1=允许修改  其他=禁止修改)
	UTMSource        string // 注册流量渠道
	Charm            int    // 魅力值
}

type checkUserPassword struct {
	database.Trans_base
	IN  checkUserPassword_in
	Out checkUserPassword_out
}

func NewCheckUserPassword() *checkUserPassword {
	return &checkUserPassword{}
}

func (this *checkUserPassword) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_CheckLogin")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.IN.UserID)
	statement.AddParamter("@EPassword", database.AdParamInput, database.AdVarChar, 64, this.IN.Password)
	sqlstring := statement.GenSql()
	jsonData := CenterDB.ExecSqlJson(sqlstring)
	var list []checkUserPassword_out
	if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
		log.Release("checkUserPassword.transaction load json unmarshal err %v", err)
		log.Release(jsonData)
		this.State = false
		return
	}
	if len(list) == 0 {
		log.Release("checkUserPassword.transaction row count == 0")
		this.State = false
		return
	}
	this.State = true
	this.Out = list[0]
}
