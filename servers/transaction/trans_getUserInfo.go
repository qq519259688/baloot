package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 玩家大厅基本信息
type trans_getInfo_in struct {
	UserID int `binding:"required"`
}

type trans_getInfo_out struct {
	UserID     int    //用户ID
	NickName   string //昵称
	ServerName string //服务器名
	FaceID     int    //头像ID
}

type trans_getInfo struct {
	database.Trans_base
	In  trans_getInfo_in
	Out trans_getInfo_out
}

func NewTransGetInfo() *trans_getInfo {
	obj := new(trans_getInfo)
	return obj
}

func (this *trans_getInfo) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	this.State = true
	ret := retRows[0]
	this.Out.UserID = int((ret[0]).(int64))
	this.Out.NickName = (ret[1]).(string)
	this.Out.ServerName = (ret[2]).(string)
	this.Out.FaceID = int((ret[3]).(int64))
}
