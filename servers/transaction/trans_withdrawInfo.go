package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 提现信息(提现前调用)
type (
	liuWithdrawInfo_in struct {
		UserID int // 用户ID
	}

	liuWithdrawInfo_out struct {
		SurchargeRate           int // 手续费比率
		StillFreeSurchargeTimes int // 剩余免手续费提取次数
		StillTotalAmount        int // 可提取总金额
		StillDailyNoAuditAmount int // 剩余日无审核提现金额
		StillDailyNoAuditTimes  int // 剩余日无审核提现次数
		StillDailyMaxTimes      int // 剩余日提现次数
		MinWithdrawAmount       int // 提现最小金额
	}

	liuWithdrawInfo struct {
		database.Trans_base
		In  liuWithdrawInfo_in
		Out liuWithdrawInfo_out
	}
)

func NewLiuWithdrawInfo() *liuWithdrawInfo {
	return &liuWithdrawInfo{}
}

func (this *liuWithdrawInfo) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_LiuWithdraw_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	ret := retRows[0]
	this.Out.SurchargeRate = int((ret[0]).(int64))
	this.Out.StillFreeSurchargeTimes = int((ret[1]).(int64))
	this.Out.StillTotalAmount = int((ret[2]).(int64))
	this.Out.StillDailyNoAuditAmount = int((ret[3]).(int64))
	this.Out.StillDailyNoAuditTimes = int((ret[4]).(int64))
	this.Out.StillDailyMaxTimes = int((ret[5]).(int64))
	this.Out.MinWithdrawAmount = int((ret[6]).(int64))
}
