package transaction

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 判断手机号是否存在
type (
	trans_existTel_in struct {
		Tel      string // 手机号
		CodeType int
	}

	trans_existTel_out struct {
		RetCode int // 操作结果
	}

	trans_existTel struct {
		database.Trans_base
		In  trans_existTel_in
		Out trans_existTel_out
	}
)

func NewTransExistTel() *trans_existTel {
	return &trans_existTel{}
}

func (this *trans_existTel) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserTel_Exist")
	statement.AddParamter("@Tel", database.AdParamInput, database.AdVarChar, 32, this.In.Tel)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	this.Out.RetCode = int((retRows[0][0]).(int64))
}
