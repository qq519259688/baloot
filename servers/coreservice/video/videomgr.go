package video

import (
	"encoding/json"
	"sort"
	"strconv"
	"sync"

	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

type videomgr struct {
	sys_lock  *sync.RWMutex
	user_lock *sync.RWMutex

	sys_videolist       map[int]*video
	user_videolist      map[int]*user_video
	sysSettle_videolist []*settleVideo
}

func newVideoMgr() *videomgr {
	ret := new(videomgr)
	ret.sys_lock = &sync.RWMutex{}
	ret.user_lock = &sync.RWMutex{}
	ret.sys_videolist = make(map[int]*video)
	ret.user_videolist = make(map[int]*user_video)

	//获取系统数值
	ret.loadSysVideoList()
	ret.loadSettleVideoList()
	return ret
}

func (this *videomgr) loadSysVideoList() {
	list := getSysVideoList()
	this.sys_lock.Lock()
	defer this.sys_lock.Unlock()
	this.sys_videolist = list
}

func (this *videomgr) loadSettleVideoList() {
	list := getSysSettleVideoList()
	if len(list) > 1 {
		//从大到小排序
		sort.SliceStable(list, func(i, j int) bool {
			return list[i].LoseAmount > list[j].LoseAmount
		})
	}

	this.sys_lock.Lock()
	defer this.sys_lock.Unlock()
	this.sysSettle_videolist = list
}

func (this *videomgr) onUserEnter(userId int) {
	this.getUserVideo(userId)
}

func (this *videomgr) onUserExit(userId int) {
	this.user_lock.Lock()
	defer this.user_lock.Unlock()
	delete(this.user_videolist, userId)
}

func (this *videomgr) getSysVideo(videoId int) *video {
	this.sys_lock.RLock()
	defer this.sys_lock.RUnlock()
	ret, ok := this.sys_videolist[videoId]
	if !ok {
		log.Debug("videomgr.getSysVideo videoId[%d] not found", videoId)
		return nil
	}
	return ret
}

func (this *videomgr) getUserVideo(userId int) *user_video {
	this.user_lock.RLock()
	uv, ok := this.user_videolist[userId]
	this.user_lock.RUnlock()
	if ok {
		return uv
	}

	uv = newUserVideo(userId)
	this.user_lock.Lock()
	this.user_videolist[userId] = uv
	this.user_lock.Unlock()
	return uv
}

func (this *videomgr) play(userId, videoId int) (bool, int, []item.ItemPack) {
	uv := this.getUserVideo(userId)
	if uv == nil {
		return false, 0, nil
	}

	return uv.play(videoId)
}

func (this *videomgr) getInfo(userId, videoId int) (bool, int, int, []item.ItemPack) {
	uv := this.getUserVideo(userId)
	if uv == nil {
		return false, 0, 0, nil
	}

	myPlayTimes := 0
	sysPlayTimes := 0
	var awards []item.ItemPack

	success, v, s := uv.getInfo(videoId)
	if v != nil {
		myPlayTimes = v.PlayTimes
	}

	if s != nil {
		sysPlayTimes = s.PlayTimes
		awards = s.Awards
	}

	return success, myPlayTimes, sysPlayTimes, awards
}

func (this *videomgr) getSettleInfo(userId, gameId, settleAmount int) *settleInfo_resp {
	uv := this.getUserVideo(userId)
	if uv == nil {
		return &settleInfo_resp{}
	}

	var list []*settleVideo

	// 先找出符合条件的结算信息
	for _, v := range this.sysSettle_videolist {
		if v.GameID != 0 && v.GameID != gameId {
			continue
		}

		if v.LoseAmount > settleAmount {
			continue
		}

		list = append(list, v)
	}

	// 没有数据
	if len(list) == 0 {
		log.Debug("videomgr.getSettleInfo userId=%d gameId=%d settleAmount=%d len(list)=0 uv=%+v",
			userId, gameId, settleAmount, uv)
		return &settleInfo_resp{}
	}

	// 2条以上数据,做排序
	if len(list) > 1 {
		// settleAmount 大到小排序
		sort.SliceStable(list, func(i, j int) bool {
			return list[i].LoseAmount > list[j].LoseAmount
		})

		// gameId 大到小排序
		sort.SliceStable(list, func(i, j int) bool {
			return list[i].GameID > list[j].GameID
		})
	}

	// 取第一条数据
	return uv.getSettleInfo(gameId, settleAmount, list[0])
}

func (this *videomgr) settle(userId, ts int) (bool, int) {
	uv := this.getUserVideo(userId)
	if uv == nil {
		return false, 0
	}
	return uv.settle(ts)
}

// 游戏返还视频列表
func (this *videomgr) getGameSettleVideoList(userId int) []*settleVideoInfo {
	uv := this.getUserVideo(userId)
	if uv == nil {
		return nil
	}

	return uv.getGameSettleVideoList()
}

// 游戏返还视频奖励
func (this *videomgr) awardGameSettleVideo(userId, settleId int) *awardRetInfo {
	uv := this.getUserVideo(userId)
	if uv == nil {
		return nil
	}

	return uv.awardGameSettleVideo(settleId)
}

// 小红点提醒
func (this *videomgr) checkTip(userId int) bool {
	uv := this.getUserVideo(userId)
	if uv == nil {
		return false
	}

	return uv.checkTip()
}

func (this *videomgr) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("videomgr.dumpSys %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	d, _ := json.Marshal(struct {
		Sys_videolist       map[int]*video
		SysSettle_videolist []*settleVideo
	}{
		Sys_videolist:       this.sys_videolist,
		SysSettle_videolist: this.sysSettle_videolist,
	})
	log.Release(string(d))
}

func (this *videomgr) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("videomgr.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	if param != "" {
		var userId int
		var err error
		if userId, err = strconv.Atoi(param); err != nil {
			log.Release("atoi error %v", err)
			return
		}
		si := this.getUserVideo(userId)
		if si == nil {
			log.Release("user %d not exist", userId)
			return
		}
		d, _ := json.Marshal(si)
		log.Release(string(d))
		return
	}

	for k, v := range this.user_videolist {
		for _, s := range v.video_list {
			log.Release("video_list userId=%d ==> %+v", k, s)
		}
		for _, s := range v.settle_list {
			log.Release("settle_list userId=%d ==> %+v", k, s)
		}
	}
	return
}
