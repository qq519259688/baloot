package video

import item "bet24.com/servers/micros/item_inventory/proto"

const (
	_                 = iota
	Video_Sign        // 1=签到
	Video_Game_Settle // 2=游戏结算
)

// 视频系统数值
type video struct {
	VideoId   int             //视频ID
	PlayTimes int             //播放次数
	Awards    []item.ItemPack //奖励
}

// 用户视频
type userVideo struct {
	VideoId    int //视频ID
	PlayTimes  int //播放次数
	UpdateTime int //更新时间
	settles    []settleInfo
}

type settleInfo struct {
	ReturnAmount int //返还金额（用于游戏结算返还  >0 表示待返还）
	TimeStamp    int //时间戳
	GameId       int //游戏ID
}

// 游戏结算视频
type settleVideo struct {
	GameID     int // 游戏ID
	LoseAmount int // 输金额
	MaxTimes   int // 最大返还次数
	MaxRate    int // 最大返还比率
}

// 游戏视频返还信息
type settleVideoInfo struct {
	SettleId     int    // 结算Id
	GameID       int    // 游戏ID
	LoseAmount   int    // 输金币
	MaxTimes     int    // 最大次数
	SettleTimes  int    // 返还次数
	MaxAmount    int    // 最大金币
	SettleAmount int    // 返还金币
	Crdate       string // 时间
	TimeStamp    int    `json:"-"` // 时间戳
}

type awardRetInfo struct {
	RetCode int `json:"retCode"` // 操作结果
	Amount  int `json:"amount"`  // 领取金币
}

type settleInfo_resp struct {
	Success      bool // 成功标识
	TimeStamp    int  // 时间戳
	ReturnAmount int  // 总返还金币
	MaxTimes     int  // 最大次数
	SettleAmount int  // 返还金币
}
