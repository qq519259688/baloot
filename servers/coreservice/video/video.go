package video

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/serviceconfig"
	item "bet24.com/servers/micros/item_inventory/proto"
)

var mgr *videomgr

func Run() {
	mgr = newVideoMgr()
	log.Debug("video running")
}

// 播放
func Play(userId, videoId int) (bool, int, []item.ItemPack) {
	if serviceconfig.Server.IsCasual != 1 {
		return false, 0, []item.ItemPack{}
	}
	return mgr.play(userId, videoId)
}

// 播放信息
func GetInfo(userId, videoId int) (bool, int, int, []item.ItemPack) {
	if serviceconfig.Server.IsCasual != 1 {
		return false, 0, 0, []item.ItemPack{}
	}
	return mgr.getInfo(userId, videoId)
}

// 获取结算信息
func GetSettleInfo(userId, gameId, settleAmount int) *settleInfo_resp {
	if serviceconfig.Server.IsCasual != 1 {
		return &settleInfo_resp{}
	}
	return mgr.getSettleInfo(userId, gameId, settleAmount)
}

// 结算
func Settle(userId, timeStamp int) (bool, int) {
	if serviceconfig.Server.IsCasual != 1 {
		return false, 0
	}
	return mgr.settle(userId, timeStamp)
}

// 游戏返还视频列表
func GetGameSettleVideoList(userId int) []*settleVideoInfo {
	if serviceconfig.Server.IsCasual != 1 {
		return []*settleVideoInfo{}
	}
	return mgr.getGameSettleVideoList(userId)
}

// 游戏返还视频奖励
func AwardGameSettleVideo(userId, settleId int) *awardRetInfo {
	if serviceconfig.Server.IsCasual != 1 {
		return nil
	}
	return mgr.awardGameSettleVideo(userId, settleId)
}

// 检查小红点提醒
func CheckTip(userId int) bool {
	if serviceconfig.Server.IsCasual != 1 {
		return false
	}
	return mgr.checkTip(userId)
}

func AddUser(userId int) {
	mgr.onUserEnter(userId)
}

func RemoveUser(userId int) {
	mgr.onUserExit(userId)
}

func Dump(param1, param2 string) {
	switch param1 {
	case "sys":
		mgr.dumpSys(param2)
	case "user":
		mgr.dumpUser(param2)
	default:
		log.Debug("video.Dump unhandled %s:%s", param1, param2)
	}
}
