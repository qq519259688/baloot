package video

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 系统数值
func getSysVideoList() map[int]*video {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Video_GetList")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	list := make(map[int]*video, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out video

		out.VideoId = int((ret[0]).(int64))
		out.PlayTimes = int((ret[1]).(int64))
		awards := (ret[2]).(string)
		if len(awards) > 0 {
			if err := json.Unmarshal([]byte(awards), &out.Awards); err != nil {
				log.Error("video.getSysVideoList awards=%s err=%v", awards, err)
				return nil
			}
		}

		list[out.VideoId] = &out
	}
	return list
}

// 用户视频列表信息
func getUserVideoList(userId int) map[int]*userVideo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserVideo_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retrows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retrows)
	list := make(map[int]*userVideo)
	for i := 0; i < rowLen; i++ {
		ret := retrows[i]
		var out userVideo

		out.VideoId = int((ret[0]).(int64))
		out.PlayTimes = int((ret[1]).(int64))
		out.UpdateTime = int((ret[2]).(int64))

		list[out.VideoId] = &out
	}

	return list
}

// 更新用户视频
func updateUserVideo(userId int, userVideo *userVideo, isStat int, funcName string) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserVideo_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@VideoID", database.AdParamInput, database.AdInteger, 4, userVideo.VideoId)
	statement.AddParamter("@PlayTimes", database.AdParamInput, database.AdInteger, 4, userVideo.PlayTimes)
	statement.AddParamter("@UpdateTime", database.AdParamInput, database.AdInteger, 4, userVideo.UpdateTime)
	statement.AddParamter("@IsStat", database.AdParamInput, database.AdInteger, 4, isStat)
	sqlstring := statement.GenSql()
	//log.Debug("%s ==> %s", funcName, sqlstring)
	dbengine.CenterDB.ExecSql(sqlstring)
}

// 获取结算视频信息列表
func getSysSettleVideoList() []*settleVideo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SettleVideo_GetList")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*settleVideo
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out settleVideo
		out.GameID = int((ret[0]).(int64))
		out.LoseAmount = int((ret[1]).(int64))
		out.MaxTimes = int((ret[2]).(int64))
		out.MaxRate = int((ret[3]).(int64))

		list = append(list, &out)
	}

	return list
}

// 游戏返还视频列表
func getGameSettleVideoList(userId int) []*settleVideoInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*settleVideoInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserGameSettleVideo_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var out settleVideoInfo

			out.SettleId = int((ret[0]).(int64))
			out.GameID = int((ret[1]).(int64))
			out.LoseAmount = int((ret[2]).(int64))
			out.MaxTimes = int((ret[3]).(int64))
			out.SettleTimes = int((ret[4]).(int64))
			out.MaxAmount = int((ret[5]).(int64))
			out.SettleAmount = int((ret[6]).(int64))
			out.TimeStamp = int((ret[7]).(int64))
			out.Crdate = (ret[8]).(string)

			list = append(list, &out)
		}
	}

	return list
}

// 添加游戏返还视频记录
func addGameSettleVideo(userId int, info *settleVideoInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserGameSettleVideo_Add")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, info.GameID)
	statement.AddParamter("@LoseAmount", database.AdParamInput, database.AdBigint, 8, info.LoseAmount)
	statement.AddParamter("@MaxTimes", database.AdParamInput, database.AdInteger, 4, info.MaxTimes)
	statement.AddParamter("@SettleTimes", database.AdParamInput, database.AdInteger, 4, info.SettleTimes)
	statement.AddParamter("@MaxAmount", database.AdParamInput, database.AdBigint, 8, info.MaxAmount)
	statement.AddParamter("@SettleAmount", database.AdParamInput, database.AdBigint, 8, info.SettleAmount)
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdVarChar, 20, info.Crdate)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	info.SettleId = int((retRows[0][0]).(int64))
	return
}

// 游戏返还视频奖励
func awardGameSettleVideo(userId int, info *settleVideoInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserGameSettleVideo_Award")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SettleID", database.AdParamInput, database.AdInteger, 4, info.SettleId)
	statement.AddParamter("@SettleTimes", database.AdParamInput, database.AdInteger, 4, info.SettleTimes)
	statement.AddParamter("@SettleAmount", database.AdParamInput, database.AdBigint, 8, info.SettleAmount)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	dbengine.CenterDB.ExecSql(sqlstring)
	return
}
