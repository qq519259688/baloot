package subsidy

import (
	"bet24.com/log"
)

var mgr *subsidymgr

// 补助
type Subsidy struct {
	GiftTimes   int  //已领取次数
	Amount      int  //领取金币数
	CoolTime    int  // 冷却时间戳
	Crdate      int  //时间戳
	IsGift      bool //是否可以领取
	LowerAmount int  //低于金额触发
	MaxTimes    int  //总次数
}

func Run() {
	log.Debug("subsidy running")
	mgr = newSubsidyMgr()
}

// 获取补助信息
func GetSubsidyInfo(userId, lowerAmount int, maxTimes int) *Subsidy {
	return mgr.getInfo(userId, lowerAmount, maxTimes)
}

// 领取补助信息
func GiftSubsidy(userId, lowerAmount int, maxTime int, coolSeconds []int) (int, string) {
	return mgr.gift(userId, lowerAmount, maxTime, coolSeconds)
}

// 领取元宝补助信息
func GiftChipSubsidy(userId, lowerAmount int, ipAddress string) bool {
	return mgr.giftChip(userId, lowerAmount, ipAddress)
}

// 领取回归奖励
func GiftReturnAward(userId int) {
	mgr.giftReturnAward(userId)
}
