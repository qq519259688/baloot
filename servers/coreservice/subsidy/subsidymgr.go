package subsidy

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/serviceconfig"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	cash "bet24.com/servers/micros/money/proto"
	chip "bet24.com/servers/micros/money/proto"
	notification "bet24.com/servers/micros/notification/proto"
	userlabel "bet24.com/servers/micros/userlabel/proto"
	vipservice "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
)

type subsidymgr struct {
}

func newSubsidyMgr() *subsidymgr {
	log.Debug("subsidy manager running")
	return &subsidymgr{}
}

// 获取补助信息
func (this *subsidymgr) getInfo(userId, lowerAmount, maxTimes int) *Subsidy {
	info := getInfo(userId)
	info.LowerAmount = lowerAmount
	info.MaxTimes = maxTimes

	now := common.GetTimeStamp()

	//判断是否是同一天
	if !common.IsSameDay(info.Crdate, now) {
		info.GiftTimes = 0
		info.CoolTime = 0
	}

	//去数据库取金币
	_, amount := cash.GetMoney(userId)

	// 获取额外破产补助
	bankruptcy := vipservice.GetExtraBankruptcy(userId)
	//if bankruptcy > 0 {
	info.Amount = lowerAmount * (100 + bankruptcy) / 100
	//}

	if info.GiftTimes < info.MaxTimes && amount <= info.LowerAmount && info.CoolTime < common.GetTimeStamp() {
		info.IsGift = true

		// 6=任务类(任务模块)(用户标签)
		go userlabel.TriggerEvent(userId, userlabel.Type_Bankruptcy, userlabel.Scope{Num: 1})
	}
	log.Debug("subsidymgr.getInfo %d :%v", userId, info)
	return info
}

// 领取补助信息
func (this *subsidymgr) gift(userId, lowerAmount, maxTimes int, coolSeconds []int) (int, string) {
	//获取救济金信息
	info := this.getInfo(userId, lowerAmount, maxTimes)
	if info == nil {
		log.Debug("subsidy.gift userId=%d not exist", userId)
		return 0, "领取失败"
	}

	log.Debug("subsidymgr.gift userId=%d lowerAmount=%d maxTimes=%d coolSeconds=%+v info=%+v",
		userId, lowerAmount, maxTimes, coolSeconds, info)

	if !info.IsGift {
		log.Debug("subsidy.gift userId=%d info=%+v", userId, info)
		return 0, "领取失败"
	}

	var seconds int
	for i := 0; i < len(coolSeconds); i++ {
		seconds = coolSeconds[i]
		if i == info.GiftTimes {
			break
		}
	}

	// 冷却时间
	info.CoolTime = common.GetTimeStamp() + seconds

	//领取次数+1
	info.GiftTimes++
	info.Crdate = common.GetTimeStamp()

	//领取
	if ret := gift(userId, info.GiftTimes, info.Crdate, info.CoolTime); ret != 1 {
		log.Debug("subsidy.gift 领取救济金失败 userId=%d ret=%d", userId, ret)
		return 0, "领取失败"
	}

	var items []item.ItemPack
	items = append(items, item.ItemPack{
		ItemId: 1,
		Count:  info.Amount,
	})

	//给道具
	if success := inventory.AddItems(userId, items, "领取救济金", common.LOGTYPE_SUBSIDY_GIFT); !success {
		log.Debug("subsidy.gift 领取救济金失败 userId=%d items=%+v", userId, items)
		return 0, "领取失败"
	}

	return 1, "领取成功"
}

func (this *subsidymgr) giftChip(userId, lowerAmount int, ipAddress string) bool {
	// 是否有低保金额
	if lowerAmount <= 0 {
		return false
	}

	// 获取元宝数量
	if ok, chip := chip.GetUserChip(userId); ok {
		// 判断元宝数量
		if chip >= lowerAmount {
			return false
		}
	}

	// 判断是否可以领取元宝
	if ok := giftChip(userId, lowerAmount, ipAddress); !ok {
		return false
	}

	// 给元宝
	go func(userId, amount int) {
		var items []item.ItemPack
		items = append(items, item.ItemPack{
			ItemId: item.Item_Chip,
			Count:  amount,
		})

		// 给道具
		if success := inventory.AddItems(userId, items, "领取救济金", common.LOGTYPE_SUBSIDY_GIFT); !success {
			log.Debug("subsidy.giftChip 领取救济金失败 userId=%d items=%+v", userId, items)
		}
	}(userId, lowerAmount)

	return true
}

// 领取回归奖励
func (this *subsidymgr) giftReturnAward(userId int) {
	if serviceconfig.Server.ReturnAwards == "" {
		return
	}

	var items []item.ItemPack
	if err := json.Unmarshal([]byte(serviceconfig.Server.ReturnAwards), &items); err != nil {
		log.Error("subsidymgr.giftReturnAward err %v", err)
		return
	}

	// 给道具
	if success := inventory.AddItems(userId, items, "回归奖励", common.LOGTYPE_RETURN); !success {
		log.Debug("subsidymgr.giftReturnAward 回归奖励失败 userId=%d items=%+v", userId, items)
		return
	}

	go notification.AddNotification(userId, notification.Notification_Return, serviceconfig.Server.ReturnAwards)
	return
}
