package subsidy

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 获取补助信息
func getInfo(userId int) *Subsidy {
	defer func() {
		if err := recover(); err != nil {
			log.Error("subsidy.getInfo transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserSubsidy_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return &Subsidy{}
	}

	return &Subsidy{
		GiftTimes: int((retRows[0][0]).(int64)),
		Crdate:    int((retRows[0][1]).(int64)),
		CoolTime:  int((retRows[0][2]).(int64)),
	}
}

// 领取补助信息
func gift(userId, giftTimes, crdate, coolTime int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("subsidy.gift transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserSubsidy_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GiftTimes", database.AdParamInput, database.AdInteger, 4, giftTimes)
	statement.AddParamter("@CoolTime", database.AdParamInput, database.AdInteger, 4, coolTime)
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdInteger, 4, crdate)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return 0
	}

	return int((retRows[0][0]).(int64))
}

// 领取元宝补助
func giftChip(userId, lowerAmount int, ipAddress string) bool {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserChipSubsidy_Gift")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@LowerAmount", database.AdParamInput, database.AdBigint, 8, lowerAmount)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 20, ipAddress)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return false
	}

	return int((retRows[0][0]).(int64)) == 1
}
