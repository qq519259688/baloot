package limiteditems

import (
	"bet24.com/log"
)

func Run() {
	getManager()
}

func Dump(param1, param2 string) {
	getManager().dump(param1, param2)
}

// 添加道具，返回可添加数量
func AddItem(userId int, itemId int, count int) int {
	newCount := getManager().addItem(userId, itemId, count)
	if newCount != count {
		log.Debug("limiteditems.AddItem userId[%d] itemId[%d],count[%d],newCount[%d]", userId, itemId, count, newCount)
	}
	return newCount
}

func Flush() {
	getManager().flush()
}
