package limiteditems

import (
	"bet24.com/log"
	"bet24.com/redis"
	"encoding/json"
	"time"
)

const refresh_config_sec = 600

func getRedisKey() string {
	return "limiteditems:useritems"
}

func (m *manager) loadUserItemsFromRedis() {
	data, ok := redis.String_Get(getRedisKey())
	if data == "" || !ok {
		return
	}
	m.lock.Lock()
	err := json.Unmarshal([]byte(data), &m.userItems)
	m.lock.Unlock()
	if err != nil {
		log.Release("manager.loadUserItemsFromRedis Unmarshal failed err:%v,%s", err, data)
		return
	}
}

func (m *manager) flush() {
	time.AfterFunc(refresh_config_sec*time.Second, m.flush)
	m.lock.RLock()
	if !m.isDirty {
		m.lock.RUnlock()
		return
	}

	m.isDirty = false
	d, _ := json.Marshal(m.userItems)
	m.lock.RUnlock()
	go redis.String_Set(getRedisKey(), string(d))
}
