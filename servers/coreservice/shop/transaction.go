package shop

import (
	"encoding/json"
	"runtime/debug"
	"strconv"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 获取汇率
func getExchangeRate() []*exchangeRate {
	defer func() {
		if err := recover(); err != nil {
			log.Error("shop.getExchangeRate transaction recover %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Shop_GetExchangeRate")
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*exchangeRate
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out exchangeRate

		out.Currency = (ret[0]).(string)
		out.CountryName = (ret[1]).(string)
		out.CountryCode = (ret[2]).(string)
		rate := string((ret[3]).([]byte))
		out.Rate, _ = strconv.ParseFloat(rate, 64)
		list = append(list, &out)
	}

	return list
}

// 商店列表
func getShopList() []*Shop_Item {
	defer func() {
		if err := recover(); err != nil {
			log.Error("shop.getShopList transaction recover %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Shop_GetList")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	var shops []*Shop_Item

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out Shop_Item

		out.ProductId = (ret[0]).(string)
		out.ProductName = (ret[1]).(string)
		//out.Amount = int((ret[2]).(int64))

		price := string(ret[3].([]byte))
		out.Price, _ = strconv.ParseFloat(price, 64)

		out.PayType = int((ret[4]).(int64))
		out.IsHot = int((ret[5]).(int64))
		out.Bonus = int((ret[6]).(int64))
		extra := (ret[7]).(string)
		out.ShopType = int((ret[8]).(int64))

		showPrice := string(ret[9].([]byte))
		out.ShowPrice, _ = strconv.ParseFloat(showPrice, 64)
		out.Sort = int((ret[10]).(int64))
		out.AnimationType = int((ret[11]).(int64))
		out.UserType = int((ret[12]).(int64))
		out.IsBottom = int((ret[13]).(int64))
		out.ProductDesc = ret[14].(string)

		if len(extra) > 0 {
			if err := json.Unmarshal([]byte(extra), &out.Extra); err != nil {
				log.Error("shop.getShopList transaction json unmarshal error %v ==> %s", err, extra)
			}
		}

		shops = append(shops, &out)
	}

	return shops
}

type firstPurchase struct {
	productId string
	value     bool // true: 没有购买过,fase :已经购买过
}

func getFirstPurchase(userId int) []firstPurchase {
	defer func() {
		if err := recover(); err != nil {
			log.Error("shop.getShopList transaction recover %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Shop_GetFirstPurchase")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	var rets []firstPurchase

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out firstPurchase

		out.productId = (ret[0]).(string)
		out.value = (int((ret[1]).(int64)) > 0)

		rets = append(rets, out)
	}

	return rets
}

// 充值触发
func payTrigger(userId int, productId string, shopType, price int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Pay_Trigger")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ProductID", database.AdParamInput, database.AdVarChar, 32, productId)
	statement.AddParamter("@ShopType", database.AdParamInput, database.AdInteger, 4, shopType)
	statement.AddParamter("@Price", database.AdParamInput, database.AdInteger, 4, price)
	sqlstring := statement.GenSql()
	dbengine.CenterDB.ExecSql(sqlstring)
	return
}

type rechargeInfo struct {
	ProductId string
	ShopType  int
}

/*
// 充值列表
func getRechargeList(userId, shopType int) []*rechargeInfo {
	var ret []*rechargeInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Shop_GetRechargeList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ShopType", database.AdParamInput, database.AdInteger, 4, shopType)
	sqlString := statement.GenSql()
	jsonData := dbengine.CenterDB.ExecSqlJson(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("shop.transaction.getRechargeList json unmarshal err %v ==> %s", err, jsonData)
	}
	return ret
}
*/
