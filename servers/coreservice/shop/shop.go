package shop

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	ShopType_All              = iota
	ShopType_Gold             // 1 = 金币
	ShopType_Diamond          // 2 = 钻石
	ShopType_Item             // 3 = 消耗道具
	ShopType_VipPackage       // 4 = VIP礼包
	ShopType_RechargeGift     // 5 = 充值礼包
	ShopType_GrowthGift       // 6 = 成长礼包
	ShopType_MonthlyCard      // 7 = 月卡
	ShopType_VipCard          // 8 = vip会员卡（周卡、月卡）
	ShopType_FirstCharge      // 9 = 首充
	ShopType_Web              // 10 = 页面支付
	ShopType_Giving           // 11 = 语聊房礼物
	ShopType_LevelRewards     // 12 = 等级礼包
	ShopType_SavingPot        // 13=存钱罐
	ShopType_HighlyProfitable // 14=一本万利
	ShopType_Max
)

// 支付类型
const (
	PayType_Diamond int = iota // 0=钻石
	PayType_RMB                // 1=人民币购买
	PayType_Gold               // 2=金币购买
)

type Shop_Item struct {
	ProductId     string          // 产品ID
	ProductName   string          // 产品名称
	Price         float64         // 基础价格(实际支付价格)
	ShowPrice     float64         // 显示价格(显示价格)
	PayType       int             // 支付类型(1=RMB  0=钻石 2=金币)
	ShopType      int             // 产品类型(1=金币  2=钻石  3=炮台  4=礼包)
	IsHot         int             // 是否热销 1=热销
	Bonus         int             // 额外赠送(多送百分比)
	Extra         []item.ItemPack // 扩展信息
	Status        int             // 状态(1=正常  2=不可用或没有购买次数)
	Sort          int             // 排序
	AnimationType int             // 动画类型
	UserType      int             // 用户类型(0=普通  1=成员)
	IsBottom      int             // 是否置底
	ProductDesc   string          // 产品描述
}

// 汇率
type exchangeRate struct {
	Currency    string  // 币种
	CountryName string  // 国家名称
	CountryCode string  // 国家编码
	Rate        float64 // 汇率
}

var mgr *shopmgr

func Run() {
	log.Debug("shop running")
	mgr = newShopMgr()
}

/*func GetExchangeRateList() []*exchangeRate {
	return mgr.getExchangeRateList()
}
*/

func GetShopList(userId, shopType int, productID string) []*Shop_Item {
	if productID == "" {
		return mgr.getShopList(userId, shopType)
	}
	item := GetProduct(userId, shopType, productID)
	var ret []*Shop_Item
	if item != nil {
		ret = append(ret, item)
	}
	return ret
}

func GetShopListByUserId(shopType int, userId int, partnerId int) []*Shop_Item {
	list := mgr.getShopList(userId, shopType)
	// 苹果渠道,特殊处理
	// 100美元以下
	price := mgr.getUserPrice(userId, 400)
	log.Debug("GetShopListByUserId UserId[%d] PartnerId[%d] price[%.1f]", userId, partnerId, price)
	if partnerId >= 20000 && partnerId < 30000 {
		for i := 0; i < len(list); {
			if list[i].PayType != PayType_RMB {
				i++
				continue
			}

			if list[i].Price <= price {
				i++
				continue
			}
			log.Debug("GetShopListByUserId UserId[%d] PartnerId[%d] removing[%s] price[%.1f]", userId, partnerId, list[i].ProductId, list[i].Price)
			list = append(list[:i], list[i+1:]...)
		}
	}
	return list
}

func GetProduct(userId, shopType int, productId string) *Shop_Item {
	return mgr.getProduct(userId, productId)
}

func Exchange(userId int, productId string) (int, string) {
	return mgr.exchange(userId, productId)
}

func ExchangeInBulk(userId int, productIds []string) (int, string) {
	return mgr.exchangeInBulk(userId, productIds)
}

func Recharge(userId int, productId string) (int, string) {
	return mgr.recharge(userId, productId)
}

// 获取币种信息，传空信息的话，默认选择一个币种
/*
func ExchangeRateInfo(currency string) *exchangeRate {
	return mgr.exchangeRateInfo(currency)
}
*/

func Dump(param1, param2 string) {
	mgr.dump(param1, param2)
}

func GetUserPrice(userId int, price float64) float64 {
	return mgr.getUserPrice(userId, price)
}
