package slotscore

func Run() {
	createScoreManager()
}

func AddScore(userId int, score int) int {
	return mgr.addScore(userId, score)
}

func GetScore(userId int) int {
	return mgr.getScore(userId)
}

func GetExchangeList() string {
	return mgr.getExchangeList()
}

func Exchange(userId int, productId int) bool {
	return mgr.exchangeChip(userId, productId)
}

func GetExchangeHistory(userId int) string {
	return mgr.getExchangeHistory(userId)
}

func Dump(param1, param2 string) {
	if param1 == "" {
		mgr.dumpSys()
		return
	}
	mgr.dumpUser(param1)
}

func Flush() {
	mgr.doFlush()
}
