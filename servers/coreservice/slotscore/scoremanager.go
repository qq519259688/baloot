package slotscore

import (
	"fmt"
	"strconv"
	"sync"

	"bet24.com/log"
	"bet24.com/servers/common"
	chip "bet24.com/servers/micros/money/proto"
	notification "bet24.com/servers/micros/notification/proto"
)

var mgr *scoremanager

func createScoreManager() {
	mgr = new(scoremanager)
	mgr.ctor()
}

type scoremanager struct {
	exchange   *slotexchange
	lock       *sync.RWMutex
	userscores map[int]int
	isDirty    bool
	historyMgr *historymanager
}

func (sc *scoremanager) ctor() {
	sc.exchange = newSlotExchange()
	sc.lock = &sync.RWMutex{}
	sc.userscores = make(map[int]int)
	sc.loadUserScoreFromRedis()
	sc.flush()
	sc.historyMgr = newHistoryManager()
}

func (sc *scoremanager) getExchangeList() string {
	return sc.exchange.getList()
}

func (sc *scoremanager) addScore(userId int, score int) int {
	if !sc.exchange.isExchangeVaid() {
		return 0
	}
	sc.lock.Lock()
	sc.userscores[userId] += score
	ret := sc.userscores[userId]
	sc.isDirty = true
	sc.lock.Unlock()
	notification.AddNotification(userId, notification.Notification_SlotScore, fmt.Sprintf("%d", ret))
	return ret
}

func (sc *scoremanager) getScore(userId int) int {
	sc.lock.RLock()
	defer sc.lock.RUnlock()
	return sc.userscores[userId]
}

func (sc *scoremanager) reduceScore(userId int, score int) bool {
	sc.lock.Lock()
	defer sc.lock.Unlock()
	if sc.userscores[userId] < score {
		return false
	}
	sc.userscores[userId] -= score
	sc.isDirty = true
	return true
}

func (sc *scoremanager) exchangeChip(userId int, productId int) bool {
	item := sc.exchange.getItem(productId)
	if item == nil {
		log.Release("scoremanager.exchangeChip item[%d] not exist", productId)
		return false
	}
	if !sc.reduceScore(userId, item.Score) {
		log.Release("scoremanager.exchangeChip userId[%d],score[%d] need [%d]", userId, sc.getScore(userId), item.Score)
		return false
	}
	// 加元宝
	chip.GiveChip(userId, item.Chip, common.LOGTYPE_SLOT_SCORE, "slotscore", "slotexchange", "")
	notification.AddNotification(userId, notification.Notification_SlotScore, fmt.Sprintf("%d", sc.getScore(userId)))
	sc.historyMgr.addHistory(userId, item.Chip)
	return true
}

func (sc *scoremanager) dumpSys() {
	log.Release("-------------------------------")
	log.Release("scoremanager.dumpSys")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	log.Release("    %v", sc.getExchangeList())
}

func (sc *scoremanager) dumpUser(userIdStr string) {
	log.Release("-------------------------------")
	log.Release("scoremanager.dumpUser[%s]", userIdStr)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	userId, err := strconv.Atoi(userIdStr)
	if err != nil {
		log.Release("    atoi error %v", err)
		return
	}

	log.Release("    score:%d", sc.getScore(userId))
	log.Release("    history:%s", sc.getExchangeHistory(userId))
}

func (sc *scoremanager) getExchangeHistory(userId int) string {
	return sc.historyMgr.getHistory(userId)
}

func (sc *scoremanager) doFlush() {
	sc.historyMgr.flush()
	sc.flush()
}
