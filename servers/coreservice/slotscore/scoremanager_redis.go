package slotscore

import (
	"bet24.com/log"
	"bet24.com/redis"
	"encoding/json"
	"time"
)

func getRedisKey() string {
	return "slotscore:userscores"
}

func (sc *scoremanager) loadUserScoreFromRedis() {
	data, ok := redis.String_Get(getRedisKey())
	if data == "" || !ok {
		return
	}
	sc.lock.Lock()
	err := json.Unmarshal([]byte(data), &sc.userscores)
	sc.lock.Unlock()
	if err != nil {
		log.Release("scoremanager.loadUserScoreFromRedis Unmarshal failed err:%v,%s", err, data)
		return
	}
}

func (sc *scoremanager) flush() {
	time.AfterFunc(refresh_config_sec*time.Second, sc.flush)
	sc.lock.RLock()
	if !sc.isDirty {
		sc.lock.RUnlock()
		return
	}

	sc.isDirty = false
	d, _ := json.Marshal(sc.userscores)
	sc.lock.RUnlock()
	go redis.String_Set(getRedisKey(), string(d))
}
