package slotscore

import (
	"encoding/json"
	"os"
	"sync"
	"time"

	"bet24.com/log"
)

const refresh_config_sec = 600

type exchangeItem struct {
	ProductId int // 产品ID
	Score     int // 所需积分
	Chip      int // 兑换元宝数量
}

func newSlotExchange() *slotexchange {
	ret := new(slotexchange)
	ret.lock = &sync.RWMutex{}
	ret.initData()
	return ret
}

type slotexchange struct {
	items   []exchangeItem
	lock    *sync.RWMutex
	isValid bool
}

func (se *slotexchange) initData() {
	defer func() {
		time.AfterFunc(refresh_config_sec*time.Second, se.initData)
	}()
	data, err := os.ReadFile("slotexchange.json")
	if err != nil {
		log.Release("slotexchange.loadData read slotexchange failed")
		return
	}
	se.lock.Lock()
	defer se.lock.Unlock()
	err = json.Unmarshal(data, &se.items)
	if err != nil {
		log.Release("slotexchange.loadData Unmarshal slotexchange failed err:%v", err)
		return
	}
	se.isValid = true
}

func (se *slotexchange) isExchangeVaid() bool {
	return se.isValid
}

func (se *slotexchange) getItem(productId int) *exchangeItem {
	se.lock.RLock()
	defer se.lock.RUnlock()
	for _, v := range se.items {
		if v.ProductId == productId {
			return &v
		}
	}

	return nil
}

func (se *slotexchange) getList() string {
	se.lock.RLock()
	d, _ := json.Marshal(se.items)
	se.lock.RUnlock()
	return string(d)
}
