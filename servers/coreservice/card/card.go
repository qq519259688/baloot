package card

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	card_recharge     = "P"   // 充值卡，卡号P开头
	card_exchange     = "E"   // 兑换卡，卡号E开头
	card_exchange_vip = "VIP" // 兑换卡，vip
	card_match        = "M"   // 比赛卡，卡号M开头
)

type cardInfo struct {
	RetCode     int // 操作结果 1=操作成功  11=卡号无效  12=每5秒钟只能输入1次  13=5分钟内连续输错10次兑换码时，禁止使用此功能5分钟  14=卡号未生效  15=卡号已过期  16=次数已用完  17=已领取过
	Items       []item.ItemPack
	CoolSeconds int    // 冷却时间秒（禁用功能）
	TimeMemo    string // 时间描述
	SceneMemo   string // 场地描述
}

var mgr *cardMgr

func Run() {
	mgr = newCardMgr()
	log.Debug("card running")
}

// 使用充值卡
func Use(userId int, cardNo string) *cardInfo {
	return mgr.use(userId, cardNo)
}
