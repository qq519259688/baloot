package card

import (
	"encoding/json"
	"runtime/debug"

	item "bet24.com/servers/micros/item_inventory/proto"

	"bet24.com/servers/coreservice/dbengine"

	"bet24.com/database"
	"bet24.com/log"
)

// 使用充值卡
func useRechargeCard(userId int, cardNo string) *cardInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_RechargeCard_Use")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@CardNo", database.AdParamInput, database.AdVarChar, 32, cardNo)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	ret := retRows[0]
	retCode := int((ret[0]).(int64))
	goldAmount := int((ret[1]).(int64))
	coolSeconds := int((ret[2]).(int64))

	var items []item.ItemPack
	items = append(items, item.ItemPack{
		ItemId: item.Item_Gold,
		Count:  goldAmount,
	})

	return &cardInfo{
		RetCode:     retCode,
		Items:       items,
		CoolSeconds: coolSeconds,
	}
}

// 使用兑换卡
func useExchangeCard(userId int, cardNo string) *cardInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_ExchangeCard_Use")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@CardNo", database.AdParamInput, database.AdVarChar, 32, cardNo)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	ret := retRows[0]
	retCode := int((ret[0]).(int64))
	award := (ret[1]).(string)
	coolSeconds := int((ret[2]).(int64))

	var items []item.ItemPack
	if retCode == 1 && len(award) > 0 {
		if err := json.Unmarshal([]byte(award), &items); err != nil {
			log.Error("useExchangeCard json unmarshal error %v", err)
		}
	}

	return &cardInfo{
		RetCode:     retCode,
		Items:       items,
		CoolSeconds: coolSeconds,
	}
}

// 使用比赛卡
func useMatchCard(userId int, cardNo string) *cardInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_MatchCard_Use")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@CardNo", database.AdParamInput, database.AdVarChar, 32, cardNo)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	ret := retRows[0]
	retCode := int((ret[0]).(int64))
	coolSeconds := int((ret[1]).(int64))
	timeMemo := (ret[2]).(string)
	sceneMemo := (ret[3]).(string)

	return &cardInfo{
		RetCode:     retCode,
		CoolSeconds: coolSeconds,
		TimeMemo:    timeMemo,
		SceneMemo:   sceneMemo,
	}
}
