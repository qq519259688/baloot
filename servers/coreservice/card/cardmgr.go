package card

import (
	"strings"

	"bet24.com/log"
	"bet24.com/servers/common"
	inventory "bet24.com/servers/micros/item_inventory/proto"
)

type cardMgr struct {
}

func newCardMgr() *cardMgr {
	obj := &cardMgr{}
	log.Debug("card manager running")
	return obj
}

// 使用卡
func (this *cardMgr) use(userId int, cardNo string) *cardInfo {
	cardNo = strings.ToUpper(cardNo)

	switch {
	case strings.HasPrefix(cardNo, card_recharge): // 充值卡
		return this.useRechargeCard(userId, cardNo)
	case strings.HasPrefix(cardNo, card_exchange): // 兑换卡
		fallthrough
	case strings.HasPrefix(cardNo, card_exchange_vip): // 兑换卡，vip
		return this.useExchangeCard(userId, cardNo)
	case strings.HasPrefix(cardNo, card_match): // 比赛卡
		return this.useMatchCard(userId, cardNo)
	}
	return nil
}

// 使用充值卡
func (this *cardMgr) useRechargeCard(userId int, cardNo string) *cardInfo {
	resp := useRechargeCard(userId, cardNo)
	if resp.RetCode == 1 {
		// 加道具
		if success := inventory.AddItems(userId, resp.Items, "使用充值卡", common.LOGTYPE_RECHARGE_CARD); !success {
			log.Debug("useRechargeCard userId=%d cardNo=%s 使用充值卡加金币失败", userId, cardNo)
		}
	}
	return resp
}

// 使用兑换卡
func (this *cardMgr) useExchangeCard(userId int, cardNo string) *cardInfo {
	resp := useExchangeCard(userId, cardNo)
	if resp.RetCode == 1 {
		// 加道具
		if success := inventory.AddItems(userId, resp.Items, "使用兑换卡", common.LOGTYPE_EXCHANGE_CARD); !success {
			log.Debug("useExchangeCard userId=%d cardNo=%s 使用兑换卡加道具失败", userId, cardNo)
		}
	}
	return resp
}

// 使用比赛卡
func (this *cardMgr) useMatchCard(userId int, cardNo string) *cardInfo {
	return useMatchCard(userId, cardNo)
}
