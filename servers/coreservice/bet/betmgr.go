package bet

import (
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/common"
)

type betmgr struct {
}

func newBetMgr() *betmgr {
	log.Debug("bet manager running")
	return &betmgr{}
}

// 投注列表
func (bm *betmgr) betList(userId, gameId, days, pageIndex, pageSize int, betZone string) *BetInfo_Resp {
	return betList(userId, gameId, days, pageIndex, pageSize, betZone)
}

// 游戏历史
func (bm *betmgr) gameHistory(userId, pageIndex, pageSize int) (int, []*History) {
	return gameHistory(userId, pageIndex, pageSize)
}

// 游戏记录
func (bm *betmgr) getGameCount(userId, gameId int) []*GameCount {
	return getGameCount(userId, gameId)
}

// 更新domino游戏记录
func (bm *betmgr) updateDominoGameCount(userId, double, triple, quariter, qunitet int, cardData string) {
	//log.Debug("betmgr.updateDominoGameCount userId=%d double=%d triple=%d quariter=%d qunitet=%d cardData=%s",
	//	userId, double, triple, quariter, qunitet, cardData)

	list := bm.getGameCount(userId, common.Game_Domino)
	if len(list) <= 0 {
		return
	}

	var d dominoStat
	if len(list[0].CardStat) > 0 {
		if err := json.Unmarshal([]byte(list[0].CardStat), &d); err != nil {
			log.Error("betmgr.updateDominoGameCount json unmarshal fail %v ==> %s", err, list[0].CardStat)
		}
	}

	d.Double = d.Double + double
	d.Triple = d.Triple + triple
	d.Quariter = d.Quariter + quariter
	d.Qunitet = d.Qunitet + qunitet
	buf, _ := json.Marshal(d)
	updateCardStat(userId, common.Game_Domino, string(buf), cardData)
}

// 更新qiuqiu游戏记录
func (bm *betmgr) updateQiuqiuGameCount(userId, sixDevil, twinCards, smallCards, bigCards, qiuqiu int, cardData string) {
	//log.Debug("betmgr.updateQiuqiuGameCount userId=%d sixDeivl=%d twinCards=%d smallCards=%d bigCards=%d qiuqiu=%d cardData=%s",
	//	userId, sixDevil, twinCards, smallCards, bigCards, qiuqiu, cardData)

	list := bm.getGameCount(userId, common.Game_Qiuqiu)
	if len(list) <= 0 {
		return
	}

	var d qiuqiuStat
	if len(list[0].CardStat) > 0 {
		if err := json.Unmarshal([]byte(list[0].CardStat), &d); err != nil {
			log.Error("betmgr.updateQiuqiuGameCount json unmarshal fail %v ==> %s", err, list[0].CardStat)
		}
	}

	d.SixDevil = d.SixDevil + sixDevil
	d.TwinCards = d.TwinCards + twinCards
	d.SmallCards = d.SmallCards + smallCards
	d.BigCards = d.BigCards + bigCards
	d.QiuQiu = d.QiuQiu + qiuqiu
	buf, _ := json.Marshal(d)
	updateCardStat(userId, common.Game_Qiuqiu, string(buf), cardData)
}
