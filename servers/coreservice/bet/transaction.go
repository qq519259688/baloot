package bet

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 投注列表
func betList(userId, gameId, days, pageIndex, pageSize int, betZone string) *BetInfo_Resp {
	defer func() {
		if err := recover(); err != nil {
			log.Release("DoAction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	var resp BetInfo_Resp

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_BetRecord_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	statement.AddParamter("@Days", database.AdParamInput, database.AdInteger, 4, days)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@TotalBetAmount", database.AdParamOutput, database.AdBigint, 8, resp.TotalBetAmount)
	statement.AddParamter("@TotalResultAmount", database.AdParamOutput, database.AdBigint, 8, resp.TotalResultAmount)
	statement.AddParamter("@TotalWinAmount", database.AdParamOutput, database.AdBigint, 8, resp.TotalWinAmount)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, resp.RecordCount)
	statement.AddParamter("@BetZone", database.AdParamInput, database.AdVarChar, 128, betZone)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return &resp
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var bet BetInfo
			bet.ChineseName = (ret[0]).(string)
			bet.BetAmount = int((ret[1]).(int64))
			bet.BetTime = (ret[2]).(string)
			bet.ResultAmount = int((ret[3]).(int64))
			bet.Tax = int((ret[4]).(int64))
			bet.Status = int((ret[5]).(int64))
			bet.WinAmount = int((ret[6]).(int64))
			bet.EnglishName = (ret[7]).(string)
			if len(ret) > 8 {
				bet.BetZone = (ret[8]).(string)
				bet.ResultZone = (ret[9]).(string)
			}
			resp.List = append(resp.List, bet)
		}
	}

	resp.TotalBetAmount = int((retRows[rowLen-1][0]).(int64))
	resp.TotalResultAmount = int((retRows[rowLen-1][1]).(int64))
	resp.TotalWinAmount = int((retRows[rowLen-1][2]).(int64))
	resp.RecordCount = int((retRows[rowLen-1][3]).(int64))
	return &resp
}

// 游戏记录
func getGameCount(userId, gameId int) []*GameCount {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*GameCount
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserGameCount_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out GameCount
		out.GameID = int((ret[0]).(int64))
		out.TotalCount = int((ret[1]).(int64))
		out.WinCount = int((ret[2]).(int64))
		out.MaxWinAmount = int((ret[3]).(int64))
		out.CardStat = (ret[4]).(string)
		list = append(list, &out)
	}

	return list
}

// 更新牌型统计信息
func updateCardStat(userId, gameId int, cardStat, cardData string) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UserGameCount_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	statement.AddParamter("@CardStat", database.AdParamInput, database.AdVarChar, 1024, cardStat)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	dbengine.CenterDB.ExecSql(sqlstring)
}

// 游戏历史
func gameHistory(userId, pageIndex, pageSize int) (int, []*History) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("DoAction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	recordCount := 0
	var list []*History

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_BetRecord_GetHistoryList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return recordCount, list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var bet History
			bet.ChineseName = (ret[0]).(string)
			bet.BetTime = (ret[1]).(string)
			bet.SettleAmount = int((ret[2]).(int64))
			list = append(list, &bet)
		}
	}

	recordCount = int((retRows[rowLen-1][0]).(int64))
	return recordCount, list
}
