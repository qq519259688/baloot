package bet

const (
	TRANSTYPE_CASINO   = "Casino"
	TRANSTYPE_SPORTS   = "Sports"
	TRANSTYPE_VIRTUALS = "Virtuals"
)

type History struct {
	ChineseName  string // 游戏名称
	BetTime      string // 下注时间
	SettleAmount int    // 结算金额
}

type BetInfo struct {
	ChineseName  string //游戏名称
	BetAmount    int    //下注金额
	BetTime      string //下注时间
	ResultAmount int    //结算金额
	Tax          int    //台费
	Status       int    //状态 1=下注  2=结算  3=撤销
	WinAmount    int    // 输赢金额
	EnglishName  string
	BetZone      string // 下注区域
	ResultZone   string // 结算区域
}

type BetInfo_Resp struct {
	TotalBetAmount    int // 总投注额
	TotalResultAmount int // 总结算额
	TotalWinAmount    int // 总输赢
	RecordCount       int // 记录数
	List              []BetInfo
}

type GameCount struct {
	GameID       int    //游戏ID
	TotalCount   int    //总局数
	WinCount     int    //胜局数
	MaxWinAmount int    //最大赢金币数
	CardStat     string //牌型统计数据(json格式)
}

type dominoStat struct {
	Double   int
	Triple   int
	Quariter int
	Qunitet  int
}

type qiuqiuStat struct {
	SixDevil   int
	TwinCards  int
	SmallCards int
	BigCards   int
	QiuQiu     int
}

var mgr *betmgr

func Run() {
	mgr = newBetMgr()
}

// 投注列表
func BetList(userId, gameId, days, pageIndex, pageSize int, betZone string) *BetInfo_Resp {
	return mgr.betList(userId, gameId, days, pageIndex, pageSize, betZone)
}

// 游戏历史
func GameHistory(userId, pageIndex, pageSize int) (int, []*History) {
	return mgr.gameHistory(userId, pageIndex, pageSize)
}

// 游戏记录
func GetGameCount(userId, gameId int) []*GameCount {
	return mgr.getGameCount(userId, gameId)
}

// 更新domino游戏记录
func UpdateDominoGameCount(userId, double, triple, quariter, qunitet int, cardData string) {
	mgr.updateDominoGameCount(userId, double, triple, quariter, qunitet, cardData)
}

// 更新qiuqiu游戏记录
func UpdateQiuqiuGameCount(userId, sixDevil, twinCards, smallCards, bigCards, qiuqiu int, cardData string) {
	mgr.updateQiuqiuGameCount(userId, sixDevil, twinCards, smallCards, bigCards, qiuqiu, cardData)
}
