package newusergift

import (
	"runtime/debug"

	"bet24.com/servers/common"

	"encoding/json"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

func getConfig(isNewYear bool) []GiftConfig {
	defer func() {
		if err := recover(); err != nil {
			log.Release("newusergift.getConfig transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	if isNewYear {
		statement.SetProcName("WS_NewYearGift_GetConfig")
		//statement.SetProcName("WS_NewUserGift_GetConfig")
	} else {
		statement.SetProcName("WS_NewUserGift_GetConfig")
	}

	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	var rets []GiftConfig

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out GiftConfig

		out.DayIndex = int((ret[0]).(int64))
		gifts := (ret[1]).(string)
		if gifts != "" {
			if err := json.Unmarshal([]byte(gifts), &out.Items); err != nil {
				log.Error("newusergift.getConfig unmarshal err %v", err)
				return nil
			}
		}

		rets = append(rets, out)
	}

	return rets
}

func getUserGiftInfo(userId int, isNewYear bool) []UserGift {
	defer func() {
		if err := recover(); err != nil {
			log.Release("newusergift.getUserGiftInfo transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	if isNewYear {
		//statement.SetProcName("WS_NewUserGift_GetUserGift")
		statement.SetProcName("WS_NewYearGift_GetUserGift")
	} else {
		statement.SetProcName("WS_NewUserGift_GetUserGift")
	}
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	var rets []UserGift

	dayIndex := common.GetDayIndex(common.GetTimeStamp())

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out UserGift

		out.DayIndex = int((ret[0]).(int64))
		out.Status = int((ret[1]).(int64))
		crdateStr := (ret[2]).(string)
		crdate := common.ParseTime(crdateStr)
		giftDayIndex := common.GetDayIndex(common.GetStamp(crdate))

		// 判断今天是否可领取
		if out.Status == 0 {
			// 可领取
			if giftDayIndex == dayIndex {
				out.Status = Receiving
			} else if giftDayIndex < dayIndex { // 过期
				out.Status = Overdue
			}
		}

		rets = append(rets, out)
	}

	return rets
}

func userReceiveGift(userId int, dayIndex int, isNewYear bool) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("newusergift.getUserGiftInfo transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	if isNewYear {
		statement.SetProcName("WS_NewYearGift_Receive")
	} else {
		statement.SetProcName("WS_NewUserGift_Receive")
	}
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@DayIndex", database.AdParamInput, database.AdInteger, 4, dayIndex)
	sqlstring := statement.GenSql()
	dbengine.CenterDB.ExecSql(sqlstring)
}
