package newusergift

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/serviceconfig"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
)

type NewusergiftManager struct {
	configs        []GiftConfig
	newYearConfigs []GiftConfig
}

func newNewusergiftManager() *NewusergiftManager {
	obj := new(NewusergiftManager)
	obj.loadConfig()
	log.Debug("newusergift manager running")
	return obj
}

func (this *NewusergiftManager) loadConfig() {
	this.configs = getConfig(false)
	this.newYearConfigs = getConfig(true)
}

func (this *NewusergiftManager) getUserGiftInfo(userId int, isNewYear bool) []UserGift {
	ret := getUserGiftInfo(userId, isNewYear)
	receivingDayIndex := -1
	var items []item.ItemPack
	for i := 0; i < len(ret); i++ {
		config := this.getConfig(ret[i].DayIndex, isNewYear)
		if config == nil {
			log.Debug("NewusergiftManager.getUserGiftInfo config not found %d", ret[i].DayIndex)
			continue
		}

		ret[i].Items = config.Items

		// 是否休闲平台
		if serviceconfig.Server.IsCasual == 1 {
			continue
		}

		if ret[i].Status == Receiving {
			receivingDayIndex = ret[i].DayIndex
			items = ret[i].Items
		}
	}

	// 能领取才给客户端
	if receivingDayIndex != -1 {
		userReceiveGift(userId, receivingDayIndex, isNewYear)
		desc := "NewUserGift"
		if isNewYear {
			desc = "NewYearGift"
		}
		inventory.AddItems(userId, items, desc, common.LOGTYPE_TASK_AWARD)
	}
	return ret
}

func (this *NewusergiftManager) receiveGift(userId, isDouble int, isNewYear bool) (int, []item.ItemPack) {
	var retCode int
	var items []item.ItemPack

	// 判断是否是休闲平台
	if serviceconfig.Server.IsCasual != 1 {
		return retCode, items
	}

	if isDouble != 0 && isDouble != 1 {
		return retCode, items
	}

	ret := getUserGiftInfo(userId, isNewYear)
	receivingDayIndex := -1
	for i := 0; i < len(ret); i++ {
		config := this.getConfig(ret[i].DayIndex, isNewYear)
		if config == nil {
			log.Debug("NewusergiftManager.receiveGift config not found %d", ret[i].DayIndex)
			continue
		}

		ret[i].Items = config.Items

		if ret[i].Status == Receiving {
			receivingDayIndex = ret[i].DayIndex
			// items = ret[i].Items
			for _, v := range ret[i].Items {
				items = append(items, item.ItemPack{
					ItemId: v.ItemId,
					Count:  v.Count * (1 + isDouble), // 是否双倍
				})
			}
		}
	}

	// 能领取才给客户端
	if receivingDayIndex != -1 {
		userReceiveGift(userId, receivingDayIndex, isNewYear)
		desc := "NewUserGift"
		if isNewYear {
			desc = "NewYearGift"
		}
		inventory.AddItems(userId, items, desc, common.LOGTYPE_TASK_AWARD)
		retCode = 1
	}

	return retCode, items
}

func (this *NewusergiftManager) getConfig(dayIndex int, isNewYear bool) *GiftConfig {
	if !isNewYear {
		for _, v := range this.configs {
			if v.DayIndex == dayIndex {
				return &v
			}
		}
	} else {
		for _, v := range this.newYearConfigs {
			if v.DayIndex == dayIndex {
				return &v
			}
		}
	}

	return nil
}
