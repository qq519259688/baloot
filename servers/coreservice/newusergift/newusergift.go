package newusergift

import (
	"encoding/json"

	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	Invalid   = iota // 0=不可领
	Received         // 1=已领取
	Receiving        // 2=当前可领取
	Overdue          // 3=过期
)

type GiftConfig struct {
	DayIndex int
	Items    []item.ItemPack
}

type UserGift struct {
	GiftConfig
	Status int
}

var mgr *NewusergiftManager

func Run() {
	log.Debug("newusergift running")
	mgr = newNewusergiftManager()
}

func GetNewUserGiftInfo(userId int) string {
	ret := mgr.getUserGiftInfo(userId, false)
	if len(ret) == 0 {
		return ""
	}
	d, _ := json.Marshal(ret)
	return string(d)
}

func ReceiveNewUserGift(userId, isDouble int) string {
	retCode, items := mgr.receiveGift(userId, isDouble, false)
	d, _ := json.Marshal(struct {
		RetCode int
		Items   []item.ItemPack
	}{
		RetCode: retCode,
		Items:   items,
	})
	return string(d)
}

func GetNewYearGiftInfo(userId int) string {
	ret := mgr.getUserGiftInfo(userId, true)
	if len(ret) == 0 {
		return ""
	}
	d, _ := json.Marshal(ret)
	return string(d)
}

func ReceiveNewYearGift(userId int, isDouble int) string {
	retCode, items := mgr.receiveGift(userId, isDouble, true)
	d, _ := json.Marshal(struct {
		RetCode int
		Items   []item.ItemPack
	}{
		RetCode: retCode,
		Items:   items,
	})
	return string(d)
}
