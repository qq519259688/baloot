package redpoint

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/friend"
	"bet24.com/servers/coreservice/monthlycard"
	"bet24.com/servers/coreservice/prizewheel"
	"bet24.com/servers/coreservice/purchase"
	_ "bet24.com/servers/coreservice/signin"
	"bet24.com/servers/coreservice/video"
	activityservice "bet24.com/servers/micros/activityservice/proto"
	notification "bet24.com/servers/micros/notification/proto"
	task "bet24.com/servers/micros/task/proto"
	userservice "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"time"
)

type user_redPoint struct {
	userId   int
	counter  int  //计数器
	isNotify bool // 是否发送通知
	endChan  chan int
	redPoint //小红点提醒
}

func newUserRedpoint(userId int) *user_redPoint {
	if userservice.IsRobot(userId) {
		log.Release("newUserRedpoint %d is IsRobot", userId)
		return nil
	}
	ret := new(user_redPoint)
	ret.userId = userId
	ret.endChan = make(chan int)

	go func() {
		time.AfterFunc(time.Second*1, ret.doCheck)
	}()

	go func() {
		for {
			select {
			case <-ret.endChan:
				log.Debug("user_redPoint doCheck for stop userId=%d", ret.userId)
				return
			case <-time.After(time.Second * 5): //每5秒钟
				ret.doCheck()
			}
		}
	}()

	return ret
}

func (this *user_redPoint) check() *redPoint {
	info := load(this.userId)
	return &redPoint{
		pointInfo: *info,
	}
}

func (this *user_redPoint) doCheck() {
	this.checkTask()           // 任务
	this.checkSign()           // 签到
	this.checkVideoSign()      // 视频广告
	this.checkPrizeWheel()     // 彩票
	this.checkPurchase()       // 100K购
	this.checkNoviceWelfare()  // 新手福利
	this.checkLevelRewards()   // 等级礼包
	this.checkDailyWheelFree() // 免费转盘

	// 频率较低的数据，计数器 24 * 5 = 120(1分钟)
	if this.counter%24 == 0 {
		// log.Debug("user_redPoint doCheckDB userId=%d counter=%d", this.userId, this.counter)
		this.checkMonth()           // 月卡
		this.checkFriend()          // 好友
		this.checkGameSettleVideo() // 游戏返还视频

		// 从数据库取数据
		info := load(this.userId)

		// 邮件（含附件）
		if this.MailTip != info.MailTip {
			this.MailTip = info.MailTip
			this.isNotify = this.isNotify || this.MailTip
		}

		// 重要邮件（含附件）
		if this.MailVipTip != info.MailVipTip {
			this.MailVipTip = info.MailVipTip
			this.isNotify = this.isNotify || this.MailVipTip
		}

		// 客服留言
		if this.MsgTip != info.MsgTip {
			this.MsgTip = info.MsgTip
			this.isNotify = this.isNotify || this.MsgTip
		}

		// 邀请(推广)
		if this.SpreadTip != info.SpreadTip {
			this.SpreadTip = info.SpreadTip
			this.isNotify = this.isNotify || this.SpreadTip
		}

		// 五星好评
		if this.ReviewTip != info.ReviewTip {
			this.ReviewTip = info.ReviewTip
			this.isNotify = this.isNotify || this.ReviewTip
		}

		// 代理升级提醒
		if this.AgentUpTip != info.AgentUpTip {
			this.AgentUpTip = info.AgentUpTip
			this.isNotify = this.isNotify || this.AgentUpTip
		}
	}

	// 计数器+1
	this.counter++

	// 发送通知
	this.notify()
}

// 游戏返还视频
func (this *user_redPoint) checkGameSettleVideo() {
	gameVideoTip := video.CheckTip(this.userId)
	if this.GameVideoTip != gameVideoTip {
		this.GameVideoTip = gameVideoTip
		this.isNotify = this.isNotify || this.GameVideoTip
	}
}

// 彩票
func (this *user_redPoint) checkPrizeWheel() {
	var prizeWheelTip bool // 彩票
	if times, maxTimes, _ := prizewheel.GetWheelTimes(this.userId); times < maxTimes {
		prizeWheelTip = true
	}
	if this.PrizeWheelTip != prizeWheelTip {
		this.PrizeWheelTip = prizeWheelTip
		this.isNotify = this.isNotify || this.PrizeWheelTip
	}
}

// 视频广告签到
func (this *user_redPoint) checkVideoSign() {
	var videoSignTip bool //签到视频提示
	videoSignTip, _, _, _ = video.GetInfo(this.userId, video.Video_Sign)
	if this.VideoSignTip != videoSignTip {
		this.VideoSignTip = videoSignTip
		this.isNotify = this.isNotify || this.VideoSignTip
	}
}

// 任务
func (this *user_redPoint) checkTask() {
	scenes := task.GetTaskTipScene(this.userId)
	taskTip := scenes > 0 && scenes != this.TaskScenes

	log.Debug("user_redpoint.checkTask userId=%d scenes=%d this.TaskScenes=%d taskTip=%v this.TaskTip=%v this.isNotify=%v",
		this.userId, scenes, this.TaskScenes, taskTip, this.TaskTip, this.isNotify)

	if this.TaskTip != taskTip {
		this.TaskTip = taskTip
		this.isNotify = this.isNotify || this.TaskTip
	}

	if scenes != this.TaskScenes {
		this.TaskScenes = scenes
	}
}

// 签到
func (this *user_redPoint) checkSign() {
	signTip := activityservice.CheckUserSignTip(this.userId)
	if this.SignTip != signTip {
		this.SignTip = signTip
		this.isNotify = this.isNotify || this.SignTip
	}
}

// 月卡
func (this *user_redPoint) checkMonth() {
	monthTip := monthlycard.CheckMonthTip(this.userId)
	if this.MonthTip != monthTip {
		this.MonthTip = monthTip
		this.isNotify = this.isNotify || this.MonthTip
	}
}

// 好友
func (this *user_redPoint) checkFriend() {
	friendTip := false
	list := friend.FriendVerifyList(this.userId)
	if len(list) > 0 {
		friendTip = true
	}

	if !friendTip {
		fs := friend.GetFriendList(this.userId)
		for _, v := range fs {
			if v.IsGift == friend.GIFT_STATUS_HAVE {
				friendTip = true
				break
			}
		}
	}

	if this.FriendTip != friendTip {
		this.FriendTip = friendTip
		this.isNotify = this.isNotify || this.FriendTip
	}
}

// 100K购
func (this *user_redPoint) checkPurchase() {
	purchaseTip := purchase.CheckTip()
	if this.PurchaseTip != purchaseTip {
		this.PurchaseTip = purchaseTip
		this.isNotify = this.isNotify || this.PurchaseTip
	}
}

// 新手福利
func (this *user_redPoint) checkNoviceWelfare() {
	noviceWelfareTip := activityservice.IsNoviceWelfareTip(this.userId)
	if this.NoviceWelfareTip != noviceWelfareTip {
		this.NoviceWelfareTip = noviceWelfareTip
		this.isNotify = this.isNotify || this.NoviceWelfareTip
	}
}

// 等级礼包
func (this *user_redPoint) checkLevelRewards() {
	levelRewardsTip := activityservice.IsLevelRewardsTip(this.userId)
	if this.LevelRewardsTip != levelRewardsTip {
		this.LevelRewardsTip = levelRewardsTip
		this.isNotify = this.isNotify || this.LevelRewardsTip
	}
}

// 等级礼包
func (this *user_redPoint) checkDailyWheelFree() {
	tip := activityservice.IsDailyWheelFree(this.userId)
	if this.DailyWheelFreeTip != tip {
		this.DailyWheelFreeTip = tip
		this.isNotify = this.isNotify || this.DailyWheelFreeTip
	}
}

func (this *user_redPoint) notify() {
	if !this.isNotify {
		return
	}

	this.isNotify = false

	// 有通知需要发送
	d, _ := json.Marshal(this)

	//log.Debug("user_redpoint.notify userId=%d ==> %s", this.userId, string(d))
	go notification.AddNotification(this.userId, notification.Notification_Redpoint, string(d))
}

func (this *user_redPoint) destructor() {
	log.Debug("user_redPoint.destructor %d", this.userId)
	close(this.endChan)
}
