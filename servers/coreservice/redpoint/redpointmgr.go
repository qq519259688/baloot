package redpoint

import (
	"bet24.com/log"
	"sync"
)

type redpointmgr struct {
	lock      *sync.RWMutex
	user_list map[int]*user_redPoint
}

func newRedpointMgr() *redpointmgr {
	ret := new(redpointmgr)
	ret.lock = &sync.RWMutex{}
	ret.user_list = make(map[int]*user_redPoint)
	log.Debug("redpoint manager running")
	return ret
}

func (this *redpointmgr) check(userId int) *redPoint {
	user := this.getUser(userId)
	if user == nil {
		log.Debug("redpointmgr.check userId[%d] is not exist", userId)
		return nil
	}
	return user.check()
}

func (this *redpointmgr) getUser(userId int) *user_redPoint {
	this.lock.RLock()
	user, ok := this.user_list[userId]
	this.lock.RUnlock()
	if !ok {
		user = newUserRedpoint(userId)
		if user == nil {
			return nil
		}
		this.lock.Lock()
		this.user_list[userId] = user
		this.lock.Unlock()
	}
	return user
}

func (this *redpointmgr) onUserEnter(userId int) {
	this.lock.Lock()
	defer this.lock.Unlock()
	_, ok := this.user_list[userId]
	if !ok {
		this.user_list[userId] = newUserRedpoint(userId)
	}
}

func (this *redpointmgr) onUserExit(userId int) {
	this.lock.Lock()
	defer this.lock.Unlock()
	u, _ := this.user_list[userId]
	if u != nil {
		u.destructor()
		delete(this.user_list, userId)
	}
}

func (this *redpointmgr) dump(param string) {
	log.Release("-------------------------------")
	log.Release("redpointmgr.dump")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	this.lock.RLock()
	log.Release("  total user count[%d]", len(this.user_list))
	for k, v := range this.user_list {
		log.Release("    %d,userId[%d],counter[%d]", k, v.userId, v.counter)
	}
	this.lock.RUnlock()
}
