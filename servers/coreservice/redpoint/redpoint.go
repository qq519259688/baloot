package redpoint

import "bet24.com/log"

// 客服、签到、月卡、彩票、邀请、任务、好友
type redPoint struct {
	MonthTip          bool // 月卡
	TaskTip           bool // 任务
	TaskScenes        int  // 任务场景
	FriendTip         bool // 好友
	VideoSignTip      bool // 视频广告(大厅)
	PrizeWheelTip     bool // 彩票
	GameVideoTip      bool // 游戏返还视频
	PurchaseTip       bool // 100K购提醒
	NoviceWelfareTip  bool // 新手福利提醒
	LevelRewardsTip   bool // 等级奖励提醒
	DailyWheelFreeTip bool // 每日转盘免费

	pointInfo // 提醒
}

// 从数据库取数据
type pointInfo struct {
	SignTip    bool // 签到
	MailTip    bool // 邮件(含附件)
	MailVipTip bool // 重要邮件(含附件)
	MsgTip     bool // 客服留言
	SpreadTip  bool // 邀请
	ReviewTip  bool // 五星好评
	AgentUpTip bool // 代理升级提示
}

var mgr *redpointmgr

func Run() {
	log.Debug("redpoint running")
	mgr = newRedpointMgr()
}

func AddUser(userId int) {
	mgr.onUserEnter(userId)
}

func RemoveUser(userId int) {
	mgr.onUserExit(userId)
}

func CheckRedPoint(userId int) *redPoint {
	return mgr.check(userId)
}

func Dump(param string) {
	mgr.dump(param)
}
