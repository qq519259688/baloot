package redpoint

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/dbengine"
	"runtime/debug"
)

// 获取小红点数据
func load(userId int) *pointInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	info := new(pointInfo)
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_RedPoint_Check")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return info
	}

	ret := retRows[0]
	crdateStr := (ret[0]).(string)
	crdate := common.ParseTime(crdateStr)
	info.SignTip = !common.IsSameDay(common.GetStamp(crdate), common.GetTimeStamp())
	info.MailTip = (ret[1]).(int64) == 1
	info.MsgTip = (ret[2]).(int64) == 1
	info.SpreadTip = (ret[3]).(int64) == 1
	info.ReviewTip = (ret[4]).(int64) == 1
	info.AgentUpTip = (ret[5]).(int64) > 0
	if len(ret) > 6 {
		info.MailVipTip = (ret[6]).(int64) == 1
	}
	return info
}
