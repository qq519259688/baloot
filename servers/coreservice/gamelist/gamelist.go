package gamelist

import (
	"bet24.com/log"
	"bet24.com/servers/transaction"
)

var list *gamelistMgr

func Run() {
	log.Debug("gamelist running")
	list = newGamelistMgr()
}

func GetGame(gameId int) *transaction.GameInfo {
	return list.getGame(gameId)
}

func Dump() {
	list.dump()
}
