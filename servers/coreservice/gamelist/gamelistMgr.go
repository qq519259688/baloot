package gamelist

import (
	"bet24.com/log"
	"bet24.com/servers/transaction"
	"sync"
	"time"
)

func newGamelistMgr() *gamelistMgr {
	obj := new(gamelistMgr)
	obj.lock = &sync.RWMutex{}
	obj.refreshData()
	return obj
}

type gamelistMgr struct {
	games []transaction.GameInfo
	lock  *sync.RWMutex
}

func (g *gamelistMgr) refreshData() {
	go g.doRefresh()
	time.AfterFunc(5*time.Minute, g.refreshData)
}

func (g *gamelistMgr) doRefresh() {
	gameList := transaction.GetGameListFromDB(false)
	g.lock.Lock()
	defer g.lock.Unlock()
	g.games = gameList
}

func (g *gamelistMgr) getGame(gameId int) *transaction.GameInfo {
	g.lock.RLock()
	defer g.lock.RUnlock()
	for _, v := range g.games {
		if v.GameID == gameId {
			return &v
		}
	}
	return nil
}

func (g *gamelistMgr) dump() {
	log.Release("-------------------------------")
	log.Release("gamelistMgr.dump")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	g.lock.RLock()
	g.lock.RUnlock()
	for _, v := range g.games {
		log.Release("    %v", v)
	}
}
