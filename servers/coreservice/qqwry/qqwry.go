package qqwry

import (
	"flag"
	"strings"
	"time"

	"bet24.com/log"
	//"bet24.com/servers/coreservice/shop"
)

var mgr QQwry

func Run() {
	datFile := flag.String("qqwry", "qqwry.dat", "纯真 IP 库的地址")
	flag.Parse()

	IPData.FilePath = *datFile

	startTime := time.Now().UnixNano()
	res := IPData.InitIPData()

	if v, ok := res.(error); ok {
		log.Debug("%v", v)
	}

	mgr = NewQQwry()

	endTime := time.Now().UnixNano()

	log.Release("IP 库加载完成 共加载:%d 条 IP 记录, 耗时:%.1f ms\n", IPData.IPNum, float64(endTime-startTime)/1000000)
}

// findIP 查找 IP 地址的接口
func FindIP(ip string) *ResultQQwry {

	if ip == "" {
		log.Error("请填写 IP 地址")
		return nil
	}

	rs := mgr.Find(ip)
	return &rs
}

// findIP 查找 IP 地址的接口
func FindIPMore(ip string) []ResultQQwry {
	var rs []ResultQQwry

	if ip == "" {
		log.Error("请填写 IP 地址")
		return rs
	}

	ips := strings.Split(ip, ",")

	if len(ips) > 0 {
		for _, v := range ips {
			rs = append(rs, mgr.Find(v))
		}
	}

	log.Debug("%v", rs)
	return rs
}

// 根据ip获取币种
/*
func GetCurrency(ip string) string {
	currency := "SAR"

	if ip == "" || ip == "127.0.0.1" {
		return currency
	}

	info := FindIP(ip)
	if info == nil {
		return currency
	}

	list := shop.GetExchangeRateList()
	for _, v := range list {
		if strings.Contains(info.Country, v.CountryName) {
			return v.Currency
		}
	}

	return currency
}
*/
