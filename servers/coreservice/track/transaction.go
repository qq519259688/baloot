package track

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 记录
func record(userId int, level_1, level_2, level_3, videoName string, amount int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UserTrack_Record")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Level_1", database.AdParamInput, database.AdNVarChar, 32, level_1)
	statement.AddParamter("@Level_2", database.AdParamInput, database.AdNVarChar, 32, level_2)
	statement.AddParamter("@Level_3", database.AdParamInput, database.AdNVarChar, 32, level_3)
	statement.AddParamter("@VideoName", database.AdParamInput, database.AdNVarChar, 32, videoName)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdBigint, 8, amount)
	sqlstring := statement.GenSql()
	dbengine.CenterDB.ExecSql(sqlstring)
	return
}
