package track

import (
	"strconv"
	"strings"

	"bet24.com/log"
)

type trackmgr struct {
}

func newTrackMgr() *trackmgr {
	log.Debug("track manager running")
	return &trackmgr{}
}

// 记录
func (this *trackmgr) record(userId int, level_1, level_2, level_3 string) {

	var (
		videoName string
		amount    int
		err       error
	)

	level := level_3

	//广告处理
	if level_2 == "请求广告" || level_2 == "请求游戏广告" {

		if level_3 != "" {

			list := strings.Split(level_3, "@")

			log.Debug("track.trackmgr record userId=%d level_1=%s level_2=%s level_3=%s list=%+v",
				userId, level_1, level_2, level_3, list)

			for idx, _ := range list {
				switch idx {
				case 0:
					level = list[idx]
				case 1:
					videoName = list[idx]
				case 2:
					if amount, err = strconv.Atoi(list[2]); err != nil {
						log.Error("track.trackmgr record userId=%d level_1=%s level_2=%s level_3=%s err=%+v",
							userId, level_1, level_2, level_3, err)
					}
				}
			}
		}
	}

	go record(userId, level_1, level_2, level, videoName, amount)
}
