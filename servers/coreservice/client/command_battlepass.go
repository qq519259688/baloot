package client

import (
	"encoding/json"

	_ "bet24.com/log"
)

// 成长礼包
func GetBattlePassPacks() Response {
	msg := "GetBattlePassPacks"
	return DoRequest(msg, "")
}

func GetUserBattlePass(userId int) Response {
	msg := "GetUserBattlePass"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func UserAwardBattlePass(userId, packId, index int) Response {
	msg := "UserAwardBattlePass"
	var req GrowthPackAward_req
	req.UserId = userId
	req.GrowthPackId = packId
	req.Index = index
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func AddUserBattlePassExp(userId, exp int) Response {
	msg := "AddUserBattlePassExp"
	var req BattlePassAddExp_req
	req.UserId = userId
	req.Exp = exp
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
