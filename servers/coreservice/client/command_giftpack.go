package client

import (
	"encoding/json"

	_ "bet24.com/log"
)

func GetGiftPacks() Response {
	msg := "GetGiftPacks"
	return DoRequest(msg, "")
}

func GetBuyableGiftPacks(userId int) Response {
	msg := "GetBuyableGiftPacks"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 成长礼包
func GetGrowthGiftPacks() Response {
	msg := "GetGrowthGiftPacks"
	return DoRequest(msg, "")
}

func GetUserGrowthTerms(userId int) Response {
	msg := "GetUserGrowthTerms"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func UserUnlockBullet(userId, bullet int) Response {
	msg := "UserUnlockBullet"
	var req GrowthPackUnlock_req
	req.UserId = userId
	req.Bullet = bullet
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func UserAwardTerm(userId, packId, index int) Response {
	msg := "UserAwardTerm"
	var req GrowthPackAward_req
	req.UserId = userId
	req.GrowthPackId = packId
	req.Index = index
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
