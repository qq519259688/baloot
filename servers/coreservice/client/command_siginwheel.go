package client

import (
	"encoding/json"
)

func GetSigninWheelInfo(userId int) string {
	msg := "GetSigninWheelInfo"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.Data
}

func DoSigninWheel(userId int, ipAddress string, userGold int) string {
	msg := "DoSigninWheel"
	var req Request_SigninWheel
	req.UserId = userId
	req.IpAddress = ipAddress
	req.UserGold = userGold
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.Data
}

func GetSigninWheelHistory() string {
	msg := "GetSigninWheelHistory"
	resp := DoRequest(msg, "")
	return resp.Data
}
