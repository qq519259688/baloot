package client

import (
	"encoding/json"
)

// 记录
func TrackRecord(userId int, level_1, level_2, level_3 string) Response {
	msg := "TrackRecord"
	var req TrackRecord_req
	req.UserId = userId
	req.Level_1 = level_1
	req.Level_2 = level_2
	req.Level_3 = level_3
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
