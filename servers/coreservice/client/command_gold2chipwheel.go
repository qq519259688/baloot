package client

import (
	"encoding/json"
)

func ChipWheel(userId, bet int, ipAddress string) int {
	msg := "ChipWheel"
	var req SlotScore_req
	req.UserId = userId
	req.Param = bet
	req.IpAddress = ipAddress
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.RetCode
}

func GetChipWheelConfig() string {
	msg := "GetChipWheelConfig"
	resp := DoRequest(msg, "")
	return resp.Data
}

func GetChipWheelHistory(userId int) string {
	msg := "GetChipWheelHistory"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.Data
}
