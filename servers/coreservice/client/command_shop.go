package client

import (
	"encoding/json"

	_ "bet24.com/log"
)

func GetExchangeRateList() Response {
	msg := "GetExchangeRateList"
	return DoRequest(msg, "")
}

func GetShopList(userId, shopType int, productID string) Response {
	msg := "GetShopList"
	var req Shop_req
	req.UserId = userId
	req.ShopType = shopType
	req.ProductID = productID
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func GetShopListByUserId(shopType int, userId int, partnerId int) Response {
	msg := "GetShopListByUserId"
	var req Shop_req
	req.ShopType = shopType
	req.UserId = userId
	req.PartnerId = partnerId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func GetProduct(userId, shopType int, productId string) Response {
	msg := "GetProduct"
	var req Shop_req
	req.UserId = userId
	req.ShopType = shopType
	req.ProductID = productId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func Recharge(userId int, productId string) Response {
	msg := "Recharge"
	var req Shop_req
	req.UserId = userId
	req.ProductID = productId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func Exchange(userId int, productId string) Response {
	msg := "Exchange"
	var req Shop_req
	req.UserId = userId
	req.ProductID = productId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func ExchangeInBulk(userId int, productIds []string) Response {
	msg := "ExchangeInBulk"
	var req Shop_req
	req.UserId = userId
	req.ProductIDs = productIds
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

/*
func ExchangeRateInfo(currency string) (string, string) {
	msg := "ExchangeRateInfo"
	var req Shop_req
	req.Currency = currency
	d, _ := json.Marshal(req)
	rsp := DoRequest(msg, string(d))
	return rsp.Action[0], rsp.Action[1]
}


func GetUserPrice(userId int, price float64) float64 {
	msg := "GetUserPrice"
	var req Shop_req
	req.Price = price
	req.UserId = userId
	d, _ := json.Marshal(req)
	rsp := DoRequest(msg, string(d))
	var respond Shop_resp
	err := json.Unmarshal([]byte(rsp.Data), &respond)
	if err != nil {
		log.Release("GetUserPrice [%d],[%f] failed", userId, price)
		return price
	}
	return respond.Price
}
*/
