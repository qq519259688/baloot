package client

import (
	"encoding/json"

	"bet24.com/log"
)

// 获取补助信息
func SubsidyGetInfo(userId, lowerAmount int, maxTimes int) Response {
	log.Debug("coreclient.SubsidyGetInfo userId=%d", userId)
	msg := "SubsidyGetInfo"
	var req Subsidy_req
	req.UserId = userId
	req.LowerAmount = lowerAmount
	req.MaxTimes = maxTimes
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 领取补助
func SubsidyGift(userId, lowerAmount int, maxTimes int, coolSeconds []int) Response {
	log.Debug("corclient.SubsidyGift userId=%d", userId)
	msg := "SubsidyGift"
	var req Subsidy_req
	req.UserId = userId
	req.LowerAmount = lowerAmount
	req.MaxTimes = maxTimes
	req.CoolSeconds = coolSeconds
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 领取元宝补助
func SubsidyGiftChip(userId, lowerAmount int, ipAddress string) Response {
	log.Debug("corclient.SubsidyGiftChip userId=%d", userId)
	msg := "SubsidyGiftChip"
	var req Subsidy_req
	req.UserId = userId
	req.LowerAmount = lowerAmount
	req.IpAddress = ipAddress
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 领取回归奖励
func GiftReturnAward(userId int) Response {
	log.Debug("corclient.GiftReturnAward userId=%d", userId)
	msg := "GiftReturnAward"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
