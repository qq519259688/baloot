package client

import (
	"encoding/json"

	_ "bet24.com/log"
)

func DoWheel(userId int, times int, nickName, ipAddress string) Response {
	msg := "DoWheel"
	var req PrizeWheel_req
	req.UserId = userId
	req.NickName = nickName
	req.Times = times
	req.IpAddress = ipAddress
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func GetWheelPrizes() Response {
	msg := "GetWheelPrizes"
	return DoRequest(msg, "")
}

func GetWheelTimes(userId int) Response {
	msg := "GetWheelTimes"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func GetPrizeRecord() Response {
	msg := "GetPrizeRecord"
	return DoRequest(msg, "")
}
