package client

import (
	"encoding/json"

	"bet24.com/log"
)

func ParseKeyword(in string) string {
	msg := "ParseKeyword"
	var req KeyWord_req
	req.In = in
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	if err := json.Unmarshal([]byte(resp.Data), &req); err != nil {
		log.Error("coreParseKeyword unmarshal failed %v", err)
		return ""
	}
	return req.Out
}
