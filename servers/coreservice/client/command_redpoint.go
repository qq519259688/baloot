package client

import (
	"encoding/json"

	"bet24.com/log"
)

func CheckRedPoint(userId int) Response {
	log.Debug("corclient.CheckRedPoint %d", userId)
	msg := "CheckRedPoint"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
