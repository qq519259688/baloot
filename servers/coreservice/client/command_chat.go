package client

import (
	"encoding/json"

	_ "bet24.com/log"
)

// 发送聊天信息
func SendChatMsg(channelID, userId, faceId, sex, vipLevel, recvID int,
	nickName, faceUrl, chatMsg, ipAddress string, msgType int) bool {
	msg := "SendChatMsg"
	var req Chat_req
	req.ChannelID = channelID
	req.SendUserID = userId
	req.Vip = vipLevel
	req.NickName = nickName
	req.FaceUrl = faceUrl
	req.FaceId = faceId
	req.Sex = sex
	req.RecvUserID = recvID
	req.ChatMsg = chatMsg
	req.IpAddress = ipAddress
	req.MsgType = msgType
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.RetCode == 1
}

// 获取聊天信息
func GetChatMsg(userId, channelID int) string {
	msg := "GetChatMsg"
	var req Chat_Msg_req
	req.UserId = userId
	req.ChannelID = channelID
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))

	return resp.Data
}

func SendChannelChat(from, to int, message string, msgType int) string {
	msg := "SendChannelChat"
	var req ChannelChat_req
	req.From = from
	req.To = to
	req.Message = message
	req.MsgType = msgType
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))

	return resp.Data
}

func SendGiftMessage(from, to int, message string) string {
	msg := "SendGiftMessage"
	var req ChannelChat_req
	req.From = from
	req.To = to
	req.Message = message
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))

	return resp.Data
}

func GetChannelChat(channelKey string, userId int) string {
	msg := "GetChannelChat"
	var req GetChannelChat_req
	req.UserId = userId
	req.ChannelKey = channelKey
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))

	return resp.Data
}

func GetChannelInfo(channelKey string) string {
	msg := "GetChannelInfo"
	var req GetChannelChat_req
	req.ChannelKey = channelKey
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.Data
}

func ClearChannelHistory(channelKey string, userId int) {
	msg := "ClearChannelHistory"
	var req GetChannelChat_req
	req.ChannelKey = channelKey
	req.UserId = userId
	d, _ := json.Marshal(req)
	DoRequest(msg, string(d))
}

func RemoveChannelChat(userId1, userId2 int) {
	msg := "RemoveChannelChat"
	var req GetChannelChat_req
	req.UserId = userId1
	req.UserId2 = userId2
	d, _ := json.Marshal(req)
	DoRequest(msg, string(d))
}

// 获取机器人聊天列表
func GetRobotChatList() Response {
	msg := "GetRobotChatList"
	return DoRequest(msg, "")
}

// 获取机器人聊天信息
func GetRobotChatInfo(userId int) Response {
	msg := "GetRobotChatInfo"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 添加机器人聊天信息
func AddRobotChatInfo(userId int, chatMsg string, seconds int, beginTime, endTime string) Response {
	msg := "AddRobotChatInfo"
	var req RobotChat_req
	req.UserId = userId
	req.Msg = chatMsg
	req.Seconds = seconds
	req.BeginTime = beginTime
	req.EndTime = endTime
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 修改机器人聊天信息
func UpdateRobotChatInfo(userId int, chatMsg string, seconds int, beginTime, endTime string) Response {
	msg := "UpdateRobotChatInfo"
	var req RobotChat_req
	req.UserId = userId
	req.Msg = chatMsg
	req.Seconds = seconds
	req.BeginTime = beginTime
	req.EndTime = endTime
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 删除机器人聊天信息
func DelRobotChatInfo(userId int) Response {
	msg := "DelRobotChatInfo"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
