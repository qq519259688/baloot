package client

import (
	"bet24.com/log"
	"encoding/json"
)

// findIP 查找 IP 地址
func FindIP(userId int, ipAddress string) *FindIP_req {
	msg := "FindIP"
	var req Request_base
	req.UserId = userId
	req.IpAddress = ipAddress
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	if resp.RetCode != 1 {
		return nil
	}
	var info FindIP_req
	if err := json.Unmarshal([]byte(resp.Data), &info); err != nil {
		log.Error("client.FindIP json unmarshal err %v", err)
		return nil
	}
	return &info
}
