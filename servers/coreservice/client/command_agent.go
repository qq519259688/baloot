package client

import (
	"encoding/json"
)

// 刷新配置
func AgentRefreshConfig() Response {
	msg := "AgentRefreshConfig"
	return DoRequest(msg, "")
}

// 获取配置信息
func AgentConfigs() Response {
	msg := "AgentConfigs"
	return DoRequest(msg, "")
}

// 代理申请
func AgentApply(userId int, memo string) Response {
	msg := "AgentApply"
	var req AgentApply_req
	req.UserId = userId
	req.Memo = memo
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 代理绑码
func AgentBind(userId, higherUserId int, ipAddress string) Response {
	msg := "AgentBind"
	var req AgentBind_req
	req.UserId = userId
	req.HigherUserId = higherUserId
	req.IpAddress = ipAddress
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 代理信息
func AgentInfo(userId int) Response {
	msg := "AgentInfo"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 会员
func AgentMembers(userId, pageIndex, pageSize int) Response {
	msg := "AgentMembers"
	var req Agent_req
	req.UserId = userId
	req.PageIndex = pageIndex
	req.PageSize = pageSize
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 代理统计
func AgentStat(userId, pageIndex, pageSize int) Response {
	msg := "AgentStat"
	var req Agent_req
	req.UserId = userId
	req.PageIndex = pageIndex
	req.PageSize = pageSize
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 收益记录
func AgentCommissionLog(userId, fromUserId, pageIndex, pageSize int) Response {
	msg := "AgentCommissionLog"
	var req Agent_req
	req.UserId = userId
	req.FromUserId = fromUserId
	req.PageIndex = pageIndex
	req.PageSize = pageSize
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 提取收益
func AgentCommissionToAmount(userId int, ipAddress string) Response {
	msg := "AgentCommissionToAmount"
	var req Request_base
	req.UserId = userId
	req.IpAddress = ipAddress
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 获取群组
func AgentGetGroup(userId int) Response {
	msg := "AgentGetGroup"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 修改群组
func AgentUpdateGroup(userId int, id int, name, url string) Response {
	msg := "AgentUpdateGroup"
	var req AgentGroup_req
	req.UserId = userId
	req.Id = id
	req.Name = name
	req.Url = url
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
