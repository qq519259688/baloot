package client

import (
	"encoding/json"
)

// 100K购信息(期数\元宝数量\倒计时\抽奖码集合)
func PurchaseInfo(userId int) Response {
	msg := "PurchaseInfo"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 100K购抽奖(返回操作结果\抽奖码)
func PurchaseBet(userId int, ipAddress string) Response {
	msg := "PurchaseBet"
	var req Request_base
	req.UserId = userId
	req.IpAddress = ipAddress
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
