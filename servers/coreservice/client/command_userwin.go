package client

import (
	_ "bet24.com/log"

	"encoding/json"
)

func AddUserWinScore(userId int, score int) Response {
	msg := "AddUserWinScore"
	var req UserWinAddScore_req
	req.UserId = userId
	req.Score = score
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func GetUserWinScore(userId int) int {
	msg := "GetUserWinScore"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.RetCode
}
