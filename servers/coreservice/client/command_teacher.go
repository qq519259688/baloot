package client

import (
	"encoding/json"
)

// 注册
func TeacherRegister(userId int) Response {
	msg := "TeacherRegister"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 师父信息
func TeacherInfo(teacherId int) Response {
	msg := "TeacherInfo"
	var req Request_base
	req.UserId = teacherId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 绑定
func BindTeacher(studentId, teacherId, isSendMail int) Response {
	msg := "BindTeacher"
	var req Teacher_req
	req.UserId = studentId
	req.TeacherId = teacherId
	req.IsSendMail = isSendMail
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 徒弟列表
func Students(teacherId int) Response {
	msg := "Students"
	var req Teacher_req
	req.TeacherId = teacherId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 收益列表
func TeacherProfitList(teacherId, days, pageIndex, pageSize int) Response {
	msg := "TeacherProfitList"
	var req TeacherProfit_req
	req.UserId = teacherId
	req.Days = days
	req.PageIndex = pageIndex
	req.PageSize = pageSize
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
