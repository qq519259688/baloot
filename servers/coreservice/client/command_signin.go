package client

import (
	"encoding/json"
)

func GetSigninInfo(userId int) Response {
	msg := "GetSigninInfo"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func DoSignin(userId int) Response {
	msg := "DoSignin"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func GiftContinueAward(userId, day int) Response {
	msg := "GiftContinueAward"
	var req GiftContinueAward_req
	req.UserId = userId
	req.Day = day
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
