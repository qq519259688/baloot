package client

// 公告列表
func AnnounceList() Response {
	msg := "AnnounceList"
	return DoRequest(msg, "")
}

// 刷新列表
func AnnounceRefresh() Response {
	msg := "AnnounceRefresh"
	return DoRequest(msg, "")
}
