package client

import (
	"encoding/json"
	"sort"

	"bet24.com/log"
)

func VideoPlay(userId, videoId int) Response {
	msg := "VideoPlay"
	var req Video_req
	req.UserId = userId
	req.VideoId = videoId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func VideoGetInfo(userId, videoId int) Response {
	msg := "VideoGetInfo"
	var req Video_req
	req.UserId = userId
	req.VideoId = videoId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func VideoSettleInfo(userId, settleAmount, gameId int) VideoSettleInfo_resp {
	msg := "VideoSettleInfo"
	var req VideoSettleInfo_req
	req.UserId = userId
	req.SettleAmount = -settleAmount
	req.GameID = gameId
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))

	var info VideoSettleInfo_resp
	if err := json.Unmarshal([]byte(resp.Data), &info); err != nil {
		log.Error("command_video.videoSettleInfo unmarshal fail %v", err)
	}

	return info
}

func VideoSettle(userId, timeStamp int) int {
	msg := "VideoSettle"
	var req VideoSettle_req
	req.UserId = userId
	req.TimeStamp = timeStamp
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.RetCode
}

func GetGameSettleVideoList(userId int) []*SettleVideoInfo_resp {
	msg := "GetGameSettleVideoList"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))

	var list []*SettleVideoInfo_resp
	if err := json.Unmarshal([]byte(resp.Data), &list); err != nil {
		log.Error("commond_video.GetGameSettleVideoList unmarshal fail %v", err)
	}
	sort.SliceStable(list, func(i, j int) bool {
		return list[i].Crdate > list[j].Crdate
	})
	return list
}

func AwardGameSettleVideo(userId, settleId int) Response {
	msg := "AwardGameSettleVideo"
	var req AwardGameSettleVideo_req
	req.UserId = userId
	req.SettleId = settleId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
