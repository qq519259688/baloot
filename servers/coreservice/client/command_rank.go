package client

import (
	"encoding/json"

	"bet24.com/log"
)

// 排行榜
func GetRankList(userId, rankType, num int) Response {
	// log.Debug("coreclient.GetRankList rankType=%d", rankType)
	msg := "GetRankList"
	var req RankList_req
	req.UserId = userId
	req.RankType = rankType
	req.Num = num
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 榜单历史
func RankHistoryList(userId int, dateFlag string) Response {
	log.Debug("coreclient.RankHistoryList userId=%d dateFlag=%s", userId, dateFlag)
	msg := "RankHistoryList"
	var req RankHistory_req
	req.UserId = userId
	req.DateFlag = dateFlag
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 领取榜单奖励
func RankAward(userId, rankType, doubleFlag int) Response {
	log.Debug("coreclient.RankAward userId=%d rankType=%d dateFlag=%d", userId, rankType, doubleFlag)
	msg := "RankAward"
	var req RankAward_req
	req.UserId = userId
	req.RankType = rankType
	req.DoubleFlag = doubleFlag
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 榜单奖励列表
func GetRankAwardList() Response {
	msg := "GetRankAwardList"
	return DoRequest(msg, "")
}
