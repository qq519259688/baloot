package client

import (
	"encoding/json"

	"bet24.com/log"
)

// 发送奖励
func SendAward(userId, activeId, hallType int, ipAddress string) Response {
	log.Debug("coreclient.SendAward userId=%d", userId)
	msg := "SendAward"
	var req Award_req
	req.UserId = userId
	req.ActiveId = activeId
	req.IpAddress = ipAddress
	req.HallType = hallType
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
