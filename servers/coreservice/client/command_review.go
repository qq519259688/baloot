package client

import (
	"encoding/json"

	"bet24.com/log"
)

//获取评论信息
func ReviewGetInfo(userId int, appName string) Response {
	log.Debug("coreclient.ReviewGetInfo userId=%d", userId)
	msg := "ReviewGetInfo"
	var req Review_req
	req.UserId = userId
	req.AppName = appName
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

//领取评论
func ReviewGift(userId int, appName, ipAddress string) Response {
	log.Debug("corclient.ReviewGift userId=%d", userId)
	msg := "ReviewGift"
	var req Review_req
	req.UserId = userId
	req.AppName = appName
	req.IpAddress = ipAddress
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
