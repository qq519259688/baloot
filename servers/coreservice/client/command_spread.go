package client

import (
	"encoding/json"
)

//会员申请
func SpreadApply(userId, code int) Response {
	msg := "SpreadApply"
	var req SpreadApply_req
	req.UserId = userId
	req.Code = code
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

//会员列表
func SpreadMembers(code, pageIndex, pageSize int) Response {
	msg := "SpreadMembers"
	var req SpreadMember_req
	req.UserId = code
	req.PageIndex = pageIndex
	req.PageSize = pageSize
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

//领取奖励
func SpreadGift(code, fromUserID int) Response {
	msg := "SpreadGift"
	var req SpreadGift_req
	req.UserId = code
	req.FromUserID = fromUserID
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
