package client

import (
	"encoding/json"

	_ "bet24.com/log"
)

func OnUserEnter(userId int, ipAddress string) Response {
	msg := "OnUserEnter"
	var req Request_base
	req.UserId = userId
	req.IpAddress = ipAddress
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func OnUserExit(userId int) Response {
	msg := "OnUserExit"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func SyncUserList(userlist []int) {
	msg := "SyncUserList"
	d, _ := json.Marshal(userlist)
	DoRequest(msg, string(d))
}
