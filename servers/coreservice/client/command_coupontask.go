package client

import (
	"encoding/json"

	"bet24.com/log"
)

// 用户券任务
func GetUserCouponTask(userId int) *UserCouponTask_resp {
	msg := "GetUserCouponTask"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	ret := DoRequest(msg, string(d))

	var resp UserCouponTask_resp
	if err := json.Unmarshal([]byte(ret.Data), &resp); err != nil {
		log.Error("command_coupontask.GetUserCouponTask unmarshal fail %v", err)
	}
	return &resp
}

// 触发任务
func TriggerCouponTask(userId, gameId, baseScore, isDouble, players int) Response {
	log.Debug("TriggerCouponTask userId=%d gameId=%d baseScore=%d isDouble=%d players=%d",
		userId, gameId, baseScore, isDouble, players)
	msg := "TriggerCouponTask"
	var req TriggerCouponTask_req
	req.UserId = userId
	req.GameId = gameId
	req.BaseScore = baseScore
	req.IsDouble = isDouble
	req.Players = players
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 领取奖励
func AwardCouponTask(userId, userTaskId int) Response {
	msg := "AwardCouponTask"
	var req AwardCouponTask_req
	req.UserId = userId
	req.UserTaskId = userTaskId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 修改临时上限
func UpdateCouponTaskTmpLimit(userId int) Response {
	msg := "UpdateCouponTaskTmpLimit"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
