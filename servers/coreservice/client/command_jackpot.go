package client

import (
	"encoding/json"

	_ "bet24.com/log"
)

func GetJackpotAmount(gameId int, isChipRoom bool) int {
	msg := "GetJackpotAmount"
	var req JackPot_req
	req.GameId = gameId
	req.IsChipRoom = isChipRoom
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.RetCode
}

func ModifyJackpot(amount int, gameId int, userId int, desc string, isChipRoom bool) int {
	msg := "ModifyJackpot"
	var req JackPotModify_req
	req.GameId = gameId
	req.UserId = userId
	req.Amount = amount
	req.Desc = desc
	req.IsChipRoom = isChipRoom
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.RetCode
}
