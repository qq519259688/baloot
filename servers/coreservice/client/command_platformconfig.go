package client

import (
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

// 获取单个字段
func GetPlatformConfig(key string) string {
	return platformconfig.GetConfig(key)
}

func GetPlatformConfigList() []string {
	return platformconfig.GetConfigList()
}

func SetPlatformConfig(key, value string) {
	platformconfig.SetConfig(key, value)
}
