package client

import (
	"encoding/json"
)

// 使用充值卡
func UseRechargeCard(userId int, cardNo string) Response {
	msg := "UseRechargeCard"
	var req ChargeCard_Req
	req.UserId = userId
	req.CardNo = cardNo
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
