package client

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
	"encoding/json"
)

type ExchangeType struct {
	Id           int
	ExchangeType int    // 兑换类型(1=奖券兑换金币  2=代理兑换元宝 3=代理兑换电话卡  4=代理兑换RP  5=金币提现)
	Price        int    // 价格
	Memo         string // 描述
}

type ExchangeHistory struct {
	ExchangeId   int //兑换ID
	ExchangeType int // 兑换类型(1=奖券兑换金币  2=代理兑换元宝 3=代理兑换电话卡  4=代理兑换RP)
	Price        int // 价格
	Items        []item.ItemPack
	Remark       string //备注
	Status       int    //状态（0=待发货  1=已发货）
	Crdate       string //时间
}

func GetExchangeType() []ExchangeType {
	var list []ExchangeType
	msg := "GetExchangeType"
	resp := DoRequest(msg, "")
	if resp.RetCode == 1 {
		if err := json.Unmarshal([]byte(resp.Data), &list); err != nil {
			log.Error("GetExchangeType json unmarshal err %v", err)
		}
	}
	return list
}

// 获取兑换信息
func GetExchangeList(userId int) Response {
	msg := "GetExchangeList"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func UserExchange(userId, exchangeId, num int, remark string) Response {
	msg := "UserExchange"
	var req UserExchange_req
	req.UserId = userId
	req.ExchangeId = exchangeId
	req.Num = num
	req.Remark = remark
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func GetExchangeHistory(userId, pageIndex, pageSize int) Response {
	msg := "GetExchangeHistory"
	var req UserExchangeHistory_req
	req.UserId = userId
	req.PageIndex = pageIndex
	req.PageSize = pageSize
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
