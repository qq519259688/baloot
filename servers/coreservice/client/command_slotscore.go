package client

import (
	"encoding/json"
)

func AddSlotScore(userId, score int) int {
	msg := "AddSlotScore"
	var req SlotScore_req
	req.UserId = userId
	req.Param = score
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.RetCode
}

func GetSlotScore(userId int) int {
	msg := "GetSlotScore"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.RetCode
}

func GetSlotScoreExchangeList() string {
	msg := "GetSlotScoreExchangeList"
	resp := DoRequest(msg, "")
	return resp.Data
}

func SlotScoreExchange(userId, productId int) bool {
	msg := "SlotScoreExchange"
	var req SlotScore_req
	req.UserId = userId
	req.Param = productId
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.RetCode == 1
}

func GetSlotScoreExchangeHistory(userId int) string {
	msg := "GetSlotScoreExchangeHistory"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.Data
}
