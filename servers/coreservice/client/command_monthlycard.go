package client

import (
	"encoding/json"

	"bet24.com/log"
)

//月卡系统信息
func MonthlyCardSysInfo() Response {
	log.Debug("coreclient.MonthlyCardSysInfo")
	msg := "MonthlyCardSysInfo"
	return DoRequest(msg, "")
}

//获取用户月卡信息
func MonthlyCardUserInfo(userId int) Response {
	log.Debug("corclient.MonthlyCardUserInfo userId=%d", userId)
	msg := "MonthlyCardUserInfo"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

//领取月卡
func MonthlyCardGift(userId int) Response {
	log.Debug("coreclient.MonthlyCardGift userId=%d", userId)
	msg := "MonthlyCardGift"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

//领取周卡
func WeekCardGift(userId int) Response {
	log.Debug("coreclient.WeekCardGift userId=%d", userId)
	msg := "WeekCardGift"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
