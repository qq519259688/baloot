package client

import (
	"encoding/json"

	"bet24.com/log"
)

func GetUserVitalityInfo(userId int) Response {
	log.Debug("corclient.GetUserVitalityInfo userId=%d", userId)
	msg := "GetUserVitalityInfo"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func AddVitalityPoint(userId, point int) Response {
	log.Debug("corclient.AddVitalityPoint userId=%d point=%d", userId, point)
	msg := "AddVitalityPoint"
	var req AddVitalityPoint_req
	req.UserId = userId
	req.Point = point
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
