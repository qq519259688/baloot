package client

import (
	"encoding/json"

	"bet24.com/log"
)

// userId=用户ID gameId=游戏ID  priority=优先级  msg=消息  ext=扩展信息
func SendBroadcast(userId, gameId, priority int, msg, ext string) Response {
	log.Debug("corclient.SendBroadcast userId=%d gameId=%d priority=%d msg=%s ext=%s",
		userId, gameId, priority, msg, ext)
	cmdmsg := "SendBroadcast"
	var req SendBroadcast_req
	req.UserID = userId
	req.GameID = gameId
	req.Priority = priority
	req.Msg = msg
	req.Ext = ext
	d, _ := json.Marshal(req)
	return DoRequest(cmdmsg, string(d))
}

func SendGameWinBroadcast(userId int, userName string, score int, gameId int, gameName string) Response {
	cmdmsg := "SendGameWinBroadcast"
	var req SendBroadcast_req
	req.UserID = userId
	req.GameID = gameId
	req.Score = score
	req.NickName = userName
	req.GameName = gameName
	d, _ := json.Marshal(req)
	return DoRequest(cmdmsg, string(d))
}
