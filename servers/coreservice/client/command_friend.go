package client

import (
	"bet24.com/log"
	"encoding/json"
)

// 搜索
func GetSearchUserinfo(userId, targetUserID int, targetNickName string) Response {
	msg := "GetSearchUserinfo"
	var req Friend_req
	req.UserId = userId
	req.TargetUserID = targetUserID
	req.TargetNickName = targetNickName
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 搜索用户列表
func GetSearchUserList(userId, targetUserID int, targetNickName string) Response {
	msg := "GetSearchUserList"
	var req Friend_req
	req.UserId = userId
	req.TargetUserID = targetUserID
	req.TargetNickName = targetNickName
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 获取好友列表
func GetFriendList(userId int) Response {
	msg := "GetFriendList"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 获取好友审核列表
func FriendVerifyList(userId int) Response {
	msg := "FriendVerifyList"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 好友申请
func FriendApply(userId, targetUserID int) Response {
	msg := "FriendApply"
	var req Friend_req
	req.UserId = userId
	req.TargetUserID = targetUserID
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func DelFriend(userId, targetUserID int) Response {
	msg := "DelFriend"
	var req Friend_req
	req.UserId = userId
	req.TargetUserID = targetUserID
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 处理好友申请
func FriendHandleApply(userId, targetUserID, apply int) Response {
	msg := "FriendHandleApply"
	var req Friend_Apply_req
	req.UserId = userId
	req.TargetUserID = targetUserID
	req.Apply = apply
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func FriendGiveGift(userId, targetUserID int) Response {
	msg := "FriendGiveGift"
	var req Friend_req
	req.UserId = userId
	req.TargetUserID = targetUserID
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func FriendGetGift(userId, targetUserID int, ipAddress string) Response {
	msg := "FriendGetGift"
	var req Friend_req
	req.UserId = userId
	req.TargetUserID = targetUserID
	req.IpAddress = ipAddress
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func FriendSetUserStatus(userId, isOnline int, serverName string) Response {
	msg := "FriendSetUserStatus"
	var req Friend_Status_req
	req.UserId = userId
	req.IsOnline = isOnline
	req.ServerName = serverName
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func FriendRoomInvite(userId int, nickName string, toUserId, roomNo int) Response {
	msg := "FriendRoomInvite"
	var req FriendRoomInvite_req
	req.UserId = userId
	req.NickName = nickName
	req.ToUserId = toUserId
	req.RoomNo = roomNo
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func FriendGetRoomInviteList(userId int) Response {
	msg := "FriendGetRoomInviteList"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func FriendRoomInviteInvalid(userId, roomNo int) Response {
	msg := "FriendRoomInviteInvalid"
	var req FriendRoomInvite_req
	req.UserId = userId
	req.RoomNo = roomNo
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func GetMaxFriendCount(userId int) Response {
	msg := "GetMaxFriendCount"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func FriendGetPotentialList(userId int) string {
	msg := "FriendGetPotentialList"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.Data
}

func FriendAddPotential(userId int, toUserId int, memo string) {
	msg := "FriendAddPotential"
	var req FriendAddPotential_req
	req.UserId = userId
	req.Memo = memo
	req.ToUserId = toUserId
	d, _ := json.Marshal(req)
	DoRequest(msg, string(d))
}

// 黑名单
func FriendGetBlackList(userId, pageIndex, pageSize int) string {
	msg := "FriendGetBlackList"
	var req FriendBlack_req
	req.UserId = userId
	req.PageIndex = pageIndex
	req.PageSize = pageSize
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.Data
}

// 添加黑名单
func FriendAddBlack(userId int, toUserId int) Response {
	msg := "FriendAddBlack"
	var req FriendBlack_req
	req.UserId = userId
	req.ToUserId = toUserId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 删除黑名单
func FriendDelBlack(userId int, toUserId int) Response {
	msg := "FriendDelBlack"
	var req FriendBlack_req
	req.UserId = userId
	req.ToUserId = toUserId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func FriendIsBlackListUser(userId int, toUserId int) bool {
	if userId == toUserId {
		return false
	}
	msg := "FriendIsBlackListUser"
	var req FriendBlack_req
	req.UserId = userId
	req.ToUserId = toUserId
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.RetCode == 1
}

func FriendIsBlackListUserIn(userId int, toUserIds []int) bool {
	if len(toUserIds) == 0 {
		return false
	}
	msg := "FriendIsBlackListUserIn"
	var req FriendBlack_req
	req.UserId = userId
	req.ToUserIds = toUserIds
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	return resp.RetCode == 1
}

func FriendGetBlackListUserIn(userId int, toUserIds []int) []int {
	if len(toUserIds) == 0 {
		return nil
	}
	msg := "FriendGetBlackListUserIn"
	var req FriendBlack_req
	req.UserId = userId
	req.ToUserIds = toUserIds
	d, _ := json.Marshal(req)
	resp := DoRequest(msg, string(d))
	var ret []int
	err := json.Unmarshal([]byte(resp.Data), &ret)
	if err != nil {
		log.Release("FriendIsBlackListUserIn ret unmarshal failed %s", resp.Data)
		return nil
	}
	return ret
}
