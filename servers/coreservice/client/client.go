package client

import (
	"context"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "Server" // 为兼容旧版链接

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SetServiceAddr(addr string) {

}

type Reply struct {
	Resp Response
}

func DoRequest(msg, data string) Response {
	xclient := getClient()
	args := &Request{
		Msg:  msg,
		Data: data,
	}

	reply := &Reply{}
	err := xclient.Call(context.Background(), msg, args, reply)
	if err != nil {
		log.Release("msg=%s data=%s call failed: %v", msg, data, err)
	}
	return reply.Resp
}
