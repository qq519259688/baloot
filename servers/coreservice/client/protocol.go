package client

import (
	_ "fmt"

	"bet24.com/servers/coreservice/coupontask"
	item "bet24.com/servers/micros/item_inventory/proto"
	task "bet24.com/servers/micros/task/proto"
	mail "bet24.com/servers/micros/userservices/proto"
)

const (
	Action_gold    = "Action_gold"
	Action_diamond = "Action_diamond"
	Action_Item    = "Action_Item"
	Action_Task    = "Action_Task"
	Action_mail    = "Action_mail"
	// ...
)

type Request struct {
	Msg  string
	Data string
}

type Response struct {
	RetCode int      // 返回值
	Action  []string // 是否需要进一步动作
	Data    string   // 返回数据
}

type Request_base struct {
	UserId    int
	IpAddress string
}

type Request_SigninWheel struct {
	Request_base
	UserGold int
}

type GiftContinueAward_req struct {
	Request_base
	Day int
}

type TaskAction_req struct {
	Request_base
	Action   int
	Progress int
	Scope    task.TaskScope
}

type TaskAward_req struct {
	Request_base
	TaskId int
}

type GetSysTask_req struct {
	TaskId int
}

type VipAddPoint_req struct {
	Request_base
	Point int
}

type VipTryWithdraw_req struct {
	Request_base
	WantedAmount int
	Query        bool
}

type UserVipInfo_resp struct {
	Level int
	Point int
	// 其他
}

type LevelAddExp_req struct {
	Request_base
	GameId int
	Score  int
}

type UserWinAddScore_req struct {
	Request_base
	Score int
}

type InventoryConsume_req struct {
	Request_base
	ItemId int
	Count  int
	Bullet int
	IsGift int // 是否赠送扣减（1=赠送扣减）
}

type InventoryConsume_resp struct {
	Succeeded bool
	ErrMsg    string
}

type InventorySell_req struct {
	Request_base
	ItemId  int
	Count   int
	LogType int
}

type InventorySell_resp struct {
	Succeeded bool
	ErrMsg    string
}

type InventoryAddItems_req struct {
	Request_base
	Items   []item.ItemPack
	Desc    string
	LogType int
}

type InventoryAddEnegy_req struct {
	Request_base
	Bullet int
}

type InventoryUseEnegy_req struct {
	Request_base
	Bullet int
	ItemId int
}

type InventoryGift_req struct {
	Request_base
	ToUserId int
	ItemId   int
	Count    int
}

type GetItemCount_req struct {
	Request_base
	ItemId int
}

type GetItemCount_resp struct {
	Count int
}

type AddNotification_req struct {
	Request_base
	NotificationId int
	Data           string
}

type SendUserMail_req struct {
	Request_base
	Title   string
	Content string
	Img     string
}

type GetUserMail_req struct {
	Request_base
	MailId int
}

type SendSysMail_req struct {
	Request_base
	*mail.SysMail
}

type UpdateSysMail_req struct {
	Request_base
	SysMsgId int
	Status   int
}

type DelSysMail_req struct {
	Request_base
	SysMsgId int
}

type Shop_req struct {
	Request_base
	Currency   string
	ShopType   int
	ProductID  string
	ProductIDs []string
	Num        int
	PartnerId  int
	Price      float64
}

type Shop_resp struct {
	RetCode  int
	ErrorMsg string
	Price    float64
}

type Shop_Giving_Resp struct {
	Success  bool
	ErrorMsg string
	Items    []item.ItemPack
	IsHot    int
}

type RankList_req struct {
	Request_base
	RankType int
	Num      int
}

type rankItem_resp struct {
	RankType     int    //类型
	Rank         int    //名次
	UserId       int    //用户ID
	NickName     string //昵称
	Amount       int    //数量
	FaceID       int    //头像ID
	FaceUrl      string //头像URL
	Sex          int    //性别
	UserWords    string //用户签名
	VipLevel     int    //Vip等级
	IsFriend     int    //是否好友
	Achievements []int  //成就列表
	Awards       []item.ItemPack
}

type PrizeWheel_req struct {
	Request_base
	NickName string //昵称
	Times    int    // 摇奖次数
}

type AddVitalityPoint_req struct {
	Request_base
	Point int
}

type BuyGiftPack_req struct {
	Request_base
	ProductId string
}

type GrowthPackUnlock_req struct {
	Request_base
	Bullet int
}

type GrowthPackAward_req struct {
	Request_base
	GrowthPackId int
	Index        int
}

type BattlePassAddExp_req struct {
	Request_base
	Exp int
}

type GrowthPackAward_resp struct {
	Succeeded bool
	ErrMsg    string
}

type GetDiamond_resp struct {
	Success bool
	Diamond int
}

type GetMoney_resp struct {
	Success bool
	Gold    int
}

type SendBroadcast_req struct {
	UserID   int    //用户ID -1=系统广播
	GameID   int    //游戏ID 0=所有游戏
	Msg      string //消息
	Priority int    //优先级
	Ext      string //扩展信息
	Score    int
	GameName string
	NickName string
}

type Subsidy_req struct {
	Request_base
	LowerAmount int
	MaxTimes    int
	CoolSeconds []int
}

type UserExchange_req struct {
	Request_base
	ExchangeId int
	Num        int
	Remark     string
}

type UserExchangeHistory_req struct {
	Request_base
	PageIndex int
	PageSize  int
}

type UserExchangeHistory_resp struct {
	RecordCount int
	List        []ExchangeHistory
}

type InventoryReduceByAdmin_req struct {
	Request_base
	ItemId     int
	Count      int
	OpUserID   int
	OpUserName string
}

type Inventory_resp struct {
	Succeeded bool
	ErrMsg    string
}

type BetList_req struct {
	Request_base
	GameID    int
	Days      int
	PageIndex int
	PageSize  int
	BetZone   string
}

type CashLog_req struct {
	Request_base
	BeginTime string
	EndTime   string
	PageIndex int
	PageSize  int
}

// 保险柜
type BankInOrOut_req struct {
	Request_base
	Amount     int
	GameID     int
	ServerName string
}

type BankInOrOut_resp struct {
	RetCode    int
	GameID     int `json:"GameID,omitempty"`
	Gold       int
	BankAmount int //保险柜金币
	OutMsg     string
}

// 聊天
type Chat_req struct {
	ChannelID  int    //聊天频道, 目前只有世界频道
	SendUserID int    //发送者ID
	Vip        int    //VIP等级
	NickName   string //昵称
	FaceUrl    string //头像URL
	FaceId     int    //头像ID
	Sex        int    //性别
	RecvUserID int    //接收者ID，私聊备用
	ChatMsg    string //内容
	IpAddress  string
	MsgType    int
}

type Chat_Msg_req struct {
	Request_base
	ChannelID int //聊天频道, 目前只有世界频道
}

type ChannelChat_req struct {
	From    int
	To      int
	Message string
	MsgType int
}

type GetChannelChat_req struct {
	UserId     int
	ChannelKey string
	UserId2    int
}

// 好友
type Friend_req struct {
	Request_base
	TargetUserID   int
	TargetNickName string
}

type Friend_Apply_req struct {
	Request_base
	TargetUserID int
	Apply        int //0，不同意，1，同意
}

type Friend_Status_req struct {
	Request_base
	IsOnline   int
	ServerName string
}

type JackPot_req struct {
	Request_base
	GameId     int
	IsChipRoom bool
}

type JackPotModify_req struct {
	JackPot_req
	Amount int
	Desc   string
}

type SpreadApply_req struct {
	Request_base
	Code int
}

type SpreadMember_req struct {
	Request_base
	PageIndex int
	PageSize  int
}

type SpreadGift_req struct {
	Request_base
	FromUserID int
}

type AchievementInfo_req struct {
	Request_base
	TaskID int
}

type AchievementUpdate_req struct {
	Request_base
	TaskID int
	Status int
}

type TrackRecord_req struct {
	Request_base
	Level_1 string
	Level_2 string
	Level_3 string
}

type GetGameCount_req struct {
	Request_base
	GameID int
}

type UpdateDominoGameCount_req struct {
	Request_base
	Double   int
	Triple   int
	Quariter int
	Qunitet  int
	CardData string
}

type UpdateQiuqiuGameCount_req struct {
	Request_base
	SixDevil   int
	TwinCards  int
	SmallCards int
	BigCards   int
	QiuQiu     int
	CardData   string
}

type Video_req struct {
	Request_base
	VideoId int
}

type VideoSettleInfo_req struct {
	Request_base
	GameID       int
	SettleAmount int
}

type VideoSettleInfo_resp struct {
	Success      bool // 成功标识
	TimeStamp    int  // 时间戳
	ReturnAmount int  // 总返还金币
	MaxTimes     int  // 最大次数
	SettleAmount int  // 返还金币
}

type VideoSettle_req struct {
	Request_base
	TimeStamp int
}

type Review_req struct {
	Request_base
	AppName string
}

// 游戏视频返还信息
type SettleVideoInfo_resp struct {
	SettleId     int    // 结算Id
	GameID       int    // 游戏ID
	ChineseName  string // 游戏名称
	LoseAmount   int    // 输金币
	MaxTimes     int    // 最大次数
	SettleTimes  int    // 返还次数
	MaxAmount    int    // 最大金币
	SettleAmount int    // 返还金币
	Crdate       string // 时间
}

type AwardGameSettleVideo_req struct {
	Request_base
	SettleId int
}

type ReceiveNewUserGift_Req struct {
	Request_base
	IsDouble int
}

type Teacher_req struct {
	Request_base
	TeacherId  int
	IsSendMail int
}

type TeacherProfit_req struct {
	Request_base
	Days      int
	PageIndex int
	PageSize  int
}

type TeacherPlayVideo_req struct {
	Request_base
	Rid       int
	StudentId int
}

type AgentBind_req struct {
	Request_base
	HigherUserId int // 上级ID
}

type Agent_req struct {
	Request_base
	FromUserId int
	PageIndex  int
	PageSize   int
}

type AgentSetBindSend_req struct {
	Request_base
	BindSend int
}

type AgentSendBindSend_req struct {
	Request_base
	ToUserID int
}

type AgentCommissionRank_req struct {
	Request_base
	Days      int
	PageIndex int
	PageSize  int
}

type AgentApply_req struct {
	Request_base
	Memo string
}

type AgentGroup_req struct {
	Request_base
	Id   int
	Name string
	Url  string
}

type RankHistory_req struct {
	Request_base
	DateFlag string // 日期
}

type RankAward_req struct {
	Request_base
	RankType   int
	DoubleFlag int
}

type TriggerCouponTask_req struct {
	Request_base
	GameId    int
	BaseScore int
	IsDouble  int
	Players   int
}

type AwardCouponTask_req struct {
	Request_base
	UserTaskId int
}

type UserCouponTask_resp struct {
	Enabled bool
	Info    *coupontask.BaseInfo
	List    []*coupontask.UserTask
}

type Transfer_req struct {
	Request_base
	ToUserID int
	Amount   int
}

type Transfer_resp struct {
	RetCode     int
	ErrMsg      string
	StillAmount int
	Refund      int
}

type Polling_req struct {
	Addr    string // 地址
	Players int    // 玩家数
}

type GiveChip_resp struct {
	Request_base
	Amount     int
	LogType    int
	SourceName string
	Remark     string
}

type SaveBankInfo_req struct {
	Request_base
	RealName string // 真实姓名
	BankName string // 银行名称
	BankCode string // 银行码
	BankCard string // 银行卡号
	Mobile   string // 手机号
}

type SlotScore_req struct {
	Request_base
	Param int // score,productId
}

type ChipWheel_req struct {
	Request_base
	Param int
}

type Award_req struct {
	Request_base
	ActiveId int
	HallType int // 0=无效  1=金币大厅  2=元宝大厅  3=不区分大厅类型
}

type CreatePrivateRoomByGameServer_req struct {
	Request_base
	GameId   int
	GameName string
	Addr     string
	TableId  int
}

type CreatePrivateRoomByUser_req struct {
	Request_base
	GameId    int
	RuleName  string
	Target    int
	UserCount int
	PlayTime  int
	Fee       int
	Prize     int
}

type PrivateRoom_req struct {
	Request_base
	RoomNo int
}

type PrivateRoomSit_req struct {
	PrivateRoom_req
	ChairId int
}

type PrivateRoomUpdateScore_req struct {
	PrivateRoom_req
	Score int
}

type PrivateRoomSetWinners_req struct {
	PrivateRoom_req
	Winners []int
}

type PrivateRoomGetGameRules_req struct {
	GameId int
}

type PrivateRoomRequstSit_req struct {
	PrivateRoom_req
	NickName string
	FaceId   int
	FaceUrl  string
}

type FriendRoomInvite_req struct {
	Request_base
	NickName string
	ToUserId int
	RoomNo   int
}

type FriendAddPotential_req struct {
	Request_base
	ToUserId int
	Memo     string
}

type RegisterPrivateServer_req struct {
	Addr      string
	GameId    int
	GameName  string
	GameRules []string
}

type RegisterPrivateGameRule_req struct {
	Addr     string
	GameId   int
	GameName string
	RuleName string
	RuleDesc string
	RuleData string
}

type UpdatePrivateServer_req struct {
	Addr   string
	Online int
}

type UpdatePrivateRoomStatus_req struct {
	RoomNo int
	Status int
}

type CreateSimpleMatch_req struct {
	Request_base
	GameId    int
	GameRule  string
	TotalUser int
	TableUser int
	Target    int
	EnrollFee int
	Prize     int
	PlayTime  int
}

type SimpleMatch_req struct {
	Request_base
	MatchNo int
}

type EnrollSimpleMatch_req struct {
	SimpleMatch_req
	NickName string
	FaceId   int
	FaceUrl  string
}

type CreatePointMatch_req struct {
	Request_base
	GameId         int
	GameRule       string
	TotalUser      int
	TableUser      int
	EnrollFee      int
	Prize          int
	PlayTime       int
	EliminateScore int
	ShrinkSec      int
	ShrinkScore    int
	WinnerCount    int
}

type SngMatch_req struct {
	Request_base
	MatchId int
}

type EnrollSngMatch_req struct {
	SngMatch_req
	NickName string
	FaceId   int
	FaceUrl  string
	FeeIndex int
}

type PlatformConfigSet_req struct {
	Key   string
	Value string
}

type KeyWord_req struct {
	In  string
	Out string
}

type ChargeCard_Req struct {
	Request_base
	CardNo string
}

type RobotChat_req struct {
	Request_base
	Msg       string // 消息
	Seconds   int    // 间隔时间(秒)
	BeginTime string // 开始时间
	EndTime   string // 截止时间
}

type ReduceMoney_req struct {
	Request_base
	Amount     int
	LogType    int
	SourceName string
	Remark     string
}

type SaveCountry_req struct {
	Request_base
	CountryName string
	Currency    string
}

type FindIP_req struct {
	IP      string `json:"ip"`
	Country string `json:"country"`
	Area    string `json:"area"`
}

type WaterPool_req struct {
	UserGold int
	IsSlots  bool
	Amount   int
	GameId   int
	Tax      int
}

type FriendBlack_req struct {
	Request_base
	ToUserId  int
	PageIndex int
	PageSize  int
	ToUserIds []int
}
