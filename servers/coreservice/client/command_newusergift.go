package client

import (
	"encoding/json"

	_ "bet24.com/log"
)

// 成长礼包
func GetNewUserGiftInfo(userId int) Response {
	msg := "GetNewUserGiftInfo"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 领取新用户登录奖励
func ReceiveNewUserGift(userId, isDouble int) Response {
	msg := "ReceiveNewUserGift"
	var req ReceiveNewUserGift_Req
	req.UserId = userId
	req.IsDouble = isDouble
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

func GetNewYearGiftInfo(userId int) Response {
	msg := "GetNewYearGiftInfo"
	var req Request_base
	req.UserId = userId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 领取新用户登录奖励
func ReceiveNewYearGift(userId, isDouble int) Response {
	msg := "ReceiveNewYearGift"
	var req ReceiveNewUserGift_Req
	req.UserId = userId
	req.IsDouble = isDouble
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}
