package client

import (
	"encoding/json"

	"bet24.com/log"
)

// 投注列表
func BetList(userId, gameId, days, pageIndex, pageSize int, betZone string) Response {
	log.Debug("coreclient.BetList userId=%d", userId)
	msg := "BetList"
	var req BetList_req
	req.UserId = userId
	req.GameID = gameId
	req.Days = days
	req.PageIndex = pageIndex
	req.PageSize = pageSize
	req.BetZone = betZone
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 游戏历史
func GameHistory(userId, pageIndex, pageSize int) Response {
	log.Debug("coreclient.GameHistory userId=%d", userId)
	msg := "GameHistory"
	var req BetList_req
	req.UserId = userId
	req.PageIndex = pageIndex
	req.PageSize = pageSize
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 游戏记录
func GetGameCount(userId, gameId int) Response {
	log.Debug("coreclient GetGameCount userId=%d", userId)
	msg := "GetGameCount"
	var req GetGameCount_req
	req.UserId = userId
	req.GameID = gameId
	d, _ := json.Marshal(req)
	return DoRequest(msg, string(d))
}

// 更新domino游戏记录
func UpdateDominoGameCount(userId, double, triple, quariter, qunitet int, cardData string) {
	log.Debug("coreclient UpdateDominoGameCount userId=%d double=%d triple=%d quariter=%d qunitet=%d cardData=%s",
		userId, double, triple, quariter, qunitet, cardData)
	msg := "UpdateDominoGameCount"
	var req UpdateDominoGameCount_req
	req.UserId = userId
	req.Double = double
	req.Triple = triple
	req.Quariter = quariter
	req.Qunitet = qunitet
	req.CardData = cardData
	d, _ := json.Marshal(req)
	DoRequest(msg, string(d))
}

// 更新qiuqiu游戏记录
func UpdateQiuqiuGameCount(userId, sixDevil, twinCards, smallCards, bigCards, qiuqiu int, cardData string) {
	log.Debug("coreclient UpdateQiuqiuGameCount userId=%d sixDevil=%d twinCards=%d smallCards=%d bigCards=%d qiuqiu=%d cardData=%s",
		userId, sixDevil, twinCards, smallCards, bigCards, qiuqiu, cardData)
	msg := "UpdateQiuqiuGameCount"
	var req UpdateQiuqiuGameCount_req
	req.UserId = userId
	req.SixDevil = sixDevil
	req.TwinCards = twinCards
	req.SmallCards = smallCards
	req.BigCards = bigCards
	req.QiuQiu = qiuqiu
	req.CardData = cardData
	d, _ := json.Marshal(req)
	DoRequest(msg, string(d))
}
