package broadcast

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/gamelist"
	"bet24.com/servers/coreservice/serviceconfig"
	notification "bet24.com/servers/micros/notification/proto"
	robot "bet24.com/servers/micros/userservices/proto"
	"bet24.com/utils"
	"encoding/json"
	"fmt"
	"strings"
)

// userId=用户ID(-1=系统 >0用户) gameId=游戏ID  priority=优先级  msg=消息  ext=扩展信息
func SendBroadcast(userId, gameId, priority int, msg, ext string) {

	//购买， 通知客户端
	d, _ := json.Marshal(notification.NotificationBroadcast{
		GameID:   gameId,
		UserId:   userId,
		Msg:      msg,
		Priority: priority,
		Ext:      ext,
	})
	notification.AddNotification(-1, notification.Notification_Broadcast, string(d))
	// log.Debug("SendBroadcast %s", msg)

	/*// 世界聊天频道
	chat.SendChatMsg(
		chat.Channel_World,
		-1,
		0,
		2,
		1,
		-1,
		"Siaran",
		"",
		msg,
		"127.0.0.1")

	return*/
}

func SendGameWinBroadcast(userId int, userName string, score int, gameId int, gameName string) {
	if score < serviceconfig.Server.MinWinBroadcast {
		return
	}

	// 机器人不广播
	if robot.IsRobot(userId) {
		return
	}

	scoreStr := utils.FormatScore(score)
	gameWinString := serviceconfig.Server.GameWinString
	if gameWinString == "" {
		gameWinString = "menang <color=#04ff0a>%s</color> di <color=#ffafd8>%s</color>. Datang dan tantang!"
	}

	gi := gamelist.GetGame(gameId)
	if gi != nil {
		gameName = gi.ChineseName
	} else {
		log.Debug("SendGameWinBroadcast failed to find game of gameId[%d]", gameId)
	}
	var str string
	varCount := strings.Count(gameWinString, "%")
	if varCount == 2 {
		str = fmt.Sprintf(gameWinString, scoreStr, gameName)
	} else if varCount == 3 {
		str = fmt.Sprintf(gameWinString, gameName, scoreStr, userName)
	}
	SendBroadcast(userId, gameId, 0, str, "")
}
