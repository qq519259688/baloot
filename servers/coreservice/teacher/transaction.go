package teacher

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/servers/coreservice/dbengine"

	"bet24.com/database"

	"bet24.com/log"
)

// 绑码奖励
func sysBindAward() []*bindAward {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*bindAward
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserTeacher_SysBindAward")
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out bindAward
		out.AwardID = int((ret[0]).(int64))
		out.Students = int((ret[1]).(int64))
		out.Profit = int((ret[2]).(int64))
		list = append(list, &out)
	}
	return list
}

// 注册
func register(userId int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserTeacher_Register")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	retRow := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRow) <= 0 {
		return 0
	}

	return int((retRow[0][0]).(int64))
}

// 师父信息
func info(teacherId int) *teacher {
	var list []*teacher
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserTeacher_GetInfo")
	statement.AddParamter("@TeacherID", database.AdParamInput, database.AdInteger, 4, teacherId)
	sqlString := statement.GenSql()
	jsonData := dbengine.CenterDB.ExecSqlJson(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
		log.Error("teacher.info.transaction json unmarshal err %v", err)
	}
	if len(list) <= 0 {
		return nil
	}
	return list[0]
}

// 绑定
func bind(studentId, teacherId int) (int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserTeacher_Bind")
	statement.AddParamter("@StudentID", database.AdParamInput, database.AdInteger, 4, studentId)
	statement.AddParamter("@TeacherID", database.AdParamInput, database.AdInteger, 4, teacherId)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return 0, 0
	}
	retCode := int((retRows[0][0]).(int64))
	amount := int((retRows[0][1]).(int64))
	return retCode, amount
}

// 徒弟列表
func students(teacherId int) []*student {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*student

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserTeacher_GetStudentList")
	statement.AddParamter("@TeacherID", database.AdParamInput, database.AdInteger, 4, teacherId)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]

		var out student
		out.StudentID = int((ret[0]).(int64))
		out.Profit = int((ret[1]).(int64))
		out.Crdate = (ret[2]).(string)

		list = append(list, &out)
	}

	return list
}

// 收益列表
func profitList(teacherId, days, pageIndex, pageSize int) (int, []*profit) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var recordCount int
	var list []*profit

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserTeacher_GetProfitList")
	statement.AddParamter("@TeacherID", database.AdParamInput, database.AdInteger, 4, teacherId)
	statement.AddParamter("@Days", database.AdParamInput, database.AdInteger, 4, days)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return recordCount, list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var out profit

			out.Rid = int((ret[0]).(int64))
			out.StudentID = int((ret[1]).(int64))
			out.Profit = int((ret[2]).(int64))
			out.Price = int((ret[3]).(int64))

			list = append(list, &out)
		}
	}

	recordCount = int((retRows[rowLen-1][0]).(int64))
	return recordCount, list
}

// 扣减收益
func reduceProfit(userId, profit, profitType int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserTeacher_ReduceProfit")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Profit", database.AdParamInput, database.AdInteger, 4, profit)
	statement.AddParamter("@ProfitType", database.AdParamInput, database.AdInteger, 4, profitType)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return 0
	}
	return int((retRows[0][0]).(int64))
}
