package teacher

// 师父信息
type teacher struct {
	RetCode     int          // 操作结果
	Students    int          // 徒弟数
	Profit      int          // 收益（未领取）
	TodayProfit int          // 今天收益
	TotalProfit int          // 累计收益
	Withdraw    int          // 提现金额
	BindAwards  []*bindAward // 绑定奖励（未领取）
}

// 绑定奖励
type bindAward struct {
	AwardID  int // 奖励ID
	Students int // 徒弟数
	Profit   int // 收益
}

// 徒弟信息
type student struct {
	StudentID int    // 徒弟ID
	Profit    int    // 收益
	Crdate    string // 绑定时间
	baseUserInfo
}

// 收益
type profit struct {
	Rid       int // 标志
	StudentID int // 徒弟ID
	Profit    int // 收益
	Price     int // 金额
	baseUserInfo
}

// 用户基础信息
type baseUserInfo struct {
	NickName string // 昵称
	Sex      int    // 性别 0=无  1=男  2=女
	FaceId   int    // 头像ID
	FaceUrl  string // 头像路径
	VipLevel int    // Vip
	IsFriend int    // 是否好友
}
