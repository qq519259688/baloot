package teacher

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

var mgr *teachermgr

func Run() {
	log.Debug("teacher running")
	mgr = newTeacherMgr()
}

// 注册
func Register(userId int) int {
	return mgr.register(userId)
}

// 信息
func Info(teacherId int) *teacher {
	return mgr.info(teacherId)
}

// 绑定
func Bind(studentId, teacherId, isSendMail int) (int, []item.ItemPack) {
	return mgr.bind(studentId, teacherId, isSendMail)
}

// 徒弟列表
func Students(teacherId int) []*student {
	return mgr.students(teacherId)
}

// 收益列表
func ProfitList(teacherId, days, pageIndex, pageSize int) (int, []*profit) {
	return mgr.profitList(teacherId, days, pageIndex, pageSize)
}

// 扣减收益
func ReduceProfit(userId, profit, profitType int) int {
	return mgr.reduceProfit(userId, profit, profitType)
}
