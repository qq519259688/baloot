package teacher

import (
	"fmt"
	"sync"

	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/friend"
	"bet24.com/servers/coreservice/serviceconfig"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	mail "bet24.com/servers/micros/userservices/proto"
	user "bet24.com/servers/micros/userservices/proto"
)

type teachermgr struct {
	sysAwards []*bindAward
	lock      *sync.RWMutex
}

func newTeacherMgr() *teachermgr {
	obj := new(teachermgr)
	obj.lock = &sync.RWMutex{}
	obj.loadAward()
	log.Debug("teacher manager running")
	return obj
}

func (t *teachermgr) loadAward() {
	list := sysBindAward()
	t.lock.Lock()
	defer t.lock.Unlock()
	t.sysAwards = list
	return
}

// 注册
func (t *teachermgr) register(userId int) int {
	return register(userId)
}

// 师父信息
func (t *teachermgr) info(teacherId int) *teacher {
	myInfo := info(teacherId)
	t.lock.RLock()
	defer t.lock.RUnlock()
	myInfo.Withdraw = serviceconfig.Server.MinWithdraw
	myInfo.BindAwards = t.sysAwards
	return myInfo
}

// 绑定
func (t *teachermgr) bind(studentId, teacherId, isSendMail int) (int, []item.ItemPack) {
	retCode, amount := bind(studentId, teacherId)
	if retCode != 1 {
		return retCode, nil
	}

	var items []item.ItemPack
	items = append(items, item.ItemPack{
		ItemId: item.Item_Chip,
		Count:  amount,
	})

	// 发邮件
	if isSendMail == 1 {
		mail.SendSysMail(studentId, &mail.SysMail{
			Id:         0,
			Title:      "Hadiah agen pengikat",
			Content:    fmt.Sprintf("Selamat, Anda telah berhasil mengikat agen %d dan mendapatkan hadiah mengikat. Harap terima hadiah lampiran tepat waktu.", teacherId),
			Status:     0,
			SourceName: "绑定代理",
			Crdate:     common.GetTimeStamp(),
			Tools:      items,
		})
	} else if isSendMail == 0 { // 直接发送奖励
		// 发送道具
		if success := inventory.AddItems(studentId, items, "绑定代理", common.LOGTYPE_BIND_TEACHER); !success {
			log.Error("teachermgr.bind AddItems fail studentId=%d teacherId=%d items=%+v", studentId, teacherId, items)
		}
	}

	return retCode, items
}

// 徒弟列表
func (t *teachermgr) students(teacherId int) []*student {
	list := students(teacherId)
	for _, v := range list {
		u := user.GetUserInfo(v.StudentID)
		if u == nil {
			log.Error("teacher.students userId=%d ==> %+v", v.StudentID, v)
			continue
		}

		v.NickName = u.NickName
		v.Sex = u.Sex
		v.FaceId = u.FaceId
		v.FaceUrl = u.FaceUrl
		v.VipLevel = u.Vip
		v.IsFriend = friend.IfFriend(teacherId, v.StudentID)
	}

	return list
}

// 收益列表
func (t *teachermgr) profitList(teacherId, days, pageIndex, pageSize int) (int, []*profit) {
	recordCount, list := profitList(teacherId, days, pageIndex, pageSize)
	for _, v := range list {
		u := user.GetUserInfo(v.StudentID)
		if u == nil {
			log.Error("teacher.profitList userId=%d ==> %+v", v.StudentID, v)
			continue
		}

		v.NickName = u.NickName
		v.Sex = u.Sex
		v.FaceId = u.FaceId
		v.FaceUrl = u.FaceUrl
		v.VipLevel = u.Vip
		v.IsFriend = friend.IfFriend(teacherId, v.StudentID)
	}
	return recordCount, list
}

// 扣减收益
func (t *teachermgr) reduceProfit(userId, profit, profitType int) int {
	return reduceProfit(userId, profit, profitType)
}
