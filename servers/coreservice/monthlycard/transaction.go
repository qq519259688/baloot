package monthlycard

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/dbengine"
)

// 月卡系统
func getSysInfo() []*sysInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("monthlycard.getSysInfo transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*sysInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_MonthlyCard_GetInfo")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info sysInfo

		info.ProductID = (ret[0]).(string)
		info.Name = (ret[1]).(string)
		info.Price = int((ret[2]).(int64))
		awards := (ret[3]).(string)
		if awards != "" {
			if err := json.Unmarshal([]byte(awards), &info.Awards); err != nil {
				log.Error("monthlycard.getSysInfo awards unmarshal fail %v", err)
			}
		}
		dayItems := (ret[4]).(string)
		if dayItems != "" {
			if err := json.Unmarshal([]byte(dayItems), &info.DayItems); err != nil {
				log.Error("monthlycard.getSysInfo dayItems unmarshal fail %v", err)
			}
		}
		info.Memo = (ret[5]).(string)

		list = append(list, &info)
	}

	return list
}

// 月卡信息
func getInfo(userId int) MonthlyCardInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("monthlycard.getInfo transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var info MonthlyCardInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserMonthlyCard_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return info
	}

	monthGift := (retRows[0][0]).(string)
	monthGiftTime := common.ParseTime(monthGift)
	info.MonthGift = common.GetStamp(monthGiftTime)

	monthExpire := (retRows[0][1]).(string)
	monthExpireTime := common.ParseTime(monthExpire)
	info.MonthExpire = common.GetStamp(monthExpireTime)

	weekGift := (retRows[0][2]).(string)
	weekGiftTime := common.ParseTime(weekGift)
	info.WeekGift = common.GetStamp(weekGiftTime)

	weekExpire := (retRows[0][3]).(string)
	weekExpireTime := common.ParseTime(weekExpire)
	info.WeekExpire = common.GetStamp(weekExpireTime)

	//log.Debug("monthlyCard.getInfo monthGift=%s monthExpire=%s weekGift=%s weekExpire=%s",
	//	monthGift, monthExpire, weekGift, weekExpire)

	return info
}

// 月卡购买
func buy(userId, monthExpire, weekExpire int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("monthlycard.buy transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserMonthlyCard_Buy")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@MonthExpire", database.AdParamInput, database.AdInteger, 4, monthExpire)
	statement.AddParamter("@WeekExpire", database.AdParamInput, database.AdInteger, 4, weekExpire)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}
	return int((retRows[0][0]).(int64))
}

// 月卡领取
func gift(userId, monthGift, weekGift int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("monthlycard.gift transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserMonthlyCard_Gift")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@MonthGift", database.AdParamInput, database.AdInteger, 4, monthGift)
	statement.AddParamter("@WeekGift", database.AdParamInput, database.AdInteger, 4, weekGift)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}
	return int((retRows[0][0]).(int64))
}
