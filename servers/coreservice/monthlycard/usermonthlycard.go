package monthlycard

import (
	"bet24.com/servers/common"
)

const (
	Valid_Days = 29 * 86400 //30天减去当天领取的
	Week_DAYS  = 6 * 86400  //7天减去当天领取的
)

type usermonthlycard struct {
	UserID int //用户id
	MonthlyCardInfo
}

func newUserMonthlyCard(userId int) *usermonthlycard {
	u := new(usermonthlycard)
	u.UserID = userId
	u.loadUserMonthlyCard()
	// log.Debug("newUserMonthlyCard userId=%d %+v", userId, u)
	return u
}

func (this *usermonthlycard) loadUserMonthlyCard() {
	this.MonthlyCardInfo = getInfo(this.UserID)
}

func (this *usermonthlycard) buyMonth() int {
	now := common.GetTimeStamp()
	day := common.GetDayIndex(now)
	if common.GetDayIndex(this.MonthExpire) < day {
		this.MonthExpire = now
	}
	this.MonthExpire = this.MonthExpire + Valid_Days
	return buy(this.UserID, this.MonthExpire, 0)
}

func (this *usermonthlycard) giftMonth() int {
	this.checkMonthlyStatus()
	if this.MonthGiftStatus != Month_GIFT_STATUS_HAVE {
		return this.MonthGiftStatus
	}

	this.MonthGift = common.GetTimeStamp()
	return gift(this.UserID, this.MonthGift, 0)
}

func (this *usermonthlycard) checkMonthTip() bool {
	this.checkMonthlyStatus()
	return this.MonthGiftStatus == Month_GIFT_STATUS_HAVE
}

func (this *usermonthlycard) checkMonthlyStatus() {
	//还没有月卡
	if this.MonthExpire <= 0 {
		this.MonthGiftStatus = Month_GIFT_STATUS_NOT
		return
	}

	now := common.GetTimeStamp()
	day := common.GetDayIndex(now)
	expireDay := common.GetDayIndex(this.MonthExpire)

	//判断是否有月卡
	if expireDay < day {
		this.MonthGiftStatus = Month_GIFT_STATUS_NOT
		return
	}

	//剩余天数
	this.MonthDays = expireDay - day

	//判断是否领取过
	if common.GetDayIndex(this.MonthGift) >= day {
		this.MonthGiftStatus = Month_GIFT_STATUS_RECEIVED
		return
	}

	this.MonthGiftStatus = Month_GIFT_STATUS_HAVE
	return
}

func (this *usermonthlycard) buyWeek() int {
	now := common.GetTimeStamp()
	day := common.GetDayIndex(now)
	if common.GetDayIndex(this.WeekExpire) < day {
		this.WeekExpire = now
	}
	this.WeekExpire = this.WeekExpire + Week_DAYS
	return buy(this.UserID, 0, this.WeekExpire)
}

func (this *usermonthlycard) giftWeek() int {
	//还没有周卡
	if this.WeekExpire <= 0 {
		return 0
	}

	now := common.GetTimeStamp()
	day := common.GetDayIndex(now)

	//判断是否有周卡
	if common.GetDayIndex(this.WeekExpire) < day {
		return 0
	}

	//判断是否领取过
	if common.GetDayIndex(this.WeekGift) >= day {
		return 0
	}

	this.WeekGift = now
	return gift(this.UserID, 0, this.WeekGift)
}
