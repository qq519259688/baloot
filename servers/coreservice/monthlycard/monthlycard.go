package monthlycard

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	Month_ProductId = "200007" //月卡
	Week_ProductId  = "100002" //周卡
)

const (
	Month_GIFT_STATUS_NOT      = 0 //0=不能领取
	Month_GIFT_STATUS_HAVE     = 1 //1=可领取
	Month_GIFT_STATUS_RECEIVED = 2 //2=已领取
)

type MonthlyCardInfo struct {
	MonthGift       int //月卡领取时间戳
	MonthExpire     int //月卡过期时间戳
	MonthGiftStatus int //月卡领取标识(0=不能领取  1=可领取  2=已领取)
	MonthDays       int //月卡剩余天数
	WeekGift        int //周卡领取时间戳
	WeekExpire      int //周卡过期时间戳
}

var mgr *monthlycardmgr

func Run() {
	mgr = NewMonthlyCardMgr()
	log.Debug("monthcard running")
}

// 获取月卡系统信息
func GetSysInfo() []*sysInfo {
	return mgr.getSysList()
}

// 获取用户月卡信息
func GetUserInfo(userId int) *usermonthlycard {
	return mgr.getUserInfo(userId)
}

// 购买月卡
func BuyMonth(userId int) (int, []item.ItemPack) {
	return mgr.buyMonth(userId)
}

// 领取月卡
func GiftMonth(userId int) (int, []item.ItemPack) {
	return mgr.giftMonth(userId)
}

func CheckMonthTip(userId int) bool {
	return mgr.checkMonthTip(userId)
}

// 购买周卡
func BuyWeek(userId int) (int, []item.ItemPack) {
	return mgr.buyWeek(userId)
}

// 领取周卡
func GiftWeek(userId int) (int, []item.ItemPack) {
	return mgr.giftWeek(userId)
}

func AddUser(userId int) {
	mgr.onUserEnter(userId)
}

func RemoveUser(userId int) {
	mgr.onUserExit(userId)
}
