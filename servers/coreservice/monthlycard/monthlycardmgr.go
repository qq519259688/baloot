package monthlycard

import (
	"sync"

	"bet24.com/log"
	"bet24.com/servers/common"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
)

type monthlycardmgr struct {
	user_list map[int]*usermonthlycard
	sys_list  []*sysInfo
	lock      *sync.RWMutex
}

type sysInfo struct {
	ProductID string          //产品ID
	Name      string          //名称
	Price     int             //价格
	Awards    []item.ItemPack //立即赠送的物品
	DayItems  []item.ItemPack //每天物品
	Memo      string          //备注
}

func NewMonthlyCardMgr() *monthlycardmgr {
	obj := new(monthlycardmgr)
	obj.user_list = make(map[int]*usermonthlycard)
	obj.lock = &sync.RWMutex{}
	obj.loadItems()
	log.Debug("monthlycard manager running")
	return obj
}

func (mc *monthlycardmgr) loadItems() {
	list := getSysInfo()

	mc.lock.Lock()
	defer mc.lock.Unlock()
	mc.sys_list = append(mc.sys_list, list...)
}

func (mc *monthlycardmgr) getUserInfo(userId int) *usermonthlycard {
	mc.lock.RLock()
	user, ok := mc.user_list[userId]
	mc.lock.RUnlock()
	if !ok {
		user = newUserMonthlyCard(userId)
	}

	user.checkMonthlyStatus()
	return user
}

func (mc *monthlycardmgr) getSysList() []*sysInfo {
	mc.lock.RLock()
	defer mc.lock.RUnlock()
	return mc.sys_list
}

func (mc *monthlycardmgr) getInfo(productId string) *sysInfo {
	for _, v := range mc.sys_list {
		if v.ProductID == productId {
			return v
		}
	}
	return nil
}

func (mc *monthlycardmgr) buyMonth(userId int) (int, []item.ItemPack) {
	u := mc.getUserInfo(userId)
	if u == nil {
		log.Debug("monthlycard.buyMonth user[%d] is not exist", userId)
		u = newUserMonthlyCard(userId)
	}

	//购买
	if ret := u.buyMonth(); ret != 1 {
		return 0, nil
	}

	return 1, nil
}

func (mc *monthlycardmgr) giftMonth(userId int) (int, []item.ItemPack) {
	u := mc.getUserInfo(userId)
	if u == nil {
		//log.Debug("monthlycard.giftMonth user[%d] is not exist", userId)
		return 0, nil
	}

	//领取
	retCode := u.giftMonth()
	if retCode != 1 {
		return retCode, nil
	}

	info := mc.getInfo(Month_ProductId)

	inventory.AddItems(userId, info.DayItems, "领取月卡福利", common.LOGTYPE_SEND_MONTHLYCARD)

	return 1, info.DayItems
}

func (mc *monthlycardmgr) checkMonthTip(userId int) bool {
	u := mc.getUserInfo(userId)
	if u == nil {
		log.Debug("monthlycard.giftMonth user[%d] is not exist", userId)
		return false
	}

	return u.checkMonthTip()
}

func (mc *monthlycardmgr) buyWeek(userId int) (int, []item.ItemPack) {
	u := mc.getUserInfo(userId)
	if u == nil {
		log.Debug("monthlycard.buyWeek user[%d] is not exist", userId)
		u = newUserMonthlyCard(userId)
	}

	//购买
	if ret := u.buyWeek(); ret != 1 {
		return 0, nil
	}

	return 1, nil
}

func (mc *monthlycardmgr) giftWeek(userId int) (int, []item.ItemPack) {
	u := mc.getUserInfo(userId)
	if u == nil {
		log.Debug("monthlycard.giftWeek user[%d] is not exist", userId)
		return 0, nil
	}

	//领取
	retCode := u.giftWeek()
	if retCode != 1 {
		return 0, nil
	}

	info := mc.getInfo(Week_ProductId)

	//领取
	inventory.AddItems(userId, info.DayItems, "领取周卡福利", common.LOGTYPE_SEND_MONTHLYCARD)

	return 1, info.DayItems
}

func (mc *monthlycardmgr) onUserEnter(userId int) {
	mc.lock.RLock()
	_, ok := mc.user_list[userId]
	mc.lock.RUnlock()
	if !ok {
		o := newUserMonthlyCard(userId)
		mc.lock.Lock()
		mc.user_list[userId] = o
		mc.lock.Unlock()
	}
}

func (mc *monthlycardmgr) onUserExit(userId int) {
	mc.lock.Lock()
	defer mc.lock.Unlock()
	delete(mc.user_list, userId)
}
