package vitality

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 获取活跃度
func getInfo(userId int) Vitality {
	defer func() {
		if err := recover(); err != nil {
			log.Error("vitality.getInfo transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserVitality_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return Vitality{}
	}

	ret := retRows[0]
	var out Vitality
	out.DayIndex = int((ret[0]).(int64))
	out.DayPoint = int((ret[1]).(int64))
	out.WeekIndex = int((ret[2]).(int64))
	out.WeekPoint = int((ret[3]).(int64))
	return out
}

// 修改活跃度
func update(userId int, info Vitality) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("vitality.update transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserVitality_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@DayIndex", database.AdParamInput, database.AdInteger, 4, info.DayIndex)
	statement.AddParamter("@DayPoint", database.AdParamInput, database.AdInteger, 4, info.DayPoint)
	statement.AddParamter("@WeekIndex", database.AdParamInput, database.AdInteger, 4, info.WeekIndex)
	statement.AddParamter("@WeekPoint", database.AdParamInput, database.AdInteger, 4, info.WeekPoint)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}

	return int((retRows[0][0]).(int64))
}
