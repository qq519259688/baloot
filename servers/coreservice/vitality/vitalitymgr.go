package vitality

import (
	"sync"

	"bet24.com/log"
)

type vitalitymgr struct {
	user_list map[int]*uservitality
	lock      *sync.RWMutex
}

func NewVitalityMgr() *vitalitymgr {
	obj := new(vitalitymgr)
	obj.user_list = make(map[int]*uservitality)
	obj.lock = &sync.RWMutex{}
	log.Debug("vitalitymanager running")
	return obj
}

func (this *vitalitymgr) getUserInfo(userId int) *uservitality {
	this.lock.RLock()
	user, ok := this.user_list[userId]
	this.lock.RUnlock()
	if !ok {
		user = newUserVitality(userId)
		if user == nil {
			return nil
		}
		this.lock.Lock()
		this.user_list[userId] = user
		this.lock.Unlock()
	}
	return user
}

func (this *vitalitymgr) addPoint(userId, point int) int {
	user := this.getUserInfo(userId)
	if user == nil {
		log.Debug("vitality.addPoint user[%d] is not exist", userId)
		return 0
	}

	return user.addPoint(point)
}

func (vm *vitalitymgr) onUserEnter(userId int) {
	vm.lock.RLock()
	_, ok := vm.user_list[userId]
	vm.lock.RUnlock()
	if ok {
		return
	}
	usr := newUserVitality(userId)
	vm.lock.Lock()
	vm.user_list[userId] = usr
	vm.lock.Unlock()
}

func (vm *vitalitymgr) onUserExit(userId int) {
	vm.lock.Lock()
	defer vm.lock.Unlock()
	delete(vm.user_list, userId)
}
