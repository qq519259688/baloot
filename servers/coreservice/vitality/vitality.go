package vitality

import "bet24.com/log"

//活跃度
type Vitality struct {
	DayIndex  int //天索引
	DayPoint  int //天活跃度
	WeekIndex int //周索引
	WeekPoint int //周活跃度
}

var mgr *vitalitymgr

func Run() {
	mgr = NewVitalityMgr()
	log.Debug("vitality running")
}

func GetUserInfo(userId int) *uservitality {
	return mgr.getUserInfo(userId)
}

func AddPoint(userId, point int) int {
	return mgr.addPoint(userId, point)
}

func AddUser(userId int) {
	mgr.onUserEnter(userId)
}

func RemoveUser(userId int) {
	mgr.onUserExit(userId)
}
