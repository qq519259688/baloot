package vitality

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	notification "bet24.com/servers/micros/notification/proto"
)

type uservitality struct {
	userId int //用户id
	Vitality
}

func newUserVitality(userId int) *uservitality {
	u := new(uservitality)
	u.userId = userId
	u.loadUserVitality()
	//log.Debug("newUserVitality userId=%d %+v", userId, u)
	return u
}

func (this *uservitality) loadUserVitality() {
	this.Vitality = getInfo(this.userId)
	isUpdate := false
	ts := common.GetTimeStamp()

	//天活跃度
	dayIndex := common.GetDayIndex(ts)
	if this.DayIndex != dayIndex {
		this.DayPoint = 0
		this.DayIndex = dayIndex
		isUpdate = true
	}

	//周活跃度
	weekIndex := common.GetWeekIndex(ts)
	if this.WeekIndex != weekIndex {
		this.WeekPoint = 0
		this.WeekIndex = weekIndex
		isUpdate = true
	}

	if !isUpdate {
		return
	}

	//去更新
	this.update()
}

func (this *uservitality) addPoint(point int) int {
	if point <= 0 {
		log.Release("vitality.addPoint userId=%d info=%+v addPoint=%d", this.userId, this.Vitality, point)
		return 0
	}

	ts := common.GetTimeStamp()

	//天活跃度
	dayIndex := common.GetDayIndex(ts)
	if this.DayIndex == dayIndex {
		this.DayPoint = this.DayPoint + point
	} else {
		this.DayPoint = point
		this.DayIndex = dayIndex
	}

	//周活跃度
	weekIndex := common.GetWeekIndex(ts)
	if this.WeekIndex == weekIndex {
		this.WeekPoint = this.WeekPoint + point
	} else {
		this.WeekPoint = point
		this.WeekIndex = weekIndex
	}

	//去更新
	this.update()

	//通知客户端
	go notification.AddNotification(this.userId, notification.Notification_Vitality, "")

	return 1
}

func (this *uservitality) update() {
	go update(this.userId, this.Vitality)
}
