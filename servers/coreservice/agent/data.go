package agent

// 代理信息
type info_out struct {
	Members      int    // 会员数
	ValidMembers int    // 有效会员数
	Commission   int    // 佣金
	Profit       int    // 收益(已提取)
	Enabled      int    // 是否启用（1=启用  其他关闭）
	Crdate       string // 创建时间
}

// 统计信息
type stat_out struct {
	Commission  int // 佣金
	Profit      int // 收益（已提取）
	RecordCount int // 记录数
	List        []statModel
}

type statModel struct {
	DateFlag   string // 日期
	Commission int    // 佣金
	Direct     int    // 属级(0=提取  1=直属  2=间属)
}

// 收益信息
type commissionInfo struct {
	ChineseName    string // 游戏名称
	FromUserID     int    // 源用户ID
	FromNickName   string // 源用户昵称
	FaceID         int    // 头像ID
	FaceUrl        string // 头像URL
	Sex            int    // 性别
	WantCommission int    // 变化收益
	Crdate         string // 时间
}

// 收益
type commission_out struct {
	Commission  int // 佣金
	RecordCount int // 记录数
	List        []commissionInfo
}

// 配置信息
type configInfo struct {
	IsOpen       int          // 是否开启（1=启用  其他关闭）
	BindSend     int          // 绑码赠送
	BetOneDirect int          // 流水1级代理返佣(以万为基数)
	BetTwoDirect int          // 流水2级代理返佣(以万为基数)
	TaxOneDirect int          // 服务费1级代理返佣(以万为基数)
	TaxTwoDirect int          // 服务费2级代理返佣(以万为基数)
	PayOneDirect int          // 充值1级代理返佣(以万为基数)
	PayTwoDirect int          // 充值2级代理返佣(以万为基数)
	BindAwards   []*bindAward // 绑定奖励
}

type bindAward struct {
	Members int // 有效会员数
	Profit  int // 奖励额
}

// 会员列表
type memberList struct {
	RecordCount int // 记录数
	OnlineCount int // 在线人数
	List        []memberInfo
}

type memberInfo struct {
	UserID   int    // 用户ID
	NickName string // 用户昵称
	FaceID   int    // 头像ID
	FaceUrl  string // 头像URL
	Sex      int    // 性别
	BindTime string // 绑定时间
	IsOnline int    // 是否在线 1=在线 0=离线
}
