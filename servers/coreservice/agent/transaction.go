package agent

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/servers/coreservice/dbengine"

	"bet24.com/database"
	"bet24.com/log"
)

// 申请
func apply(userId int, memo string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAgent_Apply")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Memo", database.AdParamInput, database.AdNVarChar, 4000, memo)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return 0
	}
	return int((retRows[0][0]).(int64))
}

// 绑码
func bind(userId, higherUserID int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAgent_Bind")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@HigherUserID", database.AdParamInput, database.AdInteger, 4, higherUserID)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return 0
	}

	return int((retRows[0][0]).(int64))
}

// 代理信息
func info(userId int) *info_out {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAgent_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return nil
	}

	return &info_out{
		Members:      int((retRows[0][0]).(int64)),
		Commission:   int((retRows[0][1]).(int64)),
		Profit:       int((retRows[0][2]).(int64)),
		Enabled:      int((retRows[0][3]).(int64)),
		Crdate:       (retRows[0][4]).(string),
		ValidMembers: int((retRows[0][5]).(int64)),
	}
}

// 会员列表
func members(userId, pageIndex, pageSize int) *memberList {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var out memberList
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAgent_GetMembers")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, out.RecordCount)
	statement.AddParamter("@OnlineCount", database.AdParamOutput, database.AdInteger, 4, out.OnlineCount)
	sqlString := statement.GenSql()
	// log.Debug(sqlString)
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return &out
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info memberInfo

			info.UserID = int((ret[0]).(int64))
			info.BindTime = (ret[1]).(string)
			info.IsOnline = int((ret[2]).(int64))
			out.List = append(out.List, info)
		}
	}

	out.RecordCount = int((retRows[rowLen-1][0]).(int64))
	out.OnlineCount = int((retRows[rowLen-1][1]).(int64))
	return &out
}

// 代理统计
func commissionStat(userId, pageIndex, pageSize int) *stat_out {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var out stat_out
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAgent_CommissionStat")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@Commission", database.AdParamOutput, database.AdInteger, 4, out.Commission)
	statement.AddParamter("@Profit", database.AdParamOutput, database.AdInteger, 4, out.Profit)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, out.RecordCount)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return &out
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info statModel

			info.DateFlag = (ret[0]).(string)
			info.Commission = int((ret[1]).(int64))
			info.Direct = int((ret[2]).(int64))
			out.List = append(out.List, info)
		}
	}

	out.Commission = int((retRows[rowLen-1][0]).(int64))
	out.Profit = int((retRows[rowLen-1][1]).(int64))
	out.RecordCount = int((retRows[rowLen-1][2]).(int64))
	return &out
}

// 收益记录
func commissionLog(userId, fromUserId int, pageIndex, pageSize int) *commission_out {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	out := &commission_out{}

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAgent_CommissionLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@FromUserID", database.AdParamInput, database.AdInteger, 4, fromUserId)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@Commission", database.AdParamOutput, database.AdInteger, 4, out.Commission)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, out.RecordCount)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return out
	}

	if rowLen > 1 {
		out.List = make([]commissionInfo, rowLen-1)
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			info := &out.List[i]

			info.ChineseName = (ret[0]).(string)
			info.FromUserID = int((ret[1]).(int64))
			info.WantCommission = int((ret[2]).(int64))
			info.Crdate = (ret[3]).(string)
		}
	}

	out.Commission = int((retRows[rowLen-1][0]).(int64))
	out.RecordCount = int((retRows[rowLen-1][1]).(int64))
	return out
}

// 代理配置
func getConfigInfo() *configInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAgent_ConfigInfo")
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return nil
	}

	ret := retRows[0]

	return &configInfo{
		IsOpen:       int((ret[0]).(int64)),
		BindSend:     int((ret[1]).(int64)),
		BetOneDirect: int((ret[2]).(int64)),
		BetTwoDirect: int((ret[3]).(int64)),
		TaxOneDirect: int((ret[4]).(int64)),
		TaxTwoDirect: int((ret[5]).(int64)),
		PayOneDirect: int((ret[6]).(int64)),
		PayTwoDirect: int((ret[7]).(int64)),
	}
}

// 提取收益
func commissionToAmount(userId int, ipAddress string) (int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAgent_CommissionToAmount")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return 0, 0
	}

	ret := retRows[0]
	retCode := int((ret[0]).(int64))
	commission := int((ret[1]).(int64))
	return retCode, commission
}

// 获取组信息
func getGroup(userId int) []*linkInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*linkInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAgent_GetGroup")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return list
	}

	info := (retRows[0][0]).(string)
	if info == "" {
		return list
	}

	if err := json.Unmarshal([]byte(info), &list); err != nil {
		log.Error("agent.transaction.getGroup info=%s err=%v", info, err)
	}

	return list
}

// 修改数组信息
func updateGroup(userId int, info string) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAgent_UpdateGroup")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Info", database.AdParamInput, database.AdNVarChar, 4000, info)
	sqlString := statement.GenSql()
	log.Debug(sqlString)
	dbengine.CenterDB.ExecSql(sqlString)
}

// 有效会员奖励
func getBindAwards() []*bindAward {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*bindAward
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAgent_BindAwards")
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out bindAward
		out.Members = int((ret[0]).(int64))
		out.Profit = int((ret[1]).(int64))
		list = append(list, &out)
	}
	return list
}
