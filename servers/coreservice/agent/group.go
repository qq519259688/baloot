package agent

import (
	"context"
	"encoding/json"
	"time"

	"bet24.com/log"
)

// 链接类型
const (
	_             = iota // 0=无效
	LINK_FACEBOOK        // 1=facebook 链接
	LINK_TELGRAM         // 2=telgram 链接
	LINK_WHATSAPP        // 3=whatsapp 链接
)

// 群信息
type groupInfo struct {
	UserID int // 用户ID
	Links  []*linkInfo
}

// 链接信息
type linkInfo struct {
	Id   int    // 类型
	Name string // 群名称
	Url  string // Url
}

func newUserGroup(userId int) *groupInfo {
	list := getGroup(userId)
	g := &groupInfo{}
	g.UserID = userId
	g.Links = append(g.Links, list...)
	return g
}

func (this *groupInfo) updateLink(id int, name, url string) int {
	if id != LINK_FACEBOOK && id != LINK_TELGRAM && id != LINK_WHATSAPP {
		log.Debug("agent.group.updateLink invalid id=%d name=%s url=%s", id, name, url)
		return 0
	}

	if name == "" || url == "" {
		log.Debug("agent.group.updateLink is empty id=%d name=%s url=%s", id, name, url)
		return 0
	}

	for _, v := range this.Links {
		if v.Id != id {
			continue
		}
		v.Name = name
		v.Url = url

		go this.saveLinkToDB()
		return 1
	}

	// 新增
	this.Links = append(this.Links, &linkInfo{
		Id:   id,
		Name: name,
		Url:  url,
	})

	go this.saveLinkToDB()
	return 1
}

func (this *groupInfo) saveLinkToDB() {
	buf, err := json.Marshal(this.Links)
	if err != nil {
		log.Error("agent.group.saveLinkToDB json marshal fail %v", err)
		return
	}
	updateGroup(this.UserID, string(buf))
}

func (this *groupInfo) clearOut() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Hour)
	select {
	case <-ctx.Done():
		// 清理
		mgr.clear(this.UserID)
	}
	cancel()
}
