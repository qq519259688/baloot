package agent

import (
	"encoding/json"
	"sync"

	task "bet24.com/servers/micros/task/proto"

	user "bet24.com/servers/micros/userservices/proto"

	item "bet24.com/servers/micros/item_inventory/proto"

	"bet24.com/servers/common"
	inventory "bet24.com/servers/micros/item_inventory/proto"

	"bet24.com/log"
)

type agentmgr struct {
	ConfigInfo *configInfo        // 代理配置
	Groups     map[int]*groupInfo // 代理群信息
	lock       *sync.RWMutex
}

func newAgentMgr() *agentmgr {
	obj := new(agentmgr)
	obj.lock = &sync.RWMutex{}
	obj.Groups = make(map[int]*groupInfo)
	obj.load()
	log.Debug("agent manager running")
	return obj
}

// 加载系统信息
func (this *agentmgr) load() {
	this.ConfigInfo = getConfigInfo()
	awards := getBindAwards()
	this.ConfigInfo.BindAwards = append(this.ConfigInfo.BindAwards, awards...)
}

// 获取组信息
func (this *agentmgr) getGroup(userId int) *groupInfo {
	if userId <= 0 {
		return nil
	}

	// 判断是否开启
	if !this.isOpen() {
		return nil
	}

	this.lock.RLock()
	userGroup, ok := this.Groups[userId]
	this.lock.RUnlock()
	if !ok {
		userGroup = newUserGroup(userId)
		this.lock.Lock()
		this.Groups[userId] = userGroup
		this.lock.Unlock()
	}

	return userGroup
}

// 设置群组信息
func (this *agentmgr) updateGroup(userId int, id int, groupName, groupLink string) int {
	// 判断是否开启
	if !this.isOpen() {
		return 21
	}

	// 获取代理信息
	info := this.info(userId)
	if info == nil {
		return 11
	}

	userGroup := this.getGroup(userId)
	return userGroup.updateLink(id, groupName, groupLink)
}

// 配置信息
func (this *agentmgr) getJsonConfigs() string {
	d, _ := json.Marshal(this.ConfigInfo)
	return string(d)
}

// 申请
func (this *agentmgr) apply(userId int, memo string) int {
	// 判断是否开启
	if !this.isOpen() {
		return 21
	}

	if userId <= 0 {
		return 11
	}
	return apply(userId, memo)
}

// 是否开启
func (this *agentmgr) isOpen() bool {
	return this.ConfigInfo.IsOpen == 1
}

// 绑码
func (this *agentmgr) bind(userId, higherUserId int) (int, []item.ItemPack) {
	// 判断是否开启
	if !this.isOpen() {
		return 21, nil
	}

	if userId <= 0 || higherUserId <= 0 || userId == higherUserId {
		return 11, nil
	}

	retCode := bind(userId, higherUserId)

	var items []item.ItemPack

	// 绑码成功
	if retCode == 1 && this.ConfigInfo.BindSend > 0 {
		items = append(items, item.ItemPack{
			ItemId: item.Item_Gold,
			Count:  this.ConfigInfo.BindSend,
		})

		// 加道具
		if success := inventory.AddItems(userId, items, "绑码赠送", common.LOGTYPE_BIND_TEACHER); !success {
			log.Error("agentMgr.bind AddItems fail userId=%d higherUserId=%d items=%+v", userId, higherUserId, items)
		}

		// 刷新任务
		go task.RefreshTask(userId)
	}

	return retCode, items
}

// 代理信息
func (this *agentmgr) info(userId int) *info_out {
	return info(userId)
}

// 会员
func (this *agentmgr) members(userId, pageIndex, pageSize int) *memberList {
	ret := members(userId, pageIndex, pageSize)
	for i := 0; i < len(ret.List); i++ {
		u := user.GetUserInfo(ret.List[i].UserID)
		if u == nil {
			log.Error("agentMgr.members userId=%d", ret.List[i].UserID)
			return ret
		}
		ret.List[i].NickName = u.NickName
		ret.List[i].Sex = u.Sex
		ret.List[i].FaceID = u.FaceId
		ret.List[i].FaceUrl = u.FaceUrl
	}
	return ret
}

// 代理统计
func (this *agentmgr) stat(userId, pageIndex, pageSize int) *stat_out {
	return commissionStat(userId, pageIndex, pageSize)
}

// 收益记录
func (this *agentmgr) commissionLog(userId, fromUserId, pageIndex, pageSize int) *commission_out {
	ret := commissionLog(userId, fromUserId, pageIndex, pageSize)
	for i := 0; i < len(ret.List); i++ {
		u := user.GetUserInfo(ret.List[i].FromUserID)
		if u == nil {
			log.Error("agentMgr.commissionLog userId=%d", ret.List[i].FromUserID)
			return ret
		}
		ret.List[i].FromNickName = u.NickName
		ret.List[i].Sex = u.Sex
		ret.List[i].FaceID = u.FaceId
		ret.List[i].FaceUrl = u.FaceUrl
	}
	return ret
}

// 提取收益
func (this *agentmgr) commissionToAmount(userId int, ipAddress string) (int, int) {
	return commissionToAmount(userId, ipAddress)
}

func (this *agentmgr) clear(userId int) {
	this.lock.Lock()
	defer this.lock.Unlock()
	delete(this.Groups, userId)
}
