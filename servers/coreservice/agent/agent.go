package agent

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

var mgr *agentmgr

func Run() {
	log.Debug("agent running")
	mgr = newAgentMgr()
}

// 刷新配置信息
func RefreshConfig() {
	mgr.load()
}

// 获取配置信息
func GetJsonConfigs() string {
	return mgr.getJsonConfigs()
}

// 申请
func Apply(userId int, memo string) int {
	return mgr.apply(userId, memo)
}

// 绑码
func Bind(userId, higherUserId int) (int, []item.ItemPack) {
	return mgr.bind(userId, higherUserId)
}

// 代理信息
func Info(userId int) *info_out {
	return mgr.info(userId)
}

// 会员
func Members(userId, pageIndex, pageSize int) *memberList {
	return mgr.members(userId, pageIndex, pageSize)
}

// 代理统计
func Stat(userId, pageIndex, pageSize int) *stat_out {
	return mgr.stat(userId, pageIndex, pageSize)
}

// 收益记录
func CommissionLog(userId, fromUserId, pageIndex, pageSize int) *commission_out {
	return mgr.commissionLog(userId, fromUserId, pageIndex, pageSize)
}

// 提取收益
func CommissionToAmount(userId int, ipAddress string) (int, int) {
	return mgr.commissionToAmount(userId, ipAddress)
}

// 获取群组
func GetGroup(userId int) *groupInfo {
	return mgr.getGroup(userId)
}

// 修改群组
func UpdateGroup(userId int, id int, name, url string) int {
	return mgr.updateGroup(userId, id, name, url)
}
