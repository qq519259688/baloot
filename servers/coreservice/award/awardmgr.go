package award

import (
	"fmt"
	"math/rand"
	"sync"

	"bet24.com/servers/coreservice/agent"

	"bet24.com/log"
	"bet24.com/servers/common"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
)

type awardMgr struct {
	lock *sync.RWMutex
	list []*awardInfo
}

// 奖励信息
type awardInfo struct {
	ActiveID   int      // 活动ID
	ActiveName string   // 活动名称
	TotalTimes int      // 总次数
	PerDay     int      // 每隔几天
	Awards     []awards // 奖励
}

// 奖励内容
type awards struct {
	Chance int             // 概率(以万为基数)
	Items  []item.ItemPack // 道具
}

func newAwardMgr() *awardMgr {
	ret := new(awardMgr)
	ret.lock = &sync.RWMutex{}
	ret.loadData()
	log.Debug("award manager running")
	return ret
}

func (this *awardMgr) loadData() {
	list := getSysInfo()

	for i := 0; i < len(list); i++ {
		chance := 0
		for j := 0; j < len(list[i].Awards); j++ {
			chance += list[i].Awards[j].Chance * 100
			list[i].Awards[j].Chance = chance
		}
	}

	this.lock.Lock()
	defer this.lock.Unlock()
	this.list = list

	for _, v := range this.list {
		log.Debug("awardmgr.loadData ActiveId[%d] ActiveName[%s] ==> %+v", v.ActiveID, v.ActiveName, v)
	}
}

// 获取信息
func (this *awardMgr) getInfo(activeId int) *awardInfo {
	this.lock.RLock()
	defer this.lock.RUnlock()

	for _, v := range this.list {
		if v.ActiveID == activeId {
			return v
		}
	}

	return nil
}

// 发送奖励
func (this *awardMgr) sendAward(userId, activeId, hallType int, ipAddress string) (int, []item.ItemPack) {
	info := this.getInfo(activeId)
	if info == nil {
		log.Debug("award.awardmgr.sendAward userId=%d activeId=%d is not exist", userId, activeId)
		return 11, nil
	}

	if info.TotalTimes <= 0 {
		log.Debug("award.awardmgr.sendAward userId=%d activeId=%d info=%v is invalid", userId, activeId, info)
		return 12, nil
	}

	logType := 0

	// 代理活动
	if activeId == ActiveAgentShare {
		// 判断是否是代理
		if g := agent.Info(userId); g.Enabled != 1 {
			return 11, nil
		}

		logType = common.LOGTYPE_AWARD_AGENT_SHARE
	} else if activeId == ActiveTaskAnswer {
		logType = common.LOGTYPE_AWARD_ANSWER
	}

	retCode := sendAward(userId, info.ActiveID, info.TotalTimes, info.PerDay, ipAddress)
	if retCode != 1 {
		return retCode, nil
	}

	// 生成随机数
	rnd := rand.Intn(10000) + 1

	// 抽中的道具
	var hitItems []item.ItemPack
	for _, v := range info.Awards {
		hitItems = v.Items
		if rnd < v.Chance {
			break
		}
	}

	// log.Debug("awardmgr.sendAward userId=%d activeId=%d hallType=%d rnd=%d hitItems=%+v",
	// 	userId, activeId, hallType, rnd, hitItems)

	var items []item.ItemPack

	// 3=不区分大厅类型
	if hallType == 3 {
		items = hitItems
	} else {
		for _, v := range hitItems {
			if hallType == 1 { // 1=金币大厅
				if v.ItemId == item.Item_Gold {
					items = append(items, v)
				}
			} else if hallType == 2 { // 2=元宝大厅
				if v.ItemId == item.Item_Chip {
					items = append(items, v)
				}
			}
		}
	}

	remark := fmt.Sprintf("奖励模块[%s]", info.ActiveName)

	go inventory.AddItems(userId, items, remark, logType)

	return retCode, items
}
