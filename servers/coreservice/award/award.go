package award

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	_                = iota
	ActiveTaskAnswer // 问卷调查
	ActiveAgentShare // 代理分享
)

// 奖励模块
var mgr *awardMgr

func Run() {
	mgr = newAwardMgr()
}

// 发送奖励
func SendAward(userId, activeId, hallType int, ipAddress string) (int, []item.ItemPack) {
	return mgr.sendAward(userId, activeId, hallType, ipAddress)
}
