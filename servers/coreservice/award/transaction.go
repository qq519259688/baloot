package award

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 系统信息
func getSysInfo() []*awardInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAward_GetSysInfo")
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*awardInfo
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out awardInfo
		out.ActiveID = int((ret[0]).(int64))
		out.ActiveName = (ret[1]).(string)
		out.TotalTimes = int((ret[2]).(int64))
		if awards := (ret[3]).(string); len(awards) > 0 {
			if err := json.Unmarshal([]byte(awards), &out.Awards); err != nil {
				log.Error("award.getSysInfo unmarshal fail %v", err)
			}
		}
		out.PerDay = int((ret[4]).(int64))
		list = append(list, &out)
	}
	return list
}

// 发送奖励
func sendAward(userId, activeId, totalTimes, perDay int, ipAddress string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserAward_Send")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ActiveID", database.AdParamInput, database.AdInteger, 4, activeId)
	statement.AddParamter("@TotalTimes", database.AdParamInput, database.AdInteger, 4, totalTimes)
	statement.AddParamter("@PerDay", database.AdParamInput, database.AdInteger, 4, perDay)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return 0
	}

	return int((retRows[0][0]).(int64))
}
