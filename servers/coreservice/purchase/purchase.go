package purchase

const (
	STATUS_IDLE     = iota // 0=空闲
	STATUS_ACTIVITY        // 1=活动中
	STATUS_PRIZE           // 2=派奖
	STATUS_END             // 3=结束
)

var mgr *purchaseMgr

func Run() {
	mgr = newPurchaseMgr()
}

// 100K购信息(期数\元宝数量\倒计时\状态\消耗金币)
func GetSysInfo() (int, int, int64, int, int) {
	return mgr.getSysInfo()
}

// 100K购抽奖(返回操作结果\抽奖码)
func Bet(userId int, ipAddress string) (int, int) {
	return mgr.bet(userId, ipAddress)
}

// 100K购用户抽奖信息
func GetUserInfo(userId int) []int {
	return mgr.getUserInfo(userId)
}

// 小红点提示
func CheckTip() bool {
	return mgr.checkTip()
}
