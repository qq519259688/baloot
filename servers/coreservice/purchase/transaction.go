package purchase

import (
	"runtime/debug"
	"time"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/dbengine"
)

// 100购活动信息
func sysInfo() (int, time.Time, time.Time, int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Purchase_GetInfo")
	sqlString := statement.GenSql()
	// log.Debug(sqlString)
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return 0, time.Now(), time.Now(), 0, 0
	}

	ret := retRows[0]
	sn := int((ret[0]).(int64))
	beginTimeStr := (ret[1]).(string)
	beginTime := common.ParseTime(beginTimeStr)
	endTimeSr := (ret[2]).(string)
	endTime := common.ParseTime(endTimeSr)
	luckNumber := int((ret[3]).(int64))
	status := int((ret[4]).(int64))
	return sn, beginTime, endTime, luckNumber, status
}

// 100购活动开启
func open(sn int, beginTime, endTime time.Time, luckNumber, status int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Purchase_Open")
	statement.AddParamter("@SN", database.AdParamInput, database.AdInteger, 4, sn)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 32, beginTime.Format(common.Layout))
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 32, endTime.Format(common.Layout))
	statement.AddParamter("@LuckNumber", database.AdParamInput, database.AdInteger, 4, luckNumber)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, status)
	sqlString := statement.GenSql()
	// log.Debug(sqlString)
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return
	}

	if retCode := int((retRows[0][0]).(int64)); retCode != 1 {
		log.Error("purchase.open transaction fail sn=%d beginTime=%v endTime=%v luckNumber=%d status=%d",
			sn, beginTime.Format(common.Layout), endTime.Format(common.Layout), luckNumber, status)
	}
	return
}

// 100购活动更新
func update(sn, luckNumber, status int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Purchase_Update")
	statement.AddParamter("@SN", database.AdParamInput, database.AdInteger, 4, sn)
	statement.AddParamter("@LuckNumber", database.AdParamInput, database.AdInteger, 4, luckNumber)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, status)
	sqlString := statement.GenSql()
	// log.Debug(sqlString)
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return
	}

	if retCode := int((retRows[0][0]).(int64)); retCode != 1 {
		log.Error("purchase.update transaction fail sn=%d luckNumber=%d status=%d",
			sn, luckNumber, status)
	}
	return
}

// 100购活动用户信息
func userInfo(sn int) map[int]*userPurchase {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	list := make(map[int]*userPurchase)
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Purchase_GetUserInfo")
	statement.AddParamter("@SN", database.AdParamInput, database.AdInteger, 4, sn)
	sqlString := statement.GenSql()
	// log.Debug(sqlString)
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[0]
		userId := int((ret[0]).(int64))
		number := int((ret[1]).(int64))

		// 存在，追加数据
		if val, ok := list[userId]; ok {
			val.Numbers = append(val.Numbers, number)
			continue
		}

		// 初始化
		list[userId] = &userPurchase{
			UserId:  userId,
			Numbers: []int{number},
		}
	}
	return list
}

// 100购活动抽奖
func bet(userId, sn, number int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Purchase_Bet")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SN", database.AdParamInput, database.AdInteger, 4, sn)
	statement.AddParamter("@Number", database.AdParamInput, database.AdInteger, 4, number)
	sqlString := statement.GenSql()
	// log.Debug(sqlString)
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return
	}

	if retCode := int((retRows[0][0]).(int64)); retCode != 1 {
		log.Error("purchase.bet transaction fail userId=%d sn=%d number=%d", userId, sn, number)
	}
	return
}
