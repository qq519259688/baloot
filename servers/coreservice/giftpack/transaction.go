package giftpack

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 系统礼包
func getGiftPackList() map[int]*GiftPack {
	defer func() {
		if err := recover(); err != nil {
			log.Error("giftpack.getGiftPackList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_GiftPack_GetList")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)

	list := make(map[int]*GiftPack)
	for i := 0; i < len(retRows); i++ {
		ret := retRows[i]
		var out GiftPack

		out.Id = int((ret[0]).(int64))
		out.Name = (ret[1]).(string)
		out.Price = int((ret[2]).(int64))
		out.Limit = int((ret[3]).(int64))
		items := (ret[4]).(string)
		if items != "" {
			if err := json.Unmarshal([]byte(items), &out.Items); err != nil {
				log.Error("giftPack.getGiftPackList transaction unmarshal fail %v", err)
				return nil
			}
		}
		out.Type = int((ret[5]).(int64))
		out.Duration = int((ret[6]).(int64))
		out.Group = int((ret[7]).(int64))
		out.GroupIndex = int((ret[8]).(int64))
		out.ProductId = (ret[9]).(string)

		list[out.Id] = &out
	}
	return list
}

// 获取用户礼包历史
func getUserGiftPackHistory(userId int) []*UserGiftPackHistory {
	defer func() {
		if err := recover(); err != nil {
			log.Error("giftPack.getUserGiftPackHistory transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserGiftPack_GetHistory")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)

	var list []*UserGiftPackHistory
	for i := 0; i < len(retRows); i++ {
		ret := retRows[i]
		var out UserGiftPackHistory

		out.GiftPackId = int((ret[0]).(int64))
		out.LastPhaseTime = int((ret[1]).(int64))

		list = append(list, &out)
	}
	return list
}

// 更新用户礼包历史
func updateUserGiftPackHistory(userId, giftPackId int, lastPhaseTime string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("giftpack.updateUserGiftPackHistory transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserGiftPack_UpdateHistory")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GiftPackId", database.AdParamInput, database.AdInteger, 4, giftPackId)
	statement.AddParamter("@LastPhaseTime", database.AdParamInput, database.AdVarChar, 20, lastPhaseTime)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}

	return int((retRows[0][0]).(int64))
}

// 获取成长礼包
func getGrowthPackList() []*GrowthPack {
	defer func() {
		if err := recover(); err != nil {
			log.Error("growthPack.getGrowthPackList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_GrowthPack_GetList")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)

	var list []*GrowthPack

	for i := 0; i < len(retRows); i++ {
		ret := retRows[i]
		var out GrowthPack

		out.Id = int((ret[0]).(int64))
		out.Name = (ret[1]).(string)
		out.Price = int((ret[2]).(int64))
		terms := (ret[3]).(string)
		if terms != "" {
			if err := json.Unmarshal([]byte(terms), &out.Terms); err != nil {
				log.Error("growthPack.getGrowthPackList Id[%d] transaction unmarshal data fail %v", out.Id, err)
				return nil
			}
		}
		out.ProductId = (ret[4]).(string)

		list = append(list, &out)
	}

	return list
}

// 获取用户成长礼包
func getUserGrowthPackList(userId int) []*UserGrowthTerm {
	defer func() {
		if err := recover(); err != nil {
			log.Error("growthPack.getUserGrowthPackList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserGrowthPack_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)

	var list []*UserGrowthTerm
	for i := 0; i < len(retRows); i++ {
		ret := retRows[i]
		var out UserGrowthTerm

		out.UserId = userId
		out.GrowthPackId = int((ret[0]).(int64))
		out.TermIndex = int((ret[1]).(int64))
		out.Bullet = int((ret[2]).(int64))
		out.Status = int((ret[3]).(int64))

		list = append(list, &out)
	}
	return list
}

// 更新用户成长礼包
func updateUserGrowthPack(info *UserGrowthTerm) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("growthPack.updateUserGrowthPack transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserGrowthPack_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, info.UserId)
	statement.AddParamter("@GrowthPackID", database.AdParamInput, database.AdInteger, 4, info.GrowthPackId)
	statement.AddParamter("@TermIndex", database.AdParamInput, database.AdInteger, 4, info.TermIndex)
	statement.AddParamter("@Bullet", database.AdParamInput, database.AdInteger, 4, info.Bullet)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, info.Status)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return retCode
	}

	retCode = int((retRows[0][0]).(int64))
	return retCode
}
