package giftpack

import (
	"encoding/json"
	"fmt"
	"strconv"
	"sync"

	"bet24.com/log"
	"bet24.com/servers/common"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
)

type growthpackmgr struct {
	packs     []*GrowthPack
	userTerms map[int][]*UserGrowthTerm
	lock      *sync.RWMutex
}

func newGrowthPackManager() *growthpackmgr {
	gpm := new(growthpackmgr)
	gpm.userTerms = make(map[int][]*UserGrowthTerm)
	gpm.lock = &sync.RWMutex{}
	gpm.loadPacks()
	return gpm
}

func (gpm *growthpackmgr) onUserEnter(userId int) {
	list := gpm.loadUserTerms(userId)

	gpm.lock.Lock()
	defer gpm.lock.Unlock()
	gpm.userTerms[userId] = list
}

func (gpm *growthpackmgr) onUserExit(userId int) {
	gpm.lock.Lock()
	defer gpm.lock.Unlock()

	delete(gpm.userTerms, userId)
}

func (gpm *growthpackmgr) getGrowthPacks() []*GrowthPack {
	gpm.lock.RLock()
	defer gpm.lock.RUnlock()

	return gpm.packs
}

func (gpm *growthpackmgr) getGrowthPack(packId int) *GrowthPack {
	gpm.lock.RLock()
	defer gpm.lock.RUnlock()

	for _, v := range gpm.packs {
		if v.Id == packId {
			return v
		}
	}
	return nil
}

func (gpm *growthpackmgr) getGrowthPackByProduct(productId string) *GrowthPack {
	gpm.lock.RLock()
	defer gpm.lock.RUnlock()

	for _, v := range gpm.packs {
		if v.ProductId == productId {
			return v
		}
	}
	return nil
}

func (gpm *growthpackmgr) getUserGrowthTerms(userId int) []*UserGrowthTerm {
	gpm.lock.RLock()
	ret, ok := gpm.userTerms[userId]
	gpm.lock.RUnlock()

	if !ok {
		ret = gpm.loadUserTerms(userId)

		gpm.lock.Lock()
		gpm.userTerms[userId] = ret
		gpm.lock.Unlock()
	}
	return ret
}

func (gpm *growthpackmgr) buyGrowthPack(userId int, productId string) (int, string, []item.ItemPack) {
	var items []item.ItemPack
	pack := gpm.getGrowthPackByProduct(productId)
	if pack == nil {
		log.Debug("growthpackmgr.buyGrowthPack userId[%d] productId[%s] invalid", userId, productId)
		return 0, "无效的礼包ID", items
	}

	userTerms := gpm.getUserGrowthTerms(userId)
	for _, v := range userTerms {
		if v.GrowthPackId == pack.Id {
			log.Debug("growthpackmgr.buyGrowthPack userId[%d] packId[%d] perchased", userId, pack.Id)
			return 0, "该礼包已经购买过了", items
		}
	}

	for _, v := range pack.Terms {
		userTerms = append(userTerms, &UserGrowthTerm{
			UserId:       userId,
			GrowthPackId: pack.Id,
			TermIndex:    v.TermIndex,
			Status:       0,
			Bullet:       v.Bullet,
		})

		//items = append(items, v.Items...)
	}

	gpm.lock.Lock()
	gpm.userTerms[userId] = userTerms
	gpm.lock.Unlock()

	gpm.updateUserTerms(userTerms)
	return 1, "购买成功", items
}

func (gpm *growthpackmgr) getGrowthTerm(growthPackId, index int) (GrowthTerm, bool) {
	gpm.lock.RLock()
	defer gpm.lock.RUnlock()
	for _, pack := range gpm.packs {
		if growthPackId != pack.Id {
			continue
		}
		for _, term := range pack.Terms {
			if term.TermIndex == index {
				return term, true
			}
		}
	}
	return GrowthTerm{}, false
}

func (gpm *growthpackmgr) userAwardTerm(userId int, growthPackId, index int) (bool, string) {
	bought := false
	term, ok := gpm.getGrowthTerm(growthPackId, index)
	if !ok {
		log.Debug("growthpackmgr.userAwardTerm invalid params %d.%d", growthPackId, index)
		return false, "未找到礼包"
	}
	userTerms := gpm.getUserGrowthTerms(userId)
	for _, v := range userTerms {
		if v.GrowthPackId != growthPackId {
			continue
		}
		bought = true
		if v.TermIndex != index {
			continue
		}
		if v.Status != GrowthTermStatus_complete {
			return false, "不符合领取条件"
		} else {
			inventory.AddItems(userId, term.Items, fmt.Sprintf("成长礼包[%d]", growthPackId), common.LOGTYPE_PACK_GROWTH)
			v.Status = GrowthTermStatus_awarded
			gpm.updateUserTerm(v)
			return true, "领取成功"
		}
	}
	if bought {
		return false, "领取失败"
	} else {
		return false, "未购买礼包"
	}
}

func (gpm *growthpackmgr) loadPacks() {
	// 操作数据库
	list := getGrowthPackList()

	gpm.lock.Lock()
	defer gpm.lock.Unlock()
	gpm.packs = list
}

func (gpm *growthpackmgr) loadUserTerms(userId int) []*UserGrowthTerm {
	var ret []*UserGrowthTerm
	// 操作数据库
	ret = getUserGrowthPackList(userId)
	return ret
}

func (gpm *growthpackmgr) updateUserTerms(userTerms []*UserGrowthTerm) {
	// 操作数据库
	for _, v := range userTerms {
		gpm.updateUserTerm(v)
	}
}

func (gpm *growthpackmgr) updateUserTerm(userTerm *UserGrowthTerm) {
	go updateUserGrowthPack(userTerm)
}

func (gpm *growthpackmgr) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("growthpackmgr.dumpSys %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	d, _ := json.Marshal(gpm.packs)
	log.Release(string(d))
}

func (gpm *growthpackmgr) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("growthpackmgr.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}
	si := gpm.getUserGrowthTerms(userId)
	if si == nil {
		log.Release("user %d not exist", userId)
		return
	}
	d, _ := json.Marshal(si)
	log.Release(string(d))
}
