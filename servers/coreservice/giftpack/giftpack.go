package giftpack

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	GiftPackLimit_None     = iota // 无限制
	GiftPackLimit_Once            // 只能购买一次
	GiftPackLimit_EveryDay        // 每天购买一次
)

const (
	GiftPackType_First       = iota // 首充礼包
	GiftPackType_TimeLimited        // 限时礼包
	GiftPackType_EveryDay           // 每日特惠
)

type GiftPack struct {
	Id        int    //礼包ID
	Name      string //礼包名称
	ProductId string // 商城产品ID
	Price     int
	Limit     int
	Items     []item.ItemPack
	//PreGiftPack int `json:",omitempty"` // 前置礼包，必须购买前置礼包才能购买本礼包，这里默认要求连续购买
	Type       int // 礼包类型
	Duration   int `json:",omitempty"`
	Group      int `json:",omitempty"` // 每日特惠中，不同价格用不同Group区分
	GroupIndex int `json:",omitempty"` // 同组顺序
}

type UserGiftPackHistory struct {
	GiftPackId    int
	LastPhaseTime int
	group         int
	groupIndex    int
}

type UserBuyable struct {
	GiftPackId int  // 礼包ID
	Buyable    bool // 是否可购买
	TimeLeft   int  // 剩余时间
}

var mgr *giftpackmgr

func Run() {
	mgr = newGiftPackManager()
	gmgr = newGrowthPackManager()
}

func GetGiftPacks() map[int]*GiftPack {
	return mgr.getGiftPackList()
}

func GetBuyable(userId int) []UserBuyable {
	return mgr.getUserGiftPack(userId)
}

func BuyGiftPack(userId int, productId string) []item.ItemPack {
	return mgr.buyGiftPack(userId, productId)
}

func Dump(param1, param2 string) {
	switch param1 {
	case "sys":
		mgr.dumpSys(param2)
	case "user":
		mgr.dumpUser(param2)
	case "gsys":
		gmgr.dumpSys(param2)
	case "guser":
		gmgr.dumpUser(param2)
	default:
		log.Debug("giftpack.Dump unhandled %s:%s", param1, param2)
	}
}
