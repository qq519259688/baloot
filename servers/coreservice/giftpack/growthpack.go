package giftpack

import (
	_ "bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	GrowthTermStatus_none     = iota // 没有
	GrowthTermStatus_complete        // 可领取
	GrowthTermStatus_awarded         // 已领取
)

type GrowthPack struct {
	Id        int          // 礼包ID
	ProductId string       // 商城产品ID
	Name      string       // 名称
	Price     int          // 价格
	Terms     []GrowthTerm // 礼包列表
}

type GrowthTerm struct {
	TermIndex int             // index
	Name      string          // 名称
	Bullet    int             // 解锁子弹
	Items     []item.ItemPack // 礼包内容
}

type UserGrowthTerm struct {
	UserId       int // 用户ID，客户端不发
	GrowthPackId int // 礼包ID
	TermIndex    int // index
	Bullet       int // 目标炮台等级
	Status       int // 目前状态
}

var gmgr *growthpackmgr

func AddUser(userId int) {
	gmgr.onUserEnter(userId)
}

func RemoveUser(userId int) {
	gmgr.onUserExit(userId)
}

func BuyGrowthPack(userId int, productId string) (int, string, []item.ItemPack) {
	return gmgr.buyGrowthPack(userId, productId)
}

func GetGrowthPacks() []*GrowthPack {
	return gmgr.getGrowthPacks()
}

func GetUserGrowthTerms(userId int) []*UserGrowthTerm {
	return gmgr.getUserGrowthTerms(userId)
}

func UserAwardTerm(userId int, growthPackId, index int) (bool, string) {
	return gmgr.userAwardTerm(userId, growthPackId, index)
}
