package prizewheel

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/dbengine"
)

// 获取转盘奖励列表
func getList() []*Prize {
	defer func() {
		if err := recover(); err != nil {
			log.Error("prizewheel.getList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_PrizeWheel_GetList")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)

	var prizes []*Prize
	for i := 0; i < len(retRows); i++ {
		ret := retRows[i]
		var out Prize

		out.Id = int((ret[0]).(int64))
		out.Chance = int((ret[1]).(int64))
		items := (ret[2]).(string)
		if items != "" {
			if err := json.Unmarshal([]byte(items), &out.Items); err != nil {
				log.Error("prizewheel.getList transaction unmarshal err %v", err)
				return nil
			}
		}

		prizes = append(prizes, &out)
	}
	return prizes
}

// 获取用户转盘信息
func getInfo(userId int) (int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_PrizeWheel_GetUserInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0, 0
	}

	ret := retRows[0]
	times := int((ret[0]).(int64))
	crdateStr := (ret[1]).(string)
	crdate := common.ParseTime(crdateStr)
	return times, common.GetStamp(crdate)
}

// 修改用户转盘信息
func updateInfo(userId, times int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_PrizeWheel_UpdateUserInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Times", database.AdParamInput, database.AdInteger, 4, times)
	sqlstring := statement.GenSql()
	dbengine.CenterDB.ExecSql(sqlstring)
	return
}

// 添加转盘奖励记录
func addRecord(userId, wheelId int, items string) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_PrizeWheel_AddRecord")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@WheelID", database.AdParamInput, database.AdInteger, 4, wheelId)
	statement.AddParamter("@Items", database.AdParamInput, database.AdVarChar, 256, items)
	sqlstring := statement.GenSql()
	dbengine.CenterDB.ExecSql(sqlstring)
	return
}

// 转盘奖励历史记录
func getRecord() []*userPrizeRecord {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*userPrizeRecord
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_PrizeWheel_GetRecord")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out userPrizeRecord

		out.UserId = int((ret[0]).(int64))
		out.NickName = (ret[1]).(string)
		out.WheelId = int((ret[2]).(int64))
		items := (ret[3]).(string)
		if err := json.Unmarshal([]byte(items), &out.Items); err != nil {
			log.Error("prizewheel.getRecord unmarshal err %v", err)
		}
		crdateStr := (ret[4]).(string)
		crdate := common.ParseTime(crdateStr)
		out.Crdate = common.GetStamp(crdate)

		list = append(list, &out)
	}

	return list
}
