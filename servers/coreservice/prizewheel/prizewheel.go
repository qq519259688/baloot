package prizewheel

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

type Prize struct {
	Id     int // 客户端排序用
	Chance int `json:"-"` // 概率 万分之
	Items  []item.ItemPack
}

var mgr *prizewheelmgr

func Run() {
	mgr = newPrizeWheelManager()
}

func Wheel(userId, count int, nickName, ipAddress string) ([]*Prize, string) {
	return mgr.wheel(userId, count, nickName, ipAddress)
}

func Dump(param1, param2 string) {
	mgr.dump(param1, param2)
}

func GetPrizes() []*Prize {
	return mgr.getPrizes()
}

func GetWheelTimes(userId int) (int, int, int) {
	return mgr.getWheelTimes(userId)
}

func GetRecord() []*userPrizeRecord {
	return mgr.getRecord()
}

func AddUser(userId int) {
	mgr.onUserEnter(userId)
}

func RemoveUser(userId int) {
	mgr.onUserExit(userId)
}
