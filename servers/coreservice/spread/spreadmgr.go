package spread

import (
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/friend"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	user "bet24.com/servers/micros/userservices/proto"
)

const award string = `[{"ItemId":1,"Count":100000}]`

type spreadmgr struct {
	items []item.ItemPack
}

func newSpreadMgr() *spreadmgr {
	obj := new(spreadmgr)
	obj.loadAward()
	log.Debug("spread manager running")
	return obj
}

func (s *spreadmgr) loadAward() {
	if err := json.Unmarshal([]byte(award), &s.items); err != nil {
		log.Error("spread loadAwards unmarshal fail %v", err)
	}
	return
}

func (s *spreadmgr) apply(userId, code int) (int, []item.ItemPack) {
	retCode := apply(userId, code)
	if retCode != 1 {
		return retCode, nil
	}

	inventory.AddItems(userId, s.items, "申请会员奖励", common.LOGTYPE_SPREAD)
	return retCode, s.items
}

func (s *spreadmgr) getMembers(code, pageIndex, pageSize int) (int, []*spreadMember) {
	recordCount, list := getMembers(code, pageIndex, pageSize)
	for _, v := range list {
		u := user.GetUserInfo(v.UserID)
		if u == nil {
			log.Error("spread.getMembers userId=%d ==> %+v", v.UserID, v)
			continue
		}

		v.NickName = u.NickName
		v.Sex = u.Sex
		v.FaceId = u.FaceId
		v.FaceUrl = u.FaceUrl
		v.VipLevel = u.Vip
		v.IsFriend = friend.IfFriend(code, v.UserID)
	}
	return recordCount, list
}

func (s *spreadmgr) gift(code, fromUserId int) (int, []item.ItemPack) {
	retCode := gift(code, fromUserId)
	if retCode != 1 {
		return retCode, nil
	}

	inventory.AddItems(code, s.items, "邀请会员奖励", common.LOGTYPE_SPREAD)
	return retCode, s.items
}

func (s *spreadmgr) checkTip(code int) bool {
	_, list := getMembers(code, 1, 100000)
	for _, v := range list {
		if v.IsGift == 0 {
			return true
		}
	}

	return false
}
