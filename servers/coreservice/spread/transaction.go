package spread

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/dbengine"
)

// 会员申请
func apply(userId, code int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Spread_Apply")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Code", database.AdParamInput, database.AdInteger, 4, code)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return retCode
	}
	retCode = int((retRows[0][0]).(int64))
	return retCode
}

// 推广会员
func getMembers(code int, pageIndex, pageSize int) (int, []*spreadMember) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var (
		recordCount int
		list        []*spreadMember
	)

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Spread_GetMembers")
	statement.AddParamter("@Code", database.AdParamInput, database.AdInteger, 4, code)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return recordCount, list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info spreadMember

			info.UserID = int((ret[0]).(int64))
			info.IsGift = int((ret[1]).(int64))
			crdateStr := (ret[2]).(string)
			crdate := common.ParseTime(crdateStr)
			info.Crdate = common.GetStamp(crdate)

			list = append(list, &info)
		}
	}

	recordCount = int((retRows[rowLen-1][0]).(int64))
	return recordCount, list
}

// 领取奖励
func gift(code, fromUserId int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Spread_Gift")
	statement.AddParamter("@Code", database.AdParamInput, database.AdInteger, 4, code)
	statement.AddParamter("@FromUserID", database.AdParamInput, database.AdInteger, 4, fromUserId)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return retCode
	}
	retCode = int((retRows[0][0]).(int64))
	return retCode
}
