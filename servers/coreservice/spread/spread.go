package spread

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

var mgr *spreadmgr

type spreadMember struct {
	UserID   int    //用户ID
	NickName string //昵称
	Sex      int    //性别 0=无  1=男  2=女
	FaceId   int    //头像ID
	FaceUrl  string //头像路径
	IsGift   int    //是否领取奖励
	Crdate   int    //绑码时间戳
	IsFriend int    //是否好友
	VipLevel int    //Vip
}

func Run() {
	log.Debug("spread running")
	mgr = newSpreadMgr()
}

func Apply(userId, code int) (int, []item.ItemPack) {
	return mgr.apply(userId, code)
}

func GetMembers(code, pageIndex, pageSize int) (int, []*spreadMember) {
	return mgr.getMembers(code, pageIndex, pageSize)
}

func Gift(code, fromUserId int) (int, []item.ItemPack) {
	return mgr.gift(code, fromUserId)
}

func CheckTip(code int) bool {
	return mgr.checkTip(code)
}
