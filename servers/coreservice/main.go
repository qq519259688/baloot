package main

import (
	"bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/coreservice/battlepass"
	"bet24.com/servers/coreservice/chat"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/dbengine"
	"bet24.com/servers/coreservice/friend"
	"bet24.com/servers/coreservice/gamelist"
	"bet24.com/servers/coreservice/giftpack"
	"bet24.com/servers/coreservice/gold2chipwheel"
	"bet24.com/servers/coreservice/jackpot"
	"bet24.com/servers/coreservice/keyword"
	"bet24.com/servers/coreservice/limiteditems"
	"bet24.com/servers/coreservice/prizewheel"
	"bet24.com/servers/coreservice/redpoint"
	"bet24.com/servers/coreservice/service"
	config "bet24.com/servers/coreservice/serviceconfig"
	"bet24.com/servers/coreservice/shop"
	"bet24.com/servers/coreservice/signin"
	"bet24.com/servers/coreservice/signinwheel"
	"bet24.com/servers/coreservice/slotscore"
	"bet24.com/servers/coreservice/userwin"
	"bet24.com/servers/coreservice/video"
	micro_common "bet24.com/servers/micros/common"
	//"bet24.com/servers/monitor"
	"bet24.com/servers/transaction"
	"bet24.com/utils"
	"fmt"
	_ "github.com/smallnest/rpcx/server"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	config.Run()
	utils.SetErrorFile("log/coreservice/err.log", "coreservice starting")

	redis.InitPool(config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB)
	dbengine.Run()
	transaction.Run()
	log.Debug("coreservice served in %s", service.Addr)
	//monitor.Run(config.Server.MonitorPort,config.Server.LogPath)
	//go common.ServeLogFile(config.Server.MonitorPort+100, config.Server.LogPath)
	service.Run()
	//go waitInput()
	//s := server.NewServer()
	//s.Register(new(service.Server), "")
	//s.Serve("tcp", service.Addr)
	micro_common.RunServiceWithPort(client.ServiceName, new(service.Server),
		fmt.Sprintf("8.141.80.205:%d", config.Server.ConsulPort), config.Server.ServerPort, config.Server.LogPath)

	log.Debug("coreservice leaving main")
}

func waitInput() {
	for {
		var cmd string
		var param1 string
		var param2 string
		fmt.Scanf("%s %s %s", &cmd, &param1, &param2)
		switch cmd {
		case "":
		case "wheel":
			prizewheel.Dump(param1, param2)
		case "giftpack":
			giftpack.Dump(param1, param2)
		case "signinwheel":
			signinwheel.Dump(param1, param2)
		case "shop":
			shop.Dump(param1, param2)
		case "video":
			video.Dump(param1, param2)
		case "jackpot":
			jackpot.Dump(param1, param2)
		case "chat":
			chat.Dump(param1, param2)
		case "slotscore":
			slotscore.Dump(param1, param2)
		case "chipwheel":
			gold2chipwheel.Dump(param1, param2)
		case "flush":
			slotscore.Flush()
			gold2chipwheel.Flush()
			limiteditems.Flush()
			//simplematch.Flush()
			//sngmatch.Flush()
		case "gamelist":
			gamelist.Dump()
		case "limiteditems":
			limiteditems.Dump(param1, param2)
		case "battlepass":
			battlepass.Dump(param1, param2)
		case "keyword":
			keyword.Dump()
		case "userwin":
			userwin.Dump(param1)
		case "redpoint":
			redpoint.Dump(param1)
		/*case "simplematch":
			simplematch.Dump(param1, param2)
		case "pointmatch":
			pointmatch.Dump(param1, param2)
		case "sngmatch":
			sngmatch.Dump(param1, param2)
		case "combomatch":
			combomatch.Dump(param1, param2)
		*/
		case "friend":
			friend.Dump(param1, param2)
		case "signin":
			signin.Dump(param1, param2)
		case "clientpool":
			micro_common.DumpClientPools()
		case "exit":
			return
		//case "panic":
		//	panic("hello panic")
		default:
			log.Release("unknown command1")
		}
	}
}
