package jackpot

import (
	"bet24.com/log"
	"sync"
)

var jp *jackpot

func Run() {
	jp = new(jackpot)
	jp.lock = &sync.RWMutex{}
	log.Debug("jackpot running")
	jp.loadData()
}

func GetJackpotAmount(gameId int, isChipRoom bool) int {
	return jp.getAmount(gameId, isChipRoom)
}

func ModifyJackpot(gameId int, amount int, userId int, desc string, isChipRoom bool) int {
	return jp.modifyAmount(gameId, amount, userId, desc, isChipRoom)
}

func Dump(param1, param2 string) {
	if param1 == "" {
		jp.dumpSys()
		return
	}
	jp.dumpGame(param1)
}
