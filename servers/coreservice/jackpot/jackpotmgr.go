package jackpot

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/serviceconfig"
	"strconv"
	"sync"
)

const (
	gold_min_default = 1000000000
	gold_max_default = 6000000000
	chip_min_default = 1000000000
	chip_max_default = 6000000000
)

type jackpot struct {
	lock        *sync.RWMutex
	amountsGold map[int]int
	amountsChip map[int]int

	minGold int
	maxGold int
	minChip int
	maxChip int
}

func (j *jackpot) loadData() {
	j.amountsGold = make(map[int]int)
	j.amountsChip = make(map[int]int)
	j.minGold, j.maxGold = serviceconfig.SpecialCfg.MinGoldPool, serviceconfig.SpecialCfg.MaxGoldPool
	j.minChip, j.maxChip = serviceconfig.SpecialCfg.MinChipPool, serviceconfig.SpecialCfg.MaxChipPool

	if j.minChip == 0 {
		j.minChip = chip_min_default
	}
	if j.maxChip == 0 {
		j.maxChip = chip_max_default
	}
	if j.minGold == 0 {
		j.minGold = gold_min_default
	}
	if j.maxGold == 0 {
		j.maxGold = gold_max_default
	}
}

func (j *jackpot) getAmount(gameId int, isChipRoom bool) int {
	j.lock.Lock()
	defer j.lock.Unlock()
	var amount int
	if isChipRoom {
		ret, ok := j.amountsChip[gameId]
		if !ok {
			ret = getJackpotAmount(gameId, isChipRoom)
			j.amountsChip[gameId] = ret
		}
		amount = ret
	} else {
		ret, ok := j.amountsGold[gameId]
		if !ok {
			ret = getJackpotAmount(gameId, isChipRoom)
			j.amountsGold[gameId] = ret
		}
		amount = ret
	}

	modify := 0
	amount, modify = j.adjustAmount(amount, isChipRoom)
	if modify != 0 {
		go modifyAction(0, gameId, modify, isChipRoom)
		if isChipRoom {
			j.amountsChip[gameId] = amount
		} else {
			j.amountsGold[gameId] = amount
		}
	}
	return amount
}

// 用户从奖池中获取到的金币什么时候加?
func (j *jackpot) modifyAmount(gameId int, amount int, userId int, desc string, isChipRoom bool) int {
	preAmount := j.getAmount(gameId, isChipRoom)
	preAmount += amount
	go modifyAction(userId, gameId, amount, isChipRoom)
	modify := 0
	preAmount, modify = j.adjustAmount(preAmount, isChipRoom)
	if modify != 0 {
		go modifyAction(0, gameId, modify, isChipRoom)
	}

	j.lock.Lock()
	if isChipRoom {
		j.amountsChip[gameId] = preAmount
	} else {
		j.amountsGold[gameId] = preAmount
	}
	j.lock.Unlock()

	return preAmount
}

func (j *jackpot) adjustAmount(amount int, isChipRoom bool) (int, int) {
	min, max := j.getMinMax(isChipRoom)
	if amount < min {
		return min, min - amount
	}
	if max > 0 && amount > max {
		return max, max - amount
	}
	return amount, 0
}

func (j *jackpot) getMinMax(isChipRoom bool) (int, int) {
	if isChipRoom {
		return j.minChip, j.maxChip
	} else {
		return j.minGold, j.maxGold
	}
}

func (j *jackpot) dumpSys() {
	log.Release("-------------------------------")
	log.Release("jackpot.dumpSys")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	log.Release("    gold[%d,%d],chip[%d,%d]", j.minGold, j.maxGold, j.minChip, j.maxChip)
}

func (j *jackpot) dumpGame(param1 string) {
	log.Release("-------------------------------")
	log.Release("jackpot.dumpGame %s", param1)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	var gameId int
	var err error
	if gameId, err = strconv.Atoi(param1); err != nil {
		log.Release("    atoi error %v", err)
		return
	}

	log.Release("    gold[%d],chip[%d]", j.getAmount(gameId, false), j.getAmount(gameId, true))
}
