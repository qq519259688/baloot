package jackpot

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 获取奖池信息
func getJackpotAmount(gameId int, isChipRoom bool) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	amount := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	if !isChipRoom {
		statement.SetProcName("Game_JackPot_GetInfo")
	} else {
		statement.SetProcName("Game_JackPotChip_GetInfo")
	}
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	statement.AddParamter("@Amount", database.AdParamOutput, database.AdBigint, 8, amount)

	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		log.Release("jackpot.getInfo gameId=%d amount=0", gameId)
		return amount
	}

	return int((retRows[0][0]).(int64))
}

// 修改奖池
func modifyAction(userId, gameId, amount int, isChipRoom bool) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName(getModifyProc(amount, isChipRoom))
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@Balance", database.AdParamOutput, database.AdBigint, 8, amount)

	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)

	rowLen := len(retRows)
	if rowLen <= 0 {
		return 0
	}
	out := int((retRows[0][0]).(int64))
	return out
}

func getModifyProc(amount int, isChipRoom bool) string {
	if isChipRoom {
		if amount >= 0 {
			return "Game_JackPotChip_Incr"
		}
		return "Game_JackPotChip_Sub"
	}

	if amount >= 0 {
		return "Game_JackPot_Incr"
	}
	return "Game_JackPot_Sub"
}
