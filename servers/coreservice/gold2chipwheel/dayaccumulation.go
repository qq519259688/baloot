package gold2chipwheel

import (
	"bet24.com/log"
	"bet24.com/redis"
	"encoding/json"
	"sync"
	"time"
)

type dayaccumulation struct {
	UpdateDay int
	// 每日累加
	Accumulation map[int]int

	lock *sync.RWMutex
}

func newDayAccumulation() *dayaccumulation {
	ret := new(dayaccumulation)
	ret.lock = &sync.RWMutex{}
	ret.Accumulation = make(map[int]int)
	ret.loadData()
	return ret
}

func (da *dayaccumulation) loadData() {
	data, ok := redis.String_Get(da.getRedisKey())
	if data == "" || !ok {
		return
	}
	da.lock.Lock()
	err := json.Unmarshal([]byte(data), &da)
	da.lock.Unlock()
	if err != nil {
		log.Release("dayaccumulation.loadData Unmarshal failed err:%v,%s", err, data)
		return
	}
	// 如果时间太长了
	if da.UpdateDay != time.Now().Day() {
		da.Accumulation = make(map[int]int)
		da.UpdateDay = time.Now().Day()
	}
	da.resetAccumulation()
}

func (da *dayaccumulation) flush() {
	da.lock.RLock()
	d, _ := json.Marshal(da)
	da.lock.RUnlock()
	go redis.String_Set(da.getRedisKey(), string(d))
}

func (da *dayaccumulation) getRedisKey() string {
	return "gold2chipwheel:accumulation"
}

func (da *dayaccumulation) addAccumulate(userId int, amount int) {
	da.lock.Lock()
	da.Accumulation[userId] += amount
	da.lock.Unlock()
}

func (da *dayaccumulation) resetAccumulation() {
	time.AfterFunc(time.Hour, da.resetAccumulation)
	defer da.flush()
	now := time.Now()
	if now.Day() == da.UpdateDay {
		return
	}
	log.Debug("dayaccumulation resetAccumulation")
	da.UpdateDay = now.Day()

	da.lock.Lock()
	da.Accumulation = make(map[int]int)
	da.lock.Unlock()
}

func (da *dayaccumulation) getAccumulate(userId int) int {
	da.lock.RLock()
	defer da.lock.RUnlock()
	return da.Accumulation[userId]
}
