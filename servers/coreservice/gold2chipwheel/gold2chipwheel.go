package gold2chipwheel

var mgr *gold2chipwheelmgr

func Run() {
	mgr = newPrizeWheelManager()
}

func Wheel(userId, bet int, ipAddress string) int {
	return mgr.wheel(userId, bet, ipAddress)
}

func Dump(param1, param2 string) {
	mgr.dump(param1, param2)
}

func GetConfig() string {
	return mgr.getPrizes()
}

func GetRecord(userId int) string {
	return mgr.getRecord(userId)
}

func Flush() {
	mgr.flush()
}
