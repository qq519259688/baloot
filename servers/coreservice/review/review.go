package review

import "bet24.com/log"

var appSet = []string{"google"}

var mgr *reviewmgr

func Run() {
	log.Debug("review running")
	mgr = newReviewMgr()
}

//获取评论信息
func Info(userId int, appName string) int {
	return mgr.info(userId, appName)
}

//领取评论
func Gift(userId int, appName, ipAddress string) (bool, int) {
	return mgr.gift(userId, appName, ipAddress)
}
