package review

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/serviceconfig"
	cash "bet24.com/servers/micros/money/proto"
)

type reviewmgr struct {
}

func newReviewMgr() *reviewmgr {
	log.Debug("review manager running")
	return &reviewmgr{}
}

// 获取评论信息
func (this *reviewmgr) info(userId int, appName string) int {
	if !this.appExist(appName) {
		log.Error("reviewmgr.info userId=%d appName=%s is not exist", userId, appName)
		return 0
	}
	return info(userId, appName)
}

// 领取评论
func (this *reviewmgr) gift(userId int, appName, ipAddress string) (bool, int) {
	if !this.appExist(appName) {
		log.Error("reviewmgr.gift userId=%d appName=%s ipAddress=%s is not exist", userId, appName, ipAddress)
		return false, 0
	}

	succ := gift(userId, appName)
	if !succ {
		return false, 0
	}

	//加金币
	ret := cash.GiveMoney(userId, serviceconfig.Server.ReviewAmount, common.LOGTYPE_SEND_REVIEW, "评论模块", "评论赠送", ipAddress)
	if ret != 1 {
		return false, 0
	}

	return true, serviceconfig.Server.ReviewAmount
}

// 判断app应用是否存在
func (this *reviewmgr) appExist(appName string) bool {
	for _, v := range appSet {
		if v == appName {
			return true
		}
	}

	return false
}
