package review

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 获取评论信息
func info(userId int, appName string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	ret := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Review_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@AppName", database.AdParamInput, database.AdVarChar, 32, appName)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return ret
	}

	ret = int((retRows[0][0]).(int64))
	return ret
}

// 领取评论
func gift(userId int, appName string) bool {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Review_Gift")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@AppName", database.AdParamInput, database.AdVarChar, 32, appName)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return false
	}

	ret := int((retRows[0][0]).(int64))
	return ret == 1
}
