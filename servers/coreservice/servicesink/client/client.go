package client

import (
	"bet24.com/log"
	"context"
	"encoding/json"
)

type Request struct {
	Msg  string
	Data string
}

type Reply struct {
	Data    string
	RetCode int
}

type Request_Notification struct {
	UserId int
	Data   string
}

func DoRequest(addr, msg, data string) (bool, *Reply) {
	xclient := getClientMgr().getClient(addr)
	if xclient == nil {
		log.Release("msg=%s data=%s call failed: client==nil", msg, data)
		return false, nil
	}
	args := &Request{
		Msg:  msg,
		Data: data,
	}

	reply := &Reply{}
	err := xclient.Call(context.Background(), msg, args, reply)
	if err != nil {
		log.Release("msg=%s data=%s call failed: %v", msg, data, err)
		return false, nil
	}
	return true, reply
}

func OnNotification(addr string, userId int, data string) {
	msg := "OnNotification"
	d, _ := json.Marshal(Request_Notification{
		UserId: userId,
		Data:   data,
	})
	DoRequest(addr, msg, string(d))
}
