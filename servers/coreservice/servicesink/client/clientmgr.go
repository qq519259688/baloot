package client

import (
	"bet24.com/log"
	"github.com/smallnest/rpcx/client"
	_ "github.com/smallnest/rpcx/protocol"
	"sync"
)

type clientmgr struct {
	clients map[string]client.XClient
	lock    *sync.RWMutex
}

var mgr *clientmgr

func getClientMgr() *clientmgr {
	if mgr == nil {
		mgr = new(clientmgr)
		mgr.ctor()
	}
	return mgr
}

func (cm *clientmgr) ctor() {
	cm.clients = make(map[string]client.XClient)
	cm.lock = &sync.RWMutex{}
}

func (cm *clientmgr) getClient(addr string) client.XClient {
	cm.lock.RLock()
	ret, ok := cm.clients[addr]
	cm.lock.RUnlock()
	if !ok || ret == nil {
		ret = openConnection(addr)
	}
	cm.lock.Lock()
	cm.clients[addr] = ret
	cm.lock.Unlock()
	return ret
}

func openConnection(addr string) client.XClient {
	log.Debug("openConnection %s", addr)
	d, _ := client.NewPeer2PeerDiscovery(addr, "")
	_xclient := client.NewXClient("Server", client.Failtry, client.RandomSelect, d, client.DefaultOption)
	return _xclient
}
