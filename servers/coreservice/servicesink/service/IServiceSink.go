package service

type IServiceSink interface {
	OnNotification(userId int, data string)
}
