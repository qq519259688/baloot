package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/servicesink/client"
	"bet24.com/utils"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net"
)

var Addr string
var _sink IServiceSink

func Run(port int, sink IServiceSink) bool {
	if utils.CheckPortInUse(port) {
		log.Release("servicesink.Run port[%d] in use", port)
		return false
	}
	if sink == nil {
		log.Release("servicesink.Run sink == nil")
		return false
	}
	Addr = fmt.Sprintf("%s:%d", getMyIP(), port)
	_sink = sink
	return true
}

func getMyIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		fmt.Println(err)
		return ""
	}
	for _, address := range addrs {
		// 检查ip地址判断是否回环地址
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""

}

type Server int

func (s *Server) OnNotification(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_Notification
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.OnNotification unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	_sink.OnNotification(req.UserId, req.Data)
	return nil
}
