package servicesink

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/servicesink/service"
	"github.com/smallnest/rpcx/server"
)

func Run(port int, sink service.IServiceSink) bool {
	if !service.Run(port, sink) {
		log.Release("servicesink.Run failed")
		return false
	}

	s := server.NewServer()
	s.Register(new(service.Server), "")
	go s.Serve("tcp", service.Addr)

	return true
}

func GetAddr() string {
	return service.Addr
}
