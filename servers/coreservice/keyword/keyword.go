package keyword

import (
	"encoding/json"
	"os"
	"regexp"
	"strings"
	"time"

	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "keywords"
const refresh_config_sec = 300

var config_string string

// 屏蔽词列表
var keywordList struct {
	Word []string

	maps  map[string]string
	regex *regexp.Regexp
}

func Run() {
	loadKeywords()
}

func loadKeywords() {
	time.AfterFunc(refresh_config_sec*time.Second, loadKeywords)
	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("fishconf/Keyword.json")
		//fmt.Println(string(data))
		if err != nil {
			log.Release("read config failed fishconf/Keyword.json %v", err)
			return
		}

		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(config_key, configString)
		}
	}

	if config_string == configString {
		return
	}

	config_string = configString

	err := json.Unmarshal([]byte(configString), &keywordList)
	keywordList.maps = make(map[string]string)
	for _, v := range keywordList.Word {
		v = strings.ReplaceAll(v, " ", "")
		if len(v) == 0 {
			continue
		}
		keywordList.maps[v] = "****"
	}
	keywordList.regex = regexp.MustCompile(strings.Join(
		[]string{"\\b", strings.Join(keys(keywordList.maps), "|"), "\\b"}, ""))

	keywordList.Word = []string{}
	for k := range keywordList.maps {
		keywordList.Word = append(keywordList.Word, k)
	}

	d, _ := json.Marshal(keywordList)
	platformconfig.SetConfig(config_key, string(d))

	if err != nil {
		log.Release("Unmarshal config failed fishconf/Keyword.json err %v", err)
	}
}

func keys(m map[string]string) []string {
	keys := make([]string, len(m))
	i := 0
	for k := range m {
		keys[i] = k
		i++
	}
	return keys
}

func ParseKeyword(in string) string {
	/*for _, s := range keywordList.Word {
		in = strings.Replace(in, s, "****", -1)
	}
	return in
	*/
	// replace all occurrences of any keyword with its corresponding symbol
	str := keywordList.regex.ReplaceAllStringFunc(in, func(match string) string {
		return keywordList.maps[match]
	})
	return str
}

func Dump() {
	log.Release("-------------------------------")
	log.Release("keyword.dump keyword count = %d", len(keywordList.Word))
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	for _, v := range keywordList.Word {
		log.Release("    %s", v)
	}
}
