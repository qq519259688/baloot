package exchange

import (
	"bet24.com/servers/coreservice/client"
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	_                          = iota // 0=无效
	EXCHANGE_TYPE_FUKA                // 1=奖券
	EXCHANGE_TYPE_TEACHER_CHIP        // 2=代理收益兑元宝
	EXCHANGE_TYPE_TEACHER_TEL         // 3=代理收益兑话费
	EXCHANGE_TYPE_TEACHER_CASH        // 4=代理收益兑RP
	EXCHANGE_TYPE_GOLD                // 5=金币提现
	EXCHANGE_TYPE_CASH                // 6=现金卡提现
)

var em *exchangemgr

func Run() {
	em = newExchangeManager()
}

type ExchangeInfo struct {
	Id            int
	ExchangeType  int    // 兑换类型(1=奖券兑换金币  2=代理兑换元宝 3=代理兑换电话卡  4=代理兑换RP  5=金币提现)
	LeftCount     int    // 剩余数量，如果小于0，则表示缺货
	LimitOnce     bool   // 是否限制购买一次
	VipNeed       int    // 所需VIP等级
	VipShow       int    `json:"-"` // 显示所需vip
	GameCountShow int    `json:"-"` // 显示所需游戏局数
	Price         int    // 价格
	Memo          string // 描述
	Items         []item.ItemPack
}

type ExchangeType struct {
	Id           int
	ExchangeType int    // 兑换类型(1=奖券兑换金币  2=代理兑换元宝 3=代理兑换电话卡  4=代理兑换RP  5=金币提现)
	Price        int    // 价格
	Memo         string // 描述
}

func GetExchangeType() []ExchangeType {
	return em.getExchangeType()
}

func GetExchangeList(userId int) []ExchangeInfo {
	return em.getExchangeList(userId)
}

func GetExchangeHistory(userId, pageIndex, pageSize int) (int, []client.ExchangeHistory) {
	return em.getExchangeHistory(userId, pageIndex, pageSize)
}

func UserExchange(userId, exchangeId, num int, remark string) (int, string) {
	return em.userExchange(userId, exchangeId, num, remark)
}
