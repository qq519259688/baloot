package exchange

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/dbengine"
	"encoding/json"
	"runtime/debug"
)

func trans_loadExchangeList() []ExchangeInfo {
	var list []ExchangeInfo
	defer func() {
		if err := recover(); err != nil {
			log.Error("exchange.trans_loadExchangeList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Exchange_GetList")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)

	for i := 0; i < len(retRows); i++ {
		ret := retRows[i]
		var out ExchangeInfo

		out.Id = int((ret[0]).(int64))
		out.ExchangeType = int((ret[1]).(int64))
		out.LeftCount = int((ret[2]).(int64))
		limitOnce := int((ret[3]).(int64))
		if limitOnce > 0 {
			out.LimitOnce = true
		}
		out.VipNeed = int((ret[4]).(int64))
		out.Price = int((ret[5]).(int64))
		items := (ret[6]).(string)
		if items != "" {
			if err := json.Unmarshal([]byte(items), &out.Items); err != nil {
				log.Error("exchange.trans_loadExchangeList transaction unmarshal fail %v", err)
				return nil
			}
		}
		out.Memo = (ret[7]).(string)
		out.VipShow = int((ret[8]).(int64))
		out.GameCountShow = int((ret[9]).(int64))

		list = append(list, out)
	}
	return list
}

func trans_userExchange(userId, exchangeId, exchangeType, price, status int, items, remark string) bool {
	defer func() {
		if err := recover(); err != nil {
			log.Error("exchange.trans_userExchange transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var retCode int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Exchange_UserExchange")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ExchangeID", database.AdParamInput, database.AdInteger, 4, exchangeId)
	statement.AddParamter("@ExchangeType", database.AdParamInput, database.AdInteger, 4, exchangeType)
	statement.AddParamter("@Price", database.AdParamInput, database.AdInteger, 4, price)
	statement.AddParamter("@Items", database.AdParamInput, database.AdVarChar, 128, items)
	statement.AddParamter("@Remark", database.AdParamInput, database.AdVarChar, 1024, remark)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, status)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return false
	}

	retCode = int((retRows[0][0]).(int64))
	return retCode == 1
}

/*
	type ExchangeHistory struct {
		ExchangeId   int //兑换ID
		ExchangeType int // 兑换类型(1=奖券兑换金币  2=代理兑换元宝 3=代理兑换电话卡  4=代理兑换RP)
		Price        int // 价格
		Items        []item.ItemPack
		Remark       string //备注
		Status       int    //状态（0=待发货  1=已发货）
		Crdate       string //时间
	}
*/
func trans_getExchangeHistory(userId, exchangeId, pageIndex, pageSize int) (int, []client.ExchangeHistory) {
	var list []client.ExchangeHistory
	defer func() {
		if err := recover(); err != nil {
			log.Error("exchange.trans_getExchangeHistory transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var recordCount int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Exchange_GetHistory")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ExchangeID", database.AdParamInput, database.AdInteger, 4, exchangeId)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var out client.ExchangeHistory

			out.ExchangeId = int((ret[0]).(int64))
			out.ExchangeType = int((ret[1]).(int64))
			out.Price = int((ret[2]).(int64))
			items := (ret[3]).(string)
			if items != "" {
				if err := json.Unmarshal([]byte(items), &out.Items); err != nil {
					log.Error("exchange.trans_getExchangeHistory transaction unmarshal fail %v", err)
				}
			}
			out.Remark = (ret[4]).(string)
			out.Status = int((ret[5]).(int64))
			out.Crdate = (ret[6]).(string)
			list = append(list, out)
		}
	}

	recordCount = int((retRows[rowLen-1][0]).(int64))
	return recordCount, list
}

func trans_updateExchange(exchangeId, leftCount int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Exchange_Update")
	statement.AddParamter("@ExchangeID", database.AdParamInput, database.AdInteger, 4, exchangeId)
	statement.AddParamter("@LeftCount", database.AdParamInput, database.AdInteger, 4, leftCount)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return retCode
	}

	retCode = int((retRows[0][0]).(int64))
	return retCode
}
