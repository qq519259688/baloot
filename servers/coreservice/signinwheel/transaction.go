package signinwheel

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/dbengine"
	item "bet24.com/servers/micros/item_inventory/proto"
)

// 历史记录
func getHistory() []SigninWheelHistory {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var list []SigninWheelHistory
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SigninWheel_GetHistory")
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info SigninWheelHistory

			info.UserId = int((ret[0]).(int64))
			info.Base = int((ret[1]).(int64))
			info.Multiple = int((ret[2]).(int64))
			info.ItemId = int((ret[3]).(int64))
			itemPack := (ret[4]).(string)
			if itemPack == "" {
				info.Items = append(info.Items, item.ItemPack{ItemId: info.ItemId, Count: info.Base * info.Multiple})
			} else {
				json.Unmarshal([]byte(itemPack), &info.Items)
			}
			list = append(list, info)
		}
	}
	return list
}

func getUserLastSignin(userId int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SigninWheel_GetLastTime")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}

	crdateStr := (retRows[0][0]).(string)
	crdate := common.ParseTime(crdateStr)
	return common.GetStamp(crdate)
}

func getSigninWheelConfig() string {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SigninWheel_GetConfig")
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return ""
	}
	return (retRows[0][0]).(string)
}

func userSigninWheel(userId, base, multiple, itemId int, items []item.ItemPack) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SigninWheel_DoSignin")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Base", database.AdParamInput, database.AdInteger, 4, base)
	statement.AddParamter("@Multiple", database.AdParamInput, database.AdInteger, 4, multiple)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdInteger, 4, base*multiple)
	statement.AddParamter("@ItemId", database.AdParamInput, database.AdInteger, 4, itemId)
	ip, _ := json.Marshal(items)
	statement.AddParamter("@ItemPack", database.AdParamInput, database.AdVarChar, 64, string(ip))
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	dbengine.CenterDB.ExecSql(sqlstring)
}
