package signinwheel

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

var mgr *signinwheelmgr

type SigninWheelResult struct {
	Base     int // 基础数值
	Multiple int // 倍率
	ItemId   int
	Items    []item.ItemPack
}

type SigninWheelHistory struct {
	UserId   int
	NickName string
	FaceId   int
	FaceUrl  string
	SigninWheelResult
}

type config_param struct {
	Param  int // 参数
	Chance int `json:",omitempty"` // 概率,客户端看不到
	ItemId int // 物品Id,默认是金币
	Count  int // 物品数量,默认是金币
}

type SigninWheelConfig struct {
	Cost                int
	totalBaseChance     int
	totalMultipleChance int
	Base                []config_param // 基础配置
	Multiple            []config_param // 倍数配置
}

type UserSiginWheelInfo struct {
	CanSignin         bool // 我是否可以签到
	MyPlayTimes       int  //我的播放次数
	SysPlayTimes      int  //系统播放次数
	SigninWheelConfig      // 签到配置信息
}

func Run() {
	mgr = newSigninWheelManager()
	log.Debug("signinwheel runing")
}

func DoSignin(userId int, ipAddress string, userGold int) (SigninWheelResult, int) {
	return mgr.doSignin(userId, ipAddress, userGold)
}

func GetHistory() []SigninWheelHistory {
	return mgr.getHistory()
}

func GetSigninWheelInfo(userId int) UserSiginWheelInfo {
	return mgr.getSigninWheelInfo(userId)
}

func CheckSignTip(userId int) bool {
	return mgr.canSignin(userId, "", true)
}

func Dump(param1, param2 string) {
	switch param1 {
	case "sys":
		mgr.dumpSys(param2)
	case "user":
		mgr.dumpUser(param2)
	case "history":
		mgr.dumpHistory()
	case "test":
		mgr.test(param2)
	case "testlevel":
		mgr.testLevel(param2)
	default:
		log.Debug("signinwheel.Dump unhandled %s:%s", param1, param2)
	}
}
