package battlepass

import (
	item "bet24.com/servers/micros/item_inventory/proto"
	"time"
)

const (
	GrowthTermStatus_none     = iota // 没有
	GrowthTermStatus_complete        // 可领取
	GrowthTermStatus_awarded         // 已领取
)

type BattlePassSeason struct {
	StartDate int
	EndDate   int
	MaxDayExp int
	MaxExp    int
	SeasonId  int
	Packs     []GrowthPack
}

func (bs *BattlePassSeason) isValid() bool {
	dayIndex := getDayIndex()
	return dayIndex >= bs.StartDate && dayIndex <= bs.EndDate
}

type GrowthPack struct {
	Id            int    // 礼包ID
	Name          string // 名称
	ProductId     string // 商城产品ID
	Price         int    // 价格
	FullProductId string
	FullPrice     int          // 完整购买价格
	Terms         []GrowthTerm // 礼包列表
}

type GrowthTerm struct {
	TermIndex int             // index
	Name      string          // 名称
	NeedExp   int             // 所需经验
	Items     []item.ItemPack // 礼包内容
}

type UserGrowthTerm struct {
	UserId       int // 用户ID，客户端不发
	GrowthPackId int // 礼包ID
	TermIndex    int // index
	NeedExp      int // 当前经验值
	Status       int // 目前状态
}

func getDayIndex() int {
	now := time.Now()
	return now.Year() + int(now.Month()) + now.Day()
}
