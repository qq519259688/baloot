package battlepass

import (
	"encoding/json"
	"os"
	"sync"
	"time"

	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const season_config_key = "battlepass_seasons"
const refresh_config_sec = 3600

type seasonmanager struct {
	seasons         []*BattlePassSeason
	configString    string
	lock            *sync.RWMutex
	currentSeasonId int
}

func newSeasonManager() *seasonmanager {
	ret := new(seasonmanager)
	ret.lock = &sync.RWMutex{}
	ret.readConf()
	return ret
}

func (m *seasonmanager) refreshAndChesk() {
	time.AfterFunc(refresh_config_sec*time.Second, m.refreshAndChesk)
	m.readConf()
	m.checkSeason()
}

func (m *seasonmanager) checkSeason() {
	seasonId := 0
	s := m.getValidSeason()
	if s != nil {
		seasonId = s.SeasonId
	}
	if m.currentSeasonId == seasonId {
		return
	}

	if seasonId == 0 {
		mgr.seasonEnd()
	} else {
		mgr.seasonStart(seasonId)
	}

	m.currentSeasonId = seasonId

}

func (m *seasonmanager) readConf() {
	configString := platformconfig.GetConfig(season_config_key)

	if configString == "" {
		data, err := os.ReadFile("fishconf/battlepass.json")
		if err != nil {
			log.Release("battlepass.seasonmanager.loadData read battlepass failed")
			return
		}
		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(season_config_key, configString)
		}
	}

	if configString == m.configString {
		return
	}

	m.configString = configString
	m.lock.Lock()
	err := json.Unmarshal([]byte(configString), &m.seasons)
	m.lock.Unlock()
	if err != nil {
		log.Release("battlepass.manager.loadData Unmarshal failed err:%v", err)
	}

}

func (m *seasonmanager) getValidSeason() *BattlePassSeason {
	m.lock.RLock()
	defer m.lock.RUnlock()
	for _, v := range m.seasons {
		if v.isValid() {
			return v
		}
	}
	return nil
}

func (m *seasonmanager) getMaxExp() int {
	s := m.getValidSeason()
	if s == nil {
		return 0
	}
	return s.MaxExp
}
func (m *seasonmanager) getMaxDayExp() int {
	s := m.getValidSeason()
	if s == nil {
		return 0
	}
	return s.MaxDayExp
}

func (m *seasonmanager) getGrowthPacks() []GrowthPack {
	var ret []GrowthPack
	s := m.getValidSeason()
	if s == nil {
		return ret
	}

	return s.Packs
}

func (m *seasonmanager) getGrowthPack(packId int) *GrowthPack {
	s := m.getValidSeason()
	if s == nil {
		return nil
	}
	for i := 0; i < len(s.Packs); i++ {
		if s.Packs[i].Id == packId {
			return &s.Packs[i]
		}
	}

	return nil
}

func (m *seasonmanager) getGrowthPackByProduct(productId string) *GrowthPack {
	s := m.getValidSeason()
	if s == nil {
		return nil
	}
	for i := 0; i < len(s.Packs); i++ {
		if s.Packs[i].ProductId == productId || s.Packs[i].FullProductId == productId {
			return &s.Packs[i]
		}
	}
	return nil
}

func (m *seasonmanager) getGrowthTerm(growthPackId, index int) *GrowthTerm {
	pack := m.getGrowthPack(growthPackId)
	if pack == nil {
		return nil
	}

	for _, term := range pack.Terms {
		if term.TermIndex == index {
			return &term
		}
	}

	return nil
}
