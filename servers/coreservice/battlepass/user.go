package battlepass

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	notification "bet24.com/servers/micros/notification/proto"
	"fmt"
)

type user struct {
	UserId   int
	Exp      int
	DayExp   int
	DayIndex int
	Terms    []*UserGrowthTerm
	isOnline bool
}

func newUser(userId int) *user {
	u := new(user)
	u.UserId = userId
	u.initData()
	u.isOnline = true
	return u
}

func (u *user) initData() {
	u.Exp = 0
	u.DayExp = 0
	u.DayIndex = 0
	u.Terms = []*UserGrowthTerm{}
	// 如果有免费礼包，但用户还没有买，则自动购买
	added := false
	packs := mgr.getGrowthPacks()
	for _, v := range packs {
		if v.Price == 0 {
			u.addPack(&v)
			added = true
		}
	}
	if added {
		u.addExp(0)
	}
}

func (u *user) addPack(pack *GrowthPack) {
	if u.isPackExist(pack.Id) {
		return
	}
	for _, v := range pack.Terms {
		u.Terms = append(u.Terms, &UserGrowthTerm{
			UserId:       u.UserId,
			GrowthPackId: pack.Id,
			TermIndex:    v.TermIndex,
			Status:       GrowthTermStatus_none,
			NeedExp:      v.NeedExp,
		})
	}
}

func (u *user) isPackExist(packId int) bool {
	for _, v := range u.Terms {
		if v.GrowthPackId == packId {
			return true
		}
	}
	return false
}

func (u *user) addExp(exp int) {
	dayIndex := getDayIndex()
	if dayIndex != u.DayIndex {
		u.DayExp = 0
		u.DayIndex = dayIndex
	}
	maxExp := mgr.getMaxExp()
	maxDayExp := mgr.getMaxDayExp()

	if exp+u.DayExp > maxDayExp {
		exp = maxDayExp - u.DayExp
	}

	if exp+u.Exp > maxExp {
		exp = maxExp - u.Exp
	}
	if exp < 0 {
		exp = 0
	}

	u.Exp += exp
	redPoint := false
	// 如果触发可领取
	for _, v := range u.Terms {
		if v.Status == GrowthTermStatus_none && v.NeedExp <= u.Exp {
			redPoint = true
			v.Status = GrowthTermStatus_complete
		}
	}

	// 通知
	if redPoint {
		notification.AddNotification(u.UserId, notification.Notification_BattlePass, "")
	}
}

func (u *user) rewardTerm(packId, termId int) (bool, string) {
	bought := false
	term := mgr.getGrowthTerm(packId, termId)
	if term == nil {
		log.Debug("battlepass_manager.user.rewardTerm invalid params %d.%d", packId, termId)
		return false, "invalid packId"
	}
	for _, v := range u.Terms {
		if v.GrowthPackId != packId {
			continue
		}
		bought = true
		if v.TermIndex != termId {
			continue
		}
		if v.Status != GrowthTermStatus_complete {
			return false, "not complete"
		} else {
			inventory_add(u.UserId, term.Items, fmt.Sprintf("battlepass[%d]", packId), common.LOGTYPE_PACK_GROWTH)
			v.Status = GrowthTermStatus_awarded
			//m.updateUserTerm(v)
			return true, "success"
		}
	}
	if bought {
		return false, "failed"
	} else {
		return false, "not bought"
	}
}

func (u *user) addTerm(term *UserGrowthTerm) {
	u.Terms = append(u.Terms, term)
}
