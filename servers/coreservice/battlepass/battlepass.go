package battlepass

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

var mgr *battlepass_manager

var inventory_add func(userId int, items []item.ItemPack, desc string, logType int) bool

func Run(inventoryAddFunc func(userId int, items []item.ItemPack, desc string, logType int) bool) {
	mgr = newBattlePassManager()
	inventory_add = inventoryAddFunc
	mgr.initData()
}

func AddUser(userId int) {
	mgr.onUserEnter(userId)
}

func RemoveUser(userId int) {
	mgr.onUserExit(userId)
}

func BuyGrowthPack(userId int, productId string) (int, string) {
	return mgr.buyGrowthPack(userId, productId)
}

func GetBattlePassPacks() []GrowthPack {
	return mgr.getGrowthPacks()
}

func GetUserBattlePass(userId int) []*UserGrowthTerm {
	return mgr.getUserGrowthTerms(userId)
}

func UserAwardBattlePass(userId int, growthPackId, index int) (bool, string) {
	return mgr.userAwardTerm(userId, growthPackId, index)
}

func Dump(param1, param2 string) {
	switch param1 {
	case "sys":
		mgr.dumpSys(param2)
	case "user":
		mgr.dumpUser(param2)
	default:
		log.Debug("battlepass.Dump unhandled %s:%s", param1, param2)
	}
}

func AddUserExp(userId int, exp int) {
	mgr.addExp(userId, exp)
}
