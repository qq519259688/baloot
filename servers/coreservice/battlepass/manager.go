package battlepass

import (
	"bet24.com/log"
	"encoding/json"
	"strconv"
)

type battlepass_manager struct {
	sm *seasonmanager
	um *usermanager
}

func newBattlePassManager() *battlepass_manager {
	m := new(battlepass_manager)
	return m
}
func (m *battlepass_manager) initData() {
	m.sm = newSeasonManager()
	m.um = newUserManager()
}

func (m *battlepass_manager) onUserEnter(userId int) {
	m.um.onUserEnter(userId)
}

func (m *battlepass_manager) onUserExit(userId int) {
	m.um.onUserExit(userId)
}

func (m *battlepass_manager) getGrowthPacks() []GrowthPack {
	return m.sm.getGrowthPacks()
}

func (m *battlepass_manager) getGrowthPack(packId int) *GrowthPack {
	return m.sm.getGrowthPack(packId)
}

func (m *battlepass_manager) getGrowthPackByProduct(productId string) *GrowthPack {
	return m.sm.getGrowthPackByProduct(productId)
}

func (m *battlepass_manager) getUserGrowthTerms(userId int) []*UserGrowthTerm {
	return m.um.getUserGrowthTerms(userId)
}

func (m *battlepass_manager) buyGrowthPack(userId int, productId string) (int, string) {
	return m.um.buyGrowthPack(userId, productId)
}

func (m *battlepass_manager) getGrowthTerm(growthPackId, index int) *GrowthTerm {
	return m.sm.getGrowthTerm(growthPackId, index)
}

func (m *battlepass_manager) userAwardTerm(userId int, growthPackId, index int) (bool, string) {
	return m.um.userAwardTerm(userId, growthPackId, index)
}

func (m *battlepass_manager) getMaxExp() int {
	return m.sm.getMaxExp()
}
func (m *battlepass_manager) getMaxDayExp() int {
	return m.sm.getMaxDayExp()
}

func (m *battlepass_manager) addExp(userId int, exp int) {
	m.um.addExp(userId, exp)
}

func (m *battlepass_manager) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("battlepass_manager.dumpSys %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	d, _ := json.Marshal(m.sm.seasons)
	log.Release(string(d))
}

func (m *battlepass_manager) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("battlepass_manager.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}
	si := m.getUserGrowthTerms(userId)
	if si == nil {
		log.Release("user %d not exist", userId)
		return
	}
	d, _ := json.Marshal(si)
	log.Release(string(d))
}

// 清理数据
func (m *battlepass_manager) seasonEnd() {
	log.Release("battlepass_manager.seasonEnd")
	m.um.clear()
}

func (m *battlepass_manager) seasonStart(seasonId int) {
	log.Release("battlepass_manager.seasonStart %d", seasonId)
	m.um.clear()
	m.um.SeasonId = seasonId
}
