package chat

import "bet24.com/log"

var mgr *chatmgr
var chMgr *channelMgr

func Run() {
	log.Debug("chat running")
	mgr = newChatMgr()
	chMgr = newChannelMgr()
}

func OnUserEnter(userId int) {
	chMgr.onUserEnter(userId)
}

func OnUserExit(userId int) {
	chMgr.onUserExit(userId)
}

func GetChatMsg(userId, channelID int) string {
	return mgr.getChatMsg(userId, channelID)
}

func SendChatMsg(channelID, userId, faceId, sex, vipLevel, recvID int,
	nickName, faceUrl, chatMsg, ipAddress string, messageType int) bool {
	return mgr.sendChatMsg(channelID, userId, faceId, sex, vipLevel, recvID, nickName, faceUrl, chatMsg, ipAddress, false, messageType)
}

func Dump(param1, param2 string) {
	if param1 == "world" {
		mgr.dump(param2)
		return
	}
	chMgr.dump(param1, param2)
}

func SendChannelChat(from, to int, msg string, msgType int) string {
	return chMgr.sendChat(from, to, msg, msgType)
}

func GetChannelChat(channelKey string, userId int) string {
	return chMgr.getChat(channelKey, userId)
}

func GetChannelInfo(channelKey string) string {
	return chMgr.getChannelInfo(channelKey)
}

func ClearChannelHistory(channelKey string, userId int) {
	chMgr.userClear(channelKey, userId)
}

// 获取机器人聊天列表
func GetRobotChatList() []robot_chat {
	return mgr.getRobotChatList()
}

// 获取机器人聊天信息
func GetRobotChatInfo(userId int) robot_chat {
	return mgr.getRobotChatInfo(userId)
}

// 添加机器人聊天信息
func AddRobotChatInfo(userId int, msg string, seconds int, beginTime, endTime string) bool {
	return mgr.addRobotChat(userId, msg, seconds, beginTime, endTime)
}

// 修改机器人聊天信息
func UpdateRobotChatInfo(userId int, msg string, seconds int, beginTime, endTime string) bool {
	return mgr.updateRobotChat(userId, msg, seconds, beginTime, endTime)
}

// 删除机器人聊天信息
func DelRobotChatInfo(userId int) bool {
	return mgr.delRobotChat(userId)
}

func SendGiftMessage(from, to int, msg string) string {
	return chMgr.sendGiftMsg(from, to, msg)
}

func RemoveChannel(userId1, userId2 int) {
	chMgr.removeChannel(userId1, userId2)
}
