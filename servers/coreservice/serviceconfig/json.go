package serviceconfig

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	slog "bet24.com/log"
	"bet24.com/servers/common"
)

var Server struct {
	LogLevel        string
	FileLevel       string
	LogPath         string
	ServerPort      int
	MaxConnNum      int
	ConsulPort      int
	MonitorPort     int
	ChannelUrl      string
	ChannelPassword string
	RedisDB         int
	LastHour        int
	IsCasual        int // 是否休闲平台  1=休闲平台   其他不是
	MinWinBroadcast int // 广播赢金最低金额
	GameWinString   string
	ReturnAwards    string
	MinWithdraw     int // 最低提取金额
	ReviewAmount    int // 评论奖励
	PurchaseCfg
}

// 10K购
type PurchaseCfg struct {
	GoldFee        int    `json:"purchase.goldFee"`        // 10W金币费用 ==> 100000
	AwardAmount    int    `json:"purchase.awardAmount"`    // 奖励金额 ==> 100000000
	BeginHour      string `json:"purchase.beginHour"`      // 活动开启时间(20时) ==> 20h
	EndHour        string `json:"purchase.endHour"`        // 活动结束时间(22时) ==> 22h
	Duration       string `json:"purchase.duration"`       // 30分钟(1800秒) ==> 1800s
	MaxNumber      int    `json:"purchase.maxNumber"`      // 最大号码 ==> 999999
	ItemId         int    `json:"purchase.itemId"`         // 道具ID ==> 2
	SuccessTitle   string `json:"purchase.successTitle"`   // 命中标题
	SuccessContent string `json:"purchase.successContent"` // 命中内容
	FailTitle      string `json:"purchase.failTitle"`      // 未命中标题
	FailContent    string `json:"purchase.failContent"`    // 未命中内容
}

// 一些配置
var SpecialCfg struct {
	ChatCost int //聊天金币
	//奖池
	MinGoldPool int //奖池下限，低于此值由系统补足
	MaxGoldPool int //奖池下限，低于此值由系统补足
	MinChipPool int // 筹码jackpot下限
	MaxChipPool int // 筹码jackpot上限
	SubsidyGold int //低于奖池下限后，系统要补贴到该金额
	GameTax     int //抽税比例 0-100
	TaxLine     int //奖池金额达到后不再抽税，低于该金额则要抽税
	PrizeCost   int //多少金币抽一次
	PrizeTimes  int //每天抽奖次数，0点重置

	//好友
	FriendGift int //赠送金币
	GiftTimes  int //每天赠送次数上限
}

// 是否休闲平台 （true=休闲平台  false=其他平台）
func IsCasual() bool {
	return Server.IsCasual == 1
}

func Run() {
	data, err := os.ReadFile("coreservice.json")
	if err != nil {
		log.Fatalf("read config failed fishconf/coreservice.json %v", err)
	}

	err = json.Unmarshal(data, &Server)
	if err != nil {
		log.Fatalf("Unmarshal config failed fishconf/coreservice.json err:%v", err)
	}
	if Server.ConsulPort == 0 {
	//	Server.ConsulPort = 5500
	Server.ConsulPort = 8500
	}

	fmt.Println(Server)

	//其他配置， 暂时用
	data, err = os.ReadFile("SpecialCfg.json")
	if err != nil {
		log.Fatalf("read config failed fishconf/SpecialCfg.json %v", err)
	}
	err = json.Unmarshal(data, &SpecialCfg)
	if err != nil {
		log.Fatalf("Unmarshal config failed fishconf/SpecialCfg.json err:%v", err)
	}

	logger, err := slog.New(Server.LogLevel, Server.FileLevel, Server.LogPath, log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	if Server.MonitorPort == 0 {
		Server.MonitorPort = 5100
	}

	now := common.GetNowTime()
	Server.LastHour = now.Hour()
	refreshLogFile()
}

func refreshLogFile() {
	time.AfterFunc(5*time.Minute, refreshLogFile)
	doRefreshLogFile()
}

func doRefreshLogFile() {
	if Server.LogPath != "" {
		now := time.Now()
		if now.Hour() != Server.LastHour {
			Server.LastHour = now.Hour()
			slog.RecreateFileLog(Server.LogPath, log.LstdFlags)
		}
	}
}
