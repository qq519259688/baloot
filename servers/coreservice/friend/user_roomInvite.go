package friend

import (
	"encoding/json"
	"fmt"
	"sort"

	"bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/common"
	notification "bet24.com/servers/micros/notification/proto"
)

func getRedisKey(userId int) string {
	return fmt.Sprintf("friend:roomInvite:userId:%d", userId)
}

// 读入redis
func (u *user_friend) roomInviteLoadFromRedis() {
	data, ok := redis.String_Get(getRedisKey(u.UserID))
	if data == "" || !ok {
		return
	}
	if err := json.Unmarshal([]byte(data), &u.RoomInviteHistory); err != nil {
		log.Error("friend.roomInviteLoadFromRedis json unmarshal failed err:%v, %s", err, data)
		return
	}
	return
}

// 存入redis
func (u *user_friend) roomInviteToRedis() {
	d, _ := json.Marshal(u.RoomInviteHistory)
	go redis.String_Set(getRedisKey(u.UserID), string(d))
}

// 邀请好友
func (u *user_friend) roomInvite(nickName string, toUserId, roomNo int) *RoomInviteInfo {
	u.lock.RLock()
	defer u.lock.RUnlock()

	// 检查房间邀请列表，是否已邀请
	for _, v := range u.RoomInviteHistory {
		// log.Debug("user_roomInvite userId=%d nickName=%s toUserId=%d roomNo=%d v==>%+v", u.UserID, nickName, toUserId, roomNo, v)
		if v.UserID == toUserId && v.RoomNo == roomNo {
			return nil
		}
	}

	info := RoomInviteInfo{
		Id:       common.GetTimeStamp(),
		UserID:   u.UserID,
		NickName: nickName,
		RoomNo:   roomNo,
		Invalid:  true,
	}
	buf, _ := json.Marshal(notification.NotificationFriend{
		NotifyId: Friend_Notify_RoomInvite,
		UserId:   u.UserID,
		Data:     info,
	})
	go notification.AddNotification(toUserId, notification.Notification_Friend, string(buf))
	return &info
}

// 添加房间邀请历史列表
func (u *user_friend) addRoomInviteHistory(info *RoomInviteInfo) {
	u.lock.Lock()
	max := 50
	u.RoomInviteHistory = append(u.RoomInviteHistory, info)
	if count := len(u.RoomInviteHistory); count > max {
		u.RoomInviteHistory = u.RoomInviteHistory[count-max:]
	}
	u.lock.Unlock()

	// 写入redis
	u.roomInviteToRedis()
}

// 获取房间邀请列表
func (u *user_friend) getRoomInviteList() []*RoomInviteInfo {
	u.lock.RLock()
	defer u.lock.RUnlock()

	ts := common.GetTimeStamp()
	for i := 0; i < len(u.RoomInviteHistory); i++ {
		if ts >= u.RoomInviteHistory[i].Id+1800 {
			u.RoomInviteHistory[i].Invalid = false
		}
	}

	var list []*RoomInviteInfo
	list = u.RoomInviteHistory
	sort.SliceStable(list, func(i, j int) bool {
		return list[i].Id > list[j].Id
	})

	return list
}

// 房间邀请无效
func (u *user_friend) roomInviteInvalid(roomNo int) {
	for i := 0; i < len(u.RoomInviteHistory); i++ {
		if u.RoomInviteHistory[i].RoomNo != roomNo {
			continue
		}

		u.lock.Lock()
		u.RoomInviteHistory[i].Invalid = false
		u.lock.Unlock()

		// 写入redis
		u.roomInviteToRedis()
		break
	}
	return
}
