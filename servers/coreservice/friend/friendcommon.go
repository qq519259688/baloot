package friend

import userservices "bet24.com/servers/micros/userservices/proto"

const (
	MAX_GIVETIMES        = 3      //最多赠送次数
	GIFT_STATUS_NOT      = 0      //0=没有礼物
	GIFT_STATUS_HAVE     = 1      //1=有礼物还没领
	GIFT_STATUS_RECEIVED = 2      //2=有礼物已领取
	GIVE_STATUS_NOT      = 0      //0=没有赠送
	GIVE_STATUS_HAVE     = 1      //1=已赠送
	GIVE_GOLD            = 100000 //赠送金币
	DEFAULT_FRIEND_COUNT = 100    //默认好友上限数量
)

const (
	CANNOT_ADD_ONESELF = iota // 0=不能申请自己
	ADD_SUCCESS               // 1=申请成功
	ALREADY_A_FRIEND          // 2=已经是好友
	ADD_TO_EXAMINE            // 3=已添加，待审核
	FRIEND_LIMIT              // 4=好友上限
)

const (
	Friend_Notify_All        = iota //0=刷新好友及申请列表
	Friend_Notify_Friend            //1=刷新好友列表
	Friend_Notify_Verify            //2=刷新申请列表
	Friend_Notify_Gift              //3=刷新领取礼物
	Friend_Notify_Status            //4=好友状态
	Friend_Notify_RoomInvite        //5=房间邀请(私人场)
)

const (
	GameStatus_Online  = "Online"
	GameStatus_Offline = "Offline"
	GameStatus_Playing = "Playing"
	GameStatus_Hall    = "Hall"
)

const (
	FriendShip_Normal         = 0 // 0=普通
	FriendShip_Friend         = 1 // 1=好友
	FriendShip_AlreadyApplied = 2 // 2=已申请(自己申请的，待对方审核)
	FriendShip_ToBeReviewed   = 3 // 3=待审核（对方申请的，待自己审核）
	FriendShip_Black          = 4 // 4=黑名单
)

type FriendBase struct {
	FriendID         int    // 好友ID
	NickName         string // 昵称
	Sex              int    // 性别
	FaceID           int    // 头像ID
	FaceUrl          string // 头像URL
	UserWords        string // 个性签名
	GameStatus       string // 游戏状态 离线/在线/在xxxx游戏中
	VipLevel         int    // Vip
	VipExpire        int
	PrivateRoomCount int                           // 私人场局数
	Currency         string                        // 币种
	Level            int                           // 人物等级
	Gold             int                           // 金币
	Charm            int                           // 魅力值
	Decorations      []userservices.UserDecoration `json:",omitempty"`
}

type FriendItem struct {
	FriendBase
	IsGift   int // 是否有礼物领取 0，没有礼物， 1，有礼物还没领取，2，有礼物已领取
	GiftTime int // 领取时间戳
	IsGive   int // 是否可以赠送礼物
	GiveTime int // 赠送时间戳
}

type SearchInfo struct {
	FriendBase
	IsFriend int // 是否好友
}

type RoomInviteInfo struct {
	Id       int
	UserID   int
	NickName string
	RoomNo   int
	Invalid  bool
}

// 好友数量
type FriendCount struct {
	Base  int // 基础好友数
	Extra int // 额外好友数
}

// 潜在好友，最近有一起玩游戏的，赠送礼物的
type PotentialFriend struct {
	UserId   int
	FriendID int
	MetTime  int
	Memo     string
	// 以下变量数据库不存
	NickName    string
	FaceID      int
	FaceUrl     string
	Decorations []userservices.UserDecoration `json:",omitempty"`
}

// 拉黑好友
type BlackItem struct {
	UserId int    // 黑名单用户id
	Crdate string // 时间
}
