package friend

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"encoding/json"
	"runtime/debug"
)

// 获取好友信息
func searchInfo(userId, toUserId int, toNickName string) []*SearchInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*SearchInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_SearchInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	statement.AddParamter("@ToNickName", database.AdParamInput, database.AdNVarChar, 32, toNickName)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info SearchInfo

		info.FriendID = int(ret[0].(int64))

		if info.FriendID <= 0 {
			continue
		}

		list = append(list, &info)
	}

	return list
}

// 获取好友列表，并返回今天剩余领取次数
func getList(userId int) (int, []*FriendItem) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	giveTimes := 0
	var list []*FriendItem
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserFriends_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, 0)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return giveTimes, list
	}

	dayIndex := common.GetDayIndex(common.GetTimeStamp())

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info FriendItem

		info.FriendID = int((ret[0]).(int64))
		info.IsGift = int((ret[1]).(int64))

		giftStr := (ret[2]).(string)
		giftTime := common.ParseTime(giftStr)
		info.GiftTime = common.GetStamp(giftTime)

		giftDayIndex := common.GetDayIndex(info.GiftTime)
		if giftDayIndex < dayIndex && info.IsGift == GIFT_STATUS_RECEIVED {
			info.IsGift = 0
		}

		giveStr := (ret[3]).(string)
		giveTime := common.ParseTime(giveStr)
		info.GiveTime = common.GetStamp(giveTime)

		giveDayIndex := common.GetDayIndex(info.GiveTime)
		if giveDayIndex >= dayIndex {
			info.IsGive = GIVE_STATUS_HAVE
			giveTimes++
		}

		list = append(list, &info)

		//log.Debug("getFriendInfo userId=%d giftStr=%s giftTime=%v giftDayIndex=%d giveStr=%v giveTime=%v giveDayIndex=%d dayIndex=%d giveTimes=%d ==> %+v",
		//	userId, giftStr, giftTime, giftDayIndex, giveStr, giveTime, giveDayIndex, dayIndex, giveTimes, info)
	}

	return MAX_GIVETIMES - giveTimes, list
}

// 获取审核列表(谁来申请的)
func getVerifyList(userId int) []*FriendBase {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*FriendBase
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserFriends_GetVerifyList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info FriendBase

		info.FriendID = int((ret[0]).(int64))
		list = append(list, &info)
	}

	return list
}

// 获取申请列表(自己申请的)
func getApplyList(userId int) []*FriendBase {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []*FriendBase
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserFriends_GetApplyList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info FriendBase

		info.FriendID = int((ret[0]).(int64))
		list = append(list, &info)
	}

	return list
}

// 好友申请
func apply(userId, toUserId int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UserFriends_Apply")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	sqlstring := statement.GenSql()
	dbengine.Execute(sqlstring)
}

// 申请处理
func deal(userId, toUserId, status int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UserFriends_DealApply")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, status)
	sqlstring := statement.GenSql()
	dbengine.Execute(sqlstring)
}

// 删除好友
func del(userId, toUserId int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UserFriends_Del")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	sqlstring := statement.GenSql()
	dbengine.Execute(sqlstring)
}

// 赠送礼物
func giveGift(userId, toUserId int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UserFriends_GiveGift")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	dbengine.Execute(sqlstring)
}

// 领取礼物
func getGift(userId, toUserId int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UserFriends_GetGift")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	dbengine.Execute(sqlstring)
}

// 获取潜在好友列表 sort by MetTime desc
func getPotentialFriendList(userId int) []PotentialFriend {
	var list []PotentialFriend
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserFriends_GetPotentialList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonRet := dbengine.Execute(sqlString)
	err := json.Unmarshal([]byte(jsonRet), &list)
	if err != nil {
		log.Release("getPotentialFriendList Unmarshal failed %s", jsonRet)
	}
	return list
}

// 删除某一条潜在好友记录
func deletePotentialFriend(userId int, friendUserId int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserFriends_DelPotential")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@FriendUserID", database.AdParamInput, database.AdInteger, 4, friendUserId)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}

// 添加一个潜在好友
func addPotentialFriend(userId int, friendUserId int, metTime int, memo string) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserFriends_AddPotential")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@FriendUserID", database.AdParamInput, database.AdInteger, 4, friendUserId)
	statement.AddParamter("@Memo", database.AdParamInput, database.AdNVarChar, 64, memo)
	statement.AddParamter("@MetTime", database.AdParamInput, database.AdInteger, 4, metTime)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}

// 获取黑名单列表
func trans_GetBlackList(userId int) []BlackItem {
	var list []BlackItem
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserFriends_GetBlackList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	if len(jsonData) > 0 {
		if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
			log.Error("trans_GetBlackList json unmarshal err %v", err)
		}
	}
	return list
}

// 添加黑名单
func trans_AddBlack(userId, toUserId int, Crdate string) int {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserFriends_AddBlack")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdVarChar, 20, Crdate)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return 1
}

// 删除黑名单
func trans_DelBlack(userId, toUserId int) int {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserFriends_DelBlack")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return 1
}

// 是否黑名单
func trans_IsBlack(userId, toUserId int) bool {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserFriends_ExistBlack")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return false
	}
	return int(retRows[0][0].(int64)) > 0
}
