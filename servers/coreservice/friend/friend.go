package friend

import "bet24.com/log"

var mgr *friendmgr

func Run() {
	log.Debug("bank running")
	mgr = newFriendMgr()
}

func AddUser(userId int) {
	mgr.onUserEnter(userId)
}

func RemoveUser(userId int) {
	mgr.onUserExit(userId)
}

func GetSearchUserinfo(userId, targetUserID int, targetNickName string) []*SearchInfo {
	return mgr.searchInfo(userId, targetUserID, targetNickName)
}

func GetFriendList(userId int) []*FriendItem {
	return mgr.getFriendList(userId)
}

func FriendApply(userId, targetUserID int) int {
	return mgr.friendApply(userId, targetUserID)
}

func DelFriend(userId, targetUserID int) []*FriendItem {
	return mgr.delFriend(userId, targetUserID)
}

func FriendVerifyList(userId int) []*FriendBase {
	return mgr.friendVerifyList(userId)
}

func FriendHandleApply(userId, targetUserID, apply int) []*FriendItem {
	return mgr.friendHandleApply(userId, targetUserID, apply)
}

func FriendGiveGift(userId, targetUserID int) int {
	return mgr.friendGiveGift(userId, targetUserID)
}

func FriendGetGift(userId, targetUserID int, ipAddress string) int {
	return mgr.friendGetGift(userId, targetUserID, ipAddress)
}

func SetUserStatus(userId, isOnline int, serverName string) {
	mgr.setUserStatus(userId, isOnline, serverName)
}

func IfFriend(userId, targetId int) int {
	return mgr.ifFriend(userId, targetId)
}

// 大厅邀请好友
func RoomInvite(userId int, nickName string, toUserId, roomNo int) {
	mgr.roomInvite(userId, nickName, toUserId, roomNo)
}

// 获取房间邀请列表
func GetRoomInviteList(userId int) []*RoomInviteInfo {
	return mgr.getRoomInviteList(userId)
}

// 房间邀请无效
func RoomInviteInvalid(userId, roomNo int) {
	mgr.roomInviteInvalid(userId, roomNo)
}

// 获取好友上限
func GetMaxFriendCount(userId int) FriendCount {
	return mgr.getMaxFriendCount(userId)
}

func Dump(param1, param2 string) {
	mgr.dumpFriends()
}

func GetPotentialList(userId int) string {
	return mgr.getPotentialFriendList(userId)
}

func AddPotentialFriend(userId int, toUserId int, memo string) {
	mgr.addPotentialFriend(userId, toUserId, memo)
}

// 黑名单列表
func GetBlackList(userId int) []BlackItem {
	return mgr.getBlackList(userId)
}

// 添加黑名单
func AddBlack(userId, toUserId int) (retCode int, message string) {
	return mgr.addBlack(userId, toUserId)
}

// 删除黑名单
func DelBlack(userId, toUserId int) (retCode int, message string) {
	return mgr.delBlack(userId, toUserId)
}

func IsBlackListUser(userId, toUserId int) bool {
	return mgr.isBlackListUser(userId, toUserId)
}

func IsBlackListUserIn(userId int, toUserIds []int) bool {
	return mgr.isBlackListUserIn(userId, toUserIds)
}
