package userwin

import (
	"bet24.com/log"
	"strconv"
	"sync"
)

var instance *userwinmanager

type userwinmanager struct {
	lock     *sync.RWMutex
	userlist map[int]int
}

func getInstance() *userwinmanager {
	if instance == nil {
		instance = new(userwinmanager)
		instance.ctor()
	}
	return instance
}

func (um *userwinmanager) ctor() {
	um.userlist = make(map[int]int)
	um.lock = &sync.RWMutex{}
}

func (um *userwinmanager) addUser(userId int) {
	um.lock.RLock()
	_, ok := um.userlist[userId]
	um.lock.RUnlock()
	if ok {
		return
	}
	um.lock.Lock()
	um.userlist[userId] = 0
	um.lock.Unlock()
}

func (um *userwinmanager) removeUser(userId int) {
	um.lock.Lock()
	delete(um.userlist, userId)
	um.lock.Unlock()
}

func (um *userwinmanager) getScore(userId int) int {
	um.lock.Lock()
	s, ok := um.userlist[userId]
	if !ok {
		delete(um.userlist, userId)
		s = 0
	} else {
		um.userlist[userId] = 0
	}
	um.lock.Unlock()
	return s
}

func (um *userwinmanager) addScore(userId, score int) {
	um.addUser(userId)
	um.lock.Lock()
	um.userlist[userId] += score
	um.lock.Unlock()
}

func (um *userwinmanager) dump(param string) {
	log.Release("-------------------------------")
	log.Release("userwinmanager.dump")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	if param == "" {
		um.lock.RLock()
		for k, v := range um.userlist {
			log.Release("    %d:%d", k, v)
		}
		um.lock.RUnlock()
		return
	}

	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}
	um.lock.RLock()
	log.Release("    %d:%d", userId, um.userlist[userId])
	um.lock.RUnlock()
}
