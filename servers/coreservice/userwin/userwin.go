package userwin

import (
	"bet24.com/log"
)

func Run() {
	log.Release("userwin.Run")
	getInstance()
}

func AddUser(userId int) {
	getInstance().addUser(userId)
}

func RemoveUser(userId int) {
	getInstance().removeUser(userId)
}

func AddScore(userId int, score int) {
	getInstance().addScore(userId, score)
}

func GetScore(userId int) int {
	return getInstance().getScore(userId)
}

func Dump(param string) {
	getInstance().dump(param)
}
