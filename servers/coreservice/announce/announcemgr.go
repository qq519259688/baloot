package announce

import (
	"encoding/json"
	"sync"

	"bet24.com/servers/common"

	"bet24.com/log"
)

type announceMgr struct {
	list []*info
	lock *sync.RWMutex
}

func newAnnounceMgr() *announceMgr {
	obj := new(announceMgr)
	obj.lock = &sync.RWMutex{}
	obj.load()
	log.Debug("announce manager running")
	return obj
}

func (this *announceMgr) load() {
	list := getList()
	this.lock.Lock()
	defer this.lock.Unlock()
	this.list = list
}

func (this *announceMgr) getJson() string {
	var list []*info
	now := common.GetNowTime()

	this.lock.RLock()
	defer this.lock.RUnlock()

	for _, v := range this.list {
		if v.Enabled != 1 {
			continue
		}

		begin := common.ParseTime(v.BeginTime)
		// log.Debug("announce.getJson v=%+v now=%v begin=%v", v, now, begin)
		if now.Before(begin) {
			continue
		}

		end := common.ParseTime(v.EndTime)
		// log.Debug("announce.getJson v=%+v now=%v end=%v", v, now, end)
		if now.After(end) {
			continue
		}

		// log.Debug("announce.getJson v=%+v ", v, now, end)
		list = append(list, v)
	}
	d, _ := json.Marshal(list)
	return string(d)
}
