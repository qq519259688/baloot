package announce

type info struct {
	Rid        int    // 标识
	Title_En   string // 英文标题
	Content_En string // 英文内容
	Title_Eg   string // 埃及标题
	Content_Eg string // 埃及内容
	Priority   int    // 优先级，5个等级,5优先级最高,1优先级最低
	Version    int    // 版本号
	BeginTime  string // 开始时间
	EndTime    string // 截止时间
	Enabled    int    // 是否启用
}
