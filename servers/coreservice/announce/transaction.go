package announce

import (
	"encoding/json"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 获取公告列表
func getList() []*info {
	var list []*info
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Announce_GetList")
	sqlString := statement.GenSql()
	jsonData := dbengine.CenterDB.ExecSqlJson(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
		log.Error("announce.transaction load json unmarshal err %v", err)
	}
	return list
}
