package announce

import (
	"bet24.com/log"
)

var mgr *announceMgr

func Run() {
	log.Debug("announce running")
	mgr = newAnnounceMgr()
}

// 获取列表
func GetJson() string {
	return mgr.getJson()
}

// 刷新数据
func RefreshData() {
	mgr.load()
}
