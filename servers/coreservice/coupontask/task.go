package coupontask

import (
	"bet24.com/log"
)

var mgr *taskmgr

func Run() {
	mgr = newTaskMgr()
	log.Debug("couponTask running")
}

// 信息及完成列表
func GetUserTask(userId int) (bool, *BaseInfo, []*UserTask) {
	return mgr.getUserTask(userId)
}

// 修改基础上限（通过收徒）
func UpdateBaseLimit(userId, baseLimit int) {
	mgr.updateBaseLimit(userId, baseLimit)
}

// 修改临时上限
func UpdateTmpLimit(userId int) int {
	return mgr.updateTmpLimit(userId)
}

// 触发任务
func Trigger(userId, gameId, baseScore, players, doubleType int) {
	mgr.trigger(userId, gameId, baseScore, players, doubleType)
}

// 领取奖励
func Award(userId, userTaskId int) *award_resp {
	return mgr.award(userId, userTaskId)
}

func AddUser(userId int) {
	mgr.onUserEnter(userId)
}

func RemoveUser(userId int) {
	mgr.onUserExit(userId)
}
