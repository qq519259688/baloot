package coupontask

const (
	status_active          = iota // 进行中
	status_complete               // 完成,可领取
	status_awardedMultiple        // 已领取多倍

	TeacherAdd   = 50     // 师徒绑定加成
	PlayMultiple = 1      // 播放赠送红包券数
	ItemID       = 620100 // 红包券ID
)

// 播放配置
type videoConfig struct {
	PlayTimes   int // 播放次数
	CouponLimit int // 券上限
}

// 任务
type task struct {
	TaskId     int // 任务ID
	GameID     int // 游戏ID
	BaseScore  int // 底分
	Players    int // 玩家人数
	DoubleType int // 加倍类型(0=普通场 1=加倍场  10=超级加倍普通场  11=超级加倍加倍场)
	Coupons    int // 券数量
}

// 用户任务
type UserTask struct {
	UserTaskId  int    // 用户任务ID
	GameId      int    // 游戏ID
	ChineseName string // 游戏名称
	BaseScore   int    // 底分
	GameCount   int    // 游戏局数(1~3局)
	Coupons     int    // 数量
	GiftStatus  int    // 状态(0=进行中  1=已领取  2=已领取多倍)
	Crdate      string // 时间
}

// 基础信息
type BaseInfo struct {
	TodayCount  int // 今天券数量
	BaseLimit   int // 基础上限(永久)
	TmpAdd      int // 临时加成
	MaxAddTimes int // 最大加成次数
	PlayTimes   int // 观看次数
	updateTime  int // 时间戳
	isFacebook  int // 是否Facebook账号
}

// 奖励信息
type (
	award_req struct {
		UserTaskId int // 用户任务ID
		UserId     int // 用户ID
		Status     int // 状态(1=领取  2=领取多倍)
	}
	award_resp struct {
		RetCode    int // 操作结果
		UserTaskId int // 用户任务ID
		Coupons    int // 券数量
		TodayCount int // 今天券数量
	}
)
