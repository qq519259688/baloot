package coupontask

import (
	"encoding/json"
	"sort"
	"sync"
	"time"

	notification "bet24.com/servers/micros/notification/proto"
)

type taskmgr struct {
	lock          *sync.RWMutex
	enabled       bool // 是否启用
	videoConfigs  []*videoConfig
	sys_tasklist  []*task
	user_tasklist map[int]*user_task
}

func newTaskMgr() *taskmgr {
	ret := new(taskmgr)
	ret.lock = &sync.RWMutex{}
	ret.user_tasklist = make(map[int]*user_task)
	ret.loadVideoConfig()
	ret.loadSysTask()
	go ret.checkSwitchRefresh()
	return ret
}

func (this *taskmgr) loadVideoConfig() {
	list := getVideoConfig()
	sort.SliceStable(list, func(i, j int) bool {
		return list[i].PlayTimes < list[j].PlayTimes
	})
	this.lock.Lock()
	defer this.lock.Unlock()
	this.videoConfigs = list
}

func (this *taskmgr) loadSysTask() {
	list := getSysList()
	this.lock.Lock()
	defer this.lock.Unlock()
	this.sys_tasklist = list
}

func (this *taskmgr) getVideoConfigs(playTimes int) *videoConfig {
	this.lock.RLock()
	defer this.lock.RUnlock()
	for _, v := range this.videoConfigs {
		if playTimes+1 == v.PlayTimes {
			return v
		}
	}
	return &videoConfig{}
}

func (this *taskmgr) getVideoMaxTimes() int {
	this.lock.RLock()
	defer this.lock.RUnlock()
	for i := len(this.videoConfigs) - 1; i >= 0; i-- {
		return this.videoConfigs[i].PlayTimes
	}
	return 0
}

func (this *taskmgr) getTask(gameId, baseScore, players, doubleType int) *task {
	this.lock.RLock()
	defer this.lock.RUnlock()
	for _, v := range this.sys_tasklist {
		if v.GameID == gameId && v.BaseScore == baseScore && v.Players == players && v.DoubleType == doubleType {
			return v
		}
	}
	return nil
}

// 用户任务
func (this *taskmgr) getUser(userId int) *user_task {
	// 判断是否开启
	if !this.getSwitch() {
		return nil
	}

	this.lock.RLock()
	u, ok := this.user_tasklist[userId]
	this.lock.RUnlock()
	if ok {
		u.checkCrossDay()
		return u
	}

	u = newUserTask(userId)
	this.lock.Lock()
	this.user_tasklist[userId] = u
	this.lock.Unlock()
	return u
}

// 信息及完成列表
func (this *taskmgr) getUserTask(userId int) (bool, *BaseInfo, []*UserTask) {
	u := this.getUser(userId)
	if u == nil {
		return false, nil, nil
	}

	// 获取开关
	enabled := this.getSwitch()

	// 获取用户信息及列表
	info, list := u.getUserTask()

	return enabled, info, list
}

// 修改基础上限（通过收徒）
func (this *taskmgr) updateBaseLimit(userId, baseLimit int) {
	u := this.getUser(userId)
	if u == nil {
		return
	}
	u.updateBaseLimit(baseLimit)
}

// 修改临时上限
func (this *taskmgr) updateTmpLimit(userId int) int {
	u := this.getUser(userId)
	if u == nil {
		return 0
	}
	return u.updateTmpLimit()
}

// 触发任务
func (this *taskmgr) trigger(userId, gameId, baseScore, players, doubleType int) {
	u := this.getUser(userId)
	if u == nil {
		return
	}

	u.trigger(gameId, baseScore, players, doubleType)
}

// 领取奖励
func (this *taskmgr) award(userId, userTaskId int) *award_resp {
	u := this.getUser(userId)
	if u == nil {
		return &award_resp{}
	}
	return u.award(userTaskId)
}

// 检查开关
func (this *taskmgr) checkSwitchRefresh() {
	this.checkSwitch()
	t := time.NewTicker(20 * time.Second)
	for {
		select {
		case <-t.C:
			this.checkSwitch()
		}
	}
}

func (this *taskmgr) checkSwitch() {
	enabled := getSwitchInfo()
	if this.enabled == enabled {
		return
	}

	this.lock.Lock()
	this.enabled = enabled
	this.lock.Unlock()

	this.lock.RLock()
	defer this.lock.RUnlock()

	// 组装协议
	d, _ := json.Marshal(notification.NotificationCouponTask{
		Action: 1,
		Data:   this.enabled,
	})

	// 通知客户端
	for u := range this.user_tasklist {
		go notification.AddNotification(u, notification.Notification_CouponTask, string(d))
	}
}

func (this *taskmgr) getSwitch() bool {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.enabled
}

func (this *taskmgr) onUserEnter(userId int) {
	this.lock.RLock()
	_, ok := this.user_tasklist[userId]
	this.lock.RUnlock()
	if ok {
		return
	}
	u := newUserTask(userId)
	this.lock.Lock()
	this.user_tasklist[userId] = u
	this.lock.Unlock()
}

func (this *taskmgr) onUserExit(userId int) {
	this.lock.Lock()
	defer this.lock.Unlock()
	delete(this.user_tasklist, userId)
}
