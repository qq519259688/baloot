package coupontask

import (
	"runtime/debug"

	"bet24.com/servers/coreservice/dbengine"

	"bet24.com/database"
	"bet24.com/log"
)

// 券广告播放配置
func getVideoConfig() []*videoConfig {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetOpenRecordSet(true)
	statement.SetNeedReturnValue(false)
	statement.SetProcName("WS_CouponTask_GetVideoConfig")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*videoConfig

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		list = append(list, &videoConfig{
			PlayTimes:   int((ret[0]).(int64)),
			CouponLimit: int((ret[1]).(int64)),
		})
	}

	return list
}

// 券系统任务
func getSysList() []*task {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetOpenRecordSet(true)
	statement.SetNeedReturnValue(false)
	statement.SetProcName("WS_CouponTask_GetSysList")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*task
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		list = append(list, &task{
			TaskId:     int((ret[0]).(int64)),
			GameID:     int((ret[1]).(int64)),
			BaseScore:  int((ret[2]).(int64)),
			Players:    int((ret[3]).(int64)),
			DoubleType: int((ret[4]).(int64)),
			Coupons:    int((ret[5]).(int64)),
		})
	}

	return list
}

// 用户任务
func getUserTaskList(userId int) []*UserTask {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetOpenRecordSet(true)
	statement.SetNeedReturnValue(false)
	statement.SetProcName("WS_CouponTask_GetTaskList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*UserTask
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		list = append(list, &UserTask{
			UserTaskId: int((ret[0]).(int64)),
			GameId:     int((ret[1]).(int64)),
			BaseScore:  int((ret[2]).(int64)),
			GameCount:  int((ret[3]).(int64)),
			Coupons:    int((ret[4]).(int64)),
			GiftStatus: int((ret[5]).(int64)),
			Crdate:     (ret[6]).(string),
		})
	}

	return list
}

// 用户信息
func getUserInfo(userId int) *BaseInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_CouponTask_GetUserInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return &BaseInfo{}
	}

	return &BaseInfo{
		TodayCount: int((retRows[0][0]).(int64)),
		BaseLimit:  int((retRows[0][1]).(int64)),
		TmpAdd:     int((retRows[0][2]).(int64)),
		PlayTimes:  int((retRows[0][3]).(int64)),
		updateTime: int((retRows[0][4]).(int64)),
		isFacebook: int((retRows[0][5]).(int64)),
	}
}

// 修改信息
func updateInfo(userId, todayCount, updateTime int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_CouponTask_UpdateInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@TodayCount", database.AdParamInput, database.AdInteger, 4, todayCount)
	statement.AddParamter("@UpdateTime", database.AdParamInput, database.AdInteger, 4, updateTime)
	sqlstring := statement.GenSql()
	dbengine.CenterDB.ExecSql(sqlstring)
}

// 修改上限
func updateLimit(userId, baseLimit, playTimes, tmpAdd int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_CouponTask_UpdateLimit")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@BaseLimit", database.AdParamInput, database.AdInteger, 4, baseLimit)
	statement.AddParamter("@PlayTimes", database.AdParamInput, database.AdInteger, 4, playTimes)
	statement.AddParamter("@TmpAdd", database.AdParamInput, database.AdInteger, 4, tmpAdd)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	dbengine.CenterDB.ExecSql(sqlstring)
}

// 跨天重置
func reset(userId int, info *BaseInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_CouponTask_Reset")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@TodayCount", database.AdParamInput, database.AdInteger, 4, info.TodayCount)
	statement.AddParamter("@TmpAdd", database.AdParamInput, database.AdInteger, 4, info.TmpAdd)
	statement.AddParamter("@PlayTimes", database.AdParamInput, database.AdInteger, 4, info.PlayTimes)
	statement.AddParamter("@UpdateTime", database.AdParamInput, database.AdInteger, 4, info.updateTime)
	sqlstring := statement.GenSql()
	dbengine.CenterDB.ExecSql(sqlstring)
}

// 新增任务
func insertTask(userId int, info *UserTask) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_CouponTask_InsertTask")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, info.GameId)
	statement.AddParamter("@BaseScore", database.AdParamInput, database.AdInteger, 4, info.BaseScore)
	statement.AddParamter("@GameCount", database.AdParamInput, database.AdInteger, 4, info.GameCount)
	statement.AddParamter("@Coupons", database.AdParamInput, database.AdInteger, 4, info.Coupons)
	statement.AddParamter("@GiftStatus", database.AdParamInput, database.AdInteger, 4, info.GiftStatus)
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}

	return int((retRows[0][0]).(int64))
}

// 更新任务
func updateTask(userId int, info *UserTask) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_CouponTask_UpdateTask")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@UserTaskID", database.AdParamInput, database.AdInteger, 4, info.UserTaskId)
	statement.AddParamter("@GameCount", database.AdParamInput, database.AdInteger, 4, info.GameCount)
	statement.AddParamter("@Coupons", database.AdParamInput, database.AdInteger, 4, info.Coupons)
	sqlstring := statement.GenSql()
	dbengine.CenterDB.ExecSql(sqlstring)
}

// 领取奖励
func awardTask(req *award_req) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_CouponTask_AwardTask")
	statement.AddParamter("@UserTaskID", database.AdParamInput, database.AdInteger, 4, req.UserTaskId)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, req.UserId)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, req.Status)

	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}

	return int((retRows[0][0]).(int64))
}

// 获取开关信息
func getSwitchInfo() bool {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_CouponTask_GetSwitchInfo")
	sqlstring := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return false
	}

	return int((retRows[0][0]).(int64)) == 1
}
