package coupontask

import (
	"encoding/json"
	"sort"
	"sync"

	"bet24.com/log"
	"bet24.com/servers/common"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	notification "bet24.com/servers/micros/notification/proto"
)

type user_task struct {
	lock   *sync.RWMutex
	userId int         // 用户ID
	Info   *BaseInfo   // 基础信息
	List   []*UserTask // 任务列表
}

func newUserTask(userId int) *user_task {
	ret := new(user_task)
	ret.lock = &sync.RWMutex{}
	ret.userId = userId
	ret.loadInfo()
	ret.loadList()
	return ret
}

func (this *user_task) loadInfo() {
	info := getUserInfo(this.userId)
	info.MaxAddTimes = mgr.getVideoMaxTimes()
	this.lock.Lock()
	defer this.lock.Unlock()
	this.Info = info
}

func (this *user_task) isFacebook() bool {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.Info.isFacebook == 1
}

func (this *user_task) loadList() {
	list := getUserTaskList(this.userId)
	this.lock.Lock()
	defer this.lock.Unlock()
	this.List = list
}

// 检查是否跨天
func (this *user_task) checkCrossDay() {
	// 检查是否同一天
	if common.IsSameDay(this.Info.updateTime, common.GetTimeStamp()) {
		return
	}

	this.lock.Lock()
	this.Info.TodayCount = 0
	this.Info.TmpAdd = 0
	this.Info.PlayTimes = 0
	this.Info.updateTime = common.GetTimeStamp()
	this.lock.Unlock()

	// 重置
	go reset(this.userId, this.Info)
}

// 信息及完成列表
func (this *user_task) getUserTask() (*BaseInfo, []*UserTask) {
	var (
		count int // 计数器
		list  []*UserTask
	)

	this.lock.RLock()
	defer this.lock.RUnlock()

	for i := len(this.List) - 1; i >= 0; i-- {
		if this.List[i].GiftStatus == status_active {
			continue
		}

		// 返还最近20条
		if count >= 20 {
			break
		}

		list = append(list, this.List[i])
		count++
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].UserTaskId > list[j].UserTaskId
	})
	return this.Info, list
}

// 修改信息
func (this *user_task) updateInfo(coupons int) {
	this.lock.Lock()
	this.Info.TodayCount += coupons
	this.lock.Unlock()

	// 更新到数据库
	go updateInfo(this.userId, this.Info.TodayCount, this.Info.updateTime)
}

// 修改基础上限
func (this *user_task) updateBaseLimit(baseLimit int) {
	this.lock.Lock()
	this.Info.BaseLimit += baseLimit
	this.lock.Unlock()

	// 更新到数据库
	go updateLimit(this.userId, this.Info.BaseLimit, this.Info.PlayTimes, this.Info.TmpAdd)
}

// 修改临时上限
func (this *user_task) updateTmpLimit() int {
	// 播放广告，获取临时加成
	cfg := mgr.getVideoConfigs(this.Info.PlayTimes)
	if cfg.CouponLimit <= 0 {
		return 0
	}

	this.lock.Lock()
	this.Info.PlayTimes++
	this.Info.TmpAdd += cfg.CouponLimit
	this.lock.Unlock()

	// 更新到数据库
	go updateLimit(this.userId, this.Info.BaseLimit, this.Info.PlayTimes, this.Info.TmpAdd)

	return cfg.CouponLimit
}

// 触发任务
func (this *user_task) trigger(gameId, baseScore, players, doubleType int) {
	// 是否Facebook账户
	if !this.isFacebook() {
		return
	}

	sysTask := mgr.getTask(gameId, baseScore, players, doubleType)
	if sysTask == nil {
		log.Debug("coupontask.usertask trigger userId=%d gameId=%d baseScore=%d players=%d doubleType=%d is nil",
			this.userId, gameId, baseScore, players, doubleType)
		return
	}

	var myTask *UserTask
	for _, v := range this.List {
		if v.GameId == sysTask.GameID && v.BaseScore == sysTask.BaseScore && v.GiftStatus == status_active {
			myTask = v
			break
		}
	}

	// 超过上限
	if this.Info.TodayCount > this.Info.BaseLimit+this.Info.TmpAdd {
		return
	}

	// 当前缓存没有
	if myTask == nil {
		myTask = &UserTask{
			UserTaskId: 0,
			GameId:     gameId,
			BaseScore:  baseScore,
			GameCount:  1,
			Coupons:    sysTask.Coupons,
			GiftStatus: status_active,
			Crdate:     common.GetNowTime().Format(common.Layout),
		}
		userTaskId := insertTask(this.userId, myTask)
		if userTaskId <= 0 {
			log.Error("coupontask.usertask trigger insert userId=%d userTaskId=%d myTask=%+v",
				this.userId, userTaskId, myTask)
			return
		}
		myTask.UserTaskId = userTaskId
		this.List = append(this.List, myTask)

		// 超过50条
		max := 50
		if count := len(this.List); count > max {
			this.List = append(this.List[:count-max], this.List[count-max+1:]...)
		}
	} else { // 更新数据
		myTask.Coupons += sysTask.Coupons
		myTask.GameCount++
		if myTask.GameCount >= 3 {
			myTask.GiftStatus = status_complete

			// 直接给奖励
			retCode := awardTask(&award_req{
				UserTaskId: myTask.UserTaskId,
				UserId:     this.userId,
				Status:     myTask.GiftStatus,
			})

			if retCode == 1 {

				// 更新个人信息
				this.updateInfo(myTask.Coupons)

				var items []item.ItemPack
				items = append(items, item.ItemPack{
					ItemId: ItemID,
					Count:  myTask.Coupons,
				})

				go inventory.AddItems(this.userId, items, "红包券任务多倍", common.LOGTYPE_TASK_COUPON_MULTIPLE)
			}
		}
		go updateTask(this.userId, myTask)
	}

	// 道具， 通知客户端
	d, _ := json.Marshal(notification.NotificationCouponTask{
		Action:     2,
		TodayCount: this.Info.TodayCount,
		Data:       myTask,
	})
	go notification.AddNotification(this.userId, notification.Notification_CouponTask, string(d))

	return
}

// 领取多倍奖励
func (this *user_task) award(userTaskId int) *award_resp {
	var myTask *UserTask
	for _, v := range this.List {
		if v.UserTaskId == userTaskId {
			myTask = v
			break
		}
	}

	resp := &award_resp{}

	// 没有找到任务
	if myTask == nil {
		resp.RetCode = 11
		return resp
	}

	// 已经领取过
	if myTask.GiftStatus == status_awardedMultiple {
		resp.RetCode = 12
		return resp
	}

	// 已经领取过
	if myTask.GiftStatus != status_complete {
		resp.RetCode = 13
		return resp
	}

	myTask.GiftStatus = status_awardedMultiple

	retCode := awardTask(&award_req{
		UserTaskId: myTask.UserTaskId,
		UserId:     this.userId,
		Status:     myTask.GiftStatus,
	})

	if retCode == 1 {
		resp.RetCode = 1
		resp.Coupons += myTask.Coupons * PlayMultiple

		// 更新个人信息
		this.updateInfo(resp.Coupons)

		resp.TodayCount = this.Info.TodayCount

		var items []item.ItemPack
		items = append(items, item.ItemPack{
			ItemId: ItemID,
			Count:  resp.Coupons,
		})
		inventory.AddItems(this.userId, items, "红包券任务", common.LOGTYPE_TASK_COUPON)
	}

	return resp
}
