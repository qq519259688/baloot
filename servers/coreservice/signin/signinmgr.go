package signin

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
	"encoding/json"
	"strconv"
	"sync"
)

type signinmgr struct {
	lock          *sync.RWMutex
	signSequence  []Signin
	continueAward []*ContinueAward
	userlist      map[int]*userSignin
}

func newSigninManager() *signinmgr {
	sm := new(signinmgr)
	sm.lock = &sync.RWMutex{}
	sm.userlist = make(map[int]*userSignin)
	sm.loadSigninInfo()
	log.Debug("signin manager running")
	return sm
}

func (sm *signinmgr) loadSigninInfo() {
	//获取系统签到数值
	signlist := getSignList()
	if len(signlist) <= 0 {
		return
	}

	//获取系统连续奖励数值
	awardlist := getContinueAwardList()

	sm.lock.Lock()
	defer sm.lock.Unlock()
	sm.signSequence = signlist
	sm.continueAward = awardlist
}

func (sm *signinmgr) getSinInfo(id int) *Signin {
	sm.lock.RLock()
	defer sm.lock.RUnlock()
	for _, v := range sm.signSequence {
		if id == v.Id {
			return &v
		}
	}
	return nil
}

func (sm *signinmgr) getSignList() []Signin {
	sm.lock.RLock()
	defer sm.lock.RUnlock()
	ret := make([]Signin, len(sm.signSequence))
	copy(ret, sm.signSequence)
	return ret
}

func (sm *signinmgr) getContinueAwardInfo(day int) *ContinueAward {
	sm.lock.RLock()
	defer sm.lock.RUnlock()
	for _, v := range sm.continueAward {
		if v.Day == day {
			return v
		}
	}
	return nil
}

func (sm *signinmgr) getContinueAwardList() []*ContinueAward {
	sm.lock.RLock()
	defer sm.lock.RUnlock()
	return sm.continueAward
}

// 获取或创建用户
func (sm *signinmgr) getUser(userId int) *userSignin {
	sm.lock.RLock()
	us, ok := sm.userlist[userId]
	sm.lock.RUnlock()
	if !ok {
		// 用户不存在
		us = newUserSignin(userId)
		sm.lock.Lock()
		sm.userlist[userId] = us
		sm.lock.Unlock()
	}
	return us
}

func (sm *signinmgr) removeUser(userId int) {
	sm.lock.Lock()
	defer sm.lock.Unlock()
	delete(sm.userlist, userId)
}

func (sm *signinmgr) getUserSigninInfo(userId int) *SigninInfo {
	user := sm.getUser(userId)
	return user.getSigninInfo()
}

func (sm *signinmgr) checkSignTip(userId int) bool {
	user := sm.getUser(userId)
	return user.checkSignTip()
}

func (sm *signinmgr) doSignin(userId int) (bool, []item.ItemPack) {
	user := sm.getUser(userId)
	return user.signin()
}

// 领取连续签到奖励
func (sm *signinmgr) giftContinueAward(userId, day int) (bool, []item.ItemPack) {
	user := sm.getUser(userId)
	return user.giftContinueAward(day)
}

func (sm *signinmgr) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("signinmgr.dumpSys %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	d, _ := json.Marshal(sm.signSequence)
	log.Release(string(d))
	log.Release(" -------- ")
	d, _ = json.Marshal(sm.continueAward)
	log.Release(string(d))
}

func (sm *signinmgr) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("signinmgr.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}
	si := sm.getUserSigninInfo(userId)
	if si == nil {
		log.Release("user %d not exist", userId)
		return
	}
	d, _ := json.Marshal(si)
	log.Release(string(d))
}
