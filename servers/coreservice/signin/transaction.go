package signin

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 系统签到数值列表
func getSignList() []Signin {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction sign.getSignList err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Sign_GetList")
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	var signs []Signin
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out Signin

		out.Id = int((ret[0]).(int64))
		award := (ret[1]).(string)
		out.DoubleVipLevel = int((ret[2]).(int64))

		if award != "" {
			if err := json.Unmarshal([]byte(award), &out.Award); err != nil {
				log.Error("sign.getSignList unmarshal err %v ==>Id=%d award=%s", err, out.Id, award)
				return nil
			}
		}

		signs = append(signs, out)
	}

	return signs
}

// 系统连续签到数值列表
func getContinueAwardList() []*ContinueAward {
	defer func() {
		if err := recover(); err != nil {
			log.Error("sign.getContinueAwardList transaction err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Sign_GetContinueList")
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	var awards []*ContinueAward
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out ContinueAward

		out.Day = int((ret[0]).(int64))
		award := (ret[1]).(string)

		if award != "" {
			if err := json.Unmarshal([]byte(award), &out.Award); err != nil {
				log.Error("sign.getContinueAwardList unmarshal err %v", err)
				return nil
			}
		}
		awards = append(awards, &out)
	}

	return awards
}

// 用户签到列表
func userSignList(userId int) []*Signin {
	defer func() {
		if err := recover(); err != nil {
			log.Error("sign.userSignList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserSign_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	var signs []*Signin
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out Signin

		out.Id = int((ret[0]).(int64))
		out.SignTime = int((ret[1]).(int64))
		if out.SignTime < 0 {
			out.SignTime = 0
		}

		signs = append(signs, &out)
	}

	return signs
}

// 更新用户签到信息
func updateUserSign(userId, signId, signTime int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("sign.updateUserSign transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserSign_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SignID", database.AdParamInput, database.AdInteger, 4, signId)
	statement.AddParamter("@SignTime", database.AdParamInput, database.AdInteger, 4, signTime)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	dbengine.CenterDB.ExecSql(sqlstring)
}

// 用户连续签到信息
func userContinueSignList(userId int) []*ContinueAward {
	defer func() {
		if err := recover(); err != nil {
			log.Error("sign.userContinueSignList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserSign_GetContinueList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := dbengine.CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)

	var continues []*ContinueAward
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out ContinueAward

		out.Day = int((ret[0]).(int64))
		out.Status = int((ret[1]).(int64))

		continues = append(continues, &out)
	}

	return continues
}

// 更新用户连续签到信息
func updateUserSignContinue(userId, day, status int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("sign.updateUserSignContinue transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserSign_UpdateContinue")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Day", database.AdParamInput, database.AdInteger, 4, day)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, status)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	dbengine.CenterDB.ExecSql(sqlstring)
}
