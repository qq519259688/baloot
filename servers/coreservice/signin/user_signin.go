package signin

import (
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/common"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	vip "bet24.com/servers/micros/userservices/proto"
)

type userSignin struct {
	userId        int
	signinList    []Signin
	continueAward []*ContinueAward
}

func newUserSignin(userId int) *userSignin {
	us := new(userSignin)
	us.userId = userId
	us.loadSigninInfo()
	return us
}

func (us *userSignin) loadSigninInfo() {
	// 从数据库读取签到信息

	//1、签到列表
	signlist := userSignList(us.userId)
	for _, v := range mgr.getSignList() {
		signTime := 0
		for _, s := range signlist {
			if v.Id == s.Id {
				signTime = s.SignTime
				break
			}
		}
		us.signinList = append(us.signinList, Signin{
			Id:             v.Id,
			Award:          v.Award,
			DoubleVipLevel: v.DoubleVipLevel,
			SignTime:       signTime,
		})
	}

	//2、连续签到奖励
	awardlist := userContinueSignList(us.userId)
	for _, v := range mgr.getContinueAwardList() {
		status := 0
		for _, s := range awardlist {
			if v.Day == s.Day {
				status = s.Status
				break
			}
		}
		us.continueAward = append(us.continueAward, &ContinueAward{
			Day:    v.Day,
			Award:  v.Award,
			Status: status,
		})
	}

	us.checkFinishSequence()
}

// 检查是否重新开启新一轮签到
func (us *userSignin) checkFinishSequence() {
	count := len(us.signinList)
	if count == 0 {
		return
	}
	if us.signinList[count-1].SignTime == 0 {
		return
	}
	lastSignTime := us.signinList[count-1].SignTime
	now := common.GetTimeStamp()
	if common.IsSameDay(lastSignTime, now) {
		return
	}
	// 一轮结束，连续签到也清零
	us.signinList = mgr.getSignList()
	us.continueAward = mgr.getContinueAwardList()
	go us.updateSigninAll()
	go us.updateContinueAwardAll()
}

// 签到
func (us *userSignin) signin() (bool, []item.ItemPack) {
	var items []item.ItemPack
	if len(us.signinList) == 0 {
		return false, items
	}

	signin := us.getCurrentSignin()
	if signin == nil {
		return false, items
	}
	signin.SignTime = common.GetTimeStamp()
	us.checkContinue()
	us.sendAward(signin.Award, fmt.Sprintf("签到ID:[%d]", signin.Id))
	items = append(items, signin.Award...)
	vipLevel := 0

	userVip := vip.GetUserVipInfo(us.userId)
	if userVip != nil && userVip.IsVip() {
		vipLevel = userVip.Level
	}

	if vipLevel >= signin.DoubleVipLevel && signin.DoubleVipLevel > 0 {
		us.sendAward(signin.Award, fmt.Sprintf("签到ID:[%d] vip doubled", signin.Id))
		items = append(items, signin.Award...)
	}
	us.updateSignin(signin.Id, signin.SignTime)
	return true, items
}

// 领取连续签到奖励
func (us *userSignin) giftContinueAward(day int) (bool, []item.ItemPack) {
	var items []item.ItemPack
	if len(us.continueAward) <= 0 {
		return false, items
	}

	for _, v := range us.continueAward {
		if v.Day != day {
			continue
		}

		if v.Status != status_completed {
			return false, nil
		}

		// 设置为领取状态
		v.Status = status_received
		us.updateContinueAward(v.Day, v.Status)

		// 发放奖励
		us.sendAward(v.Award, fmt.Sprintf("连续签到[%d]天", v.Day))
		return true, v.Award
	}

	return false, nil
}

func (us *userSignin) calContinueDay() int {
	ret := 0
	count := len(us.signinList)
	lastSignTime := 0
	for i := count - 1; i >= 0; i-- {
		if us.signinList[i].SignTime == 0 {
			continue
		}
		if lastSignTime == 0 {
			lastSignTime = us.signinList[i].SignTime
			ret = 1
			continue
		}
		if !common.IsContinueDay(lastSignTime, us.signinList[i].SignTime) {
			return ret
		}
		ret++
	}
	return ret
}

func (us *userSignin) getLastSignTime() int {
	count := len(us.signinList)
	for i := count - 1; i >= 0; i-- {
		if us.signinList[i].SignTime != 0 {
			return us.signinList[i].SignTime
		}
	}
	return 0
}

func (us *userSignin) getCurrentSignin() *Signin {
	count := len(us.signinList)
	lastSignTime := 0
	lastSignIndex := -1
	for i := count - 1; i >= 0; i-- {
		if us.signinList[i].SignTime != 0 {
			lastSignIndex = i
			lastSignTime = us.signinList[i].SignTime
			break
		}
	}
	if lastSignIndex == count-1 {
		log.Debug("userSignin.getCurrentSignin [%d] sequence has done", us.userId)
		return nil
	}
	// 当日已签到？
	if lastSignTime > 0 {
		now := common.GetTimeStamp()
		if common.IsSameDay(lastSignTime, now) {
			log.Debug("userSignin.signin [%d] has signed today", us.userId)
			return nil
		}
	}

	return &us.signinList[lastSignIndex+1]
}

func (us *userSignin) getContinueDay() int {
	continueDay := 1
	count := len(us.signinList)
	if count == 0 {
		return continueDay
	}
	now := common.GetTimeStamp()
	lastSignTime := 0
	for i := count - 1; i > 0; i-- {
		if us.signinList[i].SignTime == 0 {
			continue
		}
		if lastSignTime == 0 {
			lastSignTime = us.signinList[i].SignTime
			// 是否当天或者第二天
			if !common.IsSameDay(lastSignTime, now) && !common.IsContinueDay(lastSignTime, now) {
				return 0
			}
		}
		if common.IsContinueDay(us.signinList[i].SignTime, us.signinList[i-1].SignTime) {
			continueDay++
		} else {
			break
		}
	}
	if lastSignTime == 0 {
		lastSignTime = us.signinList[0].SignTime
	}

	if lastSignTime == 0 {
		continueDay = 0
	}

	return continueDay
}

// 当日已签到，检查是否触发连续签到
func (us *userSignin) checkContinue() {
	if len(us.continueAward) == 0 {
		return
	}
	continueDay := us.getContinueDay()

	for _, v := range us.continueAward {
		if v.Status != status_idle {
			continue
		}

		if continueDay < v.Day {
			continue
		}

		//完成
		v.Status = status_completed
		us.updateContinueAward(v.Day, v.Status)
	}
	return
}

func (us *userSignin) sendAward(items []item.ItemPack, desc string) {
	inventory.AddItems(us.userId, items, desc, common.LOGTYPE_SEND_SIGN)
}

func (us *userSignin) updateSigninAll() {
	for _, v := range us.signinList {
		us.updateSignin(v.Id, v.SignTime)
	}
}

func (us *userSignin) updateSignin(id, signTime int) {
	updateUserSign(us.userId, id, signTime)
}

func (us *userSignin) updateContinueAwardAll() {
	for _, v := range us.continueAward {
		us.updateContinueAward(v.Day, status_idle)
	}
}

func (us *userSignin) updateContinueAward(day, status int) {
	updateUserSignContinue(us.userId, day, status)
}

func (us *userSignin) getSigninInfo() *SigninInfo {
	signin := us.getCurrentSignin()
	return &SigninInfo{
		SigninTable: us.signinList,
		Continue:    us.continueAward,
		Signable:    signin != nil,
		ContinueDay: us.getContinueDay(),
	}
}

func (us *userSignin) checkSignTip() bool {
	sign := us.getCurrentSignin()
	return sign != nil
}
