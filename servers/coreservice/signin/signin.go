package signin

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	status_idle      = iota //空闲，未完成
	status_completed        //完成，未领取
	status_received         //完成，已领取
)

type Signin struct {
	Id             int
	Award          []item.ItemPack
	DoubleVipLevel int // vip？加倍，0表示没有
	SignTime       int // 签到时间，0表示未签
}

type ContinueAward struct {
	Day    int // 连续多少天
	Award  []item.ItemPack
	Status int // 状态 0=空闲，未完成  1=完成，未领取  2=完成，已领取
}

var mgr *signinmgr

func Run() {
	mgr = newSigninManager()
}

type SigninInfo struct {
	SigninTable []Signin
	Continue    []*ContinueAward
	Signable    bool
	ContinueDay int // 已连续签到多少天
}

func GetUserSigninInfo(userId int) *SigninInfo {
	return mgr.getUserSigninInfo(userId)
}

func CheckSignTip(userId int) bool {
	return mgr.checkSignTip(userId)
}

func DoSignin(userId int) (bool, []item.ItemPack) {
	return mgr.doSignin(userId)
}

// 领取连续签到奖励
func GiftContinueAward(userId, day int) (bool, []item.ItemPack) {
	return mgr.giftContinueAward(userId, day)
}

func AddUser(userId int) {
	mgr.getUser(userId)
}

func RemoveUser(userId int) {
	mgr.removeUser(userId)
}

func Dump(param1, param2 string) {
	switch param1 {
	case "sys":
		mgr.dumpSys(param2)
	case "user":
		mgr.dumpUser(param2)
	default:
		log.Debug("signin.Dump unhandled %s:%s", param1, param2)
	}
}
