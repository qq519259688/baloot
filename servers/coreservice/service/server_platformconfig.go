package service

// 考虑到未更新的服务器还会调用此接口
import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
	"context"
	"encoding/json"
	"errors"
)

func (s *Server) GetPlatformConfig(ctx context.Context, args *client.Request, reply *client.Reply) error {
	reply.Resp.Data = platformconfig.GetConfig(args.Data)
	return nil
}

func (s *Server) GetPlatformConfigList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	ret := platformconfig.GetConfigList()
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) SetPlatformConfig(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.PlatformConfigSet_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SetPlatformConfig unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	platformconfig.SetConfig(req.Key, req.Value)
	return nil
}
