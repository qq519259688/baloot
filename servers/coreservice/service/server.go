package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	config "bet24.com/servers/coreservice/serviceconfig"
	"context"
	"encoding/json"
	"errors"
	"fmt"
)

var Addr string
var mgr *manager

func Run() {
	mgr = newManager()
	Addr = fmt.Sprintf(":%d", config.Server.ServerPort)
}

type Server int

func (s *Server) SayHello(ctx context.Context, req *client.Request, rsp *client.Response) error {
	rsp.Data = fmt.Sprintf("Hello from coreservice")
	return nil
}

func (s *Server) OnUserEnter(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.OnUserEnter unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	reply.Resp = mgr.onUserEnter(req.UserId, req.IpAddress)
	return nil
}

func (s *Server) OnUserExit(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.OnUserExit unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	reply.Resp = mgr.onUserExit(req.UserId)
	return nil
}

func (s *Server) SyncUserList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var userList []int
	if err := json.Unmarshal([]byte(args.Data), &userList); err != nil {
		log.Debug("Server.SyncUserList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	mgr.syncUserList(userList)
	return nil
}
