package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/subsidy"
	"context"
	"encoding/json"
	"errors"
)

// 获取补助信息
func (s *Server) SubsidyGetInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Subsidy_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SubsidyGetInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	info := subsidy.GetSubsidyInfo(req.UserId, req.LowerAmount, req.MaxTimes)
	if info != nil {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(info)
		reply.Resp.Data = string(d)
	}
	return nil
}

// 领取补助
func (s *Server) SubsidyGift(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Subsidy_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SubsidyGift unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	ret, msg := subsidy.GiftSubsidy(req.UserId, req.LowerAmount, req.MaxTimes, req.CoolSeconds)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(struct {
		RetCode  int
		ErrorMsg string
	}{
		RetCode:  ret,
		ErrorMsg: msg,
	})
	reply.Resp.Data = string(d)
	return nil
}

// 领取元宝补助
func (s *Server) SubsidyGiftChip(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Subsidy_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SubsidyGiftChip unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	info := struct {
		RetCode bool
		Amount  int
	}{
		RetCode: false,
		Amount:  req.LowerAmount,
	}

	info.RetCode = subsidy.GiftChipSubsidy(req.UserId, req.LowerAmount, req.IpAddress)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(info)
	reply.Resp.Data = string(d)
	return nil
}

// 领取回归奖励
func (s *Server) GiftReturnAward(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GiftReturnAward unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	subsidy.GiftReturnAward(req.UserId)
	return nil
}
