package service

import (
	"bet24.com/public"
	userservices "bet24.com/servers/micros/userservices/proto"
	"context"
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/friend"
	"github.com/pkg/errors"
)

func (s *Server) GetSearchUserinfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Friend_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetSearchUserinfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.RetCode = 1
	ret := friend.GetSearchUserinfo(req.UserId, req.TargetUserID, req.TargetNickName)
	if ret != nil {
		d, _ := json.Marshal(ret[0])
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) GetSearchUserList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Friend_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetSearchUserList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.RetCode = 1
	ret := friend.GetSearchUserinfo(req.UserId, req.TargetUserID, req.TargetNickName)
	if ret != nil {
		d, _ := json.Marshal(ret)
		reply.Resp.Data = string(d)
	}
	return nil
}

// 获取好友列表
func (s *Server) GetFriendList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetFriendList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.RetCode = 1
	ret := friend.GetFriendList(req.UserId)
	if ret != nil {
		d, _ := json.Marshal(ret)
		reply.Resp.Data = string(d)
	}
	return nil
}

// 获取好友审核列表
func (s *Server) FriendVerifyList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendVerifyList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.RetCode = 1
	ret := friend.FriendVerifyList(req.UserId)
	if ret != nil {
		d, _ := json.Marshal(ret)
		reply.Resp.Data = string(d)
	}
	return nil
}

// 好友申请
func (s *Server) FriendApply(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Friend_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetFriendList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	ret := friend.FriendApply(req.UserId, req.TargetUserID)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)

	return nil
}

func (s *Server) DelFriend(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Friend_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetFriendList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	ret := friend.DelFriend(req.UserId, req.TargetUserID)
	reply.Resp.RetCode = 1
	if ret != nil {
		d, _ := json.Marshal(ret)
		reply.Resp.Data = string(d)
	}

	return nil
}

// 处理好友申请
func (s *Server) FriendHandleApply(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Friend_Apply_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetFriendList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.RetCode = 1
	ret := friend.FriendHandleApply(req.UserId, req.TargetUserID, req.Apply)
	if ret != nil {
		d, _ := json.Marshal(ret)
		reply.Resp.Data = string(d)
	}

	return nil
}

// 赠送礼物
func (s *Server) FriendGiveGift(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Friend_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetFriendList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	ret := friend.FriendGiveGift(req.UserId, req.TargetUserID)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)

	return nil
}

// 获取赠礼
func (s *Server) FriendGetGift(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Friend_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetFriendList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	ret := friend.FriendGetGift(req.UserId, req.TargetUserID, req.IpAddress)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)

	return nil
}

// 设置好友游戏状态
func (s *Server) FriendSetUserStatus(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Friend_Status_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendSetUserStatus unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	friend.SetUserStatus(req.UserId, req.IsOnline, req.ServerName)
	return nil
}

func (s *Server) FriendRoomInvite(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.FriendRoomInvite_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendRoomInvite unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	friend.RoomInvite(req.UserId, req.NickName, req.ToUserId, req.RoomNo)
	reply.Resp.RetCode = 1
	reply.Resp.Data = ""
	return nil
}

func (s *Server) FriendGetRoomInviteList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendGetRoomInviteList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret := friend.GetRoomInviteList(req.UserId)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) FriendRoomInviteInvalid(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.FriendRoomInvite_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendRoomInviteInvalid unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	friend.RoomInviteInvalid(req.UserId, req.RoomNo)
	reply.Resp.RetCode = 1
	reply.Resp.Data = ""
	return nil
}

// 获取好友上限
func (s *Server) GetMaxFriendCount(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetMaxFriendCount unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	ret := friend.GetMaxFriendCount(req.UserId)
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) FriendGetPotentialList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendGetPotentialList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	reply.Resp.Data = friend.GetPotentialList(req.UserId)
	return nil
}

func (s *Server) FriendAddPotential(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.FriendAddPotential_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendAddPotential unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	friend.AddPotentialFriend(req.UserId, req.ToUserId, req.Memo)
	return nil
}

// 黑名单列表
func (s *Server) FriendGetBlackList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.FriendBlack_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendGetBlackList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	blackList := friend.GetBlackList(req.UserId)

	// 分页处理
	totalCount := len(blackList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	type info struct {
		UserId int    // 黑名单用户id
		Crdate string // 时间

		// 以下变量数据库不存
		NickName    string
		FaceID      int
		FaceUrl     string
		Decorations []userservices.UserDecoration `json:",omitempty"`
	}

	var list []info

	for _, v := range blackList[start:end] {
		u := userservices.GetUserInfo(v.UserId)
		if u == nil {
			continue
		}

		list = append(list, info{
			UserId:      v.UserId,
			Crdate:      v.Crdate,
			NickName:    u.NickName,
			FaceID:      u.FaceId,
			FaceUrl:     u.FaceUrl,
			Decorations: u.Decorations,
		})
	}

	buf, _ := json.Marshal(struct {
		RecordCount int
		List        interface{}
	}{
		RecordCount: totalCount,
		List:        list,
	})
	reply.Resp.Data = string(buf)
	return nil
}

// 添加黑名单
func (s *Server) FriendAddBlack(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.FriendBlack_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendAddBlack unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	retCode, Message := friend.AddBlack(req.UserId, req.ToUserId)
	d, _ := json.Marshal(struct {
		RetCode int
		Message string
	}{
		RetCode: retCode,
		Message: Message,
	})
	reply.Resp.Data = string(d)
	return nil
}

// 删除黑名单
func (s *Server) FriendDelBlack(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.FriendBlack_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendDelBlack unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	retCode, message := friend.DelBlack(req.UserId, req.ToUserId)
	d, _ := json.Marshal(struct {
		RetCode int
		Message string
	}{
		RetCode: retCode,
		Message: message,
	})
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) FriendIsBlackListUser(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.FriendBlack_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendIsBlackListUser unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	if friend.IsBlackListUser(req.UserId, req.ToUserId) {
		reply.Resp.RetCode = 1
	}
	return nil
}

func (s *Server) FriendIsBlackListUserIn(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.FriendBlack_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendIsBlackListUserIn unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	if friend.IsBlackListUserIn(req.UserId, req.ToUserIds) {
		reply.Resp.RetCode = 1
	}
	return nil
}

func (s *Server) FriendGetBlackListUserIn(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.FriendBlack_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FriendGetBlackListUserIn unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	var ret []int
	for _, v := range req.ToUserIds {
		if friend.IsBlackListUser(req.UserId, v) {
			ret = append(ret, v)
		}
	}

	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}
