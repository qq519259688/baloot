package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/vitality"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

func (s *Server) GetUserVitalityInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetUserVitalityInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	info := vitality.GetUserInfo(req.UserId)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(struct {
		DayPoint  int
		WeekPoint int
	}{
		DayPoint:  info.DayPoint,
		WeekPoint: info.WeekPoint,
	})
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) AddVitalityPoint(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.AddVitalityPoint_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AddVitalityPoint unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret := vitality.AddPoint(req.UserId, req.Point)
	if ret == 1 {
		reply.Resp.RetCode = 1
	}
	return nil
}
