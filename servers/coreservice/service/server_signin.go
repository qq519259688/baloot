package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/signin"
	"context"
	"encoding/json"
	"errors"
)

func (s *Server) GetSigninInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetSigninInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	reply.Resp.RetCode = 1
	info := signin.GetUserSigninInfo(req.UserId)
	if info == nil {
		log.Debug("Server.GetSigninInfo no signin info")
		reply.Resp.RetCode = 0
	} else {
		d, _ := json.Marshal(info)
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) DoSignin(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.DoSigninIn unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	success, items := signin.DoSignin(req.UserId)
	if success {
		reply.Resp.RetCode = 1
		reply.Resp.Action = append(reply.Resp.Action, client.Action_Item)
		d, _ := json.Marshal(items)
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) GiftContinueAward(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.GiftContinueAward_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GiftContinueAward unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	success, items := signin.GiftContinueAward(req.UserId, req.Day)
	if success {
		reply.Resp.RetCode = 1
		reply.Resp.Action = append(reply.Resp.Action, client.Action_Item)
		d, _ := json.Marshal(items)
		reply.Resp.Data = string(d)
	}
	return nil
}
