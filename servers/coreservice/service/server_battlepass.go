package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/battlepass"
	"bet24.com/servers/coreservice/client"
	"context"
	"encoding/json"
	"errors"
)

// 成长礼包
func (s *Server) GetBattlePassPacks(ctx context.Context, args *client.Request, reply *client.Reply) error {
	packs := battlepass.GetBattlePassPacks()
	if len(packs) > 0 {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(packs)
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) GetUserBattlePass(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetUserBattlePass unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	terms := battlepass.GetUserBattlePass(req.UserId)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(terms)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) UserAwardBattlePass(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.GrowthPackAward_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.UserAwardBattlePass unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	var resp client.GrowthPackAward_resp
	resp.Succeeded, resp.ErrMsg = battlepass.UserAwardBattlePass(req.UserId, req.GrowthPackId, req.Index)
	if resp.Succeeded {
		reply.Resp.RetCode = 1
	}
	d, _ := json.Marshal(resp)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) AddUserBattlePassExp(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.BattlePassAddExp_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AddUserBattlePassExp unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	battlepass.AddUserExp(req.UserId, req.Exp)
	return nil
}
