package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/agent"
	"bet24.com/servers/coreservice/announce"
	"bet24.com/servers/coreservice/award"
	"bet24.com/servers/coreservice/bet"
	"bet24.com/servers/coreservice/card"
	"bet24.com/servers/coreservice/chat"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/coupontask"
	"bet24.com/servers/coreservice/exchange"
	"bet24.com/servers/coreservice/friend"
	"bet24.com/servers/coreservice/gamelist"
	"bet24.com/servers/coreservice/giftpack"
	"bet24.com/servers/coreservice/gold2chipwheel"
	"bet24.com/servers/coreservice/jackpot"
	"bet24.com/servers/coreservice/keyword"
	"bet24.com/servers/coreservice/limiteditems"
	"bet24.com/servers/coreservice/monthlycard"
	"bet24.com/servers/coreservice/newusergift"
	"bet24.com/servers/coreservice/prizewheel"
	"bet24.com/servers/coreservice/purchase"
	"bet24.com/servers/coreservice/qqwry"
	"bet24.com/servers/coreservice/rank"
	"bet24.com/servers/coreservice/redpoint"
	"bet24.com/servers/coreservice/shop"
	"bet24.com/servers/coreservice/signin"
	"bet24.com/servers/coreservice/signinwheel"
	"bet24.com/servers/coreservice/slotscore"
	"bet24.com/servers/coreservice/spread"
	"bet24.com/servers/coreservice/subsidy"
	"bet24.com/servers/coreservice/teacher"
	"bet24.com/servers/coreservice/track"
	"bet24.com/servers/coreservice/userwin"
	"bet24.com/servers/coreservice/video"
	"bet24.com/servers/coreservice/vitality"
	activityservice "bet24.com/servers/micros/activityservice/proto"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	notification "bet24.com/servers/micros/notification/proto"
	task "bet24.com/servers/micros/task/proto"
	robot "bet24.com/servers/micros/userservices/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
	vipservice "bet24.com/servers/micros/userservices/proto"
)

type manager struct {
}

func newManager() *manager {
	qqwry.Run()
	rank.Run()
	monthlycard.Run()
	giftpack.Run()
	shop.Run()
	prizewheel.Run()
	subsidy.Run()
	vitality.Run()
	redpoint.Run()
	exchange.Run()
	bet.Run()
	chat.Run()
	friend.Run()
	jackpot.Run()
	signinwheel.Run()
	track.Run()
	newusergift.Run()
	spread.Run()     // 推广
	video.Run()      // 视频
	teacher.Run()    // 师徒
	coupontask.Run() // 红包券任务
	slotscore.Run()
	gamelist.Run()
	signin.Run()

	agent.Run() // 代理体系
	gold2chipwheel.Run()
	purchase.Run() // 100K购
	award.Run()    // 奖励模块
	limiteditems.Run()
	keyword.Run()
	//battlepass.Run(inventory.AddItems)

	announce.Run() // 公告
	card.Run()

	userwin.Run()

	log.Release("coreservice running")
	return mgr
}

func (m *manager) onUserEnter(userId int, ipAddress string) client.Response {
	var ret client.Response
	ret.RetCode = 1
	if robot.IsRobot(userId) {
		return ret
	}

	log.Debug("manager.onUserEnter %d", userId)
	notification.AddUser(userId)

	friend.AddUser(userId)
	inventory.AddUser(userId, ipAddress)
	signin.AddUser(userId)
	task.AddUser(userId)
	userservices.AddUser(userId)
	vitality.AddUser(userId)
	monthlycard.AddUser(userId)
	giftpack.AddUser(userId)
	prizewheel.AddUser(userId)
	video.AddUser(userId)
	coupontask.AddUser(userId)
	redpoint.AddUser(userId) // 放到最后
	chat.OnUserEnter(userId)
	userwin.AddUser(userId)
	vipservice.AddUser(userId) // 新版vip
	activityservice.AddUser(userId)
	return ret
}

func (m *manager) onUserExit(userId int) client.Response {
	var ret client.Response
	ret.RetCode = 1
	if robot.IsRobot(userId) {
		return ret
	}

	log.Debug("manager.onUserExit %d", userId)

	notification.RemoveUser(userId)
	redpoint.RemoveUser(userId)
	inventory.RemoveUser(userId)
	signin.RemoveUser(userId)
	task.RemoveUser(userId)
	userservices.RemoveUser(userId)

	vitality.RemoveUser(userId)
	monthlycard.RemoveUser(userId)
	giftpack.RemoveUser(userId)
	friend.RemoveUser(userId)
	prizewheel.RemoveUser(userId)

	video.RemoveUser(userId)
	coupontask.RemoveUser(userId)
	userwin.RemoveUser(userId)
	chat.OnUserExit(userId)

	vipservice.RemoveUser(userId) // 新版vip
	activityservice.RemoveUser(userId)
	return ret
}

func (m *manager) syncUserList(userlist []int) {

}
