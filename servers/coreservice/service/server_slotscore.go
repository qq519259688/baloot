package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/slotscore"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

//金币日志
func (s *Server) AddSlotScore(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.SlotScore_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AddSlotScore unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	reply.Resp.RetCode = slotscore.AddScore(req.UserId, req.Param)
	reply.Resp.Data = ""
	return nil
}

func (s *Server) GetSlotScore(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetSlotScore unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	reply.Resp.RetCode = slotscore.GetScore(req.UserId)
	reply.Resp.Data = ""
	return nil
}

func (s *Server) GetSlotScoreExchangeList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	reply.Resp.RetCode = 1
	reply.Resp.Data = slotscore.GetExchangeList()
	return nil
}

func (s *Server) GetSlotScoreExchangeHistory(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetSlotScoreExchangeHistory unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	reply.Resp.RetCode = 1
	reply.Resp.Data = slotscore.GetExchangeHistory(req.UserId)
	return nil
}

func (s *Server) SlotScoreExchange(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.SlotScore_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SlotScoreExchange unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret := slotscore.Exchange(req.UserId, req.Param)
	if ret {
		reply.Resp.RetCode = 1
	} else {
		reply.Resp.RetCode = 0
	}

	reply.Resp.Data = ""
	return nil
}
