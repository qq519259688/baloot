package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/chat"
	"bet24.com/servers/coreservice/client"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

func (s *Server) SendChatMsg(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Chat_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SendChatMsg unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	ret := chat.SendChatMsg(req.ChannelID, req.SendUserID, req.FaceId, req.Sex, req.Vip, req.RecvUserID,
		req.NickName, req.FaceUrl, req.ChatMsg, req.IpAddress, req.MsgType)
	if ret {
		reply.Resp.RetCode = 1
	}
	return nil
}

func (s *Server) GetChatMsg(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Chat_Msg_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetChatMsg unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.Data = chat.GetChatMsg(req.UserId, req.ChannelID)
	return nil
}

func (s *Server) SendChannelChat(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.ChannelChat_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SendChannelChat unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.Data = chat.SendChannelChat(req.From, req.To, req.Message, req.MsgType)
	return nil
}

func (s *Server) SendGiftMessage(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.ChannelChat_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SendGiftMessage unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.Data = chat.SendGiftMessage(req.From, req.To, req.Message)
	return nil
}

func (s *Server) GetChannelChat(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.GetChannelChat_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetChannelChat unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.Data = chat.GetChannelChat(req.ChannelKey, req.UserId)
	return nil
}

func (s *Server) GetChannelInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.GetChannelChat_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetChannelInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.Data = chat.GetChannelInfo(req.ChannelKey)
	return nil
}

func (s *Server) ClearChannelHistory(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.GetChannelChat_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.ClearChannelHistory unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	chat.ClearChannelHistory(req.ChannelKey, req.UserId)
	return nil
}

func (s *Server) RemoveChannelChat(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.GetChannelChat_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.ClearChannelHistory unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	chat.RemoveChannel(req.UserId, req.UserId2)
	return nil
}

// 获取机器人聊天列表
func (s *Server) GetRobotChatList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	list := chat.GetRobotChatList()
	d, _ := json.Marshal(list)
	reply.Resp.Data = string(d)
	return nil
}

// 获取机器人聊天信息
func (s *Server) GetRobotChatInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetRobotChatInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	info := chat.GetRobotChatInfo(req.UserId)
	d, _ := json.Marshal(info)
	reply.Resp.Data = string(d)
	return nil
}

// 添加机器人聊天信息
func (s *Server) AddRobotChatInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.RobotChat_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AddRobotChatInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	info := chat.AddRobotChatInfo(req.UserId, req.Msg, req.Seconds, req.BeginTime, req.EndTime)
	d, _ := json.Marshal(info)
	reply.Resp.Data = string(d)
	return nil
}

// 修改机器人聊天信息
func (s *Server) UpdateRobotChatInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.RobotChat_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.UpdateRobotChatInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	info := chat.UpdateRobotChatInfo(req.UserId, req.Msg, req.Seconds, req.BeginTime, req.EndTime)
	d, _ := json.Marshal(info)
	reply.Resp.Data = string(d)
	return nil
}

// 删除机器人聊天信息
func (s *Server) DelRobotChatInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.DelRobotChatInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	info := chat.DelRobotChatInfo(req.UserId)
	d, _ := json.Marshal(info)
	reply.Resp.Data = string(d)
	return nil
}
