package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/redpoint"
	"context"
	"encoding/json"
	"errors"
)

func (s *Server) CheckRedPoint(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.CheckRedPoint unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	info := redpoint.CheckRedPoint(req.UserId)
	d, _ := json.Marshal(info)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}
