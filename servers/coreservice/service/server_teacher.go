package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/teacher"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

// 注册
func (s *Server) TeacherRegister(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.TeacherRegister unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	info := teacher.Register(req.UserId)
	d, _ := json.Marshal(info)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 师父信息
func (s *Server) TeacherInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.TeacherInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	info := teacher.Info(req.UserId)
	d, _ := json.Marshal(info)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 绑定
func (s *Server) BindTeacher(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Teacher_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.BindTeacher unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var respData struct {
		RetCode int
		Items   interface{}
	}
	respData.RetCode, respData.Items = teacher.Bind(req.UserId, req.TeacherId, req.IsSendMail)
	d, _ := json.Marshal(respData)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 徒弟列表
func (s *Server) Students(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Teacher_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.Students unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	list := teacher.Students(req.TeacherId)
	d, _ := json.Marshal(list)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 收益列表
func (s *Server) TeacherProfitList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.TeacherProfit_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.TeacherProfitList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var respData struct {
		RecordCount int
		List        interface{}
	}

	respData.RecordCount, respData.List = teacher.ProfitList(req.UserId, req.Days, req.PageIndex, req.PageSize)
	d, _ := json.Marshal(respData)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}
