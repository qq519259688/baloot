package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/jackpot"
	"context"
	"encoding/json"
	"errors"
)

func (s *Server) GetJackpotAmount(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.JackPot_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetJackpotAmount unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.RetCode = jackpot.GetJackpotAmount(req.GameId, req.IsChipRoom)
	return nil
}

func (s *Server) ModifyJackpot(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.JackPotModify_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.ModifyJackpot unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.RetCode = jackpot.ModifyJackpot(req.GameId, req.Amount, req.UserId, req.Desc, req.IsChipRoom)
	return nil
}
