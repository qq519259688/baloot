package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/award"
	"bet24.com/servers/coreservice/client"
	item "bet24.com/servers/micros/item_inventory/proto"
	"context"
	"encoding/json"
	"errors"
)

// 发送奖励
func (s *Server) SendAward(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Award_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SendAward unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var info struct {
		RetCode int
		Awards  []item.ItemPack
	}

	info.RetCode, info.Awards = award.SendAward(req.UserId, req.ActiveId, req.HallType, req.IpAddress)
	d, _ := json.Marshal(info)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}
