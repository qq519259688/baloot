package service

import (
	"context"
	"encoding/json"
	"errors"

	"bet24.com/log"
	"bet24.com/servers/coreservice/card"
	"bet24.com/servers/coreservice/client"
)

// 使用充值卡
func (s *Server) UseRechargeCard(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.ChargeCard_Req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.UseRechargeCard unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	resp := card.Use(req.UserId, req.CardNo)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(resp)
	reply.Resp.Data = string(d)
	return nil
}
