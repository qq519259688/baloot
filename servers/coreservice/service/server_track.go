package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/track"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

func (s *Server) TrackRecord(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.TrackRecord_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.TrackRecord unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	track.Record(req.UserId, req.Level_1, req.Level_2, req.Level_3)
	reply.Resp.RetCode = 1
	return nil
}
