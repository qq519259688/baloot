package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/broadcast"
	"bet24.com/servers/coreservice/client"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

// 发送广播
func (s *Server) SendBroadcast(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.SendBroadcast_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SendBroadcast unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	broadcast.SendBroadcast(req.UserID, req.GameID, req.Priority, req.Msg, req.Ext)
	reply.Resp.RetCode = 1
	return nil
}

func (s *Server) SendGameWinBroadcast(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.SendBroadcast_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SendGameWinBroadcast unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	broadcast.SendGameWinBroadcast(req.UserID, req.NickName, req.Score, req.GameID, req.GameName)
	reply.Resp.RetCode = 1
	return nil
}
