package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/prizewheel"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

func (s *Server) GetWheelPrizes(ctx context.Context, args *client.Request, reply *client.Reply) error {
	prizes := prizewheel.GetPrizes()
	if len(prizes) > 0 {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(prizes)
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) DoWheel(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.PrizeWheel_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.DoWheel unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	var result struct {
		Prizes []*prizewheel.Prize
		ErrMsg string
	}
	result.Prizes, result.ErrMsg = prizewheel.Wheel(req.UserId, req.Times, req.NickName, req.IpAddress)
	reply.Resp.RetCode = 1
	if len(result.Prizes) == 0 {
		reply.Resp.RetCode = 0
	}
	d, _ := json.Marshal(result)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) GetWheelTimes(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetWheelTimes unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var ret struct {
		Times      int //已抽奖次数
		PrizeTimes int //总抽奖次数
		PrizeCost  int //多少金币抽一次
	}
	ret.Times, ret.PrizeTimes, ret.PrizeCost = prizewheel.GetWheelTimes(req.UserId)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) GetPrizeRecord(ctx context.Context, args *client.Request, reply *client.Reply) error {
	record := prizewheel.GetRecord()
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(record)
	reply.Resp.Data = string(d)
	return nil
}
