package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/signinwheel"
	"context"
	"encoding/json"
	"errors"
)

func (s *Server) GetSigninWheelInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetSigninWheelInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	reply.Resp.RetCode = 1
	info := signinwheel.GetSigninWheelInfo(req.UserId)
	d, _ := json.Marshal(info)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) DoSigninWheel(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_SigninWheel
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.DoSigninWheel unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var ret struct {
		signinwheel.SigninWheelResult
		MyPlayTimes int
	}

	info, myPlayTimes := signinwheel.DoSignin(req.UserId, req.IpAddress, req.UserGold)
	ret.SigninWheelResult = info
	ret.MyPlayTimes = myPlayTimes
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) GetSigninWheelHistory(ctx context.Context, args *client.Request, reply *client.Reply) error {
	result := signinwheel.GetHistory()
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(result)
	reply.Resp.Data = string(d)
	return nil
}
