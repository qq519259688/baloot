package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/keyword"
	"context"
	"encoding/json"
	"errors"
)

func (s *Server) ParseKeyword(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.KeyWord_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.ParseKeyword unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	req.Out = keyword.ParseKeyword(req.In)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(req)
	reply.Resp.Data = string(d)
	return nil
}
