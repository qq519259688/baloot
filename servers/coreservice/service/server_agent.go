package service

import (
	"context"
	"encoding/json"
	"errors"

	"bet24.com/log"
	"bet24.com/servers/coreservice/agent"
	"bet24.com/servers/coreservice/client"
)

// 刷新配置
func (s *Server) AgentRefreshConfig(ctx context.Context, args *client.Request, reply *client.Reply) error {
	agent.RefreshConfig()
	reply.Resp.RetCode = 1
	reply.Resp.Data = ""
	return nil
}

// 获取配置信息
func (s *Server) AgentConfigs(ctx context.Context, args *client.Request, reply *client.Reply) error {
	info := agent.GetJsonConfigs()
	reply.Resp.RetCode = 1
	reply.Resp.Data = info
	return nil
}

// 代理申请
func (s *Server) AgentApply(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.AgentApply_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AgentApply unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	retCode := agent.Apply(req.UserId, req.Memo)
	d, _ := json.Marshal(retCode)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 代理绑码
func (s *Server) AgentBind(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.AgentBind_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AgentBind unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var respData struct {
		RetCode int
		Items   interface{}
	}
	respData.RetCode, respData.Items = agent.Bind(req.UserId, req.HigherUserId)
	d, _ := json.Marshal(respData)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 代理信息
func (s *Server) AgentInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AgentInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	info := agent.Info(req.UserId)
	d, _ := json.Marshal(info)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 会员
func (s *Server) AgentMembers(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Agent_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AgentMembers unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	info := agent.Members(req.UserId, req.PageIndex, req.PageSize)
	d, _ := json.Marshal(info)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 代理统计
func (s *Server) AgentStat(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Agent_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AgentStat unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	info := agent.Stat(req.UserId, req.PageIndex, req.PageSize)
	d, _ := json.Marshal(info)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 收益记录
func (s *Server) AgentCommissionLog(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Agent_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AgentCommissionLog unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	info := agent.CommissionLog(req.UserId, req.FromUserId, req.PageIndex, req.PageSize)
	d, _ := json.Marshal(info)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 提取收益
func (s *Server) AgentCommissionToAmount(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AgentCommissionToAmount unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var info struct {
		RetCode    int
		Commission int
	}

	info.RetCode, info.Commission = agent.CommissionToAmount(req.UserId, req.IpAddress)
	d, _ := json.Marshal(info)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 获取群组
func (s *Server) AgentGetGroup(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AgentGroup json unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret := agent.GetGroup(req.UserId)
	d, _ := json.Marshal(ret)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 修改群组
func (s *Server) AgentUpdateGroup(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.AgentGroup_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AgentUpdateGroup json unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret := agent.UpdateGroup(req.UserId, req.Id, req.Name, req.Url)
	d, _ := json.Marshal(ret)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}
