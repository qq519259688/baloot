package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/video"
	item "bet24.com/servers/micros/item_inventory/proto"
	"context"
	"encoding/json"
	"errors"
)

func (s *Server) VideoPlay(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Video_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.VideoPlay unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var ret struct {
		Success     bool
		MyPlayTimes int
		Awards      []item.ItemPack
	}

	ret.Success, ret.MyPlayTimes, ret.Awards = video.Play(req.UserId, req.VideoId)
	if ret.Success {
		reply.Resp.RetCode = 1
	}
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) VideoGetInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Video_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.VideoIsPlay unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var ret struct {
		Success      bool
		MyPlayTimes  int
		SysPlayTimes int
		Awards       []item.ItemPack
	}

	ret.Success, ret.MyPlayTimes, ret.SysPlayTimes, ret.Awards = video.GetInfo(req.UserId, req.VideoId)
	if ret.Success {
		reply.Resp.RetCode = 1
	}
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) VideoSettleInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.VideoSettleInfo_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.VideoSettleInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	ret := video.GetSettleInfo(req.UserId, req.GameID, req.SettleAmount)
	if ret.Success {
		reply.Resp.RetCode = 1
	}

	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) VideoSettle(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.VideoSettle_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.VideoSettle unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	success, returnAmount := video.Settle(req.UserId, req.TimeStamp)
	if success {
		reply.Resp.RetCode = returnAmount
	}
	return nil
}

func (s *Server) GetGameSettleVideoList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetGameSettleVideoList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	list := video.GetGameSettleVideoList(req.UserId)
	buf, _ := json.Marshal(list)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(buf)
	return nil
}

func (s *Server) AwardGameSettleVideo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.AwardGameSettleVideo_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AwardGameSettleVideo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	info := video.AwardGameSettleVideo(req.UserId, req.SettleId)
	buf, _ := json.Marshal(info)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(buf)
	return nil
}
