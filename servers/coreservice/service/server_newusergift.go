package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/newusergift"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

func (s *Server) GetNewUserGiftInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetNewUserGiftInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.RetCode = 1
	reply.Resp.Data = newusergift.GetNewUserGiftInfo(req.UserId)
	return nil
}

func (s *Server) ReceiveNewUserGift(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.ReceiveNewUserGift_Req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.ReceiveNewUserGift unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.RetCode = 1
	reply.Resp.Data = newusergift.ReceiveNewUserGift(req.UserId, req.IsDouble)
	return nil
}
func (s *Server) GetNewYearGiftInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetNewYearGiftInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.RetCode = 1
	reply.Resp.Data = newusergift.GetNewYearGiftInfo(req.UserId)
	return nil
}

func (s *Server) ReceiveNewYearGift(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.ReceiveNewUserGift_Req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.ReceiveNewYearGift unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	reply.Resp.RetCode = 1
	reply.Resp.Data = newusergift.ReceiveNewYearGift(req.UserId, req.IsDouble)
	return nil
}
