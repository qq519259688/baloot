package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/gold2chipwheel"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

//金币日志
func (s *Server) ChipWheel(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.ChipWheel_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.ChipWheel unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	reply.Resp.RetCode = gold2chipwheel.Wheel(req.UserId, req.Param, req.IpAddress)
	reply.Resp.Data = ""
	return nil
}

func (s *Server) GetChipWheelConfig(ctx context.Context, args *client.Request, reply *client.Reply) error {
	reply.Resp.RetCode = 1
	reply.Resp.Data = gold2chipwheel.GetConfig()
	return nil
}

func (s *Server) GetChipWheelHistory(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetChipWheelHistory unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	reply.Resp.RetCode = 1
	reply.Resp.Data = gold2chipwheel.GetRecord(req.UserId)
	return nil
}
