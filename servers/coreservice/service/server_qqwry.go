package service

import (
	"context"
	"encoding/json"
	"errors"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/qqwry"
)

func (s *Server) FindIP(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.FindIP unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	info := qqwry.FindIP(req.IpAddress)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(info)
	reply.Resp.Data = string(d)
	return nil
}

/*
func (s *Server) GetCurrency(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetCurrency unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	info := qqwry.GetCurrency(req.IpAddress)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(info)
	reply.Resp.Data = string(d)
	return nil
}
*/
