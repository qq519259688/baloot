package service

import (
	"context"
	"encoding/json"
	"errors"
	"strconv"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/rank"
	item "bet24.com/servers/micros/item_inventory/proto"
)

// 获取排行榜列表
func (s *Server) GetRankList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.RankList_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetRankList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret, list := rank.GetRankList(req.UserId, req.RankType, req.Num)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(struct {
		Amount   int
		IsGift   int
		Awards   []item.ItemPack
		RankList interface{}
	}{
		Amount:   ret.Amount,
		IsGift:   ret.IsGift,
		Awards:   ret.Awards,
		RankList: list,
	})
	reply.Resp.Data = string(d)
	return nil
}

// 榜单历史
func (s *Server) RankHistoryList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.RankHistory_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.RankHistoryList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	amount, ranks := rank.RankHistoryList(req.UserId, req.DateFlag)
	d, _ := json.Marshal(struct {
		Amount   int
		RankList interface{}
	}{
		Amount:   amount,
		RankList: ranks,
	})
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 领取榜单奖励
func (s *Server) RankAward(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.RankAward_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.RankAward unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret := rank.RankAward(req.UserId, req.RankType, req.DoubleFlag)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(strconv.Itoa(ret))
	return nil
}

// 榜单奖励列表
func (s *Server) GetRankAwardList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	ret := rank.GetRankAwardList()
	reply.Resp.RetCode = 1
	buf, _ := json.Marshal(ret)
	reply.Resp.Data = string(buf)
	return nil
}
