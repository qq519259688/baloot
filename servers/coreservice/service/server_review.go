package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/review"
	"context"
	"encoding/json"
	"errors"
)

//获取评论信息
func (s *Server) ReviewGetInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Review_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.ReviewGetInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	ret := review.Info(req.UserId, req.AppName)

	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)

	return nil
}

//领取评论
func (s *Server) ReviewGift(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Review_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.ReviewGift unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var ret struct {
		Success bool
		Amount  int
	}

	ret.Success, ret.Amount = review.Gift(req.UserId, req.AppName, req.IpAddress)

	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)

	return nil
}
