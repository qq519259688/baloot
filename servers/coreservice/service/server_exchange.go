package service

import (
	"context"
	"encoding/json"
	"errors"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/exchange"
)

//获取兑换列表
func (s *Server) GetExchangeType(ctx context.Context, args *client.Request, reply *client.Reply) error {
	list := exchange.GetExchangeType()
	if len(list) > 0 {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(list)
		reply.Resp.Data = string(d)
	}
	return nil
}

//获取兑换列表
func (s *Server) GetExchangeList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetExchangeList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	list := exchange.GetExchangeList(req.UserId)
	if len(list) > 0 {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(list)
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) UserExchange(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.UserExchange_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.UserExchange unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	retCode, msg := exchange.UserExchange(req.UserId, req.ExchangeId, req.Num, req.Remark)

	retData, _ := json.Marshal(struct {
		RetCode  int
		ErrorMsg string
	}{
		RetCode:  retCode,
		ErrorMsg: msg,
	})

	reply.Resp.RetCode = 1
	reply.Resp.Data = string(retData)
	return nil
}

func (s *Server) GetExchangeHistory(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.UserExchangeHistory_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetExchangeHistory unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	recordCount, list := exchange.GetExchangeHistory(req.UserId, req.PageIndex, req.PageSize)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(client.UserExchangeHistory_resp{
		RecordCount: recordCount,
		List:        list,
	})
	reply.Resp.Data = string(d)
	return nil
}
