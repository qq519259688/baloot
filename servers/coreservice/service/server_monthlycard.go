package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/monthlycard"
	item "bet24.com/servers/micros/item_inventory/proto"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

// 月卡系统信息
func (s *Server) MonthlyCardSysInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	info := monthlycard.GetSysInfo()
	d, _ := json.Marshal(info)
	reply.Resp.RetCode = 1
	reply.Resp.Data = string(d)
	return nil
}

// 获取用户月卡信息
func (s *Server) MonthlyCardUserInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.MonthlyCardUserInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	info := monthlycard.GetUserInfo(req.UserId)
	if info != nil {
		reply.Resp.RetCode = 1
		log.Debug("MonthlyCardUserInfo userId=%d info.userId=%d %+v", req.UserId, info.UserID, info.MonthlyCardInfo)
		d, _ := json.Marshal(info.MonthlyCardInfo)
		reply.Resp.Data = string(d)
	}
	return nil
}

// 月卡领取
func (s *Server) MonthlyCardGift(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.MonthlyCardGift unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	//log.Debug("Server.MonthlyCardGift data ==> %s, req ==> %+v", args.Data, req)
	var info struct {
		RetCode int
		Awards  []item.ItemPack
	}
	info.RetCode, info.Awards = monthlycard.GiftMonth(req.UserId)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(info)
	reply.Resp.Data = string(d)
	return nil
}

// 周卡领取
func (s *Server) WeekCardGift(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.WeekCardGift unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	var info struct {
		RetCode int
		Awards  []item.ItemPack
	}
	info.RetCode, info.Awards = monthlycard.GiftWeek(req.UserId)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(info)
	reply.Resp.Data = string(d)
	return nil
}
