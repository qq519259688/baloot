package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/purchase"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

// 100K购信息(期数\元宝数量\倒计时\抽奖码集合)
func (s *Server) PurchaseInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.PurchaseInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var ret struct {
		SN      int   // 期数
		Chip    int   // 元宝数量
		Seconds int64 // 倒计时（秒）
		Numbers []int // 抽奖码集合
		Status  int   // 状态（0=空闲  1=活动中  2=派奖  3=结束）
		GoldFee int   // 金币消耗
	}

	// 获取系统信息
	ret.SN, ret.Chip, ret.Seconds, ret.Status, ret.GoldFee = purchase.GetSysInfo()
	ret.Numbers = purchase.GetUserInfo(req.UserId)

	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

// 100K购抽奖(返回操作结果\抽奖码)
func (s *Server) PurchaseBet(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.PurchaseBet unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var ret struct {
		RetCode int
		Number  int
	}
	ret.RetCode, ret.Number = purchase.Bet(req.UserId, req.IpAddress)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}
