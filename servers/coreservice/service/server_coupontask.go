package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/coupontask"
	"context"
	"encoding/json"
	"errors"
)

// 用户券任务
func (s *Server) GetUserCouponTask(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetUserCouponTask unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	enabled, info, list := coupontask.GetUserTask(req.UserId)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(client.UserCouponTask_resp{
		Enabled: enabled,
		Info:    info,
		List:    list,
	})
	reply.Resp.Data = string(d)
	return nil
}

// 触发任务
func (s *Server) TriggerCouponTask(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.TriggerCouponTask_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.TriggerCouponTask unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	log.Debug("TriggerCouponTask userId=%d gameId=%d baseScore=%d isDouble=%d players=%d",
		req.UserId, req.GameId, req.BaseScore, req.IsDouble, req.Players)

	coupontask.Trigger(req.UserId, req.GameId, req.BaseScore, req.Players, req.IsDouble)
	reply.Resp.RetCode = 1
	reply.Resp.Data = ""
	return nil
}

// 领取奖励
func (s *Server) AwardCouponTask(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.AwardCouponTask_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AwardCouponTask unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	ret := coupontask.Award(req.UserId, req.UserTaskId)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

// 修改临时上限
func (s *Server) UpdateCouponTaskTmpLimit(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.UpdateCouponTaskTmpLimit unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	ret := coupontask.UpdateTmpLimit(req.UserId)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}
