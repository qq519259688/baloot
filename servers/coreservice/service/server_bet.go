package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/bet"
	"bet24.com/servers/coreservice/client"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

// 投注列表
func (s *Server) BetList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.BetList_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.BetList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret := bet.BetList(req.UserId, req.GameID, req.Days, req.PageIndex, req.PageSize, req.BetZone)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

// 游戏历史
func (s *Server) GameHistory(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.BetList_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GameHistory unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	recordCount, list := bet.GameHistory(req.UserId, req.PageIndex, req.PageSize)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(struct {
		RecordCount int
		List        []*bet.History
	}{
		RecordCount: recordCount,
		List:        list,
	})
	reply.Resp.Data = string(d)
	return nil
}

// 游戏记录
func (s *Server) GetGameCount(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.GetGameCount_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.BetList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	list := bet.GetGameCount(req.UserId, req.GameID)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(list)
	reply.Resp.Data = string(d)
	return nil
}

// 更新domino游戏记录
func (s *Server) UpdateDominoGameCount(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.UpdateDominoGameCount_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.UpdateDominoGameCount unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	bet.UpdateDominoGameCount(req.UserId, req.Double, req.Triple, req.Quariter, req.Qunitet, req.CardData)
	reply.Resp.RetCode = 1
	return nil
}

// 更新qiuqiu游戏记录
func (s *Server) UpdateQiuqiuGameCount(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.UpdateQiuqiuGameCount_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.UpdateQiuqiuGameCount unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	bet.UpdateQiuqiuGameCount(req.UserId, req.SixDevil, req.TwinCards, req.SmallCards, req.BigCards, req.QiuQiu, req.CardData)
	reply.Resp.RetCode = 1
	return nil
}
