package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/spread"
	item "bet24.com/servers/micros/item_inventory/proto"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
)

// 会员申请
func (s *Server) SpreadApply(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.SpreadApply_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SpreadApply unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var ret struct {
		RetCode int
		Items   []item.ItemPack
	}

	ret.RetCode, ret.Items = spread.Apply(req.UserId, req.Code)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

// 会员列表
func (s *Server) SpreadMembers(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.SpreadMember_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SpreadMembers unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var ret struct {
		RecordCount int
		List        interface{}
	}
	ret.RecordCount, ret.List = spread.GetMembers(req.UserId, req.PageIndex, req.PageSize)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}

// 领取奖励
func (s *Server) SpreadGift(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.SpreadGift_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.SpreadGift unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}

	var ret struct {
		RetCode int
		Items   []item.ItemPack
	}
	ret.RetCode, ret.Items = spread.Gift(req.UserId, req.FromUserID)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(ret)
	reply.Resp.Data = string(d)
	return nil
}
