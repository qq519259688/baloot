package service

import (
	"context"
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/shop"
	"github.com/pkg/errors"
)

/*
func (s *Server) GetExchangeRateList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	ret := shop.GetExchangeRateList()
	if len(ret) > 0 {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(ret)
		reply.Resp.Data = string(d)
	}
	return nil
}*/

func (s *Server) GetShopList(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Shop_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetShopList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret := shop.GetShopList(req.UserId, req.ShopType, req.ProductID)
	if len(ret) > 0 {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(ret)
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) GetShopListByUserId(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Shop_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetShopList unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret := shop.GetShopListByUserId(req.ShopType, req.UserId, req.PartnerId)
	if len(ret) > 0 {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(ret)
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) GetProduct(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Shop_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetProduct unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret := shop.GetProduct(req.UserId, req.ShopType, req.ProductID)
	if ret != nil {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(ret)
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) Exchange(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Shop_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.Exchange unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	retCode, msg := shop.Exchange(req.UserId, req.ProductID)
	if msg != "" {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(client.Shop_resp{
			RetCode:  retCode,
			ErrorMsg: msg,
		})
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) ExchangeInBulk(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Shop_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.ExchangeInBulk unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	retCode, msg := shop.ExchangeInBulk(req.UserId, req.ProductIDs)
	if msg != "" {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(client.Shop_resp{
			RetCode:  retCode,
			ErrorMsg: msg,
		})
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) Recharge(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Shop_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.Recharge unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	retCode, msg := shop.Recharge(req.UserId, req.ProductID)
	if msg != "" {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(client.Shop_resp{
			RetCode:  retCode,
			ErrorMsg: msg,
		})
		reply.Resp.Data = string(d)
	}
	return nil
}

/*
func (s *Server) ExchangeRateInfo(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Shop_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.ExchangeRateInfo unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	ret := shop.ExchangeRateInfo(req.Currency)
	reply.Resp.Action = make([]string, 2)
	if ret != nil {
		reply.Resp.Action[0] = ret.CountryName
		reply.Resp.Action[1] = ret.Currency
	}
	return nil
}*/

func (s *Server) GetUserPrice(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Shop_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetUserPrice unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	var resp client.Shop_resp
	resp.Price = shop.GetUserPrice(req.UserId, req.Price)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(resp)
	reply.Resp.Data = string(d)
	return nil
}
