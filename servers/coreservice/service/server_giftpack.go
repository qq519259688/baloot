package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/giftpack"
	"context"
	"encoding/json"
	"errors"
)

//获取排行榜列表
func (s *Server) GetGiftPacks(ctx context.Context, args *client.Request, reply *client.Reply) error {
	packs := giftpack.GetGiftPacks()
	if len(packs) > 0 {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(packs)
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) GetBuyableGiftPacks(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetBuyableGiftPacks unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	buyable := giftpack.GetBuyable(req.UserId)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(buyable)
	reply.Resp.Data = string(d)
	return nil
}

// 成长礼包
func (s *Server) GetGrowthGiftPacks(ctx context.Context, args *client.Request, reply *client.Reply) error {
	packs := giftpack.GetGrowthPacks()
	if len(packs) > 0 {
		reply.Resp.RetCode = 1
		d, _ := json.Marshal(packs)
		reply.Resp.Data = string(d)
	}
	return nil
}

func (s *Server) GetUserGrowthTerms(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetUserGrowthTerms unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	terms := giftpack.GetUserGrowthTerms(req.UserId)
	reply.Resp.RetCode = 1
	d, _ := json.Marshal(terms)
	reply.Resp.Data = string(d)
	return nil
}

func (s *Server) UserAwardTerm(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.GrowthPackAward_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.UserAwardTerm unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	var resp client.GrowthPackAward_resp
	resp.Succeeded, resp.ErrMsg = giftpack.UserAwardTerm(req.UserId, req.GrowthPackId, req.Index)
	if resp.Succeeded {
		reply.Resp.RetCode = 1
	}
	d, _ := json.Marshal(resp)
	reply.Resp.Data = string(d)
	return nil
}
