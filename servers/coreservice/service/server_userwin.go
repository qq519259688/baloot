package service

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/userwin"
	"context"
	"encoding/json"
	"errors"
)

func (s *Server) AddUserWinScore(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.UserWinAddScore_req
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.AddUserWinScore unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	userwin.AddScore(req.UserId, req.Score)
	reply.Resp.RetCode = 1
	return nil
}

func (s *Server) GetUserWinScore(ctx context.Context, args *client.Request, reply *client.Reply) error {
	var req client.Request_base
	if err := json.Unmarshal([]byte(args.Data), &req); err != nil {
		log.Debug("Server.GetUserWinScore unmarshal fail %v", err)
		return errors.New("unmarshal error")
	}
	reply.Resp.RetCode = userwin.GetScore(req.UserId)
	return nil
}
