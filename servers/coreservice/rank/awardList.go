package rank

import (
	"sync"
	"time"

	item "bet24.com/servers/micros/item_inventory/proto"
)

var Awards *AwardList

type AwardList struct {
	Awards []RankAwardInfo
	lock   *sync.RWMutex
}

func NewAwardList() *AwardList {
	obj := new(AwardList)
	obj.lock = &sync.RWMutex{}
	return obj
}

func (this *AwardList) Run() {
	this.refreshData()
}
func (this *AwardList) refreshData() {
	go this.doRefresh()
	time.AfterFunc(3*time.Second, this.refreshData)
}

func (this *AwardList) doRefresh() {
	list := getAwardList()
	this.lock.Lock()
	defer this.lock.Unlock()
	this.Awards = list
}

// 根据排名获取奖励信息
func (this *AwardList) getAwards(rankType, rank int) []item.ItemPack {
	this.lock.RLock()
	list := this.Awards
	this.lock.RUnlock()

	for _, v := range list {
		if v.RankType != rankType || v.Rank != rank {
			continue
		}

		return v.Items
	}

	return nil
}

// 获取奖励列表
func (this *AwardList) getList() []RankAwardInfo {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.Awards
}
