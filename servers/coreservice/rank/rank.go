package rank

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

type RankAwardInfo struct {
	RankType int             // 排行榜类型
	Rank     int             // 名次
	Items    []item.ItemPack // 道具
}

func Run() {
	log.Debug("rank running")

	//排行榜奖励
	Awards = NewAwardList()
	Awards.Run()

	//排行榜列表
	Ranks = NewRankList()
	Ranks.Run()
}

// 获取排行榜列表
func GetRankList(userId, rankType, num int) (*AwardItem, []*rankItem) {
	return Ranks.getRankList(userId, rankType, num)
}

// 榜单历史
func RankHistoryList(userId int, dateFlag string) (int, []*rankHistoryItem) {
	return 0, nil
}

// 领取积分榜奖励
func RankAward(userId, rankType, doubleFlag int) int {
	return Ranks.award(userId, rankType, doubleFlag)
}

// 获取榜单奖励列表
func GetRankAwardList() []RankAwardInfo {
	return Awards.getList()
}

// 小红点
func Tip(userId, rankType int) bool {
	return Ranks.tip(userId, rankType)
}
