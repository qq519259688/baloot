package rank

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/dbengine"
)

// 排行榜列表
func getRankList() []*rankItem {
	defer func() {
		if err := recover(); err != nil {
			log.Release("rank.getRankList transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Rank_GetList")
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)

	var list []*rankItem
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out rankItem
		out.RankType = int((ret[0]).(int64))
		out.Rank = int((ret[1]).(int64))
		out.UserId = int((ret[2]).(int64))
		out.Amount = int((ret[3]).(int64))
		out.IsGift = int((ret[4]).(int64))
		out.crdate = ret[5].(string)

		list = append(list, &out)
	}

	return list
}

func getAwardList() []RankAwardInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	var list []RankAwardInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Rank_GetAwardList")
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info RankAwardInfo

		info.RankType = int((ret[0]).(int64))
		info.Rank = int((ret[1]).(int64))
		tools := (ret[2]).(string)
		if len(tools) > 0 {
			if err := json.Unmarshal([]byte(tools), &info.Items); err != nil {
				log.Error("rank.getAwardList json unmarshal fail err %v", err)
			}
		}

		list = append(list, info)
	}

	return list
}

// 领取榜单奖励
func award(userId, rankType, doubleFlag int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Rank_Award")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RankType", database.AdParamInput, database.AdInteger, 4, rankType)
	statement.AddParamter("@DoubleFlag", database.AdParamInput, database.AdInteger, 4, doubleFlag)
	sqlString := statement.GenSql()
	log.Debug(sqlString)
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return 0
	}

	return int((retRows[0][0]).(int64))
}

// 榜单信息
func getRankInfo(userId, rankType int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("rank.getCharm transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Rank_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RankType", database.AdParamInput, database.AdInteger, 4, rankType)
	sqlString := statement.GenSql()
	retRows := dbengine.CenterDB.ExecSql(sqlString)
	if len(retRows) <= 0 {
		return 0
	}
	return int((retRows[0][0]).(int64))
}
