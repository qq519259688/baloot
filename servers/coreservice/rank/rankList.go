package rank

import (
	audioroomPb "bet24.com/servers/micros/audioroom/proto"
	badge "bet24.com/servers/micros/badge/proto"
	"sync"
	"time"

	"bet24.com/servers/common"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	cash "bet24.com/servers/micros/money/proto"

	"bet24.com/log"
	_ "bet24.com/servers/coreservice/friend"
	user "bet24.com/servers/micros/userservices/proto"
)

var Ranks *RankList

type RankList struct {
	Ranks       map[int][]*rankItem           // 榜单类型--榜单列表
	RankHistory map[string][]*rankHistoryItem // 榜单历史
	lock        *sync.RWMutex
}

func NewRankList() *RankList {
	obj := new(RankList)
	obj.Ranks = make(map[int][]*rankItem)
	obj.RankHistory = make(map[string][]*rankHistoryItem)
	obj.lock = &sync.RWMutex{}
	return obj
}

func (this *RankList) Run() {
	this.refreshData()
}

func (this *RankList) refreshData() {
	go this.doRefresh()
	time.AfterFunc(3*time.Second, this.refreshData)
}

func (this *RankList) doRefresh() {
	ranks := make(map[int][]*rankItem)

	for _, v := range getRankList() {
		// 语聊房榜单涉及到房间信息处理
		if v.RankType == RankType_audioRoom_lastWeek ||
			v.RankType == RankType_audioRoom_thisWeek ||
			v.RankType == RankType_audioRoom_consumeDiamond_day ||
			v.RankType == RankType_audioRoom_consumeDiamond_week ||
			v.RankType == RankType_audioRoom_consumeDiamond_month {

			// 获取语聊房信息
			r := audioroomPb.GetRoom(v.UserId)
			if r.RoomInfo == nil {
				log.Debug("rankList.doRefresh roomId=%d ==> %+v", v.UserId, v)
				continue
			}

			rankItem := &rankItem{
				RankType: v.RankType,
				Rank:     v.Rank,
				Amount:   v.Amount,
				UserHotInfo: user.UserHotInfo{
					UserId:   r.RoomInfo.RoomId,
					NickName: r.RoomInfo.RoomName,
					FaceUrl:  r.RoomInfo.RoomImg,
				},
				AwardItem: AwardItem{
					IsGift: v.IsGift,
					Awards: Awards.getAwards(v.RankType, v.Rank),
				},
				crdate: v.crdate,
			}

			ranks[v.RankType] = append(ranks[v.RankType], rankItem)
			continue
		}

		u := user.GetUserInfo(v.UserId)
		if u == nil {
			log.Debug("rankList.doRefresh userId=%d ==> %+v", v.UserId, v)
			continue
		}

		rankItem := &rankItem{
			RankType: v.RankType,
			Rank:     v.Rank,
			Amount:   v.Amount,
			UserHotInfo: user.UserHotInfo{
				UserId:      v.UserId,
				NickName:    u.NickName,
				FaceId:      u.FaceId,
				FaceUrl:     u.FaceUrl,
				Sex:         u.Sex,
				UserWords:   u.UserWords,
				Vip:         u.Vip,
				Decorations: u.Decorations,
				Badges:      u.Badges,
				Charm:       u.Charm,
				VipExpire:   u.VipExpire,
			},
			AwardItem: AwardItem{
				IsGift: v.IsGift,
				Awards: Awards.getAwards(v.RankType, v.Rank),
			},
			crdate: v.crdate,
		}

		ranks[v.RankType] = append(ranks[v.RankType], rankItem)
	}

	now := common.GetNowTime()
	// 只取零点且前三十分钟的数据才有效[00:30]
	if now.Hour() == 0 && now.Minute() > 5 && now.Minute() < 30 {
		// 榜单触发荣誉徽章(主要是周榜)
		for _, rankType := range badgeRankArray {
			// 榜单
			if badgeRanks := ranks[rankType]; len(badgeRanks) > 0 {
				for _, v := range badgeRanks {
					// 徽章进度（只统计周榜）
					go badge.DoAction(v.UserId, badge.Action_Ranking, 0, badge.Scope{
						RankCrdate: v.crdate,
						RankInfo: badge.RankInfo{
							Type: v.RankType,
							Rank: v.Rank,
						},
					})
				}
			}
		}
	}

	this.lock.Lock()
	defer this.lock.Unlock()
	this.Ranks = ranks
}

// 获取排行榜列表
func (this *RankList) getRankList(userId, rankType, num int) (*AwardItem, []*rankItem) {
	var list []*rankItem
	this.lock.RLock()
	if num > 0 && len(this.Ranks[rankType]) >= num {
		list = this.Ranks[rankType][:num]
	} else {
		list = this.Ranks[rankType]
	}
	this.lock.RUnlock()

	ret := new(AwardItem)

	for _, v := range list {
		if v.UserId != userId {
			//v.IsFriend = friend.IfFriend(userId, v.UserId)
			v.IsGift = Award_Invalid
			continue
		}

		ret.Amount = v.Amount
		ret.IsGift = v.IsGift
		ret.Awards = v.Awards

		// log.Debug("getRankList userId=%d rankType=%d v=%+v  ret=%+v", userId, rankType, v, ret)

		// 没有奖励
		if ret.Awards == nil {
			ret.IsGift = Award_Invalid
		}
	}

	if ret.Amount > 0 {
		return ret, list
	}

	// 没有上榜数据,需要去DB取
	if rankType == RankType_wealth {
		_, ret.Amount = cash.GetMoney(userId)
	} else {
		ret.Amount = getRankInfo(userId, rankType)
	}

	return ret, list
}

// 领取奖励
func (this *RankList) award(userId, rankType, doubleFlag int) int {
	if doubleFlag != 0 && doubleFlag != 1 {
		return 11
	}

	this.lock.RLock()
	list := this.Ranks[rankType]
	this.lock.RUnlock()

	if len(list) <= 0 {
		return 11
	}

	var awards []item.ItemPack

	for _, v := range list {
		if v.UserId != userId {
			continue
		}

		// 无效
		if v.IsGift == Award_Invalid {
			return 11
		}

		// 已领取完
		if v.IsGift == Award_Received {
			return 12
		}

		// 单次领取
		if v.IsGift != Award_Available {
			return 12
		}

		v.IsGift = Award_Received
		awards = v.Awards
		break
	}

	if len(awards) <= 0 {
		return 11
	}

	// 更新数据库
	ret := award(userId, rankType, doubleFlag)

	// 发放奖励
	if ret == 1 {
		inventory.AddItems(userId, awards, "榜单奖励", common.LOGTYPE_RANK_AWARD)
	}

	return ret
}

// 积分榜小红点
func (this *RankList) tip(userId, rankType int) bool {
	this.lock.RLock()
	list := this.Ranks[rankType]
	this.lock.RUnlock()

	for _, v := range list {
		if v.UserId != userId {
			continue
		}

		if v.IsGift == Award_Available {
			return true
		}
	}

	return false
}
