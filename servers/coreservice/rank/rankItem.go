package rank

import (
	item "bet24.com/servers/micros/item_inventory/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
)

const (
	_                                       = iota
	RankType_wealth                         // 1=财富榜
	RankType_win_day                        // 2=每日赢金榜
	RankType_win_week                       // 3=每周赢金榜
	RankType_win_month                      // 4=每月赢金榜
	RankType_badge                          // 5=徽章排行榜
	RankType_audioRoom_thisWeek             // 6=语聊房本周榜
	RankType_audioRoom_lastWeek             // 7=语聊房上周榜
	RankType_charm_day                      // 8=魅力日榜
	RankType_charm_week                     // 9=魅力周榜
	RankType_charm_month                    // 10=魅力月榜
	RankType_audioRoom_sendGiving_day       // 11=语聊房赠送礼物日榜
	RankType_audioRoom_sendGiving_week      // 12=语聊房赠送礼物周榜
	RankType_audioRoom_sendGiving_month     // 13=语聊房赠送礼物月榜
	RankType_audioRoom_receiveGiving_day    // 14=语聊房接收礼物日榜
	RankType_audioRoom_receiveGiving_week   // 15=语聊房接收礼物周榜
	RankType_audioRoom_receiveGiving_month  // 16=语聊房接收礼物月榜
	RankType_audioRoom_consumeDiamond_day   // 17=语聊房房间消耗钻石日榜
	RankType_audioRoom_consumeDiamond_week  // 18=语聊房房间消耗钻石周榜
	RankType_audioRoom_consumeDiamond_month // 19=语聊房房间消耗钻石月榜
	RankType_friendRoom_winCount_day        // 20=好友房赢局数日榜
	RankType_friendRoom_winCount_week       // 21=好友房赢局数周榜
	RankType_Max                            //
)

// 荣誉徽章榜单数组
var badgeRankArray = []int{RankType_win_week,
	RankType_audioRoom_lastWeek,
	RankType_audioRoom_sendGiving_week,
	RankType_audioRoom_receiveGiving_week,
	RankType_audioRoom_consumeDiamond_week}

const (
	Award_Invalid   = iota // 0=无效
	Award_Available        // 1=可领取
	Award_Received         // 2=已领取
)

type rankItem struct {
	RankType int // 类型
	Rank     int // 名次
	Amount   int // 数量
	IsFriend int // 是否好友
	userservices.UserHotInfo
	AwardItem
	crdate string
}

type AwardItem struct {
	Amount int // 金额
	IsGift int `json:"IsGift,omitempty"` // 是否领取(0=无效  1=可领取  2=已领取)
	Awards []item.ItemPack
}

// 榜单历史
type rankHistoryItem struct {
	Rank      int    // 名次
	UserId    int    // 用户ID
	NickName  string // 昵称
	Amount    int    // 数量
	FaceID    int    // 头像ID
	FaceUrl   string // 头像URL
	Sex       int    // 性别
	UserWords string // 用户签名
	VipLevel  int    // Vip等级
}
