package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sort"
	"time"

	slog "bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
)

var Server struct {
	LogLevel        string
	FileLevel       string
	LogPath         string
	ServerIP        string
	ServerPort      int
	CertFile        string
	KeyFile         string
	MaxConnNum      int
	ChannelUrl      string
	ChannelPassword string
	MonitorPort     int
	RedisDB         int
	LastHour        int
	ServiceAddr     string
	IsChipRoom      int
	TestPay         bool
}

// 短信配置
type SMSConfig struct {
	SMSPostUrl       string `json:"sms.postUrl"`
	SMSAccessID      string `json:"sms.accessID"`
	SMSAccessSecrect string `json:"sms.accessSecret"`
	SMSSendMsg       string `json:"sms.sendMsg"`
	SMSTestCode      string `json:"sms.testCode"`
}

var HallConfig struct {
	SubsidyAmount       int
	SubsidyTimes        int
	SubsidyCoolSeconds  []int
	AdConfig            int
	AdBannerClosed      int              // 0 表示打开，1表示关闭
	IsAgentClosed       int              // 是否关闭代理，0表示打开，1表示关闭
	SMSConfig                            //短信配置
	LoginConfig         int              // 登录选项，0-默认  1-仅游客 10-仅fb 100-仅google 110-fb+google
	ChangeNames         []ChangeNameInfo `json:"changeNames"`
	ThirdPaymentOpen    []string
	GuestPriveRoomClose int
	GuestMatchClose     int
}

// 修改昵称
type ChangeNameInfo struct {
	Times int `json:"times"` // 修改次数
	Gold  int `json:"gold"`  // 金币数
}

const config_key = "hall_config"

func init() {
	data, err := os.ReadFile("fishconf/fishhallserver.json")
	if err != nil {
		log.Fatalf("read config failed fishconf/fishhallserver.json %v", err)
		return
	}
	fmt.Println(string(data))
	err = json.Unmarshal(data, &Server)
	if err != nil {
		log.Fatalf("Unmarshal config failed fishconf/fishhallserver.json err:%v", err)
	}

	logger, err := slog.New(Server.LogLevel, Server.FileLevel, Server.LogPath, log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	if Server.MonitorPort == 0 {
		Server.MonitorPort = 8600
	}
}

func Run() {
	refreshLogFile()
}

func loadRedisConfig() bool {
	data := coreservice.GetPlatformConfig(config_key)
	if data == "" {
		slog.Release("    config msg is null")
		return false
	}

	return marshalData([]byte(data))
}

func refreshLogFile() {
	defer func() {
		time.AfterFunc(5*time.Minute, refreshLogFile)
		doRefreshLogFile()
	}()

	if loadRedisConfig() {
		return
	}

	data, err := os.ReadFile("fishconf/fishhallconfig.json")
	if err != nil {
		slog.Release("read config failed fishconf/fishhallconfig.json %v", err)
		return
	}

	if marshalData(data) {
		// 写入redis
		coreservice.SetPlatformConfig(config_key, string(data))
	}
}

func marshalData(data []byte) bool {
	err := json.Unmarshal(data, &HallConfig)
	if err != nil {
		slog.Release("Unmarshal config [%s] err:%v", string(data), err)
		return false
	}
	sort.SliceStable(HallConfig.ChangeNames, func(i, j int) bool {
		return HallConfig.ChangeNames[i].Times < HallConfig.ChangeNames[j].Times
	})

	return true
}

func doRefreshLogFile() {
	if Server.LogPath != "" {
		now := time.Now()
		if now.Hour() != Server.LastHour {
			Server.LastHour = now.Hour()
			slog.RecreateFileLog(Server.LogPath, log.LstdFlags)
		}
	}
}
