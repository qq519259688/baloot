package gatesink

import (
	"bet24.com/log"
	singlematch "bet24.com/servers/micros/matches/proto"
	"encoding/json"
	"strconv"
)

func (this *user) onSingleMatchMsg(msg, data string) {
	switch msg {
	case "createSingleMatch":
		this.createSingleMatch(msg, data)
	case "getMySingleMatch":
		this.getMySingleMatch(msg, data)
	case "getSingleMatchList":
		this.getSingleMatchList(msg, data)
	case "SingleMatchListRevive":
		this.singleMatchListRevive(msg, data)
	}

}

func (this *user) createSingleMatch(msg, data string) {
	matchId, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.createSingleMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	var ret singlematch.SingleMatch_resp
	ret.ErrorMsg, ret.MatchData = singlematch.CreateUserSingleMatch(this.getUserId(), matchId)
	d, _ := json.Marshal(ret)
	this.WriteMsg(msg, string(d))
}

func (this *user) getMySingleMatch(msg, data string) {
	matchInfo := singlematch.GetUserSingleMatch(this.getUserId())
	d, _ := json.Marshal(matchInfo)
	this.WriteMsg(msg, string(d))
}

func (this *user) singleMatchListRevive(msg, data string) {
	ok := singlematch.SingleMatchRevive(this.getUserId(), data != "")
	if ok {
		this.WriteMsg(msg, "ok")
		return
	}
	this.WriteMsg(msg, "fail")
}

func (this *user) getSingleMatchList(msg, data string) {
	this.WriteMsg(msg, singlematch.GetSingleMatchList())
}
