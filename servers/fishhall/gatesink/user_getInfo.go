package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/transaction"
)

//查询社交类用户信息(昵称、服务器名称、头像变化)
func (this *user) getInfo(msg, data string) {
	action := transaction.NewTransGetInfo()
	e := json.Unmarshal([]byte(data), &action.In)
	if e != nil {
		log.Release("getInfo Unmarshal data failed %v", data)
		this.WriteMsg(msg, fmt.Sprintf("getInfo Unmarshal data failed %v", data))
		return
	}
	action.DoAction(nil)

	buf, e := json.Marshal(action.Out)
	if e != nil {
		log.Release("send getInfo return Marshal failed err=%v", e)
		this.WriteMsg(msg, fmt.Sprintf("send getInfo return Marshal failed err=%v", e))
		return
	}

	this.WriteMsg(msg, string(buf))
	return
}
