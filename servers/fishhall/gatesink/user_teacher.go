package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"

	"bet24.com/servers/fishhall/protocol"
)

// 注册
func (this *user) teacherRegister(msg, data string) {
	resp := client.TeacherRegister(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.teacherRegister failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 师父信息
func (this *user) teacherInfo(msg, data string) {
	resp := client.TeacherInfo(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.teacherInfo failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 绑定
func (this *user) bindTeacher(msg, data string) {
	var req protocol.TeacherBind
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("bindTeacher unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.BindTeacher(this.getUserId(), req.TeacherId, 0)
	if resp.RetCode != 1 {
		log.Debug("user.bindTeacher failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 徒弟列表
func (this *user) students(msg, data string) {
	resp := client.Students(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.students failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 收益列表
func (this *user) teacherProfitList(msg, data string) {
	var req protocol.TeacherProfit
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user.teacherProfit unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.TeacherProfitList(this.getUserId(), req.Days, req.PageIndex, req.PageSize)
	if resp.RetCode != 1 {
		log.Debug("user.teacherProfitList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}
