package gatesink

import (
	"bet24.com/servers/transaction"
	"encoding/json"
	"fmt"
	"math/rand"
	"strings"

	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/redis"
	"bet24.com/servers/fishhall/config"
)

const (
	_                 = iota // 无效值
	SMSCODE_TYPE_Bind        // 1=手机号绑定
)

// 发送手机验证码
func (this *user) sendSms(msg, data string) {
	obj := transaction.NewTransExistTel()
	if err := json.Unmarshal([]byte(data), &obj.In); err != nil {
		retData := fmt.Sprintf("sendSms Unmarshal data failed %v", data)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	// 去掉空格
	obj.In.Tel = strings.ReplaceAll(obj.In.Tel, " ", "")
	obj.In.Tel = strings.Replace(obj.In.Tel, "00620", "0062", 1)

	// 校验手机号
	validTel, err := public.CheckTelValid(obj.In.Tel)
	if err != nil {
		log.Debug("sendSms tel valid err:%v", err)
		this.WriteMsg(msg, "12")
		return
	}

	obj.DoAction(nil)

	// 手机号已存在
	if obj.Out.RetCode == 1 {
		log.Debug("sendSms tel is exist ==> userId=%d obj.In=%+v", this.getUserId(), obj.In)
		this.WriteMsg(msg, "11")
		return
	}

	// 获取验证码
	value := this.getSmsCode(validTel, obj.In.CodeType)

	// 没有验证，需要生成
	if len(value) <= 0 {
		// 缓存验证码(6位验证码)
		key := fmt.Sprintf("smsCode:%s:%d", validTel, obj.In.CodeType)
		value = this.GenCode(6)

		if !redis.String_SetEx(key, value, 300) {
			log.Release("send SmsCode redis failed ==> userId=%d key=%s value=%s", this.getUserId(), key, value)
			this.WriteMsg(msg, "13")
			return
		}
	}

	// 默认短信内容
	sendMsg := config.HallConfig.SMSSendMsg

	// 向短信平台发送 Post 请求，并获取响应数据
	go func(SMSSendMsg string) {
		body, err := public.HttpSMSCode(validTel,
			config.HallConfig.SMSConfig.SMSSendMsg,
			config.HallConfig.SMSConfig.SMSAccessID+config.HallConfig.SMSConfig.SMSAccessSecrect,
			config.HallConfig.SMSConfig.SMSPostUrl,
			value)
		if err != nil {
			log.Error("HttpSMSCode %s", err)
			return
		}
		log.Debug("httpSMSCode ==> %s", body)
	}(sendMsg)

	this.WriteMsg(msg, "1")
}

// 校验验证码==>返回 isSuccess、isTest、isVerify
func (this *user) checkCode(tel string, code string, smsCodeType int) (bool, int, int) {
	//为空信息，直接返回
	if tel == "" || code == "" {
		log.Debug("checkCode tel=%s code=%s", tel, code)
		return false, 0, 0
	}

	//注册，且是万能验证码 000000
	if smsCodeType == SMSCODE_TYPE_Bind && code == "000000" {
		return true, 0, 0
	}

	//内部码(只能用于非提现部分，即实名信息)
	if code == config.HallConfig.SMSConfig.SMSTestCode {
		return true, 1, 1
	}

	return code == this.getSmsCode(tel, smsCodeType), 0, 1
}

// 获取验证码
func (this *user) getSmsCode(tel string, smsCodeType int) string {
	key := fmt.Sprintf("smsCode:%s:%d", tel, smsCodeType)
	value, succ := redis.String_Get(key)
	if !succ {
		log.Debug("get SmsCode redis failed")
		return ""
	}
	return value
}

// 生成短信验证码
func (this *user) GenCode(length int) string {
	// var chars = []byte("ABCDEFGHJKLMNPQRTUVWXYZ123456789")
	var chars = []byte("0123456789")
	code := make([]byte, length)
	clen := len(chars)
	for i := 0; i < length; i++ {
		index := rand.Intn(clen)
		code[i] = byte(chars[index])
	}

	if string(code) == config.HallConfig.SMSConfig.SMSTestCode {
		code[0] = 0
	}

	return string(code)
}
