package gatesink

import (
	"strconv"

	"bet24.com/servers/transaction"
)

// 删除应用账号
func (this *user) deleteAccount(msg, data string) {
	ret := transaction.DeleteAccount(this.getUserId())
	this.WriteMsg(msg, strconv.FormatBool(ret))
}
