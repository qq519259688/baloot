package gatesink

import (
	"encoding/json"
	"fmt"

	userlabel "bet24.com/servers/micros/userlabel/proto"

	"bet24.com/log"
	dotservice "bet24.com/servers/micros/dotservice/proto"
)

func (this *user) addDot(msg string, data string) {
	var req dotservice.Request_AddDot
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("addDot Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	go dotservice.AddDot(this.getUserId(), req.DotScope)

	// 商城点击事件
	if req.DotScope.Scene == dotservice.Scene_Shop {
		go userlabel.TriggerEvent(this.getUserId(), userlabel.Type_Charge, userlabel.Scope{Num: 1})
	}

	this.WriteMsg(msg, "")
}
