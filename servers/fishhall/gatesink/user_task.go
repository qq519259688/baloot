package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/fishhall/protocol"
	task "bet24.com/servers/micros/task/proto"
	"encoding/json"
	"fmt"
)

func (this *user) getSysTask(msg, data string) {
	var req protocol.AwardTask_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("getSysTask Unmarshal data failed %v", data)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	t := task.GetSysTask(req.TaskId)
	if t == nil {
		log.Release("user.getSysTask failed task[%d] not exist", req.TaskId)
		return
	}
	d, _ := json.Marshal(t)
	this.WriteMsg(msg, string(d))
}

func (this *user) getSysTaskList(msg, data string) {
	resp := task.GetSysTaskList()
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
}

func (this *user) getUserTaskList(msg, data string) {
	resp := task.GetUserTasks(this.getUserId())
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
}

func (this *user) awardTask(msg, data string) {
	var req protocol.AwardTask_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("awardTask Unmarshal data failed %v", data)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	ok, err := task.AwardTask(this.getUserId(), req.TaskId)
	if !ok {
		log.Debug("user.awardTask failed %v", err)
	}
	this.WriteMsg(msg, err)
}

func (this *user) awardAllTask(msg string) {
	items := task.AwardAllTask(this.getUserId())
	d, _ := json.Marshal(items)
	this.WriteMsg(msg, string(d))
}
