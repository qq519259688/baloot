package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

func (this *user) onBattlePassMsg(msg, data string) {
	switch msg {
	case "getBattlePassPacks": // 获取礼包数据
		this.getBattlePassPacks(msg)
	case "getBattlePassTerms": // 获取用户可购买的礼包列表
		this.getBattlePassTerms(msg)
	case "awardBattlePass": // 获取用户可购买的礼包列表
		this.awardBattlePass(msg, data)
	}
}

func (this *user) getBattlePassPacks(msg string) {
	resp := client.GetBattlePassPacks()
	if resp.RetCode != 1 {
		log.Debug("user.getBattlePassPacks failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) getBattlePassTerms(msg string) {
	resp := client.GetUserBattlePass(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.getBattlePassTerms failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) awardBattlePass(msg string, data string) {
	var req protocol.GrowthPackAward_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("awardBattlePass Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.UserAwardBattlePass(this.getUserId(), req.GiftPackId, req.Index)
	if resp.RetCode != 1 {
		log.Debug("user.awardBattlePass failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}
