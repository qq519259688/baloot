package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

func (this *user) useRechargeCard(msg, data string) {
	retData := ""
	var req protocol.RechargeCard_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user.useRechargeCard Unmarshal data failed %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.UseRechargeCard(this.getUserId(), req.CardNo)
	if resp.RetCode != 1 {
		log.Debug("user.useRechargeCard failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}
