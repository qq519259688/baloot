package gatesink

import (
	"encoding/json"
	"sync"

	_ "fmt"
	"strconv"

	"bet24.com/log"
	"bet24.com/redis"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/config"
	"bet24.com/servers/insecureframe/gate"
	notification "bet24.com/servers/micros/notification/proto"
	"bet24.com/servers/transaction"
	commonuser "bet24.com/servers/user"
)

var Sink *GateSink

func Run() {
	Sink = new(GateSink)
	Sink.ctor()
}

type GateSink struct {
	userlist         map[int32]*user
	lock             *sync.RWMutex
	pollNotification bool // 是否需要主动轮询通知
}

func (this *GateSink) ctor() {
	this.userlist = make(map[int32]*user)
	this.lock = &sync.RWMutex{}
}

func (this *GateSink) SetPollNotification(active bool) {
	this.pollNotification = active
}

func (this *GateSink) GetServerPort() int {
	return config.Server.ServerPort
}

func (this *GateSink) OnUserEnter(userIndex int32) {
	this.lock.RLock()
	if _, ok := this.userlist[userIndex]; ok {
		log.Release("GateSink.OnUserEnter user already exist %d", userIndex)
		this.lock.RUnlock()
		return
	}
	this.lock.RUnlock()
	pUser := newUser(userIndex)
	this.lock.Lock()
	this.userlist[userIndex] = pUser
	this.lock.Unlock()
}

func (this *GateSink) OnUserLogined(userIndex int32) {
	this.lock.RLock()
	u, ok := this.userlist[userIndex]
	this.lock.RUnlock()
	if !ok {
		log.Debug("GateSink.OnUserLogined user not exist %d", userIndex)

		return
	}
	go u.onLogined()
}

func (this *GateSink) OnUserExit(userIndex int32) {
	this.lock.Lock()
	defer this.lock.Unlock()
	if user, ok := this.userlist[userIndex]; ok {
		user.clear()
	}
	delete(this.userlist, userIndex)
}

func (this *GateSink) OnGameMessage(userindex int32, userid int, msg, data string) bool {
	this.lock.RLock()
	user, ok := this.userlist[userindex]
	this.lock.RUnlock()
	if !ok {
		log.Release("GateSink.OnGameMessage user not found %d", userindex)
		return false
	}
	return user.onGameMessage(msg, data)
}

func (this *GateSink) GetGameID() int {
	return 0
}

func (this *GateSink) GetGameName() string {
	if config.Server.IsChipRoom < 0 {
		return "广告大厅"
	}
	return "游戏大厅"
}

func (this *GateSink) GetRoomName() string {
	if config.Server.IsChipRoom < 0 {
		return "广告大厅"
	}
	return "游戏大厅"
}

func (this *GateSink) GetRobotCount() int {
	return 0
}
func (this *GateSink) GetRobotGoldLimit() (min, max int) {
	return 0, 0
}
func (this *GateSink) GetRobotOnlineSec() int {
	return 0
}

func (this *GateSink) OnUserInfoChanged(userIndex int32) {

}

func (this *GateSink) GetCertFile() string {
	return config.Server.CertFile
}
func (this *GateSink) GetKeyFile() string {
	return config.Server.KeyFile
}

func (this *GateSink) IsChipRoom() bool {
	return config.Server.IsChipRoom > 0
}

func (this *GateSink) IsLadderRoom() bool {
	return false
}

func (this *GateSink) GetChipRoom() int {
	return config.Server.IsChipRoom
}

func (this *GateSink) OnPlatformConfig(key string) {

}

func (this *GateSink) IsPrivateRoom() bool {
	return false
}

func (this *GateSink) onUserNotification(userId int) {
	if userId == -1 {
		this.lock.RLock()
		defer this.lock.RUnlock()
		for _, v := range this.userlist {
			go v.getNotifications(true)
		}
		return
	}
	u := this.getUserByUserId(userId)
	if u == nil {
		return
	}

	u.getNotifications(true)
}

func (this *GateSink) getUserByUserId(userId int) *user {
	this.lock.Lock()
	defer this.lock.Unlock()
	for _, v := range this.userlist {
		if v.getUserId() == userId {
			return v
		}
	}
	return nil
}

func (this *GateSink) SendGameCmd(userId int, msg, data string) {
	// 广播
	if userId == -1 {
		this.lock.Lock()
		defer this.lock.Unlock()
		for _, v := range this.userlist {
			go func(u *user) {
				u.WriteMsg(msg, data)
			}(v)
		}
		return
	}
	usr := this.getUserByUserId(userId)
	if usr == nil {
		log.Debug("GateSink.SendGameCmd userId[%d] not exist", userId)
		return
	}
	usr.WriteMsg(msg, data)
}
func (this *GateSink) GetUserInfo(userId int) *commonuser.UserInfo {
	return gate.GetUserByUserId(userId)
}

func (this *GateSink) WriteMoney(userId int, gameId int, amount int, tax int, status int, scoreType int, srcName string) bool {
	usr := this.getUserByUserId(userId)
	userIp := ""
	userName := strconv.Itoa(userId)
	if usr != nil {
		userIp = usr.GetIP()
		userName = usr.getNickName()
	}

	var ret bool
	if this.IsChipRoom() {
		ret, _, _ = transaction.WriteChipSync(userId, gameId, amount, tax, status, scoreType, srcName, userIp)
		if ret {
			go notification.AddNotification(userId, notification.Notification_Chip, "")
			if amount >= 5000000 && gameId > 0 {
				this.sendBroadcast(userId, userName, amount, gameId, srcName)
			}
		}
	} else {
		ret, _, _ = transaction.WriteMoneySync(userId, gameId, amount, tax, status, scoreType, srcName, userIp)
		if ret {
			go notification.AddNotification(userId, notification.Notification_Gold, "")
			if amount >= 5000000 && gameId > 0 {
				this.sendBroadcast(userId, userName, amount, gameId, srcName)
			}
		}
	}

	return ret
}

func (this *GateSink) WriteMoneyWithModifyAmount(userId int, gameId int, amount int, tax int, status int, scoreType int, srcName string, userIp string) int {
	if this.IsChipRoom() {
		return 0
	}
	_, ret := transaction.WriteMoneySyncWithModifyAmount(userId, gameId, amount, tax, status, scoreType, srcName, userIp)
	return ret
}

func (this *GateSink) sendBroadcast(userId int, userName string, score int, gameId int, gameName string) {
	go coreservice.SendGameWinBroadcast(userId, userName, score, gameId, gameName)
}

func RecvChannelData(data string) {
	//log.Debug("RecvChannelData data = %+v", data)
	var msg redis.Channel_msg
	err := json.Unmarshal([]byte(data), &msg)
	if err != nil {
		log.Release("RecvChannelData Unmarshal data failed %v", data)
		return
	}

	if msg.Message == "UsreNotification" {
		Sink.onUserNotification(msg.UserID)
	}
}

func (this *GateSink) OnNotification(userId int, data string) {
	var nt notification.Notification
	if err := json.Unmarshal([]byte(data), &nt); err != nil {
		log.Debug("GateSink.OnNotification unmarshal fail %v", err)
		return
	}
	notifications := []*notification.Notification{&nt}
	if userId == -1 {
		this.lock.RLock()
		defer this.lock.RUnlock()
		for _, v := range this.userlist {
			go func(u *user) {
				u.onNotification(notifications)
			}(v)
		}
		return
	}

	usr := this.getUserByUserId(userId)
	if usr == nil {
		return
	}
	usr.onNotification(notifications)
}
