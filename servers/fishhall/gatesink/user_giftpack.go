package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

func (this *user) getGiftPacks(msg string) {
	resp := client.GetGiftPacks()
	if resp.RetCode != 1 {
		log.Debug("user.getGiftPacks failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) getBuyableGiftPacks(msg string) {
	resp := client.GetBuyableGiftPacks(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.getBuyableGiftPacks failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) getGrowthPacks(msg string) {
	resp := client.GetGrowthGiftPacks()
	if resp.RetCode != 1 {
		log.Debug("user.getGrowthPacks failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) getGrowthPackTerms(msg string) {
	resp := client.GetUserGrowthTerms(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.getGrowthPackTerms failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) awardGrowthPack(msg string, data string) {
	var req protocol.GrowthPackAward_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("getGrowthPackAward Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.UserAwardTerm(this.getUserId(), req.GiftPackId, req.Index)
	if resp.RetCode != 1 {
		log.Debug("user.getGrowthPackAward failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}
