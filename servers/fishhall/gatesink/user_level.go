package gatesink

import (
	"bet24.com/log"
	userservices "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
)

func (this *user) getLevelList(msg, data string) {
	resp := userservices.GetLevelList()
	this.WriteMsg(msg, resp)
	return
}

func (this *user) loginAward(msg, data string) {
	var resp struct {
		RetCode bool
		Amount  int
	}
	resp.RetCode, resp.Amount = userservices.LoginAward(this.getUserId())
	if !resp.RetCode {
		log.Debug("user.loginAward failed %v", resp)
	}
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
	return
}
