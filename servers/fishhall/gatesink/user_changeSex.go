package gatesink

import (
	"bet24.com/log"
	//coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/insecureframe/gate"
	userservices "bet24.com/servers/micros/userservices/proto"
	"bet24.com/servers/transaction"
	"encoding/json"
	"fmt"
)

// 修改性别
func (this *user) changeSex(msg, data string) {
	obj := transaction.NewTransChangeSex()
	err := json.Unmarshal([]byte(data), &obj.IN)
	if err != nil {
		log.Release("changeSex UnMarshal data failed %v", data)
		this.WriteMsg(msg, fmt.Sprintf("changeSex UnMarshal data failed %v", data))
		return
	}

	info := gate.GetUserInfo(this.UserIndex)
	obj.IN.UserID = info.GetUserId()
	obj.DoAction(nil)
	buf, err := json.Marshal(obj.Out)
	if err != nil {
		log.Release("changeSex DoAction marshal fail %v", err)
		this.WriteMsg(msg, fmt.Sprintf("changeSex DoAction marshal fail %v", err))
		return
	}
	if obj.Out.RetCode == 1 {
		info.SetUserSex(obj.IN.Sex)
		info.SetUserFaceId(obj.Out.FaceID)

		// 刷新coreservice中的用户信息
		go userservices.UpdateUserInfo(this.getUserId())
	}
	this.WriteMsg(msg, string(buf))
}
