package gatesink

import (
	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/coupontask"
	"bet24.com/servers/fishhall/config"
	"bet24.com/servers/fishhall/protocol"
	"bet24.com/servers/insecureframe/gate"
	game "bet24.com/servers/micros/game/proto"
	cash "bet24.com/servers/micros/money/proto"
	chip "bet24.com/servers/micros/money/proto"
	notification "bet24.com/servers/micros/notification/proto"
	task "bet24.com/servers/micros/task/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"strconv"
	"time"
)

const SCROLL_SECOND = 10

func (this *user) selectNotification() {
	ticker := time.NewTicker(SCROLL_SECOND * time.Second)
	go func(t *time.Ticker) {
		for { //循环
			select {
			case <-this.chan_clear:
				t.Stop()
				return
			case <-t.C:
				this.getNotifications(false)
			}
		}
	}(ticker)
}

func (this *user) getNotifications(positive bool) {
	// 这里增加在线时长
	if !positive {
		go task.DoTaskAction(this.getUserId(), task.TaskAction_play_time, SCROLL_SECOND, task.TaskScope{})
	}

	if Sink.pollNotification {
		// 从数据库拉基本信息
		data := notification.GetNotifications(this.getUserId())
		if data == "" {
			return
		}

		var notifications []*notification.Notification
		if err := json.Unmarshal([]byte(data), &notifications); err != nil {
			log.Debug("user.getNotifications unmarshal fail %v", err)
			return
		}

		this.onNotification(notifications)
	}
}

func (this *user) onNotification(notifications []*notification.Notification) {
	//删除重复
	list := make(map[int]*notification.Notification)
	for i := len(notifications) - 1; i >= 0; i-- {
		v := notifications[i]

		if v.Id == notification.Notification_Match {
			this.onSimpleMatchNotification(v.Data)
			continue
		}

		if v.Id == notification.Notification_Task {
			var taskData task.UserTask
			json.Unmarshal([]byte(v.Data), &taskData)
			if taskData.TaskId == task.PreAddictionTaskId {
				notifications = append(notifications[:i], notifications[i+1:]...)
				continue
			}
		}

		// 广告大厅不发元宝通知
		if v.Id == notification.Notification_Chip && config.Server.IsChipRoom < 0 {
			notifications = append(notifications[:i], notifications[i+1:]...)
			continue
		}

		// 基础数值才需要去重(不包含广播)
		if v.Id <= notification.Notification_Invalid || v.Id == notification.Notification_Inventory /*|| v.Id == notification.Notification_Chat*/ {
			_, ok := list[v.Id]
			if ok {
				notifications = append(notifications[:i], notifications[i+1:]...)
				continue
			}
		}

		v.Data = this.handleNotification(v.Id, v.Data)
		list[v.Id] = v
	}

	d, _ := json.Marshal(notifications)
	this.WriteMsg("notification", string(d))
}

func (this *user) handleNotification(notificationId int, data string) string {
	switch notificationId {
	case notification.Notification_Gold:
		data = this.notification_gold(data)
	case notification.Notification_Chip:
		data = this.notification_chip(data)
	case notification.Notification_Vitality:
		data = this.notification_vitality(data)
	case notification.Notification_Task:
		// do nothing
	case notification.Notification_Vip:
		// do nothing
	case notification.Notification_Inventory:
		// do nothing
	case notification.Notification_Cannon:
		// do nothing
	case notification.Notification_Recharge:
		// 可能是购买重置礼包，尝试触发礼包升级任务
		this.notification_lockCountry()
	case notification.Notification_Broadcast:
		// do nothing
	case notification.Notification_Redpoint:
		// do nothing
	case notification.Notification_Chat:
		// do nothing
	case notification.Notification_Friend:
		// do nothing
	case notification.Notification_CouponTask:
		// log.Debug("handleNotification userId=%d task==>%+v", this.getUserId(), data)
		data = this.notification_coupon(data)
	case notification.Notification_Level:
		fallthrough
	case notification.Notification_Experience:
		data = this.notification_level(data)
	case notification.Notification_Return:
		// do nothing
	case notification.Notification_SlotScore:
		// do nothing
	case notification.Notification_UserInfoChanged:
		// 刷新自己的信息
		u := userservices.GetUserInfo(this.getUserId())
		user_self := gate.GetUserInfo(this.UserIndex)
		if u == nil || user_self == nil {
			break
		}
		user_self.SetUserFaceUrl(u.FaceUrl)
		user_self.SetDecorations(u.Decorations)
		user_self.SetBadges(u.Badges)
	default:
		log.Debug("handleNotification %d, unhandled id [%d]", this.getUserId(), notificationId)
		break
	}
	return data
}

func (this *user) notification_coupon(data string) string {
	var task struct {
		Action     int
		TodayCount int // 今天券数量
		Data       coupontask.UserTask
	}

	if err := json.Unmarshal([]byte(data), &task); err != nil {
		log.Error("user_notification.notification_coupon unmarshal ret err %v", err)
		return ""
	}

	// 开关模式
	if task.Action == 1 {
		return data
	}

	if info := game.GetGame(task.Data.GameId); info != nil {
		task.Data.ChineseName = info.ChineseName
	}

	buf, _ := json.Marshal(task)
	log.Debug("notification_coupon userId=%d task==>%+v", this.getUserId(), string(buf))
	return string(buf)
}

func (this *user) notification_gold(data string) string {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return ""
	}
	_, gold := cash.GetMoney(u.GetUserId())
	u.SetUserGold(gold)
	return strconv.Itoa(gold)
}

func (this *user) notification_level(data string) string {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return ""
	}
	info := userservices.GetUserLevel(u.GetUserId())
	u.SetUserLevel(info.Level)
	u.SetUserExperience(info.Experience)
	d, _ := json.Marshal(info)
	return string(d)
}

func (this *user) notification_chip(data string) string {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return ""
	}
	_, c := chip.GetUserChip(u.GetUserId())

	u.SetChip(c)
	return strconv.Itoa(c)
}

func (this *user) notification_vitality(data string) string {
	resp := coreservice.GetUserVitalityInfo(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.GetVitalityInfo failed %v", resp)
		return ""
	}

	var info protocol.GetVitality_resp
	if err := json.Unmarshal([]byte(resp.Data), &info); err != nil {
		log.Error("user.GetVitalityInfo fail userId=%d %+v", this.getUserId(), info)
		return ""
	}

	d, _ := json.Marshal(info)
	return string(d)
}

func (this *user) notification_lockCountry() {
	if this.getCurrencyIsModify() != 1 {
		return
	}

	// 锁定国家地区
	go func() {
		userservices.LockCountry(this.getUserId(), this.getCurrency())
		this.setCurrencyIsModify(0)
	}()
}
