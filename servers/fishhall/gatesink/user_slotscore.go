package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"fmt"
	"strconv"
)

func (this *user) getSlotScore(msg, data string) {
	userId := this.getUserId()
	if userId == 0 {
		return
	}
	score := client.GetSlotScore(userId)
	this.WriteMsg(msg, fmt.Sprintf("%d", score))
}

func (this *user) getSlotScoreExchange(msg, data string) {
	log.Debug("getSlotScoreExchange")
	userId := this.getUserId()
	if userId == 0 {
		log.Debug("getSlotScoreExchange userId == 0")
		this.WriteMsg(msg, "")
		return
	}
	list := client.GetSlotScoreExchangeList()
	log.Debug("getSlotScoreExchange list == %s", list)
	this.WriteMsg(msg, list)
}

func (this *user) slotScoreExchange(msg, data string) {
	userId := this.getUserId()
	if userId == 0 {
		return
	}
	productId, err := strconv.Atoi(data)
	if err != nil {
		log.Release("user.slotScoreExchange invalid argument [%d] %s", userId, data)
		return
	}
	ret := client.SlotScoreExchange(this.getUserId(), productId)
	retMsg := "success"
	if !ret {
		retMsg = "failed"
		log.Debug("user.slotScoreExchange failed [%d]", userId)
	}

	this.WriteMsg(msg, retMsg)
}

func (this *user) getSlotScoreExchangeHistory(msg, data string) {
	userId := this.getUserId()
	if userId == 0 {
		return
	}
	list := client.GetSlotScoreExchangeHistory(userId)
	this.WriteMsg(msg, list)
}
