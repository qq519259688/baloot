package gatesink

import (
	"bet24.com/log"
	setsmatch "bet24.com/servers/micros/matches/proto"
	"strconv"
)

func (this *user) onSetsMatchMsg(msg, data string) {
	switch msg {
	case "quitSetsMatch":
		this.quitSetsMatch(msg, data)
	case "getSetsMatchInfo":
		this.getSetsMatchInfo(msg, data)
	case "getMySetsMatches":
		this.getMySetsMatches(msg)
	case "getSetsMatchConfigs":
		this.getSetsMatchConfigs(msg)
	case "getEnrolledSetsMatch":
		this.getEnrolledSetsMatch(msg)
	case "getSetsMatchHistory":
		this.getSetsMatchHistory(msg)
	}
}

func (this *user) quitSetsMatch(msg, data string) {
	matchNo, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.quitSetsMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	ok := setsmatch.QuitSetsMatch(this.getUserId(), matchNo)
	retData := "success"
	if !ok {
		retData = "failed"
	}
	this.WriteMsg(msg, retData)
}

func (this *user) getSetsMatchInfo(msg, data string) {
	matchNo, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.getSetsMatchInfo %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}

	this.WriteMsg(msg, setsmatch.GetSetsMatchInfo(matchNo))
}

func (this *user) onSetsMatchNotification(data string) {
	//log.Release("onSetsMatchNotification [%d],%s", this.getUserId(), data)
	this.WriteMsg("onSetsMatchNotification", data)
}

func (this *user) getMySetsMatches(msg string) {
	this.WriteMsg(msg, setsmatch.GetUserSetsMatches(this.getUserId()))
}
func (this *user) getSetsMatchConfigs(msg string) {
	this.WriteMsg(msg, setsmatch.GetSetsMatchConfigs())
}

func (this *user) getEnrolledSetsMatch(msg string) {
	this.WriteMsg(msg, setsmatch.GetEnrolledSetsMatch(this.getUserId()))
}

func (this *user) getSetsMatchHistory(msg string) {
	this.WriteMsg(msg, setsmatch.GetSetsMatchHistory(this.getUserId()))
}
