package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

func (this *user) getExchangeList(msg, data string) {
	resp := client.GetExchangeList(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.getExchangeList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) exchangeGift(msg string, data string) {
	var req protocol.ExchangeGift_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("exchangeGift Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.UserExchange(this.getUserId(), req.ExchangeId, req.Num, req.Remark)
	if resp.RetCode != 1 {
		log.Debug("user.exchangeGift failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) getExchangeHistory(msg, data string) {
	var req protocol.ExchangeHistory_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("getExchangeHistory Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.GetExchangeHistory(this.getUserId(), req.PageIndex, req.PageSize)
	if resp.RetCode != 1 {
		log.Debug("user.getExchangeHistory failed %v", resp)
	}

	this.WriteMsg(msg, resp.Data)
	return
}
