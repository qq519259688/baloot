package gatesink

import (
	"bet24.com/log"
	activityservice "bet24.com/servers/micros/activityservice/proto"
)

// 处理新手福利指令
func (this *user) onDailyWheelMsg(msg, data string) {
	switch msg {
	case "getDailyWheelInfo":
		this.getDailyWheelInfo(msg)
	case "doDailyWheel": //
		this.doDailyWheel(msg)
	case "refreshDailyWheelTask": //
		this.refreshDailyWheelTask(msg)
	default:
		log.Release("user.onDailyWheelMsg unhandled msg %s", msg)
	}
}

func (this *user) getDailyWheelInfo(msg string) {
	this.WriteMsg(msg, activityservice.GetDailyWheelInfo(this.getUserId()))
	return
}

func (this *user) doDailyWheel(msg string) {
	this.WriteMsg(msg, activityservice.DoDailyWheel(this.getUserId()))
	return
}

func (this *user) refreshDailyWheelTask(msg string) {
	if activityservice.RefreshDailyWheelTask(this.getUserId()) {
		this.WriteMsg(msg, "ok")
	} else {
		this.WriteMsg(msg, "failed")
	}

	return
}
