package gatesink

import (
	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/common"
	"bet24.com/servers/fishhall/protocol"
	"bet24.com/servers/insecureframe/gate"
	item "bet24.com/servers/micros/item_inventory/proto"
	"bet24.com/servers/transaction"
	"encoding/json"
	"fmt"
	"strings"
)

func (this *user) getBindingInfo(msg string) {
	obj := transaction.NewTransBindingInfo()
	obj.In.UserID = this.getUserId()
	obj.DoAction()
	buf, _ := json.Marshal(obj.Out)
	this.WriteMsg(msg, string(buf))
	return
}

func (this *user) bindFacebook(msg, data string) {
	var req protocol.BindFacebook_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("%s unmarshal fail %v", msg, err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	obj := transaction.NewTransBindFacebook()

	// 删除头尾空格
	req.NickName = strings.TrimSpace(req.NickName)

	/*
		// 昵称长度：[4,30]
		nickNameRegexp := regexp.MustCompile(`^([A-Za-z0-9]+\s*){4,32}$`)

		// 昵称非法
		if NickNames := nickNameRegexp.FindAllString(req.NickName, -1); len(NickNames) <= 0 {
			log.Debug("bindFacebook NickName Illegality nickName=%s NickNames=%v", req.NickName, NickNames)
			obj.Out.RetCode = -1
			buf, err := json.Marshal(obj.Out)
			if err != nil {
				log.Release("bindFacebook Illegality marshal fail %v", err)
				this.WriteMsg(msg, fmt.Sprintf("bindFacebook Illegality marshal fail %v", err))
				return
			}

			this.WriteMsg(msg, string(buf))
			return
		}
	*/

	u := gate.GetUserInfo(this.UserIndex)
	obj.In.UserID = u.GetUserId()
	obj.In.OpenID = req.OpenId
	obj.In.NickName = req.NickName
	obj.In.FaceUrl = req.FaceUrl
	obj.DoAction()
	if obj.Out.RetCode == 1 {
		u.SetUserNickName(req.NickName)
		u.SetUserFaceId(0)
		u.SetUserFaceUrl(req.FaceUrl)

		item.AddItems(u.GetUserId(), obj.Out.Items, "BindFacebook", common.LOGTYPE_BIND_FACEBOOK)
	}

	buf, _ := json.Marshal(obj.Out)
	// log.Debug(string(buf))
	this.WriteMsg(msg, string(buf))
	return
}

func (this *user) bindPhone(msg, data string) {
	var req protocol.BindPhone_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("%s unmarshal fail %v", msg, err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	// user_tel中有短信验证码生成及验证
	// 去掉空格
	req.PhoneNumber = strings.ReplaceAll(req.PhoneNumber, " ", "")

	obj := transaction.NewTransBindTel()
	obj.In.UserID = this.getUserId()
	obj.In.SMSCode = req.SmsCode

	// 校验手机号
	validTel, err := public.CheckTelValid(req.PhoneNumber)
	if err != nil {
		log.Debug("bindPhone tel valid err:%v", err)
		obj.Out.RetCode = -3
		buf, err := json.Marshal(obj.Out)
		if err != nil {
			log.Release("bindPhone tel valid json marshal fail %v", err)
			return
		}
		this.WriteMsg(msg, string(buf))
		return
	}

	obj.In.Tel = validTel

	// 校验短信验证码
	isSuccess, _, _ := this.checkCode(obj.In.Tel, obj.In.SMSCode, SMSCODE_TYPE_Bind)
	if !isSuccess {
		log.Debug("bindPhone smscode is failed")
		obj.Out.RetCode = -4
		buf, err := json.Marshal(obj.Out)
		if err != nil {
			log.Release("bindPhone smscode is invalid json marshal fail %v", err)
			return
		}
		this.WriteMsg(msg, string(buf))
		return
	}

	// 数据库绑定
	obj.DoAction()

	// 绑定成功,给奖励
	if obj.Out.RetCode == 1 {
		obj.Out.Items = append(obj.Out.Items, item.ItemPack{
			ItemId: 1,
			Count:  2000,
		})

		item.AddItems(this.getUserId(), obj.Out.Items, "BindTel", common.LOGTYPE_BIND_TEL)
	}

	buf, err := json.Marshal(obj.Out)
	if err != nil {
		log.Error("bindPhone UserID=%d obj.Out=%+v err=%v", this.getUserId(), obj.Out, err)
	}

	this.WriteMsg(msg, string(buf))
	return
}
