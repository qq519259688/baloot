package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

func (this *user) onAgentMsg(msg, data string) {
	// 代理体系
	switch msg {
	case "agentConfigs": // 代理配置信息
		this.agentConfigs(msg, data)
	case "agentApply": // 代理申请
		this.agentApply(msg, data)
	case "agentBind": // 代理绑码
		this.agentBind(msg, data)
	case "agentInfo": // 代理信息
		this.agentInfo(msg, data)
	case "agentMembers": // 会员
		this.agentMembers(msg, data)
	case "agentStat": // 代理统计
		this.agentStat(msg, data)
	case "agentCommissionLog": // 收益记录
		this.agentCommissionLog(msg, data)
	case "agentCommissionToAmount": // 提取收益
		this.agentCommissionToAmount(msg, data)
	case "agentGetMyGroup": // 获取代理自己群组
		this.agentGetMyGroup(msg, data)
	case "agentGetGroup": // 获取会员上级群组
		this.agentGetGroup(msg, data)
	case "agentUpdateGroup": // 修改代理群组
		this.agentUpdateGroup(msg, data)
	default:
		log.Release("user.onAgentMsg unhandled msg %s", msg)
	}
}

// 代理配置
func (this *user) agentConfigs(msg, data string) {
	resp := client.AgentConfigs()
	if resp.RetCode != 1 {
		log.Debug("user.agentConfigs failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 代理申请
func (this *user) agentApply(msg, data string) {
	var req protocol.AgentApply
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("agentApply unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.AgentApply(this.getUserId(), req.Memo)
	if resp.RetCode != 1 {
		log.Debug("user.agentApply failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 代理绑码
func (this *user) agentBind(msg, data string) {
	var req protocol.AgentBind
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("agentBind unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.AgentBind(this.getUserId(), req.Code, this.GetIP())
	if resp.RetCode != 1 {
		log.Debug("user.agentBind failed %v", resp)
	}

	var respData struct {
		RetCode int
		Items   interface{}
	}
	if err := json.Unmarshal([]byte(resp.Data), &respData); err != nil {
		this.WriteMsg(msg, resp.Data)
		return
	}

	// 绑码成功，更新存储
	if respData.RetCode == 1 {
		go this.setHigherUserId(req.Code)
		go this.setUTMSource("agentinvite")
	}

	this.WriteMsg(msg, resp.Data)
	return
}

// 代理信息
func (this *user) agentInfo(msg, data string) {
	resp := client.AgentInfo(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.agentInfo failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 会员
func (this *user) agentMembers(msg, data string) {
	var req protocol.Agent
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("agentMembers unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.AgentMembers(this.getUserId(), req.PageIndex, req.PageSize)
	if resp.RetCode != 1 {
		log.Debug("user.agentMembers failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 代理统计
func (this *user) agentStat(msg, data string) {
	var req protocol.Agent
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("agentStat unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.AgentStat(this.getUserId(), req.PageIndex, req.PageSize)
	if resp.RetCode != 1 {
		log.Debug("user.agentStat failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 收益记录
func (this *user) agentCommissionLog(msg, data string) {
	var req protocol.Agent
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("agentCommissionLog unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.AgentCommissionLog(this.getUserId(), req.FromUserId, req.PageIndex, req.PageSize)
	if resp.RetCode != 1 {
		log.Debug("user.agentCommissionLog failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 提取收益
func (this *user) agentCommissionToAmount(msg, data string) {
	resp := client.AgentCommissionToAmount(this.getUserId(), this.GetIP())
	if resp.RetCode != 1 {
		log.Debug("user.agentCommissionToAmount failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 获取代理群组
func (this *user) agentGetGroup(msg, data string) {
	resp := client.AgentGetGroup(this.getHigherUserId())
	if resp.RetCode != 1 {
		log.Debug("user.agentGroup failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 获取代理自己群组
func (this *user) agentGetMyGroup(msg, data string) {
	resp := client.AgentGetGroup(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.agentGroup failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 修改代理群组
func (this *user) agentUpdateGroup(msg, data string) {
	var req protocol.AgentGroup
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("agentUpdateGroup unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.AgentUpdateGroup(this.getUserId(), req.Id, req.Name, req.Url)
	if resp.RetCode != 1 {
		log.Debug("user.agentUpdateGroup failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}
