package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/fishhall/config"
	simplematch "bet24.com/servers/micros/matches/proto"
	"encoding/json"
	"fmt"
	"strconv"
)

func (this *user) onSimpleMatchMsg(msg, data string) {
	switch msg {
	case "createSimpleMatch":
		this.createSimpleMatch(msg, data)
	case "closeSimpleMatch":
		this.closeSimpleMatch(msg, data)
	case "enrollSimpleMatch":
		this.enrollSimpleMatch(msg, data)
	case "quitSimpleMatch":
		this.quitSimpleMatch(msg, data)
	case "getSimpleMatchInfo":
		this.getSimpleMatchInfo(msg, data)
	case "getMySimpleMatches":
		this.getMySimpleMatches(msg)
	case "getSimpleMatchConfigs":
		this.getSimpleMatchConfigs(msg)
	case "getEnrolledSimpleMatch":
		this.getEnrolledSimpleMatch(msg)
	case "getSimpleMatchHistory":
		this.getSimpleMatchHistory(msg)
	}
}

type createMatchInfo struct {
	GameId    int    // 游戏ID
	GameRule  string // 规则名
	Target    int    // 结束目标，hand为局数，domino为胜利分数
	TotalUser int    // 比赛总报名人数
	TableUser int    // 每桌人数
	Fee       int    // 报名费
	Prize     int    // 奖金
	PlayTime  int
}

func (this *user) createSimpleMatch(msg, data string) {
	var req createMatchInfo
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user.createPrivateRoom unmarshal data fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	if this.isGuest() && config.HallConfig.GuestMatchClose > 0 {
		this.WriteMsg(msg, "not for guest users")
		return
	}

	roomNo, errMsg := simplematch.CreateSimpleMatch(this.getUserId(), req.GameId, req.GameRule, req.TotalUser,
		req.Target, req.TableUser, req.Fee, req.Prize, req.PlayTime)
	this.WriteMsg(msg, fmt.Sprintf(`{"RoomNo":%d,"ErrMsg":"%s"}`, roomNo, errMsg))
}

func (this *user) closeSimpleMatch(msg, data string) {
	matchNo, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.closeSimpleMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	retCode, errMsg := simplematch.CloseSimpleMatch(this.getUserId(), matchNo)
	this.WriteMsg(msg, fmt.Sprintf(`{"RetCode":%d,"ErrMsg":"%s"}`, retCode, errMsg))
}

func (this *user) enrollSimpleMatch(msg, data string) {
	matchNo, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.enrollSimpleMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}

	if this.isGuest() && config.HallConfig.GuestMatchClose > 0 {
		this.WriteMsg(msg, "not for guest users")
		return
	}

	var retInfo struct {
		MatchNo int
		ErrMsg  string
	}
	retInfo.MatchNo, retInfo.ErrMsg = simplematch.EnrollSimpleMatch(this.getUserId(), matchNo,
		this.getNickName(), this.getFaceId(), this.getFaceUrl())
	d, _ := json.Marshal(retInfo)
	this.WriteMsg(msg, string(d))
}

func (this *user) quitSimpleMatch(msg, data string) {
	matchNo, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.quitSimpleMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	ok := simplematch.QuitSimpleMatch(this.getUserId(), matchNo)
	retData := "success"
	if !ok {
		retData = "failed"
	}
	this.WriteMsg(msg, retData)
}

func (this *user) getSimpleMatchInfo(msg, data string) {
	matchNo, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.getSimpleMatchInfo %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}

	if matchNo <= 0 {
		return
	}
	this.WriteMsg(msg, simplematch.GetSimpleMatchInfo(matchNo))
}

func (this *user) onSimpleMatchNotification(data string) {
	//log.Release("onSimpleMatchNotification [%d],%s", this.getUserId(), data)
	this.WriteMsg("onSimpleMatchNotification", data)
}

func (this *user) getMySimpleMatches(msg string) {
	this.WriteMsg(msg, simplematch.GetUserSimpleMatches(this.getUserId()))
}
func (this *user) getSimpleMatchConfigs(msg string) {
	this.WriteMsg(msg, simplematch.GetSimpleMatchConfigs())
}

func (this *user) getEnrolledSimpleMatch(msg string) {
	this.WriteMsg(msg, simplematch.GetEnrolledSimpleMatch(this.getUserId()))
}

func (this *user) getSimpleMatchHistory(msg string) {
	this.WriteMsg(msg, simplematch.GetSimpleMatchHistory(this.getUserId()))
}
