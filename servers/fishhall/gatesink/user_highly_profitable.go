package gatesink

import (
	"bet24.com/log"
	highlyprofitable "bet24.com/servers/micros/highly_profitable/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	"encoding/json"
)

// 处理一本万利指令
func (this *user) onHighlyProfitableMsg(msg, data string) {
	switch msg {
	case "getHighlyProfitableActivity": // 获取一本万利活动
		this.getHighlyProfitableActivity(msg)
	case "claimHighlyProfitableAward": // 领取一本万利奖励
		this.claimHighlyProfitableAward(msg, data)
	default:
		log.Release("user.onHighlyProfitableMsg unhandled msg %s", msg)
	}
}

// 获取一本万利活动
func (this *user) getHighlyProfitableActivity(msg string) {
	resp := highlyprofitable.GetHighlyProfitableActivity(this.getUserId())
	d, _ := json.Marshal(resp)
	log.Debug("user.getHighlyProfitableActivity returns %s", string(d))
	this.WriteMsg(msg, string(d))
	return
}

// 领取一本万利奖励
func (this *user) claimHighlyProfitableAward(msg, data string) {
	var ret struct {
		Success bool
		Data    []item.ItemPack
	}

	ret.Success, ret.Data = highlyprofitable.ClaimHighlyProfitableAward(this.getUserId())
	d, _ := json.Marshal(ret)
	this.WriteMsg(msg, string(d))
	return
}
