package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/fishhall/protocol"
	userservices "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"fmt"
)

func (this *user) saveCountry(msg, data string) {
	retData := ""
	var req protocol.SaveCountry_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user.saveCountry unmarshal data fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := userservices.SaveCountry(this.getUserId(), req.CountryName, req.Currency)
	if resp != "1" {
		log.Debug("user.saveCountry failed %v", resp)
	} else {
		// 保存
		go this.setCurrency(req.Currency)
	}

	this.WriteMsg(msg, resp)
	return
}
