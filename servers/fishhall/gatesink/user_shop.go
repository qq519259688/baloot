package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

func (this *user) getExchangeRateList(msg, data string) {
	this.WriteMsg(msg, "")
}

func (this *user) getShopList(msg, data string) {
	retData := ""
	var req protocol.GetShopList_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("getShopList Unmarshal data failed %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.GetShopListByUserId(req.ShopType, this.getUserId(), this.getPartnerId())
	if resp.RetCode != 1 {
		log.Debug("user.getShopList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) exchangeGold(msg, data string) {
	log.Debug("user.exchangeGold %d %s", this.getUserId(), data)
	resp := client.Exchange(this.getUserId(), data)
	if resp.RetCode != 1 {
		log.Debug("user.Exchange failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) exchangeGoldInBulk(msg, data string) {
	log.Debug("user.exchangeGoldInBulk %d %s", this.getUserId(), data)
	var productIds []string
	if err := json.Unmarshal([]byte(data), &productIds); err != nil {
		retData := fmt.Sprintf("exchangeGoldInBulk Unmarshal data failed %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.ExchangeInBulk(this.getUserId(), productIds)
	if resp.RetCode != 1 {
		log.Debug("user.ExchangeInBulk failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) getLocalPrice(msg, data string) {
	var req struct {
		Price float64
	}
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		log.Error("getLocalPrice Unmarshal data failed %v", err)
		//this.WriteMsg(msg, retData)
		return
	}
	this.WriteMsg(msg, fmt.Sprintf("%02f%s", req.Price, this.getCurrency()))
}
