package gatesink

import (
	"bet24.com/log"
	slotsservice "bet24.com/servers/micros/slotsservice/proto"
	"encoding/json"
)

// 处理新手福利指令
func (this *user) onSlotsServiceMsg(msg, data string) {
	switch msg {
	case "getSlotsServiceConfig": // 获取系统新手福利
		this.getSlotsServiceConfig(msg, data)
	case "doSlotsServiceSpin": // 获取用户新手福利
		this.doSlotsServiceSpin(msg, data)
	default:
		log.Release("user.onSlotsServiceMsg unhandled msg %s", msg)
	}
}

func (this *user) getSlotsServiceConfig(msg, data string) {
	this.WriteMsg(msg, slotsservice.GetSlotsConfig(data, this.getUserId()))
}

func (this *user) doSlotsServiceSpin(msg, data string) {
	var spinData slotsservice.Request
	err := json.Unmarshal([]byte(data), &spinData)
	if err != nil {
		this.WriteMsg(msg, "invalid argument")
		return
	}
	_, _, result := slotsservice.Spin(spinData.GameName, this.getUserId(), spinData.Amount, spinData.IsChip, spinData.Extra)
	this.WriteMsg(msg, result)
}
