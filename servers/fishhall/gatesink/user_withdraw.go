package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"

	"bet24.com/servers/transaction"
)

// 提现信息(提现前调用)
func (this *user) withdrawInfo(msg string) {
	obj := transaction.NewLiuWithdrawInfo()
	obj.In.UserID = this.getUserId()
	obj.DoAction()
	buf, err := json.Marshal(obj.Out)
	if err != nil {
		retData := fmt.Sprintf("withdrawInfo DoAction marshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	this.WriteMsg(msg, string(buf))
}
