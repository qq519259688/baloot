package gatesink

import (
	signin "bet24.com/servers/micros/activityservice/proto"
)

func (this *user) onUserSignInMsg(msg, data string) {
	switch msg {
	case "getUserSignInInfo":
		this.getUserSignInInfo(msg, data)
	case "doUserSignIn":
		this.doUserSignIn(msg, data)
		/*case "UserSignInContinueAward":
		this.UserSignInContinueAward(msg, data)
		*/
	}
}

func (this *user) getUserSignInInfo(msg, data string) {
	this.WriteMsg(msg, signin.GetUserSigninInfo(this.getUserId()))
}

func (this *user) doUserSignIn(msg, data string) {
	this.WriteMsg(msg, signin.DoUserSignin(this.getUserId()))
}

/*

func (this *user) UserSignInContinueAward(msg, data string) {
	retData := ""
	var req protocol.GiftContinueAward_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("UserSignInContinueAward Unmarshal data failed %v", data)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.GiftContinueAward(this.getUserId(), req.Day)
	if resp.RetCode != 1 {
		log.Debug("user.UserSignInContinueAward failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}
*/
