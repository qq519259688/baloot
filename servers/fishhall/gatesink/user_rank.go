package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

// 获取排行榜列表
func (this *user) getRankList(msg, data string) {
	retData := ""
	var req protocol.RankList_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user.getRankList unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.GetRankList(this.getUserId(), req.RankType, req.Num)
	if resp.RetCode != 1 {
		log.Debug("user.getRankList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 榜单历史
func (this *user) rankHistoryList(msg, data string) {
	retData := ""
	var req protocol.RankHistoryList_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user.rankHistoryList unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.RankHistoryList(this.getUserId(), req.DateFlag)
	if resp.RetCode != 1 {
		log.Debug("user.rankHistoryList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 榜单奖励
func (this *user) rankAward(msg, data string) {
	retData := ""
	var req protocol.RankAward_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user.rankAward unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.RankAward(this.getUserId(), req.RankType, req.DoubleFlag)
	if resp.RetCode != 1 {
		log.Debug("user.rankAward failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 榜单奖励列表
func (this *user) getRankAwardList(msg, data string) {
	resp := client.GetRankAwardList()
	if resp.RetCode != 1 {
		log.Debug("user.getRankAwardList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}
