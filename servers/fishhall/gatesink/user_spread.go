package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

//会员申请
func (this *user) spreadApply(msg, data string) {
	var req protocol.SpreadApply_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("spreadApply unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.SpreadApply(this.getUserId(), req.Code)
	if resp.RetCode != 1 {
		log.Debug("user.spreadApply failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

//会员列表
func (this *user) spreadMembers(msg, data string) {
	var req protocol.SpreadMember_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("spreadMembers unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.SpreadMembers(this.getUserId(), req.PageIndex, req.PageSize)
	this.WriteMsg(msg, resp.Data)
	return
}

//领取奖励
func (this *user) spreadGift(msg, data string) {
	var req protocol.SpreadGift_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("spreadGift unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.SpreadGift(this.getUserId(), req.FromUserID)
	if resp.RetCode != 1 {
		log.Debug("user.spreadGift failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}
