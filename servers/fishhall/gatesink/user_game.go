package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/fishhall/config"
	game "bet24.com/servers/micros/game/proto"
	"bet24.com/servers/transaction"
	"encoding/json"
	"fmt"
)

// 游戏列表
func (this *user) getGameList(msg, data string) {
	retData := game.GetGameJson(this.getUserId(), this.GetIP(), this.getPartnerId(), this.getVersionCode())
	this.WriteMsg(msg, retData)
}

// 审核游戏名
func (this *user) gameRequest(msg, data string) {
	var obj transaction.GameRequestInfo
	err := json.Unmarshal([]byte(data), &obj)
	if err != nil {
		log.Release("gameRequest Unmarshal data failed %v", data)
		this.WriteMsg(msg, "")
		return
	}

	requestGame := game.GetReviewGame(this.GetIP(), obj.PartnerID, obj.VersionCode)
	this.WriteMsg(msg, requestGame)
	return
}

func (this *user) sendLoginConfig() {
	this.WriteMsg("loginconfig", fmt.Sprintf("%d", config.HallConfig.LoginConfig))
}
