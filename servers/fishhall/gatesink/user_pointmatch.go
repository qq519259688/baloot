package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/fishhall/config"
	pointmatch "bet24.com/servers/micros/matches/proto"
	"encoding/json"
	"fmt"
	"strconv"
)

func (this *user) onPointMatchMsg(msg, data string) {
	switch msg {
	case "createPointMatch":
		this.createPointMatch(msg, data)
	case "closePointMatch":
		this.closePointMatch(msg, data)
	case "enrollPointMatch":
		this.enrollPointMatch(msg, data)
	case "quitPointMatch":
		this.quitPointMatch(msg, data)
	case "getPointMatchInfo":
		this.getPointMatchInfo(msg, data)
	case "getMyPointMatches":
		this.getMyPointMatches(msg)
	case "getPointMatchConfigs":
		this.getPointMatchConfigs(msg)
	case "getEnrolledPointMatch":
		this.getEnrolledPointMatch(msg)
	case "getPointMatchHistory":
		this.getPointMatchHistory(msg)
	}
}

type createPointMatchInfo struct {
	GameId         int    // 游戏ID
	GameRule       string // 规则名
	TotalUser      int    // 比赛总报名人数
	TableUser      int    // 每桌人数
	Fee            int    // 报名费
	Prize          int    // 奖金
	PlayTime       int
	EliminateScore int
	ShrinkSec      int
	ShrinkScore    int
	WinnerCount    int
}

func (this *user) createPointMatch(msg, data string) {
	var req createPointMatchInfo
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user.createPrivateRoom unmarshal data fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	if this.isGuest() && config.HallConfig.GuestMatchClose > 0 {
		this.WriteMsg(msg, "not for guest users")
		return
	}

	roomNo, errMsg := pointmatch.CreatePointMatch(this.getUserId(), req.GameId, req.GameRule, req.TotalUser,
		req.TableUser, req.Fee, req.Prize, req.PlayTime, req.EliminateScore, req.ShrinkSec, req.ShrinkScore, req.WinnerCount)
	this.WriteMsg(msg, fmt.Sprintf(`{"RoomNo":%d,"ErrMsg":"%s"}`, roomNo, errMsg))
}

func (this *user) closePointMatch(msg, data string) {
	matchNo, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.closePointMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	retCode, errMsg := pointmatch.ClosePointMatch(this.getUserId(), matchNo)
	this.WriteMsg(msg, fmt.Sprintf(`{"RetCode":%d,"ErrMsg":"%s"}`, retCode, errMsg))
}

func (this *user) enrollPointMatch(msg, data string) {
	matchNo, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.enrollPointMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}

	if this.isGuest() && config.HallConfig.GuestMatchClose > 0 {
		this.WriteMsg(msg, "not for guest users")
		return
	}

	var retInfo struct {
		MatchNo int
		ErrMsg  string
	}
	retInfo.MatchNo, retInfo.ErrMsg = pointmatch.EnrollPointMatch(this.getUserId(), matchNo,
		this.getNickName(), this.getFaceId(), this.getFaceUrl())
	d, _ := json.Marshal(retInfo)
	this.WriteMsg(msg, string(d))
}

func (this *user) quitPointMatch(msg, data string) {
	matchNo, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.quitPointMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	ok := pointmatch.QuitPointMatch(this.getUserId(), matchNo)
	retData := "success"
	if !ok {
		retData = "failed"
	}
	this.WriteMsg(msg, retData)
}

func (this *user) getPointMatchInfo(msg, data string) {
	matchNo, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.getPointMatchInfo %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}

	this.WriteMsg(msg, pointmatch.GetPointMatchInfo(matchNo))
}

func (this *user) onPointMatchNotification(data string) {
	//log.Release("onPointMatchNotification [%d],%s", this.getUserId(), data)
	this.WriteMsg("onPointMatchNotification", data)
}

func (this *user) getMyPointMatches(msg string) {
	this.WriteMsg(msg, pointmatch.GetUserPointMatches(this.getUserId()))
}
func (this *user) getPointMatchConfigs(msg string) {
	this.WriteMsg(msg, pointmatch.GetPointMatchConfigs())
}

func (this *user) getEnrolledPointMatch(msg string) {
	this.WriteMsg(msg, pointmatch.GetEnrolledPointMatch(this.getUserId()))
}

func (this *user) getPointMatchHistory(msg string) {
	this.WriteMsg(msg, pointmatch.GetPointMatchHistory(this.getUserId()))
}
