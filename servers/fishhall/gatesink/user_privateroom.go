package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/fishhall/config"
	client "bet24.com/servers/micros/privateroom/proto"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

func (this *user) onPrivateRoomMsg(msg, data string) {
	switch msg {
	case "getPrivateRoomRules":
		this.getPrivateRoomRules(msg, data)
	case "getPrivateRoomInfo":
		this.getPrivateRoomInfo(msg, data)
	case "createPrivateRoom":
		this.createPrivateRoom(msg, data)
	case "getMyPrivateRooms":
		this.getMyPrivateRooms(msg, data)
	case "enterPrivateRoom":
		this.enterPrivateRoom(msg, data)
	case "leavePrivateRoom":
		this.leavePrivateRoom(msg, data)
	case "getPrivateRoomHistory":
		this.getPrivateRoomHistory(msg)
	case "getPlayingPrivateRoom":
		this.getPlayingPrivateRoom(msg)
	case "getPrivateRoomsByGameId":
		this.getPrivateRoomsByGameId(msg, data)
	case "subscribePrivateRoomStatus":
		this.subscribePrivateRoomStatus()
	case "desubscribePrivateRoomStatus":
		this.desubscribePrivateRoomStatus()
	case "getPrivateRoomCallList":
		this.getPrivateRoomCallList(msg)
	case "PrivateRoomCallForUser":
		this.privateRoomCallForUser(msg, data)
	}
}

func (this *user) enterPrivateRoom(msg, data string) {
	// 这里可能包含chairId
	slice := strings.Split(data, ",")
	roomNo, err := strconv.Atoi(slice[0])
	if err != nil {
		log.Release("gatesink.enterPrivateRoom %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	chairId := -1
	if len(slice) > 1 {
		chairId, err = strconv.Atoi(slice[1])
		if err != nil {
			chairId = -1
		}
	}

	if this.isGuest() && config.HallConfig.GuestPriveRoomClose > 0 {
		this.WriteMsg(msg, "not for guest users")
		return
	}

	this.WriteMsg(msg,
		client.UserRequestSit(roomNo,
			this.getUserId(),
			this.getNickName(),
			this.getFaceId(),
			this.getFaceUrl(),
			chairId, 0, 0, 0))
}

func (this *user) leavePrivateRoom(msg, data string) {
	roomNo, err := strconv.Atoi(data)
	if err != nil {
		log.Debug("gatesink.leavePrivateRoom %v %s", err, data)
		return
	}
	client.UserLeave(roomNo, this.getUserId())
}

func (this *user) getMyPrivateRooms(msg, data string) {
	this.WriteMsg(msg, client.GetUserRooms(this.getUserId()))
}

type createInfo struct {
	GameId    int    // 游戏ID
	GameRule  string // 规则名
	Target    int    // 结束目标，hand为局数，domino为胜利分数
	UserCount int    // 游戏人数
	PlayTime  int
	Fee       int // 报名费
	Prize     int // 奖金
	IsPublic  bool
	ExtraInfo string
}

func (this *user) createPrivateRoom(msg, data string) {
	var req createInfo
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user.createPrivateRoom unmarshal data fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	if this.isGuest() && config.HallConfig.GuestPriveRoomClose > 0 {
		this.WriteMsg(msg, "not for guest users")
		return
	}

	roomNo, errMsg := client.CreatePrivateRoomByUser(this.getUserId(),
		req.GameId, req.GameRule, req.Target, req.UserCount, req.Fee, req.Prize,
		client.RoomType_Normal, req.PlayTime, req.IsPublic, req.ExtraInfo)
	this.WriteMsg(msg, fmt.Sprintf(`{"RoomNo":%d,"ErrMsg":"%s"}`, roomNo, errMsg))
}

func (this *user) getPrivateRoomRules(msg, data string) {
	gameId, err := strconv.Atoi(data)
	if err != nil {
		log.Debug("gatesink.getPrivateRoomRules %v", err)
		gameId = 0
	}
	rules := client.GetPrivateRoomRules(gameId)
	if rules == "" {
		log.Debug("user.getPrivateRoomRules failed %v", rules)
	}
	this.WriteMsg(msg, rules)
}

func (this *user) getPrivateRoomInfo(msg, data string) {
	roomNo, err := strconv.Atoi(data)
	if err != nil {
		log.Debug("gatesink.getPrivateRoomInfo %v", err)
		return
	}
	roomdata := client.GetPrivateRoomInfo(roomNo)
	if roomdata == "" {
		log.Debug("user.getPrivateRoomInfo failed %v", roomdata)
	}
	this.WriteMsg(msg, roomdata)
}

func (this *user) getPrivateRoomHistory(msg string) {
	roomdata := client.GetHistory(this.getUserId())
	if roomdata == "" {
		log.Debug("user.getPrivateRoomHistory failed %v", roomdata)
	}
	this.WriteMsg(msg, roomdata)
}

func (this *user) getPlayingPrivateRoom(msg string) {
	this.WriteMsg(msg, client.GetPlayingRoomNo(this.getUserId()))
}

func (this *user) getPrivateRoomsByGameId(msg, data string) {
	gameId, err := strconv.Atoi(data)
	if err != nil {
		log.Debug("gatesink.getPrivateRoomsByGameId %v", err)
		gameId = 0
	}
	rooms := client.GetPrivateRoomsByGameId(gameId, this.getUserId())
	if rooms == "" {
		log.Debug("user.getPrivateRoomsByGameId failed %v", rooms)
	}
	this.WriteMsg(msg, rooms)
}

func (this *user) subscribePrivateRoomStatus() {
	client.UserSubscribeRoomStatus(this.getUserId())
}

func (this *user) desubscribePrivateRoomStatus() {
	client.UserDesubscribeRoomStatus(this.getUserId())
}

func (this *user) getPrivateRoomCallList(msg string) {
	this.WriteMsg(msg, client.GetCallList())
}

func (this *user) privateRoomCallForUser(msg, data string) {
	roomNo, err := strconv.Atoi(data)
	if err != nil {
		log.Debug("gatesink.privateRoomCallForUser %v", err)
		return
	}
	client.PrivateRoomCall(this.getUserId(), roomNo)
}
