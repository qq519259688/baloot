package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
	"bet24.com/servers/insecureframe/gate"
)

func (this *user) getPrizeWheelConfig(msg string) {
	resp := client.GetWheelPrizes()
	if resp.RetCode != 1 {
		log.Debug("user.getPrizeWheelConfig failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) doWheel(msg, data string) {
	retData := ""
	var req protocol.PrizeWheel_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("doWheel Unmarshal data failed %v", data)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	u := gate.GetUserInfo(this.UserIndex)

	resp := client.DoWheel(u.GetUserId(), req.Times, u.GetUserNickName(), this.GetIP())
	if resp.RetCode != 1 {
		log.Debug("user.doWheel failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) getWheelTimes(msg, data string) {
	resp := client.GetWheelTimes(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.getWheelTimes userId=%d failed %v", this.getUserId(), resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) getPrizeRecord(msg, data string) {
	resp := client.GetPrizeRecord()
	this.WriteMsg(msg, resp.Data)
}

func (this *user) getJackpotAmount(msg string) {
	Amount := client.GetJackpotAmount(0, Sink.IsChipRoom())
	d, _ := json.Marshal(Amount)
	this.WriteMsg(msg, string(d))
}
