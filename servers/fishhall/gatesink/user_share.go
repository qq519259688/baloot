package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	task "bet24.com/servers/micros/task/proto"
	"bet24.com/servers/transaction"
	"encoding/json"
	"fmt"
)

func (this *user) rewardShareItems(msg string) {
	items := transaction.GetShareItems(this.getUserId(), 1)
	if len(items) == 0 {
		this.WriteMsg(msg, "领取失败，本日已领取")
		return
	}

	item.AddItems(this.getUserId(), items, "分享领取", common.LOGTYPE_SHARE)
	this.WriteMsg(msg, "领取成功")
}

func (this *user) getShareItems(msg string) {
	items := transaction.GetShareItems(this.getUserId(), 0)
	d, _ := json.Marshal(items)
	this.WriteMsg(msg, string(d))
}

func (this *user) doSharePlatform() {
	task.DoTaskAction(this.getUserId(), task.TaskAction_shareplatform, 1, task.TaskScope{})
}

func (this *user) doShareGame() {
	task.DoTaskAction(this.getUserId(), task.TaskAction_sharegame, 1, task.TaskScope{})
}

func (this *user) finishNoviceGuidance(data string) {
	if data == "skip" {
		task.RemoveTaskByAction(this.getUserId(), task.TaskAction_novice_guidance, "")
	} else {
		param := task.TaskScope{RankCrdate: common.GetNowTimeStr()}
		task.DoTaskAction(this.getUserId(), task.TaskAction_novice_guidance, 1, param)
	}
}

func (this *user) isShowNoviceGuidance(msg string, data string) {
	var ret struct {
		Show bool
	}
	ret.Show = task.IsActionTaskActive(this.getUserId(), task.TaskAction_novice_guidance, data)
	d, _ := json.Marshal(ret)
	this.WriteMsg(msg, string(d))
}

func (this *user) finishGameNoviceGuidance(msg, data string) {
	var req struct {
		IsSkip   bool
		GameName string
	}
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("finishGameNoviceGuidance unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	if req.IsSkip {
		task.RemoveTaskByAction(this.getUserId(), task.TaskAction_novice_guidance, req.GameName)
	} else {
		param := task.TaskScope{RankCrdate: common.GetNowTimeStr(), GameName: req.GameName}
		_, taskId := task.DoTaskAction(this.getUserId(), task.TaskAction_novice_guidance, 1, param)
		if taskId > 0 {
			ret := task.AwardTaskWithItems(this.getUserId(), taskId)
			this.WriteMsg(msg, ret)
			return
		}
	}

	this.WriteMsg(msg, "")
}
