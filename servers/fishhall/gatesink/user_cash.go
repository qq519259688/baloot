package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/fishhall/protocol"
	"bet24.com/servers/insecureframe/gate"
	cash "bet24.com/servers/micros/money/proto"
)

// 金币日志
func (this *user) cashLog(msg, data string) {
	retData := ""
	var req protocol.CashList_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("cashLog unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	// 查询时间段
	if len(req.BeginTime) <= 0 || len(req.EndTime) <= 0 {
		req.BeginTime = common.GetNowTime().Format(common.Layout)
		req.EndTime = common.GetNowTime().Format(common.Layout)
	}

	// 页索引
	if req.PageIndex <= 0 {
		req.PageIndex = 1
	}

	// 每页显示50条记录
	if req.PageSize <= 0 {
		req.PageSize = 50
	}

	recordCount, logs := cash.CashLog(this.getUserId(), req.BeginTime, req.EndTime, req.PageIndex, req.PageSize)
	d, _ := json.Marshal(struct {
		RecordCount int
		List        []*cash.CashInfo
	}{
		RecordCount: recordCount,
		List:        logs,
	})

	this.WriteMsg(msg, string(d))
}

// 保险柜存款
func (this *user) bankIn(msg, data string) {
	var req protocol.BankInOrOut_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("BankInAndOut_req Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	var resp protocol.BankInOrOut_resp

	resp.RetCode, resp.Gold, resp.BankAmount, resp.OutMsg = cash.BankIn(this.getUserId(), req.Amount, req.GameID, "", "")
	if resp.RetCode != 1 {
		log.Debug("user.bankIn failed %v", resp)
	}

	//刷新金币
	u := gate.GetUserInfo(this.UserIndex)
	u.SetUserGold(resp.Gold)
	u.SetUserBankamount(resp.BankAmount)
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
}

// 保险柜取款
func (this *user) bankOut(msg, data string) {
	var req protocol.BankInOrOut_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("BankInAndOut_req Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	var resp protocol.BankInOrOut_resp

	resp.RetCode, resp.Gold, resp.BankAmount, resp.OutMsg = cash.BankOut(this.getUserId(), req.Amount, req.GameID, "", "")
	if resp.RetCode != 1 {
		log.Debug("user.bankOut failed %v", resp)
	}

	//刷新金币
	u := gate.GetUserInfo(this.UserIndex)
	u.SetUserGold(resp.Gold)
	u.SetUserBankamount(resp.BankAmount)
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
}

// 保险柜查询
func (this *user) bankQuery(msg, data string) {
	var resp protocol.BankInOrOut_resp
	resp.RetCode, resp.BankAmount = cash.BankQuery(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.bankQuery failed %v", resp)
	}
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
}

func (this *user) financeLog(msg, data string) {
	retData := ""
	var req protocol.CashList_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("financeLog unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	// 页索引
	if req.PageIndex <= 0 {
		req.PageIndex = 1
	}

	// 每页显示50条记录
	if req.PageSize <= 0 {
		req.PageSize = 50
	}

	recordCount, list := cash.FinanceLog(this.getUserId(), req.PageIndex, req.PageSize)
	d, _ := json.Marshal(struct {
		RecordCount int
		List        []*cash.FinanceInfo
	}{
		RecordCount: recordCount,
		List:        list,
	})

	this.WriteMsg(msg, string(d))
}

// 赠送
func (this *user) transferGold(msg, data string) {
	retData := ""
	var req protocol.Transfer_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("transfer unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	var resp protocol.Transfer_resp
	resp.Success, resp.ErrMsg =
		cash.GoldTransfer(this.getUserId(), req.ToUserID, req.Amount, this.GetIP())

	if !resp.Success {
		log.Debug("user.transfer failed %s", resp.ErrMsg)
	} else {
		// 是否自动刷新金币？
		// ...
	}
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
}

func (this *user) goldTransferLog(msg, data string) {
	this.WriteMsg(msg, cash.GetGoldTransferLog(this.getUserId()))
}

func (this *user) goldTransferCfg(msg, data string) {
	this.WriteMsg(msg, cash.GetGoldTransferConfig(this.getUserId()))
}
