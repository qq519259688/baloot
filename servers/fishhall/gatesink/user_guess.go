package gatesink

import guess "bet24.com/servers/micros/guess/proto"

// 处理语音房指令
func (this *user) onGuessMsg(msg, data string) {
	jsonData := guess.OnGuessMsg(this.getUserId(), msg, data)
	this.WriteMsg(msg, jsonData)
	return
}
