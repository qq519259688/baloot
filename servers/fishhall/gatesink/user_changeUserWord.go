package gatesink

import (
	"encoding/json"

	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/transaction"
)

//修改个性签名
func (this *user) changeUserWord(msg, data string) {
	obj := transaction.NewChangeUserWord()
	err := json.Unmarshal([]byte(data), &obj.IN)
	if err != nil {
		log.Debug("changeUserWord UnMarshal data failed %v", data)
		return
	}

	obj.IN.UserID = this.getUserId()
	obj.IN.UserWords = coreservice.ParseKeyword(obj.IN.UserWords)
	obj.DoAction(nil)

	retData, err := json.Marshal(obj.OUT)
	if err != nil {
		log.Debug("send changeUserWord return Marshal failed")
		return
	}

	this.WriteMsg(msg, string(retData))
}
