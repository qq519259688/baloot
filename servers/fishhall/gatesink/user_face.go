package gatesink

import (
	"bet24.com/log"
	userservices "bet24.com/servers/micros/userservices/proto"
	"bet24.com/servers/transaction"
	"encoding/json"
	"fmt"
)

// 修改头像(包括头像url)
func (this *user) changeFace(msg, data string) {
	obj := transaction.NewChangeFace()
	err := json.Unmarshal([]byte(data), &obj.IN)
	if err != nil {
		log.Release("changeFace Unmarshal data failed %v", data)
		this.WriteMsg(msg, fmt.Sprintf("changeFace Unmarshal data failed %v", data))
		return
	}

	obj.IN.UserID = this.getUserId()

	obj.DoAction(nil)

	obj.Out.ErrorMsg = fmt.Sprintf("change face error code = %d", obj.Out.RetCode)
	buf, err := json.Marshal(obj.Out)
	if err != nil {
		retData := fmt.Sprintf("changeFace DoAction marshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	// 刷新coreservice中的用户信息
	go userservices.UpdateUserInfo(this.getUserId())

	this.WriteMsg(msg, string(buf))
}
