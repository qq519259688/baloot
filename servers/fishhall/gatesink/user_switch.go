package gatesink

import (
	"bet24.com/log"
	userservices "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"fmt"
)

// 设置开关（旁观、跟踪）
func (this *user) setSwitchInfo(msg, data string) {
	usr := userservices.GetUserInfo(this.getUserId())
	if usr == nil {
		log.Release("user.setSwitchInfo fail to query user info")
		this.WriteMsg(msg, "user not found")
		return
	}
	var req userservices.Request_Switch
	err := json.Unmarshal([]byte(data), &req)
	if err != nil {
		retData := fmt.Sprintf("setSwitchInfo Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	userservices.ChangeSwitchStatus(this.getUserId(), req.SwitchInfo)
	this.WriteMsg(msg, "ok")
}

// 获取（个人）开关信息
func (this *user) getSwitchInfo(msg, data string) {
	usr := userservices.GetUserInfo(this.getUserId())
	if usr == nil {
		log.Release("user.getSwitchInfo fail to query user info")
		this.WriteMsg(msg, "user not found")
		return
	}

	d, _ := json.Marshal(usr.Switches)
	this.WriteMsg(msg, string(d))
}
