package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
	"bet24.com/servers/insecureframe/gate"
)

// 世界聊天
func (this *user) sendChatMsg(msg, data string) {
	var req protocol.Chat_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("chat_req Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return
	}
	//聊天不需要返回， 信息推送显示
	if !client.SendChatMsg(req.ChannelID, u.GetUserId(), u.GetUserFaceId(), u.GetUserSex(), u.GetUserVipLevel(),
		req.RecvUserID, u.GetUserNickName(), u.GetUserFaceUrl(), req.Msg, this.GetIP(), req.MsgType) {
		this.WriteMsg(msg, "sendChatMsg failed")
	}
}

func (this *user) getChatMsg(msg, data string) {
	var req protocol.Chat_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("chat_req Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return
	}
	this.WriteMsg(msg, client.GetChatMsg(u.GetUserId(), req.ChannelID))
}

func (this *user) sendChannelChat(msg, data string) {
	var req protocol.Chat_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("chat_req Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return
	}

	ret := client.SendChannelChat(u.GetUserId(), req.RecvUserID, req.Msg, req.MsgType)
	if ret == "failed" || ret == "" {
		this.WriteMsg(msg, ret)
	} else {
		this.WriteMsg("getChannelChat", ret)
	}

}

func (this *user) getChannelChat(msg, data string) {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return
	}
	ret := client.GetChannelChat(data, u.GetUserId())

	this.WriteMsg("getChannelChat", ret)
}

func (this *user) getChannelInfo(msg, data string) {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return
	}
	ret := client.GetChannelInfo(data)

	this.WriteMsg("getChannelInfo", ret)
}

func (this *user) clearChannelHistory(msg, data string) {
	client.ClearChannelHistory(data, this.getUserId())
}
