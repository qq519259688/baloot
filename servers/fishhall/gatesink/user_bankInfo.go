package gatesink

import (
	"encoding/json"

	"bet24.com/servers/transaction"
)

func (this *user) bankInfo(msg string) {
	obj := transaction.NewBankInfoList()
	obj.In.UserID = this.getUserId()
	obj.DoAction()
	buf, _ := json.Marshal(obj.Out)
	this.WriteMsg(msg, string(buf))
	return
}
