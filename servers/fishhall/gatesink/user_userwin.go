package gatesink

import (
	"bet24.com/servers/coreservice/client"
	"fmt"
)

// 公告列表
func (this *user) getUserWinScore(msg string) {
	score := client.GetUserWinScore(this.getUserId())
	this.WriteMsg(msg, fmt.Sprintf("%d", score))
	return
}
