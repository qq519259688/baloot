package gatesink

import (
	"bet24.com/servers/coreservice/client"
)

func (this *user) getSigninWheelInfo(msg string) {
	this.WriteMsg(msg, client.GetSigninWheelInfo(this.getUserId()))
}

func (this *user) doSigninWheel(msg string) {
	this.WriteMsg(msg, client.DoSigninWheel(this.getUserId(), this.GetIP(), this.getUserGold()))
}

func (this *user) getSigninWheelHistory(msg string) {
	this.WriteMsg(msg, client.GetSigninWheelHistory())
}
