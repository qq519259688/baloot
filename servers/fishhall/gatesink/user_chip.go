package gatesink

import (
	"bet24.com/log"
)

/************************
当服务器配置IsChipRoom < 0 ,表示未纯金币平台，chip转账操作转移至cash
************************/

// 赠送
func (this *user) transferDiamond(msg, data string) {
	log.Release("transferDiamond unimplemented")
}

func (this *user) diamondTransferCfg(msg, data string) {
	log.Release("diamondTransferCfg unimplemented")
}

// 赠送记录
func (this *user) diamondTransferLog(msg, data string) {
	log.Release("diamondTransferLog unimplemented")
}
