package gatesink

import (
	"encoding/json"

	"bet24.com/log"

	"bet24.com/servers/transaction"
)

// 客服反馈
func (this *user) feedback(msg, data string) {
	obj := transaction.NewFeedback()
	err := json.Unmarshal([]byte(data), &obj.In)
	if err != nil {
		log.Debug("feedback unmarshal data failed %v", data)
		return
	}
	obj.In.IPAddress = this.GetIP()
	obj.DoAction()
	buf, _ := json.Marshal(obj.Out)
	this.WriteMsg(msg, string(buf))
}
