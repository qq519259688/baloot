package gatesink

import (
	"bet24.com/log"
	activityservice "bet24.com/servers/micros/activityservice/proto"
	"encoding/json"
)

// 处理新手福利指令
func (this *user) onNoviceWelfareMsg(msg, data string) {
	switch msg {
	case "getSysNoviceWelfare": // 获取系统新手福利
		this.getSysNoviceWelfare(msg)
	case "getUserNoviceWelfare": // 获取用户新手福利
		this.getUserNoviceWelfare(msg)
	case "claimNoviceWelfareAward": // 领取新手福利奖励
		this.claimNoviceWelfareAward(msg, data)
	case "getNoviceWelfareGiftPacks":
		this.getNoviceWelfareGiftPacks(msg, data)
	case "getNoviceWelfareGiftPackUserTerms":
		this.getNoviceWelfareGiftPackUserTerms(msg, data)
	case "claimNoviceWelfareGiftPack":
		this.claimNoviceWelfareGiftPack(msg, data)
	default:
		log.Release("user.onNoviceWelfareMsg unhandled msg %s", msg)
	}
}

// 获取系统新手福利
func (this *user) getSysNoviceWelfare(msg string) {
	resp := activityservice.GetSysNoviceWelfare()
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
	return
}

// 获取用户新手福利
func (this *user) getUserNoviceWelfare(msg string) {
	resp := activityservice.GetUserNoviceWelfare(this.getUserId())
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
	return
}

// 领取新手福利奖励
func (this *user) claimNoviceWelfareAward(msg, data string) {
	var req activityservice.Request
	ret := "fail"
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		log.Error("user.claimNoviceWelfareAward unmarshal fail %v", err)
		this.WriteMsg(msg, ret)
		return
	}
	b := activityservice.ClaimNoviceWelfareAward(this.getUserId(), req.DayIndex, req.IsFinalPackage)
	if b {
		ret = "ok"
	}
	this.WriteMsg(msg, ret)
	return
}

// 获取新手购买礼包
func (this *user) getNoviceWelfareGiftPacks(msg string, data string) {
	this.WriteMsg(msg, activityservice.GetGiftPackages(this.getUserId()))
	return
}

// 获取我的礼包状态
func (this *user) getNoviceWelfareGiftPackUserTerms(msg string, data string) {
	this.WriteMsg(msg, activityservice.GetUserGiftPackages(this.getUserId()))
	return
}

func (this *user) claimNoviceWelfareGiftPack(msg, data string) {
	var req activityservice.Request
	var ret struct {
		Success  bool
		ErrorMsg string
	}

	if err := json.Unmarshal([]byte(data), &req); err != nil {
		log.Error("user.claimNoviceWelfareGiftPack unmarshal fail %s", data)
		ret.ErrorMsg = "Invalid argument"
		d, _ := json.Marshal(ret)
		this.WriteMsg(msg, string(d))
		return
	}
	ret.Success, ret.ErrorMsg = activityservice.ClaimGiftPack(this.getUserId(), req.PackageId, req.TermId)
	d, _ := json.Marshal(ret)
	this.WriteMsg(msg, string(d))
	return
}
