package gatesink

import (
	"fmt"

	"bet24.com/servers/fishhall/config"
)

/*
	0 - 默认 穿山甲
	10 - 谷歌
	100 - facebook

	11 - 穿山甲+谷歌
	111 - 穿山甲+谷歌+facebook
*/

func (this *user) sendAdConfig() {
	this.WriteMsg("adconfig", fmt.Sprintf("%d", config.HallConfig.AdConfig))
	this.WriteMsg("adbannerclosed", fmt.Sprintf("%d", config.HallConfig.AdBannerClosed))
	this.WriteMsg("agentclosed", fmt.Sprintf("%d", config.HallConfig.IsAgentClosed))

	// 根据流量渠道打卡对应的支付
	utmSource := this.getUTMSource()
	for _, v := range config.HallConfig.ThirdPaymentOpen {
		if v != utmSource {
			continue
		}

		this.WriteMsg("thirdPaymentOpen", "1")
		break
	}
}
