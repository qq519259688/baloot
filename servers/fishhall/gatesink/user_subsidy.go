package gatesink

import (
	"encoding/json"

	"bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/config"
)

// 获取补助信息
func (this *user) subsidyGetInfo(msg, data string) {
	resp := client.SubsidyGetInfo(this.getUserId(), config.HallConfig.SubsidyAmount, config.HallConfig.SubsidyTimes)
	if resp.RetCode != 1 {
		log.Debug("user.subsidyGetInfo failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 领取补助
func (this *user) subsidyGift(msg, data string) {
	resp := client.SubsidyGift(this.getUserId(),
		config.HallConfig.SubsidyAmount,
		config.HallConfig.SubsidyTimes,
		config.HallConfig.SubsidyCoolSeconds)
	// log.Debug("user.subsidyGift succeeded resp=%+v", resp)
	if resp.RetCode != 1 {
		log.Debug("user.subsidyGift failed %v", resp)
	} else {
		var d redis.Channel_msg
		d.Message = "RefreshGold"
		d.UserID = this.getUserId()
		js, _ := json.Marshal(d)
		redis.Publish(string(js))
	}

	this.WriteMsg(msg, resp.Data)
	return
}

// 领取元宝补助
func (this *user) subsidyGiftChip(msg, data string) {
	// if !Sink.IsChipRoom() {
	// 	return
	// }
	resp := client.SubsidyGiftChip(this.getUserId(), config.HallConfig.SubsidyAmount, this.GetIP())
	if resp.RetCode != 1 {
		log.Debug("user.subsidyGiftChip failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}
