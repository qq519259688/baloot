package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

// betlist 下注记录
func (this *user) betlist(msg, data string) {
	retData := ""
	var req protocol.BetList_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("betlist unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.BetList(this.getUserId(), req.GameID, req.Days, req.PageIndex, req.PageSize, req.BetZone)
	if resp.RetCode != 1 {
		log.Debug("user.betList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

// gameHistory 游戏历史
func (this *user) gameHistory(msg, data string) {
	retData := ""
	var req protocol.BetList_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("gameHistory unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	// 页索引
	if req.PageIndex == 0 {
		req.PageIndex = 1
	}

	// 每页显示50条记录
	if req.PageSize == 0 {
		req.PageSize = 50
	}

	resp := client.GameHistory(this.getUserId(), req.PageIndex, req.PageSize)
	if resp.RetCode != 1 {
		log.Debug("user.gameHistory failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

// 游戏记录
func (this *user) getGameCount(msg, data string) {
	var userId int
	if data != "" {
		retData := ""
		var req protocol.GetGameCount_req
		if err := json.Unmarshal([]byte(data), &req); err != nil {
			retData = fmt.Sprintf("getGameCount unmarshal fail %v", err)
			log.Release(retData)
			this.WriteMsg(msg, retData)
			return
		}
		userId = req.UserID
	} else {
		userId = this.getUserId()
	}

	resp := client.GetGameCount(userId, 0)
	if resp.RetCode != 1 {
		log.Debug("user.getGameCount failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}
