package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

const (
	_                = iota
	ActiveTaskAnswer // 问卷调查
	ActiveAgentShare // 代理分享
)

// 发送问卷调查奖励
func (this *user) sendTaskAnswerAward(msg, data string) {
	var req protocol.Award_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("sendTaskAnswerAward unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.SendAward(this.getUserId(), ActiveTaskAnswer, req.HallType, this.GetIP())
	if resp.RetCode != 1 {
		log.Debug("user.sendTaskAnswerAward failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

func (this *user) sendAgentShareAward(msg, data string) {
	resp := client.SendAward(this.getUserId(), ActiveAgentShare, 3, this.GetIP())
	if resp.RetCode != 1 {
		log.Debug("user.sendAgentShareAward failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}
