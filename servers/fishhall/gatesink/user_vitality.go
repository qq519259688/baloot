package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
)

func (this *user) getUserVitalityInfo(msg, data string) {
	resp := client.GetUserVitalityInfo(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.getUserVitalityInfo failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}
