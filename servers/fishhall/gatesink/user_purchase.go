package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
)

// 100K购信息(期数\元宝数量\倒计时\抽奖码集合)
func (this *user) purchaseInfo(msg, data string) {
	resp := client.PurchaseInfo(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.purchaseInfo failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

// 100K购抽奖(返回操作结果\抽奖码)
func (this *user) purchaseBet(msg, data string) {
	resp := client.PurchaseBet(this.getUserId(), this.GetIP())
	if resp.RetCode != 1 {
		log.Debug("user.purchaseBet failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}
