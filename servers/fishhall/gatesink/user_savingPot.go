package gatesink

import (
	activityservice "bet24.com/servers/micros/activityservice/proto"
)

// 获取用户存钱罐
func (this *user) getUserSavingPot(msg, data string) {
	jsonData := activityservice.GetUserSavingPot(this.getUserId())
	this.WriteMsg(msg, jsonData)
	return
}
