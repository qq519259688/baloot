package gatesink

import (
	audioroom "bet24.com/servers/micros/audioroom/proto"
)

// 处理语音房指令
func (this *user) onAudioRoomMsg(msg, data string) {
	jsonData := audioroom.OnAudioRoomMsg(this.getUserId(), msg, data)
	this.WriteMsg(msg, jsonData)
	return
}
