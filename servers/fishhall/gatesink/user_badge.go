package gatesink

import (
	"bet24.com/log"
	badge "bet24.com/servers/micros/badge/proto"
	"encoding/json"
)

// 处理徽章指令
func (this *user) onBadgeMsg(msg, data string) {
	switch msg {
	case "getBadgeWearList": // 获取用户佩戴的徽章的列表
		this.getBadgeWearList(msg, data)
	case "getSysBadgeList": // 获取系统成就的徽章列表
		this.getSysBadgeList(msg)
	case "wearBadge": // 佩戴成就徽章
		this.wearBadge(msg, data)
	case "getUserBadgeList": // 获取用户徽章列表
		this.getUserBadgeList(msg)
	default:
		log.Release("user.onBadgeMsg unhandled msg %s", msg)
	}
}

// 获取用户佩戴的徽章的列表
func (this *user) getBadgeWearList(msg, data string) {
	var req badge.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		log.Error("user.getBadgeWearList unmarshal fail %v", err)
		this.WriteMsg(msg, "[]")
		return
	}
	resp := badge.GetBadgeWearAndShow(req.UserId)
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
	return
}

// 获取系统成就的徽章列表
func (this *user) getSysBadgeList(msg string) {
	resp := badge.GetSysBadgeList()
	this.WriteMsg(msg, resp)
	return
}

// 佩戴成就徽章
func (this *user) wearBadge(msg, data string) {
	var req badge.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		log.Error("user.wearBadge unmarshal fail %v", err)
		this.WriteMsg(msg, "[]")
		return
	}
	resp := badge.WearBadge(this.getUserId(), req.BadgeId, req.Position)
	this.WriteMsg(msg, resp)
	return
}

// 获取用户徽章信息
func (this *user) getUserBadgeList(msg string) {
	resp := badge.GetUserBadgeList(this.getUserId())
	this.WriteMsg(msg, resp)
	return
}
