package gatesink

import (
	"bet24.com/log"
	vipservice "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
)

func (this *user) onNewVipMsg(msg, data string) {
	// 新版vip体系
	switch msg {
	case "getNewVipUser": // 获取vip 用户状态
		this.getNewVipUser(msg)
	case "getNewVipList": // 获取vip 列表
		this.getNewVipList(msg)
	case "getNewVipPurchasePackage": // 获取购买礼包列表
		this.getNewVipPurchasePackage(msg)
	case "giftNewVipDailyAward": // 领取每日奖励
		this.giftNewVipDailyAward(msg)
	case "checkNewVipDailyAward": // 检查是否能够领取每日礼包
		this.checkNewVipDailyAward(msg)
	default:
		log.Release("user.onNewVipMsg unhandled msg %s", msg)
	}
}

// 获取vip 用户状态
func (this *user) getNewVipUser(msg string) {
	resp := vipservice.GetUserVip(this.getUserId())
	if resp == "" {
		log.Release("user.getNewVipUser user not found, userId = %d", this.getUserId())
	}
	this.WriteMsg(msg, resp)
	return
}

// 获取vip 列表
func (this *user) getNewVipList(msg string) {
	resp := vipservice.GetVipList()
	if resp == "" {
		log.Release("user.getNewVipList vip config list not found")
	}
	this.WriteMsg(msg, resp)
	return
}

// 获取购买礼包列表
func (this *user) getNewVipPurchasePackage(msg string) {
	resp := vipservice.GetPurchasePackageList(this.getUserId())
	if resp == "" {
		log.Release("user.getNewVipPurchasePackage purchase package list not found")
	}
	this.WriteMsg(msg, resp)
	return
}

// 领取每日奖励
func (this *user) giftNewVipDailyAward(msg string) {
	resp := vipservice.GiftDailyAward(this.getUserId())
	this.WriteMsg(msg, resp)
	return
}

// 检查是否能够领取每日礼包
func (this *user) checkNewVipDailyAward(msg string) {
	var info struct {
		Success bool
		Code    int
	}
	info.Success, info.Code = vipservice.CheckDailyAward(this.getUserId())
	d, _ := json.Marshal(info)
	this.WriteMsg(msg, string(d))
	return
}
