package gatesink

import (
	"encoding/json"
	"strings"
	"time"

	userlabel "bet24.com/servers/micros/userlabel/proto"

	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	slotmanager "bet24.com/servers/games/slotcommon/manager"
	"bet24.com/servers/insecureframe/gate"
	task "bet24.com/servers/micros/task/proto"
)

func newUser(userIndex int32) *user {
	this := new(user)
	this.init(userIndex)
	return this
}

type user struct {
	UserIndex  int32
	chan_clear chan bool
}

func (this *user) init(userIndex int32) {
	this.UserIndex = userIndex
	this.chan_clear = make(chan bool)
}

func (this *user) clear() {
	// 用户离开
	slotmanager.OnUserExit(this.getUserId())

	u := gate.GetUserInfo(this.UserIndex)

	// 1=活跃时长(大厅模块)(用户标签)
	seconds := time.Now().Unix() - u.GetLogonTime()
	go userlabel.TriggerEvent(this.getUserId(), userlabel.Type_Login, userlabel.Scope{OnlineSeconds: int(seconds)})

	if u != nil && !Sink.IsChipRoom() {
		go func(userId int) {
			coreservice.OnUserExit(userId)
		}(this.getUserId())
	}

	close(this.chan_clear)
}

func (this *user) onLogined() {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return
	}
	userId := u.GetUserId()
	coreservice.OnUserEnter(userId, this.GetIP())
	go slotmanager.OnUserLogined(this.getUserId())
	// 开启Notification
	if !Sink.IsChipRoom() {
		go func() {
			time.Sleep(time.Second)
			task.DoTaskAction(userId, task.TaskAction_login, 1, task.TaskScope{})
		}()
		this.selectNotification()
		// 发一个广告配置
		this.sendAdConfig()
	}
}

func (this *user) onGameMessage(msg, data string) bool {
	//处理无需登录指令
	if ret := this.handleAuthlessMsg(msg, data); ret {
		return true
	}

	//需要登录的指令
	if ret := this.handleAuthMsg(msg, data); ret {
		return true
	}

	log.Debug("unhandled message %v  data %v", msg, data)
	return false
}

// 不需要登录的指令
func (this *user) handleAuthlessMsg(msg, data string) (ret bool) {
	ret = true
	switch msg {
	case "gameRequest": //审核游戏名
		this.gameRequest(msg, data)
		this.sendLoginConfig()
	case "getSlotConfig":
		ret := slotmanager.GetConfigByCmd(data)
		this.WriteMsg(msg, ret)
	case "getSlotConfigList":
		ret := slotmanager.GetConfigList(data)
		d, _ := json.Marshal(ret)
		this.WriteMsg(msg, string(d))
	case "writeSlotConfig":
		log.Debug("writeSlotConfig %s", data)
		ret := slotmanager.WriteConfigByCmd(data)
		this.WriteMsg(msg, ret)
	case "feedback":
		this.feedback(msg, data)
	case "trackRecord": //足迹记录
		this.trackRecord(msg, data)
	default:
		ret = false
	}
	return
}

func (this *user) handleAuthMsg(msg, data string) (ret bool) {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		log.Release("handleAuthMsg invalid msg = %s userIndex=%d u=%+v", msg, this.UserIndex, u)
		return false
	}

	userId := u.GetUserId()
	if userId <= 0 {
		log.Release("handleAuthMsg invalid msg = %s userIndex=%d userId=%d u=%+v", msg, this.UserIndex, userId, u)
		return false
	}

	//log.Debug("handleAuthMsg %s", msg)

	ret = true
	switch msg {
	// 商城
	case "getExchangeRateList": // 币种列表
		this.getExchangeRateList(msg, data)
	case "saveCountry": // 保存国家地区
		this.saveCountry(msg, data)
	case "getShopList":
		this.getShopList(msg, data)
	case "getLocalPrice":
		this.getLocalPrice(msg, data)
	case "changeFace": //修改头像
		this.changeFace(msg, data)
	case "changeNickName": //修改昵称
		this.changeNickName(msg, data)
	case "changeNameInfo": // 修改昵称信息
		this.changeNameInfo(msg, data)
	case "changeSex": //修改性别
		this.changeSex(msg, data)
	case "getInfo": //查询社交类用户信息(昵称、服务器名称、头像变化)
		this.getInfo(msg, data)

	// 邮件类处理
	case "sendUserMail": // 发送用户邮件
		this.sendUserMail(msg, data)
	case "getUserMails": // 获取用户邮件列表
		this.getUserMails(msg, data)
	case "sendSysMail": // 发送系统邮件(含附件)
		this.sendSysMail(msg, data)
	case "getSysMails": // 获取系统邮件列表
		this.getSysMails(msg, data)
	case "updateSysMail": // 修改系统邮件（领取邮件附件）
		this.updateSysMail(msg, data)
	case "delSysMail": // 删除系统邮件
		this.delSysMail(msg, data)
	case "updateAllSysMail": // 领取所有邮件
		this.updateAllSysMail(msg, data)

	// 金币操作
	case "cashLog": // 金币日志
		this.cashLog(msg, data)
	case "financeLog": // 充值、提现记录
		this.financeLog(msg, data)

	// 服务器类处理
	case "betlist": // 下注记录
		this.betlist(msg, data)
	case "gameHistory": // 游戏历史
		this.gameHistory(msg, data)
	case "getGameCount": // 游戏记录
		this.getGameCount(msg, data)
	case "getLonghuRooms":
		this.getLonghuRooms(msg, data)
	case "getGameRooms":
		this.getGameRooms(msg, data)

	case "setSwitchInfo":
		this.setSwitchInfo(msg, data) // 设置开关（旁观、跟踪）
	case "getSwitchInfo":
		this.getSwitchInfo(msg, data) // 获取（个人）开关信息

	case "awardTask":
		this.awardTask(msg, data)
	case "awardAllTask":
		this.awardAllTask(msg)
	case "getSysTask":
		this.getSysTask(msg, data)
	case "getSysTaskList":
		this.getSysTaskList(msg, data)
	case "getUserTaskList":
		this.getUserTaskList(msg, data)

	case "getCouponTask":
		this.getCouponTask(msg, data)
	case "awardCouponTask":
		this.awardCouponTask(msg, data)
	case "updateCouponTaskTmpLimit":
		this.updateCouponTaskTmpLimit(msg, data)

	case "getSysItems": //获取系统道具列表
		this.getSysItems(msg, data)
	case "consumeItem": //使用道具
		this.consumeItem(msg, data)
	case "sellItem": // 出售道具
		this.sellItem(msg, data)
	case "giftItems": // 赠送道具
		this.giftItems(msg, data)
	case "getGiftCardHistory": // 获取礼品卡记录
		this.getGiftCardHistory(msg, data)
	case "getUserDecorations":
		this.getUserDecorations(msg, data)
	case "setUserDecoration":
		this.setUserDecoration(msg, data)

		// 转盘
	case "prizeWheelConfig":
		this.getPrizeWheelConfig(msg)
	case "getWheelTimes":
		this.getWheelTimes(msg, data)
	case "doWheel":
		this.doWheel(msg, data)
	case "getPrizeRecord":
		this.getPrizeRecord(msg, data)
	case "getJackpotAmount":
		this.getJackpotAmount(msg)

	case "subsidyGetInfo": //获取补助信息
		this.subsidyGetInfo(msg, data)
	case "subsidyGift": //领取补助
		this.subsidyGift(msg, data)
	case "subsidyGiftChip": //领取元宝补助
		this.subsidyGiftChip(msg, data)

	case "getUserVitalityInfo": //获取用户活跃度信息
		this.getUserVitalityInfo(msg, data)

	case "getGiftPacks": // 获取礼包数据
		this.getGiftPacks(msg)
	case "getBuyableGiftPacks": // 获取用户可购买的礼包列表
		this.getBuyableGiftPacks(msg)
	case "getGrowthPacks": // 获取礼包数据
		this.getGrowthPacks(msg)
	case "getGrowthPackTerms": // 获取用户可购买的礼包列表
		this.getGrowthPackTerms(msg)
	case "awardGrowthPack": // 获取用户可购买的礼包列表
		this.awardGrowthPack(msg, data)

	case "monthlyCardSysInfo": //月卡系统信息
		this.monthlyCardSysInfo(msg, data)
	case "monthlyCardUserInfo": //月卡用户信息
		this.monthlyCardUserInfo(msg, data)
	case "monthlyCardGift": //月卡领取
		this.monthlyCardGift(msg, data)
	case "weekCardGift": //周卡领取
		this.weekCardGift(msg, data)

	case "getExchangeList":
		this.getExchangeList(msg, data)
	case "exchangeGift":
		this.exchangeGift(msg, data)
	case "getExchangeHistory":
		this.getExchangeHistory(msg, data)
	case "getShareItems":
		this.getShareItems(msg)
	case "rewardShareItems":
		this.rewardShareItems(msg)
	case "doSharePlatform":
		this.doSharePlatform()
	case "doShareGame":
		this.doShareGame()
	case "finishNoviceGuidance":
		this.finishNoviceGuidance(data)
	case "finishGameNoviceGuidance":
		this.finishGameNoviceGuidance(msg, data)
	case "isShowNoviceGuidance":
		this.isShowNoviceGuidance(msg, data)

	case "exchangeGold":
		this.exchangeGold(msg, data)
	case "exchangeGoldInBulk":
		this.exchangeGoldInBulk(msg, data)
	//保险柜
	case "bankIn": //保险柜存入
		this.bankIn(msg, data)
	case "bankOut": //保险柜取出
		this.bankOut(msg, data)
	case "bankQuery": //保险柜查询
		this.bankQuery(msg, data)
	case "goldTransferLog": // 赠送记录
		this.goldTransferLog(msg, data)
	case "goldTransferCfg": // 赠送配置信息
		this.goldTransferCfg(msg, data)
	case "transferGold":
		this.transferGold(msg, data)

	//聊天
	case "sendChatMsg": //发送聊天信息
		this.sendChatMsg(msg, data)
	case "getChatMsg": // 获取聊天信息
		this.getChatMsg(msg, data)

	case "sendChannelChat":
		this.sendChannelChat(msg, data)
	case "getChannelChat":
		this.getChannelChat(msg, data)
	case "getChannelInfo":
		this.getChannelInfo(msg, data)
	case "clearChannelHistory":
		this.clearChannelHistory(msg, data)

	// 好友
	case "searchUser": //搜索玩家
		this.getSearchUserinfo(msg, data)
	case "searchUserList": //搜索玩家
		this.getSearchUserList(msg, data)
	case "friendApply": //添加好友
		this.friendApply(msg, data)
	case "friendApplyList": //好友审核列表
		this.friendVerifyList(msg, data)
	case "friendHandleApply": //好友申请处理
		this.friendHandleApply(msg, data)
	case "getfriendList": //好友列表
		this.getFriendList(msg, data)
	case "delfriend": //删除好友
		this.delFriend(msg, data)
	case "friendGiveGift": //赠送礼物
		this.friendGiveGift(msg, data)
	case "friendGetGift": //获得礼物
		this.friendGetGift(msg, data)
	case "friendRoomInvite": // 房间邀请(好友场)
		this.friendRoomInvite(msg, data)
	case "friendGetRoomInviteList": // 好友邀请列表
		this.friendGetRoomInviteList(msg, data)
	case "friendRoomInviteInvalid": // 好友邀请失效
		this.friendRoomInviteInvalid(msg, data)
	case "getMaxFriendCount": // 获取好友上限
		this.getMaxFriendCount(msg)
	case "getPotentialFriendList": // 获取潜在好友
		this.getPotentialFriendList(msg)
	case "friendGetBlackList": // 黑名单列表
		this.friendGetBlackList(msg, data)
	case "friendAddBlack": // 添加黑名单
		this.friendAddBlack(msg, data)
	case "friendDelBlack": // 删除黑名单
		this.friendDelBlack(msg, data)

	//推广(邀请)
	case "spreadApply": //会员申请
		this.spreadApply(msg, data)
	case "spreadMembers": //会员列表
		this.spreadMembers(msg, data)
	case "spreadGift": //领取奖励
		this.spreadGift(msg, data)

	//评论
	case "reviewInfo": //获取评论信息
		this.reviewGetInfo(msg, data)
	case "reviewGift": //领取评论
		this.reviewGift(msg, data)

	// 签到转盘
	case "getSigninWheelInfo":
		this.getSigninWheelInfo(msg)
	case "doSigninWheel":
		this.doSigninWheel(msg)
	case "getSigninWheelHistory":
		this.getSigninWheelHistory(msg)

		//服务器类处理
	case "getGameList": //游戏列表
		this.getGameList(msg, data)
	case "changeUserWord": //修改个性签名
		this.changeUserWord(msg, data)
	case "getMyGameStatus": //获取游戏状态，用于金币场断线重连
		this.getMyGameStatus(msg)

	//case "trackRecord": //足迹记录
	//	this.trackRecord(msg, data)
	case "testPay": // 充值测试用
		this.testPay(data)
	case "triggerEvent": // 触发用户标签事件
		this.triggerEvent(msg, data)

	case "sendSms": //发送短信验证码
		this.sendSms(msg, data)
	case "bindPhone":
		this.bindPhone(msg, data)
	case "bindFacebook":
		this.bindFacebook(msg, data)
	case "getBindingInfo":
		this.getBindingInfo(msg)
	case "bankInfo": // 银行信息
		this.bankInfo(msg)

	case "getNewUserGift":
		this.getNewUserGift(msg)
	case "receiveNewUserGift":
		this.receiveNewUserGift(msg, data)
	case "getNewYearGift":
		this.getNewYearGift(msg)
	case "receiveNewYearGift":
		this.receiveNewYearGift(msg, data)

	case "getGameSettleVideoList":
		this.getGameSettleVideoList(msg, data)
	case "awardGameSettleVideo":
		this.awardGameSettleVideo(msg, data)

	case "withdrawInfo": // 提现信息(提现前调用)
		this.withdrawInfo(msg)

	// 师徒体系
	case "teacherRegister": // 注册
		this.teacherRegister(msg, data)
	case "teacherInfo": // 师父信息
		this.teacherInfo(msg, data)
	case "bindTeacher": // 绑定
		this.bindTeacher(msg, data)
	case "students": // 徒弟列表
		this.students(msg, data)
	case "teacherProfitList": // 收益列表
		this.teacherProfitList(msg, data)

	case "rankAward": // 榜单奖励
		this.rankAward(msg, data)
	case "getLevelList": // 等级信息
		this.getLevelList(msg, data)
	case "hallAward": // 登录奖励
		this.loginAward(msg, data)
	case "getSlotScore":
		this.getSlotScore(msg, data)
	case "getSlotScoreExchange":
		this.getSlotScoreExchange(msg, data)
	case "slotScoreExchange":
		this.slotScoreExchange(msg, data)
	case "getSlotScoreExchangeHistory":
		this.getSlotScoreExchangeHistory(msg, data)

	case "getChipWheelConfig":
		this.getChipWheelConfig(msg)
	case "doChipWheel":
		this.doChipWheel(msg, data)
	case "getChipWheelHistory":
		this.getChipWheelHistory(msg)

	case "purchaseInfo": // 100K购信息(期数\元宝数量\倒计时\抽奖码集合)
		this.purchaseInfo(msg, data)
	case "purchaseBet": // 100K购抽奖(返回操作结果\抽奖码)
		this.purchaseBet(msg, data)

	case "sendTaskAnswerAward": // 问卷调查奖励
		this.sendTaskAnswerAward(msg, data)
	case "sendAgentShareAward": // 代理分享奖励
		this.sendAgentShareAward(msg, data)

	case "useRechargeCard":
		this.useRechargeCard(msg, data) // 使用充值卡

	case "getUserWinScore":
		this.getUserWinScore(msg) // 获取当前累积的输赢

	case "deleteAccount": // 删除账号
		this.deleteAccount(msg, data)

	case "addDot": // 添加打点
		this.addDot(msg, data)

	case "getUserSavingPot": // 获取存钱罐
		this.getUserSavingPot(msg, data)

	default:
		if strings.Contains(msg, "agent") {
			this.onAgentMsg(msg, data)
			return
		}

		if strings.Contains(msg, "SlotsService") {
			this.onSlotsServiceMsg(msg, data)
			return
		}

		if strings.Contains(msg, "LadderService") {
			this.onUserLadderMsg(msg, data)
			return
		}

		if strings.Contains(msg, "DailyWheel") {
			this.onDailyWheelMsg(msg, data)
			return
		}

		if strings.Contains(msg, "BattlePass") {
			this.onBattlePassMsg(msg, data)
			return
		}

		if strings.Contains(msg, "UserSignIn") {
			this.onUserSignInMsg(msg, data)
			return
		}

		if strings.Contains(msg, "PrivateRoom") {
			this.onPrivateRoomMsg(msg, data)
			return
		}
		if strings.Contains(msg, "SimpleMatch") {
			this.onSimpleMatchMsg(msg, data)
			return
		}
		if strings.Contains(msg, "PointMatch") {
			this.onPointMatchMsg(msg, data)
			return
		}
		if strings.Contains(msg, "SetsMatch") {
			this.onSetsMatchMsg(msg, data)
			return
		}
		if strings.Contains(msg, "SngMatch") {
			this.onSngMatchMsg(msg, data)
			return
		}
		if strings.Contains(msg, "ComboMatch") {
			this.onComboMatchMsg(msg, data)
			return
		}
		if strings.Contains(msg, "SingleMatch") {
			this.onSingleMatchMsg(msg, data)
			return
		}

		if strings.Contains(msg, "AudioRoom") { // 语音房相关处理
			this.onAudioRoomMsg(msg, data)
			return
		}

		// 需要定向输出的
		if strings.Contains(msg, "rankList") { // 获取排行榜列表
			this.getRankList(msg, data)
			return
		}
		// 需要定向输出的
		if strings.Contains(msg, "rankHistoryList") { // 获取历史榜单
			this.rankHistoryList(msg, data)
			return
		}

		if strings.Contains(msg, "getRankAwardList") { // 榜单奖励列表
			this.getRankAwardList(msg, data)
			return
		}

		if strings.Contains(msg, "getUserItems") { //获取背包内道具列表
			this.getUserItems(msg, data)
			return
		}

		if strings.Contains(msg, "announceList") { // 公告列表
			this.announceList(msg, data)
			return
		}

		if slotmanager.OnGameMessage(u.GetUserId(), msg, data) {
			return
		}

		if strings.Contains(msg, "NewVip") { // 新版vip
			this.onNewVipMsg(msg, data)
			return
		}

		if strings.Contains(msg, "Badge") { // 徽章
			this.onBadgeMsg(msg, data)
			return
		}

		if strings.Contains(msg, "GiftService") {
			this.onGiftServiceMsg(msg, data)
			return
		}

		if strings.Contains(msg, "NoviceWelfare") { // 新手福利任务
			this.onNoviceWelfareMsg(msg, data)
			return
		}

		if strings.Contains(msg, "LevelRewards") { // 等级礼包
			this.onLevelRewardsMsg(msg, data)
			return
		}

		if strings.HasPrefix(msg, "guess") { // 竞猜相关协议
			this.onGuessMsg(msg, data)
			return
		}

		if strings.Contains(msg, "HighlyProfitable") { // 一本万利
			this.onHighlyProfitableMsg(msg, data)
			return
		}

		//if strings.Contains(msg, "getBroadcasts") { //获取广播消息
		//	this.getBroadcasts(msg, data)
		//	return
		//}

		ret = false
	}
	return
}

func (this *user) WriteMsg(msg, data string) bool {
	return gate.SendMessage(this.UserIndex, msg, data)
}

func (this *user) GetIP() string {
	return gate.GetUserIP(this.UserIndex)
}

func (this *user) GetEventChannel() chan interface{} {
	return gate.GetEventChannel(this.UserIndex)
}

func (this *user) getUserId() int {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return 0
	}

	return u.GetUserId()
}

func (this *user) getPartnerId() int {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return 0
	}

	return u.GetPartnerId()
}

func (this *user) getVersionCode() int {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return 0
	}

	return u.GetVersionCode()
}

func (this *user) getUserGold() int {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return 0
	}

	return u.GetUserGold()
}

func (this *user) getNickName() string {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return ""
	}

	return u.GetUserNickName()
}

func (this *user) getFaceId() int {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return 0
	}

	return u.GetUserFaceId()
}

func (this *user) getFaceUrl() string {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return ""
	}

	return u.GetUserFaceUrl()
}

func (this *user) getHigherUserId() int {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return 0
	}
	return u.GetHigherUserID()
}

func (this *user) setHigherUserId(higherUserId int) {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return
	}
	u.SetHigherUserID(higherUserId)
}

func (this *user) getCurrency() string {
	return "SAR"
	/*
		u := gate.GetUserInfo(this.UserIndex)
		if u == nil {
			return "SAR"
		}
		ret := u.GetCurrency()
		if ret == "" {
			ret = "SAR"
		}
		return ret*/
}

func (this *user) setCurrency(currency string) {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return
	}
	u.SetCurrency(currency)
}

func (this *user) getCurrencyIsModify() int {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return 0
	}
	return u.GetCurrencyIsModify()
}

func (this *user) setCurrencyIsModify(isModify int) {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return
	}
	u.SetCurrencyIsModify(isModify)
}

func (this *user) getUTMSource() string {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return ""
	}
	return u.GetUTMSource()
}

func (this *user) setUTMSource(source string) {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return
	}
	u.SetUTMSource(source)
}

func (this *user) isGuest() bool {
	u := gate.GetUserInfo(this.UserIndex)
	if u == nil {
		return false
	}
	return u.IsGuest()
}
