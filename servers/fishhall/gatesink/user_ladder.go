package gatesink

import (
	"bet24.com/log"
	ladderservice "bet24.com/servers/micros/ladderservice/proto"
	"encoding/json"
	"fmt"
	"strconv"
)

func (this *user) onUserLadderMsg(msg, data string) {
	switch msg {
	case "LadderService_getSystemInfo":
		this.WriteMsg(msg, ladderservice.GetSystemLadderInfo())
	case "LadderService_getUserInfo":
		this.getUserLadderInfo(msg, data)
	case "LadderService_getUserConsecutiveWinCount":
		this.getUserConsecutiveWinCount(msg, data)
	case "LadderService_useConsecutiveCard":
		this.useConsecutiveCard(msg, data)
	case "LadderService_getRoomList":
		this.WriteMsg(msg, ladderservice.GetLadderRoomList())
	case "LadderService_getUserSettlementRecord":
		this.getUserSettlementRecord(msg, data)
	case "LadderService_getUserHistoricalSettlementRecord":
		this.getUserHistoricalSettlement(msg, data)
	}
}

func (this *user) getUserLadderInfo(msg, data string) {
	gameId, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.getUserLadderInfo Unmarshal failed %s", data)
		gameId = 0
	}
	ladderInfo := ladderservice.GetUserLadderInfo(this.getUserId(), gameId)
	var ret string
	if ladderInfo != nil {
		d, _ := json.Marshal(ladderInfo)
		ret = string(d)
	}
	this.WriteMsg(msg, ret)
}

func (this *user) getUserConsecutiveWinCount(msg, data string) {
	var req struct {
		UserId int
		GameId int
	}

	err := json.Unmarshal([]byte(data), &req)
	if err != nil {
		log.Release("gatesink.getUserConsecutiveWinCount Unmarshal failed %s", data)
		req.UserId = this.getUserId()
		req.GameId = 0
	}

	count := ladderservice.GetUserConsecutiveWinCount(req.UserId, req.GameId)
	this.WriteMsg(msg, fmt.Sprintf("%d", count))
}

func (this *user) useConsecutiveCard(msg, data string) {
	if ladderservice.UseConsecutiveCard(this.getUserId()) {
		this.WriteMsg(msg, "ok")
	} else {
		this.WriteMsg(msg, "failed")
	}
}

func (this *user) getUserSettlementRecord(msg, data string) {
	ret := ladderservice.GetUserSettlementRecord(this.getUserId())
	buf, _ := json.Marshal(ret)
	this.WriteMsg(msg, string(buf))
}

func (this *user) getUserHistoricalSettlement(msg, data string) {
	ret := ladderservice.GetUserHistoricalRecord(this.getUserId())
	buf, _ := json.Marshal(ret)
	this.WriteMsg(msg, string(buf))
}
