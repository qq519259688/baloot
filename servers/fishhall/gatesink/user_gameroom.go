package gatesink

import (
	_ "bet24.com/log"
	"bet24.com/redis"
	"encoding/json"
	"fmt"
	"strings"
)

type gameRoom struct {
	Name      string
	Online    string
	PrizePool string
}

func (this *user) getGameRooms(msg, data string) {
	retData := ""
	addrKey := "Addr"
	onlineKey := "Online"
	if Sink.IsChipRoom() {
		addrKey = "ChipAddr"
		onlineKey = "ChipOnline"
	}
	keys := redis.Key_GetKeys(fmt.Sprintf("%s:%s:*", data, addrKey))
	if len(keys) > 0 {
		var rooms []gameRoom
		for _, v := range keys {
			name, ok := redis.String_Get(v)
			if ok {
				var room gameRoom
				room.Name = name
				room.Online, ok = redis.String_Get(strings.Replace(v, addrKey, onlineKey, -1))
				rooms = append(rooms, room)
			}
		}
		d, _ := json.Marshal(rooms)
		retData = string(d)
	}
	this.WriteMsg(msg, retData)
}

func (this *user) getLonghuRooms(msg, data string) {
	this.getGameRooms(msg, "longhu")
}
