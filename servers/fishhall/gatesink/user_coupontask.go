package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
	game "bet24.com/servers/micros/game/proto"
	"encoding/json"
	"fmt"
)

// 用户券任务
func (this *user) getCouponTask(msg, data string) {
	resp := client.GetUserCouponTask(this.getUserId())
	if resp.List != nil {
		for _, v := range resp.List {
			if info := game.GetGame(v.GameId); info != nil {
				v.ChineseName = info.ChineseName
			}
		}
	}

	buf, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(buf))
}

// 领取奖励
func (this *user) awardCouponTask(msg, data string) {
	var req protocol.AwardCouponTask_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("awardCouponTask Unmarshal data failed %v", data)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.AwardCouponTask(this.getUserId(), req.UserTaskId)
	if resp.RetCode != 1 {
		log.Debug("user.awardCouponTask failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

// 修改临时上限
func (this *user) updateCouponTaskTmpLimit(msg, data string) {
	resp := client.UpdateCouponTaskTmpLimit(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.updateCouponTaskTmpLimit failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}
