package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/config"
	userlabel "bet24.com/servers/micros/userlabel/proto"
)

// 充值测试用
func (this *user) testPay(productId string) {
	if config.Server.TestPay {
		log.Release("testPay UserId[%d] productId[%s]", this.getUserId(), productId)
		client.Recharge(this.getUserId(), productId)
	}
}

// 触发用户标签事件
func (this *user) triggerEvent(msg, data string) {
	retData := ""
	var req userlabel.Request_TriggerEvent
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("test.triggerEvent unmarshal fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	userlabel.TriggerEvent(this.getUserId(), req.TypeId, req.Scope)
	this.WriteMsg(msg, "")
	return
}
