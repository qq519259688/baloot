package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/fishhall/protocol"
	item "bet24.com/servers/micros/item_inventory/proto"
	mail "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"fmt"
)

// 发送用户邮件(客服留言)
func (this *user) sendUserMail(msg, data string) {
	retData := ""
	var req protocol.SendUserMail_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("sendUserMail unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	var ret struct {
		SendImgCount int
		Mails        interface{}
	}
	ret.SendImgCount, ret.Mails = mail.SendUserMail(this.getUserId(), req.Title, req.Content, req.Img)
	d, _ := json.Marshal(ret)
	this.WriteMsg(msg, string(d))
	return
}

// 获取用户邮件列表(客服留言)
func (this *user) getUserMails(msg, data string) {
	retData := ""
	var req protocol.GetUserMail_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("getUserMails unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	var ret struct {
		SendImgCount int
		Mails        interface{}
	}
	ret.SendImgCount, ret.Mails = mail.GetUserMails(this.getUserId(), req.MailId)
	d, _ := json.Marshal(ret)
	this.WriteMsg(msg, string(d))
	return
}

// 发送系统邮件(含附件)
func (this *user) sendSysMail(msg, data string) {
	retData := ""
	var req protocol.SendSysMail_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user.sendSysMail unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	if mail.SendSysMail(this.getUserId(), &mail.SysMail{
		Id:      0,
		Title:   req.Title,
		Content: req.Content,
		//Status:     req.Status,
		SourceName: req.SourceName,
		Crdate:     common.GetTimeStamp(),
		//Tools:      req.Tools,
	}) {
		retData = "success"
	}

	this.WriteMsg(msg, retData)
	return
}

// 获取系统邮件列表
func (this *user) getSysMails(msg, data string) {
	mails := mail.GetSysMails(this.getUserId(), 0)
	d, _ := json.Marshal(mails)
	this.WriteMsg(msg, string(d))
	return
}

// 修改系统邮件
func (this *user) updateSysMail(msg, data string) {
	retData := ""
	var req protocol.UpdateSysMail_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user.updateSysMail unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	var info struct {
		RetCode int
		Items   []item.ItemPack
	}
	info.RetCode, info.Items = mail.UpdateSysMail(this.getUserId(), req.SysMsgId, req.Status)
	d, _ := json.Marshal(info)
	this.WriteMsg(msg, string(d))
	return
}

// 删除系统邮件
func (this *user) delSysMail(msg, data string) {
	retData := ""
	var req protocol.DelSysMail_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user.delSysMail unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	ret := mail.DelSysMail(this.getUserId(), req.SysMsgId)
	if ret != 1 {
		log.Debug("user.delSysMail failed")
	} else {
		retData = "success"
	}
	this.WriteMsg(msg, retData)
	return
}

// 修改系统邮件
func (this *user) updateAllSysMail(msg, data string) {
	jsonData := mail.UpdateAllSysMail(this.getUserId())
	this.WriteMsg(msg, jsonData)
	return
}
