package gatesink

import "bet24.com/servers/coreservice/client"

//月卡系统信息
func (this *user) monthlyCardSysInfo(msg, data string) {
	resp := client.MonthlyCardSysInfo()
	this.WriteMsg(msg, resp.Data)
	return
}

//获取用户月卡信息
func (this *user) monthlyCardUserInfo(msg, data string) {
	resp := client.MonthlyCardUserInfo(this.getUserId())
	this.WriteMsg(msg, resp.Data)
	return
}

//月卡领取
func (this *user) monthlyCardGift(msg, data string) {
	resp := client.MonthlyCardGift(this.getUserId())
	this.WriteMsg(msg, resp.Data)
	return
}

//周卡领取
func (this *user) weekCardGift(msg, data string) {
	resp := client.WeekCardGift(this.getUserId())
	this.WriteMsg(msg, resp.Data)
	return
}
