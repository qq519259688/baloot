package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

//获取评论信息
func (this *user) reviewGetInfo(msg, data string) {
	retData := ""
	var req protocol.Review_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("reviewGetInfo unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.ReviewGetInfo(this.getUserId(), req.AppName)
	if resp.RetCode != 1 {
		log.Debug("user.reviewGetInfo failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

//领取评论
func (this *user) reviewGift(msg, data string) {
	retData := ""
	var req protocol.Review_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("reviewGift unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.ReviewGift(this.getUserId(), req.AppName, this.GetIP())
	if resp.RetCode != 1 {
		log.Debug("user.reviewGift failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}
