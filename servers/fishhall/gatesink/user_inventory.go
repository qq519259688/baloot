package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/fishhall/protocol"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
)

func (this *user) getSysItems(msg, data string) {
	items := item.GetItems()
	retData := ""
	if len(items) > 0 {
		d, _ := json.Marshal(items)
		retData = string(d)
	}
	this.WriteMsg(msg, retData)
}

func (this *user) getUserItems(msg, data string) {
	items := inventory.GetUserItems(this.getUserId())
	retData := ""
	if len(items) > 0 {
		d, _ := json.Marshal(items)
		retData = string(d)
	}
	this.WriteMsg(msg, retData)
}

func (this *user) consumeItem(msg, data string) {
	var req protocol.InventoryCosume_req
	var resp protocol.InventoryCosume_resp
	err := json.Unmarshal([]byte(data), &req)
	if err != nil {
		resp.ErrMsg = fmt.Sprintf("consumeItem Unmarshal data failed %v", data)
		log.Release(resp.ErrMsg)
		d, _ := json.Marshal(resp)
		this.WriteMsg(msg, string(d))
		return
	}

	resp.ItemId = req.ItemId
	resp.Succeeded, resp.ErrMsg = inventory.Consume(this.getUserId(), req.ItemId, 0, req.Count, 0)
	if !resp.Succeeded {
		log.Debug("inventory.Consume failed %v", resp)
	}
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
}

func (this *user) sellItem(msg, data string) {
	var req protocol.InventoryCosume_req
	var resp protocol.InventoryCosume_resp
	err := json.Unmarshal([]byte(data), &req)
	if err != nil {
		resp.ErrMsg = fmt.Sprintf("sellItem Unmarshal data failed %v", data)
		log.Release(resp.ErrMsg)
		d, _ := json.Marshal(resp)
		this.WriteMsg(msg, string(d))
		return
	}

	resp.ItemId = req.ItemId
	resp.Succeeded, resp.ErrMsg = inventory.Sell(this.getUserId(), req.ItemId, req.Count, 0)
	if !resp.Succeeded {
		log.Debug("inventory.sellItem failed %v", resp)
	}
	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
}

func (this *user) giftItems(msg, data string) {
	var req inventory.Request_Gift
	err := json.Unmarshal([]byte(data), &req)
	if err != nil {
		retData := fmt.Sprintf("giftItems Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	var resp struct {
		RetCode int
		Data    string
	}
	var ok bool
	ok, resp.Data = inventory.Gift(this.getUserId(), req.ToUserId, req.ItemId, req.Count)
	if !ok {
		log.Debug("inventory.giftItems failed %v", resp)
	} else {
		resp.RetCode = 1
	}

	d, _ := json.Marshal(resp)
	this.WriteMsg(msg, string(d))
}

func (this *user) getUserDecorations(msg, data string) {
	usr := userservices.GetUserInfo(this.getUserId())
	if usr == nil {
		log.Release("user.getUserDecorations fail to query user info")
		this.WriteMsg(msg, "user not found")
		return
	}
	d, _ := json.Marshal(usr.Decorations)
	this.WriteMsg(msg, string(d))
}

func (this *user) setUserDecoration(msg, data string) {
	usr := userservices.GetUserInfo(this.getUserId())
	if usr == nil {
		log.Release("user.setUserDecoration fail to query user info")
		this.WriteMsg(msg, "user not found")
		return
	}
	var req userservices.UserDecoration
	err := json.Unmarshal([]byte(data), &req)
	if err != nil {
		retData := fmt.Sprintf("setUserDecoration Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	userservices.SetUserDecoration(this.getUserId(), req.Type, req.ItemId)
	this.WriteMsg(msg, "ok")
}

func (this *user) getGiftCardHistory(msg, data string) {
	this.WriteMsg(msg, inventory.GetGiftCardHistory(this.getUserId()))
}
