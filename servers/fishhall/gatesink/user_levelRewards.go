package gatesink

import (
	"bet24.com/log"
	activityservice "bet24.com/servers/micros/activityservice/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	"encoding/json"
)

// 处理等级礼包指令
func (this *user) onLevelRewardsMsg(msg, data string) {
	switch msg {
	case "getLevelRewards": // 获取等级礼包
		this.getLevelRewards(msg)
	case "claimLevelRewards": // 领取等级奖励
		this.claimLevelRewards(msg, data)
	default:
		log.Release("user.onLevelRewardsMsg unhandled msg %s", msg)
	}
}

// 获取等级礼包
func (this *user) getLevelRewards(msg string) {
	respStr := activityservice.GetLevelRewards(this.getUserId())
	this.WriteMsg(msg, respStr)
	return
}

// 领取等级奖励
func (this *user) claimLevelRewards(msg, data string) {
	var req activityservice.Request
	var ret struct {
		Success bool
		Data    []item.ItemPack
	}

	if err := json.Unmarshal([]byte(data), &req); err != nil {
		log.Error("user.claimLevelRewards unmarshal fail %s", data)
		d, _ := json.Marshal(ret)
		this.WriteMsg(msg, string(d))
		return
	}
	ret.Success, ret.Data = activityservice.ClaimLevelRewards(this.getUserId(), req.Level, req.IsAll)
	d, _ := json.Marshal(ret)
	this.WriteMsg(msg, string(d))
	return
}
