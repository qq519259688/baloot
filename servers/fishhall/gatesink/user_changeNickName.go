package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/config"
	cash "bet24.com/servers/micros/money/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
	"bet24.com/servers/transaction"
	"encoding/json"
	"fmt"
)

// 修改昵称
func (this *user) changeNickName(msg, data string) {
	obj := transaction.NewTransChangeNickName()
	err := json.Unmarshal([]byte(data), &obj.In)
	if err != nil {
		log.Release("changeNickName UnMarshal data failed %v", data)
		this.WriteMsg(msg, fmt.Sprintf("changeNickName UnMarshal data failed %v", data))
		return
	}

	obj.In.NewNickName = coreservice.ParseKeyword(obj.In.NewNickName)

	/*
		//删除头尾空格
		obj.In.NewNickName = strings.TrimSpace(obj.In.NewNickName)

		//昵称长度：[4,30]
		nickNameRegexp := regexp.MustCompile(`^([A-Za-z0-9]+\s*){4,32}$`)

		//昵称非法
		if NickNames := nickNameRegexp.FindAllString(obj.In.NewNickName, -1); len(NickNames) <= 0 {
			log.Debug("changeNickName NickName Illegality NewNickName=%s NickNames=%v", obj.In.NewNickName, NickNames)
			obj.Out.RetCode = -1
			obj.Out.ErrorMsg = "Illegality"
			buf, err := json.Marshal(obj.Out)
			if err != nil {
				log.Release("changeNickName Illegality marshal fail %v", err)
				this.WriteMsg(msg, fmt.Sprintf("changeNickName Illegality marshal fail %v", err))
				return
			}
			this.WriteMsg(msg, string(buf))
			return
		}
	*/

	obj.In.UserID = this.getUserId()

	if changeTimes, gold := this.getChangeNameInfo(); gold > 0 {
		remark := fmt.Sprintf("第%d次修改昵称", changeTimes)
		success := cash.ReduceMoney(this.getUserId(), gold, common.LOGTYPE_CHANGE_NICKNAME, "修改昵称", remark, this.GetIP())
		if !success {
			obj.Out.RetCode = 21
			obj.Out.ErrorMsg = "金币不足"
			buf, err := json.Marshal(obj.Out)
			if err != nil {
				log.Release("changeNickName marshal err %v", err)
				this.WriteMsg(msg, fmt.Sprintf("changeNickName marshal err %v", err))
				return
			}

			// 把修改昵称结果通知客户端
			this.WriteMsg(msg, string(buf))
			return
		}
	}

	obj.DoAction(nil)

	buf, err := json.Marshal(obj.Out)
	if err != nil {
		log.Release("changeNickName marshal err %v", err)
		this.WriteMsg(msg, fmt.Sprintf("changeNickName marshal err %v", err))
		return
	}

	// 刷新coreservice中的用户信息
	go userservices.UpdateUserInfo(this.getUserId())

	// 把修改昵称结果通知客户端
	this.WriteMsg(msg, string(buf))
	return
}

// 修改昵称次数
func (this *user) changeNameInfo(msg, data string) {
	var ret struct {
		ChangeTimes int
		GoldAmount  int
	}

	ret.ChangeTimes, ret.GoldAmount = this.getChangeNameInfo()

	buf, err := json.Marshal(ret)
	if err != nil {
		log.Release("changeNameInfo marshal err %v", err)
		this.WriteMsg(msg, fmt.Sprintf("changeNameInfo marshal err %v", err))
		return
	}

	this.WriteMsg(msg, string(buf))
	return
}

// 获取修改昵称信息
func (this *user) getChangeNameInfo() (int, int) {
	changeTimes, gold := 0, 0

	// 没有修改昵称配置信息
	if len(config.HallConfig.ChangeNames) == 0 {
		return changeTimes, gold
	}

	// 已修改次数
	changeTimes = transaction.ChangeNameTimes(this.getUserId())

	// 修改次数+1
	changeTimes++

	for _, v := range config.HallConfig.ChangeNames {
		gold = v.Gold
		if changeTimes == v.Times {
			break
		}
	}

	return changeTimes, gold
}
