package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
	game "bet24.com/servers/micros/game/proto"
	"encoding/json"
	"fmt"
)

func (this *user) getGameSettleVideoList(msg, data string) {
	list := client.GetGameSettleVideoList(this.getUserId())
	for _, v := range list {
		if info := game.GetGame(v.GameID); info != nil {
			v.ChineseName = info.ChineseName
		}
	}
	buf, _ := json.Marshal(list)
	this.WriteMsg(msg, string(buf))
	return
}

func (this *user) awardGameSettleVideo(msg, data string) {
	retData := ""
	var req protocol.AwardGameSettleVideo_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user.awardGameSettleVideo unmarshal data fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.AwardGameSettleVideo(this.getUserId(), req.SettleId)
	if resp.RetCode != 1 {
		log.Debug("user.awardGameSettleVideo failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}
