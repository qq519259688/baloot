package gatesink

import (
	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
)

// 公告列表
func (this *user) announceList(msg, data string) {
	resp := client.AnnounceList()
	if resp.RetCode != 1 {
		log.Debug("announceList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}
