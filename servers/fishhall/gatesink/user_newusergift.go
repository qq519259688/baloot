package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

func (this *user) getNewUserGift(msg string) {
	resp := client.GetNewUserGiftInfo(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.getNewUserGift failed %v", resp)
		resp.Data = ""
	}

	this.WriteMsg(msg, resp.Data)
}

func (this *user) receiveNewUserGift(msg, data string) {
	retData := ""
	var req protocol.ReceiveNewUserGift_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("er.receiveNewUserGift unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.ReceiveNewUserGift(this.getUserId(), req.IsDouble)
	if resp.RetCode != 1 {
		log.Debug("user.receiveNewUserGift failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}

func (this *user) getNewYearGift(msg string) {
	resp := client.GetNewYearGiftInfo(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.getNewYearGift failed %v", resp)
		resp.Data = ""
	}

	this.WriteMsg(msg, resp.Data)
}

func (this *user) receiveNewYearGift(msg, data string) {
	retData := ""
	var req protocol.ReceiveNewUserGift_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("er.receiveNewYearGift unmarshal fail %v", err)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	resp := client.ReceiveNewYearGift(this.getUserId(), req.IsDouble)
	if resp.RetCode != 1 {
		log.Debug("user.receiveNewYearGift failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
	return
}
