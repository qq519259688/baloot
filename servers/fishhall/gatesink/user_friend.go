package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

// 搜索玩家（兼容旧接口协议）
func (this *user) getSearchUserinfo(msg, data string) {

	var req protocol.Friend_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("getSearchUserinfo Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.GetSearchUserinfo(this.getUserId(), req.TargetUserID, req.TargetNickName)
	if resp.RetCode != 1 {
		log.Debug("user.getSearchUserinfo failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

// 搜索玩家列表
func (this *user) getSearchUserList(msg, data string) {
	var req protocol.Friend_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("getSearchUserList Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.GetSearchUserList(this.getUserId(), req.TargetUserID, req.TargetNickName)
	if resp.RetCode != 1 {
		log.Debug("user.getSearchUserList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

// 获取好友列表
func (this *user) getFriendList(msg, data string) {
	resp := client.GetFriendList(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.GetFriendList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

// 好友审核列表
func (this *user) friendVerifyList(msg, data string) {
	resp := client.FriendVerifyList(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.friendVerifyList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

// 添加好友
func (this *user) friendApply(msg, data string) {
	var req protocol.Friend_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("Friend_req Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.FriendApply(this.getUserId(), req.TargetUserID)
	if resp.RetCode != 1 {
		log.Debug("user.friendApply failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

// 删除好友
func (this *user) delFriend(msg, data string) {
	var req protocol.Friend_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("Friend_req Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.DelFriend(this.getUserId(), req.TargetUserID)
	if resp.RetCode != 1 {
		log.Debug("user.DelFriend failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

// 好友申请处理
func (this *user) friendHandleApply(msg, data string) {
	var req protocol.Friend_HandleApply_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("Friend_HandleApply_req Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.FriendHandleApply(this.getUserId(), req.TargetUserID, req.Apply)
	if resp.RetCode != 1 {
		log.Debug("user.FriendHandleApply failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

// 赠送礼物
func (this *user) friendGiveGift(msg, data string) {
	var req protocol.Friend_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("Friend_req Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.FriendGiveGift(this.getUserId(), req.TargetUserID)
	if resp.RetCode != 1 {
		log.Debug("user.friendGiveGift failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

// 获得礼物
func (this *user) friendGetGift(msg, data string) {
	var req protocol.Friend_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("Friend_req Unmarshal data failed %v", data)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	resp := client.FriendGetGift(this.getUserId(), req.TargetUserID, this.GetIP())
	if resp.RetCode != 1 {
		log.Debug("user.friendGetGift failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) friendRoomInvite(msg, data string) {
	var req protocol.FriendRoomInvite_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user.friendRoomInvite unmarshal data fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	client.FriendRoomInvite(this.getUserId(), this.getNickName(), req.ToUserId, req.RoomNo)
	this.WriteMsg(msg, "")
}

func (this *user) friendGetRoomInviteList(msg, data string) {
	resp := client.FriendGetRoomInviteList(this.getUserId())
	if resp.RetCode != 1 {
		log.Debug("user.friendGetRoomInviteList failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}

func (this *user) friendRoomInviteInvalid(msg, data string) {
	var req protocol.FriendRoomInvite_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user.friendRoomInviteInvalid unmarshal data fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	client.FriendRoomInviteInvalid(this.getUserId(), req.RoomNo)
	this.WriteMsg(msg, "")
}

func (this *user) getMaxFriendCount(msg string) {
	resp := client.GetMaxFriendCount(this.getUserId())
	this.WriteMsg(msg, resp.Data)
}

func (this *user) getPotentialFriendList(msg string) {
	this.WriteMsg(msg, client.FriendGetPotentialList(this.getUserId()))
}

// 黑名单列表
func (this *user) friendGetBlackList(msg, data string) {
	var req protocol.FriendBlack_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user.friendGetBlackList unmarshal data fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	jsonData := client.FriendGetBlackList(this.getUserId(), req.PageIndex, req.PageSize)
	this.WriteMsg(msg, jsonData)
	return
}

// 添加黑名单
func (this *user) friendAddBlack(msg, data string) {
	var req protocol.FriendBlack_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user.friendAddBlack unmarshal data fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	rsp := client.FriendAddBlack(this.getUserId(), req.ToUserId)
	this.WriteMsg(msg, rsp.Data)
	return
}

// 删除黑名单
func (this *user) friendDelBlack(msg, data string) {
	var req protocol.FriendBlack_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user.friendDelBlack unmarshal data fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}
	rsp := client.FriendDelBlack(this.getUserId(), req.ToUserId)
	this.WriteMsg(msg, rsp.Data)
	return
}
