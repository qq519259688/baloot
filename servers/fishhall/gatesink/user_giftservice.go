package gatesink

import (
	"bet24.com/log"
	gift "bet24.com/servers/micros/giftservice/proto"
	"encoding/json"
	"fmt"
)

// 处理徽章指令
func (this *user) onGiftServiceMsg(msg, data string) {
	switch msg {
	case "GiftService_GetGiftList":
		this.GiftService_GetGiftList(msg, data)
	case "GiftService_SendGift":
		this.GiftService_SendGift(msg, data)
	case "GiftService_GetUnclaimedGifts":
		this.GiftService_GetUnclaimedGifts(msg, data)
	case "GiftService_ClaimUserGift":
		this.GiftService_ClaimUserGift(msg, data)
	case "GiftService_CancelChargeGift":
		this.GiftService_CancelChargeGift(msg, data)
	case "GiftService_GetReceivedRecord":
		this.GiftService_GetReceivedRecord(msg, data)
	default:
		log.Release("user.onGiftServiceMsg unhandled msg %s", msg)
	}
}

func (this *user) GiftService_GetGiftList(msg, data string) {
	this.WriteMsg(msg, gift.GetGiftListString(this.getUserId()))
}

func (this *user) GiftService_SendGift(msg, data string) {
	var req struct {
		ToUserId int
		GiftId   int
	}
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("GiftService_SendGift Unmarshal data failed %v", data)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	var ret struct {
		RetCode int
		Extra   string
	}

	ret.RetCode, ret.Extra = gift.SendGift(this.getUserId(), req.ToUserId, req.GiftId)

	d, _ := json.Marshal(ret)
	this.WriteMsg(msg, string(d))
}

func (this *user) GiftService_GetUnclaimedGifts(msg, data string) {
	this.WriteMsg(msg, gift.GetUnclaimedGiftsString(this.getUserId()))
}

func (this *user) GiftService_ClaimUserGift(msg, data string) {
	var req struct {
		RID int
	}
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("GiftService_ClaimUserGift Unmarshal data failed %v", data)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	this.WriteMsg(msg, gift.ClaimUserGift(this.getUserId(), req.RID))
}

func (this *user) GiftService_CancelChargeGift(msg, data string) {
	var req struct {
		ToUserId  int
		ProductId string
	}
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("GiftService_CancelChargeGift Unmarshal data failed %v", data)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}
	gift.CancelChargeGift(this.getUserId(), req.ToUserId, req.ProductId)
	this.WriteMsg(msg, "")
}

func (this *user) GiftService_GetReceivedRecord(msg, data string) {
	this.WriteMsg(msg, gift.GetReceivedGiftRecord(this.getUserId()))
}
