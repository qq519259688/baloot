package gatesink

import (
	"bet24.com/redis"
	"fmt"
)

func (this *user) getMyGameStatus(msg string) {
	// userGameStatus:UserID:123456
	keyName := fmt.Sprintf("UserGameStatus:UserID:%v:", this.getUserId())
	d, _ := redis.String_Get(keyName)

	this.WriteMsg(msg, d)
}
