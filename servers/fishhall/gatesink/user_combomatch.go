package gatesink

import (
	"bet24.com/log"
	combomatch "bet24.com/servers/micros/matches/proto"
	"encoding/json"
	"fmt"
	"strconv"
)

func (this *user) onComboMatchMsg(msg, data string) {
	switch msg {
	case "enrollComboMatch":
		this.enrollComboMatch(msg, data)
	case "quitComboMatch":
		this.quitComboMatch(msg, data)
	case "getComboMatchList":
		this.getComboMatchList(msg, data)
	case "getComboMatchHistory":
		this.getComboMatchHistory(msg, data)
	case "getUserComboMatchId":
		this.getUserComboMatchId(msg, data)
	case "getComboMatchInfo":
		this.getComboMatchInfo(msg, data)
	case "getComboMatchConfirmCount":
		this.getComboMatchConfirmCount(msg, data)
	case "confirmComboMatch":
		this.confirmComboMatch(msg, data)
	}

}

type enrollComboMatchReq struct {
	MatchId  int
	FeeIndex int
}

func (this *user) enrollComboMatch(msg, data string) {
	var req enrollComboMatchReq
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user.enrollComboMatch unmarshal data fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	var retInfo struct {
		MatchNo int
		ErrMsg  string
	}
	retInfo.MatchNo, retInfo.ErrMsg = combomatch.EnrollComboMatch(this.getUserId(), req.MatchId, this.getNickName(), this.getFaceId(), this.getFaceUrl(), req.FeeIndex)
	d, _ := json.Marshal(retInfo)
	this.WriteMsg(msg, string(d))
}

func (this *user) quitComboMatch(msg, data string) {
	matchId, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.quitComboMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	ok := combomatch.QuitComboMatch(this.getUserId(), matchId)
	retData := "success"
	if !ok {
		retData = "failed"
	}
	this.WriteMsg(msg, retData)
}

func (this *user) getComboMatchList(msg, data string) {
	d := combomatch.GetComboMatchList(this.getUserId())
	//log.Release("getComboMatchHistory %d data = %s", this.getUserId(), d)
	this.WriteMsg(msg, d)
}

func (this *user) getComboMatchHistory(msg, data string) {
	this.WriteMsg(msg, combomatch.GetComboMatchHistory(this.getUserId()))
}

func (this *user) getUserComboMatchId(msg, data string) {
	this.WriteMsg(msg, combomatch.GetUserComboMatchId(this.getUserId()))
}

func (this *user) getComboMatchInfo(msg, data string) {
	matchId, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.quitComboMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	d := combomatch.GetComboMatchInfo(matchId, this.getUserId())
	//log.Release("getComboMatchHistory %d data = %s", this.getUserId(), d)
	this.WriteMsg(msg, d)
}

func (this *user) getComboMatchConfirmCount(msg, data string) {
	matchId, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.getComboMatchConfirmCount %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	count := combomatch.GetComboMatchConfirmCount(matchId)
	this.WriteMsg(msg, fmt.Sprintf("%d", count))
}

func (this *user) confirmComboMatch(msg, data string) {
	matchId, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.confirmComboMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	if combomatch.ComboMatchConfirm(matchId, this.getUserId()) {
		this.WriteMsg(msg, "success")
	} else {
		this.WriteMsg(msg, "failed")
	}

}
