package gatesink

import (
	"fmt"
	"strconv"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
)

func (this *user) getChipWheelConfig(msg string) {
	userId := this.getUserId()
	if userId == 0 {
		return
	}
	list := client.GetChipWheelConfig()
	this.WriteMsg(msg, list)
}

func (this *user) doChipWheel(msg, data string) {
	userId := this.getUserId()
	if userId == 0 {
		return
	}
	bet, err := strconv.Atoi(data)
	if err != nil {
		log.Release("user.doChipWheel invalid argument [%d] %s", userId, data)
		return
	}
	retMsg := fmt.Sprintf("%d", client.ChipWheel(this.getUserId(), bet, this.GetIP()))
	this.WriteMsg(msg, retMsg)
}

func (this *user) getChipWheelHistory(msg string) {
	userId := this.getUserId()
	if userId == 0 {
		return
	}
	list := client.GetChipWheelHistory(userId)
	this.WriteMsg(msg, list)
}
