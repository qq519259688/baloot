package gatesink

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/fishhall/protocol"
)

func (this *user) trackRecord(msg, data string) {
	var req protocol.TrackRecord_req
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("trackRecord unmarshal data failed %v", data)
		log.Error(retData)
		this.WriteMsg(msg, retData)
		return
	}

	userId := this.getUserId()
	if userId == 0 {
		req.Level_2 = req.Level_2 + this.GetIP()
	}
	resp := client.TrackRecord(userId, req.Level_1, req.Level_2, req.Level_3)
	if resp.RetCode != 1 {
		log.Debug("user.trackRecord failed %v", resp)
	}
	this.WriteMsg(msg, resp.Data)
}
