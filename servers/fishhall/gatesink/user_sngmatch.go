package gatesink

import (
	"bet24.com/log"
	sngmatch "bet24.com/servers/micros/matches/proto"
	"encoding/json"
	"fmt"
	"strconv"
)

func (this *user) onSngMatchMsg(msg, data string) {
	switch msg {
	case "enrollSngMatch":
		this.enrollSngMatch(msg, data)
	case "quitSngMatch":
		this.quitSngMatch(msg, data)
	case "getSngMatchList":
		this.getSngMatchList(msg, data)
	case "getSngMatchHistory":
		this.getSngMatchHistory(msg, data)
	case "getUserSngMatchId":
		this.getUserSngMatchId(msg, data)
	}

}

type enrollSngMatchReq struct {
	MatchId  int
	FeeIndex int
}

func (this *user) enrollSngMatch(msg, data string) {
	var req enrollSngMatchReq
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user.enrollSngMatch unmarshal data fail %v", err)
		log.Release(retData)
		this.WriteMsg(msg, retData)
		return
	}

	var retInfo struct {
		MatchNo int
		ErrMsg  string
	}
	retInfo.MatchNo, retInfo.ErrMsg = sngmatch.EnrollSngMatch(this.getUserId(), req.MatchId, this.getNickName(), this.getFaceId(), this.getFaceUrl(), req.FeeIndex)
	d, _ := json.Marshal(retInfo)
	this.WriteMsg(msg, string(d))
}

func (this *user) quitSngMatch(msg, data string) {
	matchId, err := strconv.Atoi(data)
	if err != nil {
		log.Release("gatesink.quitSngMatch %v", err)
		this.WriteMsg(msg, "invalid argument")
		return
	}
	ok := sngmatch.QuitSngMatch(this.getUserId(), matchId)
	retData := "success"
	if !ok {
		retData = "failed"
	}
	this.WriteMsg(msg, retData)
}

func (this *user) getSngMatchList(msg, data string) {
	d := sngmatch.GetSngMatchList(this.getUserId())
	//log.Release("getSngMatchHistory %d data = %s", this.getUserId(), d)
	this.WriteMsg(msg, d)
}

func (this *user) getSngMatchHistory(msg, data string) {
	this.WriteMsg(msg, sngmatch.GetSngMatchHistory(this.getUserId()))
}

func (this *user) getUserSngMatchId(msg, data string) {
	this.WriteMsg(msg, fmt.Sprintf("%d", sngmatch.GetUserSngMatchId(this.getUserId())))
}
