package main

import (
	"bet24.com/log"
	"bet24.com/redis"
	coreservice "bet24.com/servers/coreservice/client"
	_ "bet24.com/servers/coreservice/servicesink"
	"bet24.com/servers/fishhall/config"
	"bet24.com/servers/fishhall/gatesink"
	slotmanager "bet24.com/servers/games/slotcommon/manager"
	"bet24.com/servers/games/slotcommon/usermanager"
	"bet24.com/servers/insecureframe"
	"bet24.com/servers/insecureframe/gate"
	micro_common "bet24.com/servers/micros/common"
	game "bet24.com/servers/micros/game/proto"
	notification "bet24.com/servers/micros/notification/proto"
	_ "bet24.com/servers/monitor"
	"bet24.com/servers/transaction"
	"bet24.com/utils"
	_ "encoding/json"
	"fmt"
	_ "github.com/smallnest/rpcx/protocol"
	"math/rand"
	"os"
	"strconv"
	"time"
)

var stopping bool

func waitInput() {
	for {
		var cmd string
		var param1 string
		var param2 string
		fmt.Scanf("%s %s %s", &cmd, &param1, &param2)
		switch cmd {
		case "exit":
			stopping = true
			gate.StopServer()
		case "slotuser":
			usermanager.Dump(param1, param2)
		case "slotconfig":
			log.Release("%v", slotmanager.GetConfigList(param1))
		case "getslotconfig":
			log.Release("%v", slotmanager.GetConfig(param1, param2))
		case "listuser":
			gate.DumpUsers()
		case "clientpool":
			micro_common.DumpClientPools()
		case "messagerecord":
			userId, err := strconv.Atoi(param1)
			if err != nil {
				userId = 0
			}
			gate.DumpMessageRecord(userId)
		case "clearrecord":
			userId, err := strconv.Atoi(param1)
			if err != nil {
				userId = 0
			}
			gate.ClearMessageRecord(userId)
		default:
			log.Release("unknown command")
		}
	}
}

func main() {
	defer waitInput()

	rand.Seed(time.Now().UnixNano())

	redis.InitPool(config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB)
	coreservice.SetServiceAddr(config.Server.ServiceAddr)
	config.Run()
	//monitor.Run(config.Server.MonitorPort)
	logFilename := "log/fishhall/err.log"
	// redirect stdout and stderr to log file
	logFile, _ := os.OpenFile(logFilename, os.O_WRONLY|os.O_CREATE|os.O_SYNC|os.O_APPEND, 0644)
	utils.RedirectStderr(logFile)
	os.Stderr.Write([]byte(fmt.Sprintf("[%v]fishhall starting\n", time.Now())))
	log.Release("fishhall.run config = %v", config.HallConfig.ThirdPaymentOpen)
	if config.Server.TestPay {
		log.Error("TestPay enabled")
	}
	transaction.Run()

	// 对用户的服务永远放在最后
	gatesink.Run()
	slotmanager.RunAllSlots(gatesink.Sink)
	insecureframe.Run(gatesink.Sink,
		config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB, config.Server.LogPath)

	pollAddrToService()

	go notification.Subscribe(gatesink.Sink.OnNotification, config.Server.ServerPort)
	log.Release("Server started %v", time.Now())
}

func pollAddrToService() {
	if !gatesink.Sink.IsChipRoom() {
		return
	}
	if stopping {
		return
	}
	time.AfterFunc(10*time.Second, pollAddrToService)
	game.GamePolling(fmt.Sprintf("wss://%s:%d", config.Server.ServerIP, config.Server.ServerPort), gate.GetUserCount())
}
