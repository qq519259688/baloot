package protocol

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

/*
	getShopList
	上行 : ShopItem_req
	下行 : []*Shop_Item

	ShopType_Gold    // 0 = 金币
	ShopType_Diamond // 1 = 钻石
	ShopType_Cannon  // 2 = 炮台
	ShopType_Gift    // 3 = 礼包
	ShopType_Max

	type Shop_Item struct {
		ProductID   string
		ProductName string
		Amount      int
		Price       int // 单位为钻石数量或者人民币分
		IsHot       int
		Bonus       int    // 多送百分比
		Extra       string // 扩展信息
	}
*/

type GetShopList_req struct {
	ShopType  int
	ProductId string // 具体的productId,如果不传，则取列表
}

/*
	shopExchange
*/

type ShopExchange_req struct {
	ProductId string
}

type InventoryCosume_req struct {
	ItemId int
	Count  int
}

type InventoryGift_req struct {
	ToUserId int
	ItemId   int
	Count    int
}

type InventoryCosume_resp struct {
	ItemId    int
	Succeeded bool
	ErrMsg    string
}

/*
	签到

	getSigninInfo
		无上行，获取签到列表

		type Signin struct {
			Id             int
			Award          item.ItemPack
			DoubleVipLevel int // vip？加倍，0表示没有
			SignTime       int // 签到时间，0表示未签
		}

		type ContinueAward struct {
			Day       int // 连续多少天
			Award     []item.ItemPack
			Completed bool // 是否已领取
		}

		type SigninInfo struct {
			SigninTable []*Signin
			Continue    []*ContinueAward
			Signable    bool      // 当前是否可签到
		}`

	-----------------------------------------
	doSignin
		无上行
		下行：	[]item.ItemPack
		签到成功后，返回签到奖励列表，如果没有，表示签到失败
*/

/*
	任务

	const (
		TaskStatus_active   = iota // 任务进行中  0
		TaskStatus_complete        // 完成
		TaskStatus_awarded         // 已领取奖励
		TaskStatus_inactive        // 未开始
		TaskStatus_expired         // 已过期
	)


	type Task struct {
		Id           int
		Title        string
		Desc         string
		Action       int
		Target       int
		Repeatable   bool        // 是否可重复
		Duration     int         // 任务有效期
		PreTask      int         // 如果不为0，则需要完成前置任务才创建，否则自动创建
		Limited_time Task_Period // 如果是限时开放的任务，则在这里添加时效
		Awards       []item.ItemPack
	}

	type UserTask struct {
		TaskId      int
		Scheduled   int // 完成进度
		Status      int
		Task_Period // 任务对玩家的时效，时效内已完成，如果没有后置任务，则不重置，等时效结束后再创建
	}

	type Task_Period struct {
		Start    int // 开始时间戳 秒 0表示无时效
		Duration int // 持续时间 秒
	}

	getSysTask 获取系统任务信息
		上行 : {TaskId:taskId}
		下行 : Task

	getSysTaskList 获取所有系统任务
		无上行
		下行 : []Task

	getUserTaskList 获取我当前任务
	    无上行
	    下行 ： []UserTask

	awardTask 领取奖励
	    上行 : {TaskId:taskId}
	    下行 : {Success bool,ErrMsg string}
*/

type AwardTask_req struct {
	TaskId int
}

/*
	背包

	// Item.Type
	const (
		Item_Gold     = iota // 金币
		Item_Diamond         // 钻石
		Item_Vitality        // 活跃度
		Item_Cannon          // 炮台
		Item_Consume         // 其他消耗品
	)

	type Item struct {
		Id        int
		ActiveId  int
		Type      int
		Name      string
		Desc      string
		Start     int    // 开始时间戳 秒 0表示无时效
		Duration  int    // 持续时间 秒
		ExtraInfo string // 其他特效
	}

	// 实体道具
	type ItemPack struct {
		ItemId int
		Count  int
	}

	type UserItem struct {
		ItemId    int // 道具ID
		ActiveId  int // 时效道具使用中的ID， 0表示非时效道具
		Type      int
		Count     int    // 道具数量
		Start     int    // 开始时间戳 秒 0表示还未开始使用
		Duration  int    // 持续时间 秒
		ExtraInfo string // 其他特效
	}

	getSysItems
	    获取物品系统表
	    无上行
	    下行 : []Item


	getUserItems
		获取我的道具列表
		无上行
		下行 : []UserItem

	consumeItem
		使用道具
		上行 : InventoryCosume_req
		下行 : {Succeeded bool,ErrMsg string}
*/

/*
	通知推送
	notification
	上行： 无，客户端登录后主动注册
	下行： []Notification_push

		const (
			Notification_Task      = iota // 任务
			Notification_Vip              // vip等级变化
			Notification_Inventory        // 背包
			Notification_Cannon           // 炮台
			Notification_Gold             // 金币
			Notification_Diamond          // 钻石
		)
*/

type Notification_push struct {
	Type int
	Data string
}

type GiftContinueAward_req struct {
	Day int
}

type SendUserMail_req struct {
	Title   string
	Content string
	Img     string
}

type GetUserMail_req struct {
	MailId int
}

type SendSysMail_req struct {
	Title      string          //标题
	Content    string          //内容
	Status     int             //状态
	SourceName string          //源名称
	Crdate     int             //时间戳
	Tools      []item.ItemPack //附件内容
}

type UpdateSysMail_req struct {
	SysMsgId int
	Status   int
}

type DelSysMail_req struct {
	SysMsgId int
}

type RankList_req struct {
	RankType int
	Num      int
}

type RankHistoryList_req struct {
	DateFlag string
}

type RankAward_req struct {
	RankType   int
	DoubleFlag int
}

/*
转盘
prizeWheelConfig 无参数
doWheel
*/
type PrizeWheel_req struct {
	Times int
}

/*
	type Prize struct {
		Id     int // 客户端排序用
		Chance int `json:"-"` // 概率 万分之
		Items  item.ItemPack
	}
	type PrizeWheel_resp struct {
		Prizes []*prizewheel.Prize
		ErrMsg string
	}
*/

type SaveRealName_req struct {
	RealName string
	IDCard   string
}

/*
	礼包
		包含策划文档中3个模块内容
			限时礼包
			每日特惠
			首充礼包

		const (
			GiftPackLimit_None     = iota // 无限制
			GiftPackLimit_Once            // 只能购买一次
			GiftPackLimit_EveryDay        // 每天购买一次
		)

		const (
			GiftPackType_First       = iota // 首充礼包
			GiftPackType_TimeLimited        // 限时礼包
			GiftPackType_EveryDay           // 每日特惠
		)

		type GiftPack struct {
			Id    int
			Price int
			Limit int
			Items []item.ItemPack
			//PreGiftPack int `json:",omitempty"` // 前置礼包，必须购买前置礼包才能购买本礼包，这里默认要求连续购买
			Type       int // 礼包类型
			Start      int `json:",omitempty"` // 如果是限时礼包
			Duration   int `json:",omitempty"`
			Group      int `json:",omitempty"` // 每日特惠中，不同价格用不同Group区分
			GroupIndex int `json:",omitempty"` // 同组顺序
		}

	协议
		getGiftPacks 获取系统礼包
		上行 无
		下行 map[int]GiftPack

		getBuyableGiftPacks 获取当前可购买的礼包
		上行 无
		下行 []UserBuyable
			type UserBuyable struct {
				GiftPackId int  // 礼包ID
				Buyable    bool // 是否可购买
				TimeLeft   int  // 剩余时间
			}

		buyGiftPack 购买礼包，测试用
		上行： BuyGiftPack_req
		下行： 无
*/

type BuyGiftPack_req struct {
	ProductID string
}

/*
	成长礼包
		const (
			GrowthTermStatus_none     = iota // 没有
			GrowthTermStatus_complete        // 可领取
			GrowthTermStatus_awarded         // 已领取
		)
		type GrowthPack struct {
			Id    int          // 礼包ID
			Name  string       // 名称
			Price int          // 价格
			Terms []GrowthTerm // 礼包列表
		}

		type GrowthTerm struct {
			TermIndex int             // index
			Name      string          // 名称
			Bullet    int             // 解锁子弹
			Items     []item.ItemPack // 礼包内容
		}

		type UserGrowthTerm struct {
			UserId       int // 用户ID，客户端不发
			GrowthPackId int // 礼包ID
			TermIndex    int // index
			Bullet       int // 目标炮台等级
			Status       int // 目前状态
		}

		协议：
			getGrowthPacks 获取系统礼包
				[]GrowthPack
			buyGrowthPack 购买礼包，测试用
				BuyGiftPack_req
			getGrowthPackTerms 获取所有礼包状态
			    []UserGrowthTerm
			awardGrowthPack 领取礼包奖励
			    GrowthPackAward_req

*/

type GrowthPackAward_req struct {
	GiftPackId int
	Index      int
}

type VipAddPoint_req struct {
	Point int
}

type GetDiamond_resp struct {
	Success bool
	Diamond int
}

type GetMoney_resp struct {
	Success bool
	Gold    int
}

type GetVitality_resp struct {
	DayPoint  int //日活跃度
	WeekPoint int //周活跃度
}

/*
	防沉迷通知
	pre_addiction
*/

/*
	实物兑换
	getExchangeList 获取可兑换的列表
	exchangeGift    进行兑换


	type ExchangeInfo struct {
		Id        int
		LeftCount int  // 剩余数量，如果小于0，则不限量，等于0表示缺货
		LimitOnce bool // 是否限制购买一次
		VipNeed   int  // 所需VIP等级
		Price     int  // 价格
		Items     []item.ItemPack
	}

*/

type ExchangeGift_req struct {
	ExchangeId int
	Num        int
	Remark     string
}

type ExchangeHistory_req struct {
	PageIndex int
	PageSize  int
}

type BetList_req struct {
	GameID    int // 游戏ID
	Days      int // 天数(0=今天  1=昨天  7=最近一周  all=最近30天)
	PageIndex int // 页索引
	PageSize  int // 页大小
	BetZone   string
}

type CashList_req struct {
	BeginTime string
	EndTime   string
	PageIndex int
	PageSize  int
}

// 保险柜
type BankInOrOut_req struct {
	Amount     int
	GameID     int
	ServerName string
}

type BankInOrOut_resp struct {
	RetCode    int
	Gold       int //身上金币
	BankAmount int //保险柜金币
	OutMsg     string
}

// 聊天
type Chat_req struct {
	ChannelID  int    //聊天频道
	RecvUserID int    //私聊备用
	Msg        string //内容
	MsgType    int
}

// 好友
type Friend_req struct {
	TargetUserID   int    //搜索，申请，删除， 赠送礼物，领取礼物
	TargetNickName string // 昵称
}
type Friend_HandleApply_req struct {
	TargetUserID int
	Apply        int //0：不同意； 1：同意
}

type SpreadApply_req struct {
	Code int
}

type SpreadMember_req struct {
	PageIndex int
	PageSize  int
}

type SpreadGift_req struct {
	FromUserID int
}

// vip
/*
	type VipPrivilege struct {
		Privilege int `json:"pid"`
		Param     int `json:"p"`
	}


	type VipInfo struct {
		Level      int             //等级
		Point      int             //分数
		Desc       string          //描述
		GiftPack   []item.ItemPack //升级礼包(json格式)
		Privileges []VipPrivilege
	}

	获取vip配置表
	getVipInfo()
	返回: []VipInfo

	获取我的VIP信息
	getUserVip()
	返回：VipLevel,VipPoint
*/

// 签到
/*
	// 签到历史记录
	type SigninWheelHistory struct {
		UserId   int
		NickName string
		FaceId   int
		FaceUrl  string
		Time     int
		SigninWheelResult
	}

	// 签到配置
	type config_param struct {
		Param  int // 参数
		Chance int `json:"-"` // 概率,客户端看不到
	}

	type SigninWheelConfig struct {
		totalBaseChance     int
		totalMultipleChance int
		Base                []config_param // 基础配置
		Multiple            []config_param // 倍数配置
	}

	type UserSiginWheelInfo struct {
		CanSignin bool     // 我是否可以签到
		SigninWheelConfig   // 签到配置信息
	}

	// 签到结果，如果为0则表示签到失败
	type SigninWheelResult struct {
		Base     int // 基础数值
		Multiple int // 倍率
	}

	三个协议，都不带参数
	getSigninWheelInfo     输出 UserSiginWheelInfo
	doSigninWheel          输出 SigninWheelResult
	getSigninWheelHistory  输出 SigninWheelHistory
*/

type AchievementInfo_req struct {
	TaskID int
}

type AchievementUpdate_req struct {
	TaskID int
	Status int
}

type GetGameCount_req struct {
	UserID int
}

type TrackRecord_req struct {
	Level_1 string // 1级类目
	Level_2 string // 2级类目
	Level_3 string // 3级类目
}

type Review_req struct {
	AppName string
}

// 绑定相关
type BindingInfo struct {
	FacebookBound int
	PhoneBound    int
}

type BindFacebook_req struct {
	OpenId   string // openid
	NickName string // 昵称
	FaceUrl  string // 头像URL
}

type BindPhone_req struct {
	PhoneNumber string
	SmsCode     string
}

type Bind_resp struct {
	RetCode int
	ErrMsg  string
}

type AwardGameSettleVideo_req struct {
	SettleId int
}

type ReceiveNewUserGift_req struct {
	IsDouble int // 是否双倍（视频广告）
}

type TeacherBind struct {
	TeacherId int
}

type TeacherProfit struct {
	Days      int
	PageIndex int
	PageSize  int
}

type TeacherPlayVideo struct {
	Rid       int
	StudentId int
}

type AgentBind struct {
	Code int
}

type Agent struct {
	FromUserId int
	PageIndex  int
	PageSize   int
}

type AgentSetBindSend struct {
	BindSend int
}

type AgentSendBindSend struct {
	ToUserID int
}

type AgentCommissionRank struct {
	Days      int
	PageIndex int
	PageSize  int
}

type AgentApply struct {
	Memo string
}

type AgentGroup struct {
	Id   int
	Name string
	Url  string
}

type AwardCouponTask_req struct {
	UserTaskId int
}
type Transfer_req struct {
	ToUserID int
	Amount   int
}

type Transfer_resp struct {
	Success bool
	ErrMsg  string
}

type SaveBankInfo_req struct {
	RealName string // 真实姓名
	BankName string // 银行名称
	BankCode string // 银行码
	BankCard string // 银行卡号
	Mobile   string // 手机号
}

type Award_req struct {
	HallType int // 0=无效  1=金币大厅  2=元宝大厅  3=不区分大厅类型
}

type RechargeCard_req struct {
	CardNo string
}

type SaveCountry_req struct {
	CountryName string
	Currency    string
}

type FriendRoomInvite_req struct {
	ToUserId int
	RoomNo   int
}

type FriendBlack_req struct {
	ToUserId  int
	PageIndex int
	PageSize  int
}
