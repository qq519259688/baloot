package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	slog "bet24.com/log"
	"bet24.com/utils"
)

var Server struct {
	LogLevel        string
	FileLevel       string
	LogPath         string
	CertFile        string
	KeyFile         string
	MaxConnNum      int
	Login           string
	Password        string
	Database        string
	Datasource      string
	GameDB          string
	ChannelUrl      string
	ChannelPassword string
	RedisDB         int
	LastHour        int
	ServiceAddr     string
	//
	ServerName     string
	ServerIP       string
	ServerPort     int
	ServerAddr     string
	MonitorPort    int
	OfflineSeconds int64
	IsChipRoom     int
	LoseTax        int //输家写分
}

var RoomConfigName = "blitzludo"

// func GetRoomInfo() string {
// 	d, _ := json.Marshal(Room)
// 	return string(d)
// }

func init() {
	configFile := "blitzludo/server.json"
	data, err := os.ReadFile(configFile)
	if err != nil {
		log.Fatalf("read config failed blitzludo/server.json %v", err)
	}
	fmt.Println(string(data))
	err = json.Unmarshal(data, &Server)
	if err != nil {
		log.Fatalf("Unmarshal config failed blitzludo/server.json err:%v", err)
		return
	}

	logger, err := slog.New(Server.LogLevel, Server.FileLevel, fmt.Sprintf("%v/%v", Server.LogPath, RoomConfigName), log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	originPort := Server.ServerPort
	for i := 0; i < 100; i++ {
		if utils.CheckPortInUse(Server.ServerPort) {
			Server.ServerPort++
			continue
		}
		break
	}

	if Server.MonitorPort == 0 {
		Server.MonitorPort = Server.ServerPort + 100
	}
	now := time.Now()
	Server.LastHour = now.Hour()

	if originPort != Server.ServerPort {
		// slog.Debug("Port %d in use, change to %d", originPort, Server.ServerPort)
		d, _ := json.Marshal(Server)
		os.WriteFile(configFile, d, 0644)
	}
}

func Run() {
	Rooms = new(RoomList)
	Rooms.lock = &sync.RWMutex{}
	refreshLogFile()
	loadRoomConfig()
}

func refreshLogFile() {
	time.AfterFunc(5*time.Minute, refreshLogFile)
	doRefreshLogFile()
}

func doRefreshLogFile() {
	if Server.LogPath != "" {
		now := time.Now()
		if now.Hour() != Server.LastHour {
			Server.LastHour = now.Hour()
			slog.RecreateFileLog(fmt.Sprintf("%v/%v", Server.LogPath, RoomConfigName), log.LstdFlags)
		}
	}
}
