package gamelogic

import "bet24.com/log"

// 操作结果
type ActionResult struct {
	Action         int //动作信息
	Number         int //骰子数值
	PlaneId        int //操作棋子ID
	OldPosition    int //棋子原来的位置
	Position       int //棋子移动位置
	CrashedChairId int //被撞击座位
	CrashedPlaneId int //被撞击棋子ID
}

type GameScene struct {
	Index        int
	Phase        int
	WhoseTurn    int
	LastTurn     int
	ActionResult ActionResult // 操作信息
	Players      []PlayerInfo
	LeftSec      int //本阶段剩余时间
	tableId      int
	userActions  []userAction
	pool         int
	base         int
}

func (gs *GameScene) dump(includeAction bool) {
	log.Debug("====GameScene Table[%d] Index[%d] pool[%d]====", gs.tableId, gs.Index, gs.pool)
	log.Debug("    Phase[%d] WhoseTurn[%d] LastTurn[%d]", gs.Phase, gs.WhoseTurn, gs.LastTurn)
	log.Debug("    Users:%d", gs.getValidUserCount())
	for i := 0; i < CHAIR_COUNT; i++ {
		if !gs.Players[i].IsValid {
			continue
		}
		gs.Players[i].dump(i)
	}
	if includeAction && len(gs.userActions) > 0 {
		log.Debug("    Actions:%d", len(gs.userActions))
		for _, v := range gs.userActions {
			v.dump()
		}
	}

	log.Debug("---------------------------")
}

func newGameScene(tableId int) *GameScene {
	gs := new(GameScene)
	gs.ctor()
	gs.initData(tableId)
	return gs
}

func (gs *GameScene) ctor() {
	gs.Players = make([]PlayerInfo, CHAIR_COUNT)
}

func (gs *GameScene) initData(tableId int) {
	gs.userActions = []userAction{}
	gs.pool = 0
	for i := 0; i < CHAIR_COUNT; i++ {
		gs.Players[i].initData(0, 0, 0)
	}
	gs.LeftSec = 0
	gs.tableId = tableId
	gs.gameInit(CHAIR_COUNT)
}

func (gs *GameScene) gameInit(whoseTurn int) {
	gs.Index = 0
	gs.Phase = Phase_Free
	gs.WhoseTurn = whoseTurn
	gs.LastTurn = -1
	gs.initActionResult()
	for i := 0; i < CHAIR_COUNT; i++ {
		gs.Players[i].gameInit()
	}
}
