package gamelogic

import (
	"encoding/json"

	"bet24.com/servers/games/blitzludo/config"
	"bet24.com/servers/insecureframe/frame"
)

func newGameSink() *gamesink {
	gs := new(gamesink)
	return gs
}

type gamesink struct {
}

func (gs *gamesink) GetServerPort() int {
	return config.Server.ServerPort
}
func (gs *gamesink) GetGameID() int {
	return GAMEID
}
func (gs *gamesink) GetGameName() string {
	return GAME_NAME
}

func (gs *gamesink) GetRoomName() string {
	return config.Server.ServerName
}

func (gs *gamesink) CreateTableSink(table frame.Table, data string) frame.TableSink {
	return newTableSink(table, data)
}

func (gs *gamesink) GetOfflineSeconds() int64 {
	return config.Server.OfflineSeconds
}

//	func (gs *gamesink) GetRoomList() string {
//		d, _ := json.Marshal(config.Rooms)
//		return string(d)
//	}
func (gs *gamesink) GetCertFile() string {
	return config.Server.CertFile
}
func (gs *gamesink) GetKeyFile() string {
	return config.Server.KeyFile
}
func (gs *gamesink) GetServerAddr() string {
	return config.Rooms.Rooms[0].ServerIP
}

func (gs *gamesink) GetRobotCount() int {
	return config.Rooms.Rooms[0].RobotCount
}

func (gs *gamesink) GetRobotGoldLimit() (min, max int) {
	min = config.Rooms.Rooms[0].MinRobotGold
	max = config.Rooms.Rooms[0].MaxRobotGold
	return
}

func (gs *gamesink) GetRobotOnlineSec() int {
	return config.Rooms.Rooms[0].RobotOnlineSec
}

func (gs *gamesink) GetRoomDatas() []string {
	var ret []string
	for _, v := range config.Rooms.Rooms {
		//d, _ := json.Marshal(v)
		ret = append(ret, v.RoomName)
	}
	return ret
}

func (gs *gamesink) GetMinRoomCount(roomData string) int {
	for _, v := range config.Rooms.Rooms {
		if roomData == v.RoomName {
			return v.MinRoom
		}
	}
	return 0
}
func (gs *gamesink) IsChipRoom() bool {
	return config.Server.IsChipRoom > 0
}

func (ts *tablesink) OnBaseScoreChanged(baseScore int) {
	ts.roomInfo.BaseScore = baseScore
	d, _ := json.Marshal(ts.roomInfo.RoomInfoBase)
	ts.table.SendGameData(-1, CMD_ROOMINFO, string(d))
}

func (gs *gamesink) OnPlatformConfig(key string) {
	config.OnConfigChanged(key)
}

func (gs *gamesink) IsPrivateRoom() bool {
	return false
}

func (gs *gamesink) GetChairCount() int {
	return CHAIR_COUNT
}

func (gs *gamesink) GetRoomRobotCount(roomName string) int {
	for _, v := range config.Rooms.Rooms {
		if roomName == v.RoomName {
			return v.RobotCount
		}
	}
	return 0
}

func (gs *gamesink) GetRoomRobotGoldLimit(roomName string) (min, max int) {
	min = 0
	max = 0
	for _, v := range config.Rooms.Rooms {
		if roomName == v.RoomName {
			min = v.MinRobotGold
			max = v.MaxRobotGold
			break
		}
	}
	return
}
func (gs *gamesink) GetRoomLevel(data string) int {
	return 0
}
