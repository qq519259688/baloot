package gamelogic

type Plane struct {
	Id       int
	Position int  // 在机场是0，1表示刚起飞，最多走 13 * 3 + 18
	CanMove  bool // 当前是否可以移动
	// 检测碰撞需要通过椅子号转换判断
}

// 检查是否已经起飞
func (p *Plane) isTookOff() bool {
	return p.Position > 0
}

func (p *Plane) isLanded() bool {
	return p.Position == MAX_STEP
}

// 获得即将抵达终点距离
func (p *Plane) calculateArriveDistance() int {
	//只有进入即将到达区域才计算
	if p.isWillArrive() {
		return MAX_STEP - p.Position
	}
	return 0
}

func (p *Plane) isWillArrive() bool {
	return  p.Position > LOOP_START && p.Position < MAX_STEP
}

func (p *Plane) resetCanMove(number int) {
	if p.Position == 0 {
		p.CanMove = number == 6
		return
	}

	if p.Position+number > MAX_STEP {
		p.CanMove = false
		return
	}

	p.CanMove = true
}
