package config

import (
	"encoding/json"
	"fmt"
	"os"

	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "masharie_table_rooms"
const config_key_chip = "masharie_table_chip_rooms"
const config_key_private = "masharie_table_private_rooms"

type OpenHistory struct {
	SerialNumber int
	AreaResult   []int
}

type RoomInfoBase struct {
	RoomName         string
	ServerIP         string
	ServerPort       int
	MinBet           int
	MaxBet           int //个人最大下注额
	PersonalBetRatio int //每次下注计算 (下注金额+已经下注金额)/（以下注金额+自身携带金额）<= PersonalBetRatio
	TotalMax         int
	BetTime          int
	OpenTime         int
	PrizesTime       int
	HistoryCount     int
	State            int
	StateSec         int
	SerialNumber     int
	Historys         []OpenHistory
	TotalBet         int
	ProjectMultiple  []float64         //项目倍数
	WeekRankAward    [][]item.ItemPack //周榜奖励
	BankerTaxRate    int
	TaxRate          int
	BankerConf
}

type BankerConf struct {
	BankerMin        int // 庄家下限
	BankruptcyAmount int // 庄家破产金额 低于此金额，庄家自动下庄
	MaxBankerSet     int // 庄家局数轮换，有排队的情况 0时不限制
	MaxBetRate       int
}

type RoomInfo struct {
	RoomInfoBase
	MinRoom        int
	JackpotRate    int         // 排行榜奖池比例
	PrizePoolRate  int         // 彩金奖池比例
	FreeChipsLimit int         // 免费筹码上限
	RobotConfig    []RobotInfo // 机器人配置
	ChipAmounts    []int       // 机器人筹码配置
}

type RobotInfo struct {
	HourStart int
	HourEnd   int
	CountMin  int
	CountMax  int
	GoldMin   int
	GoldMax   int
	Online    int
}

var Rooms struct {
	Rooms []RoomInfo
}

var Room *RoomInfo

var RoomConfigName string

func getConfigKey() string {
	// 私人场优先
	if Server.IsPrivateRoom {
		return config_key_private
	}
	if Server.IsChipRoom {
		return config_key_chip
	}

	return config_key
}
func loadRedisConfig(createRoom bool) bool {
	data := platformconfig.GetConfig(getConfigKey())
	if data == "" {
		log.Release("    config msg is null")
		return false
	}

	ok, _ := marshalData([]byte(data))
	return ok
}

func loadRoomConfig() {
	if loadRedisConfig(true) {
		log.Release("using remote config ignored rooms.json !!!")
		return
	}
	// 保存
	// 加载房间表
	data, err := os.ReadFile("masharie_table/rooms.json")
	//fmt.Println(string(data))
	if err != nil {
		log.Release("read rooms failed masharie_table/rooms.json")
		return
	}
	if ok, newConfig := marshalData(data); ok {
		// 写入redis
		platformconfig.SetConfig(getConfigKey(), newConfig)
	}
}

func marshalData(data []byte) (bool, string) {
	changedConfigString := string(data)
	err := json.Unmarshal(data, &Rooms)
	if err != nil {
		log.Release("Unmarshal rooms failed err:%v", err)
		return false, changedConfigString
	}
	if len(Rooms.Rooms) <= 0 {
		log.Release("Rooms.Rooms == 0")
		return false, changedConfigString
	}

	Room = &Rooms.Rooms[0]

	if len(Server.ServerIP) > 0 {
		ws := "ws"
		if len(Server.CertFile) > 0 {
			ws = "wss"
		}
		for i := 0; i < len(Rooms.Rooms); i++ {
			Rooms.Rooms[i].ServerIP = fmt.Sprintf("%s://%s:%d", ws, Server.ServerIP, Server.ServerPort)
		}
	}

	return true, changedConfigString
}

func OnConfigChanged(key string) {
	if key != getConfigKey() {
		return
	}
	log.Release("正在刷新配置信息")
	loadRedisConfig(false)
}
