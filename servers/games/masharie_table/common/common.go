package common

import (
	"fmt"

	"bet24.com/servers/games/masharie_table/config"
	userservices "bet24.com/servers/micros/userservices/proto"
)

const (
	GAMEID    = 40
	GAME_NAME = "masharie"
)

const (
	GameState_Bet = iota
	GameState_Open
	GameState_Prizes
)

const (
	TIMERID_CHECKROBOT = iota
)

type UserSetScore struct {
	UserId int
	Score  int
}

const (
	Invalid = iota
	Win
	Lose
)

var result_desc = []string{"n/a", "Win", "Lose"}

type HandCards struct {
	Cards         []int
	Project       CardProject
	ProjectLength int
	MaxCard       int
}

// 牌组 5组 默认情况下 最后组是庄家的牌
type CardDeck struct {
	HandCards
	Ranking int //名次
}

type GameState struct {
	RoomName     string
	SerialNumber int
	State        int                  // 当前状态
	Sec          int                  // 当前阶段已使用时间
	TrumpCard    int                  // 主牌(投注阶段下发)
	BankerCards  HandCards            // 庄家手牌
	DiamondCards HandCards            // 方块手牌
	ClubCards    HandCards            // 梅花手牌
	HeartCards   HandCards            // 红心手牌
	SpadeCards   HandCards            // 黑桃手牌
	AreaResult   []int                // 每个区域的开奖结果
	Historys     []config.OpenHistory // 历史纪录
	UserCount    int                  // 在线用户数
	TableUser    ScoreUsers           // 幸运星和赢金榜的玩家
	PrizePool    int                  // 彩池
	PrizeArea    int                  // 彩池区域
	//RichList     []int              // 富豪榜
	//BetList []int          //下注榜
	Scores       []UserSetScore // 结算阶段，广播所有人的分数,给桌面玩家或者自己头像飘分用
	BetCmds      []Bet          // 本局下注列表
	BankerInfo   BankerInfo
	BankerSettle int // 庄家结算金额
}

type BankerProfile struct {
	UserId      int
	NickName    string
	FaceId      int
	FaceUrl     string
	VipLevel    int
	VipExpire   int
	Decorations []userservices.UserDecoration
}

// 上庄等待队列
type BankerCandidate struct {
	BankerProfile
	Take int // 携带金币
}

type BankerInfo struct {
	BankerProfile
	Gold       int               // 庄家当前金币 系统庄为9999999999每局重置
	Score      int               // 输赢分数
	SetCount   int               // 局数
	Candidates []BankerCandidate // 等待上庄列表
	Tax        int               `json:"-"`
}

type BetBase struct {
	BetId   int
	Amount  int
	IsFree  bool
	IsRobot bool `json:"-"`
}

type Bet struct {
	BetBase
	UserId int
	Index  int // 添加一个索引，用于失败撤销

}

type Batch_Bet struct {
	UserId int
	Bets   []BetBase
}

type RankingUser struct {
	UserId int
	Score  int
}

// 是否相同下注类型
func (b *Bet) IsSameBet(b2 *Bet) bool {
	return b.BetId == b2.BetId && b.IsFree == b2.IsFree
}

type BetRet struct {
	ErrorMsg string
}

type Result struct {
	Bet
	WinAmount  int
	LoseAmount int
	AreaResult []int
	Tax        int // 税收
}

type RecordInfo struct {
	Result
	SerialNumber int
}

type UserClear struct {
	RoomName string
	UserId   int
}

// 连胜用户和累计下注用户
type ScoreUser struct {
	UserId      int
	NickName    string
	FaceId      int
	FaceUrl     string
	VipLevel    int
	VipExpire   int
	Decorations []userservices.UserDecoration
	Amount      int
	IsRobot     bool `json:"-"`
	BetAmount   int
	Info        []int
}

type ScoreUsers struct {
	LuckyStarUsers      []ScoreUser //幸运星
	PrizeLuckyStarUsers []ScoreUser
	WinGoldRankingUsers []ScoreUser
	BetGoldRankingUsers []ScoreUser
}

// 结算信息
type SettleInfo struct {
	BetAmount       int
	ActualBetAmount int //实际下注的,用于庄家结算
	WinAmount       int
	ActualWinAmount int //实际赢金,用于庄家结算
	TaxAmount       int //台费用于上报水池
	LoseAmount      int
	TotalOdds       float64
	BetDesc         string
	ResultDesc      string
}

func IsInTopN(rankList []ScoreUser, userId, n int) bool {
	for i, v := range rankList {
		if i >= n {
			break
		}
		if v.UserId == userId {
			return true
		}
	}
	return false
}

func GetRedisKey(key string) string {
	return fmt.Sprintf("%s:%s", GAME_NAME, key)
}
