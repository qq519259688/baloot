package common

import (
	"fmt"

	"bet24.com/log"
)

/*
方块 梅花  红心 黑桃
Diamond
Club
Heart
Spade
*/

type BidType int

const (
	BidType_Diamond BidType = iota
	BidType_Club
	BidType_Heart
	BidType_Spade
	BidType_Max
)

var bidTypeNames = []string{"D", "C", "H", "S"}

type AreaHandCards int

const (
	Area_Diamond AreaHandCards = iota
	Area_Club
	Area_Heart
	Area_Spade
	Area_Banker
	Area_Max
)

type CardProject int

const (
	Project_Base          CardProject = iota //基础 2倍
	Project_Sira                             //三张同花顺 3倍
	Project_Baloot                           //有主花色的K和Q 4倍
	Project_Fifty                            //四张的同花顺 5倍
	Project_Hundred                          //炸弹 6倍
	Project_StraightFlush                    //5张同花顺 7倍
)

var projectMultiple = map[CardProject]float64{
	Project_Base:          1,
	Project_Sira:          2,
	Project_Baloot:        3,
	Project_Fifty:         4,
	Project_Hundred:       5,
	Project_StraightFlush: 6,
}

var project_desc = []string{"Base", "Sira", "Baloot", "Fifty", "Hundred", "StraightFlush"}
var project_abbreviation = []string{"B", "S", "Bt", "F", "H", "SF"}

// 刷新项目倍率
func ResetProjectMultiple(multiple []float64) {
	for i, o := range multiple {
		p := CardProject(i)
		if p < Project_Base || p > Project_StraightFlush {
			continue // Invalid project index, skip
		}
		if projectMultiple[p] != o {
			projectMultiple[p] = o
		}
	}
}

func GetMultiple(project CardProject) (multiple float64) {
	multiple = projectMultiple[project]
	return
}

func GetProjectDesc(project CardProject, isHtml bool) string {
	if isHtml {
		return project_abbreviation[project]
	}
	return project_desc[project]
}

func GetBetDesc(betId int) string {
	bidType := BidType(betId)
	if bidType >= BidType_Max || bidType < BidType_Diamond {
		log.Release("common.GetDesc failed %d,%d", bidType, BidType_Max)
		return "invalid bet"
	}
	return bidTypeNames[bidType]
}

func GetResultDesc(bidType, result int) string {
	return fmt.Sprintf("%v:%v", bidTypeNames[bidType], result_desc[result])
}

// 生成无效牌牌堆
func GetInvalidHandCards(HandCardCount, CardCount int) HandCards {
	var handCards HandCards
	count := HandCardCount
	ret := make([]int, count)
	for i := 0; i < count; i++ {
		ret[i] = CardCount
	}
	handCards.Cards = ret
	handCards.Project = Project_Base
	return handCards
}
