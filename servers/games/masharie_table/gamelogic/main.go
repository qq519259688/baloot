package gamelogic

import (
	"bet24.com/servers/games/masharie_table/config"
	"bet24.com/servers/insecureframe/frame"
)

var Stopping bool

func Run() {
	if test() {
		return
	}
	Stopping = false
	frame.RunFrame(newGameSink(), config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB)
}

func test() bool {
	return false
}
