package gamelogic

const (
	Broadcast      = "broadcast"
	TotalResult    = "totalresult"
	Option         = "Option"
	Result         = "result"
	Config         = "config"
	History        = "history"
	Bet            = "bet"
	BatchBet       = "batchbet"
	BetClear       = "betclear"
	Betfailed      = "betfailed"
	BetRecord      = "betrecord"
	BetRank        = "betrank" // 投注排行榜
	UserList       = "userlist"
	FreeChip       = "freechip"       // 检查免费筹码
	FreeBet        = "freebet"        // 免费投注
	FreeChipChange = "freechipchange" // 免费卷变化
	PrizeRank      = "prizerank"      // 奖池排行榜
	ApplyBanker    = "applybanker"    // 申请上庄
	CancelBanker   = "cancelbanker"   // 取消申请
	ApplyFailed    = "applyfailed"    // 申请失败
	BankerInfo     = "bankerinfo"     // 上庄信息
)
