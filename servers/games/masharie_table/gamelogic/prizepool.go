package gamelogic

import (
	"encoding/json"
	"sync"
	"time"

	"bet24.com/log"
	"bet24.com/redis"
	timetool "bet24.com/servers/common"
	"bet24.com/servers/games/masharie_table/common"
	"bet24.com/servers/games/masharie_table/config"
)

const PrizePoolKey = "prizePool"
const PrizeRankCount = 20

// 项目奖池发奖比例
var projectPrize = map[common.CardProject]int{
	common.Project_Fifty:         20,
	common.Project_Hundred:       40,
	common.Project_StraightFlush: 60,
}

type PrizeRankUser struct {
	Time      int // 时间
	UserId    int // 用户ID
	BetAmount int // 投注额
	Score     int // 彩金
}

// 彩金
type prizePool struct {
	Jackpot  int             // 奖池 (总下注额的2%) 从config.Room.PrizePoolRate中取
	Rank     []PrizeRankUser // 前20排行榜
	lock     *sync.RWMutex
	roomInfo *config.RoomInfo
}

// 创建一个彩金
func newPrizePool(roomInfo *config.RoomInfo) *prizePool {
	ret := new(prizePool)
	ret.lock = &sync.RWMutex{}
	ret.roomInfo = roomInfo
	// 从redis中获取奖池数据
	key := PrizePoolKey
	data, _ := redis.String_Get(common.GetRedisKey(key))
	ret.Jackpot = 0
	ret.Rank = make([]PrizeRankUser, 0)

	if data == "" {
		// redis中没有奖池数据
		ret.prizePoolToRedis()
	} else {
		err := json.Unmarshal([]byte(data), ret)
		if err != nil {
			log.Release("prizePool json.Unmarshal err:%v", err)
		}
	}
	//起一个定时器,每2分钟同步一次数据到Redis
	ret.startTimer()
	return ret
}

func (pp *prizePool) startTimer() {
	pp.prizePoolToRedis()
	go func() {
		time.AfterFunc(2*time.Minute, func() {
			pp.startTimer()
		})
	}()
}

func (pp *prizePool) getPrizePool() int {
	pp.lock.RLock()
	var jackpot = pp.Jackpot
	defer pp.lock.RUnlock()
	return jackpot
}

// 更新奖池
func (pp *prizePool) updatePrizePool(betAmount int) {
	pp.lock.Lock()
	pp.Jackpot += betAmount * pp.roomInfo.PrizePoolRate / 100
	pp.lock.Unlock()
}

// 扣除奖池
func (pp *prizePool) deductJackpot(jackpot int) {
	pp.lock.Lock()
	pp.Jackpot -= jackpot
	pp.lock.Unlock()
}

func (pp *prizePool) prizePoolToRedis() {
	key := PrizePoolKey
	pp.lock.RLock()
	data, err := json.Marshal(pp)
	pp.lock.RUnlock()
	if err != nil {
		log.Debug("prizePool json.Marshal err:%v", err)
		return
	}
	redis.String_Set(common.GetRedisKey(key), string(data))
}

// 记录幸运星榜单
func (pp *prizePool) updatePrizeRank(userId, score, betAmount int) {

	ts := timetool.GetTimeStamp()
	var rank = PrizeRankUser{
		Time:      ts,
		UserId:    userId,
		BetAmount: betAmount,
		Score:     score,
	}
	pp.lock.Lock()
	//最新的在最前面
	pp.Rank = append(pp.Rank, rank)
	if len(pp.Rank) > PrizeRankCount {
		pp.Rank = pp.Rank[1:]
	}
	pp.lock.Unlock()
	pp.prizePoolToRedis()
}

// 获得幸运星榜单数据
func (pp *prizePool) getPrizeRank() []PrizeRankUser {
	pp.lock.RLock()
	defer pp.lock.RUnlock()
	return pp.Rank
}

func GetProjectRate(project common.CardProject) (rate int) {
	rate = projectPrize[project]
	return
}
