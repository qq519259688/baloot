package gamelogic

import (
	"encoding/json"
	"math/rand"
	"time"

	"bet24.com/log"
	"bet24.com/servers/games/masharie_table/common"
	"bet24.com/servers/insecureframe/robot"
	"bet24.com/servers/user"
)

var numAreasToBet = 2

type robot_action struct {
	bidCmd     string
	actionTime int64
	robot      int32
	userId     int
}

func (ts *tablesink) checkRobot() {
	count := gs.GetRoomRobotCount(ts.roomInfo.RoomName)
	if count == 0 {
		return
	}
	needCount := count - ts.getRobotCount()
	if needCount <= 0 {
		return
	}
	goldMin, goldMax := gs.GetRoomRobotGoldLimit(ts.roomInfo.RoomName)
	tableId := ts.table.GetTableID()
	for i := 0; i < count; i++ {
		time.AfterFunc(time.Duration(rand.Intn(1000))*time.Millisecond, func() {
			robot.GetOneRobotEnterTable(tableId, goldMin, goldMax)
		})
	}
}

func (ts *tablesink) getRobotCount() int {
	userlist := ts.table.GetUserList()
	ret := 0
	for _, v := range userlist {
		if v.IsRobot() {
			ret++
		}
	}
	return ret
}
// 是否上榜
func (ts *tablesink) isInRankBoard(userId int) bool {
	ts.lock.RLock()
	defer ts.lock.RUnlock()

	if common.IsInTopN(ts.score_users.LuckyStarUsers, userId, 1) {
		return true
	}

	return common.IsInTopN(ts.score_users.BetGoldRankingUsers, userId, 7)
}

func (ts *tablesink) arrangeRobotActions() {
	// 清理
	ts.robotLock.Lock()
	ts.robotActions = []robot_action{}
	ts.robotLock.Unlock()

	// 获取用户列表
	userlist := ts.table.GetUserList()
	var robotList []*user.UserInfo
	//真人数量
	userCount := 0
	for _, v := range userlist {
		if v.IsRobot() {
			robotList = append(robotList, v)
		} else {
			userCount++
		}
	}

	if len(robotList) == 0 {
		log.Debug("tablesink.arrangeRobotActions no robot")
		return
	}

	now := time.Now().UnixNano() / 1000000
	index := 0
	for _, v := range robotList {
		index++
		if userCount == 0 {
			//百分之60的概率跳过机器人下注
			if rand.Intn(100) < 60 {
				continue
			}
		}
		gold := ts.table.GetUserChipOrGoldByUser(v)
		isInBoard := ts.isInRankBoard(v.GetUserId())

		betIds, amounts := ts.getRobotAction(isInBoard, gold)

		// 如果没有投注区域或没有投注次数，则跳过这个机器人
		if len(betIds) == 0 || len(amounts) == 0 {
			continue
		}
		// 延迟时间 = 1-下注时间秒-2秒 + 0-100毫秒 + 50毫秒*投注索引
		delayTime := int64((rand.Intn(ts.roomInfo.BetTime-2)+1)*1000 + rand.Intn(100) + index*50)
		ts.robotLock.Lock()
		for i, betId := range betIds {
			//如果没有投注金额，则跳过这个投注区域
			if len(amounts[i]) == 0 {
				continue
			}
			for _, amount := range amounts[i] {
				cmdBid := common.Bet{
					UserId: v.GetUserId(),
					Index:  index,
					BetBase: common.BetBase{
						BetId:  betId,
						Amount: amount,
					},
				}
				d, _ := json.Marshal(cmdBid)
				ts.robotActions = append(ts.robotActions, robot_action{
					bidCmd:     string(d),
					actionTime: now + delayTime + int64((50+rand.Intn(500))*i),
					robot:      v.GetUserIndex(),
					userId:     v.GetUserId(),
				})
			}
		}
		ts.robotLock.Unlock()
	}
	ts.startRobotPoll()
}

// 获取机器人动作
func (ts *tablesink) getRobotAction(isInBoard bool, money int) (betIds []int, amounts [][]int) {

	maxBet := money / ts.roomInfo.PersonalBetRatio

	// 下注概率
	r := rand.Intn(100)
	noBid := true //是否不下注
	zoneIndex := 0
	if !isInBoard {
		//随机下注2-4个区域
		numAreasToBet = rand.Intn(3) + 2
	}
	switch {
	case isInBoard:
		noBid = r < 10
		//上榜用户随机下注1-4个区域
		numAreasToBet = rand.Intn(4) + 1
		zoneIndex = 0
	case money >= 10000000:
		noBid = r < 95
		zoneIndex = 5
	case money >= 8000000:
		noBid = r < 90
		zoneIndex = 4
	case money >= 5000000:
		noBid = r < 85
		zoneIndex = 3
	case money >= 2000000:
		noBid = r < 80
		zoneIndex = 2
	case money >= 1000000:
		noBid = r < 70
		zoneIndex = 1
	default:
		zoneIndex = 0
		noBid = r < 50
	}

	if noBid {
		return
	}

	// 下注区域
	bidZone := [][]int{
		// BidType_Diamond BidType_Club BidType_Heart BidType_Spade
		{25, 25, 25, 25}, // < 1m
		{25, 25, 25, 25}, // < 2m
		{25, 25, 25, 25}, // < 5m
		{25, 25, 25, 25}, // < 8m
		{25, 25, 25, 25}, // < 10m
		{25, 25, 25, 25}, // < 15m
		{25, 25, 25, 25}, // >= 15m
	}

	betIds = ts.getRandomIndexesFromArray(bidZone[zoneIndex], numAreasToBet)

	bidAmountZone := [][]int{
		// 5% 10%  %20 %40 %60
		{50, 30, 15, 5, 0}, // < 1m
		{50, 40, 10, 0, 0}, // < 2m
		{40, 30, 30, 0, 0}, // < 5m
		{30, 40, 30, 0, 0}, // < 8m
		{40, 20, 40, 0, 0}, // < 10m
		{30, 50, 20, 0, 0}, // < 15m
		{60, 40, 0, 0, 0},  // >= 15m
	}
	// 选金额
	bidAmountPercent := []int{2, 5, 10, 20, 40}
	chipAmounts := []int{100, 1000, 5000, 10000, 100000}
	//用最大可以下注额下注
	bidAmount := maxBet / 100 * bidAmountPercent[ts.getRandomIndexFromArray(bidAmountZone[zoneIndex])]
	percentage := 100
	betPercentages := make([]int, len(betIds))
	for i := 0; i < len(betIds)-1; i++ { // 对除了最后一个区域之外的区域进行随机分配
		min := 10                                 // 最低百分比
		max := percentage - min*(len(betIds)-i-1) // 最高百分比，保证剩余的区域都能有最低百分比
		if max < min {
			max = min
		}
		betPercentages[i] = min + rand.Intn(max-min+1) // 生成一个[min, max]之间的随机整数
		percentage -= betPercentages[i]                // 更新剩余的总配比
	}
	betPercentages[len(betIds)-1] = percentage
	for i := 0; i < len(betIds); i++ {
		amountsForBet := []int{}

		curPercentage := betPercentages[i]
		// 使用百分比计算金额
		amount := int(float64(bidAmount*curPercentage) / 100)
		if amount < ts.roomInfo.MinBet { // 如果低于最低下注额，就设为最低下注额
			amount = ts.roomInfo.MinBet
		}
		partialAmount := amount

		//fmt.Printf("第%d次下注，百分比是%d%%，金额是%d\n", i+1, curPercentage, partialAmount)
		for i := len(chipAmounts) - 1; i >= 0; i-- {
			if partialAmount >= chipAmounts[i] {
				chip := chipAmounts[i]
				count := partialAmount / chip
				if count < 3 {
					continue
				}
				for j := 0; j < count; j++ {
					amountsForBet = append(amountsForBet, chip)
				}
				partialAmount %= chip
			}
		}
		amounts = append(amounts, amountsForBet)
	}

	//打印日志
	//log.Release("机器人下注 betIds:%v amounts:%v ", betIds, amounts)

	return

}

func (ts *tablesink) getRandomIndexesFromArray(a []int, n int) []int {
	if len(a) <= 1 {
		return []int{0}
	}

	total := 0
	for _, v := range a {
		total += v
	}

	if total == 0 {
		return []int{0}
	}

	if n > len(a) {
		n = len(a)
	}

	indexes := make([]int, 0, n)
	used := make(map[int]bool)
	for len(indexes) < n {
		r := rand.Intn(total)

		index := 0
		for i := 0; i < len(a); i++ {
			index += a[i]
			if r < index && !used[i] {
				indexes = append(indexes, i)
				used[i] = true
				break
			}
		}
	}

	return indexes
}

func (ts *tablesink) getRandomIndexFromArray(a []int) int {
	if len(a) <= 1 {
		return 0
	}

	total := 0
	for _, v := range a {
		total += v
	}

	if total == 0 {
		return 0
	}

	r := rand.Intn(total)

	index := 0
	for i := 0; i < len(a); i++ {
		index += a[i]
		if r < index {
			return i
		}
	}

	return 0
}

func (ts *tablesink) checkRobotAction() bool {
	now := time.Now().UnixNano() / 1000000
	ts.robotLock.Lock()
	defer ts.robotLock.Unlock()
	for i := 0; i < len(ts.robotActions); {
		if ts.robotActions[i].actionTime <= now {
			go ts.doRobotAction(ts.robotActions[i])
			ts.robotActions = append(ts.robotActions[:i], ts.robotActions[i+1:]...)
		} else {
			i++
		}
	}
	return len(ts.robotActions) == 0
}

func (ts *tablesink) doRobotAction(action robot_action) {
	go ts.handleBet(action.robot, action.userId, action.bidCmd)
}

func (ts *tablesink) startRobotPoll() {
	if gs.GetRobotCount() == 0 {
		return
	}

	//if ts.robotStopEvent == nil {
	ts.robotStopEvent = make(chan bool)
	//}

	ticker := time.NewTicker(50 * time.Millisecond)
	go func(t *time.Ticker) {
		for { //循环
			select {
			case <-ts.robotStopEvent:
				t.Stop()
				log.Debug("robotStopEvent")
				ts.robotStopEvent = nil
				return
			case <-t.C:
				if ts.checkRobotAction() {
					t.Stop()
					ts.robotStopEvent = nil
					return
				}
			}
		}
	}(ticker)
	ts.robotStopEvent = nil
}

func (ts *tablesink) stopRobotPoll() {
	if ts.robotStopEvent == nil {
		return
	}
	//log.Debug("stopRobotPoll called")
	ts.robotStopEvent <- true
	ts.robotStopEvent = nil
}
