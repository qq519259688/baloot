package gamelogic

import (
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/games/masharie_table/common"
	"bet24.com/servers/insecureframe/message"
	userservices "bet24.com/servers/micros/userservices/proto"
	"bet24.com/servers/transaction"
	"bet24.com/servers/user"
)

func (ts *tablesink) OnGameMessage(userIndex int32, msg, data string) bool {
	/*
		if !ok {
			log.Debug("tablesink.OnGameMessage user not exist %d", userIndex)
			return false
		}
	*/
	log.Debug("tablesink.OnGameMessage Receive Msg  %d,%v,%v", userIndex, msg, data)
	usr, _ := ts.table.GetUser(userIndex)
	if usr == nil {
		log.Debug("tablesink.OnGameMessage user not exist")
		return false
	}
	userId := usr.GetUserId()
	switch msg {
	case History:
		ts.sendHistory(userIndex)
	case Option:
		ts.sendGameOption(userIndex)
	case Bet:
		ts.handleBet(userIndex, userId, data)
	case BatchBet:
		ts.handleBatchBet(userIndex, userId, msg, data)
	case BetClear:
		ts.onBetClear(userIndex, userId, msg, data)
	case BetRecord:
		ts.onGetBetRecord(userIndex, userId, msg, data)
	case BetRank:
		ts.onGetBetRank(userIndex, userId, msg, data)
	case UserList:
		ts.onGetUserList(userIndex, msg)
	case FreeChip:
		ts.onCheckFreeChip(userIndex, userId, msg, data)
	case FreeBet:
		ts.handleFreeBet(userIndex, userId, data)
	case PrizeRank:
		ts.onGetPrizeRank(userIndex, userId, msg, data)
	case ApplyBanker:
		ts.onApplyBanker(userIndex, userId, msg, data)
	case CancelBanker:
		ts.onCancelBanker(userIndex, userId, msg)
	default:
		log.Debug("tablesink.OnGameMessage unhandled message %v  data %v", msg, data)
	}
	return true
}

func (ts *tablesink) sendBetFailed(userIndex int32, index int, errMsg string) {
	var bet_failed struct {
		ErrorMsg string
		Index    int
	}
	bet_failed.Index = index
	bet_failed.ErrorMsg = errMsg
	d, _ := json.Marshal(bet_failed)
	ts.table.SendGameData(userIndex, Betfailed, string(d))
}

func (ts *tablesink) sendBetOK(userIndex int32, data string) {
	ts.table.SendGameData(userIndex, Bet, data)
}

func (ts *tablesink) sendHistory(userIndex int32) {
	ts.table.SendGameData(userIndex, History, ts.getHistory())
}

func (ts *tablesink) sendGameScene(userIndex int32) {
	ts.table.SendGameData(userIndex, message.Table_GameScene, ts.getStateData())
}

// 处理筹码下注
func (ts *tablesink) handleBet(userIndex int32, userId int, data string) {
	usr := ts.table.GetUserByUserId(userId)
	if usr == nil {
		log.Debug("tablesink.handleBet user not exist UserId:%v", userId)
		return
	}
	// 庄家不能下注
	if userId == ts.bankerInfo.UserId {
		log.Debug("tablesink.handleBet user is banker")
		return
	}
	// log.Debug("handleBet %v", data)
	var bet common.Bet
	e := json.Unmarshal([]byte(data), &bet)
	if e != nil {
		log.Release("handleBet Unmarshal data failed %v |e=%v:", data, e)
		return
	}

	amount := bet.Amount
	bet.UserId = userId
	bet.IsFree = false
	bet.IsRobot = usr.IsRobot()
	if amount <= 0 {
		ts.sendBetFailed(userIndex, bet.Index, "Invalid Bet!")
		return
	}
	// 看下能否下注
	ok, errMsg := ts.isCanBet(bet)
	if !ok {
		ts.sendBetFailed(userIndex, bet.Index, errMsg)
		return
	}
	ok, errMsg = ts.checkGold(bet)
	if !ok {
		ts.sendBetFailed(userIndex, bet.Index, errMsg)
		return
	}
	ret, _ := ts.table.WriteUserMoney(userId, -amount, 0, 1, common.GAMEID*100+1, ts.roomInfo.RoomName)

	if !ret {
		// 扣金币失败
		ts.sendBetFailed(userIndex, bet.Index, "Not enough cash!")
		return
	}
	ts.lock.RLock()
	_, isBet := ts.userBetList[userId]
	ts.lock.RUnlock()
	ok, err := ts.addBet(bet)
	if !ok {
		ts.table.WriteUserMoney(userId, amount, 0, 3, common.GAMEID*100+3, ts.roomInfo.RoomName)
		ts.sendBetFailed(userIndex, bet.Index, err)
	} else {
		ts.handleBetSuccess(userId, bet, usr)
		if !isBet && !usr.IsRobot() {
			//如果第一次下注写重连
			gs.setOfflineStatus(userId, true, ts.getBetRemainingSecond(), true)
		}
		d, _ := json.Marshal(bet)
		ts.sendBetOK(-1, string(d))

	}
}

// 处理批量下注
func (ts *tablesink) handleBatchBet(userIndex int32, userId int, msg, data string) {
	usr := ts.table.GetUserByUserId(userId)
	if usr == nil {
		log.Debug("tablesink.handleBatchBet user not exist UserId:%v", userId)
		return
	}
	// 庄家不能下注
	if userId == ts.bankerInfo.UserId {
		log.Debug("tablesink.handleBatchBet user is banker")
		return
	}
	var batchBet common.Batch_Bet
	e := json.Unmarshal([]byte(data), &batchBet)
	if e != nil {
		log.Release("handleBatchBet Unmarshal data failed %v", data)
		return
	}

	// 先判断所有区域是否能下
	totalAmount := 0
	for _, v := range batchBet.Bets {
		var bet common.Bet
		bet.BetBase = v
		bet.UserId = userId
		ok, errMsg := ts.isCanBet(bet)
		if !ok {
			log.Debug("handleBatchBet.isCanBet failed to add bet %v,%v", bet, errMsg)
			ts.sendBetFailed(userIndex, bet.Index, errMsg)
			return
		}
		ok, errMsg = ts.checkGold(bet)
		if !ok {
			log.Debug("handleBatchBet.checkGold failed to add bet %v,%v", bet, errMsg)
			ts.sendBetFailed(userIndex, bet.Index, errMsg)
			return
		}
		totalAmount += bet.Amount
	}

	ret, _ := ts.table.WriteUserMoney(userId, -totalAmount, 0, 1, common.GAMEID*100+1, ts.roomInfo.RoomName)

	if !ret {
		// 扣金币失败
		ts.sendBetFailed(userIndex, 0, "Not enough cash!")
		return
	}
	ts.lock.RLock()
	_, isBet := ts.userBetList[userId]
	ts.lock.RUnlock()
	for i, v := range batchBet.Bets {
		var bet common.Bet
		bet.BetBase = v
		bet.UserId = userId
		bet.IsFree = false
		bet.IsRobot = usr.IsRobot()
		ok, _ := ts.addBet(bet)
		if !ok {
			go ts.table.WriteUserMoney(userId, bet.Amount, 0, 3, common.GAMEID*100+3, ts.roomInfo.RoomName)
		} else {
			if i == 0 && !isBet && !usr.IsRobot() {
				//如果第一次下注写重连
				gs.setOfflineStatus(userId, true, ts.getBetRemainingSecond(), true)

			}
			ts.handleBetSuccess(userId, bet, usr)
		}
	}

	ts.table.SendGameData(-1, msg, data)
}

// 处理免费筹码下注
func (ts *tablesink) handleFreeBet(userIndex int32, userId int, data string) {
	usr := ts.table.GetUserByUserId(userId)
	if usr == nil {
		log.Debug("tablesink.handleFreeBet user not exist UserId:%v", userId)
		return
	}
	// log.Debug("handleFreeBet %v", data)
	var bet common.Bet
	e := json.Unmarshal([]byte(data), &bet)
	if e != nil {
		log.Release("handleFreeBet Unmarshal data failed %v |e=%v:", data, e)
		return
	}

	amount := bet.Amount
	bet.UserId = userId
	bet.IsFree = true
	bet.IsRobot = usr.IsRobot()
	if amount <= 0 {
		ts.sendBetFailed(userIndex, bet.Index, "Invalid Bet!")
		return
	}
	// 看下能否下注
	ok, errMsg := ts.isCanBet(bet)
	if !ok {
		ts.sendBetFailed(userIndex, bet.Index, errMsg)
		return
	}
	ok, errMsg = ts.checkGold(bet)
	if !ok {
		ts.sendBetFailed(userIndex, bet.Index, errMsg)
		return
	}

	//扣除免费筹码
	result, freeChip := ts.freeChips.useFreeChips(userId, amount)
	if !result {
		log.Debug("handleFreeBet.useFreeChips Not enough free chip %v,freeChip:%v", result, freeChip)
		ts.sendBetFailed(userIndex, bet.Index, "Not enough free chip!")
		return
	}
	ts.lock.RLock()
	_, isBet := ts.userBetList[userId]
	ts.lock.RUnlock()
	addResult, err := ts.addBet(bet)
	if !addResult {
		//下注失败，返还免费筹码
		ts.freeChips.addFreeChips(userId, amount)
		ts.sendBetFailed(userIndex, bet.Index, err)
	} else {
		//免费不记录下注额
		if !usr.IsRobot() {
			if !isBet {
				gs.setOfflineStatus(userId, true, ts.getBetRemainingSecond(), true)
			}
			d, _ := json.Marshal(bet)
			ts.table.SendGameData(userIndex, FreeBet, string(d))
		}
		//免费卷不广播给所有人,有免费卷下注时,总额度是不对的
		// d, _ := json.Marshal(bet)
		// ts.sendBetOK(-1, string(d))
	}
}

// 成功下注后处理handleBetSuccess
func (ts *tablesink) handleBetSuccess(userId int, bet common.Bet, usr *user.UserInfo) {
	if !usr.IsRobot() {
		amount := bet.Amount
		switch bet.BetId {
		case int(common.BidType_Diamond):
			ts.diamondAmount += amount
		case int(common.BidType_Club):
			ts.clubAmount += amount
		case int(common.BidType_Heart):
			ts.heartAmount += amount
		case int(common.BidType_Spade):
			ts.spadeAmount += amount
		}

	}

}

func (ts *tablesink) onBetClear(userIndex int32, userId int, msg, data string) {
	ts.lock.Lock()
	betList, ok := ts.userBetList[userId]
	if !ok {
		ts.lock.Unlock()
		return
	}

	amount := 0
	for _, v := range betList {
		ts.roomInfo.TotalBet -= v.Amount
		amount += v.Amount
	}
	delete(ts.userBetList, userId)
	ts.lock.Unlock()
	ts.table.WriteUserMoney(userId, amount, 0, 3, common.GAMEID*100+3, ts.roomInfo.RoomName)

	betClear := common.UserClear{UserId: userId}
	d, _ := json.Marshal(betClear)
	ts.table.SendGameData(userIndex, msg, string(d))
	ts.userList.clearBet(userId, amount)
}

func (ts *tablesink) onGetBetRecord(userIndex int32, userId int, msg, data string) {
	//log.Debug("tablesink.onGetBetRecord %v", data)
	var getRecord struct {
		RecordCount int
	}
	e := json.Unmarshal([]byte(data), &getRecord)
	if e != nil {
		log.Release("tablesink.onGetBetRecord Unmarshal data failed %v", data)
		return
	}

	records := transaction.GetGameRecord(userId, common.GAMEID, ts.roomInfo.RoomName, getRecord.RecordCount)
	retData, _ := json.Marshal(records)
	ts.table.SendGameData(userIndex, msg, string(retData))
}

func (ts *tablesink) sendGameOption(userIndex int32) {
	d, _ := json.Marshal(ts.roomInfo.RoomInfoBase)
	ts.table.SendGameData(userIndex, Option, string(d))
}

func (ts *tablesink) onGetUserList(userIndex int32, msg string) {
	//合并两个表
	userlist := ts.userList.getLuckyStarUsers(1, ts.bankerInfo.UserId)
	userlist = append(userlist, ts.userList.getBetGoldRankingUsers(0)...)

	userIds := make([]int, len(userlist))
	for i, u := range userlist {
		userIds[i] = u.UserId
	}
	users := userservices.GetUserInfoInBulk(userIds)
	//补齐用户金币
	for i, v := range userlist {
		user := users[i]
		if user.UserId == v.UserId {
			userlist[i].Amount = ts.table.GetUserChipOrGoldByUserId(user.UserId)
		}
	}
	retData, _ := json.Marshal(userlist)
	ts.table.SendGameData(userIndex, msg, string(retData))
}

func (ts *tablesink) onGetBetRank(userIndex int32, userId int, msg, data string) {
	log.Debug("tablesink.onGetBetRank %v", data)

	//获取betRank前20名
	dayRankList, dayJackpot, dayMyBetAmount, dayMyRank := ts.dayBetRank.getRankList(userId)
	weekRankList, weekJackpot, weekMyBetAmount, weekMyRank := ts.weekBetRank.getRankList(userId)
	//判断排行榜数据是否为空
	if len(dayRankList) == 0 && len(weekRankList) == 0 {
		//如果排行榜数据为空，则返回空数据
		rankData := struct {
			Day  []common.ScoreUser `json:"Day"`
			Week []common.ScoreUser `json:"Week"`
		}{
			Day:  []common.ScoreUser{},
			Week: []common.ScoreUser{},
		}
		retData, _ := json.Marshal(rankData)
		ts.table.SendGameData(userIndex, msg, string(retData))
		return
	}

	//拼接数据返回
	rankData := struct {
		Day struct {
			RankList    []common.ScoreUser `json:"RankList"`
			Jackpot     int                `json:"Jackpot"`
			MyBetAmount int                `json:"MyBetAmount"`
			MyRank      int                `json:"MyRank"`
		} `json:"Day"`
		Week struct {
			RankList    []common.ScoreUser `json:"RankList"`
			Jackpot     int                `json:"Jackpot"`
			MyBetAmount int                `json:"MyBetAmount"`
			MyRank      int                `json:"MyRank"`
		} `json:"Week"`
	}{}
	//如果昵称为空则补齐
	dayUserList := make([]common.ScoreUser, 0, len(dayRankList))
	//如果rankList昵称为空则补齐
	dayUserIds := make([]int, len(dayRankList))
	for i, u := range dayRankList {
		dayUserIds[i] = u.UserId
	}
	dayUsers := userservices.GetUserInfoInBulk(dayUserIds)
	for i, v := range dayRankList {
		scoreUser := common.ScoreUser{
			UserId:      v.UserId,
			BetAmount:   v.BetAmount,
			NickName:    "",
			FaceId:      0,
			FaceUrl:     "",
			VipLevel:    0,
			VipExpire:   0,
			Decorations: nil,
		}
		user := dayUsers[i]
		if user.UserId == v.UserId {
			scoreUser.NickName = user.NickName
			scoreUser.FaceId = user.FaceId
			scoreUser.FaceUrl = user.FaceUrl
			scoreUser.VipLevel = user.Vip
			scoreUser.VipExpire = user.VipExpire
			scoreUser.Decorations = user.Decorations
		}
		dayUserList = append(dayUserList, scoreUser)
	}
	weekUserList := make([]common.ScoreUser, 0, len(weekRankList))
	weekUserIds := make([]int, len(weekRankList))
	for i, u := range weekRankList {
		weekUserIds[i] = u.UserId
	}
	weekUsers := userservices.GetUserInfoInBulk(weekUserIds)
	for i, v := range weekRankList {

		scoreUser := common.ScoreUser{
			UserId:      v.UserId,
			BetAmount:   v.BetAmount,
			NickName:    "",
			FaceId:      0,
			FaceUrl:     "",
			VipLevel:    0,
			VipExpire:   0,
			Decorations: nil,
		}
		user := weekUsers[i]
		if user.UserId == v.UserId {
			scoreUser.NickName = user.NickName
			scoreUser.FaceId = user.FaceId
			scoreUser.FaceUrl = user.FaceUrl
			scoreUser.VipLevel = user.Vip
			scoreUser.VipExpire = user.VipExpire
			scoreUser.Decorations = user.Decorations
		}
		weekUserList = append(weekUserList, scoreUser)

	}
	rankData.Day.RankList = dayUserList
	rankData.Day.Jackpot = dayJackpot
	rankData.Day.MyBetAmount = dayMyBetAmount
	rankData.Day.MyRank = dayMyRank
	rankData.Week.RankList = weekUserList
	rankData.Week.Jackpot = weekJackpot
	rankData.Week.MyBetAmount = weekMyBetAmount
	rankData.Week.MyRank = weekMyRank
	retData, _ := json.Marshal(rankData)
	ts.table.SendGameData(userIndex, msg, string(retData))

}

func (ts *tablesink) onGetPrizeRank(userIndex int32, userId int, msg, data string) {
	log.Debug("tablesink.onGetPrizeRank %v", data)
	//获取奖池排行榜前20名
	rankList := ts.prizePool.getPrizeRank()

	//判断排行榜数据是否为空
	if len(rankList) == 0 {
		//如果排行榜数据为空，则返回空数据
		ts.table.SendGameData(userIndex, msg, "[]")
		return
	}

	//拼接数据返回
	scoreUserList := make([]common.ScoreUser, 0, len(rankList))
	userIds := make([]int, len(rankList))
	for i, u := range rankList {
		userIds[i] = u.UserId
	}
	users := userservices.GetUserInfoInBulk(userIds)
	//如果rankList昵称为空则补齐
	for i, v := range rankList {

		info := make([]int, 0, 2)
		info = append(info, v.Score)
		info = append(info, v.Time)
		scoreUser := common.ScoreUser{
			UserId:      v.UserId,
			BetAmount:   v.BetAmount,
			NickName:    "",
			FaceId:      0,
			FaceUrl:     "",
			VipLevel:    0,
			VipExpire:   0,
			Decorations: nil,
			Info:        info,
		}
		user := users[i]
		if user.UserId == v.UserId {
			scoreUser.NickName = user.NickName
			scoreUser.FaceId = user.FaceId
			scoreUser.FaceUrl = user.FaceUrl
			scoreUser.VipLevel = user.Vip
			scoreUser.VipExpire = user.VipExpire
			scoreUser.Decorations = user.Decorations
		}
		scoreUserList = append(scoreUserList, scoreUser)
	}
	retData, _ := json.Marshal(scoreUserList)
	ts.table.SendGameData(userIndex, msg, string(retData))
}

func (ts *tablesink) onCheckFreeChip(userIndex int32, userId int, msg, data string) {
	//log.Debug("tablesink.onCheckFreeChip %v", data)
	freeChip, lastHour := ts.freeChips.checkFreeChipsNum(userId)
	//返回数据
	var result struct {
		Free     int //剩余免费额度
		LastHour int //前一次领取时间 0 6 12 18
	}
	result.Free = freeChip
	result.LastHour = lastHour
	retData, _ := json.Marshal(result)
	ts.table.SendGameData(userIndex, msg, string(retData))
}
