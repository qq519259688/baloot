package gamelogic

import (
	"sort"
	"sync"

	"bet24.com/log"
	"bet24.com/servers/games/masharie_table/common"
	userservices "bet24.com/servers/micros/userservices/proto"
)

type gameLog struct {
	serialNumber int // 局数编号
	betAmount    int // 投注金额
	winAmount    int // 赢金
	loseAmount   int // 输金
}

const (
	GAME_LOG_COUNT = 20 // 游戏日志记录数量
)

type masharieUser struct {
	userId      int
	nickName    string
	faceId      int
	faceUrl     string
	vipLevel    int
	vipExpire   int
	decorations []userservices.UserDecoration

	totalBetCount  int // 累计投注次数
	totalBetAmount int // 累计投注金额
	totalWinAmount int // 累计赢金
	totalWinCount  int // 累计赢次数
	gameWinAmount  int // 本局实际赢金

	recentBetAmount int // 最近投注总额
	recentWinAmount int // 最近赢金
	recentWinCount  int // 最近赢次数

	isBet   bool
	gameLog []gameLog //游戏日志 不下注也清除日志
}

type userlist struct {
	users []*masharieUser
	lock  *sync.RWMutex
}

func newUserList() *userlist {
	ret := new(userlist)
	ret.lock = &sync.RWMutex{}
	return ret
}

// 清除最旧的一条统计数据
func (u *masharieUser) clearGameLog() {

	if len(u.gameLog) >= 1 && len(u.gameLog) >= GAME_LOG_COUNT {
		// 如果gameLog切片的长度超过限制，那么删除最旧的一条游戏日志
		oldestLog := u.gameLog[0]
		u.recentBetAmount -= oldestLog.betAmount
		u.recentWinAmount -= oldestLog.winAmount
		if oldestLog.winAmount > 0 {
			u.recentWinCount--
		}
		u.gameLog = u.gameLog[1:]
	}
}

func (u *masharieUser) addGameLog(serialNumber, betAmount, winAmount, loseAmount, totalResult int) {
	// 写入的数据 是一个用户一条,并且是结算后的统计数据,所以不需要考虑多条的情况

	u.clearGameLog()

	//新增
	log := gameLog{
		serialNumber: serialNumber,
		betAmount:    betAmount,
		winAmount:    totalResult,
		loseAmount:   loseAmount,
	}
	u.gameLog = append(u.gameLog, log)
	u.recentBetAmount += betAmount
	u.recentWinAmount += totalResult
	if totalResult > 0 {
		u.recentWinCount++
	}
}

func (ul *userlist) addUser(userId int, nickName string,
	faceId int, faceUrl string, vipLevel, vipExpire int, decorations []userservices.UserDecoration) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			log.Release("userlist.addUser UserId  %d already exist", userId)
			return
		}
	}
	u := new(masharieUser)
	u.userId = userId
	u.nickName = nickName
	u.faceId = faceId
	u.faceUrl = faceUrl
	u.vipLevel = vipLevel
	u.vipExpire = vipExpire
	u.decorations = decorations
	u.totalBetCount = 0
	u.totalBetCount = 0
	u.totalBetAmount = 0
	u.totalWinAmount = 0
	u.totalWinCount = 0
	u.gameWinAmount = 0
	u.recentBetAmount = 0
	u.recentWinAmount = 0
	u.recentWinCount = 0
	ul.users = append(ul.users, u)

}

func (ul *userlist) removeUser(userId int) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for k, v := range ul.users {
		if v.userId == userId {
			ul.users = append(ul.users[:k], ul.users[k+1:]...)
			return
		}
	}
	log.Release("userlist.removeUser %d not found", userId)
}

// 结束的时候清理状态，并且根据未下注进行连胜扣减
func (ul *userlist) clearBetStatus(serialNumber int) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		//无论是否下注都要计算参与游戏局数

		if !v.isBet {
			//如果没有下注清除连胜
			// if v.recentStreakWin > 0 {
			// 	v.recentStreakWin = 0
			// }

			//没有下注也添加日志 解决参与的局数超过20局以后 每局都被清除上一局数据的问题
			v.addGameLog(serialNumber, 0, 0, 0, 0)
		} else {
			v.isBet = false
		}
		v.gameWinAmount = 0
	}
}

func (ul *userlist) addResult(userId, serialNumber, betAmount, winAmount, loseAmount, totalResult int) {
	// log.Debug("userlist.addResult %d,betAmount:%d,winAmount:%d,loseAmount:%d,totalResult:%d", userId, betAmount, winAmount, loseAmount, totalResult)
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			// if recentStreakWin < 0 {
			// 	v.recentStreakWin = 0
			// }
			if totalResult > 0 {
				v.totalWinAmount = v.totalWinAmount + totalResult
				v.gameWinAmount = totalResult
				//v.recentStreakWin = v.recentStreakWin + winAmount
				v.totalWinCount++
			}
			v.addGameLog(serialNumber, betAmount, winAmount, loseAmount, totalResult)
			return
		}
	}
	log.Release("userlist.addResult %d not found", userId)
}

func (ul *userlist) addBet(userId, amount, betId int) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			v.totalBetAmount += amount
			v.totalBetCount++
			v.isBet = v.totalBetAmount > 0
			return
		}
	}
	log.Release("userlist.addBet %d not found", userId)
}

func (ul *userlist) clearBet(userId, amount int) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			v.totalBetAmount -= amount
			v.totalBetCount--
			v.isBet = v.totalBetAmount > 0
			//撤销下注，不需要更新下注区域
			return
		}
	}
	log.Release("userlist.addBet %d not found", userId)
}

// 幸运星
func (ul *userlist) getLuckyStarUsers(count, bankerId int) []common.ScoreUser {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	//先考虑最近赢金次数,如果次数相同考虑赢金数,如果赢金数相同考虑投注数
	sort.Slice(ul.users, func(i, j int) bool {
		if ul.users[i].recentWinCount > ul.users[j].recentWinCount {
			return true
		} else if ul.users[i].recentWinCount == ul.users[j].recentWinCount {
			if ul.users[i].recentWinAmount > ul.users[j].recentWinAmount {
				return true
			} else if ul.users[i].recentWinAmount == ul.users[j].recentWinAmount {
				if ul.users[i].totalBetAmount > ul.users[j].totalBetAmount {
					return true
				}
			}
		}
		return false
	})

	ret := []common.ScoreUser{}
	for k, v := range ul.users {
		if k >= count {
			break
		}
		//不限制赢金次数大于0
		// if v.recentWinCount <= 0 {
		// 	break
		// }
		if v.userId == bankerId {
			continue
		}
		info := make([]int, 0, 2)
		info = append(info, v.recentWinCount)  //赢局数
		info = append(info, v.recentBetAmount) //投注数

		u := common.ScoreUser{UserId: v.userId,
			NickName:    v.nickName,
			FaceId:      v.faceId,
			FaceUrl:     v.faceUrl,
			VipLevel:    v.vipLevel,
			VipExpire:   v.vipExpire,
			Decorations: v.decorations,
			Info:        info}
		ret = append(ret, u)
	}
	return ret
}

// 每回合赢金排行 最近的
func (ul *userlist) getWinGoldRankingUsers(count, bankerSettle, bankerId, bankerFaceId int, bankerFaceUrl string) []common.ScoreUser {

	ul.lock.Lock()
	defer ul.lock.Unlock()
	ret := []common.ScoreUser{}
	if len(ul.users) == 0 {
		//没有人下注
		return ret
	}
	//排序
	sort.Slice(ul.users, func(i, j int) bool {
		return ul.users[i].gameWinAmount > ul.users[j].gameWinAmount
	})
	info := make([]int, 0, 1)
	//如果赢最多的也没有庄家赢得多,则返回庄家信息
	if ul.users[0].gameWinAmount <= 0 || ul.users[0].gameWinAmount <= bankerSettle {
		info = append(info, bankerSettle)
		u := common.ScoreUser{UserId: bankerId,
			NickName: "",
			FaceId:   bankerFaceId,
			FaceUrl:  bankerFaceUrl,
			Info:     info}
		ret = append(ret, u)
		return ret
	}

	for k, v := range ul.users {
		if k >= count {
			break
		}
		if v.gameWinAmount <= 0 {
			break
		}
		if v.userId == bankerId {
			continue
		}

		info = append(info, v.gameWinAmount)
		u := common.ScoreUser{UserId: v.userId,
			NickName:    v.nickName,
			FaceId:      v.faceId,
			FaceUrl:     v.faceUrl,
			VipLevel:    v.vipLevel,
			VipExpire:   v.vipExpire,
			Decorations: v.decorations,
			Info:        info}

		ret = append(ret, u)
	}
	return ret

}

// 传递0表示不限制数量 最近的
func (ul *userlist) getBetGoldRankingUsers(count int) []common.ScoreUser {

	ul.lock.Lock()
	defer ul.lock.Unlock()
	//排序
	sort.Slice(ul.users, func(i, j int) bool {
		return ul.users[i].recentBetAmount > ul.users[j].recentBetAmount
	})
	ret := []common.ScoreUser{}
	for k, v := range ul.users {
		if k >= count && count != 0 {
			break
		}
		if v.recentBetAmount < 0 {
			break
		}

		info := make([]int, 0, 2)
		info = append(info, v.recentWinCount)  //赢局数
		info = append(info, v.recentBetAmount) //投注数
		u := common.ScoreUser{UserId: v.userId,
			NickName:    v.nickName,
			FaceId:      v.faceId,
			FaceUrl:     v.faceUrl,
			VipLevel:    v.vipLevel,
			VipExpire:   v.vipExpire,
			Decorations: v.decorations,
			Info:        info}
		ret = append(ret, u)
	}
	return ret
}
