package gamelogic

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/games/masharie_table/common"
	userservices "bet24.com/servers/micros/userservices/proto"
)

func (ts *tablesink) sendApplyBankerFailed(userIndex int32, errorMsg string) {
	ts.table.SendGameData(userIndex, ApplyFailed, errorMsg)
}

func (ts *tablesink) broadcastBankerInfo() {
	d, _ := json.Marshal(ts.bankerInfo)
	ts.table.SendGameData(-1, BankerInfo, string(d))
}

func (ts *tablesink) onApplyBanker(userIndex int32, userId int, msg, data string) {
	log.Debug("tablesink_banker.onApplyBanker %d %d", userIndex, userId)
	added, err := ts.addBanker(userIndex, userId)
	if !added {
		ts.sendApplyBankerFailed(userIndex, err)
		return
	}

	ts.broadcastBankerInfo()
}

func (ts *tablesink) onCancelBanker(userIndex int32, userId int, msg string) {
	if ts.removeCandidate(userId) {
		ts.broadcastBankerInfo()
		return
	}
	// 如果是庄家，则需要等待游戏结束
	if userId != ts.bankerInfo.UserId {
		return
	}

	if ts.roomInfo.State == common.GameState_Bet {
		ts.forceChangeBanker = true
		return
	}

	// 直接切换庄家
	if ts.changeBanker(true) {
		ts.broadcastBankerInfo()
	}
}

func (ts *tablesink) addBanker(userIndex int32, userId int) (bool, string) {
	if ts.roomInfo.BankerMin == 0 {
		return false, "not applicable"
	}
	// 本身就是庄家？
	if userId == ts.bankerInfo.UserId {
		return false, "already banker"
	}
	// 本身在列表
	for _, candidate := range ts.bankerInfo.Candidates {
		if candidate.UserId == userId {
			return false, "already in line"
		}
	}

	// 钱够不够？
	usr := ts.table.GetUserByUserId(userId)
	if usr == nil {
		log.Debug("tablesink_banker.addBanker invalid user %d", userId)
		return false, "invalid user"
	}

	userGold := ts.table.GetUserChipOrGoldByUserId(userId)
	if userGold < ts.roomInfo.BankerMin {
		log.Debug("tablesink_banker.addBanker %d %d", userGold, ts.roomInfo.BankerMin)
		return false, "not enough gold"
	}
	take := userGold

	// 添加到庄家列表
	ts.bankerInfo.Candidates = append(ts.bankerInfo.Candidates, common.BankerCandidate{
		BankerProfile: common.BankerProfile{
			UserId:      userId,
			NickName:    usr.GetUserNickName(),
			FaceId:      usr.GetUserFaceId(),
			FaceUrl:     usr.GetUserFaceUrl(),
			VipLevel:    usr.GetUserVipLevel(),
			VipExpire:   usr.GetUserVipExpire(),
			Decorations: usr.GetDecorations(),
		},
		Take: take,
	})
	return true, ""
}

func (ts *tablesink) checkBankerQuality() {
	changed := false
	// 清理列表中钱不够的
	for i := 0; i < len(ts.bankerInfo.Candidates); {
		candidate := ts.bankerInfo.Candidates[i]
		usr := ts.table.GetUserByUserId(candidate.UserId)
		userGold := ts.table.GetUserChipOrGoldByUserId(candidate.UserId)
		if usr == nil || userGold < ts.roomInfo.BankerMin {
			changed = true
			ts.bankerInfo.Candidates = append(ts.bankerInfo.Candidates[:i], ts.bankerInfo.Candidates[i+1:]...)
			continue
		} else {
			i++
		}
	}

	defer func() {
		if !changed {
			return
		}
		log.Debug("checkBankerQuality changed banker info new banker : %d", ts.bankerInfo.UserId)
		//ts.lock.Lock()
		//当前测试可以不刷新榜单
		//ts.score_users.LuckyStarUsers = ts.userList.getLuckyStarUsers(5, ts.bankerInfo.UserId)
		//ts.score_users.WinGoldRankingUsers = ts.userList.getWinGoldRankingUsers(3, ts.bankerInfo.UserId)
		//log.Debug("users %v,%v", ts.score_users.LuckyStarUsers, ts.score_users.WinGoldRankingUsers)

		//ts.lock.Unlock()
		ts.broadcastBankerInfo()
	}()

	if ts.forceChangeBanker {
		ts.forceChangeBanker = false
		changed = ts.changeBanker(true)
		return
	}

	// 当前庄家是否需要切换
	if ts.bankerInfo.UserId == -1 && len(ts.bankerInfo.Candidates) == 0 {
		// 没有庄家也没有候选者
		return
	}

	if ts.bankerInfo.UserId != -1 {
		banker := ts.table.GetUserByUserId(ts.bankerInfo.UserId)
		// 庄家不够钱
		if banker == nil || ts.bankerInfo.Gold < ts.roomInfo.BankruptcyAmount {
			changed = ts.changeBanker(true)
			return
		}
		// 局数已满
		if ts.bankerInfo.SetCount >= ts.roomInfo.MaxBankerSet && ts.roomInfo.MaxBankerSet != 0 {
			changed = ts.changeBanker(false)
			return
		}
	} else {
		// 没有庄家，但有候选人？
		changed = ts.changeBanker(false)
	}
}

func (ts *tablesink) updateBankerGold() {
	ts.table.WriteUserMoney(ts.bankerInfo.UserId, ts.bankerInfo.Gold, 0, 2, 8+common.GAMEID*100, ts.roomInfo.RoomName)
	ts.table.WriteUserMoney(ts.bankerInfo.UserId, -ts.bankerInfo.Gold, 0, 2, 9+common.GAMEID*100, ts.roomInfo.RoomName)
}

// 切换庄家
func (ts *tablesink) changeBanker(force bool) bool {
	ts.forceChangeBanker = false
	if !force && len(ts.bankerInfo.Candidates) == 0 {
		return false
	}
	// 如果有上一个庄家，则把剩余分数写回去
	if ts.bankerInfo.UserId != -1 {
		go ts.table.WriteUserMoney(ts.bankerInfo.UserId, ts.bankerInfo.Gold, ts.bankerInfo.Tax, 2, 7+common.GAMEID*100, ts.roomInfo.RoomName)

		winRate := 1.00
		// 写betlist
		go ts.table.WriteBetRecordWithPlayTime(ts.bankerInfo.UserId,
			ts.bankerInfo.Gold-ts.bankerInfo.Score,
			ts.bankerInfo.Gold,
			winRate,
			fmt.Sprintf("banker(%d)", ts.bankerInfo.Gold-ts.bankerInfo.Score), "banker result", ts.roomInfo.RoomName, ts.roomInfo.BetTime)
		go gs.setOfflineStatus(ts.bankerInfo.UserId, false, 0, false)
	}

	// 强制切换
	newBanker := ts.raiseCandidate()
	if newBanker == nil {
		log.Debug("gamesink.changeBanker raiseCandidate failed")
		ts.bankerInfo.Gold = 0
		ts.bankerInfo.UserId = -1
		ts.bankerInfo.NickName = ""
		ts.bankerInfo.FaceId = -1
		ts.bankerInfo.FaceUrl = ""
		ts.bankerInfo.VipLevel = 0
		ts.bankerInfo.VipExpire = 0
		ts.bankerInfo.Decorations = []userservices.UserDecoration{}
		ts.bankerInfo.Score = 0
		ts.bankerInfo.SetCount = 0
		ts.bankerInfo.Tax = 0
		return true
	}
	userGold := ts.table.GetUserChipOrGoldByUserId(newBanker.UserId)
	newBanker.Take = userGold

	// 庄家先扣分
	b, _ := ts.table.WriteUserMoney(newBanker.UserId, -userGold, 0, 2, 6+common.GAMEID*100, ts.roomInfo.RoomName)
	if !b {
		return false
	}

	ts.bankerInfo.Gold = userGold
	ts.bankerInfo.UserId = newBanker.UserId
	ts.bankerInfo.NickName = newBanker.NickName
	ts.bankerInfo.FaceId = newBanker.FaceId
	ts.bankerInfo.FaceUrl = newBanker.FaceUrl
	ts.bankerInfo.VipLevel = newBanker.VipLevel
	ts.bankerInfo.VipExpire = newBanker.VipExpire
	ts.bankerInfo.Decorations = newBanker.Decorations
	ts.bankerInfo.Score = 0
	ts.bankerInfo.SetCount = 0
	go gs.setOfflineStatus(ts.bankerInfo.UserId, true, 0, false)
	return true
}

func (ts *tablesink) raiseCandidate() *common.BankerCandidate {
	if len(ts.bankerInfo.Candidates) == 0 {
		//log.Debug("换庄失败%v", ts.bankerInfo.Candidates)
		return nil
	}

	ret := &ts.bankerInfo.Candidates[0]
	ts.bankerInfo.Candidates = ts.bankerInfo.Candidates[1:]
	return ret
}

func (ts *tablesink) removeCandidate(userId int) bool {
	for i := 0; i < len(ts.bankerInfo.Candidates); {
		candidate := ts.bankerInfo.Candidates[i]
		if userId == candidate.UserId {
			ts.bankerInfo.Candidates = append(ts.bankerInfo.Candidates[:i], ts.bankerInfo.Candidates[i+1:]...)
			return true
		} else {
			i++
		}
	}
	return false
}
