package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/games/masharie_table/common"
	"bet24.com/servers/games/masharie_table/config"
	"bet24.com/servers/games/masharie_table/gamelogic"
	"bet24.com/servers/insecureframe/frame"
	"bet24.com/servers/insecureframe/gate"
	"bet24.com/servers/transaction"
	"bet24.com/utils"
)

func waitInput() {
	for {
		var cmd string
		var param1 string
		var param2 string
		fmt.Scanf("%s %s %s", &cmd, &param1, &param2)
		switch cmd {
		case "exit":
			gamelogic.Stopping = true
			frame.StopServer()
			go transaction.DoGameRoomPing(common.GAMEID, 1, config.Room.RoomName)
		case "listuser":
			gate.DumpUsers()
		default:
			if !frame.Dump(cmd, param1, param2) {
				log.Release("unknown command")
			}
		}
	}
}

func main() {
	defer waitInput()
	rand.Seed(time.Now().UnixNano())
	coreservice.SetServiceAddr(config.Server.ServiceAddr)
	config.Run()
	utils.SetErrorFile("log/masharie_table/err.log", "masharie_table starting")
	//monitor.Run(config.Server.MonitorPort)
	gamelogic.Run()
	startRoomPing()
}

func startRoomPing() {
	if gamelogic.Stopping {
		return
	}
	d, _ := json.Marshal(config.Room.RoomInfoBase)
	go transaction.DoGameRoomPing(common.GAMEID, 2, config.Room.RoomName)
	frame.UpdateRoomList(config.Room.RoomName, config.Room.RoomName, string(d), 0)
	time.AfterFunc(10*time.Second, startRoomPing)
}
