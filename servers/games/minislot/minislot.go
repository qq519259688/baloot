package minislot

import (
	"bet24.com/log"
	"bet24.com/servers/games/slotcommon"
)

const (
	GAMEID       = 63
	GAME_NAME    = "minislot"
	GAME_MESSAGE = "minislot"
)

const LogColor = log.Blue

var logic *GameLogic

func Run(slotSink slotcommon.SlotSink) {
	logic = newGameLogic(slotSink)
}

func OnGameMessage(userId int, msg, data string) {
	logic.onMessage(userId, msg, data)
}

type CmdBet struct {
	Amount int
}

type CmdBetReturn struct {
	CmdBet
	PrizeId   int
	WinAmount int
	ErrMsg    string
}
