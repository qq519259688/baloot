package rezekislot

import (
	_ "bet24.com/log"
)

type FreeSpinInfo struct {
	slotId   int
	minCount int
	freeTime int
}

func newFreeSpinInfo(freeSlotId int, minCount int, freeTime int) *FreeSpinInfo {
	ret := new(FreeSpinInfo)
	ret.slotId = freeSlotId
	ret.minCount = minCount
	ret.freeTime = freeTime
	return ret
}

func (f *FreeSpinInfo) getFreeTime(slots []int) (freeTime int, freeSlotCount int) {
	freeSlotCount = 0
	freeTime = 0
	for _, v := range slots {
		if v == f.slotId {
			freeSlotCount++
		}
	}

	if freeSlotCount < f.minCount {
		return freeTime, freeSlotCount
	}
	freeTime = f.freeTime
	return freeTime, freeSlotCount
}
