package rezekislot

import (
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/games/slotcommon/admanager"
)

func (this *GameLogic) writeGameMsg(userId int, gameMsg RezekiSlot_Message) {
	data, _ := json.Marshal(gameMsg)
	this.slotSink.SendGameCmd(userId, GAME_MESSAGE, string(data))
	log.Debug("rezekislot.writeGameMsg %s", string(data))
}

func (this *GameLogic) sendBet(userId int, errMsg string) {
	var msg RezekiSlot_Message
	msg.MsgID = Msg_Bet
	data, _ := json.Marshal(RezekiSlot_Bet{ErrorMsg: errMsg})
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) sendResult(userId int, result RezekiSlot_Result) {
	var msg RezekiSlot_Message
	msg.MsgID = Msg_Result
	data, _ := json.Marshal(result)
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) sendJackpot(userId int) {
	if userId <= 0 {
		return
	}
	var msg RezekiSlot_Message
	msg.MsgID = Msg_Jackpot
	data, _ := json.Marshal(RezekiSlot_Jackpot{Amount: this.jackpotManager.getAmount()})
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) sendSlots(userId int) {
	var msg RezekiSlot_Message
	msg.MsgID = Msg_Slots
	data, _ := json.Marshal(this.slotManager.Slots)
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) sendBetLevel(userId int) {
	this.slotCommon.SendBetLevel(userId)
}

func (this *GameLogic) sendFanConfig(userId int) {
	var msg RezekiSlot_Message
	msg.MsgID = Msg_FanConfig
	data, _ := json.Marshal(this.fanManager.fanConfig)
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) onMessage(userId int, msg, data string) {
	var gameMsg RezekiSlot_Message

	e := json.Unmarshal([]byte(data), &gameMsg)
	if e != nil {
		log.Release("onMessage Unmarshal data failed %v", data)
		return
	}

	//defer utils.TimeCost(fmt.Sprintf("RezekiSlot.onMessage %d", gameMsg.MsgID))()

	switch gameMsg.MsgID {
	case Msg_Enter:
		this.slotCommon.OnUserEnter(userId)
		this.sendSlots(userId)
		this.sendJackpot(userId)
		this.sendFanConfig(userId)
		this.sendBetLevel(userId)
		this.sendFreeAdConfig(userId)
		this.sendFreeStatus(userId)
	case Msg_Exit:
		this.userExit(userId)
	case Msg_Bet:
		this.onRecvBet(userId, gameMsg.Data)
	case Msg_Jackpot:
		this.sendJackpot(userId)
	case Msg_GetConfig:
		this.sendSlots(userId)
		this.sendJackpot(userId)
		this.sendFanConfig(userId)
		this.sendBetLevel(userId)
		this.sendFreeAdConfig(userId)
		this.sendFreeStatus(userId)
	case Msg_UseAd:
		this.onRecvUseAd(userId)
	default:
		log.Release("onMessage unhandle msg %v", data)
	}
}

func (this *GameLogic) onTribalSlotBroadCast(data string) {
	var msg RezekiSlot_Message
	msg.MsgID = Msg_Broadcast
	msg.Data = data
	this.writeGameMsg(-1, msg)
}

func (this *GameLogic) onTribalSlotJackpot(data string) {
	var msg RezekiSlot_Message
	msg.MsgID = Msg_Jackpot
	msg.Data = data
	this.writeGameMsg(-1, msg)
}

func (this *GameLogic) onRecvBet(userId int, data string) {
	var bet RezekiSlot_Bet
	e := json.Unmarshal([]byte(data), &bet)
	if e != nil {
		log.Release("onRecvBet Unmarshal data failed %v", data)
		return
	}

	// 是否有免费次数
	isFree, lastBet, fromAd := this.useFreeSpin(userId)
	if isFree {
		log.Debug("GameLogic.onRecvBet using free spin")
		bet.Amount = lastBet
	}

	amount := bet.Amount
	_, base := this.betLevelManager.GetLevelAndBase(amount)
	if base <= 0 {
		this.sendBet(userId, "Invalid Bet!")
		return
	}
	if isFree {
		// 获取结果并写分
		this.handleResult(userId, amount, isFree, fromAd)
	} else {
		scoreType := GAMEID*100 + 1
		status := 1
		isSuceeded := this.slotSink.WriteMoney(userId, GAMEID, -amount, 0, status, scoreType, GAME_NAME)

		if !isSuceeded {
			log.Release("onRecvBet WriteMoney failed,UserId[%d],amount[%d]", userId, amount)
			// 扣金币失败
			this.sendBet(userId, "Not enough money!")
			return
		}
		// 获取结果并写分
		this.handleResult(userId, amount, isFree, fromAd)
	}

}

func (this *GameLogic) handleResult(userId int, amount int, isFree bool, fromAd bool) {
	//defer utils.TimeCost(fmt.Sprintf("rezeki.handleResult %d", userId))()
	result := this.getResult(userId, amount, isFree)
	this.slotCommon.WriteResult(userId, amount, result.WinAmount+result.JackpotAmount, isFree, fromAd, this.getResultDesc(result), GAMEID)
	// 发送结果给客户端
	this.sendResult(userId, result)
	// 如果有jackpot
	// 发送事件
	userInfo := this.slotSink.GetUserInfo(userId)
	if userInfo == nil {
		log.Debug("handleResult userInfo == nil")
		return
	}
	jackpotMoney := result.JackpotAmount
	if jackpotMoney > 0 {
		var broadCast JackPot_Broadcast
		broadCast.UserID = userId
		broadCast.JackpotLevel = result.JackpotLevel
		broadCast.Amount = jackpotMoney
		broadCast.FaceID = userInfo.GetUserFaceId()
		broadCast.FaceUrl = userInfo.GetUserFaceUrl()
		broadCast.NickName = userInfo.GetUserNickName()
		broadCast.Sex = userInfo.GetUserSex()
		broadCast.Vip = userInfo.GetUserVipLevel()
		data, _ := json.Marshal(broadCast)
		//event.DispatchEvent(EVENT_BROADCAST, string(data))
		this.onTribalSlotBroadCast(string(data))
	}
}

//检查免费状态
func (this *GameLogic) sendFreeStatus(userId int) {
	// 是否有免费次数
	var config RezekiSlot_FreeStatus
	isFree, lastBet, _ := this.useFreeSpin(userId)
	config.IsFree = isFree
	if isFree {
		config.LastBet = lastBet
		config.FreeCount = this.getFreeSpinTime(userId)
	}
	var msg RezekiSlot_Message
	msg.MsgID = Msg_FreeStatus
	data, _ := json.Marshal(config)
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) sendFreeAdConfig(userId int) {
	var config RezekiSlot_FreeAdConfig
	config.AdCount, _ = admanager.GetSlotUserInfo(userId, GAMEID)
	config.TotalAdCount, config.FreeSpinCount = admanager.GetSlotConfig(GAMEID)
	config.Bet = this.getAdBet(config.TotalAdCount - config.AdCount)
	config.AdBets = this.AdBets
	var msg RezekiSlot_Message
	msg.MsgID = Msg_FreeAdConfig
	data, _ := json.Marshal(config)
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) getAdBet(adCount int) int {
	if len(this.AdBets) == 0 {
		return 0
	}

	for i := 0; i < len(this.AdBets); i++ {
		if adCount >= this.AdBets[i].AdCount {
			return this.AdBets[i].BetAmount
		}
	}

	return this.AdBets[len(this.AdBets)-1].BetAmount
}

func (this *GameLogic) onRecvUseAd(userId int) {
	var usedAd RezekiSlot_UsedAd
	usedAd.FreeSpinCount, usedAd.AdCount = admanager.UseAd(userId, GAMEID)
	if usedAd.FreeSpinCount > 0 {
		totalAdCount, _ := admanager.GetSlotConfig(GAMEID)
		usedAd.Bet = this.getAdBet(totalAdCount - usedAd.AdCount - 1)
		usedAd.NextAdBetAmount = this.getAdBet(totalAdCount - usedAd.AdCount)
		this.addFreeSpin(userId, usedAd.FreeSpinCount, usedAd.Bet, true)
	}

	var msg RezekiSlot_Message
	msg.MsgID = Msg_UseAd
	data, _ := json.Marshal(usedAd)
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}
