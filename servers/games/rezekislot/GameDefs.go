package rezekislot

const (
	COLUMN_COUNT = 5
	ROW_COUNT    = 3
	RESULT_COUNT = 16 //最后一个格子为特殊区域
	GAMEID       = 58
	GAME_NAME    = "rezekislot"
	GAME_MESSAGE = "rezekislot"
)

// 中奖连线信息
type Result struct {
	SlotID    int     // 格子ID
	SlotCount int     // 数量
	WinShape  int     // 连线数值 从左到右,从个位数起 111表示第一行3个 1111第一行4个,333第三行三个
	WinRate   float64 // 赢的倍数
}

// 扇子中奖连线信息
type FanResult struct {
	FanID     int  // 扇子ID
	WinAmount int  // 共赢金额
	IsGrand   bool //是否Grand
}

// 消息定义
const (
	Msg_Bet          = 1  // 下注
	Msg_Result       = 2  // 发送结果
	Msg_Jackpot      = 3  // 获取彩池额度
	Msg_Slots        = 4  // 获取目前使用的Slot类型
	Msg_BetLevel     = 5  // 下注配置信息
	Msg_GetConfig    = 6  // 获取配置信息
	Msg_Broadcast    = 7  // 奖池广播
	Msg_Enter        = 8  // 进入游戏
	Msg_Exit         = 9  // 离开游戏
	Msg_FreeAdConfig = 10 // 广告配置及自己的剩余
	Msg_UseAd        = 11 // 看广告后请求免费次数
	Msg_FanConfig    = 13 // 扇子配置信息
	Msg_FreeStatus   = 14 // 检查免费状态
)

type RezekiSlot_Message struct {
	MsgID int
	Data  string
}

type RezekiSlot_Bet struct {
	Amount   int
	ErrorMsg string
}

type RezekiSlot_Result struct {
	BetAmount      int       // 下注金额
	WinAmount      int       // 共赢金额
	Slots          []int     // 16个格子ID
	Lines          []Result  // 中奖连线
	Fan            FanResult // 扇子中间结果
	IsFree         bool      // 这次滚动是否为免费
	FreeSpin       int       // 本次结果获取的免费次数
	RemainFreeTime int       // 剩余免费次数
	JackpotLevel   int       // 0 无 1 mini 2 minor 3 major 4 grand 这个游戏只有4级
	JackpotAmount  int       // 本次获得奖池奖励，如果是grand，包含奖池的100%
}

type JackPot_Broadcast struct {
	UserID          int
	JackpotLevel    int
	Amount          int
	FaceID          int
	FaceChangeIndex int
	FaceUrl         string
	NickName        string
	Vip             int
	Sex             int
}

type RezekiSlot_Jackpot struct {
	Amount int
}

type RezekiSlot_FreeAdBet struct {
	AdCount   int
	BetAmount int
}

type RezekiSlot_FreeAdConfig struct {
	TotalAdCount  int // 每日免费次数
	AdCount       int // 剩余次数
	FreeSpinCount int // 看广告后获取的免费次数
	Bet           int // 本次看视频获得的底注
	AdBets        []RezekiSlot_FreeAdBet
}

// 下行数据，上行无数据
type RezekiSlot_UsedAd struct {
	FreeSpinCount   int // 获取免费次数，如果为0 表示没有免费
	Bet             int // 免费次数的底注
	AdCount         int // 剩余次数
	NextAdBetAmount int // 下次广告的底注
}

type RezekiSlot_FreeStatus struct {
	IsFree    bool //是否免费
	FreeCount int  //剩余次数
	LastBet   int  //免费时的下注金额
}
