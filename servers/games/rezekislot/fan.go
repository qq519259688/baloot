package rezekislot

import "bet24.com/log"

type Fan struct {
	FanID     int // 编号
	Win       int // 赢金跟IsLevel相关
	IsLevel   int // 是否跟随等级增加翻倍奖励
	IsAllWins int // 是否全赢记录的是全赢的倍数
	IsGrand   int // 是否Grand大奖
}
type FanManager struct {
	fanConfig []Fan
}

func newFanManager(fanConfig []Fan) *FanManager {
	ret := new(FanManager)
	ret.fanConfig = fanConfig
	return ret
}

func (f *Fan) isLevel() bool {
	return f.IsLevel > 0
}

func (f *Fan) isAllWins() bool {
	return f.IsAllWins > 0
}

func (f *Fan) isGrand() bool {
	return f.IsGrand > 0
}

func (fm *FanManager) getFan(fanId int) *Fan {
	for _, v := range fm.fanConfig {
		if v.FanID == fanId {
			return &v
		}
	}
	return nil
}
func (fm *FanManager) isGrand(fanId int) bool {
	s := fm.getFan(fanId)
	if s == nil {
		log.Color(LogColor, "FanManager.isGrand invlaid FanId %d", fanId)
		return false
	}
	return s.isGrand()
}

func (fm *FanManager) isLevel(fanId int) bool {
	s := fm.getFan(fanId)
	if s == nil {
		log.Color(LogColor, "FanManager.isLevel invlaid FanId %d", fanId)
		return false
	}
	return s.isLevel()
}

func (fm *FanManager) isAllWins(fanId int) bool {
	s := fm.getFan(fanId)
	if s == nil {
		log.Color(LogColor, "FanManager.isAllWins invlaid FanId %d", fanId)
		return false
	}
	return s.isAllWins()
}

//获取所有的Jackpot扇子ID
func (fm *FanManager) getJackpotFanId() []int {
	ret := []int{}
	for _, v := range fm.fanConfig {
		if v.isGrand() {
			ret = append(ret, v.FanID)
		}
	}
	return ret
}

func (fm *FanManager) getResults(fanId, betAmount, baseAmount, winAmount int) (ret FanResult) {

	ret.FanID = fanId
	ret.IsGrand = false
	//检查id是否正确
	if fanId == -1 {
		return ret
	}

	//检查配置表是否存在
	fan := fm.getFan(fanId)
	if fan == nil {
		return ret
	}

	//判断是否为 随着下注额倍数增加
	if fan.isLevel() {
		winRate := betAmount / baseAmount //获得倍数
		ret.WinAmount = fan.Win * winRate
		return ret
	} else if fan.isAllWins() {
		//全赢 当前线获胜金额乘以倍数
		ret.WinAmount = (fan.IsAllWins - 1) * winAmount
		return ret
	} else if fan.isGrand() {
		//判断是否为 Grand
		ret.IsGrand = true
		return ret
	}

	return ret
}
