package rezekislot

import (
	"math/rand"

	"bet24.com/log"
)

type SlotCount struct {
	SlotID int
	Count  int
}
type SlotColumn struct {
	SlotCounts []SlotCount
	pool       []int
}

func (sc *SlotColumn) makePool() {
	sc.pool = []int{}
	for _, v := range sc.SlotCounts {
		for i := 0; i < v.Count; i++ {
			sc.pool = append(sc.pool, v.SlotID)
		}
	}
}

func (sc *SlotColumn) getSlot() int {
	return sc.pool[rand.Intn(len(sc.pool))]
}

type SlotLevel struct {
	Columns []SlotColumn
}

type SlotCountManager struct {
	levels    []SlotLevel
	WinAmount int
}

func newSlotCountManager(counts [][][]int) *SlotCountManager {
	ret := new(SlotCountManager)
	ret.loadDataByConfig(counts)
	return ret
}

func newSlotCountManagerByConfig(winAmount int, counts [][][]int) *SlotCountManager {
	log.Color(LogColor, "newSlotCountManagerByConfig")
	ret := new(SlotCountManager)
	ret.WinAmount = winAmount
	ret.loadDataByConfig(counts)
	return ret
}

func (scm *SlotCountManager) loadDataByConfig(counts [][][]int) {
	for _, level := range counts {
		// level
		var sl SlotLevel
		for _, column := range level {
			var sc SlotColumn
			for i := 0; i < len(column); i++ {
				if column[i] == 0 {
					continue
				}
				sc.SlotCounts = append(sc.SlotCounts, SlotCount{SlotID: i, Count: column[i]})
			}
			sc.makePool()
			sl.Columns = append(sl.Columns, sc)
		}
		scm.levels = append(scm.levels, sl)
	}
}

/*
获取16个格子数据,最后一个为特殊区域
*/
func (scm *SlotCountManager) get16Slots(betLevel int, isFree bool, freeSlotId int) []int {
	ret := make([]int, RESULT_COUNT)
	isHaveFree := make([]bool, COLUMN_COUNT)

	if betLevel >= len(scm.levels) {
		betLevel = len(scm.levels) - 1
	}
	level := scm.levels[betLevel]
	for r := 0; r < ROW_COUNT; r++ {
		for c := 0; c < COLUMN_COUNT; c++ {
			row := (r*COLUMN_COUNT + c) % 5
			id := scm.getSlotIdByLevel(&level.Columns[c], isFree, isHaveFree[row], freeSlotId)

			if !isHaveFree[row] && id == freeSlotId {
				isHaveFree[row] = true
			}
			ret[r*COLUMN_COUNT+c] = id
		}
	}
	//特殊区域
	ret[RESULT_COUNT-1] = scm.getSlotIdByLevel(&level.Columns[5], isFree, isHaveFree[2], freeSlotId)
	return ret
}

/*
获取扇子ID
*/
func (scm *SlotCountManager) getFanID(magicSlotId, topSlotId, betLevel int, isFree bool) int {

	//判断顶格是否为扇子 因为只有扇子为通用素材所以判断是否为Magic即可
	if magicSlotId != topSlotId && !isFree {
		return -1
	}
	if betLevel >= len(scm.levels) {
		betLevel = len(scm.levels) - 1
	}
	level := scm.levels[betLevel]
	ret := level.Columns[6].getSlot()
	return ret
}

/*
根据等级配置获取单个素材ID
*/
func (scm *SlotCountManager) getSlotIdByLevel(sc *SlotColumn, isFree, isHaveFreeRow bool, freeSlotId int) int {
	slotId := -1
	for {
		slotId = sc.getSlot()
		//每列最多出现一个免费素材
		if !isFree && isHaveFreeRow && slotId == freeSlotId {
			continue
		} else {
			break
		}
	}
	return slotId
}
