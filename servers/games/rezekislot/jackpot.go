package rezekislot

import (
	coreservice "bet24.com/servers/coreservice/client"
)

const (
	JackpotLevelNone = iota // 不产生jackpot
	JackpotLevelMini
	JackpotLevelMinor
	JackpotLevelMajor
	JackpotLevelGrand
)

type JackpotManager struct {
	jackpotFanId          []int //扇子id
	jackpotFanCount       int
	grandJackpotPercent   int
	inGrandJackpotPercent int
	isChipRoom            bool
}

func newJackpotManager(jackpotFanId []int, count int, isChipRoom bool) *JackpotManager {
	ret := new(JackpotManager)
	ret.jackpotFanCount = count
	ret.jackpotFanId = jackpotFanId
	ret.isChipRoom = isChipRoom
	ret.loadData()
	return ret
}
func (jm *JackpotManager) loadData() { // 从redis读取jackpot数量

	jm.inGrandJackpotPercent = 1
	jm.grandJackpotPercent = 100 //设置返还为百分之百
}

func (jm *JackpotManager) getAmount() int {
	return coreservice.GetJackpotAmount(GAMEID, jm.isChipRoom)
}

//每次下注抽取投注额的十万分之一
func (jm *JackpotManager) addJackpot(slots []int, betAmount int, isFree bool, userId int) bool {
	//免费的情况下不扣
	if isFree {
		return false
	}
	if betAmount < 100000 {
		return false
	}
	jm.modifyAmount(betAmount/100000, userId)
	return true
}

//修改奖池金额
func (jm *JackpotManager) modifyAmount(amount int, userId int) {
	coreservice.ModifyJackpot(amount, GAMEID, userId, "rezekislot bet", jm.isChipRoom)
}

func (jm *JackpotManager) checkJackpot(fan FanResult, betAmount int, betLevel int, userId int) (level int, winAmount int) {
	if fan.FanID < 0 {
		return
	}

	if !fan.IsGrand {
		return
	}

	count := 0
	for _, v := range jm.jackpotFanId {
		if v == fan.FanID {
			count++
		}
	}

	if count < jm.jackpotFanCount {
		return
	}

	if count == 1 {
		//无论下注等级是多少 只要中就全部返还
		level = JackpotLevelGrand
		grandAmount := jm.getAmount() / 100 * jm.grandJackpotPercent //抽取的比例中大奖时为百分百返还
		jm.modifyAmount(-grandAmount, userId)
		winAmount = grandAmount
	}
	return
}
