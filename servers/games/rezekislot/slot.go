package rezekislot

import (
	"sort"

	"bet24.com/log"
)

type Slot struct {
	SlotID  int     // 编号
	Name    string  // 描述
	IsMagic int     // 万能牌
	Win3    float64 //  3个连线赔率
	Win4    float64 // 4个连线赔率
	Win5    float64 // 5个连线赔率
}

func (s *Slot) isMagic() bool {
	return s.IsMagic > 0
}

type SlotManager struct {
	Slots      []Slot
	FreeSlotId int
}

func newSlotManager(slots []Slot, freeSlotId int) *SlotManager {
	ret := new(SlotManager)
	ret.Slots = slots
	ret.FreeSlotId = freeSlotId
	return ret
}

func (sm *SlotManager) getMagicSlotId() int {
	for _, v := range sm.Slots {
		if v.isMagic() {
			return v.SlotID
		}
	}
	return -1
}

func (sm *SlotManager) getSlot(slotId int) *Slot {
	for _, v := range sm.Slots {
		if v.SlotID == slotId {
			return &v
		}
	}
	return nil
}
func (sm *SlotManager) isMagic(slotId int) bool {
	s := sm.getSlot(slotId)
	if s == nil {
		log.Color(LogColor, "SlotManager.isMagic invlaid SlotId %d", slotId)
		return false
	}
	return s.isMagic()
}

// 查看5个组合是否中
func (sm *SlotManager) getResult(slots []int) (slotId, count int, winMultiple float64) {
	slotId = -1
	winMultiple = 0.0
	count = 0

	if len(slots) != COLUMN_COUNT {
		log.Color(LogColor, "SlotManager.getResult invalid len %v", slots)
		return
	}
	for i := 0; i < COLUMN_COUNT; i++ {
		//免费素材不能连线
		if slots[i] == sm.FreeSlotId {
			break
		}
		//通用素材则直接通过
		if sm.isMagic(slots[i]) {
			count++
			continue
		}
		if slotId == -1 {
			slotId = slots[i]
			count++
			continue
		}
		//如果ID不相同则表示不连续
		if slots[i] != slotId {
			break
		}

		count++
	}
	//检查id是否正确
	if slotId == -1 {
		return
	}
	//检查配置表是否存在
	slot := sm.getSlot(slotId)
	if slot == nil {
		return
	}
	switch count {
	case 3:
		winMultiple = slot.Win3
	case 4:
		winMultiple = slot.Win4
	case 5:
		winMultiple = slot.Win5
	default:
		return
	}

	return
}

/*

必须由第一列开始算连线
其中第三列有第四个格子
*/
func (sm *SlotManager) getResults(slots []int) []Result {
	var ret []Result
	for r1 := 0; r1 < ROW_COUNT; r1++ {
		for r2 := 0; r2 < ROW_COUNT; r2++ {
			//第三列有四个 倒着排
			for r3 := 0; r3 < 4; r3++ {
				for r4 := 0; r4 < ROW_COUNT; r4++ {
					for r5 := 0; r5 < ROW_COUNT; r5++ {
						slotId, count, winMultiple := sm.getResult([]int{
							slots[r1*COLUMN_COUNT],
							slots[r2*COLUMN_COUNT+1],
							GetSlotValue(slots, r3*COLUMN_COUNT+2),
							slots[r4*COLUMN_COUNT+3],
							slots[r5*COLUMN_COUNT+4],
						})
						//拼接连接线给到前端 第三列有4个格子取模将第三个排在最前方便前端显示 1234
						if winMultiple > 0 {
							winShape := 0
							if count == 3 {
								winShape = (r1+1)*100 + (r2+1)*10 + ((r3+1)%4 + 1)
							} else if count == 4 {
								winShape = (r1+1)*1000 + (r2+1)*100 + ((r3+1)%4+1)*10 + r4 + 1
							} else if count == 5 {
								winShape = (r1+1)*10000 + (r2+1)*1000 + ((r3+1)%4+1)*100 + (r4+1)*10 + r5 + 1
							}
							ret = append(ret, Result{
								SlotID:    slotId,
								SlotCount: count,
								WinShape:  winShape,
								WinRate:   winMultiple,
							})
						}
					}
				}
			}
		}
	}
	// 去重，把短的去掉
	if len(ret) == 0 {
		return ret
	}
	sort.Slice(ret, func(i, j int) bool {
		return ret[i].WinShape < ret[j].WinShape
	})
	for i := 0; i < len(ret)-1; {
		flag := false
		for j := i + 1; j < len(ret); j++ {
			if ret[i].WinShape == ret[j].WinShape ||
				ret[i].WinShape == ret[j].WinShape/10 ||
				ret[i].WinShape == ret[j].WinShape/100 {
				flag = true
				break
			}
		}
		if flag {
			ret = append(ret[:i], ret[i+1:]...)
		} else {
			i++
		}
	}
	return ret
}

//根据索引获得对应数值
func GetSlotValue(slots []int, index int) int {
	//由于第三列有4个格子 Slots生成的数据中最后一个表示第三列顶部
	if index > (RESULT_COUNT - 1) {
		index = RESULT_COUNT - 1
	}
	return slots[index]
}
