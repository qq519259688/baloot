package config

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"sync"

	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
)

const config_key = "ludo_rooms"

type RobotParam struct {
	WinAmount int // 输赢金额
	Param1    int // 换牌解闷概率
	Param2    int // 换牌闷玩家概率
}

type RobotConfig struct {
	MinRobotGold   int
	MaxRobotGold   int
	RobotOnlineSec int
	RobotCount     int
	MinRoom        int
	Params         []RobotParam
	winAmount      int
}

func (rc *RobotConfig) getParam1() int {
	biggest := 0
	for _, v := range rc.Params {
		if rc.winAmount >= v.WinAmount {
			return v.Param1
		}
		if v.Param1 > biggest {
			biggest = v.Param1
		}
	}
	return biggest
}

func (rc *RobotConfig) addWinAmount(amount int) {
	rc.winAmount += amount
}

type DiceCount struct {
	Point int //点数
	Count int //概率
}

type DiceRound struct {
	StartRound int //开始轮次
	EndRound   int //截止轮次
	pool       []int
	DiceCount  []int
}
type DiceConfig struct {
	Rounds []*DiceRound
}

func (dr *DiceRound) makePool() {
	dr.pool = []int{}
	for k, v := range dr.DiceCount {
		for i := 0; i < v; i++ {
			dr.pool = append(dr.pool, k+1)
		}
	}

	//logs.Debug("DiceRound.makePool %d-%d[%v", dr.StartRound, dr.EndRound, dr.pool)
}

/*
根据轮次获取骰子点数
*/
func (dc *DiceConfig) getDicePointByRound(round int) int {
	var point = -1
	for _, v := range dc.Rounds {
		if round >= v.StartRound && round <= v.EndRound {
			point = v.pool[rand.Intn(len(v.pool))]
			return point
		}
	}
	if point < 1 {
		last := len(dc.Rounds) - 1
		point = dc.Rounds[last].pool[rand.Intn(len(dc.Rounds[last].pool))]
	}
	return point
}

type RoomInfoBase struct {
	RoomID     int //场次ID， 唯一
	RoomName   string
	RoomDesc   string
	MinGold    int
	MaxGold    int
	BaseScore  int
	ServerIP   string
	ServerPort int
	DiceSec    int //掷骰子计时 4000 客户端显示少一秒
	MoveSec    int //移动飞机计时10000
	IsDual     int // 是否2人桌
	TaxRate    int // 税收
}

type RoomInfo struct {
	RoomInfoBase
	RobotConfig
	DiceConfig
}

type RoomList struct {
	Rooms []RoomInfo
	lock  *sync.RWMutex
}

var Rooms *RoomList

func (r *RoomList) AddWinAmount(roomId int, amount int) {
	r.lock.Lock()
	defer r.lock.Unlock()
	for i := 0; i < len(r.Rooms); i++ {
		if r.Rooms[i].RoomID == roomId {
			r.Rooms[i].addWinAmount(amount)
			return
		}
	}
}

func (r *RoomList) GetParam1(roomId int) int {
	r.lock.RLock()
	defer r.lock.RUnlock()
	for i := 0; i < len(r.Rooms); i++ {
		if r.Rooms[i].RoomID == roomId {
			return r.Rooms[i].getParam1()
		}
	}
	return 0
}

func (r *RoomList) GetDicePointByRound(roomId, round int) int {

	r.lock.RLock()
	defer r.lock.RUnlock()
	for i := 0; i < len(r.Rooms); i++ {
		if r.Rooms[i].RoomID == roomId {
			return r.Rooms[i].getDicePointByRound(round)
		}
	}
	return -1
}

func (r *RoomList) Dump() string {
	r.lock.RLock()
	defer r.lock.RUnlock()
	var ret string
	for _, v := range r.Rooms {
		ret = fmt.Sprintf("%s Room:%s winAmount:%d\n", ret, v.RoomName, v.winAmount)
	}
	return ret
}

func loadRedisConfig() bool {
	data := coreservice.GetPlatformConfig(config_key)
	if data == "" {
		log.Release("    config msg is null")
		return false
	}

	return marshalData([]byte(data))
}

func loadRoomConfig() {
	if loadRedisConfig() {
		log.Release("using remote config ignored rooms.json !!!")
		return
	}
	// 保存
	// 加载房间表
	data, err := os.ReadFile("ludo/rooms.json")
	//fmt.Println(string(data))
	if err != nil {
		log.Release("read rooms failed ludo/rooms.json")
		return
	}
	if marshalData(data) {
		// 写入redis
		coreservice.SetPlatformConfig(config_key, string(data))
	}
}

func marshalData(data []byte) bool {
	Rooms.lock.Lock()
	defer Rooms.lock.Unlock()

	err := json.Unmarshal(data, &Rooms)
	if err != nil {
		log.Release("Unmarshal rooms failed err:%v", err)
		return false
	}
	if len(Rooms.Rooms) <= 0 {
		log.Release("Rooms.Rooms == 0")
		return false
	}
	//写入掷骰子轮次概率数据
	for _, v := range Rooms.Rooms {
		if len(v.Rounds) <= 0 {
			log.Release("v.Dice.Rounds == 0")
			return false
		}

		for i := 0; i < len(v.Rounds); i++ {
			v.Rounds[i].makePool()
		}
	}

	if len(Server.ServerIP) > 0 {
		ws := "ws"
		if len(Server.CertFile) > 0 {
			ws = "wss"
		}

		for i := 0; i < len(Rooms.Rooms); i++ {
			Rooms.Rooms[i].ServerIP = fmt.Sprintf("%s://%s:%d", ws, Server.ServerIP, Server.ServerPort)
		}
	}
	return true
}

func OnConfigChanged(key string) {
	if key != config_key {
		return
	}
	log.Release("正在刷新配置信息")
	loadRedisConfig()
}
