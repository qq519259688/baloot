package gamelogic

import (
	"bet24.com/servers/games/ludo/config"
	"bet24.com/servers/insecureframe/frame"
)

var Stopping bool

func Run() {
	Stopping = false
	frame.RunFrame(newGameSink(),
		config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB)
}
