package gamelogic

import (
	"bet24.com/log"
)

type PlayerInfo struct {
	IsValid          bool //是否有效玩家
	Dropped          bool //是否已弃牌
	AutoOut          bool //托管标志
	userId           int
	chairId          int
	enterGold        int
	Planes           []Plane
	LastAction       int
	Number           int  //骰子点数
	ContinueSixPoint int  //连续6点次数
	Kills            int  //击杀次数
	Death            int  //死亡次数
	SixPointTotal    int  //出现6点次数
	Place            int  //名次
	Score            int  //奖金
	round            int  //回合
	hope             int  //期待点数
	hopeRound        int  //期待回合
	isShakeOutHope   bool //最近几轮内是否摇出期待点数
	writtenScore     bool // 是否已写分
}

func (p *PlayerInfo) initData(chairId, userId, enterGold int) {
	p.IsValid = true
	p.chairId = chairId
	p.Dropped = false
	p.AutoOut = false
	p.Kills = 0
	p.Death = 0
	p.SixPointTotal = 0
	p.Place = -1
	p.Score = 0
	p.userId = userId
	p.LastAction = Action_Move // 初始化成move，用于判断第一个动作
	p.Planes = make([]Plane, PLANE_COUNT)
	for i := 0; i < PLANE_COUNT; i++ {
		p.Planes[i].Id = i
		p.Planes[i].CanMove = false
		p.Planes[i].Position = 0
	}
	p.enterGold = enterGold
	p.round = 0
	p.hope = 6 //默认期待起飞点数6
	p.hopeRound = 0
	p.isShakeOutHope = false
	p.writtenScore = false
}

func (p *PlayerInfo) dump(chairId int) {
	log.Debug("    ----Player [%d] chair [%d] enterGold:%d", p.userId, chairId, p.enterGold)
	log.Debug("        Planes[%v],Place[%d],Score[%d]", p.Planes, p.Place, p.Score)
	log.Debug("        LastAction[%d],Number[%d]", p.LastAction, p.Number)
	log.Debug("    +++++++++++++++++")
}

//可移动棋子不同坐标数量
func (p *PlayerInfo) canMovePlaneCountByPosition() int {
	samePosition := make(map[int]int)
	for _, v := range p.Planes {
		if v.CanMove {
			samePosition[v.Position] = 1
		}
	}
	return len(samePosition)
}

//找可以移动的飞机
func (p *PlayerInfo) canMovePlane(planeId int) (ok bool, Id int, isReach bool, stepCount int) {
	ok = false
	Id = planeId
	isReach = false
	stepCount = -1

	if !isValidPlane(planeId) {
		log.Release("PlayerInfo.movePlane invalid PlaneId  %d", planeId)
		return
	}
	number := p.Number
	if p.Planes[planeId].Position == 0 {
		if number != 6 {
			log.Release("PlayerInfo.movePlane plane is not take off %d,%d", planeId, number)
			return
		} else {
			p.Planes[planeId].Position = 1 // 起飞
			stepCount = 1
			ok = true
			return
		}
	}

	// 是否超出
	if p.Planes[planeId].Position+number > MAX_STEP {
		log.Release("PlayerInfo.movePlane Position[%d] + number[%d] > MAX_STEP[%d]", p.Planes[planeId].Position, number, MAX_STEP)
		ok = false
		return
	}

	p.Planes[planeId].Position += number			
	stepCount = number
	ok = true
	//是否到达终点
	if p.Planes[planeId].Position == MAX_STEP {
		log.Release("PlayerInfo.movePlane Arrive End Position[%d] == MAX_STEP[%d]", p.Planes[planeId].Position, MAX_STEP)
		isReach = true
	}

	return
}

func (p *PlayerInfo) setRollNumber(number int) bool {
	p.Number = number
	//当骰子连续三次掷出6点时，会自动跳过操作，轮到下一名玩家操作
	if number == 6 && p.ContinueSixPoint >= 2 {
		p.ContinueSixPoint = 0
		p.SixPointTotal++
		for i := 0; i < PLANE_COUNT; i++ {
			p.Planes[i].CanMove = false
		}
		return false
	}

	if number == 6 {
		p.ContinueSixPoint++
		p.SixPointTotal++
	}

	for i := 0; i < PLANE_COUNT; i++ {
		p.Planes[i].resetCanMove(number)
	}

	return true
}

func (p *PlayerInfo) isWin() bool {
	for _, v := range p.Planes {
		if !v.isLanded() {
			return false
		}
	}
	return true
}

func (p *PlayerInfo) isCanMove() bool {
	for _, v := range p.Planes {
		if v.CanMove {
			return true
		}
	}
	return false
}

//判断期望点是否可以抵达
func (p *PlayerInfo) isHopeCanArrive() bool {
	if p.hope == 6 {
		return false
	}
	for _, v := range p.Planes {
		if v.Position+p.hope == MAX_STEP {
			return true
		}
	}
	return false
}

//判断是否即将获胜
func (p *PlayerInfo) isWillWin() {
	//是否全部棋子进入最后阶段
	count := 0
	for _, v := range p.Planes {
		if v.Position == MAX_STEP || v.isWillArrive() {
			count++
		}
	}

	if count != 4 {
		return
	}

	if p.isHopeCanArrive() {
		return
	}
	hopePoint := p.getArriveHopePoint()
	if hopePoint > 0 && hopePoint < 6 && p.hope != hopePoint {
		p.hope = hopePoint
		p.isShakeOutHope = false
	}
}

//获得即将抵达终点总点数
func (p *PlayerInfo) getArriveHopePoint() int {
	validPoints := make(map[int]int)
	for _, v := range p.Planes {
		distance := v.calculateArriveDistance()
		if distance > 0 && distance < 6 {
			validPoints[v.Id] = distance
		}
	}
	//Map随机获取
	for _, v := range validPoints {
		return v
	}

	return 0
}

//检查是否撞机
func (p *PlayerInfo) checkCrashed(chairId int, pos int) []*Plane {
	var crashed []*Plane
	for i := 0; i < PLANE_COUNT; i++ {
		if isCrash(chairId, pos, p.chairId, p.Planes[i].Position) {
			crashed = append(crashed, &p.Planes[i])
		}
	}
	return crashed
}

//检查该对手的所有棋子 与自己棋子坐标的关系  0相对安全 1追赶 2超过
func (p *PlayerInfo) checkChairPosition(chairId int, pos int) int {
	//临时标记
	temp := 0
	if pos == 0 || pos >= 52 {
		return temp
	}
	chair1pos := (chairId*13 + pos) % 52 //自己的棋子操作后的位置
	for i := 0; i < PLANE_COUNT; i++ {
		//这里不能考虑对手位置是否安全 只需要考虑对手是否起飞和进入最后阶段
		if p.Planes[i].Position == 0 || p.Planes[i].Position >= 52 {
			continue
		}

		chair2pos := (p.chairId*13 + p.Planes[i].Position) % 52 //棋盘上对手的位置
		if chair1pos == chair2pos {
			continue
		}

		//超过对手
		if chair1pos > chair2pos && chair1pos-chair2pos <= 6 {
			return 2
		} else if chair2pos > chair1pos && chair2pos-chair1pos <= 6 {
			temp = 1
		}
	}
	return temp
}

//检查是否在相同位置数量
func (p *PlayerInfo) checkSamePositionCount(position, planeId int) int {
	myPlaneCount := 0
	for i := 0; i < PLANE_COUNT; i++ {
		if p.Planes[i].Id == planeId {
			continue
		}
		if p.Planes[i].Position == 0 {
			continue
		}
		if p.Planes[i].Position == position {
			myPlaneCount++
		}
	}
	return myPlaneCount
}

//计算总位置点数
func (p *PlayerInfo) calculatePosition() int {
	position := 0
	for i := 0; i < PLANE_COUNT; i++ {
		if p.Planes[i].Position == 0 {
			position -= 6
		} else {
			position += p.Planes[i].Position
		}
	}
	return position
}
