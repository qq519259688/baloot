package gamelogic

import (
	"encoding/json"
	"time"

	"bet24.com/servers/games/ludo/config"
	"bet24.com/servers/insecureframe/frame"
	"bet24.com/servers/insecureframe/gate"
)

type tablesink struct {
	table        frame.Table
	gameScene    *GameScene
	roomInfo     config.RoomInfo
	LastOpraTime time.Time //记录操作时间点
	privateData  string
}

func newTableSink(table frame.Table, data string) *tablesink {
	ts := new(tablesink)
	ts.privateData = data
	ts.table = table
	ts.gameScene = newGameScene()

	err := json.Unmarshal([]byte(data), &ts.roomInfo)
	if err != nil {
		found := false
		for _, v := range config.Rooms.Rooms {
			if data == v.RoomName {
				ts.roomInfo = v
				found = true
				break
			}
		}
		if !found {
			ts.roomInfo = config.Rooms.Rooms[0]
		}
	}

	return ts
}

func (ts *tablesink) Destroy() {
	ts.table.LogWithTableId("------tablesink：Destroy-------")
}

func (ts *tablesink) OnGetMinGold() int {
	return ts.roomInfo.MinGold
}
func (ts *tablesink) OnGetMaxGold() int {
	return ts.roomInfo.MaxGold
}

func (ts *tablesink) OnGameMessage(userIndex int32, msg, data string) bool {
	switch msg {
	case CMD_ACTION:
		ts.recvAction(userIndex, data)
	case CMD_TABLECHAT:
		ts.recvChatMsg(userIndex, data)
	case CMD_CANCLE_AUTO:
		ts.recvCancleAutoMsg(userIndex, data)
	default:
		ts.table.LogWithTableId("tablesink.OnGameMessage %s\n%s", msg, data)
		return false
	}
	return true
}

func (ts *tablesink) OnUserEnterTable(userIndex int32, chairId int) {
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		ts.table.LogWithTableId("tablesink.OnUserEnterTable user not exist")
		return
	}
	ts.table.LogWithTableId("tablesink.OnUserEnterTable chair[%d]: %d:%s ", chairId, usr.GetUserId(), usr.GetUserNickName())
	if ts.table.GetUserChipOrGoldByUser(usr) < ts.roomInfo.MinGold {
		ts.table.LogWithTableId("----玩家：%d 金币不足， T出去 金币数量 %d ", usr.GetUserId(), ts.table.GetUserChipOrGoldByUser(usr))
		//ts.table.UserWatch(userIndex)
		ts.table.KickUser(userIndex, true)
		return
	}

	//下发房间配置
	d, _ := json.Marshal(ts.roomInfo.RoomInfoBase)
	ts.table.SendGameData(userIndex, CMD_ROOMINFO, string(d))

	// 如果是中途进入，则不起定时器
	if !ts.table.IsPlaying() {
		/*
			-- timer
			ts.readyTimer[chairId].Stop()
			ts.readyTimer[chairId].Reset(time.Duration(SEC_READY) * time.Millisecond)
		*/
		ts.table.SetTimer(TIMER_READY_0+chairId, SEC_READY)
	}

	//有玩家进桌， 更新在线
	if !Stopping {
		go func() {
			frame.UpdateRoomOnline(ts.roomInfo.RoomName, ts.roomInfo.RoomID-1)
		}()
	}
	// 机器人，随机300秒准备
	if usr.IsRobot() {
		// sec := rand.Intn(2*1000) + 1000
		ts.table.SetTimer(TIMER_READY_0+chairId, 300)
	}

}

func (ts *tablesink) OnUserExitTable(userIndex int32, chairId int) {
	usr := ts.table.GetUserByChair(chairId)
	if usr == nil {
		ts.table.LogWithTableId("tablesink.OnUserExitTable user not exist")
		return
	}
	ts.table.LogWithTableId("tablesink.OnUserExitTable chair[%d]: %d:%s", chairId, usr.GetUserId(), usr.GetUserNickName())

	//有玩家离开， 更新在线
	if !Stopping {
		go func() {
			frame.UpdateRoomOnline(ts.roomInfo.RoomName, ts.roomInfo.RoomID-1)
		}()
	}
	// 如果是中途离开，则检查是否结束
	if ts.gameScene.Phase == Phase_Free || ts.gameScene.Phase == Phase_End {
		/*
			-- timer
			ts.readyTimer[chairId].Stop()
		*/
		ts.table.KillTimer(TIMER_READY_0 + chairId)
		/*
			if ts.getValidUserCount() == 1 {
				time.AfterFunc(3*time.Second, ts.checkIsNeedRobot)
			}*/

		ts.table.SetTimer(TIMER_ADD_ROBOT, 2000)
		go ts.checkAndStartGame()
	} else {
		// 没有参与游戏？
		if !ts.gameScene.Players[chairId].IsValid {
			return
		}
		if ts.gameScene.Players[chairId].Dropped {
			return
		}
		//游戏中离开，逃跑
		ts.dealDrop(chairId)
	}
}

//用户离线
func (ts *tablesink) OnUserOffline(chairId int) {
	usr := ts.table.GetUserByChair(chairId)
	if usr == nil {
		ts.table.LogWithTableId("tablesink.OnUserOffline user not exist")
		return
	}
	ts.table.LogWithTableId("tablesink.OnUserOffline %d:%s", usr.GetUserId(), usr.GetUserNickName())

}

//用户重进
func (ts *tablesink) OnUserReplay(chairId int) {
	usr := ts.table.GetUserByChair(chairId)
	if usr == nil {
		ts.table.LogWithTableId("tablesink.OnUserOffline user not exist")
		return
	}
	ts.table.LogWithTableId("tablesink.OnUserReplay %d:%s", usr.GetUserId(), usr.GetUserNickName())
	//下发房间配置
	d, _ := json.Marshal(ts.roomInfo.RoomInfoBase)
	ts.table.SendGameData(usr.GetUserIndex(), CMD_ROOMINFO, string(d))
}

//准备
func (ts *tablesink) OnUserReady(userIndex int32, chairId int) {
	if chairId == -1 {
		return
	}
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		ts.table.LogWithTableId("tablesink.OnUserReady user not exist")
		return
	}
	userGold := ts.table.GetUserChipOrGoldByUser(usr)
	if userGold < ts.roomInfo.MinGold || (ts.roomInfo.MaxGold > 0 && userGold > ts.roomInfo.MaxGold) {
		ts.table.LogWithTableId("tablesink.OnUserReady 玩家：%d 金币不足， T出去 ", usr.GetUserId())
		ts.table.KickUser(usr.GetUserIndex(), !usr.IsRobot())
		// if usr.IsRobot() {
		// 	ts.table.KickUser(usr.GetUserIndex(), false)
		// } else {
		// 	ts.table.UserWatch(usr.GetUserIndex())
		// }
		return
	}
	ts.table.LogWithTableId("tablesink.OnUserReady chair[%d]: %d:%s", chairId, usr.GetUserId(), usr.GetUserNickName())
	/*
		-- timer
		ts.readyTimer[chairId].Stop()
	*/
	ts.table.KillTimer(TIMER_READY_0 + chairId)
	ts.table.SetTimer(TIMER_ADD_ROBOT, 2000)

	ts.checkAndStartGame()
}

//取消准备
func (ts *tablesink) OnUserCancelReady(userIndex int32, chairId int) {
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		ts.table.LogWithTableId("tablesink.OnUserCancelReady user not exist")
		return
	}
	ts.table.LogWithTableId("tablesink.OnUserCancelReady %d:%s", usr.GetUserId(), usr.GetUserNickName())
}

func (ts *tablesink) OnGetChairScene(chairId int, player bool) string {
	// ts.table.LogWithTableId("------------OnGetChairScene %d", chairId)
	now := time.Now()
	ts.gameScene.LeftSec = 1000 - int(now.Sub(ts.LastOpraTime).Seconds())
	str := ts.gameScene.getScene(chairId, player)

	return str
}

func (ts *tablesink) OnGetChairCount() int {
	return CHAIR_COUNT
}

func (ts *tablesink) OnTimer(timerId int) {
	switch timerId {
	case TIMER_READY_0:
		fallthrough
	case TIMER_READY_1:
		fallthrough
	case TIMER_READY_2:
		fallthrough
	case TIMER_READY_3:
		ts.checkUserReadyStatus(timerId - TIMER_READY_0)
	case TIMER_MATCH:
		ts.dealMatchTimeOut()
	case TIMER_GAME:
		//ts.table.LogWithTableId("------gameTimer-------")
		ts.dealPlayTimeOut()
	case TIMER_ROBOT:
		ts.onRobotChatTimer()
	case TIMER_ADD_ROBOT:
		ts.checkIsNeedRobot()
	case TIMER_REMOVE_ROBOT:
		ts.removeOneRobot()
	}
}

func (ts *tablesink) DumpScene() {
	ts.gameScene.dump(ts.table.GetTableID())
}

func (ts *tablesink) GetGoldLimit() (min, max int) {
	return ts.roomInfo.MinGold, ts.roomInfo.MaxGold
}

func (ts *tablesink) IsDual() bool {
	return ts.roomInfo.IsDual > 0
}

func (ts *tablesink) GetDiceSec() int {
	sec := ts.roomInfo.DiceSec
	if sec <= 0 {
		sec = 3000
	}
	sec += 1000 //加1秒操作延时
	return sec
}

func (ts *tablesink) GetMoveSec() int {
	sec := ts.roomInfo.MoveSec
	if sec <= 0 {
		sec = 10000
	}
	return sec
}

//获得骰子点数
func (ts *tablesink) getRandomNumber(round int) int {
	num := config.Rooms.GetDicePointByRound(ts.roomInfo.RoomID, round)

	//ts.table.LogWithTableId("getRandomNumber %d, %d", ts.gameScene.round, num)
	return num
}
