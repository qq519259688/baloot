package wildapeslot

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/games/slotcommon/admanager"
	"bet24.com/utils"
)

func (this *GameLogic) writeGameMsg(userId int, gameMsg DragonSlot_Message) {
	data, _ := json.Marshal(gameMsg)
	this.slotSink.SendGameCmd(userId, GAME_MESSAGE, string(data))
}

func (this *GameLogic) sendBet(userId int, errMsg string) {
	var msg DragonSlot_Message
	msg.MsgID = Msg_Bet
	data, _ := json.Marshal(DragonSlot_Bet{ErrorMsg: errMsg})
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) sendResult(userId int, result DragonSlot_Result) {
	var msg DragonSlot_Message
	msg.MsgID = Msg_Result
	data, _ := json.Marshal(result)
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) sendSlots(userId int) {
	var msg DragonSlot_Message
	msg.MsgID = Msg_Slots
	data, _ := json.Marshal(this.slotManager.Slots)
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) sendBetLevel(userId int) {
	this.slotCommon.SendBetLevel(userId)
}

func (this *GameLogic) sendWinShapes(userId int) {
	var msg DragonSlot_Message
	msg.MsgID = Msg_WinShapes
	data, _ := json.Marshal(this.getWinShapes())
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) onMessage(userId int, msg, data string) {
	var gameMsg DragonSlot_Message

	e := json.Unmarshal([]byte(data), &gameMsg)
	if e != nil {
		log.Release("onMessage Unmarshal data failed %v", data)
		return
	}

	//defer utils.TimeCost(fmt.Sprintf("wildapeslot.onMessage %d", gameMsg.MsgID))()

	switch gameMsg.MsgID {
	case Msg_Enter:
		this.slotCommon.OnUserEnter(userId)
		this.sendSlots(userId)
		this.sendBetLevel(userId)
		this.sendWinShapes(userId)
		this.sendFreeAdConfig(userId)
		this.lockRemove.Lock()
		delete(this.removingFreespins, userId)
		this.lockRemove.Unlock()
	case Msg_Exit:
		this.userExit(userId)
	case Msg_Bet:
		this.onRecvBet(userId, gameMsg.Data)
	case Msg_GetConfig:
		this.sendSlots(userId)
		this.sendBetLevel(userId)
		this.sendWinShapes(userId)
		this.sendFreeAdConfig(userId)
	case Msg_UseAd:
		this.onRecvUseAd(userId)
	default:
		log.Release("onMessage unhandle msg %v", data)
	}
}

func (this *GameLogic) onTribalSlotBroadCast(data string) {
	var msg DragonSlot_Message
	msg.MsgID = Msg_Broadcast
	msg.Data = data
	this.writeGameMsg(-1, msg)
}

func (this *GameLogic) onRecvBet(userId int, data string) {
	var bet DragonSlot_Bet
	e := json.Unmarshal([]byte(data), &bet)
	if e != nil {
		log.Release("onRecvBet Unmarshal data failed %v", data)
		return
	}

	// 是否有免费次数
	isBonus, lastBet, fromAd, isFree := this.useFreeSpin(userId)
	log.Debug("useFreeSpin isFree=%t,isBonus=%t", isFree, isBonus)
	if isBonus || isFree {
		log.Debug("GameLogic.onRecvBet using free spin")
		bet.Amount = lastBet
	}

	amount := bet.Amount
	_, base := this.betLevelManager.GetLevelAndBase(amount)
	if base <= 0 {
		this.sendBet(userId, "Invalid Bet!")
		return
	}

	log.Debug("onRecvBet isbonus=%t isfree=%t", isBonus, isFree)

	if isBonus || isFree {
		// 获取结果并写分
		this.handleResult(userId, amount, isFree, fromAd, isBonus)
	} else {
		scoreType := GAMEID*100 + 1
		status := 1
		isSuceeded := this.slotSink.WriteMoney(userId, GAMEID, -amount, 0, status, scoreType, GAME_NAME)

		if !isSuceeded {
			log.Release("onRecvBet WriteMoney failed,UserId[%d],amount[%d]", userId, amount)
			// 扣金币失败
			this.sendBet(userId, "Not enough money!")
			return
		}
		// 获取结果并写分
		this.handleResult(userId, amount, false, false, false)
	}
}

func (this *GameLogic) handleResult(userId int, amount int, isFree bool, fromAd bool, isBonus bool) {
	defer utils.TimeCost(fmt.Sprintf("wildapeslot.handleResult %d", userId))()
	result := this.getResult(userId, amount, isFree, isBonus)

	this.slotCommon.WriteResult(userId, amount, result.WinAmount, isFree,
		fromAd, this.getResultDesc(result), GAMEID)

	// 发送结果给客户端
	this.sendResult(userId, result)

	userInfo := this.slotSink.GetUserInfo(userId)
	if userInfo == nil {
		log.Debug("handleResult userInfo == nil")
		return
	}
}

func (this *GameLogic) sendFreeAdConfig(userId int) {
	var config DragonSlot_FreeAdConfig
	config.AdCount, _ = admanager.GetSlotUserInfo(userId, GAMEID)
	config.TotalAdCount, config.FreeSpinCount = admanager.GetSlotConfig(GAMEID)
	config.Bet = this.betLevelManager.GetAmountByLevel(this.getAdBet(config.TotalAdCount - config.AdCount))
	config.AdBets = this.AdBets
	var msg DragonSlot_Message
	msg.MsgID = Msg_FreeAdConfig
	data, _ := json.Marshal(config)
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}

func (this *GameLogic) getAdBet(adCount int) int {
	if len(this.AdBets) == 0 {
		return 0
	}

	for i := 0; i < len(this.AdBets); i++ {
		if adCount >= this.AdBets[i].AdCount {
			return this.AdBets[i].BetLevel
		}
	}

	return this.AdBets[len(this.AdBets)-1].BetLevel
}

func (this *GameLogic) onRecvUseAd(userId int) {
	var usedAd DragonSlot_UsedAd
	usedAd.FreeSpinCount, usedAd.AdCount = admanager.UseAd(userId, GAMEID)
	if usedAd.FreeSpinCount > 0 {
		totalAdCount, _ := admanager.GetSlotConfig(GAMEID)
		usedAd.Bet = this.betLevelManager.GetAmountByLevel(this.getAdBet(totalAdCount - usedAd.AdCount - 1))
		usedAd.NextAdBetAmount = this.betLevelManager.GetAmountByLevel(this.getAdBet(totalAdCount - usedAd.AdCount))
		this.addFreeSpin(userId, usedAd.FreeSpinCount, usedAd.Bet, true, false)
	}

	var msg DragonSlot_Message
	msg.MsgID = Msg_UseAd
	data, _ := json.Marshal(usedAd)
	msg.Data = string(data)
	this.writeGameMsg(userId, msg)
}
