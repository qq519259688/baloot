package wildapeslot

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"

	"bet24.com/log"
)

type SlotCount struct {
	SlotID int
	Count  int
}

type SlotColumn struct {
	SlotCounts []SlotCount
	pool       []int
}

//把多个slotid放到一个数组，再随机取
func (sc *SlotColumn) makePool() {
	sc.pool = []int{}
	for _, v := range sc.SlotCounts {
		for i := 0; i < v.Count; i++ {
			sc.pool = append(sc.pool, v.SlotID)
		}
	}
}

func (sc *SlotColumn) getSlot() int {
	return sc.pool[rand.Intn(len(sc.pool))]
}

type SlotLevel struct {
	Columns []SlotColumn
}

type SlotCountManager struct {
	levels    []SlotLevel
	WinAmount int
}

func newSlotCountManager(confFile string) *SlotCountManager {
	log.Color(LogColor, "newSlotCountManager")
	ret := new(SlotCountManager)
	ret.loadData(confFile)
	return ret
}

func newSlotCountManagerByConfig(winAmount int, counts [][][]int) *SlotCountManager {
	log.Color(LogColor, "newSlotCountManagerByConfig")
	ret := new(SlotCountManager)
	ret.WinAmount = winAmount
	ret.loadDataByConfig(counts)
	return ret
}

func (scm *SlotCountManager) loadDataByConfig(counts [][][]int) {
	for _, level := range counts {
		// level
		var sl SlotLevel
		for _, column := range level {
			var sc SlotColumn
			for i := 0; i < len(column); i++ {
				if column[i] == 0 {
					continue
				}
				sc.SlotCounts = append(sc.SlotCounts, SlotCount{SlotID: i, Count: column[i]})
			}
			sc.makePool()
			sl.Columns = append(sl.Columns, sc)
		}
		scm.levels = append(scm.levels, sl)
	}
}

func (scm *SlotCountManager) loadData(confFile string) {
	var counts [][][]int
	data, err := os.ReadFile(fmt.Sprintf("slotconf/%s.json", confFile))
	if err != nil {
		data, err = os.ReadFile("slotconf/wildapeslot_count.json")
		if err != nil {
			log.Error("SlotCountManager.loadData read %s.json failed", confFile)
		}
	}

	err = json.Unmarshal(data, &counts)
	if err != nil {
		log.Error("SlotCountManager.loadData Unmarshal %s.json failed err:%v", confFile, err)
		return
	}
	scm.loadDataByConfig(counts)
}

//免费的时候FreeSpinInfo也会调这里
func (scm *SlotCountManager) get15Slots(betLevel int, isFree bool, isBonus bool) []int {
	rowCount := ROW_COUNT
	if isFree || isBonus {
		rowCount = FREE_ROW_COUNT
	}

	ret := make([]int, rowCount*COLUMN_COUNT)
	if betLevel >= len(scm.levels) {
		betLevel = len(scm.levels) - 1
	}
	level := scm.levels[betLevel]
	for r := 0; r < rowCount; r++ {
		for c := 0; c < COLUMN_COUNT; c++ {
			//循环获取直到不是apeid的
			id := scm.getNoFreeSlot(&level.Columns[c], isFree, isBonus)
			ret[r*COLUMN_COUNT+c] = id
		}
	}

	return ret
}

func (scm *SlotCountManager) getNoFreeSlot(sc *SlotColumn, isFree bool, isBonus bool) int {
	var id int
	for {
		id = sc.getSlot()
		if !isFree && !isBonus { //普通滚动不出wild
			if id == MAGIC_ID {
				continue
			} else {
				break
			}
		} else if id != FREE_ID && id != BONUS_ID { //特殊滚动不出bonus和free
			break
		}
	}
	return id
}
