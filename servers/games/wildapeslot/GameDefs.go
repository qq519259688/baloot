package wildapeslot

import (
	"fmt"

	"bet24.com/log"
)

const (
	COLUMN_COUNT      = 5
	ROW_COUNT         = 3
	FREE_ROW_COUNT    = 4
	RESULT_COUNT      = 15 //免费玩法有20个元素
	FREE_RESULT_COUNT = 20 //普通情况
	GAMEID            = 55
	GAME_NAME         = "wildapeslot"
	GAME_MESSAGE      = "wildapeslot"
)

// 中奖连线信息
type Result struct {
	SlotID    int     // 格子ID
	SlotCount int     // 数量
	WinShape  int     // 连线数值 从左到右,从个位数起 111表示第一行3个 1111第一行4个,333第三行三个
	WinRate   float64 // 赢的倍数
}

func dumpSlots(slots []int) {
	for i := 0; i < ROW_COUNT; i++ {
		col := ""
		for c := 0; c < COLUMN_COUNT; c++ {
			d := slots[i*COLUMN_COUNT+c]
			if d >= 10 {
				col = fmt.Sprintf("%s %d", col, d)
			} else {
				col = fmt.Sprintf("%s  %d", col, d)
			}
		}
		log.Color(LogColor, col)
	}
}

// 消息定义
const (
	Msg_Bet    = 1 // 下注
	Msg_Result = 2 // 发送结果
	//Msg_Jackpot = 3 // 获取彩池额度
	Msg_BetLevel = 3
	Msg_Slots    = 4 // 获取目前使用的Slot类型
	//Msg_BetLevel     = 5  // 下注配置信息
	Msg_WinShapes = 5

	Msg_GetConfig    = 6  // 获取配置信息
	Msg_Broadcast    = 7  // 奖池广播
	Msg_Enter        = 8  // 进入游戏
	Msg_Exit         = 9  // 离开游戏
	Msg_FreeAdConfig = 10 // 广告配置及自己的剩余
	Msg_UseAd        = 11 // 看广告后请求免费次数
)

type DragonSlot_Message struct {
	MsgID int
	Data  string
}

type DragonSlot_Bet struct {
	Amount   int
	ErrorMsg string
}

type TestSlots struct {
	Slots []int
}

type DragonSlot_Result struct {
	BetAmount int      // 下注金额
	WinAmount int      // 共赢金额
	Slots     []int    // 15个格子ID
	Lines     []Result // 中奖连线
	FreeApe   bool     // 3*3免费
	BonusFree int      //bonus的免费次数
	WildCount int      //wild个数
}

type JackPot_Broadcast struct {
	UserID          int
	JackpotLevel    int
	Amount          int
	FaceID          int
	FaceChangeIndex int
	FaceUrl         string
	NickName        string
	Vip             int
	Sex             int
}

type DragonSlot_Jackpot struct {
	Amount int
}

type DragonSlot_FreeAdBet struct {
	AdCount  int
	BetLevel int
}

type DragonSlot_FreeAdConfig struct {
	TotalAdCount  int // 每日免费次数
	AdCount       int // 剩余次数
	FreeSpinCount int // 看广告后获取的免费次数
	Bet           int // 本次看视频获得的底注
	AdBets        []DragonSlot_FreeAdBet
}

// 下行数据，上行无数据
type DragonSlot_UsedAd struct {
	FreeSpinCount   int // 获取免费次数，如果为0 表示没有免费
	Bet             int // 免费次数的底注
	AdCount         int // 剩余次数
	NextAdBetAmount int // 下次广告的底注
	DragonId        int
}

// 中免费的时候选择龙
type DragonSlot_SelectDragon struct {
	DragonId  int // 上行
	FreeCount int // 下行
}
