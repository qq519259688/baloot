package wildapeslot

import (
	"encoding/json"
	"os"

	"bet24.com/log"
)

func NewWinShape(shape int, desc string) *WinShape {
	ws := new(WinShape)
	ws.Shape = shape
	ws.Desc = desc
	if !ws.isValid() {
		return nil
	}

	return ws
}

type ShapeConfig struct {
	WinShape1 []WinShape
	WinShape2 []WinShape
}

func (sc *ShapeConfig) loadConfig() {
	data, err := os.ReadFile("slotconf/wildapeslot_shape.json")
	if err != nil {
		log.Error("SlotManager.loadData read wildapeslot_shape.json failed")
	}

	err = json.Unmarshal(data, &sc)
	if err != nil {
		log.Error("SlotManager.loadData Unmarshal wildapeslot_shape.json failed err:%v", err)
		return
	}

	log.Debug("shapeConfig %v", sc)
}

type WinShape struct {
	Shape int
	Desc  string
}

func (ws *WinShape) isValid() bool {
	shape := ws.Shape
	for i := 0; i < COLUMN_COUNT; i++ {
		if shape%10 >= ROW_COUNT {
			log.Release("WinShape.IsValid r%d = %d", i, shape%10)
			return false
		}
		shape = shape / 10
	}
	return true
}

//从路线个位数开始取5个id
func (ws *WinShape) getFive(slots []int) []int {
	ret := make([]int, COLUMN_COUNT)
	shape := ws.Shape
	for i := 1; i <= COLUMN_COUNT; i++ {
		var row = shape % 10
		ret[COLUMN_COUNT-i] = slots[(row+1)*COLUMN_COUNT-i]
		shape = shape / 10
	}
	return ret
}

func (ws *WinShape) getCount(slots []int) (slotID, slotCount, magicCount int) {
	slotlen := len(slots)
	if slotlen != RESULT_COUNT && slotlen != FREE_RESULT_COUNT {
		log.Release("WinShape.isWin count = %d", len(slots))
		return 0, 0, 0
	}
	five_slot := ws.getFive(slots)

	slotID, slotCount, magicCount = ws.getShapeCount(five_slot)

	log.Debug("shape %d", ws.Shape)
	log.Debug("getCount %v, id=%d,slotCount=%d", five_slot, slotID, slotCount)
	return
}

func (ws *WinShape) getShapeCount(slots []int) (slotID, slotCount, magicCount int) {
	slotID = 0xFFFFFFFF
	slotCount = 0
	magicCount = 0
	magicSlotID := 0xFFFFFFFF
	for _, v := range slots {
		if v == MAGIC_ID {
			slotCount++
			magicSlotID = v
			magicCount++
			continue
		}
		if slotID != v && slotID != 0xFFFFFFFF {
			break
		}
		slotCount++
		slotID = v
	}
	if slotID == 0xFFFFFFFF {
		slotID = magicSlotID
	}
	return
}
