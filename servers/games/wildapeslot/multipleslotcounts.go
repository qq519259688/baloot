package wildapeslot

import (
	"bet24.com/log"
)

type mulcounts struct {
	WinAmount  int
	SlotCounts [][][]int
}

type MultipleSlotCountManager struct {
	mgr_list []*SlotCountManager

	testCount []int
}

func newMultipleSlotCountManager() *MultipleSlotCountManager {
	ret := new(MultipleSlotCountManager)
	ret.loadData()
	return ret
}

func (mscm *MultipleSlotCountManager) loadData() {
	defer func() {
		log.Debug("MultipleSlotCountManager.loadData manager count = %d", len(mscm.mgr_list))
	}()

	mscm.mgr_list = append(mscm.mgr_list, newSlotCountManager("nofile"))
}

func (mscm *MultipleSlotCountManager) getMgr(winAmount int) *SlotCountManager {
	count := len(mscm.mgr_list)
	if count == 0 {
		return nil
	}

	if count == 1 {
		return mscm.mgr_list[0]
	}

	for i := 0; i < len(mscm.mgr_list); i++ {
		if winAmount >= mscm.mgr_list[i].WinAmount {
			mscm.testCount[i]++
			return mscm.mgr_list[i]
		}
	}
	//mscm.testCount[len(mscm.mgr_list)-1]++
	return mscm.mgr_list[len(mscm.mgr_list)-1]
}
