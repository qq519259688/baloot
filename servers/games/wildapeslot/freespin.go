package wildapeslot

import (
	"encoding/json"
	"os"

	"bet24.com/log"
)

type DragonChance struct {
	Name             string //  名称
	Id               int
	FreeCount        int // 免费次数
	SlotCounts       [][][]int
	slotCountManager *SlotCountManager
}

type FreeSpinInfo struct {
	ApeId      int
	DragonGame []*DragonChance
	MatrixFree [3][9]int
}

func newFreeSpinInfo() *FreeSpinInfo {
	ret := new(FreeSpinInfo)
	ret.loadConfig()
	return ret
}

func (f *FreeSpinInfo) loadConfig() {
	data, err := os.ReadFile("slotconf/wildapeslot_free.json")
	if err != nil {
		log.Error("read wildapeslot_free.json failed")
	}
	err = json.Unmarshal(data, &f)
	if err != nil {
		log.Error("Unmarshal wildapeslot_free.json failed err:%v", err)
		return
	}
	for _, v := range f.DragonGame {
		v.slotCountManager = newSlotCountManagerByConfig(0, v.SlotCounts)
	}

	//初始化免费元素矩阵
	for i := 0; i < 3; i++ {
		index := 0
		for col := i; col < 3+i; col++ {
			for row := 0; row < 3; row++ {
				f.MatrixFree[i][index] = col + row*5
				index++
			}
		}
	}
	log.Debug("freeMatrix=", f.MatrixFree)

}

func (f *FreeSpinInfo) fillBonusResult(slots []int) int {
	count := 0
	for i := 0; i < 15; i++ {
		if slots[i] == BONUS_ID {
			count++
		}
	}

	return count
}

// 看下本轮是否中free，3*3的元素，返回，0表示没有中
func (f *FreeSpinInfo) getFreeApe(slots []int) bool {
	for i := 0; i < len(f.MatrixFree); i++ {
		allApe := true

		for j := 0; j < len(f.MatrixFree[i]); j++ {
			index := f.MatrixFree[i][j]
			if slots[index] != FREE_ID {
				allApe = false
				log.Debug("find not free matrix%d,index%d", i, index)
				break
			}
		}

		if allApe {
			return true
		}
	}
	return false
}

//去掉
func (f *FreeSpinInfo) get15Slots(level int, isFree bool, isBonus bool) []int {
	var ret []int
	dragon := f.DragonGame[0]
	if dragon == nil {
		log.Debug("FreeSpinInfo.get15Slots dragonId[%d] not exist")
		return ret
	}

	return dragon.slotCountManager.get15Slots(level, isFree, isBonus)
}

func (f *FreeSpinInfo) getLastDragonId() int {
	return f.DragonGame[len(f.DragonGame)-1].Id
}
