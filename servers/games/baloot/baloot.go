package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"time"
	"github.com/gin-gonic/gin"
	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/games/baloot/config"
	"bet24.com/servers/games/baloot/gamelogic"
	"bet24.com/servers/insecureframe/frame"
	"bet24.com/servers/insecureframe/gate"
	robotmanager "bet24.com/servers/insecureframe/robot"
	"bet24.com/servers/transaction"
//	"bet24.com/utils"
	
)

func waitInput() {
	for {
		var cmd string
		var param1 string
		var param2 string
		fmt.Scanf("%s %s %s", &cmd, &param1, &param2)
		switch cmd {
		case "exit":
			gamelogic.Stopping = true
			robotmanager.Exit()
			frame.StopServer()
			go transaction.DoGameRoomPing(gamelogic.GAMEID, 1, config.RoomConfgName)
			break
		case "listuser":
			gate.DumpUsers()
		case "robotlist":
			robotmanager.Dump()
		default:
			if !frame.Dump(cmd, param1, param2) {
				log.Release("unknown command")
			}
		}
	}
}

func startRoomPing() {
	if gamelogic.Stopping {
		return
	}
	time.AfterFunc(10*time.Second, startRoomPing)
	//将本server的Ip和端口写入redis
	for i := 0; i < len(config.Rooms.Rooms); i++ {
		d, _ := json.Marshal(config.Rooms.Rooms[i].RoomInfoBase)
		go transaction.DoGameRoomPing(gamelogic.GAMEID, 2, config.Rooms.Rooms[i].RoomName)
		frame.UpdateRoomList(config.Rooms.Rooms[i].RoomName, config.Rooms.Rooms[i].RoomDesc, string(d), config.Rooms.Rooms[i].RoomID-1)
	}
}

func initRoomList() {
	for i := 0; i < len(config.Rooms.Rooms); i++ {
		d, _ := json.Marshal(config.Rooms.Rooms[i].RoomInfoBase)
		go transaction.DoGameRoomPing(gamelogic.GAMEID, 2, config.Rooms.Rooms[i].RoomName)
		frame.UpdateRoomList(config.Rooms.Rooms[i].RoomName, config.Rooms.Rooms[i].RoomDesc, string(d), config.Rooms.Rooms[i].RoomID-1)
	}
	time.AfterFunc(10*time.Second, startRoomPing)
}

func main() {
	//defer waitInput()
	rand.Seed(time.Now().UnixNano())
	coreservice.SetServiceAddr(config.Server.ServiceAddr)
	fmt.Printf("coreservice====")
	config.Run()
	fmt.Printf("config====")
	//utils.SetErrorFile("log/baloot/err.log", "baloot starting")
	gamelogic.Run()
	fmt.Printf("gamelogic====")
	initRoomList()
	fmt.Printf("initRoomList====")
	router := gin.Default()
	router.Run(":9091")
}
