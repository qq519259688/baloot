package config

import (
	"encoding/json"
	"fmt"
	"os"

	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
)

const config_key = "baloot_rooms"
const config_key_chip = "baloot_chip_rooms"
const config_key_private = "baloot_private_rooms"
const config_key_ladder = "baloot_ladder_rooms"
const config_key_ladder_private = "baloot_ladder_private_rooms"

type RobotConfig struct {
	MinRobotGold             int
	MaxRobotGold             int
	RobotOnlineSec           int
	RobotCount               int
	MinRoom                  int
	MinRobotNotFirstOutMS    int // 非首出最小时间（毫秒）
	MaxRobotNotFirstOutMS    int // 非首出最大时间（毫秒）
	DiminishingNotFirstOutMS int // 非首出递减时间（毫秒）
	MinRobotFirstOutMS       int // 首出最小时间（毫秒）
	MaxRobotFirstOutMS       int // 首出最大时间（毫秒）
	DiminishingFirstOutMS    int // 首出递减时间（毫秒）
}

type TimerConfig struct {
	SecBuy         int // 买公牌时间
	SecChooseTrump int // 选择花色时间
	SecPlay        int // 出牌时间
	SecCloseOpen   int // 开关时间
	SecDouble      int // 加倍时间
}
type PrivateRoomConfig struct {
	TargetOptions    []int `json:",omitempty"`
	UserCountOptions []int `json:",omitempty"`
	EnrollFeeOptions []int `json:",omitempty"`
	PlayTimeOptions  []int `json:",omitempty"`
	IsCorrectionMode bool  `json:",omitempty"` // 是否是纠错模式
}

type LadderRoomConfig struct {
	AdditionalPercent int `json:",omitempty"`
}

type RoomInfoBase struct {
	RoomID            int //场次ID， 唯一
	RoomName          string
	RoomDesc          string
	MinGold           int
	MaxGold           int
	BaseScore         int
	TaxRate           int // 税收
	ScoreToWin        int // 达到多少分才赢
	ServerIP          string
	ServerPort        int
	IsDual            int
	ValueDoubling     []int
	IsDoublingMode    bool // 翻倍模式
	IsOpenCardLibrary bool // 牌库是否启动,测试用
	LevelParam        int  // 经验加成百分比
	MaxSurrenderCount int  // 最大解散次数
	RoomType          int  // 房间类型，1.比赛和私人房 2.金币场 3.快速场
	LockLevel         int  // 解锁等级
	TimerConfig
	PrivateRoomConfig
	LadderRoomConfig
}

type RoomInfo struct {
	RoomInfoBase
	RobotConfig
	Test          bool
	DifferInSun   int // Sun或者Ashkal模式下，两队最终得分相差值
	DifferInHokum int // Hokum模式下，两队最终得分相差值
	MinWinPercent int // 赢牌一方其中一个玩家的最小占比
}

var Rooms struct {
	Rooms []RoomInfo
}

func getConfigKey() string {
	if Server.IsChipRoom > 0 {
		return config_key_chip
	}

	if Server.IsPrivateRoom > 0 && Server.IsLadderRoom > 0 {
		return config_key_ladder_private
	}

	if Server.IsPrivateRoom > 0 {
		return config_key_private
	}

	if Server.IsLadderRoom > 0 {
		return config_key_ladder
	}

	return config_key
}

func loadRedisConfig() bool {
	data := coreservice.GetPlatformConfig(getConfigKey())
	if data == "" {
		log.Release("    config msg is null")
		return false
	}
	log.Release("%v", data)
	return marshalData([]byte(data))
}

func loadRoomConfig() {
	// if loadRedisConfig() {
	// 	log.Release("using remote config ignored rooms.json !!!")
	// 	return
	// }
	// 保存
	// 加载房间配置表
	data, err := os.ReadFile("rooms.json")
	fmt.Println(string(data))
	if err != nil {
		log.Release("read rooms failed baloot/rooms.json")
		return
	}
	if marshalData(data) {
		// 写入redis
		coreservice.SetPlatformConfig(getConfigKey(), string(data))
	}
}

func marshalData(data []byte) bool {
	err := json.Unmarshal(data, &Rooms)
	if err != nil {
		log.Release("Unmarshal rooms failed err:%v", err)
		return false
	}
	if len(Rooms.Rooms) <= 0 {
		log.Release("Rooms.Rooms == 0")
		return false
	}

	if len(Server.ServerIP) > 0 {
		ws := "ws"
		if len(Server.CertFile) > 0 {
			ws = "wss"
		}

		for i := 0; i < len(Rooms.Rooms); i++ {
			Rooms.Rooms[i].ServerIP = fmt.Sprintf("%s://%s:%d", ws, Server.ServerIP, Server.ServerPort)
		}
	}

	return true
}

func OnConfigChanged(key string) {
	if key != getConfigKey() {
		return
	}
	log.Release("正在刷新配置信息")
	loadRedisConfig()
}
