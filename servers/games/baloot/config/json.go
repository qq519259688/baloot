package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	slog "bet24.com/log"
	"bet24.com/utils"
)

var Server struct {
	LogLevel        string
	FileLevel       string
	LogPath         string
	CertFile        string
	KeyFile         string
	MaxConnNum      int
	ChannelUrl      string
	ChannelPassword string
	RedisDB         int
	LastHour        int
	ServiceAddr     string
	ServerName      string
	ServerIP        string
	ServerPort      int
	ServerAddr      string
	MonitorPort     int
	OfflineSeconds  int64
	IsChipRoom      int
	IsPrivateRoom   int
	IsLadderRoom    int
}

var RoomConfgName = "baloot"

func init() {
	configFile := "server.json"
	data, err := os.ReadFile(configFile)
	if err != nil {
		log.Fatal("read config failed baloot/server.json ", err)
	}
	fmt.Println(string(data))
	err = json.Unmarshal(data, &Server)
	if err != nil {
		log.Fatalf("Unmarshal config failed baloot/server.json err:%v", err)
		return
	}

	logger, err := slog.New(Server.LogLevel, Server.FileLevel, fmt.Sprintf("%v/%v", Server.LogPath, RoomConfgName), log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	originPort := Server.ServerPort
	for i := 0; i < 100; i++ {
		if utils.CheckPortInUse(Server.ServerPort) {
			Server.ServerPort++
			continue
		}
		break
	}

	if Server.MonitorPort == 0 {
		Server.MonitorPort = Server.ServerPort + 100
	}

	if originPort != Server.ServerPort {
		slog.Debug("Port %d in use, change to %d", originPort, Server.ServerPort)
		d, _ := json.Marshal(Server)
		os.WriteFile(configFile, d, 0644)
	}

	now := time.Now()
	Server.LastHour = now.Hour()

}

func Run() {
	refreshLogFile()
	loadRoomConfig()
}

func refreshLogFile() {
	time.AfterFunc(5*time.Minute, refreshLogFile)
	doRefreshLogFile()
}

func doRefreshLogFile() {
	if Server.LogPath != "" {
		now := time.Now()
		if now.Hour() != Server.LastHour {
			Server.LastHour = now.Hour()
			slog.RecreateFileLog(fmt.Sprintf("%v/%v", Server.LogPath, RoomConfgName), log.LstdFlags)
		}
	}
}
