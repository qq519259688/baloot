package gamelogic

import (
	"encoding/json"

	"bet24.com/servers/games/baloot/config"
	"bet24.com/servers/insecureframe/frame"
)

func newGameSink() *gamesink {
	return new(gamesink)
}

type gamesink struct {
}

func (gs *gamesink) GetServerPort() int {
	return config.Server.ServerPort
}
func (gs *gamesink) GetGameID() int {
	return GAMEID
}
func (gs *gamesink) GetGameName() string {
	return GAME_NAME
}

func (gs *gamesink) GetRoomName() string {
	return config.Server.ServerName
}

func (gs *gamesink) CreateTableSink(table frame.Table, data string) frame.TableSink {
	return newTableSink(table, data)
}

func (gs *gamesink) GetOfflineSeconds() int64 {
	return config.Server.OfflineSeconds
}

func (gs *gamesink) GetCertFile() string {
	return config.Server.CertFile
}
func (gs *gamesink) GetKeyFile() string {
	return config.Server.KeyFile
}
func (gs *gamesink) GetServerAddr() string {
	return config.Rooms.Rooms[0].ServerIP
}

func (gs *gamesink) GetRobotCount() int {
	return config.Rooms.Rooms[0].RobotCount
}

func (gs *gamesink) GetRobotGoldLimit() (min, max int) {
	min = config.Rooms.Rooms[0].MinRobotGold
	max = config.Rooms.Rooms[0].MaxRobotGold
	return
}

func (gs *gamesink) GetRobotOnlineSec() int {
	return config.Rooms.Rooms[0].RobotOnlineSec
}

func (gs *gamesink) GetRoomDatas() []string {
	var ret []string
	for _, v := range config.Rooms.Rooms {
		ret = append(ret, v.RoomName)
	}
	return ret
}

func (gs *gamesink) GetMinRoomCount(roomData string) int {
	for _, v := range config.Rooms.Rooms {
		if roomData == v.RoomName {
			return v.MinRoom
		}
	}
	return 0
}
func (gs *gamesink) IsChipRoom() bool {
	return config.Server.IsChipRoom > 0
}

func (gs *gamesink) OnPlatformConfig(key string) {
	config.OnConfigChanged(key)
}

func (gs *gamesink) IsPrivateRoom() bool {
	return config.Server.IsPrivateRoom > 0
}

func (gs *gamesink) GetChairCount() int {
	return CHAIR_COUNT
}

func (gs *gamesink) GetRoomRobotCount(roomName string) int {
	for _, v := range config.Rooms.Rooms {
		if roomName == v.RoomName {
			return v.RobotCount
		}
	}
	return 0
}

func (gs *gamesink) GetRoomRobotGoldLimit(roomName string) (min, max int) {
	min = 0
	max = 0
	for _, v := range config.Rooms.Rooms {
		if roomName == v.RoomName {
			min = v.MinRobotGold
			max = v.MaxRobotGold
			break
		}
	}
	return
}

// GameSink_LadderRoom
func (gs *gamesink) IsLadderRoom() bool {
	return config.Server.IsLadderRoom > 0
}

func (gs *gamesink) GetMinGold(roomName string) int {
	for _, v := range config.Rooms.Rooms {
		if roomName == v.RoomName {
			return v.MinGold
		}
	}
	return 0
}
func (gs *gamesink) GetMaxGold(roomName string) int {
	for _, v := range config.Rooms.Rooms {
		if roomName == v.RoomName {
			return v.MaxGold
		}
	}
	return 0
}
func (gs *gamesink) GetAdditionalPercent(roomName string) int {
	for _, v := range config.Rooms.Rooms {
		if roomName == v.RoomName {
			return v.AdditionalPercent
		}
	}
	return 0
}
func (gs *gamesink) GetBaseScore(roomName string) int {
	for _, v := range config.Rooms.Rooms {
		if roomName == v.RoomName {
			return v.BaseScore
		}
	}
	return 0
}

func (gs *gamesink) GetRoomLevel(data string) int {
	var roomInfo config.RoomInfo
	json.Unmarshal([]byte(data), &roomInfo)
	for _, v := range config.Rooms.Rooms {
		if roomInfo.RoomName == v.RoomName {
			return v.LockLevel
		}
	}

	return 0
}
