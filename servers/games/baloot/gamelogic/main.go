package gamelogic

import (
	"bet24.com/servers/games/baloot/config"
	"bet24.com/servers/insecureframe/frame"
)

var Stopping bool

func Run() {
	if test() {
		return
	}
	Stopping = false
	frame.RunFrameWithLogServer(newGameSink(), config.Server.ChannelUrl,
		config.Server.ChannelPassword, config.Server.RedisDB, config.Server.LogPath)
}

func test() bool {
	return newCardLogic().test()
}
