package admanager

import (
	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"runtime/debug"
)

// 获取玩家信息 输出参数：本游戏总输赢，当日剩余点击视频数量 每日点击视频总数量，每次点击获取免费次数
func getUserInfo(userId, gameId int) (userWin int, adCount int, totalAdCount int, freeSpinCount int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("slotuser.getUserInfo transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SlotUser_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GameId", database.AdParamInput, database.AdInteger, 4, gameId)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	userWin = int((retRows[0][0]).(int64))
	adCount = int((retRows[0][1]).(int64))
	totalAdCount = int((retRows[0][2]).(int64))
	freeSpinCount = int((retRows[0][3]).(int64))
	return
}

// 使用视频获取免费次数
func useAd(userId, gameId int) (succeeded bool, adCount int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("slotuser.useAd transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SlotUser_UseAd")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	succeeded = (int((retRows[0][0]).(int64)) == 1)
	adCount = int((retRows[0][1]).(int64))
	return
}
