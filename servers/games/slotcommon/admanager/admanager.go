package admanager

import (
	"strconv"
	"sync"
	_ "time"

	"bet24.com/log"
)

var mgr *slotadmgr

const DELAY_REMOVE = 300 // 300秒后删除用户
const DELAY_CHECK = 60   // 每分钟检查一次

func newSlotAdManager(isChipRoom bool) *slotadmgr {
	ret := new(slotadmgr)
	ret.initData()
	ret.isChipRoom = isChipRoom
	log.Debug("slotadmgr running isChipRoom[%v]", isChipRoom)
	return ret
}

type slotConfig struct {
	totalAdCount  int
	freeSpinCount int
}

type slotadmgr struct {
	lock       *sync.RWMutex
	configs    map[int]slotConfig
	isChipRoom bool
}

func (sm *slotadmgr) initData() {
	sm.lock = &sync.RWMutex{}
	sm.configs = make(map[int]slotConfig)
}

func (sm *slotadmgr) getSlotConfig(gameId int) (totalAdCount, freeSpinCount int) {
	if sm.isChipRoom {
		return 0, 0
	}
	sm.lock.RLock()
	cfg, ok := sm.configs[gameId]
	sm.lock.RUnlock()
	if ok {
		return cfg.totalAdCount, cfg.freeSpinCount
	}
	_, _, totalAdCount, freeSpinCount = getUserInfo(0, gameId)
	sm.lock.Lock()
	sm.configs[gameId] = slotConfig{totalAdCount: totalAdCount, freeSpinCount: freeSpinCount}
	sm.lock.Unlock()

	return totalAdCount, freeSpinCount
}

func (sm *slotadmgr) getSlotUserInfo(userId int, gameId int) (adCount, winAmount int) {
	winAmount, adCount, totalAdCount, freeSpinCount := getUserInfo(userId, gameId)
	if userId == 0 {
		sm.lock.Lock()
		sm.configs[gameId] = slotConfig{totalAdCount: totalAdCount, freeSpinCount: freeSpinCount}
		sm.lock.Unlock()
	}
	return adCount, winAmount
}

func (sm *slotadmgr) useAd(userId, gameId int) (int, int) {
	if sm.isChipRoom {
		return 0, 0
	}
	success, adCount := useAd(userId, gameId)
	if !success {
		return 0, adCount
	}
	sm.lock.RLock()
	config, _ := sm.configs[gameId]
	sm.lock.RUnlock()
	return config.freeSpinCount, adCount
}

func (sm *slotadmgr) dump(param1, param2 string) {
	log.Debug("-------------------------------")
	log.Debug("slotadmgr.dump %s %s", param1, param2)
	defer func() {
		log.Debug("+++++++++++++++++++++++++++++++")
		log.Debug("")
	}()
	switch param1 {
	case "user":
		userId, err := strconv.Atoi(param2)
		if err != nil {
			log.Debug("    atoi error %v", err)
			return
		}
		adCount, winAmount := sm.getSlotUserInfo(userId, 0)
		log.Debug("        %d win[%d] count[%d]", userId, winAmount, adCount)
	}

}
