package admanager

func Run(isChipRoom bool) {
	mgr = newSlotAdManager(isChipRoom)
}

func GetSlotConfig(gameId int) (totalAdCount, freeSpinCount int) {
	return mgr.getSlotConfig(gameId)
}

func GetSlotUserInfo(userId int, gameId int) (adCount, winAmount int) {
	return mgr.getSlotUserInfo(userId, gameId)
}

func UseAd(userId, gameId int) (int, int) {
	return mgr.useAd(userId, gameId)
}

func Dump(param1 string, param2 string) {
	mgr.dump(param1, param2)
}
