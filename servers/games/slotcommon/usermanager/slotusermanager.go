package usermanager

import (
	"encoding/json"
	"os"
	"strconv"
	"sync"
	"time"

	"bet24.com/log"
)

var mgr *slotusermgr

const (
	check_inteval   = 300
	remove_interval = 1800
)

const newbie_lowest_amount = 10000000

type newbie_info struct {
	LowestAmount int
	SetCount     int
}

func newSlotUserManager() *slotusermgr {
	ret := new(slotusermgr)
	ret.ctor()
	log.Debug("slotusermgr running")
	return ret
}

type slotusermgr struct {
	lock     *sync.RWMutex
	userlist map[int]*slotuser

	lockRemove    *sync.RWMutex
	removingUsers map[int]int64

	//newbieLowest     map[int]int
	maxNoneFreeCount map[int]int

	Whitelist  []int
	newbieInfo newbie_info

	userTotalCountMgr *count_manager
}

func (um *slotusermgr) ctor() {
	um.lock = &sync.RWMutex{}
	um.userlist = make(map[int]*slotuser)

	um.lockRemove = &sync.RWMutex{}
	um.removingUsers = make(map[int]int64)
	//um.newbieLowest = make(map[int]int)
	um.maxNoneFreeCount = make(map[int]int)
	um.userTotalCountMgr = newCountManager()

	go um.checkRemoveUser()
}

/*
func (um *slotusermgr) setNewbieLowest(gameId int, amount int) {
	um.lock.Lock()
	um.newbieLowest[gameId] = amount
	um.lock.Unlock()
}
*/

func (um *slotusermgr) setMaxNoneFreeCount(gameId int, count int) {
	um.lock.Lock()
	um.maxNoneFreeCount[gameId] = count
	um.lock.Unlock()
}

/*func (um *slotusermgr) getNewbieLowest(gameId int) int {
	um.lock.RLock()
	defer um.lock.RUnlock()
	v, ok := um.newbieLowest[gameId]
	if !ok {
		return newbie_lowest_amount
	}
	return v
}*/
func (um *slotusermgr) isNewbie(amount, setCount int) bool {
	if amount > um.newbieInfo.LowestAmount {
		return false
	}
	if setCount > um.newbieInfo.SetCount {
		return false
	}
	return true
}

func (um *slotusermgr) getMaxNoneFreeCount(gameId int) int {
	um.lock.RLock()
	defer um.lock.RUnlock()
	v, ok := um.maxNoneFreeCount[gameId]
	if !ok {
		return 0
	}
	return v
}

func (um *slotusermgr) loadWhitelist() {
	data, err := os.ReadFile("slotconf/whitelist.json")
	um.lock.Lock()
	err = json.Unmarshal(data, &um.Whitelist)
	um.lock.Unlock()
	if err != nil {
		log.Release("slotusermgr.loadWhitelist failed %v", err)
	}
}

func (um *slotusermgr) loadNewbieInfo() {
	data, err := os.ReadFile("slotconf/newbieinfo.json")
	um.lock.Lock()
	err = json.Unmarshal(data, &um.newbieInfo)
	um.lock.Unlock()
	if err != nil {
		log.Release("slotusermgr.loadNewbieInfo failed %v", err)
	}
	if um.newbieInfo.SetCount == 0 {
		um.newbieInfo.SetCount = 10
	}
	if um.newbieInfo.LowestAmount == 0 {
		um.newbieInfo.LowestAmount = newbie_lowest_amount
	}

	log.Debug("slotusermgr newbieInfo %v", um.newbieInfo)
}

func (um *slotusermgr) checkRemoveUser() {
	time.AfterFunc(check_inteval*time.Second, um.checkRemoveUser)
	um.loadWhitelist()
	um.loadNewbieInfo()
	var toRemove []int
	latestRemoveTime := time.Now().Unix() - remove_interval
	um.lockRemove.RLock()
	for k, v := range um.removingUsers {
		if v < latestRemoveTime {
			toRemove = append(toRemove, k)
		}
	}
	um.lockRemove.RUnlock()
	if len(toRemove) == 0 {
		return
	}
	um.lockRemove.Lock()
	for _, v := range toRemove {
		delete(um.removingUsers, v)
	}
	um.lockRemove.Unlock()

	um.lock.Lock()
	for _, v := range toRemove {
		delete(um.userlist, v)
	}
	um.lock.Unlock()
}

func (um *slotusermgr) addUser(userId int) {
	if userId == 0 {
		return
	}
	um.lockRemove.Lock()
	delete(um.removingUsers, userId)
	um.lockRemove.Unlock()
	um.userTotalCountMgr.onUserEnter(userId)
	um.lock.Lock()
	_, ok := um.userlist[userId]
	if !ok {
		um.userlist[userId] = newSlotUser(userId)
	}
	um.lock.Unlock()

}

func (um *slotusermgr) removeUser(userId int) {
	um.lockRemove.Lock()
	defer um.lockRemove.Unlock()
	um.removingUsers[userId] = time.Now().Unix()
	go um.userTotalCountMgr.onUserExit(userId)

}

func (um *slotusermgr) getUser(userId int) *slotuser {
	um.lock.RLock()
	u, ok := um.userlist[userId]
	um.lock.RUnlock()
	if ok {
		return u
	}

	u = newSlotUser(userId)

	um.lock.Lock()
	um.userlist[userId] = u
	um.lock.Unlock()
	return u
}

func (um *slotusermgr) addResult(userId int, gameId int, betAmount int, winAmount int, isFree bool) {
	u := um.getUser(userId)
	u.addResult(gameId, betAmount, winAmount, isFree)
}

func (um *slotusermgr) getUserReturnLevel(userId, gameId, betAmount int) int {
	if um.isWhitList(userId) {
		return SlotReturnLevel_WhiteList
	}
	u := um.getUser(userId)
	if u == nil {
		return 0
	}
	return u.getReturnLevel(gameId, betAmount)
}

func (um *slotusermgr) isNewbieUser(userId int, gameId int) bool {
	u := um.getUser(userId)
	if u == nil {
		log.Debug("slotusermgr.isNewbieUser %d not exist", userId)
		return false
	}
	return u.isNewbie(gameId)
}

func (sm *slotusermgr) dump(param1, param2 string) {
	log.Release("-------------------------------")
	log.Release("slotusermgr.dump %s %s", param1, param2)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	switch param1 {
	case "user":
		userId, err := strconv.Atoi(param2)
		if err != nil {
			log.Release("    atoi error %v", err)
			return
		}
		u := sm.getUser(userId)
		if u == nil {
			log.Release("  %d not exist", userId)
			return
		}
		u.dump()
	default:
		for _, wl := range sm.Whitelist {
			log.Release("    Whitelist %d", wl)
		}
	}
}

func (sm *slotusermgr) isWhitList(userId int) bool {
	for _, v := range sm.Whitelist {
		if v == userId {
			return true
		}
	}
	return false
}
