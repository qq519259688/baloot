package usermanager

func Run() {
	mgr = newSlotUserManager()
}

func AddUser(userId int) {
	mgr.addUser(userId)
}

func RemoveUser(userId int) {
	mgr.removeUser(userId)
}

func AddResult(userId int, gameId int, betAmount int, winAmount int, isFree bool) {
	mgr.addResult(userId, gameId, betAmount, winAmount, isFree)
}

func GetUserReturnLevel(userId, gameId, betAmount int) int {
	return mgr.getUserReturnLevel(userId, gameId, betAmount)
}

/*
func SetNewbieLowest(gameId int, amount int) {
	mgr.setNewbieLowest(gameId, amount)
}
*/

func SetMaxNoneFreeCount(gameId, count int) {
	mgr.setMaxNoneFreeCount(gameId, count)
}

func Dump(param1 string, param2 string) {
	mgr.dump(param1, param2)
}

func IsNewbie(userId int, gameId int) bool {
	return mgr.isNewbieUser(userId, gameId)
}
