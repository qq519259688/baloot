package usermanager

import (
	"bet24.com/log"
	"bet24.com/utils"
	"sync"
)

const (
	SlotReturnLevel_Low = iota
	SlotReturnLevel_High
	SlotReturnLevel_Newbie
	SlotReturnLevel_Free
	SlotReturnLevel_WhiteList
)

// 用户不同游戏的统计数据
type usergame struct {
	gameId                int
	betAmount             int // 总下注额
	winAmount             int // 总返还
	betCount              int // 总局数
	freeCount             int
	currentLevel          int
	totalBetCount         int
	noneFreeContinueCount int // 多少局没出现free了
}

func newUserGame(userId, gameId int) *usergame {
	ug := new(usergame)
	// 读取总局数
	ug.gameId = gameId
	ug.totalBetCount = mgr.userTotalCountMgr.getTotalCount(userId, gameId)
	return ug
}

func (ug *usergame) isNewbie(betAmount int) bool {
	return mgr.isNewbie(betAmount, ug.totalBetCount)
}

func (ug *usergame) isFree() bool {
	maxNoneFree := mgr.getMaxNoneFreeCount(ug.gameId)
	if maxNoneFree == 0 {
		return false
	}
	return ug.noneFreeContinueCount >= maxNoneFree
}

func (ug *usergame) getReturnLevel(betAmount int) int {
	if ug.isFree() {
		return SlotReturnLevel_Free
	}
	if ug.isNewbie(betAmount) {
		return SlotReturnLevel_Newbie
	}
	if ug.betCount < 10 || ug.betAmount == 0 {
		return ug.currentLevel
	}

	if ug.betCount%10 != 0 {
		return ug.currentLevel
	}

	// 每10局做个变换
	rate := ug.winAmount * 100 / ug.betAmount
	if rate >= 95 {
		ug.currentLevel = SlotReturnLevel_Low
	} else if rate < 80 {
		ug.currentLevel = SlotReturnLevel_High
	}

	return ug.currentLevel
}

type slotuser struct {
	userId int
	games  map[int]*usergame
	lock   *sync.RWMutex
}

func newSlotUser(userId int) *slotuser {
	if userId == 0 {
		return nil
	}
	ret := new(slotuser)
	ret.userId = userId
	ret.lock = &sync.RWMutex{}
	ret.games = make(map[int]*usergame)
	// 把已有局数加载进来
	log.Debug("newSlotUser %d loading games", userId)
	ret.lock.Lock()
	for _, v := range slot_ids {
		ret.games[v] = newUserGame(userId, v)
	}
	ret.lock.Unlock()
	return ret
}

func (u *slotuser) addResult(gameId int, betAmount int, winAmount int, isFree bool) {
	u.lock.Lock()
	ug, ok := u.games[gameId]
	if !ok {
		ug = newUserGame(u.userId, gameId)
		u.games[gameId] = ug
	}
	if !isFree {
		ug.betAmount += betAmount
	} else {
		ug.freeCount++
	}
	ug.winAmount += winAmount
	ug.betCount++
	ug.totalBetCount++
	if isFree {
		ug.noneFreeContinueCount = 0
	} else {
		ug.noneFreeContinueCount++
	}
	u.lock.Unlock()
	mgr.userTotalCountMgr.addTotalCount(u.userId, gameId)
}

func (u *slotuser) getReturnLevel(gameId int, betAmount int) int {
	u.lock.RLock()
	ug, ok := u.games[gameId]
	u.lock.RUnlock()
	if !ok {
		return SlotReturnLevel_Low
	}
	// 这里增加判断条件
	return ug.getReturnLevel(betAmount)
}

func (u *slotuser) dump() {
	log.Release("    slotuser.dump %d", u.userId)
	u.lock.RLock()
	for k, v := range u.games {
		log.Release("      gameId[%d] level[%d]", k, v.getReturnLevel(100))
		log.Release("        total[%d],bet[%s],win[%s],betCount[%d],freeCount[%d]", v.totalBetCount, utils.FormatScore(v.betAmount),
			utils.FormatScore(v.winAmount), v.betCount, v.freeCount)
	}
	u.lock.RUnlock()
	log.Release("    ---------------------")
}

func (u *slotuser) isNewbie(gameId int) bool {
	u.lock.RLock()
	ug, ok := u.games[gameId]
	u.lock.RUnlock()
	if !ok {
		return true
	}
	return ug.isNewbie(0)
}
