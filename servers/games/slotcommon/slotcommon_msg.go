package slotcommon

import (
	"bet24.com/log"
	"bet24.com/servers/fishhall/config"
	"bet24.com/servers/games/slotcommon/betlevel"
	"bet24.com/servers/games/slotcommon/usermanager"
	"encoding/json"
)

type slot_message struct {
	MsgID int
	Data  string
}

const (
	Msg_Unlock_Bet = 100 // 解锁所有下注
)

func (sc *Slotcommon) sendMessage(userId int, msgId int, msgData string) {
	sm := slot_message{MsgID: msgId, Data: msgData}
	data, _ := json.Marshal(sm)
	sc.slotSink.SendGameCmd(userId, sc.gameMessage, string(data))
}

func (sc *Slotcommon) SetBetLevelManager(levelMgr *betlevel.BetLevelManager) {
	sc.levelMgr = levelMgr
}

func (sc *Slotcommon) SendBetLevel(userId int) {
	if sc.levelMgr == nil {
		log.Release("Slotcommon.sendBetLevel [%s] levelMgr == nil", sc.gameName)
		return
	}
	var data []byte
	if config.Server.IsChipRoom > 0 && usermanager.IsNewbie(userId, sc.gameId) {
		data, _ = json.Marshal(sc.levelMgr.NewbieLevels())
	} else {
		data, _ = json.Marshal(sc.levelMgr.Levels())
	}
	sc.sendMessage(userId, sc.levelMgr.MsgId(), string(data))
}

func (sc *Slotcommon) sendUnlockBet(userId int) {
	if config.Server.IsChipRoom > 0 {
		sc.sendMessage(userId, Msg_Unlock_Bet, "")
	}
}
