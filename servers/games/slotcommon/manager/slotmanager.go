package manager

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"bet24.com/log"
	"bet24.com/servers/games/TribalSlot"
	"bet24.com/servers/games/animalslot"
	"bet24.com/servers/games/minislot"
	"bet24.com/servers/games/rezekislot"
	"bet24.com/servers/games/slotcommon"
	"bet24.com/servers/games/slotcommon/admanager"
	"bet24.com/servers/games/slotcommon/usermanager"
	"bet24.com/servers/games/slotpanda"
)

type SlotMessage struct {
	MsgID int
	Data  string
}

func RunAllSlots(sink slotcommon.SlotSink) {
	admanager.Run(sink.IsChipRoom())
	usermanager.Run()
	TribalSlot.Run(sink)
	animalslot.Run(sink)
	slotpanda.Run(sink)

	rezekislot.Run(sink)
	minislot.Run(sink)
}

func OnUserLogined(userId int) {
	usermanager.AddUser(userId)
}

func OnUserExit(userId int) {
	usermanager.RemoveUser(userId)
	slotpanda.OnUserExit(userId)
	animalslot.OnUserExit(userId)

	rezekislot.OnUserExit(userId)

	TribalSlot.OnUserExit(userId)

}

func OnGameMessage(userId int, msg, data string) bool {
	if strings.Contains(msg, TribalSlot.GAME_MESSAGE) { // tribalslot
		TribalSlot.OnGameMessage(userId, msg, data)
		return true
	}
	if strings.Contains(msg, minislot.GAME_MESSAGE) { // tribalslot
		minislot.OnGameMessage(userId, msg, data)
		return true
	}
	if strings.Contains(msg, animalslot.GAME_MESSAGE) { // tribalslot
		animalslot.OnGameMessage(userId, msg, data)
		return true
	}

	if strings.Contains(msg, rezekislot.GAME_MESSAGE) { // rezekislot
		rezekislot.OnGameMessage(userId, msg, data)
		return true
	}

	if strings.Contains(msg, slotpanda.GAME_MESSAGE) { // slotpanda
		slotpanda.OnGameMessage(userId, msg, data)
		return true
	}
	return false
}

func Dump(game string) {

}

type config_read struct {
	GameName   string
	ConfigName string
}

type config_write struct {
	GameName   string
	ConfigName string
	Data       string
}

func GetConfigByCmd(requestData string) string {
	var f config_read
	err := json.Unmarshal([]byte(requestData), &f)
	if err != nil {
		log.Debug("slotmanager.GetConfig Unmarshal failed err:%s", requestData)
		return "invalid argument"
	}
	return GetConfig(f.GameName, f.ConfigName)
}

func GetConfig(gameName, configName string) string {
	data, e := os.ReadFile(fmt.Sprintf("slotconf/%s.json", configName))
	if e != nil {
		log.Debug("slotmanager.GetConfig config file[%s] not found", configName)
		return fmt.Sprintf("file[%s] not found", configName)
	}

	return string(data)
}

func WriteConfig(gameName, configName, data string) string {
	if !json.Valid([]byte(data)) {
		log.Debug("slotmanager.WriteConfig invalid data:%s", data)
		return "invalid data"
	}
	fileName := fmt.Sprintf("slotconf/%s.json", configName)
	_, e := os.ReadFile(fileName)
	if e != nil {
		log.Debug("slotmanager.GetConfig config file[%s] not found", fileName)
		return fmt.Sprintf("file[%s] not found", fileName)
	}

	os.WriteFile(fileName, []byte(data), 0644)
	return "ok"
}

func WriteConfigByCmd(requestData string) string {
	var f config_write
	err := json.Unmarshal([]byte(requestData), &f)
	if err != nil {
		log.Debug("slotmanager.WriteConfig Unmarshal failed err:%s", requestData)
		return "invalid argument"
	}
	return WriteConfig(f.GameName, f.ConfigName, f.Data)
}

func GetConfigList(gameName string) []string {
	var ret []string
	dir, err := os.ReadDir("slotconf")
	if err != nil {
		return ret
	}
	for _, fi := range dir {
		if !fi.IsDir() { // 忽略目录
			if gameName == "" {
				ret = append(ret, fi.Name())
			} else {
				if strings.Contains(fi.Name(), gameName) {
					ret = append(ret, fi.Name())
				}
			}

		}
	}
	return ret
}
