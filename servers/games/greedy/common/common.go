package common

import (
	"fmt"

	"bet24.com/servers/games/greedy/config"
	userservices "bet24.com/servers/micros/userservices/proto"
)

const (
	GAMEID    = 41
	GAME_NAME = "greedybigo"
)

const (
	GameState_Bet  = iota
	GameState_Open 
)

const (
	TIMERID_CHECKROBOT = iota
)

type UserSetScore struct {
	UserId int
	Score  int
}

type GameState struct {
	RoomName     string
	SerialNumber int
	State        int                  // 当前状态
	Sec          int                  // 当前阶段已使用时间
	SpinResult   BetOption            // 本局开奖结果
	Historys     []config.OpenHistory // 历史纪录
	AreaPopular  AreaPopular          // 区域热度
	TableUser    ScoreUsers           // 幸运星和赢金榜的玩家
	BetCmds      []Bet                // 本局下注列表
	CloseGame    bool                 // 本轮开奖后是否关闭游戏
}

type BetBase struct {
	BetId   int
	Amount  int
	Session int // 场次
}

type Bet struct {
	BetBase
	UserId int
	Index  int // 添加一个索引，用于失败撤销
}

type Batch_Bet struct {
	UserId int
	Bets   []BetBase
}

type RankingUser struct {
	UserId int
	Score  int
}

// 是否相同下注类型
func (b *Bet) IsSameBet(b2 *Bet) bool {
	return b.BetId == b2.BetId
	//&& b.Session == b2.Session
}

type BetRet struct {
	ErrorMsg string
}

type Result struct {
	Bet
	WinAmount  int
	SpinResult BetOption
	Tax        int
}

type RecordInfo struct {
	SpinResult   BetOption
	SerialNumber int
}

type UserClear struct {
	RoomName string
	UserId   int
}

// 连胜用户和累计下注用户
type ScoreUser struct {
	UserId      int
	NickName    string
	FaceId      int
	FaceUrl     string
	VipLevel    int
	VipExpire   int
	Decorations []userservices.UserDecoration
	IsRobot     bool `json:"-"`
	Info        []int
}

type ScoreUsers struct {
	WinGoldRankingUsers    []ScoreUser
	DayWinGoldRankingUsers []ScoreUser
}

type AreaPopular struct {
	MostPopular int   // 最受欢迎区域的索引
	BetCount    []int // 下注人次
	DiamondHot  []int // 钻石热度
}

func IsInTopN(rankList []ScoreUser, userId, n int) bool {
	for i, v := range rankList {
		if i >= n {
			break
		}
		if v.UserId == userId {
			return true
		}
	}
	return false
}

func GetRedisKey(key string) string {
	return fmt.Sprintf("%s:%s", GAME_NAME, key)
}
