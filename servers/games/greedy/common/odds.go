package common

import (
	"fmt"
	"math/rand"

	"bet24.com/log"
)

/*
	热狗，烤串，鸡腿，肉片，萝卜，玉米，菠菜，西红柿
	HotDog:10 //热狗
	Kebab:15 //烤串
	ChickenLeg:25 //鸡腿
	MeatSlice:45 //肉片
	Radish:5 // 萝卜
	Corn:5 //玉米
	Spinach:5 //菠菜
	Tomato:5 //西红柿
*/

type BetOption int

const (
	HotDog BetOption = iota
	Kebab
	ChickenLeg
	MeatSlice
	Radish
	Corn
	Spinach
	Tomato
	BidTypeMax
)

var betOptionNames = []string{
	"热狗",
	"肉串",
	"鸡腿",
	"肉片",
	"萝卜",
	"玉米",
	"菠菜",
	"西红柿",
}

var betWinningOdds = map[BetOption]float64{
	HotDog:     10,
	Kebab:      15,
	ChickenLeg: 25,
	MeatSlice:  45,
	Radish:     5,
	Corn:       5,
	Spinach:    5,
	Tomato:     5,
}

var betProbability = []int{
	1000, // HotDog
	667,  // Kebab
	400,  // ChickenLeg
	222,  // MeatSlice
	2000, // Radish
	2000, // Corn
	2000, // Spinach
	2000, // Tomato
}

func Spin() BetOption {
	// 计算下注区域的总概率
	var total float64
	for _, p := range betProbability {
		total += float64(p) / 10
	}

	// 生成一个随机数
	r := rand.Float64() * total

	// 根据随机数返回中奖结果
	var sum float64
	for i, p := range betProbability {
		sum += float64(p) / 10
		if r < sum {
			return BetOption(i)
		}
	}

	// 如果程序执行到这里，说明出现了意外情况，返回默认值
	return Corn
}

func GetOdds(spinResult BetOption) (odds float64) {
	odds = betWinningOdds[spinResult]
	return
}

func GetResultOdds(betId int, spinResult BetOption) (odds float64) {
	odds = 0
	if int(spinResult) == betId {
		odds = GetOdds(spinResult)
	}
	return
}

// 刷新获胜赔率
func ResetWinningOdds(winningOdds []float64) {
	for i, o := range winningOdds {
		f := BetOption(i)
		if f < HotDog || f > BidTypeMax {
			continue // Invalid bet index, skip
		}
		if betWinningOdds[f] != o {
			betWinningOdds[f] = o
		}
	}
}

// 刷新下注区域概率
func ResetBetProbability(probability []int) {
	for i, p := range probability {
		f := BetOption(i)
		if f < HotDog || f > BidTypeMax {
			continue // Invalid bet index, skip
		}
		if betProbability[i] != p {
			betProbability[i] = p
		}
	}
}

func GetBetDesc(betId int) string {
	max := int(BidTypeMax)
	if betId >= max || betId < int(HotDog) {
		log.Release("common.GetDesc failed %d,%d", betId, max)
		return "invalid bet"
	}
	return betOptionNames[betId]
}

func GetResultDesc(spinResult BetOption) string {
	return fmt.Sprintf("%d&[%s]", spinResult, betOptionNames[spinResult])
}
