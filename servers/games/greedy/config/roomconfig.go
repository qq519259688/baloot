package config

import (
	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
	"encoding/json"
	"fmt"
	"os"
)

const config_key = "greedybigo_rooms"
const config_key_chip = "greedybigo_chip_rooms"
const config_key_private = "greedybigo_private_rooms"

type OpenHistory struct {
	SerialNumber int
	SpinResult   int
}

type RoomInfoBase struct {
	RoomName     string
	ServerIP     string
	ServerPort   int
	MinBet       int
	MaxBet       int //个人最大下注额
	TotalMax     int
	BetTime      int
	OpenTime     int
	HistoryCount int
	State        int
	StateSec     int
	SerialNumber int
	Historys     []OpenHistory
	TotalBet     int
	Odds         []float64
	TaxRate      int
}

type RoomInfo struct {
	RoomInfoBase
	MinRoom     int
	RobotConfig []RobotInfo // 机器人配置
	JackpotRate int         // 奖池比例
	ChipAmounts []int       // 机器人筹码配置
	Probability []int       // 概率
}

type RobotInfo struct {
	HourStart int
	HourEnd   int
	CountMin  int
	CountMax  int
	GoldMin   int
	GoldMax   int
	Online    int
}

var Rooms struct {
	Rooms []RoomInfo
}

var Room *RoomInfo

var RoomConfigName string

func getConfigKey() string {
	// 私人场优先
	if Server.IsPrivateRoom {
		return config_key_private
	}
	if Server.IsChipRoom {
		return config_key_chip
	}

	return config_key
}

func loadRedisConfig(createRoom bool) bool {
	data := platformconfig.GetConfig(getConfigKey())
	if data == "" {
		log.Release("config msg is null")
		return false
	}

	ok, _ := marshalData([]byte(data))
	return ok
}

func loadRoomConfig() {
	if loadRedisConfig(true) {
		log.Release("using remote config ignored rooms.json !!!")
		return
	}
	// 保存
	// 加载房间表
	data, err := os.ReadFile("greedy/rooms.json")
	//fmt.Println(string(data))
	if err != nil {
		log.Release("read rooms failed greedy/rooms.json")
		return
	}
	if ok, newConfig := marshalData(data); ok {
		// 写入redis
		platformconfig.SetConfig(getConfigKey(), newConfig)
	}
}

func marshalData(data []byte) (bool, string) {
	changedConfigString := string(data)
	err := json.Unmarshal(data, &Rooms)
	if err != nil {
		log.Release("Unmarshal rooms failed err:%v", err)
		return false, changedConfigString
	}
	if len(Rooms.Rooms) <= 0 {
		log.Release("Rooms.Rooms == 0")
		return false, changedConfigString
	}

	Room = &Rooms.Rooms[0]

	if len(Server.ServerIP) > 0 {
		ws := "ws"
		if len(Server.CertFile) > 0 {
			ws = "wss"
		}
		for i := 0; i < len(Rooms.Rooms); i++ {
			Rooms.Rooms[i].ServerIP = fmt.Sprintf("%s://%s:%d", ws, Server.ServerIP, Server.ServerPort)
		}
	}

	return true, changedConfigString
}

func OnConfigChanged(key string) {
	if key != getConfigKey() {
		return
	}
	log.Release("正在刷新配置信息")
	loadRedisConfig(false)
}
