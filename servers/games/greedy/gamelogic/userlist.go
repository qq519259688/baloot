package gamelogic

import (
	"sort"
	"sync"

	"bet24.com/log"
	"bet24.com/servers/games/greedy/common"
	userservices "bet24.com/servers/micros/userservices/proto"
)

type BetArea struct {
	Area  int
	Count int
}

type greedyUser struct {
	userId      int
	nickName    string
	faceId      int
	faceUrl     string
	vipLevel    int
	vipExpire   int
	decorations []userservices.UserDecoration

	totalBet            int
	winAmount           int //实际赢金 输了不计
	cumulativeWinAmount int //累计赢金 输了不计
	winAmountOnly       int //只计算赢的金额
	isBet               bool
}

type userlist struct {
	users []*greedyUser
	lock  *sync.RWMutex
}

func newUserList() *userlist {
	ret := new(userlist)
	ret.lock = &sync.RWMutex{}
	return ret
}

func (ul *userlist) addUser(userId int, nickName string,
	faceId int, faceUrl string, vipLevel, vipExpire int, decorations []userservices.UserDecoration) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			log.Release("userlist.addUser %d already exist", userId)
			return
		}
	}
	u := new(greedyUser)

	u.userId = userId
	u.nickName = nickName
	u.faceId = faceId
	u.faceUrl = faceUrl
	u.vipLevel = vipLevel
	u.vipExpire = vipExpire
	u.decorations = decorations
	ul.users = append(ul.users, u)

}

func (ul *userlist) removeUser(userId int) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for k, v := range ul.users {
		if v.userId == userId {
			ul.users = append(ul.users[:k], ul.users[k+1:]...)
			return
		}
	}
	log.Release("userlist.removeUser %d not found", userId)
}

// 结束的时候清理状态，并且根据未下注进行连胜扣减
func (ul *userlist) clearBetStatus() {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if !v.isBet {
			if v.winAmount > 0 {
				v.winAmount = 0
			}
			v.winAmountOnly = 0
		} else {
			v.isBet = false
			v.winAmountOnly = 0
		}
	}
}

func (ul *userlist) addResult(userId, winAmount, winAmountOnly int) {
	log.Debug("userlist.addResult %d,%d", userId, winAmount)
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			if winAmount < 0 {
				v.winAmount = 0
			}
			if winAmountOnly > 0 {
				v.winAmountOnly = winAmountOnly
			} else {
				v.winAmountOnly = 0
			}
			if winAmount > 0 {
				v.winAmount = winAmount
				v.cumulativeWinAmount = v.cumulativeWinAmount + winAmount
			}
			return
		}
	}
	log.Release("userlist.addResult %d not found", userId)
}

func (ul *userlist) addBet(userId, amount, betId int) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			v.totalBet += amount
			v.isBet = v.totalBet > 0
			return
		}
	}
	log.Release("userlist.addBet %d not found", userId)
}

func (ul *userlist) clearBet(userId, amount int) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			v.totalBet -= amount
			v.isBet = v.totalBet > 0
			//撤销下注，不需要更新下注区域
			return
		}
	}
	log.Release("userlist.addBet %d not found", userId)
}

// 每回合赢金排行
func (ul *userlist) getWinGoldRankingUsers(count int) []common.ScoreUser {

	ul.lock.Lock()
	defer ul.lock.Unlock()
	//排序
	sort.Slice(ul.users, func(i, j int) bool {
		return ul.users[i].winAmountOnly > ul.users[j].winAmountOnly
	})

	ret := []common.ScoreUser{}
	for k, v := range ul.users {
		if k >= count {
			break
		}
		if v.winAmountOnly <= 0 {
			break
		}
		info := make([]int, 0, 1)
		info = append(info, v.winAmountOnly)
		u := common.ScoreUser{UserId: v.userId, NickName: v.nickName,
			FaceId:      v.faceId,
			FaceUrl:     v.faceUrl,
			VipLevel:    v.vipLevel,
			VipExpire:   v.vipExpire,
			Decorations: v.decorations,
			Info:        info}

		ret = append(ret, u)
	}
	return ret

}
