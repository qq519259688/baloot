package gamelogic

import (
	"encoding/json"
	"os"
	"sync"
	"time"

	"bet24.com/log"
	"bet24.com/servers/games/greedy/common"
	"bet24.com/servers/games/greedy/config"
	"bet24.com/servers/insecureframe/frame"
)

type tablesink struct {
	table         frame.Table
	LastStateTick time.Time
	userBetList   map[int][]common.Bet
	timer         *time.Timer
	lock          *sync.RWMutex
	spinResult    common.BetOption
	closeGame     bool
	noPlayerCount int // 无人参与的局数数量
	userList      *userlist

	score_users *common.ScoreUsers

	robotActions   []robot_action
	robotStopEvent chan bool
	robotLock      *sync.RWMutex

	scores  []common.UserSetScore // 结算阶段，广播所有人的分数,给桌面玩家或者自己头像飘分用
	winRank *winPool

	hotDogAmount     int
	kebabAmount      int
	chickenLegAmount int
	meatSliceAmount  int

	radishAmount  int
	cornAmount    int
	spinachAmount int
	tomatoAmount  int

	drawMeatSliceWinAmount int //记录魔灯投注金额 用于避免重复开大奖

	areaPopular *common.AreaPopular
	privateData string

	roomInfo config.RoomInfo
}

func newTableSink(table frame.Table, data string) *tablesink {
	ts := new(tablesink)
	ts.table = table
	ts.privateData = data
	ts.lock = &sync.RWMutex{}

	err := json.Unmarshal([]byte(data), &ts.roomInfo)
	if err != nil {
		found := false
		for _, v := range config.Rooms.Rooms {
			if data == v.RoomName {
				ts.roomInfo = v
				found = true
				break
			}
		}
		if !found {
			ts.roomInfo = config.Rooms.Rooms[0]
		}
	}

	if ts.roomInfo.HistoryCount == 0 {
		ts.roomInfo.HistoryCount = 10
	}
	//记录这是今天的第几局
	ts.roomInfo.SerialNumber = ts.currentGameNumber()
	ts.winRank = newWinPool()
	ts.userList = newUserList()
	ts.areaPopular = new(common.AreaPopular)
	ts.score_users = new(common.ScoreUsers)
	ts.robotLock = &sync.RWMutex{}
	ts.closeGame = false
	ts.noPlayerCount = 0
	ts.startPeriod()
	if ts.test() {
		log.Debug("tablesink.setFrame test end")
	}

	table.SetTimer(common.TIMERID_CHECKROBOT, 5000)
	return ts
}

func (ts *tablesink) test() bool {
	if len(os.Args) < 2 {
		return false
	}
	common.ResetWinningOdds(ts.roomInfo.Odds)
	common.ResetBetProbability(ts.roomInfo.Probability)
	bidTypeMax := int(common.BidTypeMax)
	testCount := 1000000
	total := float64(-testCount * int(common.BidTypeMax))
	returns := make([]int, bidTypeMax)
	for i := 0; i < testCount; i++ {
		spinResult := common.Spin()
		for j := 0; j < bidTypeMax; j++ {
			odds := common.GetResultOdds(j, spinResult)
			if odds > 0 {
				returns[j]++
				total += odds
			}
		}
	}
	testResult := make([]float64, bidTypeMax)
	for j := 0; j < bidTypeMax; j++ {
		testResult[j] = float64(returns[j]) / float64(testCount) * common.GetResultOdds(j, -1)
	}
	log.Debug("testCount [%d] result %v total = %f", testCount, testResult, total)

	for j := 0; j < bidTypeMax; j++ {
		ts.testOne(j)
	}

	return true
}

func (ts *tablesink) testOne(betType int) {
	testCount := 10000
	returns := float64(0)
	for i := 0; i < testCount; i++ {
		spinResult := common.Spin()
		odds := common.GetResultOdds(betType, spinResult)
		if odds > 0 {
			returns += odds
		}
	}

	log.Debug("[%s] testCount [%d] returns %v rate = %f", common.GetBetDesc(betType), testCount, returns, float64(returns)/float64(testCount))
}
func (ts *tablesink) Destroy() {
	ts.table.LogWithTableId("------tablesink：Destroy-------")
	//close(ts.stopChan)
}

func (ts *tablesink) OnUserEnterTable(userIndex int32, chairId int) {
	u, _ := ts.table.GetUser(userIndex)
	if u == nil {
		log.Debug("tablesink.OnUserEnterTable %d not exist", userIndex)
		return
	}
	ts.winRank.addUser(u.GetUserId())
	if !u.IsRobot() {
		log.Debug("gamesink.OnUserEnter %d[%s]", userIndex, u.GetUserNickName())
		// 发送配置信息
		ts.sendGameOption(userIndex)
		// 发送场景
		ts.sendGameScene(userIndex)
		ts.sendDayWin(userIndex, u.GetUserId())
	}
	ts.userList.addUser(u.GetUserId(), u.GetUserNickName(), u.GetUserFaceId(), u.GetUserFaceUrl(), u.GetUserVipLevel(), u.GetUserVipExpire(), u.GetDecorations())
}

func (ts *tablesink) OnUserExitTable(userIndex int32, chairId int) {
	//判断一下用户是否有下注
	usr, _ := ts.table.GetUser(userIndex)
	if usr == nil {
		log.Debug("tablesink.OnUserExit %d not exist", userIndex)
		return
	}
	userId := usr.GetUserId()
	ts.lock.RLock()
	_, isBet := ts.userBetList[userId]
	ts.lock.RUnlock()
	if usr.IsRobot() {
		if !ts.winRank.isRank(userId) {
			ts.winRank.removeUser(userId)
		}
	}
	if !isBet {
		ts.userList.removeUser(userId)
		return
	}

	ts.userList.removeUser(userId)
}

func (ts *tablesink) OnUserOffline(chairId int) {

}

func (ts *tablesink) OnUserReplay(chairId int) {

}

func (ts *tablesink) OnUserReady(userIndex int32, chairId int) {

}

func (ts *tablesink) OnUserCancelReady(userIndex int32, chairId int) {

}

func (ts *tablesink) OnGetChairScene(chairId int, isPlayer bool) string {
	return ts.getStateData()
}

func (ts *tablesink) OnGetPrivateRoomScene(chairId int) string {
	return ts.getStateData()
}

func (ts *tablesink) OnGetChairCount() int {
	return 1
}

func (ts *tablesink) OnTimer(timerId int) {
	switch timerId {
	case common.TIMERID_CHECKROBOT:
		ts.checkRobot()
		ts.table.SetTimer(timerId, 5000)
	default:
		ts.table.LogWithTableId("tablesink.OnTimer unhandled timer[%d]", timerId)
	}
}

func (ts *tablesink) DumpScene() {

}

func (ts *tablesink) GetGoldLimit() (min, max int) {
	return ts.roomInfo.MinBet, ts.roomInfo.MaxBet
}

func (ts *tablesink) IsDual() bool {
	return false
}

func (ts *tablesink) OnBaseScoreChanged(baseScore int) {

}

func (ts *tablesink) SetPrivateRoomParam(param int, value string) {
	ts.table.LogWithTableId("tablesink.SetPrivateRoomParam %d:%s", param, value)

}

func (ts *tablesink) OnPrivateRoomStatusChanged(oldStatus, newStatus int) {
	ts.table.LogWithTableId("OnPrivateRoomStatusChanged %d->%d", oldStatus, newStatus)
}

func (ts *tablesink) OnPrivateRoomDismissed() {
	ts.table.LogWithTableId("OnPrivateRoomDismissed ")
	log.Debug("tablesink.OnPrivateRoomDismissed")
	ts.closeGame = true
}
