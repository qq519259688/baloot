package gamelogic

import (
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/games/greedy/common"
	"bet24.com/servers/insecureframe/message"
	"bet24.com/servers/transaction"
	"bet24.com/servers/user"
)

func (ts *tablesink) OnGameMessage(userIndex int32, msg, data string) bool {
	/*
		if !ok {
			log.Debug("tablesink.OnGameMessage user not exist %d", userIndex)
			return false
		}
	*/
	log.Debug("tablesink.OnGameMessage Receive Msg  %d,%v,%v", userIndex, msg, data)
	usr, _ := ts.table.GetUser(userIndex)
	if usr == nil {
		log.Debug("tablesink.OnGameMessage user not exist")
		return false
	}
	userId := usr.GetUserId()
	switch msg {
	case History:
		ts.sendHistory(userIndex)
	case Option:
		ts.sendGameOption(userIndex)
	case Bet:
		ts.handleBet(userIndex, userId, data)
	case BatchBet:
		ts.handleBatchBet(userIndex, userId, msg, data)
	case BetClear:
		ts.onBetClear(userIndex, userId, msg, data)
	case BetRecord:
		ts.onGetBetRecord(userIndex, userId, msg, data)
	case Popularity:
		ts.onGetPopularity(userIndex, userId, msg, data)
	case WinRank:
		ts.onGetWinRank(userIndex, userId, msg, data)
	default:
		log.Debug("tablesink.OnGameMessage unhandled message %v  data %v", msg, data)
	}
	return true
}

func (ts *tablesink) sendBetFailed(userIndex int32, index int, errMsg string) {
	var bet_failed struct {
		ErrorMsg string
		Index    int
	}
	bet_failed.Index = index
	bet_failed.ErrorMsg = errMsg
	d, _ := json.Marshal(bet_failed)
	ts.table.SendGameData(userIndex, Betfailed, string(d))
}

func (ts *tablesink) sendBetOK(userIndex int32, data string) {
	ts.table.SendGameData(userIndex, Bet, data)
}

func (ts *tablesink) sendHistory(userIndex int32) {
	ts.table.SendGameData(userIndex, History, ts.getHistory())
}

func (ts *tablesink) sendGameScene(userIndex int32) {
	ts.table.SendGameData(userIndex, message.Table_GameScene, ts.getStateData())
}

func (ts *tablesink) sendDayWin(userIndex int32, userId int) {
	_, _, winAmount := ts.winRank.getRankList(userId, -1)
	d, _ := json.Marshal(winAmount)
	ts.table.SendGameData(userIndex, DayWin, string(d))
}

// 处理筹码下注
func (ts *tablesink) handleBet(userIndex int32, userId int, data string) {
	usr, _ := ts.table.GetUser(userIndex)
	if usr == nil {
		log.Debug("tablesink.handleBet user not exist")
		return
	}
	// log.Debug("handleBet %v", data)
	var bet common.Bet
	e := json.Unmarshal([]byte(data), &bet)
	if e != nil {
		log.Release("handleBet Unmarshal data failed %v |e=%v:", data, e)
		return
	}

	amount := bet.Amount
	bet.UserId = userId

	if amount <= 0 {
		ts.sendBetFailed(userIndex, bet.Index, "Invalid Bet!")
		return
	}
	// 看下能否下注
	ok, errMsg := ts.isCanBet(bet)
	if !ok {
		ts.sendBetFailed(userIndex, bet.Index, errMsg)
		return
	}
	ret, _ := ts.table.WriteUserMoney(userId, -amount, 0, 1, common.GAMEID*100+1, ts.roomInfo.RoomName)

	if !ret {
		// 扣金币失败
		ts.sendBetFailed(userIndex, bet.Index, "Not enough cash!")
		return
	}

	ok, err := ts.addBet(bet)
	if !ok {
		ts.table.WriteUserMoney(userId, amount, 0, 3, common.GAMEID*100+3, ts.roomInfo.RoomName)
		ts.sendBetFailed(userIndex, bet.Index, err)
	} else {
		ts.handleBetSuccess(userId, bet, usr)
		d, _ := json.Marshal(bet)
		ts.sendBetOK(-1, string(d))
	}
}

// 处理批量下注
func (ts *tablesink) handleBatchBet(userIndex int32, userId int, msg, data string) {
	usr, _ := ts.table.GetUser(userIndex)
	if usr == nil {
		log.Debug("tablesink.handleBatchBet user not exist")
		return
	}
	var batchBet common.Batch_Bet
	e := json.Unmarshal([]byte(data), &batchBet)
	if e != nil {
		log.Release("handleBatchBet Unmarshal data failed %v", data)
		return
	}

	// 先判断所有区域是否能下
	totalAmount := 0
	for _, v := range batchBet.Bets {
		var bet common.Bet
		bet.BetBase = v
		bet.UserId = userId
		ok, errMsg := ts.isCanBet(bet)
		if !ok {
			log.Debug("handleBatchBet failed to add bet %v,%v", bet, errMsg)
			ts.sendBetFailed(userIndex, bet.Index, errMsg)
			return
		}
		totalAmount += bet.Amount
	}

	ret, _ := ts.table.WriteUserMoney(userId, -totalAmount, 0, 1, common.GAMEID*100+1, ts.roomInfo.RoomName)

	if !ret {
		// 扣金币失败
		ts.sendBetFailed(userIndex, 0, "Not enough cash!")
		return
	}

	for _, v := range batchBet.Bets {
		var bet common.Bet
		bet.BetBase = v
		bet.UserId = userId

		ok, _ := ts.addBet(bet)
		if !ok {
			go ts.table.WriteUserMoney(userId, bet.Amount, 0, 3, common.GAMEID*100+3, ts.roomInfo.RoomName)
		} else {
			ts.handleBetSuccess(userId, bet, usr)
		}
	}

	ts.table.SendGameData(-1, msg, data)
}

// 成功下注后处理handleBetSuccess
func (ts *tablesink) handleBetSuccess(userId int, bet common.Bet, usr *user.UserInfo) {
	ts.lock.Lock()
	if !usr.IsRobot() {
		amount := bet.Amount
		switch bet.BetId {
		case int(common.HotDog):
			ts.hotDogAmount += amount
		case int(common.Kebab):
			ts.kebabAmount += amount
		case int(common.ChickenLeg):
			ts.chickenLegAmount += amount
		case int(common.MeatSlice):
			ts.meatSliceAmount += amount
			ts.drawMeatSliceWinAmount += amount
		case int(common.Radish):
			ts.radishAmount += amount
		case int(common.Corn):
			ts.cornAmount += amount
		case int(common.Spinach):
			ts.spinachAmount += amount
		case int(common.Tomato):
			ts.tomatoAmount += amount
		}
	}

	betCount := ts.areaPopular.BetCount[bet.BetId]
	//必须达到热区最低要求才更新
	if betCount <= 20 {
		ts.lock.Unlock()
		return
	}
	//是否推送
	isPush := false
	// 根据下注人次更新钻石热度
	if ts.areaPopular.MostPopular != -1 {
		// 更新最受欢迎的区域
		maxCount := ts.areaPopular.BetCount[ts.areaPopular.MostPopular]
		if betCount > maxCount {
			ts.areaPopular.MostPopular = bet.BetId
			isPush = true
		}
	} else {
		ts.areaPopular.MostPopular = bet.BetId
		isPush = true
	}
	// 当前钻石数量
	diamondHotOld := ts.areaPopular.DiamondHot[bet.BetId]

	diamondHotNew := diamondHotOld
	if betCount <= 50 {
		diamondHotNew = 1
	} else if betCount > 50 && betCount <= 100 {
		diamondHotNew = 2
	} else if betCount > 100 {
		diamondHotNew = 3
	}

	// 更新钻石数量
	ts.areaPopular.DiamondHot[bet.BetId] = diamondHotNew

	// 如果钻石数量发生了改变，触发推送
	if diamondHotNew != diamondHotOld {
		isPush = true
	}

	if isPush {
		d, _ := json.Marshal(ts.areaPopular)
		ts.table.SendGameData(-1, Popularity, string(d))
	}
	ts.lock.Unlock()
}

func (ts *tablesink) onBetClear(userIndex int32, userId int, msg, data string) {
	ts.lock.Lock()
	betList, ok := ts.userBetList[userId]
	if !ok {
		ts.lock.Unlock()
		return
	}

	amount := 0
	for _, v := range betList {
		ts.roomInfo.TotalBet -= v.Amount
		amount += v.Amount
	}
	delete(ts.userBetList, userId)
	ts.lock.Unlock()
	ts.table.WriteUserMoney(userId, amount, 0, 3, common.GAMEID*100+3, ts.roomInfo.RoomName)

	betClear := common.UserClear{UserId: userId}
	d, _ := json.Marshal(betClear)
	ts.table.SendGameData(userIndex, msg, string(d))
	ts.userList.clearBet(userId, amount)
}

func (ts *tablesink) onGetBetRecord(userIndex int32, userId int, msg, data string) {
	//log.Debug("tablesink.onGetBetRecord %v", data)
	var getRecord struct {
		RecordCount int
	}
	e := json.Unmarshal([]byte(data), &getRecord)
	if e != nil {
		log.Release("tablesink.onGetBetRecord Unmarshal data failed %v", data)
		return
	}

	records := transaction.GetGameRecord(userId, common.GAMEID, ts.roomInfo.RoomName, getRecord.RecordCount)
	retData, _ := json.Marshal(records)
	ts.table.SendGameData(userIndex, msg, string(retData))
}

func (ts *tablesink) onGetPopularity(userIndex int32, userId int, msg, data string) {
	d, _ := json.Marshal(ts.areaPopular)
	ts.table.SendGameData(userIndex, msg, string(d))
}

func (ts *tablesink) sendGameOption(userIndex int32) {
	d, _ := json.Marshal(ts.roomInfo.RoomInfoBase)
	ts.table.SendGameData(userIndex, Option, string(d))
}

func (ts *tablesink) onGetWinRank(userIndex int32, userId int, msg, data string) {
	//获取winRank前10名
	rankList := ts.GetWinRank(0)
	log.Debug("tablesink.onGetWinRank %v,userId:%d,rankList:%v", data, userId, rankList)
	//不为空
	if len(rankList) > 0 {
		retData, _ := json.Marshal(rankList)
		ts.table.SendGameData(userIndex, msg, string(retData))
	} else {
		ts.table.SendGameData(userIndex, msg, "[]")
	}

}
