package gamelogic

const (
	Broadcast   = "broadcast"
	TotalResult = "totalresult"
	Option      = "Option"
	Result      = "result"
	Config      = "config"
	History     = "history"
	Bet         = "bet"
	BatchBet    = "batchbet"
	BetClear    = "betclear"
	Betfailed   = "betfailed"
	BetRecord   = "betrecord"
	BetList     = "betlist"    // 投注列表
	Popularity  = "popularity" // 人气值
	WinRank     = "winrank"    // 赢金排行榜
	DayWin      = "daywin"     // 今日赢金
)
