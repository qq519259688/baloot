package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/games/quickludo/config"
	"bet24.com/servers/games/quickludo/gamelogic"
	"bet24.com/servers/insecureframe/frame"
	"bet24.com/servers/insecureframe/gate"
	robotmanager "bet24.com/servers/insecureframe/robot"

	"bet24.com/servers/transaction"
	"bet24.com/utils"
)

func waitInput() {
	for {
		var cmd string
		var param1 string
		var param2 string
		fmt.Scanf("%s %s %s", &cmd, &param1, &param2)
		switch cmd {
		case "exit":
			gamelogic.Stopping = true
			robotmanager.Exit()
			frame.StopServer()
			go transaction.DoGameRoomPing(gamelogic.GAMEID, 1, config.RoomConfigName)
		case "listuser":
			gate.DumpUsers()
		case "robotlist":
			robotmanager.Dump()
		case "robotWin":
			log.Release(config.Rooms.Dump())
		default:
			if !frame.Dump(cmd, param1, param2) {
				log.Release("unknown command")
			}
		}
	}
}

func startRoomPing() {
	if gamelogic.Stopping {
		return
	}
	time.AfterFunc(20*time.Second, startRoomPing)
	//将本server的Ip和端口写入redis
	for i := 0; i < len(config.Rooms.Rooms); i++ {
		d, _ := json.Marshal(config.Rooms.Rooms[i].RoomInfoBase)
		// fmt.Println(string(d))
		go transaction.DoGameRoomPing(gamelogic.GAMEID, 2, config.Rooms.Rooms[i].RoomName)
		frame.UpdateRoomList(config.Rooms.Rooms[i].RoomName, config.Rooms.Rooms[i].RoomDesc, string(d), config.Rooms.Rooms[i].RoomID-1)
	}
}

func main() {
	defer waitInput()
	rand.Seed(time.Now().UnixNano())
	coreservice.SetServiceAddr(config.Server.ServiceAddr)
	// 可以从coreservice中读取配置
	config.Run()
	utils.SetErrorFile("log/quickludo/err.log", "quickludo starting")
	// 把数据库链接都创建起来
	gamelogic.Run()

	//monitor.Run(config.Server.MonitorPort)
	startRoomPing()
}
