package gamelogic

import (
	"bet24.com/servers/games/quickludo/config"
	"bet24.com/servers/insecureframe/frame"
)

var Stopping bool

func Run() {
	Stopping = false
	frame.RunFrameWithLogServer(newGameSink(),
		config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB, config.Server.LogPath)
}
