package slotpanda

const (
	COLUMN_COUNT = 5
	ROW_COUNT    = 3
	RESULT_COUNT = 15
	GAMEID       = 55
	GAME_NAME    = "bambooslot"
	GAME_MESSAGE = "pandaslot"
)

type SlotCount struct {
	SlotID int
	Count  int
}

// 中奖连线信息
type Result struct {
	SlotID     int // 格子ID
	SlotCount  int // 数量
	WinShapeID int // 连线ID
	WinRate    int // 赢的倍数
}

// 消息定义
const (
	Msg_Bet          = 1  // 下注
	Msg_Result       = 2  // 发送结果
	Msg_FreeResult   = 20 // 发送FreeSpin结果
	Msg_Jackpot      = 3  // 获取彩池额度
	Msg_Slots        = 4  // 获取目前使用的Slot类型
	Msg_WinShapes    = 5
	Msg_GetConfig    = 6  // 获取配置信息
	Msg_Broadcast    = 7  // 奖池广播
	Msg_Enter        = 8  // 进入游戏
	Msg_Exit         = 9  // 离开游戏
	Msg_FreeAdConfig = 10 // 广告配置及自己的剩余
	Msg_UseAd        = 11 // 看广告后请求免费次数
)

// 错误码
const (
	ErrCode_InvalidBet     = 1 // 玩家投注不合理
	ErrCode_NotEnoughMoney = 2 // 玩家金币不足
	ErrCode_NotExist       = 3 // 玩家已退出不存在
)

type Slot_Message struct {
	MsgID int
	Data  string
}

type SlotPanda_Bet struct {
	Amount   int
	ErrorMsg string
	ErrCode  int
}

type SlotPanda_Result struct {
	BetAmount     int      // 下注金额
	WinAmount     int      // 共赢金额
	Slots         []int    // 15个格子ID
	Lines         []Result // 中奖连线
	FreeSpinInner int      // 如果产生freespin，内圈倍数
	FreeSpinOuter int      // 如果产生freespin，外圈倍数  不产生为0
	Bonus         *Bonus
}

type TestSlots struct {
	Slots []int
}

type SlotPanda_FreeAdBet struct {
	AdCount   int
	BetAmount int
}

type SlotPanda_FreeAdConfig struct {
	TotalAdCount  int // 每日免费次数
	AdCount       int // 剩余次数
	FreeSpinCount int // 看广告后获取的免费次数
	Bet           int // 本次看视频获得的底注
	AdBets        []SlotPanda_FreeAdBet
}

// 下行数据，上行无数据
type SlotPanda_UsedAd struct {
	FreeSpinCount   int // 获取免费次数，如果为0 表示没有免费
	Bet             int // 免费次数的底注
	AdCount         int // 剩余次数
	NextAdBetAmount int // 下次广告的底注
}

type SlotPanda_Jackpot struct {
	Amount int
}
