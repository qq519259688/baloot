package slotpanda

import (
	"bet24.com/log"
	"bet24.com/servers/games/slotcommon"
)

const LogColor = log.Red

var logic *GameLogic

func Run(slotSink slotcommon.SlotSink) {
	logic = NewGameLogic(slotSink)
	logic.run()
}

func OnGameMessage(userId int, msg, data string) {
	logic.onSlotMessage(userId, msg, data)
}

func OnUserExit(userId int) {
	logic.userExit(userId)
}
