package slotpanda

import (
	"encoding/json"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/games/slotcommon/admanager"
	"bet24.com/utils"
)

func (this *GameLogic) writeSlotGameMsg(userId int, gameMsg Slot_Message) {
	data, _ := json.Marshal(gameMsg)
	this.slotSink.SendGameCmd(userId, GAME_MESSAGE, string(data))
}

func (this *GameLogic) sendSlotBet(userId int, errMsg string, errCode, amount int) {
	var msg Slot_Message
	msg.MsgID = Msg_Bet
	data, _ := json.Marshal(SlotPanda_Bet{Amount: amount, ErrorMsg: errMsg, ErrCode: errCode})
	msg.Data = string(data)
	this.writeSlotGameMsg(userId, msg)
}

func (this *GameLogic) sendSlotResult(userId int, result SlotPanda_Result) {
	var msg Slot_Message
	msg.MsgID = Msg_Result
	data, _ := json.Marshal(result)
	msg.Data = string(data)
	this.writeSlotGameMsg(userId, msg)
}

func (this *GameLogic) sendFreeSlotResult(userId int, result FreeSpinResult) {
	var msg Slot_Message
	msg.MsgID = Msg_FreeResult
	data, _ := json.Marshal(result)
	msg.Data = string(data)
	this.writeSlotGameMsg(userId, msg)
}

func (this *GameLogic) sendSlots(userId int) {
	var msg Slot_Message
	msg.MsgID = Msg_Slots
	data, _ := json.Marshal(this.getSlots())
	msg.Data = string(data)
	this.writeSlotGameMsg(userId, msg)
}

func (this *GameLogic) sendWinShapes(userId int) {
	var msg Slot_Message
	msg.MsgID = Msg_WinShapes
	data, _ := json.Marshal(this.getWinShapes())
	msg.Data = string(data)
	this.writeSlotGameMsg(userId, msg)
}

func (this *GameLogic) onSlotMessage(userId int, msg, data string) {
	var gameMsg Slot_Message

	defer utils.TimeCost(fmt.Sprintf("slotpanda.onSlotMessage %d,%s", userId, data))()

	e := json.Unmarshal([]byte(data), &gameMsg)
	if e != nil {
		log.Release("onSlotPandaMessage Unmarshal data failed %v", data)
		return
	}

	switch gameMsg.MsgID {
	case Msg_Enter:
		this.slotCommon.OnUserEnter(userId)
		this.sendSlots(userId)
		this.sendWinShapes(userId)
		this.sendBetLevel(userId)
		this.sendJackpot(userId)
		this.sendFreeAdConfig(userId)
		this.lock.Lock()
		_, ok := this.userFreeSpins[userId]
		if !ok {
			this.userFreeSpins[userId] = newFreeSpinInfo(userId, this.freeSlotCount, this.WinShpes, this.Wins, this.slotmgr, this.FreeSlotChanged)
		}
		this.lock.Unlock()
		this.lockRemove.Lock()
		delete(this.removingFreespins, userId)
		this.lockRemove.Unlock()
	case Msg_Exit:
		this.userExit(userId)
	case Msg_Bet:
		this.handleSlotBet(userId, gameMsg.Data)
	case Msg_GetConfig:
		this.sendSlots(userId)
		this.sendWinShapes(userId)
		this.sendFreeAdConfig(userId)
		this.sendJackpot(userId)
		this.sendBetLevel(userId)
	case Msg_UseAd:
		this.onRecvUseAd(userId)
	default:
		log.Release("onSlotPandaMessage unhandle msg %v", data)
	}
}

func (this *GameLogic) onSlotBroadCast(data string) {
	var msg Slot_Message
	msg.MsgID = Msg_Broadcast
	msg.Data = data
	this.writeSlotGameMsg(-1, msg)
}

func (this *GameLogic) sendBetLevel(userId int) {
	this.slotCommon.SendBetLevel(userId)
}

func (this *GameLogic) handleSlotBet(userId int, data string) {
	//defer utils.TimeCost(fmt.Sprintf("slotpanda.handleSlotBet %d,%s", userId, data))()
	var bet SlotPanda_Bet
	e := json.Unmarshal([]byte(data), &bet)
	if e != nil {
		log.Release("handleSlotBet Unmarshal data failed %v", data)
		return
	}

	// 是否有免费次数
	free, freeSpinResult, fromAd := this.useFreeSpin(userId)
	if free {
		this.handleFreeResult(userId, freeSpinResult, fromAd)
		return
	}

	amount := bet.Amount
	if amount <= 0 {
		this.sendSlotBet(userId, "Invalid Bet!", ErrCode_InvalidBet, amount)
		return
	}

	scoreType := GAMEID*100 + 1
	status := 1
	isSuceeded := this.slotSink.WriteMoney(userId, GAMEID, -amount, 0, status, scoreType, GAME_NAME)
	if !isSuceeded {
		log.Release("handleSlotBet WriteMoney failed,UserId[%d],amount[%d]", userId, amount)
		// 扣金币失败
		this.sendSlotBet(userId, "Not enough money!", ErrCode_NotEnoughMoney, amount)
		return
	}
	// 获取结果并写分
	this.handleSlotResult(userId, amount)
}

func (this *GameLogic) handleFreeResult(userId int, result FreeSpinResult, fromAd bool) {
	// 写分
	amount := result.getWinAmount()
	this.slotCommon.WriteResult(userId, result.BetAmount, amount, true, fromAd, result.getDesc(), GAMEID)

	// 发送结果给客户端
	this.sendFreeSlotResult(userId, result)
}

func (this *GameLogic) handleSlotResult(userId int, amount int) {
	//defer utils.TimeCost(fmt.Sprintf("slotpanda.handleSlotResult %d", userId))()
	controlType := this.slotCommon.GetControlType(userId)
	result, ok := this.getResult(userId, amount, controlType)
	if !ok {
		this.sendSlotBet(userId, "User Not Exist", ErrCode_NotExist, amount)
		return
	}
	bonusAmount := 0
	if result.Bonus != nil {
		bonusAmount = result.Bonus.BonusResult + result.Bonus.JackpotResult
	}

	this.slotCommon.WriteResult(userId, amount, result.WinAmount+bonusAmount, false, false, this.getResultDesc(result), GAMEID)

	// 发送结果给客户端
	this.sendSlotResult(userId, result)
}

func (this *GameLogic) sendFreeAdConfig(userId int) {
	var config SlotPanda_FreeAdConfig
	config.AdCount, _ = admanager.GetSlotUserInfo(userId, GAMEID)
	config.TotalAdCount, config.FreeSpinCount = admanager.GetSlotConfig(GAMEID)
	config.Bet = this.getAdBet(config.TotalAdCount - config.AdCount)
	config.AdBets = this.AdBets
	var msg Slot_Message
	msg.MsgID = Msg_FreeAdConfig
	data, _ := json.Marshal(config)
	msg.Data = string(data)
	this.writeSlotGameMsg(userId, msg)
}

func (this *GameLogic) getAdBet(adCount int) int {
	if len(this.AdBets) == 0 {
		return 0
	}

	for i := 0; i < len(this.AdBets); i++ {
		if adCount >= this.AdBets[i].AdCount {
			return this.AdBets[i].BetAmount
		}
	}

	return this.AdBets[len(this.AdBets)-1].BetAmount
}

func (this *GameLogic) onRecvUseAd(userId int) {
	var usedAd SlotPanda_UsedAd
	usedAd.FreeSpinCount, usedAd.AdCount = admanager.UseAd(userId, GAMEID)
	if usedAd.FreeSpinCount > 0 {
		totalAdCount, _ := admanager.GetSlotConfig(GAMEID)
		usedAd.Bet = this.getAdBet(totalAdCount - usedAd.AdCount - 1)
		usedAd.NextAdBetAmount = this.getAdBet(totalAdCount - usedAd.AdCount)
		this.addFreeSpin(userId, usedAd.FreeSpinCount, usedAd.Bet, true)
	}

	var msg Slot_Message
	msg.MsgID = Msg_UseAd
	data, _ := json.Marshal(usedAd)
	msg.Data = string(data)
	this.writeSlotGameMsg(userId, msg)
}

func (this *GameLogic) sendJackpot(userId int) {
	if userId <= 10 {
		return
	}
	var msg Slot_Message
	msg.MsgID = Msg_Jackpot
	data, _ := json.Marshal(SlotPanda_Jackpot{Amount: this.jackpotManager.getAmount()})
	msg.Data = string(data)
	this.writeSlotGameMsg(userId, msg)
}
