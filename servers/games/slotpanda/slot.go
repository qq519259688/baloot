package slotpanda

import (
	"bet24.com/log"
)

type Slot struct {
	SlotID    int    // 编号
	Name      string // 描述
	IsMagic   int    // 万能牌
	IsBonus   int    // 是否bonus
	IsScatter int    // 是否scatter
}

func (s *Slot) isMagic() bool {
	return s.IsMagic > 0
}

func (s *Slot) isBonus() bool {
	return s.IsBonus > 0
}

func (s *Slot) isScatter() bool {
	return s.IsScatter > 0
}

type slotmanager struct {
	Slots []Slot
}

func newSlotManager(slots []Slot) *slotmanager {
	ret := new(slotmanager)
	ret.Slots = slots
	return ret
}

func (sm *slotmanager) getBonusSlotId() int {
	for _, v := range sm.Slots {
		if v.isBonus() {
			return v.SlotID
		}
	}
	return 0
}

func (sm *slotmanager) getWildSlotId() int {
	for _, v := range sm.Slots {
		if v.isMagic() {
			return v.SlotID
		}
	}
	return 0
}

func (sm *slotmanager) getScatterSlotId() int {
	for _, v := range sm.Slots {
		if v.isScatter() {
			return v.SlotID
		}
	}
	return 0
}

func (sm *slotmanager) getSlot(slotID int) Slot {
	for _, v := range sm.Slots {
		if slotID == v.SlotID {
			return v
		}
	}
	log.Release("slotpanda.slotmanager.getSlot failed %d", slotID)
	var slot Slot
	return slot
}
