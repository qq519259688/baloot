package animalslot

import (
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/games/slotcommon/admanager"
)

func (this *GameLogic) writeTribalSlotGameMsg(userId int, gameMsg TribalSlot_Message) {
	data, _ := json.Marshal(gameMsg)
	this.slotSink.SendGameCmd(userId, GAME_MESSAGE, string(data))
}

func (this *GameLogic) sendTribalSlotBet(userId int, errMsg string) {
	var msg TribalSlot_Message
	msg.MsgID = Msg_Bet
	data, _ := json.Marshal(TribalSlot_Bet{ErrorMsg: errMsg})
	msg.Data = string(data)
	this.writeTribalSlotGameMsg(userId, msg)
}

func (this *GameLogic) sendTribalSlotResult(userId int, result TribalSlot_Result) {
	var msg TribalSlot_Message
	msg.MsgID = Msg_Result
	data, _ := json.Marshal(result)
	msg.Data = string(data)
	this.writeTribalSlotGameMsg(userId, msg)
}

func (this *GameLogic) sendTribalSlotJackpot(userId int) {
	var msg TribalSlot_Message
	msg.MsgID = Msg_Jackpot
	data, _ := json.Marshal(TribalSlot_Jackpot{Amount: this.getJackPotAmount()})
	msg.Data = string(data)
	this.writeTribalSlotGameMsg(userId, msg)
}

func (this *GameLogic) sendTribalSlotSlots(userId int) {
	var msg TribalSlot_Message
	msg.MsgID = Msg_Slots
	data, _ := json.Marshal(this.getSlots())
	msg.Data = string(data)
	this.writeTribalSlotGameMsg(userId, msg)
}

func (this *GameLogic) sendTribalSlotWinShapes(userId int) {
	var msg TribalSlot_Message
	msg.MsgID = Msg_WinShapes
	data, _ := json.Marshal(this.getWinShapes())
	msg.Data = string(data)
	this.writeTribalSlotGameMsg(userId, msg)
}

func (this *GameLogic) onTribalSlotMessage(userId int, msg, data string) {
	var gameMsg TribalSlot_Message

	e := json.Unmarshal([]byte(data), &gameMsg)
	if e != nil {
		log.Release("onTribalSlotMessage Unmarshal data failed %v", data)
		return
	}

	switch gameMsg.MsgID {
	case Msg_Enter:
		this.slotCommon.OnUserEnter(userId)
		this.sendTribalSlotSlots(userId)
		this.sendTribalSlotWinShapes(userId)
		this.sendTribalSlotJackpot(userId)
		this.sendFreeAdConfig(userId)
	case Msg_Exit:
		this.userExit(userId)
	case Msg_Bet:
		this.handleTribalSlotBet(userId, gameMsg.Data)
	case Msg_Jackpot:
		this.sendTribalSlotJackpot(userId)
	case Msg_GetConfig:
		this.sendTribalSlotSlots(userId)
		this.sendTribalSlotWinShapes(userId)
		this.sendTribalSlotJackpot(userId)
		this.sendFreeAdConfig(userId)
	case Msg_UseAd:
		this.onRecvUseAd(userId)
	default:
		log.Release("onTribalSlotMessage unhandle msg %v", data)
	}
}

func (this *GameLogic) onTribalSlotBroadCast(data string) {
	var msg TribalSlot_Message
	msg.MsgID = Msg_Broadcast
	msg.Data = data
	this.writeTribalSlotGameMsg(-1, msg)
}

func (this *GameLogic) onTribalSlotJackpot(data string) {
	var msg TribalSlot_Message
	msg.MsgID = Msg_Jackpot
	msg.Data = data
	this.writeTribalSlotGameMsg(-1, msg)
}

func (this *GameLogic) handleTribalSlotBet(userId int, data string) {
	var bet TribalSlot_Bet
	e := json.Unmarshal([]byte(data), &bet)
	if e != nil {
		log.Release("handleTribalSlotBet Unmarshal data failed %v", data)
		return
	}

	// 是否有免费次数
	free, lastBet, fromAd := this.useFreeSpin(userId)
	if free {
		bet.Amount = lastBet
	}

	amount := bet.Amount
	if amount < this.MinBet || amount > this.MaxBet {
		this.sendTribalSlotBet(userId, "Invalid Bet!")
		return
	}
	if free {
		// 获取结果并写分
		this.handleTribalSlotResult(userId, amount, true, fromAd)
	} else {
		scoreType := GAMEID*100 + 1
		status := 1
		isSuceeded := this.slotSink.WriteMoney(userId, GAMEID, -amount, 0, status, scoreType, GAME_NAME)

		if !isSuceeded {
			log.Release("handleTribalSlotBet WriteMoney failed,UserId[%d],amount[%d]", userId, amount)
			// 扣金币失败
			this.sendTribalSlotBet(userId, "Not enough money!")
			return
		}

		// 获取结果并写分
		this.handleTribalSlotResult(userId, amount, false, false)
	}

}

func (this *GameLogic) handleTribalSlotResult(userId int, amount int, isFree bool, fromAd bool) {
	controlType := 0
	if !isFree {
		controlType = this.slotCommon.GetControlType(userId)
	}
	result := this.getResult(userId, amount, isFree, controlType)

	// 如果有特殊玩法
	specialMoney := 0
	specialMultiple := result.Special.WinRate1 + result.Special.WinRate2
	if specialMultiple > 0 {
		specialMoney = specialMultiple * amount
	}

	// 是否中奖池了
	jackpotMoney := result.Jackpot.Amount
	if jackpotMoney > 0 {
		go this.modifyJackpot(-jackpotMoney, userId)
	}

	// 写入jackpot
	if result.WinAmount+specialMoney+jackpotMoney <= 0 {
		var jackPotAmount = this.JackPotInfo.getInJackPotAmount(amount)
		if jackPotAmount > 0 {
			go this.modifyJackpot(jackPotAmount, userId)
		}
	}

	// writemoney
	//this.writeResult(userId, result.WinAmount+specialMoney+jackpotMoney, amount)
	this.slotCommon.WriteResult(userId, amount, result.WinAmount, isFree, fromAd, this.getResultDesc(result), GAMEID)

	// 发送结果给客户端
	this.sendTribalSlotResult(userId, result)

	userInfo := this.slotSink.GetUserInfo(userId)
	if userInfo == nil {
		log.Debug("handleTribalSlotResult userInfo == nil")
		return
	}

	// 如果有jackpot
	// 发送事件
	if jackpotMoney > 0 {
		var broadCast JackPot_Broadcast
		broadCast.UserID = userId
		broadCast.Amount = jackpotMoney
		broadCast.FaceID = userInfo.GetUserFaceId()
		broadCast.FaceUrl = userInfo.GetUserFaceUrl()
		broadCast.NickName = userInfo.GetUserNickName()
		broadCast.Sex = userInfo.GetUserSex()
		broadCast.Vip = userInfo.GetUserVipLevel()
		data, _ := json.Marshal(broadCast)
		//event.DispatchEvent(EVENT_BROADCAST, string(data))
		this.onTribalSlotBroadCast(string(data))
	}

}

func (this *GameLogic) sendFreeAdConfig(userId int) {
	var config FafafaSlot_FreeAdConfig
	config.AdCount, _ = admanager.GetSlotUserInfo(userId, GAMEID)
	config.TotalAdCount, config.FreeSpinCount = admanager.GetSlotConfig(GAMEID)
	config.Bet = this.getAdBet(config.TotalAdCount - config.AdCount)
	config.AdBets = this.AdBets
	var msg TribalSlot_Message
	msg.MsgID = Msg_FreeAdConfig
	data, _ := json.Marshal(config)
	msg.Data = string(data)
	this.writeTribalSlotGameMsg(userId, msg)
}

func (this *GameLogic) getAdBet(adCount int) int {
	if len(this.AdBets) == 0 {
		return 0
	}

	for i := 0; i < len(this.AdBets); i++ {
		if adCount >= this.AdBets[i].AdCount {
			return this.AdBets[i].BetAmount
		}
	}

	return this.AdBets[len(this.AdBets)-1].BetAmount
}

func (this *GameLogic) onRecvUseAd(userId int) {
	var usedAd FafafaSlot_UsedAd
	usedAd.FreeSpinCount, usedAd.AdCount = admanager.UseAd(userId, GAMEID)
	if usedAd.FreeSpinCount > 0 {
		totalAdCount, _ := admanager.GetSlotConfig(GAMEID)
		usedAd.Bet = this.getAdBet(totalAdCount - usedAd.AdCount - 1)
		usedAd.NextAdBetAmount = this.getAdBet(totalAdCount - usedAd.AdCount)
		this.addFreeSpin(userId, usedAd.FreeSpinCount, usedAd.Bet, true)
	}

	var msg TribalSlot_Message
	msg.MsgID = Msg_UseAd
	data, _ := json.Marshal(usedAd)
	msg.Data = string(data)
	this.writeTribalSlotGameMsg(userId, msg)
}
