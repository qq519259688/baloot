package animalslot

const (
	COLUMN_COUNT = 5
	ROW_COUNT    = 3
	RESULT_COUNT = 15
	GAMEID       = 50
	GAME_NAME    = "animalslot"
	GAME_MESSAGE = "animalslot"
)

type Slot struct {
	SlotID  int    // 编号
	Name    string // 描述
	IsMagic int    // 万能牌
}

func (s *Slot) isMagic() bool {
	return s.IsMagic > 0
}

type SlotCount struct {
	SlotID int
	Count  int
}

// 中奖连线信息
type Result struct {
	SlotID     int // 格子ID
	SlotCount  int // 数量
	WinShapeID int // 连线ID
	WinRate    int // 赢的倍数
}

type SlotCount_Rate struct {
	P int   // 概率
	S []int // 数量
}

// 消息定义
const (
	Msg_Bet          = 1 // 下注
	Msg_Result       = 2 // 发送结果
	Msg_Jackpot      = 3 // 获取彩池额度
	Msg_Slots        = 4 // 获取目前使用的Slot类型
	Msg_WinShapes    = 5
	Msg_GetConfig    = 6  // 获取配置信息
	Msg_Broadcast    = 7  // 奖池广播
	Msg_Enter        = 8  // 进入游戏
	Msg_Exit         = 9  // 离开游戏
	Msg_FreeAdConfig = 10 // 广告配置及自己的剩余
	Msg_UseAd        = 11 // 看广告后请求免费次数
)

type TribalSlot_Message struct {
	MsgID int
	Data  string
}

type TribalSlot_Bet struct {
	Amount   int
	ErrorMsg string
}

type SpecialResult struct {
	SlotID    int
	SlotCount int
	WinRate1  int
	WinRate2  int
}

type JackpotResult struct {
	WinShapeID int // 连线ID
	Amount     int // 中奖金额
}

type TribalSlot_Result struct {
	BetAmount int           // 下注金额
	WinAmount int           // 共赢金额
	Slots     []int         // 15个格子ID
	Lines     []Result      // 中奖连线
	Special   SpecialResult // 特殊奖励，如果没有，则SlotCount == 0
	Jackpot   JackpotResult // 是否中了奖池
	FreeSpin  int           // 本次结果获取的免费次数
}

type TribalSlot_Jackpot struct {
	Amount int
}

type JackPot_Broadcast struct {
	UserID          int
	Amount          int
	FaceID          int
	FaceChangeIndex int
	FaceUrl         string
	NickName        string
	Vip             int
	Sex             int
}

type TestSlots struct {
	Slots []int
}

type FafafaSlot_FreeAdBet struct {
	AdCount   int
	BetAmount int
}

type FafafaSlot_FreeAdConfig struct {
	TotalAdCount  int // 每日免费次数
	AdCount       int // 剩余次数
	FreeSpinCount int // 看广告后获取的免费次数
	Bet           int // 本次看视频获得的底注
	AdBets        []FafafaSlot_FreeAdBet
}

// 下行数据，上行无数据
type FafafaSlot_UsedAd struct {
	FreeSpinCount   int // 获取免费次数，如果为0 表示没有免费
	Bet             int // 免费次数的底注
	AdCount         int // 剩余次数
	NextAdBetAmount int // 下次广告的底注
}
