package animalslot

import (
	"bet24.com/log"
	"bet24.com/servers/games/slotcommon"
)

const LogColor = log.Blue

var logic *GameLogic

func Run(slotSink slotcommon.SlotSink) {
	logic = NewGameLogic(slotSink)
	logic.run()
}

func OnGameMessage(userId int, msg, data string) {
	logic.onTribalSlotMessage(userId, msg, data)
}

func OnUserExit(userId int) {
	logic.userExit(userId)
}
