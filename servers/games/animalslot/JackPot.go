package animalslot

// JackPot 定义
type JackPot_Bonus struct {
	MinBet    int // 下限
	MaxBet    int // 上限
	BonusRate int // 奖金比例，万分比
}

type JackPot struct {
	BetRate int // 投注率万分比
	Bonus   []JackPot_Bonus
}

func (j *JackPot) getInJackPotAmount(betAmount int) int {
	return betAmount * j.BetRate / 10000
}

func (j *JackPot) getBonus(betAmount int, jackpotAmount int) int {
	for _, v := range j.Bonus {
		if betAmount < v.MaxBet && betAmount >= v.MinBet {
			return int(jackpotAmount) * v.BonusRate / 10000
		}
	}

	return 0
}
