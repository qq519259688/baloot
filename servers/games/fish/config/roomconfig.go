package config

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"

	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
	"bet24.com/utils"
)

const config_key = "fish_rooms"
const config_key_chip = "fish_chip_rooms"
const config_key_private = "fish_private_rooms"

type OpenHistory struct {
	SerialNumber int
	SpinResult   int
}

type RoomInfoBase struct {
	RoomName   string
	RoomDesc   string
	RoomID     int
	RoomMin    int
	RoomMax    int
	BulletMin  int // 炮台限制，0表示不限制
	BulletMax  int
	Demo       int // 非零表示练习场
	ServerIP   string
	ServerPort int
}

type RoomInfo struct {
	RoomInfoBase
	SpawnCfg        []string
	Group           []string
	OddsExtra       int // 附加难度，大于0表示容易，小于0表示难，万分比
	RoomSecond      int // 每局时间
	NewbieExtraList []NewbieExtra
}

type NewbieExtra struct {
	MaxBullet int
	Extra     int
}

func (this *RoomInfo) GetSpawn() string {
	if len(this.SpawnCfg) > 1 {
		return this.SpawnCfg[rand.Intn(len(this.SpawnCfg))]
	}
	return this.SpawnCfg[0]
}

func (this *RoomInfo) GetGroup() string {
	if len(this.Group) > 1 {
		return this.Group[rand.Intn(len(this.Group))]
	}
	return this.Group[0]
}

func GetSpawn(roomName string) string {
	for _, v := range Rooms.Rooms {
		if v.RoomName == roomName {
			return v.GetSpawn()
		}
	}
	return ""
}

func GetGroup(roomName string) string {
	for _, v := range Rooms.Rooms {
		if v.RoomName == roomName {
			return v.GetGroup()
		}
	}
	return ""
}

func GetNewbieExtraList(roomName string) []NewbieExtra {
	for _, v := range Rooms.Rooms {
		if v.RoomName == roomName {
			return v.NewbieExtraList
		}
	}
	return nil
}

var Rooms struct {
	Rooms []RoomInfo
}

//var Room *RoomInfo

func getConfigKey() string {
	// 私人场优先
	if Server.IsPrivateRoom {
		return config_key_private
	}
	if Server.IsChipRoom {
		return config_key_chip
	}
	return config_key
}

func loadRedisConfig(createRoom bool) bool {
	data := platformconfig.GetConfig(getConfigKey())
	if data == "" {
		log.Release("    config msg is null")
		return false
	}

	ok, _ := marshalData([]byte(data))
	return ok
}

func loadRoomConfig() {
	if loadRedisConfig(true) {
		log.Release("using remote config ignored rooms.json !!!")
		return
	}
	// 保存
	// 加载房间表
	data, err := os.ReadFile("fish/rooms.json")
	//fmt.Println(string(data))
	if err != nil {
		log.Release("read rooms failed fish/rooms.json")
		return
	}
	if ok, newConfig := marshalData(data); ok {
		// 写入redis
		platformconfig.SetConfig(getConfigKey(), newConfig)
	}
}

func marshalData(data []byte) (bool, string) {
	changedConfigString := string(data)
	err := json.Unmarshal(data, &Rooms)
	if err != nil {
		log.Release("Unmarshal rooms failed err:%v", err)
		return false, changedConfigString
	}
	if len(Rooms.Rooms) <= 0 {
		log.Release("Rooms.Rooms == 0")
		return false, changedConfigString
	}

	//Room = &Rooms.Rooms[0]

	//originPort := Room.ServerPort
	for i := 0; i < 100; i++ {
		if utils.CheckPortInUse(Server.ServerPort) {
			Server.ServerPort++
			continue
		}
		break
	}

	if len(Server.ServerIP) > 0 {
		ws := "ws"
		if len(Server.CertFile) > 0 {
			ws = "wss"
		}
		//Room.ServerIP = fmt.Sprintf("%s://%s:%d", ws, Server.ServerIP, Room.ServerPort)
		for i := 0; i < len(Rooms.Rooms); i++ {
			Rooms.Rooms[i].ServerIP = fmt.Sprintf("%s://%s:%d", ws, Server.ServerIP, Server.ServerPort)
		}
	}

	return true, changedConfigString
}

func OnConfigChanged(key string) {
	if key != getConfigKey() {
		return
	}
	log.Release("正在刷新配置信息")
	loadRedisConfig(false)
}
