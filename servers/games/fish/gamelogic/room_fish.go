package gamelogic

import (
	"bet24.com/log"
	"bet24.com/servers/games/fish/config"
	"bet24.com/servers/games/fish/fish"
	"sync"
)

type fish_manager struct {
	fishkey       int
	fish_list     []*fish.Fish
	lock          *sync.RWMutex
	spawn_manager *spawn_fish_manager
	roomName      string
}

func newFishManager(roomName string) *fish_manager {
	this := new(fish_manager)
	this.fishkey = 0
	this.lock = &sync.RWMutex{}
	this.roomName = roomName
	//this.spawn_manager = newSpwanFishManager(false)
	return this
}

func (this *fish_manager) getFish(fishKey int) *fish.Fish {
	this.lock.RLock()
	defer this.lock.RUnlock()
	for _, v := range this.fish_list {
		if v.FishKey == fishKey {
			return v
		}
	}
	return nil
}

func (this *fish_manager) generateFish(isGroupState bool) (ret []*fish.Fish) {
	this.lock.Lock()
	defer this.lock.Unlock()
	// 清理死鱼
	for i := 0; i < len(this.fish_list); {
		if this.fish_list[i].IsDead() {
			//log.Debug("fish %d is dead", this.fish_list[i].FishKey)
			this.fish_list = append(this.fish_list[:i], this.fish_list[i+1:]...)
		} else {
			i++
		}

	}
	if isGroupState {
		return
	}

	spawns := this.spawn_manager.checkSpawn()
	for _, v := range spawns {
		//log.Debug("fish_manager.generateFish %v", v)
		fish := fish.GenerateFish(this.fishkey, v.FishID, v.Speed, v.RouteID, 0)
		this.fishkey++
		this.fish_list = append(this.fish_list, fish)
		ret = append(ret, fish)
	}

	return
}

func (this *fish_manager) addFish(fishID, delaySecs int) {
	this.lock.Lock()
	defer this.lock.Unlock()
	fish := fish.GenerateFish(this.fishkey, fishID, 0, 0, delaySecs)
	this.fishkey++
	this.fish_list = append(this.fish_list, fish)
}

func (this *fish_manager) removeFish(fishKey int) bool {
	this.lock.Lock()
	defer this.lock.Unlock()
	for k, v := range this.fish_list {
		if v.FishKey == fishKey {
			this.fish_list = append(this.fish_list[:k], this.fish_list[k+1:]...)
			return true
		}
	}
	log.Release("removeFish fishKey(%d) not exist", fishKey)
	return false
}

func (this *fish_manager) getFishCount() int {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return len(this.fish_list)
}

func (this *fish_manager) dump() {
	this.lock.RLock()
	defer this.lock.RUnlock()
	log.Debug("fish_manager.dump total count = %d", len(this.fish_list))
	for _, v := range this.fish_list {
		v.Dump()
	}
}

func (this *fish_manager) clear(byCreate bool) {
	this.lock.Lock()
	defer this.lock.Unlock()
	this.fish_list = []*fish.Fish{}
	this.spawn_manager = newSpwanFishManager(byCreate, config.GetSpawn(this.roomName))
}

func (this *fish_manager) addExtraLife(seconds int) {
	this.lock.Lock()
	defer this.lock.Unlock()
	for _, v := range this.fish_list {
		v.AddExtraLife(seconds)
	}
}

func (this *fish_manager) getBossAfter() int {
	return this.spawn_manager.getBossAfter()
}

func (this *fish_manager) pauseForFrozen(seconds int64) {
	this.spawn_manager.pauseForFrozen(seconds)
}

func (this *fish_manager) getRandomFish() int {
	return this.spawn_manager.getRandomFish()
}
