package gamelogic

import (
	"bet24.com/log"
	"bet24.com/servers/games/fish/fish"
	"bet24.com/servers/games/fish/fishcommon"
	"bet24.com/servers/insecureframe/frame"
	"encoding/json"
)

// 玩家纪录
type RecordInfo struct {
	FishID   int // 鱼ID
	FishOdds int // 鱼的倍率
	Catched  int // 捕获数量
	Consume  int // 消耗金币
	Return   int // 返还金币
}

type UserBetInfo struct {
	ServerName   string `json:"SN"`  // 服务器名
	RoomID       int    `json:"RID"` // 桌子号
	BatchID      int    `json:"BID"` // 序列号
	TotalConsume int    `json:"TC"`  // 总消耗
	TotalReturn  int    `json:"TR"`  // 总返还
	From         int    `json:"F"`   // 开始时间
	To           int    `json:"T"`   // 结束时间
}

type UserRecord struct {
	UserBetInfo
	UserId  int // 用户ID
	Records map[int]*RecordInfo
}

func (this *UserRecord) addRecord(fishID int, consume int, ret int) {
	fi := fish.GetFishInfo(fishID)
	if fi == nil {
		log.Release("UserRecord.addRecord invalid fishID %d", fishID)
		return
	}
	// 是否已有
	record, ok := this.Records[fishID]
	if !ok {
		record = new(RecordInfo)
		record.FishID = fishID
		record.FishOdds = fi.Odds
		this.Records[fishID] = record
	}
	if ret > 0 {
		record.Catched++
	}
	record.Consume += consume
	record.Return += ret
	//log.Debug("UserRecord.addRecord[%d],fish[%d],consume[%d]ret[%d]", this.UserId, fishID, consume, ret)
}

func (this *UserRecord) flush(table frame.Table) {
	this.To = fishcommon.GetTime()
	this.TotalConsume = 0
	this.TotalReturn = 0
	var detail [][]int
	for _, v := range this.Records {
		this.TotalReturn += v.Return
		this.TotalConsume += v.Consume
		r := []int{v.FishID, v.FishOdds, v.Catched, v.Consume, v.Return}
		detail = append(detail, r)
	}
	bd, _ := json.Marshal(this.UserBetInfo)
	rd, _ := json.Marshal(detail)

	log.Debug("UserRecord.flush %d Consume[%d]Return[%d]", this.UserId, this.TotalConsume, this.TotalReturn)
	go table.WriteBetRecord(this.UserId, this.TotalConsume, this.TotalReturn, 2.0, string(bd),
		string(rd), this.ServerName)

	/*
		writeBetRecord := transaction.NewTransWriteBetRecord()
		writeBetRecord.IN.UserID = this.UserId
		writeBetRecord.IN.GameID = fishcommon.GAMEID
		writeBetRecord.IN.BetAmount = this.TotalConsume
		writeBetRecord.IN.WinAmount = this.TotalReturn
		writeBetRecord.IN.WinRate = 2.0
		writeBetRecord.IN.RoomName = this.ServerName

		writeBetRecord.IN.BetDesc = string(bd)
		rd, _ := json.Marshal(detail)
		writeBetRecord.IN.ResultDesc = string(rd)
		go writeBetRecord.DoAction(nil)
	*/
}
