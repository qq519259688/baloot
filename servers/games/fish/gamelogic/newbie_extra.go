package gamelogic

import (
	"bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/games/fish/config"
	"fmt"
	"sync"
)

type newbie_extra struct {
	extralist []config.NewbieExtra
	userlist  map[int]int
	lock      *sync.RWMutex
}

//var ne *newbie_extra

/*func getNewbieExtra() *newbie_extra {
	if ne == nil {
		ne = newNewbieExtra()
	}
	return ne
}*/

func newNewbieExtra(roomName string) *newbie_extra {
	ne := new(newbie_extra)
	ne.userlist = make(map[int]int)
	ne.lock = &sync.RWMutex{}
	ne.getExtraList(roomName)
	return ne
}

func (ne *newbie_extra) getExtraList(roomName string) {
	ne.extralist = config.GetNewbieExtraList(roomName)
}

func (ne *newbie_extra) getUserKey(userId int) string {
	return fmt.Sprintf("fish:newbie:%d", userId)
}

func (ne *newbie_extra) onUserEnter(userId int) {
	if len(ne.extralist) == 0 || userId == 0 {
		return
	}
	ne.lock.RLock()
	_, ok := ne.userlist[userId]
	ne.lock.RUnlock()
	if ok {
		log.Debug("newbie_extra.onUserEnter %d already exist", userId)
		return
	}
	ne.lock.Lock()
	ne.userlist[userId] = redis.String_GetInt(ne.getUserKey(userId))
	ne.lock.Unlock()
}

func (ne *newbie_extra) onUserExit(userId int) {
	if len(ne.extralist) == 0 || userId == 0 {
		return
	}
	ne.lock.RLock()
	bulletCount, ok := ne.userlist[userId]
	ne.lock.RUnlock()
	if !ok {
		log.Debug("newbie_extra.onUserExit %d not exist", userId)
		return
	}
	redis.String_Set(ne.getUserKey(userId), fmt.Sprintf("%d", bulletCount))
}

func (ne *newbie_extra) getExtra(userId int) int {
	if len(ne.extralist) == 0 || userId == 0 {
		return 0
	}
	ne.lock.Lock()
	ne.userlist[userId]++
	bulletCount, _ := ne.userlist[userId]
	ne.lock.Unlock()

	for _, v := range ne.extralist {
		if bulletCount <= v.MaxBullet {
			return v.Extra
		}
	}
	return 0
}
