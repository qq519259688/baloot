package gamelogic

import (
	"bet24.com/log"
	"bet24.com/servers/games/fish/bullet"
	"bet24.com/servers/games/fish/fishcommon"
	"sync"
)

type userBullet struct {
	bullet.Bullet
	odds int
}

type bullet_manager struct {
	bullet_list []*userBullet
	bulleyKey   []int
	lock        *sync.RWMutex
}

func newBulletManager() *bullet_manager {
	this := new(bullet_manager)
	this.bulleyKey = make([]int, fishcommon.SEAT_COUNT)
	for i := 1; i < fishcommon.SEAT_COUNT; i++ {
		this.bulleyKey[i] = i * 500000000
	}
	this.lock = &sync.RWMutex{}
	return this
}

func (this *bullet_manager) addBullet(userID int, bulletID int, angle int, fishkey int, chairID int, odds int) *bullet.Bullet {
	this.lock.Lock()
	defer this.lock.Unlock()
	this.bulleyKey[chairID]++
	b := bullet.Bullet{BulletID: bulletID, BulletKey: this.bulleyKey[chairID], Angle: angle, UserID: userID, FishKey: fishkey}
	this.bullet_list = append(this.bullet_list, &userBullet{Bullet: b, odds: odds})
	return &b
}

func (this *bullet_manager) getFlyingBullet(userId int) int {
	ret := 0
	this.lock.Lock()
	defer this.lock.Unlock()
	for _, v := range this.bullet_list {
		if v.UserID == userId {
			ret += v.odds
		}
	}
	return ret
}

func (this *bullet_manager) getBullet(bulletKey int) *userBullet {
	this.lock.RLock()
	defer this.lock.RUnlock()
	for _, v := range this.bullet_list {
		if v.BulletKey == bulletKey {
			return v
		}
	}
	return nil
}

func (this *bullet_manager) removeBullet(bulletKey int) bool {
	this.lock.Lock()
	defer this.lock.Unlock()
	for k, v := range this.bullet_list {
		if v.BulletKey == bulletKey {
			this.bullet_list = append(this.bullet_list[:k], this.bullet_list[k+1:]...)
			return true
		}
	}
	log.Release("removeBullet bulletKey(%d) not exist", bulletKey)
	return false
}

func (this *bullet_manager) removeUserBullet(userID int) int {
	this.lock.Lock()
	defer this.lock.Unlock()
	ret := 0
	for i := 0; i < len(this.bullet_list); {
		if this.bullet_list[i].UserID == userID {
			this.bullet_list = append(this.bullet_list[:i], this.bullet_list[i+1:]...)
			ret++
		} else {
			i++
		}
	}
	return ret
}
