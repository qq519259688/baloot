package gamelogic

import (
	"sync"
	"time"

	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
	userservice "bet24.com/servers/micros/userservices/proto"
)

const IDLE_SECONDS = 300

type special struct {
	fishkey       int
	bulletOdds    int
	specialEffect int
	specialType   int
	specialPower  int
	specialLimit  int
	earn          int
}

func (s *special) used(earn int) {
	s.specialLimit--
	s.earn += earn
}

type fishuser struct {
	userId int
	// 临时成绩
	consume int // 成绩缓存
	earn    int

	// 我的概率
	extra_odds          int
	isIndividualControl bool // 是否走个人奖池
	waterValue          int  // 水池值
	totalBetAmount      int
	chan_score          chan int
	delayWriteScore     bool
	cannonPower         int
	BulletId            int
	BulletMax           int
	CannonId            int
	Essence             int

	bulletMin int
	bulletMax int

	specials []*special

	lastAction int64

	newbieExtra *newbie_extra

	scoreLock   *sync.RWMutex
	controlLock *sync.RWMutex
}

func newFishUser(userId int, bulletMin, bulletMax int, newbieExtra *newbie_extra) *fishuser {
	u := new(fishuser)
	u.chan_score = make(chan int)
	u.delayWriteScore = true
	u.userId = userId
	u.bulletMin = bulletMin
	u.bulletMax = bulletMax
	u.BulletMax = bulletMax
	u.setBullet(u.bulletMin)
	u.isIndividualControl = false
	u.extra_odds = 0
	u.waterValue = 0
	u.totalBetAmount = 0
	u.lastAction = time.Now().Unix()
	u.newbieExtra = newbieExtra
	u.scoreLock = &sync.RWMutex{}
	u.controlLock = &sync.RWMutex{}
	decos := userservice.GetUserDecoration(userId)
	for _, v := range decos {
		if v.Type == item.Item_Decoration_FishCannon {
			u.setCannon(v.ItemId, false)
		}
	}
	return u
}

func (fu *fishuser) isIdled() bool {
	return time.Now().Unix()-fu.lastAction > IDLE_SECONDS
}

func (fu *fishuser) doAction() {
	fu.lastAction = time.Now().Unix()
}

func (fu *fishuser) getUserOdds() int {
	return fu.getUserExtraOdds() + fu.cannonPower + fu.newbieExtra.getExtra(fu.userId)
}

func (fu *fishuser) addScore(amount int) {
	fu.doAction()
	fu.scoreLock.Lock()
	defer fu.scoreLock.Unlock()
	if amount > 0 {
		fu.earn += amount
	} else {
		fu.consume += amount
	}
}

func (fu *fishuser) getConsumeAndEarnAndClear() (int, int) {
	fu.scoreLock.Lock()
	defer fu.scoreLock.Unlock()
	c, e := fu.consume, fu.earn
	fu.consume = 0
	fu.earn = 0
	return c, e
}

func (fu *fishuser) setUserExtraOdd(odds int, isControl bool) {
	if fu == nil {
		return
	}
	fu.controlLock.Lock()
	defer fu.controlLock.Unlock()
	fu.extra_odds = odds
	fu.isIndividualControl = isControl
}

func (fu *fishuser) getUserExtraOdds() int {
	fu.controlLock.RLock()
	defer fu.controlLock.RUnlock()
	return fu.extra_odds
}

func (fu *fishuser) isInControl() bool {
	fu.controlLock.RLock()
	defer fu.controlLock.RUnlock()
	return fu.isIndividualControl
}

func (fu *fishuser) setWaterPoolValue(waterValue, betAmount int) {
	fu.waterValue += waterValue
	fu.totalBetAmount += betAmount
}

func (fu *fishuser) getWaterPoolValueAndClear() (int, int) {
	v := fu.waterValue
	b := fu.totalBetAmount
	fu.waterValue = 0
	fu.totalBetAmount = 0
	return v, b
}

func (fu *fishuser) setBullet(bulletID int) (isChanged bool) {
	isChanged = false
	// 检查一下限制
	if fu.bulletMin > 0 && bulletID < fu.bulletMin {
		log.Debug("fishuser.setBullet bulletID[%d] < fu.bulletMin[%d]", bulletID, fu.bulletMin)
		return
	}
	if fu.bulletMax > 0 && bulletID > fu.bulletMax {
		log.Debug("fishuser.setBullet bulletID[%d] > fu.bulletMax[%d]", bulletID, fu.bulletMax)
		return
	}
	if bulletID > fu.BulletMax {
		log.Debug("fishuser.setBullet bulletID[%d] > fu.BulletMax[%d]", bulletID, fu.BulletMax)
		return
	}

	isChanged = fu.BulletId != bulletID
	fu.BulletId = bulletID
	fu.doAction()
	return
}

func (fu *fishuser) getBulletId() int {
	return fu.BulletId
}

func (fu *fishuser) getMaxBulletId() int {
	return fu.BulletMax
}

func (fu *fishuser) setMaxBulletId(value int) {
	fu.BulletMax = value
	fu.setBullet(value)
}

func (fu *fishuser) setCannon(cannon int, check bool) (isChanged bool) {
	fu.doAction()
	isChanged = false
	if check && !userservice.SetUserDecoration(fu.userId, item.Item_Decoration_FishCannon, cannon) {
		return
	}
	isChanged = true
	itm := item.GetItem(cannon)
	if itm != nil {
		fu.cannonPower = itm.ExtraPower
	}
	fu.CannonId = cannon
	return
}

func (fu *fishuser) addSpecial(fishkey, bulletOdds, specialEffect, specialType, specialPower, specialLimit int) {
	log.Debug("fishuser addSpecial %d,%d,%d", fishkey, bulletOdds, specialEffect)
	fu.specials = append(fu.specials, &special{fishkey, bulletOdds, specialEffect, specialType, specialPower, specialLimit, 0})
}

func (fu *fishuser) clearSpecial() {
	fu.specials = nil
}

func (fu *fishuser) removeSpecial(fishkey int) int {
	for i := 0; i < len(fu.specials); {
		if fu.specials[i].fishkey == fishkey {
			earn := fu.specials[i].earn
			fu.specials = append(fu.specials[:i], fu.specials[i+1:]...)
			return earn
		} else {
			i++
		}
	}
	return 0
}

func (fu *fishuser) useSpecial(fishkey int) (int, int) {
	if len(fu.specials) == 0 {
		return 0, 0
	}
	for _, v := range fu.specials {
		if v.fishkey != fishkey {
			continue
		}
		if v.specialLimit > 0 {
			return v.bulletOdds, v.specialPower
		}
	}
	fu.doAction()
	return 0, 0
}

func (fu *fishuser) specialUsed(fishkey int, earn int) {
	fu.doAction()
	for _, v := range fu.specials {
		if v.fishkey == fishkey {
			v.used(earn)
			break
		}
	}
}
