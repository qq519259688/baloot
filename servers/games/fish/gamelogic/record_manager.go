package gamelogic

import (
	"bet24.com/log"
	"bet24.com/servers/games/fish/fishcommon"
	"bet24.com/servers/insecureframe/frame"
	"sync"
)

type RecordManager struct {
	ServerName  string // 服务器名
	RoomID      int    // 桌子号
	BatchID     int    // 序列号
	From        int
	UserRecords map[int]*UserRecord
	lock        *sync.RWMutex
	table       frame.Table
}

func (this *RecordManager) newUserRecord(userID int) *UserRecord {
	this.lock.RLock()
	ur, ok := this.UserRecords[userID]
	this.lock.RUnlock()
	if ok {
		return ur
	}
	ur = new(UserRecord)
	ur.UserId = userID
	ur.ServerName = this.ServerName
	ur.RoomID = this.RoomID
	ur.BatchID = this.BatchID
	ur.From = this.From
	ur.Records = make(map[int]*RecordInfo)
	this.lock.Lock()
	this.UserRecords[userID] = ur
	this.lock.Unlock()
	return ur
}

func (this *RecordManager) getUserRecord(userID int) *UserRecord {
	return this.newUserRecord(userID)
}

func (this *RecordManager) addUserRecord(userID int, fishID int, consume int, ret int) {
	ur := this.getUserRecord(userID)
	ur.addRecord(fishID, consume, ret)
}

// 写入数据库
func (this *RecordManager) flush() {
	this.lock.RLock()
	defer this.lock.RUnlock()

	if len(this.UserRecords) == 0 {
		log.Release("RecordManager.flush no record")
		return
	}
	log.Release("RecordManager.flush")
	for _, v := range this.UserRecords {
		v.flush(this.table)
	}
}

func newRecordManager(ServerName string, RoomID int, table frame.Table) *RecordManager {
	ret := new(RecordManager)
	ret.ServerName = ServerName
	ret.RoomID = RoomID
	ret.BatchID = fishcommon.GetBatchID()
	ret.From = fishcommon.GetTime()
	ret.UserRecords = make(map[int]*UserRecord)
	ret.lock = &sync.RWMutex{}
	ret.table = table
	return ret
}

func (this *RecordManager) dumpRecord() {
	this.lock.RLock()
	defer this.lock.RUnlock()
	log.Release("RecordManager.dumpRecord")
	for _, v := range this.UserRecords {
		log.Release("  User[%d]", v.UserId)
		log.Release("    %v", v.Records)
	}
}
