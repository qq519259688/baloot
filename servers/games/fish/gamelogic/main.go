package gamelogic

import (
	"bet24.com/servers/games/fish/bullet"
	"bet24.com/servers/games/fish/config"
	"bet24.com/servers/games/fish/fish"
	"bet24.com/servers/insecureframe/frame"
)

var Stopping bool

func Run() {
	if test() {
		return
	}
	Stopping = false
	frame.RunFrame(newGameSink(), config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB)
	fish.Run()
	bullet.Run()
}
func test() bool {
	return false
}
