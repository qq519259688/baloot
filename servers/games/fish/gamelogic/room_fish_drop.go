package gamelogic

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"

	"bet24.com/log"
)

type DropInfo struct {
	FishId int
	ItemId int
	Count  int
	Chance int
}

type room_fish_drop struct {
	dropInfoList map[int]*DropInfo
}

func newRoomFishDrop(roomName string) *room_fish_drop {
	ret := new(room_fish_drop)
	ret.loadConfig(roomName)
	return ret
}

func (r *room_fish_drop) loadConfig(roomName string) {
	r.dropInfoList = make(map[int]*DropInfo)

	fileName := fmt.Sprintf("fish/drop_%v.json", roomName)
	data, err := os.ReadFile(fileName)
	if err != nil {
		log.Release("read drop config failed %v", fileName)
		return
	}
	var dl []DropInfo
	err = json.Unmarshal(data, &dl)
	if err != nil {
		log.Release("Unmarshal drop config failed err:%v", err)
	}

	for i := 0; i < len(dl); i++ {
		r.dropInfoList[dl[i].FishId] = &dl[i]
	}

	log.Debug("drop config initData ok %v", r.dropInfoList)
}

func (r *room_fish_drop) getDropItem(fishId int) (int, int) {
	drop, ok := r.dropInfoList[fishId]
	if !ok {
		return 0, 0
	}

	if rand.Intn(10000) < drop.Chance {
		return drop.ItemId, drop.Count
	}

	return 0, 0
}
