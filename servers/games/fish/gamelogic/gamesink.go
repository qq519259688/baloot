package gamelogic

import (
	"math/rand"

	"bet24.com/servers/games/fish/config"
	common "bet24.com/servers/games/fish/fishcommon"
	"bet24.com/servers/insecureframe/frame"
)

var gs *gamesink

func newGameSink() *gamesink {
	gs = new(gamesink)
	return gs
}

type gamesink struct {
}

func (gs *gamesink) GetServerPort() int {
	return config.Server.ServerPort
}
func (gs *gamesink) GetGameID() int {
	return common.GAMEID
}
func (gs *gamesink) GetGameName() string {
	return common.GAME_NAME
}

func (gs *gamesink) GetRoomName() string {
	return config.Server.ServerName
}
func (gs *gamesink) CreateTableSink(table frame.Table, data string) frame.TableSink {
	return newTableSink(table, data)
}
func (gs *gamesink) GetOfflineSeconds() int64 {
	return 0
}

func (gs *gamesink) GetCertFile() string {
	return config.Server.CertFile
}
func (gs *gamesink) GetKeyFile() string {
	return config.Server.KeyFile
}
func (gs *gamesink) GetServerAddr() string {
	return config.Rooms.Rooms[0].ServerIP
}

func (gs *gamesink) GetRobotCount() int {
	return 0
}

func (gs *gamesink) GetRobotGoldLimit() (min, max int) {
	return
}

func (gs *gamesink) GetRobotOnlineSec() int {
	return 0
}

func (gs *gamesink) GetRoomDatas() []string {
	var ret []string
	for _, v := range config.Rooms.Rooms {
		ret = append(ret, v.RoomName)
	}
	return ret
}

func (gs *gamesink) GetMinRoomCount(roomData string) int {
	return 0
}

func (gs *gamesink) GetRoomRobotCount(roomName string) int {
	return 0
}

func (gs *gamesink) GetRoomRobotGoldLimit(roomName string) (min, max int) {
	return 0, 0
}

func (gs *gamesink) IsChipRoom() bool {
	return config.Server.IsChipRoom
}

func (gs *gamesink) OnPlatformConfig(key string) {
	config.OnConfigChanged(key)
}

func (gs *gamesink) IsPrivateRoom() bool {
	return config.Server.IsPrivateRoom
}

func (gs *gamesink) GetChairCount() int {
	return common.SEAT_COUNT
}

func (gs *gamesink) IsTableCloseByLogic() bool {
	return true
}

func (gs *gamesink) IgnoreSameIP() bool {
	return true
}

func (gs *gamesink) EnterPlayingRoomFirst() bool {
	return true
}

func (gs *gamesink) getRandom(min, max int) int {
	if min >= max {
		return min
	}
	return min + rand.Intn(max-min)
}

func (gs gamesink) isBetween(p, min, max int) bool {
	return p >= min && p <= max
}
func (gs *gamesink) GetRoomLevel(data string) int {
	return 0
}
