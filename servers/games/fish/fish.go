package main

import (
	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/games/fish/config"
	common "bet24.com/servers/games/fish/fishcommon"
	"bet24.com/servers/games/fish/gamelogic"
	"bet24.com/servers/insecureframe/frame"
	"bet24.com/servers/insecureframe/gate"
	"bet24.com/servers/transaction"
	"bet24.com/utils"
	"encoding/json"
	"fmt"
	"math/rand"
	"time"
)

func waitInput() {
	for {
		var cmd string
		var param1 string
		var param2 string
		fmt.Scanf("%s %s %s", &cmd, &param1, &param2)
		switch cmd {
		case "exit":
			gamelogic.Stopping = true
			frame.StopServer()
			go transaction.DoGameRoomPing(common.GAMEID, 1, config.RoomConfigName)
		case "listuser":
			gate.DumpUsers()
		default:
			if !frame.Dump(cmd, param1, param2) {
				log.Release("unknown command")
			}
		}
	}
}

func main() {
	defer waitInput()
	rand.Seed(time.Now().UnixNano())
	coreservice.SetServiceAddr(config.Server.ServiceAddr)
	config.Run()
	utils.SetErrorFile("log/fish/err.log", "fish starting")
	//monitor.Run(config.Server.MonitorPort)
	gamelogic.Run()
	startRoomPing()
}
func startRoomPing() {
	if gamelogic.Stopping {
		return
	}
	time.AfterFunc(10*time.Second, startRoomPing)
	for i := 0; i < len(config.Rooms.Rooms); i++ {
		d, _ := json.Marshal(config.Rooms.Rooms[i].RoomInfoBase)
		go transaction.DoGameRoomPing(common.GAMEID, 2, config.Rooms.Rooms[i].RoomName)
		frame.UpdateRoomList(config.Rooms.Rooms[i].RoomName, config.Rooms.Rooms[i].RoomDesc,
			string(d), i)
	}
}
