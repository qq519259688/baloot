package fish

import (
	"bet24.com/log"
	"time"
)

var fishcfg *FishCfg
var routemgr *RouteManager

const (
	Fish_Alive = 0
	Fish_Dead  = 1
	Fish_Life  = 120000 // 暂定鱼存在30秒
)

type Fish struct {
	FishID    int `json:"ID"` // 鱼的种类
	FishKey   int `json:"K"`  // 鱼的唯一ID
	Status    int `json:"S"`  // 状态
	Route     int `json:"R"`  // 游动线路
	Age       int `json:"A"`  // 已经生成的时间，ms
	Speed     int `json:"Sp"`
	spawnMs   int
	extraLife int
}

func (f *Fish) Dump() {
	log.Debug("%d,%d,%d,%d,%d", f.FishID, f.FishKey, f.Status, f.Route, f.GetAge())
}

func (f *Fish) GetAge() int {
	ret := int(time.Now().UnixNano()/1000000) - f.spawnMs
	if ret < 0 {
		return 0
	}
	return ret
}

func (f *Fish) IsDead() bool {
	f.Age = f.GetAge()
	return f.Status == Fish_Dead || f.Age >= Fish_Life+f.extraLife
}

func (f *Fish) AddExtraLife(seconds int) {
	f.spawnMs += seconds * 1000
}

func (f *Fish) IsCatch(userOdds int, systemOdds int, roomOdds int) bool {
	cfg := fishcfg.getFish(f.FishID)
	if cfg == nil {
		log.Release("Fish.IsCatch fishID[%d] not found", f.FishID)
		return false
	}
	return cfg.isCatch(userOdds, systemOdds, roomOdds)
}

func (f *Fish) GetPrize() (int, int) {
	cfg := fishcfg.getFish(f.FishID)
	if cfg == nil {
		log.Release("Fish.GetPrize fishID[%d] not found", f.FishID)
		return 0, 0
	}
	return cfg.getPrize()
}

func (f *Fish) GetOdds() int {
	fishcfg := fishcfg.getFish(f.FishID)
	if fishcfg == nil {
		log.Release("Fish.GetOdds fish not found %d", f.FishID)
		return 1
	}
	return fishcfg.Odds
}

func Run() {
	fishcfg = new_fishcfg()
	routemgr = new_route_manager()
}

func GetFishConfig() string {
	return fishcfg.getConfig()
}

func GetFishInfo(fishID int) *FishInfo {
	return fishcfg.getFish(fishID)
}

func GenerateFish(fishKey int, fishID, speed int, routeID int, delaySec int) *Fish {
	fish := new(Fish)
	fish.FishKey = fishKey
	fish.Status = Fish_Alive
	fish.FishID = fishID
	fish.Speed = speed
	fish.Route = routeID
	fish.spawnMs = int(time.Now().Add(time.Second*time.Duration(delaySec)).UnixNano()) / 1000000
	return fish
}

func GetRandomRoute() int {
	return routemgr.getRandomRoute()
}
