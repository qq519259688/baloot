package fish

import (
	"encoding/json"
	"math/rand"
	"os"
	"time"

	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

type FishInfo struct {
	FishID    int    // 鱼的ID
	Name      string // 鱼的名字
	CatchRate int    // 概率分子
	Odds      int    // 赔付倍数
	FishType  int
	BonusFish int `json:",omitempty"`
	item.EffectInfo
	Prizes []Prize `json:",omitempty"`
}

const MAX_RETURN_RATE = 9000

var maxReturn int

const config_key = "fish_fishconfig"

type Prize struct {
	Chance    int `json:",omitempty"` // 概率 万分之
	ItemId    int `json:",omitempty"`
	ItemCount int `json:",omitempty"`
}

func (this *FishInfo) IsCatch(userOdds int, systemOdds int, roomOdds int) bool {
	return this.isCatch(userOdds, systemOdds, roomOdds)
}

// 以万为单位
func (this *FishInfo) isCatch(userOdds int, systemOdds int, roomOdds int) bool {
	odds := this.CatchRate
	odds = int(odds * (10000 + userOdds + systemOdds + roomOdds) / 10000)

	if odds*this.Odds > maxReturn {
		// log.Debug("FishInfo.isCatch 倍率：%d 概率超出 %d 修正后: %d", this.Odds, odds, maxReturn/this.Odds)
		odds = maxReturn / this.Odds

	}

	//log.Debug("FishInfo.isCatch %d,%d", this.CatchRate, odds)
	return rand.Intn(10000) < odds
}

func (this *FishInfo) getPrize() (int, int) {
	if len(this.Prizes) == 0 {
		return 0, 0
	}
	totalChance := 0
	for _, v := range this.Prizes {
		totalChance += v.Chance
	}

	result := rand.Intn(totalChance)
	current := 0

	for _, v := range this.Prizes {
		if result < current+v.Chance {
			return v.ItemId, v.ItemCount
		}
		current += v.Chance
	}
	log.Debug("FishInfo.getPrize no result error current = %d,result = %d,totalChance = %d", current, result, totalChance)
	return 0, 0
}

type FishCfg struct {
	Fishes           []FishInfo
	MaxReturn        int
	lastConfigString string
}

func new_fishcfg() *FishCfg {
	ret := new(FishCfg)
	ret.initData()
	return ret
}

func (this *FishCfg) initData() {
	time.AfterFunc(10*time.Minute, this.initData)
	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("fish/FishConfig.json")
		if err != nil {
			log.Release("FishCfg.FishConfig.loadData read json failed")
			return
		}
		configString = string(data)
		platformconfig.SetConfig(config_key, configString)
	}
	if configString == this.lastConfigString {
		return
	}
	this.lastConfigString = configString

	err := json.Unmarshal([]byte(configString), &this)
	if err != nil {
		log.Release("Unmarshal FishConfig.json failed err:%v", err)
		return
	}
	maxReturn = MAX_RETURN_RATE
	if this.MaxReturn > 0 {
		maxReturn = this.MaxReturn
	}
	log.Debug("FishConfig initData ok MaxReturn = %d,\n%v", maxReturn, this.getConfig())
}

func (this *FishCfg) getFish(fishID int) *FishInfo {
	for _, v := range this.Fishes {
		if v.FishID == fishID {
			return &v
		}
	}
	return nil
}

func (this *FishCfg) getRandomFish() int {
	r := rand.Intn(len(this.Fishes))
	return this.Fishes[r].FishID
}

func (this *FishCfg) getConfig() string {
	d, _ := json.Marshal(this.Fishes)
	return string(d)
}
