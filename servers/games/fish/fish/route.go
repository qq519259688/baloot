package fish

import (
	"encoding/json"
	"math/rand"
	"os"

	"bet24.com/log"
)

// 所有鱼的行进速度是统一的
// 非停留状态，不设置时间

const (
	RouteType_Line   = 0 // 直线
	RouteType_Bezier = 1 // 贝塞尔曲线
	RouteType_Stay   = 2 // 停留
)

type Point struct {
	X int
	Y int
}

// 路线片段
type RouteSeg struct {
	Type   int     // 类型
	Points []Point // 关键点
	Time   int     // 时间，停留才有用
}

// 路线
type Route struct {
	RouteID int // 路线ID
	Segs    []RouteSeg
}

type RouteManager struct {
	//Routes []Route
	RouteMin int
	RouteMax int
}

func new_route_manager() *RouteManager {
	ret := new(RouteManager)
	ret.initData()
	return ret
}

func (this *RouteManager) initData() {
	data, err := os.ReadFile("fish/FishRoute.json")
	if err != nil {
		log.Release("read FishRoute.json failed")
	}
	err = json.Unmarshal(data, &this)
	if err != nil {
		log.Release("Unmarshal FishRoute.json failed err:%v", err)
	}
	log.Debug("FishRoute initData ok %v", this)
}

func (this *RouteManager) getRandomRoute() int {
	if this.RouteMax <= this.RouteMin {
		return this.RouteMin
	}
	return this.RouteMin + rand.Intn(this.RouteMax-this.RouteMin)
}
