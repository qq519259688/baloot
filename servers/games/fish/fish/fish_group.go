package fish

import (
	"encoding/json"
	"fmt"
	"os"

	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const group_config_key = "fishgroup_"

type route struct {
	Type        int // 类型
	Time        int // 时间，当类型为停留时才有效
	Destination Point
}

type group_fish struct {
	FishID     int
	Delay      int
	GroupDelay int
	Count      int
	LocalRoute int
}

type FishGroup struct {
	GroupName string
	FishKey   int // 鱼群起始的fishkey
	Speed     int
	Duration  int
	PassedMs  int // 已经过去多长时间了ms
	Fishes    []group_fish
}

func getConfigKey(groupName string) string {
	return fmt.Sprintf("%s%s", group_config_key, groupName)
}

func NewFishGroup(groupName string) *FishGroup {
	this := new(FishGroup)
	if !this.initData(groupName) {
		return nil
	}
	return this
}

func (this *FishGroup) initData(groupName string) bool {
	configString := platformconfig.GetConfig(getConfigKey(groupName))
	if configString == "" {
		groupFile := fmt.Sprintf("fish/%v.json", groupName)
		data, err := os.ReadFile(groupFile)

		if err != nil {
			log.Release("read FishGroup.json failed %v", groupFile)
			return false
		}
		configString = string(data)
		platformconfig.SetConfig(getConfigKey(groupName), configString)
	}
	err := json.Unmarshal([]byte(configString), &this)
	if err != nil {
		log.Release("Unmarshal FishGroup.json failed err:%v", err)
		return false
	}
	log.Debug("FishGroup initData ok %v", this)
	return true
}

func (this *FishGroup) GetJson(passedMs int) string {
	this.PassedMs = passedMs
	data, _ := json.Marshal(this)
	return string(data)
}
