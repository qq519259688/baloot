package prizepool

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

// 获取奖池信息
func getInfo(roomName string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("prizePool.getInfo transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var pool int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_PrizePool_GetInfo")
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdVarChar, 32, roomName)
	statement.AddParamter("@Pool", database.AdParamOutput, database.AdInteger, 4, pool)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return pool
	}
	if len(retRows[0]) <= 0 {
		return pool
	}

	if val, ok := retRows[0][0].(int64); ok {
		pool = int(val)
	}
	//pool = int((*retRows[0][0].(*interface{})).(int64))
	return pool
}

// 更新奖池
func update(roomName string, pool int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("prizePool.update transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_PrizePool_Update")
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdVarChar, 32, roomName)
	statement.AddParamter("@Pool", database.AdParamInput, database.AdInteger, 4, pool)
	sqlstring := statement.GenSql()
	dbengine.Execute(sqlstring)
}

// 获取用户的奖池能量
func getUserEnergy(userid int, roomName string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("prizePool.getUserEnergy transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_PrizePool_GetEnergy")
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userid)
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdVarChar, 32, roomName)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}
	if len(retRows[0]) <= 0 {
		return 0
	}

	ret := 0
	if val, ok := retRows[0][0].(int64); ok {
		ret = int(val)
	}
	// ret := int((*retRows[0][0].(*interface{})).(int64))
	return ret
}

// 修改用户的奖池能量
func setUserEnergy(userid int, roomName string, energy int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("prizePool.modifyUserEnergy transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_PrizePool_ModifyEnergy")
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userid)
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdVarChar, 32, roomName)
	statement.AddParamter("@Energy", database.AdParamInput, database.AdInteger, 4, energy)
	sqlstring := statement.GenSql()
	dbengine.ExecuteRs(sqlstring)
}
