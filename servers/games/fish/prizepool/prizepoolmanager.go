package prizepool

import (
	"sync"
)

func GetPrizePool(roomName string, roomMinBulletId int) *PrizePool {
	return getPoolManager().getPrizePool(roomName, roomMinBulletId)
}

var poolMgr *poolmanager

func getPoolManager() *poolmanager {
	if poolMgr == nil {
		poolMgr = new(poolmanager)
		poolMgr.ctor()
	}
	return poolMgr
}

type poolmanager struct {
	pools map[string]*PrizePool
	lock  *sync.RWMutex
}

func (pm *poolmanager) ctor() {
	pm.pools = make(map[string]*PrizePool)
	pm.lock = &sync.RWMutex{}
}

func (pm *poolmanager) getPrizePool(roomName string, roomMinBulletId int) *PrizePool {
	pm.lock.RLock()
	p, ok := pm.pools[roomName]
	pm.lock.RUnlock()
	if ok {
		return p
	}

	p = NewPrizePool(roomName, roomMinBulletId)

	pm.lock.Lock()
	pm.pools[roomName] = p
	pm.lock.Unlock()
	return p
}
