package bullet

import (
	"encoding/json"
	"os"
	"time"

	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "fishbullet_config"

type BulletInfo struct {
	BulletID int // 子弹ID
	Odds     int // 倍数，即对应消耗的金币
	Upgrade  int // 升级需消耗道具数量
}

type BulletCfg struct {
	Bullets          []BulletInfo
	lastConfigString string
}

func new_bulletcfg() *BulletCfg {
	ret := new(BulletCfg)
	ret.initData()
	return ret
}

func (this *BulletCfg) initData() {
	time.AfterFunc(10*time.Minute, this.initData)
	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("fish/BulletConfig.json")
		if err != nil {
			log.Release("BulletCfg.initData.loadData read json failed")
			return
		}
		configString = string(data)
		platformconfig.SetConfig(config_key, configString)
	}
	if configString == this.lastConfigString {
		return
	}
	this.lastConfigString = configString

	err := json.Unmarshal([]byte(configString), &this)
	if err != nil {
		log.Release("BulletCfg.initData Unmarshal config [%s] err:%v", configString, err)
		return
	}

	log.Debug("BulletConfig initData ok %v", this)
}

func (this *BulletCfg) getBullet(bulletID int) *BulletInfo {
	for _, v := range this.Bullets {
		if v.BulletID == bulletID {
			return &v
		}
	}
	return nil
}

func (this *BulletCfg) getConfig() string {
	d, _ := json.Marshal(this.Bullets)
	return string(d)
}

func (this *BulletCfg) getUpgradeCount(curId int) int {
	for _, v := range this.Bullets {
		if v.BulletID == curId+1 {
			return v.Upgrade
		}
	}
	return 0
}
