package bullet

import "bet24.com/log"

type Bullet struct {
	BulletID  int `json:"ID"`
	BulletKey int `json:"BK"`
	Angle     int `json:"A"`
	UserID    int `json:"U"`
	FishKey   int `json:"FK"`
	ClientID  int `json:"C"`
}

var bullet_cfg *BulletCfg

func Run() {
	bullet_cfg = new_bulletcfg()
}

func GetBullet(bulletID int) *BulletInfo {
	if bullet_cfg == nil {
		bullet_cfg = new_bulletcfg()
	}
	return bullet_cfg.getBullet(bulletID)
}

func (b *Bullet) GetOdds() int {
	bi := GetBullet(b.BulletID)
	if bi == nil {
		log.Release("Bullet.GetOdds bullet not found  %d", b.BulletID)
		return 1
	}
	return bi.Odds
}

func GetConfig() string {
	return bullet_cfg.getConfig()
}

func GetMinBulletID() int {
	return bullet_cfg.Bullets[0].BulletID
}

func GetUpgradeCount(curBulletId int) int {
	return bullet_cfg.getUpgradeCount(curBulletId)
}

func Dump() {
	log.Release("-------------------------------")
	log.Release("bullet.Dump")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	log.Release(GetConfig())
}
