package fishcommon

import (
	"bet24.com/servers/games/fish/fish"
)

const (
	PING              = "ping"
	LOGIN             = "login"
	JOIN              = "join"
	CHANGE_TABLE      = "change_table"
	EXIT              = "exit"
	USER_ENTER        = "user_enter"
	USER_EXIT         = "user_exit"
	GAME_SCENE        = "game_scene"
	ADD_BULLET        = "add_bullet"
	BULLET_FAILED     = "bullet_failed"
	ADD_FISH          = "add_fish"
	CATCH_FISH        = "catch_fish"
	CATCH_FAILED      = "catch_failed"
	SET_BULLET        = "user_set_bullet"
	SET_CANNON        = "user_set_cannon"
	UPDATE_USER_INFO  = "update_user_info"
	USE_ITEM          = "use_item"
	USE_ITEM_FAILED   = "use_item_failed"
	EFFECTING         = "effecting"         // 广播全局道具状态，这里为空，表示取消状态
	BATCH_CATCH       = "batch_catch"       // 范围攻击，主动使用道具或者击中特殊鱼产生
	SPECIAL_CATCH     = "special_catch"     // 触发特殊鱼后，可以使用特殊攻击
	SPECIAL_CATCH_END = "special_catch_end" // 特效结束
	SPECIAL_SHOOT     = "special_shoot"     // 特殊鱼发射
	// 服务器下发的配置
	CONFIG_BULLET  = "conf_bullet"
	CONFIG_FISH    = "conf_fish"
	CONFIG_ROUTE   = "conf_route"
	GROUP_COME     = "group_come" // 鱼群来了
	ITEM_REFRESH   = "item_refresh"
	UPGRADE_BULLET = "upgrade_bullet" // 升级子弹倍数
	POOL_VALUE     = "pool_value"
	POOL_DRAW      = "pool_draw"
	POOL_ENERGY    = "pool_energy"
	IDLE_KICK      = "idle_kick"
)

type GameScene_User struct {
	UserId   int
	CannonId int
	Bullet   int
}

type GameScene struct {
	RoomID    int
	BossAfter int // 还有多少秒刷Boss，小于等于0不显示
	Users     []GameScene_User
	Fishes    []*fish.Fish
	Effecting []int // 当前效果
}

type AddBullet struct {
	BulletID int // 子弹类型
	FishKey  int // 是否瞄准
	Angle    int // 角度
	ClientID int // 客户端ID，失败时自行删除
}

type CatchFish struct {
	BulletKey int
	FishKey   int
}

// 使用特殊鱼的效果捕获
type SpecialCatch struct {
	OriginFishKey  int   // 获得炮弹的鱼的fishkey或道具ID
	TargetFishKeys []int // 目标鱼的fishkey
}

type SpecialCatchEnd struct {
	OriginFishKey int // 获得炮弹的鱼的fishkey或道具ID
	UserId        int // 用户ID，下行才有
	TotalEarn     int // 总共获取的金币数量，下行才有
}

type CatchFishFailed struct {
	CatchFish
	BulletID int
}

type CatchFishRet struct {
	CatchFish
	IsCatch             bool // 是否击落
	Coin                int  // 奖励
	UserId              int
	BulletID            int
	SlotMultiple        int `json:",omitempty"` // 如果击中slot鱼，会产生一个倍数奖励
	ItemId              int `json:",omitempty"` // 如果产生道具
	ItemCount           int `json:",omitempty"` // 如果产生道具
	PrizeWheelItemId    int `json:",omitempty"` // 如果击中转盘鱼，产生转盘的结果
	PrizeWheelItemCount int `json:",omitempty"` // 如果击中转盘鱼，产生转盘的结果
}

type SetBullet struct {
	BulletID int
}

type SetCannon struct {
	Cannon int
}

type UpdateUserInfo struct {
	UserId   int
	Gold     int
	BulletID int
	Cannon   int
	Essence  int
}

// 当存入时，不需要加密码
type BankOperation struct {
	Amount   int
	Password string
}

type BankOpeReturn struct {
	CashAmount int
	BankAmount int
	Errmsg     string
}

/*
使用道具
use_item
上下行: UseItem 上行不需要传UserId
*/
type UseItem struct {
	UserId int // 上行不用填
	ItemId int
}

/*
	使用道具失败
	use_item_failed
	下行: 失败原因
*/

type UseItemFailed struct {
	ItemId int
	ErrMsg string
}

/*
   房间效果广播
   effecting
   广播全局道具状态，这里为空，表示取消状态
   []int,可以有多个效果同时生效
   场景数据添加
   Effecting []int // 当前效果
*/

type UpgradeBullet_resp struct {
	Succeeded bool
	ErrorMsg  string
}

type PrizePoolDraw_resp struct {
	UserId int  // 谁中奖了
	Pool   int  // 奖池剩余数量
	Bingo  bool // 是否中奖
	Point  int  // 如果中奖，则为奖金，如果不中，则为能量点
}
