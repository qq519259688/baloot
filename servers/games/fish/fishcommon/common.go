package fishcommon

import (
	"time"
)

const (
	GAMEID                  int = 70
	GAME_NAME                   = "fish"
	CREATE_FORWARD_SECONDES     = 10
	SEAT_COUNT                  = 4

	TIMER_WRITESCORE      = 1    // 写分
	TIME_WRITESCORE       = 3000 // 每隔3秒写分
	TIMER_UPDATAWATERPOOL = 2
	TIME_UPDATAWATERPOOL  = 10000
)

func GetTime() int {
	now := time.Now()
	return now.Second() + now.Minute()*100 + now.Hour()*10000
}

func GetBatchID() int {
	now := time.Now()
	return now.Minute() + now.Hour()*100 + now.Day()*10000 + int(now.Month())*1000000 + (now.Year()%10)*100000000
}

func IsValidChair(chairId int) bool {
	return chairId >= 0 && chairId < SEAT_COUNT
}
