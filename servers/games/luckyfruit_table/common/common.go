package common

import (
	"fmt"

	"bet24.com/servers/games/luckyfruit_table/config"
	userservices "bet24.com/servers/micros/userservices/proto"
)

const (
	GAMEID    = 39
	GAME_NAME = "luckyfruit"
)

const (
	GameState_Bet = iota
	GameState_Open
)

const (
	TIMERID_CHECKROBOT = iota
)

type UserSetScore struct {
	UserId int
	Score  int
}

type GameState struct {
	RoomName     string
	SerialNumber int
	State        int                  // 当前状态
	Sec          int                  // 当前阶段已使用时间
	SpinResult   Fruit                // 本局开奖结果
	Historys     []config.OpenHistory // 历史纪录
	TableUser    ScoreUsers           // 幸运星和赢金榜的玩家
	//RichList     []int              //富豪榜
	BetList []int //下注榜
	//Scores  []UserSetScore // 结算阶段，广播所有人的分数,给桌面玩家或者自己头像飘分用 水果机不需要 只飘自己的分数
	BetCmds []Bet // 本局下注列表
}

type BetBase struct {
	BetId   int
	Amount  int
	IsFree  bool
	IsRobot bool `json:"-"`
}

type Bet struct {
	BetBase
	UserId int
	Index  int // 添加一个索引，用于失败撤销
}

type Batch_Bet struct {
	UserId int
	Bets   []BetBase
}

type RankingUser struct {
	UserId int
	Score  int
}

// 是否相同下注类型
func (b *Bet) IsSameBet(b2 *Bet) bool {
	return b.BetId == b2.BetId && b.IsFree == b2.IsFree
}

type BetRet struct {
	ErrorMsg string
}

type Result struct {
	Bet
	WinAmount  int
	SpinResult Fruit
	WinArea    Fruit
	Tax        int
}

type RecordInfo struct {
	SpinResult   Fruit
	SerialNumber int
}

type UserClear struct {
	RoomName string
	UserId   int
}

// 连胜用户和累计下注用户
type ScoreUser struct {
	UserId      int
	NickName    string
	FaceId      int
	FaceUrl     string
	VipLevel    int
	VipExpire   int
	Decorations []userservices.UserDecoration
	IsRobot     bool `json:"-"`
	Info        []int
	BetAmount   int
}

type ScoreUsers struct {
	//LuckyStarUsers    []ScoreUser
	WinGoldRankingUsers []ScoreUser
	BetGoldRankingUsers []ScoreUser
}

func IsInTopN(rankList []ScoreUser, userId, n int) bool {
	for i, v := range rankList {
		if i >= n {
			break
		}
		if v.UserId == userId {
			return true
		}
	}
	return false
}

func GetRedisKey(key string) string {
	return fmt.Sprintf("%s:%s", GAME_NAME, key)
}
