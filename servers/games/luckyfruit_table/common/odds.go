package common

import (
	"fmt"
	"math/rand"

	"bet24.com/log"
)

/*
大苹果 大西瓜 大葡萄 大香蕉 大橘子 1:50 1:20 1:10 1:8 1:5
苹果 西瓜 葡萄 香蕉 橘子 1:2 1:2 1:2 1:2 1:2
BigApple 50
BigWatermelon 20
BigGrape 10
BigBanana 8
BigOrange 5
Apple 2
Watermelon 2
Grape 2
Banana 2
Orange 2
*/

type Fruit int

const (
	Orange Fruit = iota
	Banana
	Grape
	Watermelon
	Apple
	BigOrange
	BigBanana
	BigGrape
	BigWatermelon
	BigApple
	BidTypeMax
)

var fruitNames = []string{
	"橙子",
	"香蕉",
	"葡萄",
	"西瓜",
	"苹果",
	"大橙子",
	"大香蕉",
	"大葡萄",
	"大西瓜",
	"大苹果",
}

var fruitProbability = []int{
	1357, // Orange
	1356, // Banana
	1357, // Grape
	1357, // Watermelon
	1356, // Apple
	1300, // BigOrange
	812,  // BigBanana
	650,  // BigGrape
	325,  // BigWatermelon
	130,  // BigApple
}

var fruitWinningOdds = map[Fruit]float64{
	Orange:        2,
	Banana:        2,
	Grape:         2,
	Watermelon:    2,
	Apple:         2,
	BigOrange:     5,
	BigBanana:     8,
	BigGrape:      10,
	BigWatermelon: 20,
	BigApple:      50,
}

func Spin() Fruit {
	// 计算水果的总概率
	var total float64
	for _, p := range fruitProbability {
		total += float64(p) / 10
	}

	// 生成一个随机数
	r := rand.Float64() * total

	// 根据随机数返回中奖结果
	var sum float64
	for i, p := range fruitProbability {
		sum += float64(p) / 10
		if r < sum {
			return Fruit(i)
		}
	}

	// 如果程序执行到这里，说明出现了意外情况，返回默认值
	return Orange
}

func convertBig(fruit Fruit) Fruit {
	switch fruit {
	case BigOrange:
		return Orange
	case BigBanana:
		return Banana
	case BigGrape:
		return Grape
	case BigWatermelon:
		return Watermelon
	case BigApple:
		return Apple
	default:
		return fruit
	}
}

func GetWinArea(spinResult Fruit) (result Fruit) {
	result = convertBig(spinResult)
	return
}

func GetOdds(spinResult Fruit) (odds float64) {
	odds = fruitWinningOdds[spinResult]
	return
}

func GetResultOdds(betFruit int, spinResult Fruit) (odds float64) {
	result := GetWinArea(spinResult)
	odds = 0

	if int(result) == betFruit {
		odds = GetOdds(spinResult)
	}
	return
}

// 刷新获胜赔率
func ResetWinningOdds(winningOdds []float64) {
	for i, o := range winningOdds {
		f := Fruit(i)
		if f < Orange || f > BigApple {
			continue // Invalid fruit index, skip
		}
		if fruitWinningOdds[f] != o {
			fruitWinningOdds[f] = o
		}
	}
}

// 刷新水果概率
func ResetFruitProbability(probability []int) {
	for i, p := range probability {
		f := Fruit(i)
		if f < Orange || f > BigApple {
			continue // Invalid fruit index, skip
		}
		if fruitProbability[i] != p {
			fruitProbability[i] = p
		}
	}
}

func GetBetDesc(betId int) string {
	max := int(Apple) + 1
	if betId >= max || betId < 0 {
		log.Release("common.GetDesc failed %d,%d", betId, max)
		return "invalid bet"
	}
	return fruitNames[betId]
}

func GetResultDesc(spinResult Fruit) string {
	return fmt.Sprintf("[%s:%s]", fruitNames[spinResult], fruitNames[GetWinArea(spinResult)])
}
