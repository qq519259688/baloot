package config

import (
	"encoding/json"
	"fmt"
	"os"
	slog "bet24.com/log"
	"bet24.com/utils"
	"log"
	"time"

)

var Server struct {
	LogLevel        string
	FileLevel       string
	LogPath         string
	CertFile        string
	KeyFile         string
	MaxConnNum      int
	Login           string
	Password        string
	Database        string
	Datasource      string
	GameDB          string
	ChannelUrl      string
	ChannelPassword string
	RedisDB         int
	LastHour        int
	ServiceAddr     string
	ServerName      string
	ServerIP        string
	ServerPort      int
	ServerAddr      string
	MonitorPort     int
	OfflineSeconds  int64
	IsChipRoom      bool
	IsPrivateRoom   bool
}

func init() {
	configFile := "luckyfruit_table/server.json"
	data, err := os.ReadFile(configFile)
	if err != nil {
		log.Fatalf("read config failed luckyfruit_table/server.json %v", err)
	}
	fmt.Println(string(data))
	err = json.Unmarshal(data, &Server)
	if err != nil {
		log.Fatalf("Unmarshal config failed luckyfruit_table/server.json err:%v", err)
		return
	}

	logger, err := slog.New(Server.LogLevel, Server.FileLevel, fmt.Sprintf("%v/%v", Server.LogPath, RoomConfigName), log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	originPort := Server.ServerPort
	for i := 0; i < 100; i++ {
		if utils.CheckPortInUse(Server.ServerPort) {
			Server.ServerPort++
			continue
		}
		break
	}
	if Server.MonitorPort == 0 {
		Server.MonitorPort = Server.ServerPort + 100
	}
	if originPort != Server.ServerPort {
		slog.Debug("Port %d in use, change to %d", originPort, Server.ServerPort)
		d, _ := json.Marshal(Server)
		os.WriteFile(configFile, d, 0644)
	}
	now := time.Now()
	Server.LastHour = now.Hour()
}

func Run() {
	refreshLogFile()
	loadRoomConfig()
}

func refreshLogFile() {
	time.AfterFunc(5*time.Minute, refreshLogFile)
	doRefreshLogFile()
}

func doRefreshLogFile() {
	if Server.LogPath != "" {
		now := time.Now()
		if now.Hour() != Server.LastHour {
			Server.LastHour = now.Hour()
			slog.RecreateFileLog(fmt.Sprintf("%v/%v", Server.LogPath, RoomConfigName), log.LstdFlags)
		}
	}
}
