package gamelogic

import (
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/games/luckyfruit_table/common"
	"bet24.com/servers/insecureframe/message"
	userservices "bet24.com/servers/micros/userservices/proto"
	"bet24.com/servers/transaction"
	"bet24.com/servers/user"
)

func (ts *tablesink) OnGameMessage(userIndex int32, msg, data string) bool {
	/*
		if !ok {
			log.Debug("tablesink.OnGameMessage user not exist %d", userIndex)
			return false
		}
	*/
	log.Debug("tablesink.OnGameMessage Receive Msg  %d,%v,%v", userIndex, msg, data)
	usr, _ := ts.table.GetUser(userIndex)
	if usr == nil {
		log.Debug("tablesink.OnGameMessage user not exist")
		return false
	}
	userId := usr.GetUserId()
	switch msg {
	case History:
		ts.sendHistory(userIndex)
	case Option:
		ts.sendGameOption(userIndex)
	case Bet:
		ts.handleBet(userIndex, userId, data)
	case BatchBet:
		ts.handleBatchBet(userIndex, userId, msg, data)
	case BetClear:
		ts.onBetClear(userIndex, userId, msg, data)
	case BetRecord:
		ts.onGetBetRecord(userIndex, userId, msg, data)
	case BetRank:
		ts.onGetBetRank(userIndex, userId, msg, data)
	case BetList:
		ts.onGetBetList(userIndex, userId, msg, data)
	case FreeChip:
		ts.onCheckFreeChip(userIndex, userId, msg, data)
	case FreeBet:
		ts.handleFreeBet(userIndex, userId, data)
	default:
		log.Debug("tablesink.OnGameMessage unhandled message %v  data %v", msg, data)
	}
	return true
}

func (ts *tablesink) sendBetFailed(userIndex int32, index int, errMsg string) {
	var bet_failed struct {
		ErrorMsg string
		Index    int
	}
	bet_failed.Index = index
	bet_failed.ErrorMsg = errMsg
	d, _ := json.Marshal(bet_failed)
	ts.table.SendGameData(userIndex, Betfailed, string(d))
}

func (ts *tablesink) sendBetOK(userIndex int32, data string) {
	ts.table.SendGameData(userIndex, Bet, data)
}

func (ts *tablesink) sendHistory(userIndex int32) {
	ts.table.SendGameData(userIndex, History, ts.getHistory())
}

func (ts *tablesink) sendGameScene(userIndex int32) {
	ts.table.SendGameData(userIndex, message.Table_GameScene, ts.getStateData())
}

// 处理筹码下注
func (ts *tablesink) handleBet(userIndex int32, userId int, data string) {
	usr, _ := ts.table.GetUser(userIndex)
	if usr == nil {
		log.Debug("tablesink.handleBet user not exist")
		return
	}
	// log.Debug("handleBet %v", data)
	var bet common.Bet
	e := json.Unmarshal([]byte(data), &bet)
	if e != nil {
		log.Release("handleBet Unmarshal data failed %v |e=%v:", data, e)
		return
	}

	amount := bet.Amount
	bet.UserId = userId
	bet.IsFree = false
	bet.IsRobot = usr.IsRobot()
	if amount <= 0 {
		ts.sendBetFailed(userIndex, bet.Index, "Invalid Bet!")
		return
	}
	// 看下能否下注
	ok, errMsg := ts.isCanBet(bet)
	if !ok {
		ts.sendBetFailed(userIndex, bet.Index, errMsg)
		return
	}
	ret, _ := ts.table.WriteUserMoney(userId, -amount, 0, 1, common.GAMEID*100+1, ts.roomInfo.RoomName)

	if !ret {
		// 扣金币失败
		ts.sendBetFailed(userIndex, bet.Index, "Not enough cash!")
		return
	}

	ok, err := ts.addBet(bet)
	if !ok {
		ts.table.WriteUserMoney(userId, amount, 0, 3, common.GAMEID*100+3, ts.roomInfo.RoomName)
		ts.sendBetFailed(userIndex, bet.Index, err)
	} else {
		ts.handleBetSuccess(userId, bet, usr)
		d, _ := json.Marshal(bet)
		ts.sendBetOK(-1, string(d))
	}
}

// 处理批量下注
func (ts *tablesink) handleBatchBet(userIndex int32, userId int, msg, data string) {
	usr, _ := ts.table.GetUser(userIndex)
	if usr == nil {
		log.Debug("tablesink.handleBatchBet user not exist")
		return
	}
	var batchBet common.Batch_Bet
	e := json.Unmarshal([]byte(data), &batchBet)
	if e != nil {
		log.Release("handleBatchBet Unmarshal data failed %v", data)
		return
	}

	// 先判断所有区域是否能下
	totalAmount := 0
	for _, v := range batchBet.Bets {
		var bet common.Bet
		bet.BetBase = v
		bet.UserId = userId
		ok, errMsg := ts.isCanBet(bet)
		if !ok {
			log.Debug("handleBatchBet failed to add bet %v,%v", bet, errMsg)
			ts.sendBetFailed(userIndex, bet.Index, errMsg)
			return
		}
		totalAmount += bet.Amount
	}

	ret, _ := ts.table.WriteUserMoney(userId, -totalAmount, 0, 1, common.GAMEID*100+1, ts.roomInfo.RoomName)

	if !ret {
		// 扣金币失败
		ts.sendBetFailed(userIndex, 0, "Not enough cash!")
		return
	}

	for _, v := range batchBet.Bets {
		var bet common.Bet
		bet.BetBase = v
		bet.UserId = userId
		bet.IsFree = false
		bet.IsRobot = usr.IsRobot()
		ok, _ := ts.addBet(bet)
		if !ok {
			go ts.table.WriteUserMoney(userId, bet.Amount, 0, 3, common.GAMEID*100+3, ts.roomInfo.RoomName)
		} else {
			ts.handleBetSuccess(userId, bet, usr)
		}
	}

	ts.table.SendGameData(-1, msg, data)
}

// 处理免费筹码下注
func (ts *tablesink) handleFreeBet(userIndex int32, userId int, data string) {
	usr, _ := ts.table.GetUser(userIndex)
	if usr == nil {
		log.Debug("tablesink.handleFreeBet user not exist")
		return
	}
	// log.Debug("handleFreeBet %v", data)
	var bet common.Bet
	e := json.Unmarshal([]byte(data), &bet)
	if e != nil {
		log.Release("handleFreeBet Unmarshal data failed %v |e=%v:", data, e)
		return
	}

	amount := bet.Amount
	bet.UserId = userId
	bet.IsFree = true
	bet.IsRobot = usr.IsRobot()
	if amount <= 0 {
		ts.sendBetFailed(userIndex, bet.Index, "Invalid Bet!")
		return
	}
	// 看下能否下注
	ok, errMsg := ts.isCanBet(bet)
	if !ok {
		ts.sendBetFailed(userIndex, bet.Index, errMsg)
		return
	}
	//检查免费筹码数量
	freeChip, _ := ts.freeChips.checkFreeChipsNum(userId)
	if freeChip < amount {
		log.Debug("handleFreeBet.checkFreeChipsNum Not enough free chip amount:%v,freeChip:%v", amount, freeChip)
		ts.sendBetFailed(userIndex, bet.Index, "Not enough free chip!")
		return
	}
	//扣除免费筹码
	ok, freeChip = ts.freeChips.useFreeChips(userId, amount)
	if !ok {
		log.Debug("handleFreeBet.useFreeChips Not enough free chip %v,freeChip:%v", ok, freeChip)
		ts.sendBetFailed(userIndex, bet.Index, "Not enough free chip!")
		return
	}

	ok, err := ts.addBet(bet)
	if !ok {
		//下注失败，返还免费筹码
		ts.freeChips.addFreeChips(userId, amount)
		ts.sendBetFailed(userIndex, bet.Index, err)
	} else {
		//免费不记录下注额
		d, _ := json.Marshal(bet)
		ts.table.SendGameData(userIndex, FreeBet, string(d))
	}
}

// 成功下注后处理handleBetSuccess
func (ts *tablesink) handleBetSuccess(userId int, bet common.Bet, usr *user.UserInfo) {
	if !usr.IsRobot() {
		amount := bet.Amount
		switch bet.BetId {
		case int(common.Apple):
			ts.appleAmount += amount
			ts.drawAppleWinAmount += amount
		case int(common.Watermelon):
			ts.watermelonAmount += amount
		case int(common.Grape):
			ts.grapeAmount += amount
		case int(common.Banana):
			ts.bananaAmount += amount
		case int(common.Orange):
			ts.orangeAmount += amount
		}
	}

}

func (ts *tablesink) onBetClear(userIndex int32, userId int, msg, data string) {
	ts.lock.Lock()
	betList, ok := ts.userBetList[userId]
	if !ok {
		ts.lock.Unlock()
		return
	}

	amount := 0
	for _, v := range betList {
		ts.roomInfo.TotalBet -= v.Amount
		amount += v.Amount
	}
	delete(ts.userBetList, userId)
	ts.lock.Unlock()
	ts.table.WriteUserMoney(userId, amount, 0, 3, common.GAMEID*100+3, ts.roomInfo.RoomName)

	betClear := common.UserClear{UserId: userId}
	d, _ := json.Marshal(betClear)
	ts.table.SendGameData(userIndex, msg, string(d))
	ts.userList.clearBet(userId, amount)
}

func (ts *tablesink) onGetBetRecord(userIndex int32, userId int, msg, data string) {
	//log.Debug("tablesink.onGetBetRecord %v", data)
	var getRecord struct {
		RecordCount int
	}
	e := json.Unmarshal([]byte(data), &getRecord)
	if e != nil {
		log.Release("tablesink.onGetBetRecord Unmarshal data failed %v", data)
		return
	}

	records := transaction.GetGameRecord(userId, common.GAMEID, ts.roomInfo.RoomName, getRecord.RecordCount)
	retData, _ := json.Marshal(records)
	ts.table.SendGameData(userIndex, msg, string(retData))
}

func (ts *tablesink) sendGameOption(userIndex int32) {
	d, _ := json.Marshal(ts.roomInfo.RoomInfoBase)
	ts.table.SendGameData(userIndex, Option, string(d))
}

func (ts *tablesink) onGetBetRank(userIndex int32, userId int, msg, data string) {
	//用户id
	var userIds []int
	//获取betRank前15名
	rankList, jackpot, myBetAmount, myRank := ts.betRank.getRankList(userId)
	log.Debug("tablesink.onGetBetRank %v,userId:%d,rankList:%v, jackpot:%d, myBetAmount:%d, myRank:%d", data, userId, rankList, jackpot, myBetAmount, myRank)
	//不为空
	if len(rankList) > 0 {
		//拼接数据返回
		var rankData struct {
			RankList    []common.ScoreUser
			Jackpot     int
			MyBetAmount int
			MyRank      int
		}
		scoreUserList := make([]common.ScoreUser, 0, len(rankList))
		//如果rankList昵称为空则补齐
		userIds = make([]int, len(rankList))
		for i, u := range rankList {
			userIds[i] = u.UserId
		}
		users := userservices.GetUserInfoInBulk(userIds)
		//如果rankList昵称为空则补齐
		for i, v := range rankList {

			scoreUser := common.ScoreUser{
				UserId:      v.UserId,
				BetAmount:   v.BetAmount,
				NickName:    "",
				FaceId:      0,
				FaceUrl:     "",
				VipLevel:    0,
				VipExpire:   0,
				Decorations: nil,
			}
			user := users[i]
			if user.UserId == v.UserId {
				scoreUser.NickName = user.NickName
				scoreUser.FaceId = user.FaceId
				scoreUser.FaceUrl = user.FaceUrl
				scoreUser.VipLevel = user.Vip
				scoreUser.VipExpire = user.VipExpire
				scoreUser.Decorations = user.Decorations
			}
			scoreUserList = append(scoreUserList, scoreUser)
		}
		rankData.RankList = scoreUserList
		rankData.Jackpot = jackpot
		rankData.MyBetAmount = myBetAmount
		rankData.MyRank = myRank
		retData, _ := json.Marshal(rankData)
		ts.table.SendGameData(userIndex, msg, string(retData))
	} else {
		ts.table.SendGameData(userIndex, msg, "[]")
	}

}

func (ts *tablesink) onGetBetList(userIndex int32, userId int, msg, data string) {

	rankList := ts.userList.getBetGoldRankingUsers(0)
	log.Debug("tablesink.onGetBetList %v,rankList:%v", data, rankList)
	//不为空
	if len(rankList) > 0 {
		retData, _ := json.Marshal(rankList)
		ts.table.SendGameData(userIndex, msg, string(retData))
	} else {
		ts.table.SendGameData(userIndex, msg, "[]")
	}
}

func (ts *tablesink) onCheckFreeChip(userIndex int32, userId int, msg, data string) {
	//log.Debug("tablesink.onCheckFreeChip %v", data)
	freeChip, lastHour := ts.freeChips.checkFreeChipsNum(userId)
	//返回数据
	var result struct {
		Free     int //剩余免费额度
		LastHour int //前一次领取时间 0 6 12 18
	}
	result.Free = freeChip
	result.LastHour = lastHour
	retData, _ := json.Marshal(result)
	ts.table.SendGameData(userIndex, msg, string(retData))
}
