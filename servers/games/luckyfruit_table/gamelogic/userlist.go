package gamelogic

import (
	"sort"
	"sync"

	"bet24.com/log"
	"bet24.com/servers/games/luckyfruit_table/common"
	userservices "bet24.com/servers/micros/userservices/proto"
)

type BetArea struct {
	Area  int
	Count int
}

type luckyfruitUser struct {
	userId      int
	nickName    string
	faceId      int
	faceUrl     string
	vipLevel    int
	vipExpire   int
	decorations []userservices.UserDecoration

	totalBet            int
	betAreas            []BetArea //下注区域 统计经常下注的两个区域
	winAmount           int       //实际赢金 输了不计
	cumulativeWinAmount int       //累计赢金 输了不计
	winAmountOnly       int       //只计算赢的金额
	isBet               bool
}

type userlist struct {
	users []*luckyfruitUser
	lock  *sync.RWMutex
}

func newUserList() *userlist {
	ret := new(userlist)
	ret.lock = &sync.RWMutex{}
	return ret
}

func (ul *userlist) addUser( userId int, nickName string,
	faceId int, faceUrl string, vipLevel, vipExpire int, decorations []userservices.UserDecoration) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			log.Release("userlist.addUser UserId  %d already exist", userId)
			return
		}
	}
	u := new(luckyfruitUser)
	u.userId = userId
	u.nickName = nickName
	u.faceId = faceId
	u.faceUrl = faceUrl
	u.vipLevel = vipLevel
	u.vipExpire = vipExpire
	u.decorations = decorations
	ul.users = append(ul.users, u)

}

func (ul *userlist) removeUser(userId int) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for k, v := range ul.users {
		if v.userId == userId {
			ul.users = append(ul.users[:k], ul.users[k+1:]...)
			return
		}
	}
	log.Release("userlist.removeUser %d not found", userId)
}

// 结束的时候清理状态，并且根据未下注进行连胜扣减
func (ul *userlist) clearBetStatus() {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if !v.isBet {
			if v.winAmount > 0 {
				v.winAmount = 0
			}
			v.winAmountOnly = 0
		} else {
			v.isBet = false
		}
	}
}

func (ul *userlist) addResult(userId, winAmount, winAmountOnly int) {
	log.Debug("userlist.addResult %d,%d", userId, winAmount)
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			if winAmount < 0 {
				v.winAmount = 0
			}
			if winAmountOnly > 0 {
				v.winAmountOnly = winAmountOnly
			} else {
				v.winAmountOnly = 0
			}
			if winAmount > 0 {
				v.winAmount = winAmount
				v.cumulativeWinAmount = v.cumulativeWinAmount + winAmount
			}
			return
		}
	}
	log.Release("userlist.addResult %d not found", userId)
}

func (ul *userlist) addBet(userId, amount, betId int) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			v.totalBet += amount
			v.isBet = v.totalBet > 0
			updateBetAreas(v, betId)
			return
		}
	}
	log.Release("userlist.addBet %d not found", userId)
}

func (ul *userlist) clearBet(userId, amount int) {
	ul.lock.Lock()
	defer ul.lock.Unlock()
	for _, v := range ul.users {
		if v.userId == userId {
			v.totalBet -= amount
			v.isBet = v.totalBet > 0
			//撤销下注，不需要更新下注区域
			return
		}
	}
	log.Release("userlist.addBet %d not found", userId)
}

func updateBetAreas(u *luckyfruitUser, area int) {
	for i := range u.betAreas {
		if u.betAreas[i].Area == area {
			u.betAreas[i].Count++
			return
		}
	}
	//如果没有找到，添加
	u.betAreas = append(u.betAreas, BetArea{Area: area, Count: 1})
}

// 幸运星
// func (ul *userlist) getLuckyStarUsers(count int) []common.ScoreUser {
// 	ul.lock.Lock()
// 	defer ul.lock.Unlock()
// 	sort.Slice(ul.users, func(i, j int) bool {
// 		return ul.users[i].cumulativeWinAmount > ul.users[j].cumulativeWinAmount
// 	})
// 	ret := []common.ScoreUser{}
// 	for k, v := range ul.users {
// 		if k >= count {
// 			break
// 		}
// 		if v.cumulativeWinAmount <= 0 {
// 			break
// 		}
// 		u := common.ScoreUser{UserId: v.userId, Score: v.cumulativeWinAmount}
// 		ret = append(ret, u)
// 	}
// 	return ret
// }

// 每回合赢金排行
func (ul *userlist) getWinGoldRankingUsers(count int) []common.ScoreUser {

	ul.lock.Lock()
	defer ul.lock.Unlock()
	//排序
	sort.Slice(ul.users, func(i, j int) bool {
		return ul.users[i].winAmountOnly > ul.users[j].winAmountOnly
	})

	ret := []common.ScoreUser{}
	for k, v := range ul.users {
		if k >= count {
			break
		}
		if v.winAmountOnly <= 0 {
			break
		}
		info := make([]int, 0, 1)
		info = append(info, v.winAmountOnly)
		u := common.ScoreUser{UserId: v.userId, NickName: v.nickName,
			FaceId:      v.faceId,
			FaceUrl:     v.faceUrl,
			VipLevel:    v.vipLevel,
			VipExpire:   v.vipExpire,
			Decorations: v.decorations,
			Info:        info}

		ret = append(ret, u)
	}
	return ret

}

// 传递0表示不限制数量
func (ul *userlist) getBetGoldRankingUsers(count int) []common.ScoreUser {

	ul.lock.Lock()
	defer ul.lock.Unlock()
	//排序
	sort.Slice(ul.users, func(i, j int) bool {
		return ul.users[i].totalBet > ul.users[j].totalBet
	})

	ret := []common.ScoreUser{}
	for k, v := range ul.users {
		if k >= count && count != 0 {
			break
		}
		if v.totalBet < 0 {
			break
		}
		//添加热衷下注区域
		betArea := getTopTwoBetAreas(v.betAreas)

		u := common.ScoreUser{UserId: v.userId, NickName: v.nickName,
			FaceId:      v.faceId,
			FaceUrl:     v.faceUrl,
			VipLevel:    v.vipLevel,
			VipExpire:   v.vipExpire,
			Decorations: v.decorations,
			BetAmount:   v.totalBet,
			Info:        betArea}
		ret = append(ret, u)
	}
	return ret
}

func getTopTwoBetAreas(betAreas []BetArea) []int {
	// 对下注区域按照Count值从大到小排序 如果数量相同，按照Area值从小到大排序
	sort.Slice(betAreas, func(i, j int) bool {
		if betAreas[i].Count == betAreas[j].Count {
			return betAreas[i].Area < betAreas[j].Area
		}
		return betAreas[i].Count > betAreas[j].Count
	})

	// 取前两个下注区域
	topTwoBetAreas := make([]int, 0, 2)
	for i := 0; i < len(betAreas) && len(topTwoBetAreas) < 2; i++ {
		topTwoBetAreas = append(topTwoBetAreas, betAreas[i].Area)
	}
	// log.Release("下注区域betAreas:%v ,topTwoBetAreas:%v", betAreas, topTwoBetAreas)
	return topTwoBetAreas
}
