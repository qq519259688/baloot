package gamelogic

import (
	"math/rand"
	"time"

	"bet24.com/servers/games/luckyfruit_table/common"
	"bet24.com/servers/games/luckyfruit_table/config"
	"bet24.com/servers/insecureframe/frame"
)

var gs *gamesink

func newGameSink() *gamesink {
	gs = new(gamesink)
	return gs
}

type gamesink struct {
}

func (gs *gamesink) GetServerPort() int {
	return config.Server.ServerPort
}
func (gs *gamesink) GetGameID() int {
	return common.GAMEID
}
func (gs *gamesink) GetGameName() string {
	return common.GAME_NAME
}

func (gs *gamesink) GetRoomName() string {
	return config.Server.ServerName
}
func (gs *gamesink) CreateTableSink(table frame.Table, data string) frame.TableSink {
	return newTableSink(table, data)
}
func (gs *gamesink) GetOfflineSeconds() int64 {
	return 0
}

func (gs *gamesink) GetCertFile() string {
	return config.Server.CertFile
}
func (gs *gamesink) GetKeyFile() string {
	return config.Server.KeyFile
}
func (gs *gamesink) GetServerAddr() string {
	return config.Rooms.Rooms[0].ServerIP
}

func (gs *gamesink) GetRobotCount() int {
	if len(config.Rooms.Rooms[0].RobotConfig) == 0 {
		return 0
	}
	max := 0
	for _, v := range config.Rooms.Rooms[0].RobotConfig {
		if v.CountMax > max {
			max = v.CountMax
		}
	}
	return max
}

func (gs *gamesink) GetRobotGoldLimit() (min, max int) {
	if len(config.Rooms.Rooms[0].RobotConfig) == 0 {
		return 0, 0
	}
	hour := time.Now().Hour()
	for _, v := range config.Rooms.Rooms[0].RobotConfig {
		if gs.isBetween(hour, v.HourStart, v.HourEnd) {
			min, max = v.GoldMin, v.GoldMax
			return
		}
	}
	min, max = config.Rooms.Rooms[0].RobotConfig[0].GoldMin, config.Rooms.Rooms[0].RobotConfig[0].GoldMax
	return
}

func (gs *gamesink) GetRobotOnlineSec() int {
	if len(config.Rooms.Rooms[0].RobotConfig) == 0 {
		return 0
	}
	hour := time.Now().Hour()
	for _, v := range config.Rooms.Rooms[0].RobotConfig {
		if gs.isBetween(hour, v.HourStart, v.HourEnd) {
			return v.Online
		}
	}
	return config.Rooms.Rooms[0].RobotConfig[0].Online
}

func (gs *gamesink) GetRoomDatas() []string {
	var ret []string
	for _, v := range config.Rooms.Rooms {
		ret = append(ret, v.RoomName)
	}
	return ret
}

func (gs *gamesink) GetMinRoomCount(roomData string) int {
	for _, v := range config.Rooms.Rooms {
		if roomData == v.RoomName {
			return v.MinRoom
		}
	}
	return 0
}

func (gs *gamesink) GetRoomRobotCount(roomName string) int {
	for _, v := range config.Rooms.Rooms {
		if roomName != v.RoomName {
			continue
		}
		hour := time.Now().Hour()
		for _, v1 := range v.RobotConfig {
			if gs.isBetween(hour, v1.HourStart, v1.HourEnd) {
				return gs.getRandom(v1.CountMin, v1.CountMax)
			}
		}
	}

	return 0
}

func (gs *gamesink) GetRoomRobotGoldLimit(roomName string) (min, max int) {
	for _, v := range config.Rooms.Rooms {
		if roomName != v.RoomName {
			continue
		}
		hour := time.Now().Hour()
		for _, v1 := range v.RobotConfig {
			if gs.isBetween(hour, v1.HourStart, v1.HourEnd) {
				return v1.GoldMin, v1.GoldMax
			}
		}
	}
	return 0, 0
}

func (gs *gamesink) IsChipRoom() bool {
	return config.Server.IsChipRoom
}

func (gs *gamesink) OnPlatformConfig(key string) {
	config.OnConfigChanged(key)
}

func (gs *gamesink) IsPrivateRoom() bool {
	return config.Server.IsPrivateRoom
}

func (gs *gamesink) GetChairCount() int {
	return 1
}

func (gs *gamesink) getRandom(min, max int) int {
	if min >= max {
		return min
	}
	return min + rand.Intn(max-min)
}

func (gs gamesink) isBetween(p, min, max int) bool {
	return p >= min && p <= max
}

func (gs *gamesink) GetRoomLevel(data string) int {
	return 0
}
