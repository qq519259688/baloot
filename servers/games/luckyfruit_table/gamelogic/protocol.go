package gamelogic

const (
	Broadcast      = "broadcast"
	TotalResult    = "totalresult"
	Option         = "Option"
	Result         = "result"
	Config         = "config"
	History        = "history"
	Bet            = "bet"
	BatchBet       = "batchbet"
	BetClear       = "betclear"
	Betfailed      = "betfailed"
	BetRecord      = "betrecord"
	BetRank        = "betrank"        // 投注排行榜
	BetList        = "betlist"        // 投注列表
	FreeChip       = "freechip"       // 检查免费筹码
	FreeBet        = "freebet"        // 免费投注
	FreeChipChange = "freechipchange" // 免费卷变化

)
