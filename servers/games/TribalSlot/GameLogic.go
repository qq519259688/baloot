package TribalSlot

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"sort"
	"sync"
	"time"

	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/games/slotcommon"
	"bet24.com/servers/games/slotcommon/slotcount"
	"bet24.com/servers/games/slotcommon/usermanager"
)

type GameLogic struct {
	MinBet           int
	MaxBet           int
	MinJackpot       int64
	MaxJackpot       int64
	MaxNoneFreeCount int
	Slots            []Slot
	WinShpes         []WinShape
	Wins             []Win
	JackPotInfo      JackPot
	FreeSpin         FreeSpinInfo
	Special          SpecialMultple
	TaxRate          int
	AdBets           []FafafaSlot_FreeAdBet

	lock          *sync.RWMutex
	JackPotAmount int

	//origin_slots []Slot
	//Test         []TestSlots

	//SlotCounts  []SlotCount_Rate
	//defaultRate int // 默认的赔率

	//userFreeSpins map[int]*userFreeSpin

	slotSink slotcommon.SlotSink

	slotCommon       *slotcommon.Slotcommon
	multipleCountMgr *slotcount.MultipleSlotCountManager
}

func NewGameLogic(slotSink slotcommon.SlotSink) *GameLogic {
	obj := new(GameLogic)
	obj.lock = &sync.RWMutex{}
	obj.MinJackpot = 1000000
	obj.MaxJackpot = 100000000
	obj.slotSink = slotSink
	return obj
}

func (this *GameLogic) run() {
	log.Color(LogColor, "TribalSlot GameLogic.run")
	rand.Seed(time.Now().UnixNano())
	this.refreshData()
	this.slotCommon = slotcommon.NewSlotCommon(this.slotSink, GAMEID, GAME_NAME, this.TaxRate, GAME_MESSAGE)
}

func (this *GameLogic) refreshData() {
	go this.initData()
	time.AfterFunc(5*time.Minute, this.refreshData)
}

func (this *GameLogic) initData() {
	data, err := os.ReadFile("slotconf/TribalSlot.json")
	if err != nil {
		log.Error("read TribalSlot.json failed")
	}
	this.lock.Lock()
	err = json.Unmarshal(data, &this)
	if err != nil {
		log.Error("Unmarshal TribalSlot.json failed err:%v", err)
		this.lock.Unlock()
		return
	}
	// 初始化所有的格字
	this.multipleCountMgr = slotcount.NewMultipleSlotCountManager("tribalslotcount_mul")
	// 把特殊玩法的概率排序
	this.Special.sortByMultiple()
	log.Color(LogColor, "%v", this.Special)
	this.FreeSpin.sort()
	log.Color(LogColor, "%v", this.FreeSpin)
	this.lock.Unlock()

	if this.MaxNoneFreeCount > 0 {
		usermanager.SetMaxNoneFreeCount(GAMEID, this.MaxNoneFreeCount)
	}

	this.JackPotAmount = coreservice.GetJackpotAmount(0, this.slotSink.IsChipRoom())
	sort.Slice(this.AdBets, func(i, j int) bool {
		return this.AdBets[i].AdCount > this.AdBets[j].AdCount
	})
}

func (this *GameLogic) getUserSlotCount(userId int, betAmount int) *slotcount.SlotCountManager {
	level := usermanager.GetUserReturnLevel(userId, GAMEID, betAmount)
	return this.multipleCountMgr.GetMgr(level)
}

func (this *GameLogic) getSlot(slotID int) Slot {
	for _, v := range this.Slots {
		if slotID == v.SlotID {
			return v
		}
	}
	log.Release("GameLogic.getSlot failed %d", slotID)
	var slot Slot
	return slot
}

func (this *GameLogic) get15Slots(userId int, isFree bool, betAmount int) []Slot {
	this.lock.RLock()
	defer this.lock.RUnlock()
	sc := this.getUserSlotCount(userId, betAmount)
	ret := make([]Slot, RESULT_COUNT)
	for i := 0; i < 15; {
		slotId := sc.GetOneSlot(0, i)
		if isFree && slotId == this.FreeSpin.SlotID {
			continue
		}
		ret[i] = this.getSlot(slotId)
		i++
	}
	return ret
}

func (this *GameLogic) getOneResult(slotID, slotCount, shapeID int) *Result {
	for _, v := range this.Wins {
		if v.SlotID != slotID {
			continue
		}
		for _, rate := range v.Rates {
			if rate.Count == slotCount {
				return &Result{SlotID: slotID, SlotCount: slotCount, WinShapeID: shapeID, WinRate: rate.Win}
			}
		}
	}
	return nil
}

func (this *GameLogic) getResult(userId int, betAmount int, isFree bool, controlType int) TribalSlot_Result {
	if isFree {
		controlType = 0
	}

	var ret TribalSlot_Result

	if betAmount <= 0 {
		log.Release("TribalSlot.GameLogic.GetResult betAmount = %d", betAmount)
		return ret
	}
	ret.Slots = make([]int, RESULT_COUNT)
	ret.BetAmount = betAmount
	// 取15个result
	slots := this.get15Slots(userId, isFree, betAmount)
	for k, v := range slots {
		ret.Slots[k] = v.SlotID
	}

	// 计算结果
	shapeCount := len(this.WinShpes)
	for k, v := range this.WinShpes {
		// 查看每条连线的数量
		slotID, slotCount, magicCount := v.getCount(slots)
		// 查看结果
		result := this.getOneResult(slotID, slotCount, k)
		if result != nil {
			// 如果中奖池了
			if result.WinRate == -1 {
				// jackpot不能有wild
				if magicCount == 0 {
					ret.Jackpot.WinShapeID = k
					ret.Jackpot.Amount = this.JackPotInfo.getBonus(betAmount, this.JackPotAmount)
				}
			} else {
				// 中奖了
				ret.WinAmount += betAmount * result.WinRate / shapeCount
				ret.Lines = append(ret.Lines, *result)
			}
		}
	}

	// 是否有特殊玩法
	ret.Special = this.Special.getSpecialResult(slots)

	ret.FreeSpin = this.FreeSpin.getFreeTime(slots)

	specialMultiple := ret.Special.WinRate1 + ret.Special.WinRate2
	if controlType == 1 && (ret.WinAmount > betAmount || ret.FreeSpin > 0 || specialMultiple > 1.0) {
		return this.getResult(userId, betAmount, isFree, controlType)
	}
	if controlType == 2 && ret.WinAmount < betAmount && ret.FreeSpin == 0 && specialMultiple < 1.0 {
		return this.getResult(userId, betAmount, isFree, controlType)
	}

	if ret.FreeSpin > 0 {
		this.addFreeSpin(userId, ret.FreeSpin, betAmount, false)
	}
	return ret
}

func (this *GameLogic) getJackPotAmount() int {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.JackPotAmount
}

func (this *GameLogic) getSlots() []Slot {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.Slots
}

func (this *GameLogic) getWinShapes() []WinShape {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.WinShpes
}

func (this *GameLogic) useFreeSpin(userId int) (bool, int, bool) {
	return this.slotCommon.UseFreeSpin(userId)
}

func (this *GameLogic) getFreeSpinTime(userId int) int {
	return this.slotCommon.GetFreeSpinTime(userId)
}

func (this *GameLogic) userExit(userId int) {
	this.slotCommon.OnUserExit(userId)
}

func (this *GameLogic) getBetDesc(betAmount int) string {
	return fmt.Sprintf("bet %d", betAmount)
}

func (this *GameLogic) getResultDesc(result TribalSlot_Result) string {
	var total struct {
		Slots   []int
		Lines   []int
		Special int
		Jackpot int
	}
	total.Slots = result.Slots
	for _, v := range result.Lines {
		total.Lines = append(total.Lines, v.WinShapeID)
	}
	total.Jackpot = result.Jackpot.Amount
	total.Special = result.BetAmount * (result.Special.WinRate1 + result.Special.WinRate2)
	data, _ := json.Marshal(total)
	return string(data)
}

func (this *GameLogic) modifyJackpot(amount int, userId int) {
	newAmount := coreservice.ModifyJackpot(0, amount, userId, GAME_NAME, this.slotSink.IsChipRoom())
	this.lock.Lock()
	this.JackPotAmount = newAmount
	this.lock.Unlock()
	this.sendTribalSlotJackpot(userId)
}
func (this *GameLogic) addFreeSpin(userId int, freeCount, bet int, fromAd bool) {
	this.slotCommon.AddFreeSpin(userId, freeCount, bet, fromAd)
}
