package TribalSlot

import (
	_ "bet24.com/log"
)

type WinRate struct {
	Count int // 数量
	Win   int // 赔率
}

type Win struct {
	SlotID int // SlotID
	Rates  []WinRate
}
