package TribalSlot

import (
	"sort"
)

type FreeSpinTime struct {
	Count int // 匹配数量
	Time  int // 免费次数
}

type FreeSpinInfo struct {
	SlotID int
	Time   []FreeSpinTime
}

func (f *FreeSpinInfo) getFreeTime(slots []Slot) int {
	freeSlotCount := 0
	for _, v := range slots {
		if v.SlotID == f.SlotID {
			freeSlotCount++
		}
	}
	for _, v := range f.Time {
		if v.Count <= freeSlotCount {
			return v.Time
		}
	}

	return 0
}

func (f *FreeSpinInfo) sort() {
	sort.Slice(f.Time, func(i, j int) bool {
		return f.Time[i].Time > f.Time[j].Time
	})
}
