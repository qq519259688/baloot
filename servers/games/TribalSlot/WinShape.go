package TribalSlot

import (
	"bet24.com/log"
)

func NewWinShape(shape int, desc string) *WinShape {
	ws := new(WinShape)
	ws.Shape = shape
	ws.Desc = desc
	if !ws.isValid() {
		return nil
	}

	return ws
}

type WinShape struct {
	Shape int
	Desc  string
}

func (ws *WinShape) isValid() bool {
	shape := ws.Shape
	for i := 0; i < COLUMN_COUNT; i++ {
		if shape%10 >= ROW_COUNT {
			log.Release("WinShape.IsValid r%d = %d", i, shape%10)
			return false
		}
		shape = shape / 10
	}
	return true
}

func (ws *WinShape) getFive(slots []Slot) []Slot {
	ret := make([]Slot, COLUMN_COUNT)
	shape := ws.Shape
	for i := 1; i <= COLUMN_COUNT; i++ {
		var row = shape % 10
		ret[COLUMN_COUNT-i] = slots[(row+1)*COLUMN_COUNT-i]
		shape = shape / 10
	}
	return ret
}

func (ws *WinShape) getCount(slots []Slot) (slotID, slotCount, magicCount int) {
	if len(slots) != RESULT_COUNT {
		log.Release("WinShape.isWin count = %d", len(slots))
		return 0, 0, 0
	}
	five_slot := ws.getFive(slots)
	//log.Color(LogColor,"WinShape.getCount,%d,%v", ws.Shape, five_slot)
	// 先计算SlotID和Count
	slotID = 0xFFFFFFFF
	slotCount = 0
	magicCount = 0
	magicSlotID := 0xFFFFFFFF
	for _, v := range five_slot {
		if v.isMagic() {
			slotCount++
			magicSlotID = v.SlotID
			magicCount++
			continue
		}
		if slotID != v.SlotID && slotID != 0xFFFFFFFF {
			break
		}
		slotCount++
		slotID = v.SlotID
	}
	if slotID == 0xFFFFFFFF {
		slotID = magicSlotID
	}
	// log.Color(LogColor, "WinShape.getCount ret,%d,%d", slotID, slotCount)
	return
}
