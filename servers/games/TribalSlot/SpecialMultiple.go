package TribalSlot

import (
	_ "bet24.com/log"
	"math/rand"
	"sort"
)

type MultipleInfo struct {
	Multiple int
	Percent  int
}

type Normal_Multiple struct {
	SlotCount int
	Multiple  int
	IsSpecial int
}

type SpecialMultple struct {
	SlotID          int
	NormalMultiple  []Normal_Multiple
	MultiplePercent []MultipleInfo
}

func (s *SpecialMultple) getSpecialResult(slots []Slot) SpecialResult {
	var ret SpecialResult
	specialSlotCount := 0
	for _, v := range slots {
		if v.SlotID == s.SlotID {
			specialSlotCount++
		}
	}
	normalMultiple := 0
	isSpecial := 0
	for _, v := range s.NormalMultiple {
		if v.SlotCount <= specialSlotCount && v.Multiple > normalMultiple {
			normalMultiple = v.Multiple
			isSpecial = v.IsSpecial + isSpecial
		}
	}
	//log.Color(LogColor, "getSpecialResult specialSlotCount = %d,normalMultiple = %d,isSpecial = %d", specialSlotCount, normalMultiple, isSpecial)
	if normalMultiple == 0 {
		return ret
	}
	ret.SlotID = s.SlotID
	ret.SlotCount = specialSlotCount
	ret.WinRate1 = normalMultiple
	ret.WinRate2 = 0
	if isSpecial == 0 {
		return ret
	}
	// 取10000的模数，从高倍率往低取
	r := rand.Intn(10000)
	for _, v := range s.MultiplePercent {
		if r <= v.Percent {
			ret.WinRate2 = v.Multiple
			break
		}
		r -= v.Percent
	}
	return ret
}

func (s *SpecialMultple) sortByMultiple() {
	sort.Slice(s.MultiplePercent, func(i, j int) bool {
		return s.MultiplePercent[i].Multiple > s.MultiplePercent[j].Multiple
	})
}
