package proto

import (
	"context"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "cardlibrary"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name          string
	LibraryType   int
	ControlChair  int
	FirstOutChair int
	Doublings     int
}

type Response struct {
	Data         string
	RetCode      int
	Success      bool
	ControlCards []int
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("waterpool failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

// 牌库类型
const (
	Baloot_LibraryType_Scenario          = iota // 剧情牌
	Baloot_LibraryType_Invincible               // 无敌牌
	Baloot_LibraryType_HighScore                // 高分牌
	Baloot_LibraryType_Doubling                 // 加倍场牌
	Baloot_LibraryType_NewUser                  // 新手牌
	Baloot_LibraryType_Doubling_Scenario        // 加倍场多项目体验牌
)

func GetControlCards(libraryType, controlChair, firstOutCard, doublings int) (bool, []int) {
	xclient := getClient()

	args := &Request{
		LibraryType:   libraryType,
		ControlChair:  controlChair,
		FirstOutChair: firstOutCard,
		Doublings:     doublings,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetControlCards", args, reply)
	if err != nil {
		log.Debug("cardlibrary failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.Success, reply.ControlCards
}
