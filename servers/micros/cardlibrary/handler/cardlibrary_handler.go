package handler

import (
	"context"
	"fmt"

	"bet24.com/servers/micros/cardlibrary/handler/gamecardlibrary"
	pb "bet24.com/servers/micros/cardlibrary/proto"
)

var instance *CardLibrary

type CardLibrary struct {
}

func GetInstance() *CardLibrary {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func newHandler() *CardLibrary {
	ret := new(CardLibrary)
	ret.ctor()
	return ret
}

func (h *CardLibrary) ctor() {
	gamecardlibrary.Run()
}

func Dump(cmd, param string) {
	gamecardlibrary.Dump(cmd, param)
}

// interfaces
func (h *CardLibrary) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (h *CardLibrary) GetControlCards(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success, rsp.ControlCards = gamecardlibrary.GetControlCards(req.LibraryType, req.ControlChair, req.FirstOutChair, req.Doublings)
	return nil
}
