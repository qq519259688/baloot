package main

import (
	"log"

	slog "bet24.com/log"
	pb "bet24.com/servers/micros/cardlibrary/proto"
)

func main() {
	logger, err := slog.New("debug", "debug", "log/client", log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	slog.Debug(pb.SayHello("client"))
}
