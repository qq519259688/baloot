package main

import (
	"fmt"

	_ "bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/micros/cardlibrary/config"
	"bet24.com/servers/micros/cardlibrary/handler"
	pb "bet24.com/servers/micros/cardlibrary/proto"
	"bet24.com/servers/micros/common"
)

func main() {
	config.Run(pb.ServiceName)
	go waitInput()
	redis.InitPool(config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB)
	common.RunService(pb.ServiceName, handler.GetInstance(), fmt.Sprintf("localhost:%d", config.Server.ConsulPort))
}

func waitInput() {
	for {
		var cmd string
		var param string
		fmt.Scanf("%s %s %s", &cmd, &param)
		switch cmd {
		case "exit":
			common.DeregisterService(pb.ServiceName)
		default:
			handler.Dump(cmd, param)
		}
	}
}
