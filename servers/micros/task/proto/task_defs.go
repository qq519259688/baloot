package proto

import (
	"bet24.com/servers/common"
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	PreAddictionTaskId = 610800
	PreAddcitionTime   = 10800
)

const (
	_                               = iota
	TaskAction_login                // 1=登录
	TaskAction_earn                 // 2=赚取金币
	TaskAction_wingame              // 3=赢一把游戏
	TaskAction_pay                  // 4=充值
	TaskAction_fire                 // 5=游戏消耗金币
	TaskAction_play_time            // 6=添加在线时长
	TaskAction_use_item             // 7=使用道具、技能
	TaskAction_enter_room           // 8=进入房间
	TaskAction_day_vitality         // 9=天活跃度
	TaskAction_week_vitality        // 10=周活跃度
	TaskAction_achievement          // 11=成就
	TaskAction_playgame             // 12=参与游戏
	TaskAction_worldchat            // 13=世界聊天
	TaskAction_gameinteract         // 14=游戏互动道具
	TaskAction_addfriend            // 15=添加好友
	TaskAction_shareplatform        // 16=平台分享
	TaskAction_sharegame            // 17=游戏分享
	TaskAction_gift                 // 18=赠送礼物
	TaskAction_privatechat          // 19=发私聊
	TaskAction_playmatch            // 20=参加比赛
	TaskAction_matchpromote         // 21=比赛晋级
	TaskAction_matchfinal           // 22=比赛进入最后一轮
	TaskAction_matchchampion        // 23=比赛获得冠军
	TaskAction_exchange_item        // 24=购买道具
	TaskAction_novice_guidance      // 25=新手引导
	TaskAction_playSNG              // 26=进行sng比赛
	TaskAction_playComboMatch       // 27=进行实物赛
	TaskAction_upMicDuration        // 28=上麦时长
	TaskAction_diamondGift          // 29=赠送钻石礼物
	TaskAction_sendGivingDiamond    // 30=赠送钻石礼物价值
	TaskAction_receiveGivingDiamond // 31=收到钻石礼物价值
	TaskAction_roomChat             // 32=在房间聊天
	TaskAction_betWin               // 33=投注赢取，不扣减投注额

	TaskAction_gameaction = 50 // 游戏条件，不通的游戏可定义不同的内容，可扩充。配合TaskScop
)

const (
	TaskStatus_active   = iota // 任务进行中
	TaskStatus_complete        // 完成
	TaskStatus_awarded         // 已领取奖励
	TaskStatus_inactive        // 未开始
	TaskStatus_expired         // 已过期
)

var status_desc = []string{"ACTIVE", "comple", "awarde", "inacti", "expire"}

const (
	TaskDuration_Daily  = -1 // 每日任务
	TaskDuration_Weekly = -2 // 每周任务
)

var duration_desc = []string{"daily", "weekly"}

const (
	TaskScene_None             = iota   // 不显示
	TaskScene_Hall             = 1      // 1=大厅
	TaskScene_Game             = 10     // 10=游戏
	TaskScene_AudioRoom        = 100    // 100=语聊房任务
	TaskScene_Agent            = 1000   // 1000=代理会员任务
	TaskScene_DailyWheel       = 10000  // 每日转盘任务
	TaskScene_HighlyProfitable = 100000 // 一本万利任务
)

type Task struct {
	Id          int
	Scene       int
	Title       string
	Desc        string
	Action      int
	Target      int
	Repeatable  bool // 是否可重复
	Duration    int  // 任务有效期
	PreTask     int  `json:",omitempty"` // 如果不为0，则需要完成前置任务才创建，否则自动创建
	Awards      []item.ItemPack
	Extra       string // 预留跳转描述
	Task_Period        // 如果是限时开放的任务，则在这里添加时效
	TaskScope          // 是否限制游戏
}

// 任务的适用范围
type TaskScope struct {
	GameNames  []string `json:",omitempty"` // 游戏ID范围
	GameName   string   `json:",omitempty"` // 应用调用时传入参数
	RankCrdate string   `json:",omitempty"` // 榜单的创建开始日期
}

func (t *Task) IsAplicable(param TaskScope) bool {
	if len(t.GameNames) == 0 {
		return true
	}
	for _, v := range t.GameNames {
		if v == param.GameName {
			return true
		}
	}
	return false
}

func (t *Task) IsOpen() bool {
	if t.Start == 0 {
		return true
	}
	now := common.GetTimeStamp()
	return now >= t.Start && now <= (t.Start+t.Duration)
}

type UserTask struct {
	TaskId      int
	Scheduled   int // 完成进度
	Status      int
	Task_Period // 任务对玩家的时效，时效内已完成，如果没有后置任务，则不重置，等时效结束后再创建
}

type Task_Period struct {
	Start    int `json:",omitempty"` // 开始时间戳 秒 0表示无时效
	Duration int `json:",omitempty"` // 持续时间 秒
}

func GetTaskStatusDesc(status int) string {
	if status < len(status_desc) && status >= 0 {
		return status_desc[status]
	}
	return "unknown"
}

func GetTaskDurationDesc(duration int) string {
	duration = duration*-1 - 1
	if duration < len(duration_desc) && duration >= 0 {
		return duration_desc[duration]
	}
	return "unknown"
}
