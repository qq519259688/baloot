package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	"context"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "task"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name      string
	UserId    int
	Count     int
	Scene     int
	IsRefresh bool
	Tasks     []int
}

type Response struct {
	Data    string
	Success bool
	Items   []item.ItemPack
	Scene   int
	TaskId  int
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

func AddUser(userId int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "AddUser", args, nil)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func RemoveUser(userId int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "RemoveUser", args, nil)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

type Response_Tasks struct {
	Tasks []*UserTask
}

func GetUserTasks(userId int) []*UserTask {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response_Tasks{}

	err := xclient.Call(context.Background(), "GetUserTasks", args, reply)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.Tasks
}

type Request_Task struct {
	TaskId int
}

type Response_Task struct {
	Task *Task
}

func GetSysTask(taskId int) *Task {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_Task{
		TaskId: taskId,
	}

	reply := &Response_Task{}

	err := xclient.Call(context.Background(), "GetSysTask", args, reply)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.Task
}

type Response_SysTasks struct {
	Tasks map[int]*Task
}

func GetSysTaskList() map[int]*Task {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{}

	reply := &Response_SysTasks{}

	err := xclient.Call(context.Background(), "GetSysTaskList", args, reply)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.Tasks
}

type Request_Action struct {
	UserId   int
	Action   int
	Progress int
	Param    TaskScope
}

func DoTaskAction(userId int, action, progress int, param TaskScope) (bool, int) {
	if userId == 0 {
		return false, 0
	}
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_Action{
		UserId:   userId,
		Action:   action,
		Progress: progress,
		Param:    param,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "DoTaskAction", args, reply)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false, 0
	}
	return reply.Success, reply.TaskId
}

func IsActionTaskActive(userId int, action int, gameName string) bool {
	if userId == 0 {
		return false
	}
	xclient := getClient()

	args := &Request_Action{
		UserId: userId,
		Action: action,
		Param:  TaskScope{GameName: gameName},
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "IsActionTaskActive", args, reply)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.Success
}

func RemoveTaskByAction(userId int, action int, gameName string) {
	if userId == 0 {
		return
	}
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_Action{
		UserId: userId,
		Action: action,
		Param:  TaskScope{GameName: gameName},
	}

	err := xclient.Call(context.Background(), "RemoveTaskByAction", args, nil)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

type Request_Award struct {
	UserId int
	TaskId int
}

func AwardTask(userId int, taskId int) (bool, string) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_Award{
		UserId: userId,
		TaskId: taskId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "AwardTask", args, reply)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false, "rpc call failed"
	}
	return reply.Success, reply.Data
}

func AwardTaskWithItems(userId int, taskId int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_Award{
		UserId: userId,
		TaskId: taskId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "AwardTaskWithItems", args, reply)
	if err != nil {
		log.Debug("AwardTaskWithItems task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func IsTriggerPreAddiction(userId int) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "IsTriggerPreAddiction", args, reply)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.Success
}

func RefreshTask(userId int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "RefreshTask", args, nil)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return
	}
}

func GetTaskTipScene(userId int) int {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetTaskTipScene", args, reply)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.Scene
}

func AwardAllTask(userId int) []item.ItemPack {
	xclient := getClient()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "AwardAllTask", args, reply)
	if err != nil {
		log.Debug("task.AwardAllTask failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}

	return reply.Items
}

func CreateRandomTasksByScene(userId int, scene int, count int, isRefresh bool) {
	xclient := getClient()

	args := &Request{
		UserId:    userId,
		Scene:     scene,
		Count:     count,
		IsRefresh: isRefresh,
	}

	err := xclient.Call(context.Background(), "CreateRandomTasksByScene", args, nil)
	if err != nil {
		log.Debug("task.CreateRandomTasksByScene failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func IsAllTaskFinished(userId int, scene int) bool {
	xclient := getClient()

	args := &Request{
		UserId: userId,
		Scene:  scene,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "IsAllTaskFinished", args, reply)
	if err != nil {
		log.Debug("task.IsAllTaskFinished failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}

	return reply.Success
}

func CreateUserTasks(userId int, taskIds []int) bool {
	xclient := getClient()
	args := &Request{
		UserId: userId,
		Tasks:  taskIds,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "CreateUserTasks", args, reply)
	if err != nil {
		log.Debug("task.CreateUserTasks failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.Success
}

func IsTasksFinished(userId int, taskIds []int) bool {
	xclient := getClient()
	args := &Request{
		UserId: userId,
		Tasks:  taskIds,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "IsTasksFinished", args, reply)
	if err != nil {
		log.Debug("task.IsTasksFinished failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.Success
}

func GetTasksRewards(taskIds []int) []item.ItemPack {
	xclient := getClient()
	args := &Request{
		Tasks: taskIds,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetTasksRewards", args, reply)
	if err != nil {
		log.Debug("task.GetTasksRewards failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.Items
}

func ClaimTaskRewards(userId int, taskIds []int) []item.ItemPack {
	xclient := getClient()
	args := &Request{
		Tasks:  taskIds,
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "ClaimTaskRewards", args, reply)
	if err != nil {
		log.Debug("task.ClaimTaskRewards failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.Items
}
