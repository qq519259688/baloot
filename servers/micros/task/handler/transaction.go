package handler

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/task/proto"
	"encoding/json"
	"runtime/debug"
)

// 系统任务数值列表
func getSysTaskList() map[int]*pb.Task {
	defer func() {
		if err := recover(); err != nil {
			log.Error("task.getSysTaskList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Task_GetList")
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)

	tasks := make(map[int]*pb.Task)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out pb.Task
		out.Id = int((ret[0]).(int64))
		out.Title = (ret[1]).(string)
		out.Desc = (ret[2]).(string)
		out.Action = int((ret[3]).(int64))
		out.Target = int((ret[4]).(int64))
		if repeatable := int((ret[5]).(int64)); repeatable == 1 {
			out.Repeatable = true
		}
		out.Duration = int((ret[6]).(int64))
		out.PreTask = int((ret[7]).(int64))
		out.Start = int((ret[8]).(int64))
		out.Duration = int((ret[9]).(int64))
		awards := (ret[10]).(string)
		if awards != "" {
			if err := json.Unmarshal([]byte(awards), &out.Awards); err != nil {
				log.Error("task.getSysTaskList unmarshal err %v", err)
				return nil
			}
		}
		out.Scene = int((ret[11]).(int64))
		scope := (ret[12]).(string)
		var ts pb.TaskScope
		if scope != "" {
			if err := json.Unmarshal([]byte(scope), &ts); err != nil {
				log.Error("task.getSysTaskList unmarshal err %v", err)
				return nil
			}
		}
		out.TaskScope = ts
		tasks[out.Id] = &out
	}

	return tasks
}

// 用户任务列表
func getUserTaskList(userId int) map[int]*pb.UserTask {
	defer func() {
		if err := recover(); err != nil {
			log.Error("task.getSysTaskList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserTask_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)

	userTasks := make(map[int]*pb.UserTask)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out pb.UserTask
		out.TaskId = int((ret[0]).(int64))
		out.Scheduled = int((ret[1]).(int64))
		out.Status = int((ret[2]).(int64))
		startStr := (ret[3]).(string)
		startTime := common.ParseTime(startStr)
		out.Task_Period.Start = common.GetStamp(startTime)
		out.Task_Period.Duration = int((ret[4]).(int64))
		userTasks[out.TaskId] = &out
	}
	return userTasks
}

// 更新用户任务
func updateUserTask(userId int, userTask *pb.UserTask) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserTask_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@TaskID", database.AdParamInput, database.AdInteger, 4, userTask.TaskId)
	statement.AddParamter("@Scheduled", database.AdParamInput, database.AdInteger, 4, userTask.Scheduled)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, userTask.Status)
	statement.AddParamter("@Start", database.AdParamInput, database.AdInteger, 4, userTask.Task_Period.Start)
	statement.AddParamter("@Duration", database.AdParamInput, database.AdInteger, 4, userTask.Task_Period.Duration)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	dbengine.Execute(sqlstring)
	return
}

// 删除任务
func removeUserTask(userId int, taskId int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UserTask_Remove")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@TaskID", database.AdParamInput, database.AdInteger, 4, taskId)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	dbengine.Execute(sqlstring)
	return
}
