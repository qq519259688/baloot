package handler

import (
	"encoding/json"
	"os"
	"time"

	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
	pb "bet24.com/servers/micros/task/proto"
)

const config_key = "task_config"
const refresh_config_sec = 600

func (tm *taskmgr) loadSysTaskFromJson() bool {
	time.AfterFunc(refresh_config_sec*time.Second, func() {
		tm.loadSysTaskFromJson()
	})
	configString := platformconfig.GetConfig(config_key)
	if configString == "" {
		data, err := os.ReadFile("serviceconf/systemtask.json")
		if err != nil {
			return false
		}
		configString = string(data)
		platformconfig.SetConfig(config_key, configString)
	} else {
		log.Release("task.taskmgr loading config from redis")
	}

	if configString == tm.lastConfig {
		return false
	}
	tm.lastConfig = configString

	var list []pb.Task

	err := json.Unmarshal([]byte(configString), &list)
	if err != nil {
		log.Release("taskmgr.loadSysTaskFromJson Unmarshal task failed err:%v", err)
		return false
	}

	if len(list) == 0 {
		log.Release("taskmgr.loadSysTaskFromJson Unmarshal task failed err:%v", err)
		return false
	}

	tm.sys_lock.Lock()
	for k, v := range list {
		tm.sys_tasklist[v.Id] = &list[k]
	}
	tm.sys_lock.Unlock()

	return true
}
