package handler

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/task/proto"
	robot "bet24.com/servers/micros/userservices/proto"
	"context"
	"fmt"
)

var instance *Task

func GetInstance() *Task {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *Task {
	ret := new(Task)
	ret.ctor()
	return ret
}

var mgr *taskmgr

type Task struct {
}

func (h *Task) ctor() {
	mgr = newTaskMgr()

}

func (d *Task) dump(cmd, param1, param2 string) {
	//log.Debug("DbEngine.Dump %s,%s,%s", cmd, param1, param2)
	switch cmd {
	case "sys":
		mgr.dumpSys(param1)
	case "user":
		mgr.dumpUser(param1)
	default:
		log.Release("Task.Dump unhandled cmd %s", cmd)
	}
}

func (h *Task) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (h *Task) AddUser(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	mgr.onUserEnter(req.UserId)
	return nil
}

func (h *Task) RemoveUser(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	mgr.onUserExit(req.UserId)
	return nil
}

func (h *Task) GetUserTasks(ctx context.Context, req *pb.Request, rsp *pb.Response_Tasks) error {
	rsp.Tasks = mgr.getUserTasks(req.UserId)
	return nil
}

func (h *Task) GetSysTask(ctx context.Context, req *pb.Request_Task, rsp *pb.Response_Task) error {
	rsp.Task = mgr.getSysTask(req.TaskId)
	return nil
}

func (h *Task) GetSysTaskList(ctx context.Context, req *pb.Request, rsp *pb.Response_SysTasks) error {
	rsp.Tasks = mgr.getSysTaskList()
	return nil
}

func (h *Task) DoTaskAction(ctx context.Context, req *pb.Request_Action, rsp *pb.Response) error {
	if robot.IsRobot(req.UserId) {
		return nil
	}
	rsp.Success, rsp.TaskId = mgr.doTaskAction(req.UserId, req.Action, req.Progress, req.Param)
	return nil
}

func (h *Task) IsActionTaskActive(ctx context.Context, req *pb.Request_Action, rsp *pb.Response) error {
	if robot.IsRobot(req.UserId) {
		return nil
	}
	rsp.Success = mgr.isActionTaskActive(req.UserId, req.Action, req.Param)
	return nil
}

func (h *Task) RemoveTaskByAction(ctx context.Context, req *pb.Request_Action, rsp *pb.Response) error {
	mgr.removeTaskByAction(req.UserId, req.Action, req.Param)
	return nil
}

func (h *Task) AwardTask(ctx context.Context, req *pb.Request_Award, rsp *pb.Response) error {
	rsp.Success, rsp.Data = mgr.awardTask(req.UserId, req.TaskId)
	return nil
}

func (h *Task) AwardTaskWithItems(ctx context.Context, req *pb.Request_Award, rsp *pb.Response) error {
	rsp.Data = mgr.awardTaskWithItems(req.UserId, req.TaskId)
	return nil
}

func (h *Task) IsTriggerPreAddiction(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = mgr.isTriggerPreAddiction(req.UserId)
	return nil
}

func (h *Task) RefreshTask(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	mgr.refreshTask(req.UserId)
	return nil
}

func (h *Task) GetTaskTipScene(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Scene = mgr.getTaskTipScene(req.UserId)
	return nil
}

func (h *Task) AwardAllTask(ctx context.Context, req *pb.Request_Award, rsp *pb.Response) error {
	rsp.Items = mgr.awardAllTask(req.UserId)
	return nil
}

func (h *Task) CreateRandomTasksByScene(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	mgr.createRandomTasksByScene(req.UserId, req.Scene, req.Count, req.IsRefresh)
	return nil
}

func (h *Task) IsAllTaskFinished(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = mgr.isAllTaskFinished(req.UserId, req.Scene)
	return nil
}

func (h *Task) CreateUserTasks(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = mgr.createUserTasks(req.UserId, req.Tasks)
	return nil
}

func (h *Task) IsTasksFinished(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = mgr.isTasksFinished(req.UserId, req.Tasks)
	return nil
}

func (h *Task) GetTasksRewards(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Items = mgr.getTasksRewards(req.Tasks)
	return nil
}

func (h *Task) ClaimTaskRewards(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Items = mgr.claimTaskRewards(req.UserId, req.Tasks)
	return nil
}
