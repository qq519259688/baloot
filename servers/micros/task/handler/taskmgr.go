package handler

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	activityservice "bet24.com/servers/micros/activityservice/proto"
	badge "bet24.com/servers/micros/badge/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	pb "bet24.com/servers/micros/task/proto"
	"encoding/json"
	"strconv"
	"sync"
	"time"
)

type taskmgr struct {
	sys_lock  *sync.RWMutex
	user_lock *sync.RWMutex

	sys_tasklist  map[int]*pb.Task
	user_tasklist map[int]*user_task

	lastConfig string
}

func newTaskMgr() *taskmgr {
	ret := new(taskmgr)
	ret.sys_lock = &sync.RWMutex{}
	ret.user_lock = &sync.RWMutex{}
	ret.sys_tasklist = make(map[int]*pb.Task)
	ret.user_tasklist = make(map[int]*user_task)
	ret.loadSysTaskList()
	ret.checkDayRefresh()
	return ret
}

func (tm *taskmgr) checkDayRefresh() {
	ticker := time.NewTicker(10 * time.Minute)
	go func(t *time.Ticker) {
		lastCheck := common.GetTimeStamp()
		for { //循环
			select {
			case <-t.C:
				now := common.GetTimeStamp()
				if !common.IsSameDay(lastCheck, now) {
					tm.refreshUsers()
				}
				lastCheck = now
			}
		}
	}(ticker)
}

func (tm *taskmgr) refreshUsers() {
	var userIds []int
	tm.user_lock.RLock()
	for k := range tm.user_tasklist {
		userIds = append(userIds, k)
	}
	tm.user_lock.RUnlock()

	for _, v := range userIds {
		tm.onUserExit(v)
		tm.onUserEnter(v)
	}
}

func (tm *taskmgr) loadSysTaskList() {
	if tm.loadSysTaskFromJson() {
		return
	}
	tasks := getSysTaskList()
	tm.sys_lock.Lock()
	defer tm.sys_lock.Unlock()
	tm.sys_tasklist = tasks
}

func (tm *taskmgr) onUserEnter(userId int) {
	tm.user_lock.RLock()
	_, ok := tm.user_tasklist[userId]
	tm.user_lock.RUnlock()
	if ok {
		return
	}
	tm.user_lock.Lock()
	tm.user_tasklist[userId] = newUserTask(userId)
	tm.user_lock.Unlock()
}

func (tm *taskmgr) onUserExit(userId int) {
	tm.user_lock.Lock()
	defer tm.user_lock.Unlock()
	delete(tm.user_tasklist, userId)
}

func (tm *taskmgr) getSysTask(taskId int) *pb.Task {
	tm.sys_lock.RLock()
	defer tm.sys_lock.RUnlock()
	ret, ok := tm.sys_tasklist[taskId]
	if !ok {
		log.Debug("taskmgr.getSysTask taskId[%d] not found", taskId)
		return nil
	}
	return ret
}

func (tm *taskmgr) getNextTasks(taskId int) []*pb.Task {
	tm.sys_lock.RLock()
	defer tm.sys_lock.RUnlock()
	var ret []*pb.Task
	for _, v := range tm.sys_tasklist {
		if v.PreTask == taskId && v.IsOpen() {
			ret = append(ret, v)
		}
	}
	return ret
}

func (tm *taskmgr) getSysTaskList() map[int]*pb.Task {
	tm.sys_lock.RLock()
	defer tm.sys_lock.RUnlock()
	return tm.sys_tasklist
}

func (tm *taskmgr) getUserTasks(userId int) []*pb.UserTask {
	tm.user_lock.RLock()
	defer tm.user_lock.RUnlock()
	var ret []*pb.UserTask
	ut, ok := tm.user_tasklist[userId]
	if !ok {
		return ret
	}
	ret = ut.getTaskList()
	return ret
}

func (tm *taskmgr) getUserTask(userId int) *user_task {
	tm.user_lock.RLock()
	ut, ok := tm.user_tasklist[userId]
	if ok {
		tm.user_lock.RUnlock()
		return ut
	}
	tm.user_lock.RUnlock()

	ut = newUserTask(userId)
	tm.user_lock.Lock()
	tm.user_tasklist[userId] = ut
	tm.user_lock.Unlock()
	return ut
}

func (tm *taskmgr) doTaskAction(userId int, action, progress int, param pb.TaskScope) (bool, int) {
	go activityservice.DoAction(userId, action, progress, param)
	go badge.DoAction(userId, action, progress, badge.Scope{GameName: param.GameName, RankCrdate: param.RankCrdate})
	ut := tm.getUserTask(userId)
	return ut.doTaskAction(action, progress, param)
}

func (tm *taskmgr) isActionTaskActive(userId int, action int, param pb.TaskScope) bool {
	ut := tm.getUserTask(userId)
	return ut.isActionTaskActive(action, param)
}

func (tm *taskmgr) removeTaskByAction(userId int, action int, param pb.TaskScope) {
	ut := tm.getUserTask(userId)
	ut.removeTaskByAction(action, param)
}

// 刷新任务
func (tm *taskmgr) refreshTask(userId int) {
	ut := tm.getUserTask(userId)
	ut.loadUserTask()
}

func (tm *taskmgr) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("taskmgr.dumpSys %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	tm.sys_lock.RLock()
	defer tm.sys_lock.RUnlock()

	if param != "" {
		d, _ := json.Marshal(tm.sys_tasklist)
		log.Release(string(d))
	} else {
		for _, v := range tm.sys_tasklist {
			log.Release("%d:Scn[%d]Title[%s]Act[%d]Trg[%d]", v.Id, v.Scene, v.Title, v.Action, v.Target)
		}
	}
}

func (tm *taskmgr) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("taskmgr.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}
	ut := tm.getUserTask(userId)
	if ut == nil {
		log.Release("user %d not exist", userId)
		return
	}
	ut.dump()
	//d, _ := json.Marshal(si)
	//log.Release(string(d))
}

func (tm *taskmgr) awardTask(userId int, taskId int) (bool, string) {
	ut := tm.getUserTask(userId)
	return ut.awardTask(taskId)
}

func (tm *taskmgr) awardTaskWithItems(userId int, taskId int) string {
	ut := tm.getUserTask(userId)
	return ut.awardTaskWithItems(taskId)
}

func (tm *taskmgr) isTriggerPreAddiction(userId int) bool {
	ut := tm.getUserTask(userId)
	return ut.isTriggerPreAddiction()
}

func (tm *taskmgr) getTaskTipScene(userId int) int {
	ut := tm.getUserTask(userId)
	return ut.getTaskTipScene()
}

func (tm *taskmgr) awardAllTask(userId int) []item.ItemPack {
	ut := tm.getUserTask(userId)
	return ut.awardAllTask()
}

func (tm *taskmgr) getSysTasksByScene(scene int) []*pb.Task {
	var ret []*pb.Task
	tm.sys_lock.RLock()
	for k, v := range tm.sys_tasklist {
		if common.DecimalAnd(v.Scene, scene) > 0 {
			ret = append(ret, tm.sys_tasklist[k])
		}
	}
	tm.sys_lock.RUnlock()
	return ret
}

func (tm *taskmgr) getSysTasksByTaskIds(taskIds []int) []*pb.Task {
	var ret []*pb.Task
	tm.sys_lock.RLock()
	for k, v := range tm.sys_tasklist {
		for _, taskId := range taskIds {
			if v.Id == taskId {
				ret = append(ret, tm.sys_tasklist[k])
				break
			}
		}
	}
	tm.sys_lock.RUnlock()
	return ret
}

func (tm *taskmgr) createRandomTasksByScene(userId int, scene int, maxCount int, isRefresh bool) {
	ut := tm.getUserTask(userId)
	ut.createRandomTasksByScene(scene, maxCount, isRefresh)
}

func (tm *taskmgr) isAllTaskFinished(userId int, scene int) bool {
	ut := tm.getUserTask(userId)
	return ut.isAllTaskFinished(scene)
}

func (tm *taskmgr) createUserTasks(userId int, tasks []int) bool {
	ut := tm.getUserTask(userId)
	return ut.createTasksByIds(tasks)
}

func (tm *taskmgr) isTasksFinished(userId int, tasks []int) bool {
	ut := tm.getUserTask(userId)
	return ut.isTasksFinished(tasks)
}

func (tm *taskmgr) getTasksRewards(taskIds []int) []item.ItemPack {
	var ret []item.ItemPack
	tm.sys_lock.RLock()
	for _, taskId := range taskIds {
		sysTask, ok := tm.sys_tasklist[taskId]
		if !ok {
			continue
		}
		ret = append(ret, sysTask.Awards...)
	}
	tm.sys_lock.RUnlock()

	return item.GroupItems(ret)
}

func (tm *taskmgr) claimTaskRewards(userId int, taskIds []int) []item.ItemPack {
	ut := tm.getUserTask(userId)
	return ut.claimTaskRewards(taskIds)
}
