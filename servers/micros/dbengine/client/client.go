package main

import (
	slog "bet24.com/log"
	pb "bet24.com/servers/micros/dbengine/proto"
	"log"

	_ "bet24.com/database"
)

func main() {
	logger, err := slog.New("debug", "debug", "log/client", log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	slog.Debug(pb.SayHello("client"))

}
