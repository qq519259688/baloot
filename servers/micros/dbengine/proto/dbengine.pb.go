package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "dbengine"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name string
}

type Response struct {
	Data string
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("dbengine failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

func Execute(sql string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: sql,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "Execute", args, reply)
	if err != nil {
		log.Debug("dbengine failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	return reply.Data
}

type ResponseRs struct {
	Data [][]interface{}
}

func ExecuteRs(sql string) [][]interface{} {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: sql,
	}

	reply := &ResponseRs{}

	err := xclient.Call(context.Background(), "ExecuteRs", args, reply)
	if err != nil {
		log.Debug("dbengine failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}

	return reply.Data
}
