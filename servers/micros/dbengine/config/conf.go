package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	slog "bet24.com/log"
)

var Server struct {
	// 通用配置
	LogLevel   string
	FileLevel  string
	LogPath    string
	LastDay    int
	ConsulPort int

	// 服务配置
	Login      string
	Password   string
	Database   string
	Datasource string
}

func Run(name string) {
	data, err := os.ReadFile(fmt.Sprintf("%s.json", name))
	if err != nil {
		log.Fatalf("read config failed serviceconf/%s.json %v", name, err)
	}

	err = json.Unmarshal(data, &Server)
	if err != nil {
		log.Fatalf("Unmarshal config failed serviceconf/%s.json err:%v", name, err)
	}

	if Server.ConsulPort == 0 {
		Server.ConsulPort = 8500
	}

	if Server.LogPath == "" {
		Server.LogPath = fmt.Sprintf("log/%s", name)
	}

	logger, err := slog.New(Server.LogLevel, Server.FileLevel, Server.LogPath, log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	now := time.Now()
	Server.LastDay = now.Day()
	refreshLogFile()
}

func refreshLogFile() {
	time.AfterFunc(5*time.Minute, refreshLogFile)
	doRefreshLogFile()
}

func doRefreshLogFile() {
	if Server.LogPath != "" {
		now := time.Now()
		if now.Day() != Server.LastDay {
			Server.LastDay = now.Day()
			slog.RecreateFileLog(Server.LogPath, log.LstdFlags)
		}
	}
}
