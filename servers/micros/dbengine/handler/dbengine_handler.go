package handler

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/micros/dbengine/config"
	pb "bet24.com/servers/micros/dbengine/proto"
	"context"
	"errors"
)

var instance *DbEngine

func GetInstance() *DbEngine {
	if instance == nil {
		instance = newDBEngine()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newDBEngine() *DbEngine {
	ret := new(DbEngine)
	ret.ctor()
	return ret
}

type DbEngine struct {
	db *database.DataBase
}

func (d *DbEngine) ctor() {
	d.db = database.NewDataBase(
		config.Server.Datasource, config.Server.Database,
		public.DecryptDBString(config.Server.Login),
		public.DecryptDBString(config.Server.Password))
}

func (d *DbEngine) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = "Hello from DbEngine:" + req.Name
	return nil
}

func (d *DbEngine) Execute(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	if d.db == nil {
		return errors.New("db not initialized")
	}
	rsp.Data = d.db.ExecSqlJson(req.Name)
	return nil
}

func (d *DbEngine) ExecuteRs(ctx context.Context, req *pb.Request, rsp *pb.ResponseRs) error {
	if d.db == nil {
		return errors.New("db not initialized")
	}
	rsp.Data = d.db.ExecSql(req.Name)
	return nil
}

func (d *DbEngine) dump(cmd, param1, param2 string) {
	log.Debug("DbEngine.Dump %s,%s,%s", cmd, param1, param2)
	switch cmd {
	default:
		log.Release("DbEngine.Dump unhandled cmd %s", cmd)
	}
}
