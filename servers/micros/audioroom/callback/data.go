package callback

// 房间创建回调
type Request_CreateRoom struct {
	Appid            string `json:"appid" form:"appid"`                       // APP 的唯一标识。
	Room_id          string `json:"room_id" form:"room_id"`                   // 房间 ID。
	Id_name          string `json:"id_name" form:"id_name"`                   // 房间创建者的用户 ID。
	Room_create_time string `json:"room_create_time" form:"room_create_time"` // 房间创建时间，单位：毫秒。
	Timestamp        string `json:"timestamp" form:"timestamp"`               // 服务器当前时间，Unix 时间戳，单位：秒。
	Nonce            string `json:"nonce" form:"nonce"`                       // 随机数。
	Signature        string `json:"signature" from:"signature"`               // 检验串，详情见 检验说明。
	Event            string `json:"event" form:"event"`                       // 回调事件，此回调返回值为 room_create。
}

// 登录房间回调
type Request_LoginRoom struct {
	Appid           string `json:"appid" form:"appid"`                     // APP 的唯一标识。
	Event           string `json:"event" form:"event"`                     // 回调事件，此回调返回值为 room_login。
	Login_time      string `json:"login_time" form:"login_time"`           // 用户登录房间时间戳，单位：毫秒。
	Nonce           string `json:"nonce" form:"nonce"`                     // 随机数。
	Room_id         string `json:"room_id" form:"room_id"`                 // 房间 ID。
	Room_name       string `json:"room_name" form:"room_name"`             // 房间名。
	Room_seq        string `json:"room_seq" form:"room_seq"`               // 房间生命周期唯一标识。例如用户两次登录 room_id 都为 r1，但是第一次登录的房间 room_seq 为 123，第二次登录的房间 room_seq 为 234。此时 room_seq（123）房间销毁后才能创建 room_seq（234）房间，虽然 room_id 一样，但是为不同房间。
	Session_id      string `json:"session_id" from:"session_id"`           // 用户会话 ID。
	Signature       string `json:"signature" form:"signature"`             // 检验串，详情见 检验说明。
	Timestamp       string `json:"timestamp" form:"timestamp"`             // 服务器当前时间，Unix 时间戳。
	User_account    string `json:"user_account" form:"user_account"`       // 用户账号 ID。
	User_nickname   string `json:"user_nickname" form:"user_nickname"`     // 用户昵称。
	User_role       string `json:"user_role" form:"user_role"`             // 用户角色。1：主播。2：观众。
	User_update_seq string `json:"user_update_seq" form:"user_update_seq"` // 房间用户列表变更 seq，用户登录或者退出都会递增 1。
}

// 退出房间回调
type Request_LogoutRoom struct {
	Appid           string `json:"appid" form:"appid"`                     // APP 的唯一标识。
	Event           string `json:"event" form:"event"`                     // 回调事件，此回调返回值为 room_logout。
	Login_time      string `json:"login_time" form:"login_time"`           // 用户登录房间时间戳，单位：毫秒。
	Logout_time     string `json:"logout_time" form:"logout_time"`         // 用户退出房间时间戳，单位：毫秒。
	Nonce           string `json:"nonce" form:"nonce"`                     // 随机数。
	Reason          string `json:"reason" form:"reason"`                   // 退出原因。0：正常退出。1：心跳超时退出。2：用户断线退出。3：调用后台接口被踢出。4：token 过期退出。
	Room_id         string `json:"room_id" form:"room_id"`                 // 房间 ID。
	Room_seq        string `json:"room_seq" form:"room_seq"`               // 房间生命周期唯一标识。例如用户两次登录 room_id 都为 r1，但是第一次登录的房间 room_seq 为 123，第二次登录的房间 room_seq 为 234。此时 room_seq（123）房间销毁后才能创建 room_seq（234）房间，虽然 room_id 一样，但是为不同房间。
	Session_id      string `json:"session_id" form:"session_id"`           // 用户会话 ID。
	Signature       string `json:"signature" form:"signature"`             // 检验串，详情见 检验说明。
	Timestamp       string `json:"timestamp" form:"timestamp"`             // 服务器当前时间，Uinx 时间戳。
	User_account    string `json:"user_account" form:"user_account"`       // 用户账号 ID。
	User_nickname   string `json:"user_nickname" form:"user_nickname"`     // 用户昵称。
	User_role       string `json:"user_role" form:"user_role"`             // 用户角色。1：anchor2：audience
	User_update_seq string `json:"user_update_seq" form:"user_update_seq"` // 房间用户列表变更 seq，用户登录或者退出都会递增 1。
}

// 流创建回调
type Request_StreamCreate struct {
	Stream_sid   string   `json:"stream_sid" form:"stream_sid"`     // 流 server ID, 流的唯一标识，由 ZEGO 后台生成，开发者可不关注。
	Channel_id   string   `json:"channel_id" form:"channel_id"`     // 频道 ID，对应客户端的 RoomID，不超过 127 字节。
	Title        string   `json:"title" form:"title"`               // 标题，不超过 255 字节。
	Stream_alias string   `json:"stream_alias" form:"stream_alias"` // 流名，对应客户端的 StreamID，不超过 255 字节。
	Publish_id   string   `json:"publish_id" form:"publish_id"`     // 发布者 ID，对应客户端的 UserID，不超过 255 字节。
	Publish_name string   `json:"publish_name" form:"publish_name"` // 发布者名字，对应客户端的 UserName，不超过 255 字节。
	Rtmp_url     []string `json:"rtmp_url" form:"rtmp_url"`         // RTMP 拉流地址，不超过 1024 字节。
	Hls_url      []string `json:"hls_url" form:"hls_url"`           // HLS 拉流地址，不超过 1024 字节。
	Hdl_url      []string `json:"hdl_url" form:"hdl_url"`           // HDL 拉流地址，不超过 1024 字节。
	Pic_url      []string `json:"pic_url" form:"pic_url"`           // 截图地址，不超过 255 字节。
	Create_time  string   `json:"create_time" form:"create_time"`   // 流创建时间，Unix 时间戳。
	Timestamp    string   `json:"timestamp" form:"timestamp"`       // 流创建后，服务器回调给客户时，服务器的当前时间。
	Nonce        string   `json:"nonce" form:"nonce"`               // 随机数。
	Signature    string   `json:"signature" form:"signature"`       // 检验串，请参考 回调说明 - 检验说明。
	Extra_info   string   `json:"extra_info" form:"extra_info"`     // 流附加信息。
	Appid        string   `json:"appid" form:"appid"`               // AppId。
	Event        string   `json:"event" form:"event"`               // 回调事件，此回调返回值为 stream_create。
	Stream_id    string   `json:"stream_id" form:"stream_id"`       // 流 ID，对应客户端的 StreamID，同 stream_alias 意义相同
	Stream_seq   string   `json:"stream_seq" form:"stream_seq"`     // 服务器流列表变更的 seq，每次流变更都会加 1。
	Room_id      string   `json:"room_id" form:"room_id"`           // 房间 ID，对应客户端的 RoomID，同 channel_id 意义相同，不超过 127 字节。
	Recreate     string   `json:"recreate" form:"recreate"`         // 是否重复推流。1：表示客户端重新推了一条服务器已存在的流。0：表示新流。
	User_id      string   `json:"user_id" form:"user_id"`           // 用户 ID，同 publish_id 意义相同，不超过 255 字节。
	User_name    string   `json:"user_name" form:"user_name"`       // 用户昵称，同 publish_name 意义相同，不超过 255 字节。
	Stream_attr  string   `json:"stream_attr" form:"stream_attr"`   // 流信息。示例："{"cid":0}"，cid 表示该流的编码 ID。cid 取值如下：	0：H264	1：H264 分层编码	2：VP8 编码	3：H265
}

// 流关闭回调
type Request_StreamClose struct {
	Stream_sid         string `json:"stream_sid" form:"stream_sid"`                 // 流 server ID, 流的唯一标识，由 ZEGO 后台生成，开发者可不关注。
	Destroy_timemillis string `json:"destroy_timemillis" form:"destroy_timemillis"` // 流关闭时间，单位毫秒。
	Create_time_ms     string `json:"create_time_ms" form:"create_time_ms"`         // 流创建时间，单位毫秒。
	Third_define_data  string `json:"third_define_data" form:"third_define_data"`   // 客户自定义数据。
	Type               string `json:"type" form:"type"`                             // 关闭类型。0：正常关闭（用户调用流关闭接口，后台调用流关闭接口）。非0为异常关闭：1：用户心跳超时关闭。2：用户重复登录同一个房间，关闭上次登录会话的流。3：服务端接口 kickout 用户，关闭用户创建的流。4：tcp 掉线关闭流（可配置通知时间）。5：房间被清除关闭流。100：服务端接口关闭流。
	Channel_id         string `json:"channel_id" form:"channel_id"`                 // 频道 ID，对应客户端的 RoomID，不超过 127 字节。
	Stream_alias       string `json:"stream_alias" form:"stream_alias"`             // 流名，对应客户端的 StreamID，不超过 255 字节。
	Timestamp          string `json:"timestamp" form:"timestamp"`                   // 服务器当前时间，Unix 时间戳。
	Nonce              string `json:"nonce" form:"nonce"`                           // 随机数。
	Signature          string `json:"signature" form:"signature"`                   // 检验串，请参考 回调说明 - 检验说明。
	Appid              string `json:"appid" form:"appid"`                           // AppID。
	Event              string `json:"event" form:"event"`                           // 回调事件，此回调返回值为 stream_close。
	Stream_id          string `json:"stream_id" form:"stream_id"`                   // 流 ID，对应客户端的 StreamID，同 stream_alias 意义相同。
	Stream_seq         string `json:"stream_seq" form:"stream_seq"`                 // 服务器流列表变更的 seq，每次流变更都会加 1。
	Room_id            string `json:"room_id" form:"room_id"`                       // 房间 ID，对应客户端的 RoomID，同 channel_id 意义相同，不超过 127 字节。
	User_id            string `json:"user_id" form:"user_id"`                       // 用户 ID，同 publish_id 意义相同，不超过 255 字节。
	User_name          string `json:"user_name" form:"user_name"`                   // 用户昵称，同 publish_name 意义相同，不超过 255 字节。
}
