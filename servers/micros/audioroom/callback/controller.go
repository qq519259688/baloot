package callback

import (
	"bet24.com/servers/micros/audioroom/handler/manager"
	"encoding/hex"
	"encoding/json"
	"net/http"
	"sort"
	"strconv"
	"strings"

	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/micros/audioroom/config"
	pb "bet24.com/servers/micros/audioroom/proto"
	"github.com/gin-gonic/gin"
)

// 创建房间（房间第1个人进来时创建的房间流）
func CreateRoom(c *gin.Context) {
	var req Request_CreateRoom
	if err := c.ShouldBind(&req); err != nil {
		log.Error("callback.CreateRoom params err %v", err)
		return
	}

	log.Debug("controller.CreateRoom %+v", req)

	// 处理业务逻辑
	if !checkSign(req.Signature, req.Timestamp, req.Nonce) {
		log.Error("controller.CreateRoom 校验失败 %+v", req)
		return
	}

	roomId, err := strconv.Atoi(req.Room_id)
	if err != nil {
		log.Error("controller.CreateRoom room_id strconv.Atoi err %v", err)
		return
	}

	// 获取房间信息
	buf, _ := json.Marshal(struct {
		RoomId int
	}{
		RoomId: roomId,
	})
	pb.OnAudioRoomMsg(0, "getAudioRoom", string(buf))

	c.String(http.StatusOK, "success")
	return
}

// 登录房间
func LoginRoom(c *gin.Context) {
	var req Request_LoginRoom
	if err := c.ShouldBind(&req); err != nil {
		log.Error("callback.LoginRoom params err %v", err)
		return
	}

	log.Debug("controller.LoginRoom %+v", req)

	// 处理业务逻辑
	if !checkSign(req.Signature, req.Timestamp, req.Nonce) {
		log.Error("controller.LoginRoom 校验失败 %+v", req)
		return
	}

	userId, err := strconv.Atoi(req.User_account)
	if err != nil {
		log.Error("controller.LoginRoom user_account strconv.Atoi err %v", err)
		return
	}

	roomId, err := strconv.Atoi(req.Room_id)
	if err != nil {
		log.Error("controller.LoginRoom room_id strconv.Atoi err %v", err)
		return
	}

	// 进入房间
	buf, _ := json.Marshal(struct {
		RoomId int
	}{
		roomId,
	})
	pb.OnAudioRoomMsg(userId, "enterAudioRoom", string(buf))

	c.String(http.StatusOK, "success")
	return
}

// 退出房间
func LogoutRoom(c *gin.Context) {
	var req Request_LogoutRoom
	if err := c.ShouldBind(&req); err != nil {
		log.Error("callback.logoutRoom params err %v", err)
		return
	}

	log.Debug("controller.LogoutRoom %+v", req)

	// 处理业务逻辑
	if !checkSign(req.Signature, req.Timestamp, req.Nonce) {
		log.Error("controller.LogoutRoom 校验失败 %+v", req)
		return
	}

	userId, err := strconv.Atoi(req.User_account)
	if err != nil {
		log.Error("controller.LogoutRoom user_account strconv.Atoi err %v", err)
		return
	}

	roomId, err := strconv.Atoi(req.Room_id)
	if err != nil {
		log.Error("controller.LogoutRoom room_id strconv.Atoi err %v", err)
		return
	}

	// 离开房间
	manager.ExitRoomNotify(userId, roomId)

	c.String(http.StatusOK, "success")
	return
}

// 流创建回调
func StreamCreate(c *gin.Context) {
	var req Request_StreamCreate
	if err := c.ShouldBind(&req); err != nil {
		log.Error("callback.StreamCreate params err %v", err)
		return
	}

	log.Debug("controller.StreamCreate %+v", req)

	// 处理业务逻辑
	if !checkSign(req.Signature, req.Timestamp, req.Nonce) {
		log.Error("controller.StreamCreate 校验失败 %+v", req)
		return
	}

	userId, err := strconv.Atoi(req.User_id)
	if err != nil {
		log.Error("controller.StreamCreate user_id strconv.Atoi err %v", err)
		return
	}

	roomId, err := strconv.Atoi(req.Room_id)
	if err != nil {
		log.Error("controller.StreamCreate room_id strconv.Atoi err %v", err)
		return
	}

	// 上麦通知
	manager.OnTheMicNotify(userId, roomId, req.Stream_id)

	c.String(http.StatusOK, "success")
	return
}

// 流关闭回调
func StreamClose(c *gin.Context) {
	var req Request_StreamClose
	if err := c.ShouldBind(&req); err != nil {
		log.Error("callback.StreamClose params err %v", err)
		return
	}

	log.Debug("controller.StreamClose %+v", req)

	// 处理业务逻辑
	if !checkSign(req.Signature, req.Timestamp, req.Nonce) {
		log.Error("controller.StreamClose 校验失败 %+v", req)
		return
	}

	userId, err := strconv.Atoi(req.User_id)
	if err != nil {
		log.Error("controller.StreamClose user_id strconv.Atoi err %v", err)
		return
	}

	roomId, err := strconv.Atoi(req.Room_id)
	if err != nil {
		log.Error("controller.StreamClose room_id strconv.Atoi err %v", err)
		return
	}

	if userId < 0 {
		userId *= -1
	}

	// 下麦
	manager.OffTheMicNotify(userId, roomId, req.Stream_id)

	c.String(http.StatusOK, "success")
	return
}

// 校验
func checkSign(signature, timestamp, nonce string) bool {
	var list = []string{config.Server.Zego.Callbacksecret, nonce, timestamp}

	// 参数值按字典降序排序
	sort.SliceStable(list, func(i, j int) bool {
		return list[i] < list[j]
	})

	data := strings.Join(list, "")
	sign := hex.EncodeToString(public.Sha1([]byte(data)))
	// log.Debug("controller.checkSign data=%s sign=%s signature=%s", data, sign, signature)
	return sign == signature
}
