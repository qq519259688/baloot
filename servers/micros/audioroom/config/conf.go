package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	slog "bet24.com/log"
)

var Server struct {
	LogLevel   string
	FileLevel  string
	LogPath    string
	LastDay    int
	ConsulPort int

	ChannelUrl      string
	ChannelPassword string
	RedisDB         int

	WebPort int
	WebKey  string
	TlsPort int    // HTTPS 端口
	TlsCert string // HTTPS 证书FILE
	TlsKey  string // HTTPS 证书KEY

	Zego // 音视频信息
}

type Zego struct {
	AppId          uint32 `json:"zego.appId"`          // 用于标识访问者的身份
	ServerSecret   string `json:"zego.serverSecret"`   // 用于加密签名字符串和服务器验证签名字符串的密钥
	Callbacksecret string `json:"zego.callbackSecret"` // 服务端校验密钥
}

func Run(name string) {
	data, err := os.ReadFile(fmt.Sprintf("serviceconf/%s.json", name))
	if err != nil {
		log.Fatalf("read config failed serviceconf/%s.json %v", name, err)
	}

	err = json.Unmarshal(data, &Server)
	if err != nil {
		log.Fatalf("Unmarshal config failed serviceconf/%s.json err:%v", name, err)
	}

	if Server.ConsulPort == 0 {
		Server.ConsulPort = 5500
	}
	if Server.LogPath == "" {
		Server.LogPath = fmt.Sprintf("log/%s", name)
	}

	logger, err := slog.New(Server.LogLevel, Server.FileLevel, Server.LogPath, log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	now := time.Now()
	Server.LastDay = now.Day()
	refreshLogFile()
}

func refreshLogFile() {
	time.AfterFunc(5*time.Minute, refreshLogFile)
	doRefreshLogFile()
}

func doRefreshLogFile() {
	if Server.LogPath != "" {
		now := time.Now()
		if now.Day() != Server.LastDay {
			Server.LastDay = now.Day()
			slog.RecreateFileLog(Server.LogPath, log.LstdFlags)
		}
	}
}
