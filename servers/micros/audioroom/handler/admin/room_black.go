package admin

import (
	"bet24.com/servers/common"
	"bet24.com/servers/micros/audioroom/handler/manager"
	pb "bet24.com/servers/micros/audioroom/proto"
	user "bet24.com/servers/micros/userservices/proto"
)

// 获取黑名单 - 后台
func (this *adminMgr) AdminGetBlackList(roomId, blackType int) []pb.AdminRoomBlackList {
	if roomId == 0 {
		var blackList []pb.AdminRoomBlackList
		for _, v := range manager.AdminMgr.GetAllRoom() {
			list := this.getOnlineBlackList(v.RoomId, blackType)
			blackList = append(blackList, list...)
		}
		return blackList
	}
	return this.getOnlineBlackList(roomId, blackType)
}

// 获取在线的黑名单列表
func (this *adminMgr) getOnlineBlackList(roomId, blackType int) []pb.AdminRoomBlackList {
	r := manager.AdminMgr.GetRoom(roomId)
	if r == nil {
		return nil
	}

	var list []pb.AdminRoomBlackList

	for _, v := range r.GetBlackList() {
		for _, b := range v.BlackTypes {
			if b.BlackType != blackType {
				continue
			}

			// 无效用户
			u := user.GetUserInfo(v.UserId)
			if u == nil {
				continue
			}

			info := r.GetMemberInfo(u.UserId)

			// 追加数据
			list = append(list, pb.AdminRoomBlackList{
				UserID:     u.UserId,
				NickName:   u.NickName,
				RoleID:     info.RoleId,
				Level:      info.Level,
				ExpireTime: b.ExpireTimeStamp - common.GetTimeStamp(),
				Crdate:     info.Crdate,
			})
		}
	}
	return list
}
