package admin

import (
	"bet24.com/servers/common"
	"bet24.com/servers/micros/audioroom/handler/manager"
	pb "bet24.com/servers/micros/audioroom/proto"
	user "bet24.com/servers/micros/userservices/proto"
	"sort"
)

// 获取语聊房麦位 - 后台
func (this *adminMgr) AdminGetRoomMic(roomId int) []pb.AdminRoomMic {
	var micList []pb.AdminRoomMic
	if roomId == 0 {
		for _, v := range manager.AdminMgr.GetAllRoom() {
			list := this.getOnlineMic(v.RoomId)
			micList = append(micList, list...)
		}
	} else {
		micList = this.getOnlineMic(roomId)
	}
	return this.changeMicListSort(micList)
}

// 获取在线的麦位
func (this *adminMgr) getOnlineMic(roomId int) []pb.AdminRoomMic {
	r := manager.AdminMgr.GetRoom(roomId)
	if r == nil {
		return nil
	}

	var list []pb.AdminRoomMic
	for k, v := range r.GetMicListInfo() {
		if v.UserId <= 0 {
			continue
		}

		u := user.GetUserInfo(v.UserId)
		if u == nil {
			continue
		}

		member := r.GetMemberInfo(v.UserId)
		list = append(list, pb.AdminRoomMic{
			UserID:    u.UserId,
			NickName:  u.NickName,
			RoleID:    member.RoleId,
			Level:     member.Level,
			MicNum:    k + 1,
			UpMicType: r.MicMode,
			UpMicTime: common.TimeStampToString(int64(v.TimeStamp)),
		})
	}
	return list
}

// 改变麦位列表的排序
func (this *adminMgr) changeMicListSort(list []pb.AdminRoomMic) []pb.AdminRoomMic {
	// 排序规则：优先加入
	sort.SliceStable(list, func(i, j int) bool {
		return list[i].UpMicTime < list[j].UpMicTime
	})
	return list
}
