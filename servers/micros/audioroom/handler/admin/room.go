package admin

import (
	"bet24.com/servers/micros/audioroom/handler/manager"
	pb "bet24.com/servers/micros/audioroom/proto"
	"sort"
)

var Mgr *adminMgr

type adminMgr struct {
}

func LoadAdminMgr() {
	Mgr = new(adminMgr)
	return
}

// 获取房间列表 - 后台
func (this *adminMgr) AdminGetRoomList(roomName, sortName, sortType string, roomId int) []pb.AdminRoomInfo {
	ret := manager.AdminMgr.GetRoomList(roomName, roomId)

	if sortName == "Level" {
		this.adminRoomLevelSort(ret, sortType) // 房间等级排序
	} else if sortName == "MemberCount" {
		this.adminMemberCountSort(ret, sortType) // 成员数量排序
	} else if sortName == "CollectDiamond" {
		this.adminCollectDiamondSort(ret, sortType) // 钻石收益排序
	} else if sortName == "DiamondAmount" {
		this.adminDiamondAmountSort(ret, sortType) // 钻石消耗排序
	} else if sortName == "CollectGold" {
		this.adminCollectGoldSort(ret, sortType) // 金币收益排序
	} else if sortName == "GoldAmount" {
		this.adminGoldAmountSort(ret, sortType) // 金币消耗排序
	} else {
		sort.SliceStable(ret, func(i, j int) bool {
			return ret[i].Crdate > ret[j].Crdate
		})
	}
	return ret
}

// 房间等级排序 - 后台
func (this *adminMgr) adminRoomLevelSort(items []pb.AdminRoomInfo, sortOrder string) {
	sort.SliceStable(items, func(i, j int) bool {
		if sortOrder == "ASC" {
			if items[i].Level == items[j].Level {
				if items[i].Exps == items[j].Exps {
					return items[i].Crdate > items[j].Crdate
				}
				return items[i].Exps < items[j].Exps
			}
			return items[i].Level < items[j].Level
		}
		if items[i].Level == items[j].Level {
			if items[i].Exps == items[j].Exps {
				return items[i].Crdate > items[j].Crdate
			}
			return items[i].Exps > items[j].Exps
		}
		return items[i].Level > items[j].Level
	})
}

// 成员数量排序 - 后台
func (this *adminMgr) adminMemberCountSort(items []pb.AdminRoomInfo, sortOrder string) {
	sort.SliceStable(items, func(i, j int) bool {
		if items[i].MemberCount == items[j].MemberCount {
			return items[i].Crdate > items[j].Crdate
		}
		if sortOrder == "ASC" {
			return items[i].MemberCount < items[j].MemberCount
		}
		return items[i].MemberCount > items[j].MemberCount
	})
}

// 钻石收益排序 - 后台
func (this *adminMgr) adminCollectDiamondSort(items []pb.AdminRoomInfo, sortOrder string) {
	sort.SliceStable(items, func(i, j int) bool {
		if items[i].CollectDiamond == items[j].CollectDiamond {
			return items[i].Crdate > items[j].Crdate
		}
		if sortOrder == "ASC" {
			return items[i].CollectDiamond < items[j].CollectDiamond
		}
		return items[i].CollectDiamond > items[j].CollectDiamond
	})
}

// 钻石消耗排序 - 后台
func (this *adminMgr) adminDiamondAmountSort(items []pb.AdminRoomInfo, sortOrder string) {
	sort.SliceStable(items, func(i, j int) bool {
		if items[i].DiamondAmount == items[j].DiamondAmount {
			return items[i].Crdate > items[j].Crdate
		}
		if sortOrder == "ASC" {
			return items[i].DiamondAmount < items[j].DiamondAmount
		}
		return items[i].DiamondAmount > items[j].DiamondAmount
	})
}

// 金币收益排序 - 后台
func (this *adminMgr) adminCollectGoldSort(items []pb.AdminRoomInfo, sortOrder string) {
	sort.SliceStable(items, func(i, j int) bool {
		if items[i].CollectGold == items[j].CollectGold {
			return items[i].Crdate > items[j].Crdate
		}
		if sortOrder == "ASC" {
			return items[i].CollectGold < items[j].CollectGold
		}
		return items[i].CollectGold > items[j].CollectGold
	})
}

// 金币消耗排序 - 后台
func (this *adminMgr) adminGoldAmountSort(items []pb.AdminRoomInfo, sortOrder string) {
	sort.SliceStable(items, func(i, j int) bool {
		if items[i].GoldAmount == items[j].GoldAmount {
			return items[i].Crdate > items[j].Crdate
		}
		if sortOrder == "ASC" {
			return items[i].GoldAmount < items[j].GoldAmount
		}
		return items[i].GoldAmount > items[j].GoldAmount
	})
}

// 获取语聊房详细信息 - 后台
func (this *adminMgr) AdminGetRoomDetail(roomId int) pb.AdminRoomDetail {
	var resp pb.AdminRoomDetail
	roomInfo := manager.AdminMgr.GetRoom(roomId)
	if roomInfo == nil {
		return resp
	}

	expend := manager.AdminMgr.GetRoomExpend(roomId)

	resp.RoomId = roomInfo.RoomId
	resp.RoomName = roomInfo.RoomName
	resp.RoomImg = roomInfo.RoomImg
	resp.Family = roomInfo.Family
	resp.Country = roomInfo.Country
	resp.Language = roomInfo.Language
	resp.JoinFee = roomInfo.JoinFee
	resp.Announce = roomInfo.Announce
	resp.Tag = roomInfo.Tag
	resp.MemberCount = roomInfo.MemberCount
	resp.Crdate = roomInfo.Crdate
	resp.Level = roomInfo.Level
	resp.Exps = roomInfo.Exps
	resp.UpMicCount = roomInfo.GetUpMicCount()
	resp.CollectDiamond = expend.CollectDiamond
	resp.DiamondAmount = expend.DiamondAmount
	resp.CollectGold = expend.CollectGold
	resp.GoldAmount = expend.GoldAmount
	resp.OnlineCount = roomInfo.OnlineCount
	resp.CumulativeDuration = 0
	return resp
}
