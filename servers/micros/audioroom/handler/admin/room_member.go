package admin

import (
	"bet24.com/servers/micros/audioroom/handler/manager"
	pb "bet24.com/servers/micros/audioroom/proto"
	user "bet24.com/servers/micros/userservices/proto"
	"sort"
)

// 获取语聊房成员 - 后台
func (this *adminMgr) AdminGetRoomMember(roomId int) []pb.AdminRoomMember {
	var memberList []pb.AdminRoomMember
	if roomId == 0 {
		for _, v := range manager.AdminMgr.GetAllRoom() {
			list := this.getOnlineGroupMember(v.RoomId)
			memberList = append(memberList, list...)
		}
	} else {
		memberList = this.getOnlineGroupMember(roomId)
	}
	return this.changeMemberListSort(memberList)
}

// 获取在线的群成员
func (this *adminMgr) getOnlineGroupMember(roomId int) []pb.AdminRoomMember {
	r := manager.AdminMgr.GetRoom(roomId)
	if r == nil {
		return nil
	}

	var list []pb.AdminRoomMember
	onlineUser := r.GetOnlineUsers()
	if len(onlineUser) == 0 {
		return list
	}

	for _, member := range r.GetMembers() {
		var isExist bool
		for _, uid := range onlineUser {
			if member.UserId != uid {
				continue
			}
			isExist = true
		}
		if !isExist {
			continue
		}

		// 无效用户
		u := user.GetUserInfo(member.UserId)
		if u == nil {
			continue
		}

		list = append(list, pb.AdminRoomMember{
			RoomID:   r.RoomId,
			UserID:   u.UserId,
			NickName: u.NickName,
			RoleID:   member.RoleId,
			Exps:     member.Exps,
			Level:    member.Level,
			Crdate:   member.Crdate,
		})
	}
	return list
}

// 获取语聊房房间的在线用户 - 后台
func (this *adminMgr) AdminGetRoomOnlineUsers(roomId int) []pb.AdminRoomMember {
	var onlineList []pb.AdminRoomMember
	if roomId == 0 {
		for _, v := range manager.AdminMgr.GetAllRoom() {
			userList := this.getRoomOnlineUsers(v.RoomId)
			onlineList = append(onlineList, userList...)
		}
	} else {
		onlineList = this.getRoomOnlineUsers(roomId)
	}
	return this.changeMemberListSort(onlineList)
}

// 获取房间内的在线用户
func (this *adminMgr) getRoomOnlineUsers(roomId int) []pb.AdminRoomMember {
	r := manager.AdminMgr.GetRoom(roomId)
	if r == nil {
		return nil
	}

	var list []pb.AdminRoomMember
	onlineUser := r.GetOnlineUsers()
	if len(onlineUser) == 0 {
		return list
	}

	for _, v := range onlineUser {
		// 无效用户
		u := user.GetUserInfo(v)
		if u == nil {
			continue
		}

		member := r.GetMemberInfo(v)
		list = append(list, pb.AdminRoomMember{
			UserID:   u.UserId,
			NickName: u.NickName,
			RoleID:   member.RoleId,
			Exps:     member.Exps,
			Level:    member.Level,
			Crdate:   member.Crdate,
		})
	}
	return list
}

// 改变成员列表的排序
func (this *adminMgr) changeMemberListSort(list []pb.AdminRoomMember) []pb.AdminRoomMember {
	// 排序规则：创建者 > 经验值 > 优先加入
	sort.SliceStable(list, func(i, j int) bool {
		if list[i].RoleID == list[j].RoleID {
			if list[i].Exps == list[j].Exps {
				return list[i].Crdate < list[j].Crdate
			}
			return list[i].Exps > list[j].Exps
		}
		return list[i].RoleID < list[j].RoleID
	})
	return list
}
