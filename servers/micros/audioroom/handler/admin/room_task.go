package admin

import (
	"bet24.com/servers/common"
	"bet24.com/servers/micros/audioroom/handler/config"
	"bet24.com/servers/micros/audioroom/handler/manager"
	pb "bet24.com/servers/micros/audioroom/proto"
)

// 生成系统任务集合
func (this *adminMgr) GenerateSysTaskMap(sysFlag bool) map[int]*pb.TaskConfig {
	sysTask := config.Mgr.GetSysTask(sysFlag)

	sysTaskMap := make(map[int]*pb.TaskConfig)
	for _, v := range sysTask {
		sysTaskMap[v.Id] = v
	}
	return sysTaskMap
}

// 获取房间任务列表 - 后台
func (this *adminMgr) AdminGetRoomTask(roomId int) []pb.AdminRoomTask {
	list := make([]pb.AdminRoomTask, 0, 0)
	currentTime := common.GetTimeStamp()
	sysTaskMap := this.GenerateSysTaskMap(true)
	for _, v := range manager.AdminMgr.GetAllRoom() {
		if roomId > 0 && v.RoomId != roomId {
			continue
		}

		r := manager.AdminMgr.GetRoom(v.RoomId)
		if r == nil {
			continue
		}

		taskList := r.GetRoomTaskList()

		for k, _ := range taskList {
			taskInfo := pb.AdminRoomTask{
				RoomID:     r.RoomId,
				TaskID:     taskList[k].TaskId,
				Schedule:   taskList[k].Schedule,
				CurrNum:    taskList[k].CurrNum,
				UpdateTime: common.TimeStampToString(int64(taskList[k].TimeStamp)),
			}

			// 过期重置
			if !common.IsSameDay(taskList[k].TimeStamp, currentTime) {
				taskInfo.Schedule = 0
				taskInfo.CurrNum = 0
			}

			if sysTaskInfo, ok := sysTaskMap[taskList[k].TaskId]; ok {
				taskInfo.TaskName = sysTaskInfo.Name
			}
			list = append(list, taskInfo)
		}
	}
	return list
}
