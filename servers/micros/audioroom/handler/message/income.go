package message

import (
	"bet24.com/servers/micros/audioroom/handler/income"
)

// 获取收益信息
func audioRoomGetIncomeInfo(userId int, data string) string {
	return income.GetIncomeInfo(userId, data)
}

// 获取收益记录
func audioRoomGetIncomeLog(userId int, data string) string {
	return income.GetIncomeLog(userId, data)
}

// 获取用户收益统计
func audioRoomGetUserIncomeStat(userId int, data string) string {
	return income.GetUserIncomeStat(userId, data)
}

// 获取游戏收益统计
func audioRoomGetGameIncomeStat(userId int, data string) string {
	return income.GetGameIncomeStat(userId, data)
}
