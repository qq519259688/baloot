package message

import (
	"bet24.com/servers/micros/audioroom/handler/game"
	"bet24.com/servers/micros/audioroom/handler/manager"
)

// 开始游戏
func audioRoomStartGame(userId int, data string) string {
	return game.StartGame(userId, data)
}

func audioRoomGetGameRoomInfo(userId int, data string) string {
	return game.GetGameRoomInfo(userId, data)
}

// 创建游戏房间
func audioRoomCreateGameRoom(userId int, data string) string {
	return manager.CreateGameRoom(userId, data)
}

// 关闭游戏房间
func audioRoomCloseGameRoom(userId int, data string) string {
	return manager.CloseGameRoom(userId, data)
}
