package message

import (
	"bet24.com/log"
	"strings"
)

// 消息分发处理器
func MessageHandler(userId int, msg, data string) string {
	switch msg {
	case "loginAudioRoomServer": // 登录服务器
		return loginAudioRoomServer(userId, data)
	case "logoutAudioRoomServer": // 退出服务器
		return logoutAudioRoomServer(userId, data)
	case "getAudioRoomConfig": // 获取房间配置
		return getAudioRoomConfig(userId, data)
	case "getAudioRoom": // 获取房间信息
		return getAudioRoom(userId, data)
	case "getAudioRoomHotInfo": // 获取房间热数据
		return getAudioRoomHotInfo(userId, data)
	case "isEnterAudioRoom": // 是否允许进入房间
		return isEnterAudioRoom(userId, data)
	case "enterAudioRoom": // 进入房间
		return enterAudioRoom(userId, data)
	case "exitAudioRoom": // 退出房间
		return exitAudioRoom(userId, data)
	case "createAudioRoom": // 创建房间
		return createAudioRoom(userId, data)
	case "updateAudioRoom": // 修改房间
		return updateAudioRoom(userId, data)
	case "getAudioRoomOnlineRoom": //获取用户在线房间
		return getAudioRoomOnlineRoom(userId, data)
	case "getAudioRoomExploreList": // 获取探索列表
		return getAudioRoomExploreList(userId, data)
	case "getAudioRoomJoinList": // 获取加入房间列表
		return getAudioRoomJoinList(userId, data)
	case "getAudioRoomAttentionList": // 获取关注房间列表
		return getAudioRoomAttentionList(userId, data)
	case "getAudioRoomBrowseList": // 获取浏览房间列表
		return getAudioRoomBrowseList(userId, data)
	case "addAudioRoomJoin": // 加入
		return addAudioRoomJoin(userId, data)
	case "delAudioRoomJoin": // 取消加入
		return delAudioRoomJoin(userId, data)
	case "addAudioRoomAttention": // 关注
		return addAudioRoomAttention(userId, data)
	case "delAudioRoomAttention": // 取消关注
		return delAudioRoomAttention(userId, data)
	case "AudioRoomInviteJoin": // 发出成员邀请
		return audioRoomInviteJoin(userId, data)
	case "AudioRoomAcceptJoin": // 接受成员邀请
		return audioRoomAcceptJoin(userId, data)
	case "getAudioRoomOnlineUsers": // 获取在线用户成员列表
		return getAudioRoomOnlineUsers(userId, data)
	case "AudioRoomSearchUser": // 查询房间在线用户信息
		return audioRoomSearchUser(userId, data)
	case "getAudioRoomMembers": // 获取成员列表
		return getAudioRoomMembers(userId, data)
	case "searchAudioRoom": // 搜索房间
		return searchAudioRoom(userId, data)
	case "recommendAudioRoomUser": // 推荐用户
		return recommendAudioRoomUser(userId, data)
	case "recommendAudioRoom": // 推荐房间
		return recommendAudioRoom(userId, data)
	case "AudioRoomListByTag": // 根据标签获取房间列表
		return audioRoomListByTag(userId, data)
	case "AudioRoomInviteOnTheMic": // 邀请上麦
		return audioRoomInviteOnTheMic(userId, data)
	case "AudioRoomKickMic": // 踢麦(强制下麦)
		return audioRoomKickMic(userId, data)
	case "AudioRoomSetMic": // 加解锁麦(status -1=上锁  0=解锁)
		return audioRoomSetMic(userId, data)
	case "getAudioRoomMicList": //获取麦列表
		return getAudioRoomMicList(userId, data)
	case "AudioRoomGetPermission": // 获取权限
		return audioRoomGetPermission(userId, data)
	case "AudioRoomSetPermission": // 设置权限
		return audioRoomSetPermission(userId, data)
	case "AudioRoomGetEnterCondition": // 获取进入房间条件
		return audioRoomGetEnterCondition(userId, data)
	case "AudioRoomSetEnterCondition": // 设置进入房间条件
		return audioRoomSetEnterCondition(userId, data)
	case "AudioRoomGetBlackList": // 获取黑名单用户列表
		return audioRoomGetBlackList(userId, data)
	case "AudioRoomAddBlack": // 设置黑名单
		return audioRoomAddBlack(userId, data)
	case "AudioRoomRemoveBlack": // 移除黑名单
		return audioRoomRemoveBlack(userId, data)
	case "AudioRoomGetOperateLog": // 操作记录
		return audioRoomGetOperateLog(userId, data)
	case "AudioRoomAddAdmin": // 添加管理员
		return audioRoomAddAdmin(userId, data)
	case "AudioRoomDelAdmin": // 取消管理员
		return audioRoomDelAdmin(userId, data)
	case "AudioRoomDelMember": // 删除会员
		return audioRoomDelMember(userId, data)
	case "AudioRoomIsAttention": // 是否关注过房间
		return audioRoomIsAttention(userId, data)
	case "AudioRoomGetRoomImg": //房间封面
		return getRoomImg(userId, data)
	case "getAudioRoomSysTask": // 获取系统任务
		return getAudioRoomSysTask(userId, data)
	case "getAudioRoomUpgradeConfig": // 获取升级配置
		return getAudioRoomUpgradeConfig(userId, data)
	case "getAudioRoomTaskList": // 获取房间任务列表
		return getAudioRoomTaskList(userId, data)
	case "getAudioRoomCollect": // 获取昨天收集信息
		return getAudioRoomCollect(userId, data)
	case "giftAudioRoomCollect": // 领取收集奖励
		return giftAudioRoomCollect(userId, data)
	case "getAudioRoomExtInfo": // 获取房间扩展信息
		return getAudioRoomExtInfo(userId, data)
	case "AudioRoomGetUserTaskList": // 获取用户房间任务列表
		return audioRoomGetUserTaskList(userId, data)
	case "AudioRoomShare": //  分享
		return audioRoomShare(userId, data)
	case "AudioRoomSendMessage": // 发送短信
		return audioRoomSendMessage(userId, data)
	case "AudioRoomGetUserRoomInfo": // 用户房间信息
		return audioRoomGetUserRoomInfo(userId, data)
	case "AudioRoomGetUserRoomTaskStat": // 获取用户任务统计
		return audioRoomGetUserRoomTaskStat(userId, data)
	case "AudioRoomStartGame": // 开始游戏
		return audioRoomStartGame(userId, data)
	case "AudioRoomSetScreenLock": // 设置屏幕锁
		return audioRoomSetScreenLock(userId, data)
	case "AudioRoomIsBannedSpeak": // 是否禁止发言
		return audioRoomIsBannedSpeak(userId, data)
	case "AudioRoomGetGameRoomInfo":
		return audioRoomGetGameRoomInfo(userId, data)
	case "AudioRoomCreateGameRoom": // 创建游戏房间
		return audioRoomCreateGameRoom(userId, data)
	case "AudioRoomCloseGameRoom": // 关闭游戏房间
		return audioRoomCloseGameRoom(userId, data)
	case "AudioRoomGetIncomeInfo": // 收益信息
		return audioRoomGetIncomeInfo(userId, data)
	case "NotifyRoom": // 通知房间
		return notifyRoom(userId, data)
	case "updateRoomImg": // 修改房间封面
		return updateRoomImg(userId, data)
	case "AudioRoomApplyOnMic": // 申请上麦
		return audioRoomApplyOnMic(userId, data)
	case "AudioRoomCancelApplyOnMic": // 取消申请上麦
		return audioRoomCancelApplyOnMic(userId, data)
	case "AudioRoomDealApplyOnMic": // 处理申请上麦(1=同意  2=拒绝)
		return audioRoomDealApplyOnMic(userId, data)

	default:
		// 上麦
		if strings.Contains(msg, "AudioRoomOnTheMic") {
			return audioRoomOnTheMic(userId, data)
		}

		// 发送礼物
		if strings.Contains(msg, "AudioRoomSendGiving") {
			return audioRoomSendGiving(userId, data)
		}

		// 游戏列表
		if strings.Contains(msg, "AudioRoomGetGameList") {
			return audioRoomGetGameList(userId, data)
		}

		// 游戏房间列表
		if strings.Contains(msg, "AudioRoomGetGameRoomList") {
			return audioRoomGetGameRoomList(userId, data)
		}

		// 上麦申请列表
		if strings.Contains(msg, "AudioRoomGetOnMicApplyList") {
			return audioRoomGetOnMicApplyList(userId, data)
		}

		// 收益记录
		if strings.Contains(msg, "AudioRoomGetIncomeLog") {
			return audioRoomGetIncomeLog(userId, data)
		}

		// 用户收益统计
		if strings.Contains(msg, "AudioRoomGetUserIncomeStat") {
			return audioRoomGetUserIncomeStat(userId, data)
		}

		// 游戏收益统计
		if strings.Contains(msg, "AudioRoomGetGameIncomeStat") {
			return audioRoomGetGameIncomeStat(userId, data)
		}
	}

	log.Error("message_main.MessageHandler unprocessed userId=%d msg=%s data=%s", userId, msg, data)
	return ""
}
