package message

import (
	"bet24.com/servers/micros/audioroom/handler/config"
	"bet24.com/servers/micros/audioroom/handler/manager"
)

// 登录服务器
func loginAudioRoomServer(userId int, data string) string {
	return manager.LoginServer(userId, data)
}

// 退出服务器
func logoutAudioRoomServer(userId int, data string) string {
	return manager.LogoutServer(userId, data)
}

// 获取房间配置
func getAudioRoomConfig(userId int, data string) string {
	return config.Mgr.GetRoomConfigJson()
}

// 获取房间信息
func getAudioRoom(userId int, data string) string {
	return manager.GetRoomInfoJson(userId, data)
}

// 获取房间信息
func getAudioRoomHotInfo(userId int, data string) string {
	return manager.GetRoomHotInfo(userId, data)
}

// 是否允许进入房间
func isEnterAudioRoom(userId int, data string) string {
	return manager.IsEnterRoom(userId, data)
}

// 进入房间
func enterAudioRoom(userId int, data string) string {
	return manager.EnterRoom(userId, data)
}

// 退出房间
func exitAudioRoom(userId int, data string) string {
	return manager.ExitRoom(userId, data)
}

// 创建房间
func createAudioRoom(userId int, data string) string {
	return manager.CreateRoom(userId, data)
}

// 修改房间
func updateAudioRoom(userId int, data string) string {
	return manager.UpdateRoom(userId, data)
}

// 获取房间封面
func getRoomImg(userId int, data string) string {
	return manager.GetRoomImg(userId, data)
}

// 修改房间封面
func updateRoomImg(userId int, data string) string {
	return manager.UpdateRoomImg(userId, data)
}

// 获取用户在线房间
func getAudioRoomOnlineRoom(userId int, data string) string {
	return manager.GetOnlineRoom(userId, data)
}

// 获取探索列表
func getAudioRoomExploreList(userId int, data string) string {
	return manager.GetExploreList(userId, data)
}

// 获取加入房间列表
func getAudioRoomJoinList(userId int, data string) string {
	return manager.GetJoinList(userId, data)
}

// 获取关注房间列表
func getAudioRoomAttentionList(userId int, data string) string {
	return manager.GetAttentionList(userId, data)
}

// 获取浏览房间列表
func getAudioRoomBrowseList(userId int, data string) string {
	return manager.GetBrowseList(userId, data)
}

// 加入(扣费)
func addAudioRoomJoin(userId int, data string) string {
	return manager.AddJoin(userId, data)
}

// 取消加入
func delAudioRoomJoin(userId int, data string) string {
	return manager.DelJoin(userId, data)
}

// 关注
func addAudioRoomAttention(userId int, data string) string {
	return manager.AddAttention(userId, data)
}

// 取消关注
func delAudioRoomAttention(userId int, data string) string {
	return manager.DelAttention(userId, data)
}

// 发出成员邀请
func audioRoomInviteJoin(userId int, data string) string {
	return manager.InviteJoin(userId, data)
}

// 接受成员邀请
func audioRoomAcceptJoin(userId int, data string) string {
	return manager.AcceptJoin(userId, data)
}

// 获取在线用户列表
func getAudioRoomOnlineUsers(userId int, data string) string {
	return manager.GetOnlineUsers(userId, data)
}

// 查询房间在线用户信息
func audioRoomSearchUser(userId int, data string) string {
	return manager.SearchUser(userId, data)
}

// 获取成员列表
func getAudioRoomMembers(userId int, data string) string {
	return manager.GetMembers(userId, data)
}

// 搜索
func searchAudioRoom(userId int, data string) string {
	return manager.SearchRoom(userId, data)
}

// 推荐用户
func recommendAudioRoomUser(userId int, data string) string {
	return manager.RecommendUser(userId, data)
}

// 推荐房间
func recommendAudioRoom(userId int, data string) string {
	return manager.RecommendRoom(userId, data)
}

// 根据标签获取房间列表
func audioRoomListByTag(userId int, data string) string {
	return manager.RoomListByTag(userId, data)
}

// 上麦
func audioRoomOnTheMic(userId int, data string) string {
	return manager.OnTheMic(userId, data)
}

// 邀请上麦
func audioRoomInviteOnTheMic(userId int, data string) string {
	return manager.InviteOnTheMic(userId, data)
}

// 踢麦(强制下麦)
func audioRoomKickMic(userId int, data string) string {
	return manager.KickMic(userId, data)
}

// 加解锁麦(status -1=上锁  0=解锁)
func audioRoomSetMic(userId int, data string) string {
	return manager.SetMic(userId, data)
}

// 获取麦列表
func getAudioRoomMicList(userId int, data string) string {
	return manager.GetMicList(userId, data)
}

// 获取权限
func audioRoomGetPermission(userId int, data string) string {
	return manager.GetPermission(userId, data)
}

// 设置权限
func audioRoomSetPermission(userId int, data string) string {
	return manager.SetPermission(userId, data)
}

// 获取进入房间条件
func audioRoomGetEnterCondition(userId int, data string) string {
	return manager.GetEnterCondition(userId, data)
}

// 设置进入房间条件
func audioRoomSetEnterCondition(userId int, data string) string {
	return manager.SetEnterCondition(userId, data)
}

// 获取黑名单用户列表
func audioRoomGetBlackList(userId int, data string) string {
	return manager.GetBlackList(userId, data)
}

// 设置黑名单
func audioRoomAddBlack(userId int, data string) string {
	return manager.AddBlack(userId, data)
}

// 移除黑名单
func audioRoomRemoveBlack(userId int, data string) string {
	return manager.RemoveBlack(userId, data)
}

// 操作记录
func audioRoomGetOperateLog(userId int, data string) string {
	return manager.GetOperateLog(userId, data)
}

// 添加管理员
func audioRoomAddAdmin(userId int, data string) string {
	return manager.AddAdmin(userId, data)
}

// 取消管理员
func audioRoomDelAdmin(userId int, data string) string {
	return manager.DelAdmin(userId, data)
}

// 取消会员
func audioRoomDelMember(userId int, data string) string {
	return manager.DelMember(userId, data)
}

// 是否关注过房间
func audioRoomIsAttention(userId int, data string) string {
	return manager.IsAttention(userId, data)
}

// 发送礼物
func audioRoomSendGiving(userId int, data string) string {
	return manager.SendGiving(userId, data)
}

// 获取系统任务
func getAudioRoomSysTask(userId int, data string) string {
	return manager.GetSysTask(userId, data)
}

// 获取升级配置
func getAudioRoomUpgradeConfig(userId int, data string) string {
	return manager.GetUpgradeConfig(userId, data)
}

// 获取房间任务列表
func getAudioRoomTaskList(userId int, data string) string {
	return manager.GetTaskList(userId, data)
}

// 获取昨天收集信息
func getAudioRoomCollect(userId int, data string) string {
	return manager.GetCollect(userId, data)
}

// 领取收集奖励
func giftAudioRoomCollect(userId int, data string) string {
	return manager.GiftCollect(userId, data)
}

// 获取扩展信息
func getAudioRoomExtInfo(userId int, data string) string {
	return manager.GetExtInfo(userId, data)
}

// 获取用户房间任务列表
func audioRoomGetUserTaskList(userId int, data string) string {
	return manager.GetUserTaskList(userId, data)
}

// 分享
func audioRoomShare(userId int, data string) string {
	return manager.Share(userId, data)
}

// 发送短信
func audioRoomSendMessage(userId int, data string) string {
	return manager.SendMessage(userId, data)
}

// 用户房间信息
func audioRoomGetUserRoomInfo(userId int, data string) string {
	return manager.GetUserRoomInfo(userId, data)
}

// 获取用户任务统计
func audioRoomGetUserRoomTaskStat(userId int, data string) string {
	return manager.GetUserRoomTaskStat(userId, data)
}

// 获取游戏列表
func audioRoomGetGameList(userId int, data string) string {
	return manager.GetGameList(userId, data)
}

// 获取游戏房间列表
func audioRoomGetGameRoomList(userId int, data string) string {
	return manager.GetGameRoomList(userId, data)
}

// 通知房间
func notifyRoom(userId int, data string) string {
	return manager.NotifyRoom(userId, data)
}

// 设置屏幕锁（1=锁定，0=解锁）
func audioRoomSetScreenLock(userId int, data string) string {
	return manager.SetScreenLock(userId, data)
}

// 是否禁止发言
func audioRoomIsBannedSpeak(userId int, data string) string {
	return manager.IsBannedSpeak(userId, data)
}

// 麦位申请列表
func audioRoomGetOnMicApplyList(userId int, data string) string {
	return manager.GetOnMicApplyList(userId, data)
}

// 申请上麦
func audioRoomApplyOnMic(userId int, data string) string {
	return manager.ApplyOnMic(userId, data)
}

// 取消申请上麦
func audioRoomCancelApplyOnMic(userId int, data string) string {
	return manager.CancelApplyOnMic(userId, data)
}

// 处理申请上麦(1=同意  2=拒绝)
func audioRoomDealApplyOnMic(userId int, data string) string {
	return manager.DealApplyOnMic(userId, data)
}
