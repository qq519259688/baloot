package user

import (
	"bet24.com/servers/common"
	"bet24.com/servers/micros/audioroom/handler/config"
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/database"
)

// 加载用户贡献
func (this *UserRoom) loadContribute() {
	list := database.GetUserContribute(this.userId)
	for k, _ := range list {
		this.contributeList = append(this.contributeList, &list[k])
	}
	return
}

// 获取用户贡献余额
func (this *UserRoom) GetContribute(roomId, action int) (bool, int) {

	dayLimit := 0

	// 获取日限额
	for _, d := range config.Mgr.GetRoomConfig().DayLimits {
		if d.Action != action {
			continue
		}

		dayLimit = d.Exps
	}

	// 没有配置即无限制
	if dayLimit == 0 {
		return true, dayLimit
	}

	for _, v := range this.contributeList {
		if v.RoomId != roomId {
			continue
		}

		if v.Action != action {
			continue
		}

		exps := v.Exps
		if !common.IsSameDay(v.Expire, common.GetTimeStamp()) {
			exps = 0
		}

		if exps >= dayLimit {
			return false, 0
		}

		return true, dayLimit - exps
	}

	return true, dayLimit
}

// 添加用户贡献
func (this *UserRoom) AddContribute(roomId, action, exps int) {
	for _, v := range this.contributeList {
		if v.RoomId != roomId {
			continue
		}

		if v.Action != action {
			continue
		}

		if !common.IsSameDay(v.Exps, common.GetTimeStamp()) {
			v.Exps = 0
		}

		v.Exps += exps
		v.Expire = common.GetTimeStamp()

		// TODO:更新数据库
		database.UpdateUserContribute(this.userId, v)
		return
	}

	info := &pb.UserContribute{
		RoomId: roomId,
		Action: action,
		Exps:   exps,
		Expire: common.GetTimeStamp(),
	}

	this.contributeList = append(this.contributeList, info)

	// TODO：更新数据库
	database.UpdateUserContribute(this.userId, info)
	return
}

// 删除贡献
func (this *UserRoom) DelContribute(roomId int) {
	for i := 0; i < len(this.contributeList); i++ {
		if this.contributeList[i].RoomId != roomId {
			continue
		}

		this.contributeList = append(this.contributeList[:i], this.contributeList[i+1:]...)
	}
}
