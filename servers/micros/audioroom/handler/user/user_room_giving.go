package user

import (
	"bet24.com/log"
	"bet24.com/servers/micros/audioroom/handler/config"
	"bet24.com/servers/micros/audioroom/handler/income"
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/database"
	badge "bet24.com/servers/micros/badge/proto"
	giftservice "bet24.com/servers/micros/giftservice/proto"
	task "bet24.com/servers/micros/task/proto"
	"fmt"
)

// 发送礼物
func (this *UserRoom) SendGiving(roomId, roomOwner, toUserId, giftId, num int) (retCode int, message string) {
	log.Debug("user_giving.sendGiving userId=%d roomId=%d toUserId=%d giftId=%d num=%d", this.userId, roomId, toUserId, giftId, num)

	var (
		totalDiamond int   // 总钻石数
		totalGold    int   // 总金币数
		sendDiamond  int   // 每人获得钻石数
		sendGold     int   // 每人获得金币数
		toUserIds    []int // 接收礼物的id群
	)

	// 所有在线用户
	if toUserId == 1 {
		for _, v := range this.roomMgr.GetOnlineUsers(roomId) {
			if v.UserId == this.userId {
				continue
			}
			toUserIds = append(toUserIds, v.UserId)
		}
	} else if this.userId != toUserId {
		toUserIds = append(toUserIds, toUserId)
	}

	// 数量异常
	if len(toUserIds) <= 0 {
		message = fmt.Sprintf("user_giving.sendGiving userId=%d roomId=%d toUserId=%d giftId=%d num=%d is irregular",
			this.userId, roomId, toUserId, giftId, num)
		log.Debug(message)
		return
	}

	// 发送礼物
	retCode, message, result := giftservice.SendGiftBulk(this.userId, toUserIds, giftId, num)

	//log.Debug("user_giving.sendGiving userId=%d roomId=%d toUserId=%d giftId=%d num=%d retCode=%v message=%s result=%+v toUserIds=%+v",
	//	this.userId, roomId, toUserId, giftId, num, retCode, message, result, toUserIds)

	// 判断是否成功
	if retCode != 1 {
		message = fmt.Sprintf("user_giving.sendGiving userId=%d roomId=%d toUserId=%d giftId=%d num=%d retCode=%d message=%s result=%+v",
			this.userId, roomId, toUserId, giftId, num, retCode, message, result)
		log.Debug(message)
		return
	}

	cfg := config.Mgr.GetRoomConfig()

	switch result.PayType {
	case giftservice.PayType_Diamond: // 赠送钻石
		sendDiamond = int(result.Price)
		totalDiamond = int(result.Price) * len(toUserIds)
	case giftservice.PayType_Gold: // 赠送金币
		sendGold = int(result.Price)
		totalGold = int(result.Price) * len(toUserIds)
	}

	log.Debug("user_giving.sendGiving userId=%d roomId=%d toUserId=%d giftId=%d num=%d retCode=%v message=%s result=%+v toUserIds=%+v totalDiamond=%d sendDiamond=%d",
		this.userId, roomId, toUserId, giftId, num, retCode, message, result, toUserIds, totalDiamond, sendDiamond)

	// 给所有在线用户赠送礼物
	for _, uid := range toUserIds {
		if uid == this.userId {
			continue
		}

		// 触发收益
		for _, v := range result.Items {
			income.TriggerGiftIncome(this.userId, pb.TriggerData{
				RoomId:    roomId,
				Receiver:  uid,
				RoomOwner: roomOwner,
				ItemType:  v.ItemId,
				Amount:    v.Count,
			})
		}

		if sendDiamond > 0 {

			go func(uid int) {
				// 触发用户任务
				this.roomMgr.DoUserTaskAction(uid, roomId, pb.UserTask_Action_AcceptGiving_Diamond, sendDiamond, 0)

				// 徽章进度
				badge.DoAction(uid, badge.Action_AudioRoom_ReceiveGivingDiamond, sendDiamond, badge.Scope{})
				badge.DoAction(uid, badge.Action_AudioRoom_ReceiveGivingNum, num, badge.Scope{GiftId: giftId})

				// 触发大厅任务
				task.DoTaskAction(uid, task.TaskAction_receiveGivingDiamond, sendDiamond, task.TaskScope{})
			}(uid)
		}

		// 添加礼物记录
		go database.AddGiftHistory(pb.GiftHistory{
			RoomId:        roomId,
			Sender:        this.userId,
			Receiver:      uid,
			GiftId:        giftId,
			GiftNum:       num,
			DiamondAmount: sendDiamond,
			GoldAmount:    sendGold,
		})
	}

	// 达到大礼物条件
	if totalDiamond >= cfg.GivingDiamondMax || result.AnimationType == giftservice.AnimationType_Global {

		// 发送礼物通知
		go this.givingNotify(roomId, toUserId, result.GiftId, num, totalDiamond >= cfg.GivingDiamondMax)
	}

	//  钻石
	if totalDiamond > 0 {
		go func() {
			// 触发房间任务
			this.roomMgr.DoRoomTaskAction(this.userId, roomId, pb.RoomTask_Action_Giving_Diamond, totalDiamond)

			// 触发用户任务
			this.roomMgr.DoUserTaskAction(this.userId, roomId, pb.UserTask_Action_SendGiving_Diamond, totalDiamond, 0)

			// 收集点数
			this.roomMgr.AddCollect(this.userId, roomId, totalDiamond)

			// 徽章进度
			badge.DoAction(this.userId, badge.Action_AudioRoom_SendGivingDiamond, totalDiamond, badge.Scope{})
			badge.DoAction(this.userId, badge.Action_AudioRoom_SendGivingNum, num, badge.Scope{GiftId: giftId})

			// 触发大厅任务
			task.DoTaskAction(this.userId, task.TaskAction_diamondGift, 1, task.TaskScope{})

			// 触发大厅任务
			task.DoTaskAction(this.userId, task.TaskAction_sendGivingDiamond, totalDiamond, task.TaskScope{})
		}()
	}

	// 金币
	if totalGold > 0 {
		go func() {
			// 触发任务
			this.roomMgr.DoRoomTaskAction(this.userId, roomId, pb.RoomTask_Action_Giving_Gold, totalGold)

			// 徽章进度
			badge.DoAction(this.userId, badge.Action_PropConsumeGold, totalGold, badge.Scope{})
		}()
	}

	retCode, message = 1, "success"
	return
}
