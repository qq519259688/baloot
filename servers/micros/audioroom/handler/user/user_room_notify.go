package user

import (
	"encoding/json"

	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	notification "bet24.com/servers/micros/notification/proto"
	user "bet24.com/servers/micros/userservices/proto"
)

// 发出邀请加入通知
func (this *UserRoom) inviteJoinNotify(toUserId, roomId int, code string) {
	u := user.GetUserInfo(this.userId)
	if u == nil {
		return
	}

	buf, _ := json.Marshal(notification.NotificationAudioRoom{
		NotifyId: pb.Notify_Action_Refresh_User,
		RoomId:   roomId,
		Data: pb.ReasonData{
			Reason:   pb.Notify_Reason_User_Invite,
			UserId:   u.UserId,
			NickName: u.NickName,
			Code:     code,
		},
	})

	log.Debug("user_room_notify.inviteJoinNotify userId=%d toUserId=%d roomId=%d buf=%s", this.userId, toUserId, roomId, string(buf))
	go notification.AddNotification(toUserId, notification.Notification_AudioRoom, string(buf))

	return
}

// 发送礼物通知
func (this *UserRoom) givingNotify(roomId, toUserId, giftId, num int, isBarrage bool) {
	// 发送数据
	var sendData struct {
		UserId     int    `json:"sender.UserId"`
		NickName   string `json:"sender.NickName"`
		FaceId     int    `json:"sender.FaceId"`
		FaceUrl    string `json:"sender.FaceUrl"`
		ToUserId   int    `json:"receiver.UserId,omitempty"`
		ToNickName string `json:"receiver.NickName,omitempty"`
		ToFaceId   int    `json:"receiver.FaceId,omitempty"`
		ToFaceUrl  string `json:"receiver.FaceUrl,omitempty"`
		GiftId     int    `json:"GiftId"`
		Num        int    `json:"Num"`
		IsBarrage  bool
	}

	// 礼物信息
	sendData.GiftId = giftId
	sendData.Num = num
	sendData.IsBarrage = isBarrage

	// 发送者用户信息
	if sender := user.GetUserInfo(this.userId); sender != nil {
		sendData.UserId = sender.UserId
		sendData.NickName = sender.NickName
		sendData.FaceId = sender.FaceId
		sendData.FaceUrl = sender.FaceUrl
	}

	// 接收者用户信息
	if receiver := user.GetUserInfo(toUserId); receiver != nil {
		sendData.ToUserId = receiver.UserId
		sendData.ToNickName = receiver.NickName
		sendData.ToFaceId = receiver.FaceId
		sendData.ToFaceUrl = receiver.FaceUrl
	}

	buf, _ := json.Marshal(notification.NotificationAudioRoom{
		NotifyId: pb.Notify_Action_Giving,
		RoomId:   roomId,
		Data:     sendData,
	})

	// 给所有用户发送通知
	log.Debug("user_room_notify.givingNotify userId=%d toUserId=%d roomId=%d giftId=%d buf=%s", this.userId, toUserId, roomId, giftId, string(buf))
	notification.AddNotification(-1, notification.Notification_AudioRoom, string(buf))
	return
}

// 等级通知
func (this *UserRoom) levelNotify(roomId int) {
	buf, _ := json.Marshal(notification.NotificationAudioRoom{
		NotifyId: pb.Notify_Action_Refresh_User,
		RoomId:   roomId,
		Data: pb.ReasonData{
			Reason: pb.Notify_Reason_User_Level_Up,
			UserId: this.userId,
		},
	})

	log.Debug("user_room_notify.levelNotify userId=%d roomId=%d buf=%s", this.userId, roomId, string(buf))
	go notification.AddNotification(this.userId, notification.Notification_AudioRoom, string(buf))

	return
}
