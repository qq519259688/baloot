package user

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/micros/audioroom/handler/config"
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/database"
	"math"
	"sort"
)

// 任务列表
func (this *UserRoom) GetTaskList(roomId int) []*pb.UserRoomTask {
	// 判断是否是成员
	if this.UserRoomInfo(roomId) == nil {
		return nil
	}

	list, ok := this.GetUserTask(roomId)
	if ok {
		for k := range list {
			if !common.IsSameDay(list[k].TimeStamp, common.GetTimeStamp()) {
				list[k].Schedule = 0
				list[k].CurrNum = 0
				list[k].Status = 0
			}
		}
		return list
	}

	// TODO: 从数据库获取
	list = database.GetUserTaskList(this.userId, roomId)

	// 初始化任务
	for _, v := range config.Mgr.GetSysTask(false) {
		isExist := false

		for _, t := range list {
			if v.Id != t.TaskId {
				continue
			}

			isExist = true
			break
		}

		if isExist {
			continue
		}

		info := &pb.UserRoomTask{
			RoomTask: pb.RoomTask{
				TaskId:    v.Id,
				Schedule:  0,
				CurrNum:   0,
				TimeStamp: common.GetTimeStamp(),
			},
			Status: 0,
		}

		list = append(list, info)

		// TODO:更新数据库
		go database.UpdateUserTask(this.userId, roomId, info)
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].TaskId < list[j].TaskId
	})

	this.SetUserTask(roomId, list)

	return list
}

// 获取用户任务
func (this *UserRoom) GetUserTask(roomId int) ([]*pb.UserRoomTask, bool) {
	this.lock.RLock()
	defer this.lock.RUnlock()
	list, ok := this.taskList[roomId]
	return list, ok
}

// 设置用户任务
func (this *UserRoom) SetUserTask(roomId int, list []*pb.UserRoomTask) {
	this.lock.Lock()
	defer this.lock.Unlock()
	this.taskList[roomId] = list
}

// 触发房间任务
func (this *UserRoom) DoTaskAction(roomId, action, num, ext int) {
	//log.Debug("user_roomtask.doTaskAction userId=%d roomId=%d action=%d num=%d ext=%d",
	//	this.userId, roomId, action, num, ext)

	r := this.UserRoomInfo(roomId)
	if r == nil {
		log.Release("user_roomtask.doTaskAction userId=%d roomId=%d action=%d num=%d is nil",
			this.userId, roomId, action, num)
		return
	}

	// 遍历房间任务
	for _, v := range this.GetTaskList(roomId) {

		// 获取任务配置
		cfg := config.Mgr.GetTaskConfig(v.TaskId, false)
		if cfg == nil {
			log.Debug("user_roomtask.doTaskAction userId=%d roomId=%d taskId %d not found", this.userId, roomId, v.TaskId)
			continue
		}

		// 判断触发动作
		if cfg.Action != action {
			continue
		}

		// 判断是否过期
		if !common.IsSameDay(v.TimeStamp, common.GetTimeStamp()) {
			v.Schedule = 0
			v.CurrNum = 0
			v.Status = 0
		}

		// 判断任务是否完成
		if cfg.Target > 0 && v.Schedule >= cfg.Target {
			continue
		}

		// 当前数值
		v.CurrNum += num

		if len(cfg.Exps) <= 0 || ext > len(cfg.Exps) {
			log.Debug("user_roomtask.doTaskAction userId=%d roomId=%d taskId exps=%+v is invalid", this.userId, roomId, v.TaskId, cfg.Exps)
			return
		}

		// 计算经验值
		exps := int(math.Floor(float64(v.CurrNum/cfg.NeedNum)*cfg.Exps[ext] + 0.5))
		if cfg.Target > 0 && v.Schedule+exps > cfg.Target {
			exps = cfg.Target - v.Schedule
		}

		// 经验值不够,累计数值积累
		if exps <= 0 {
			// TODO:更新数据库
			go database.UpdateUserTask(this.userId, r.RoomId, v)
			continue
		}

		// 扣掉升级的所需数值
		v.CurrNum = v.CurrNum % cfg.NeedNum

		// 任务进度
		v.Schedule += exps

		// 更新时间戳
		v.TimeStamp = common.GetTimeStamp()

		// 达到统计阈值
		if cfg.StatValue > 0 && v.Schedule >= cfg.StatValue {
			v.Status = 1
		}

		// 添加房间经验
		r.Exps += exps
		r.DayExps += exps

		// 判断是否升级
		if newLevel := config.Mgr.GetLevel(r.Exps, false); newLevel > r.Level {
			r.Level = newLevel

			// 发送通知
			go this.levelNotify(r.RoomId)
		}

		// 同步房间成员等级经验
		go this.roomMgr.UpdateMemberLevel(this.userId, r.RoomId, r.Level, r.Exps)

		// TODO:更新数据库
		go database.UpdateUserTask(this.userId, r.RoomId, v)
		go database.UpdateUserExps(this.userId, r.RoomId, r.Level, r.Exps)
	}

	return
}

// 清理任务
func (this *UserRoom) DelTask(roomId int) {
	this.lock.Lock()
	defer this.lock.Unlock()
	delete(this.taskList, roomId)
}
