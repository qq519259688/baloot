package room

import (
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/database"
)

// 添加操作日志
func (this *Room) AddOperateLog(roomId, userId, toUserId, operateType int) {
	database.AddOperateLog(roomId, userId, toUserId, operateType)
	return
}

// 获取操作日志
func (this *Room) GetOperateLog(toUserId, operateType, pageIndex, pageSize int) (int, []pb.OperateInfo) {
	return database.OperateLog(this.RoomId, toUserId, operateType, pageIndex, pageSize)
}
