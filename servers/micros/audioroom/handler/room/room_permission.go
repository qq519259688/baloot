package room

import (
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/database"
)

// 加载权限
func (this *Room) loadPermission(isFromDB bool) {
	var list []pb.PermissionInfo

	if isFromDB {
		// 数据库取数据
		list = database.GetPermissionList(this.RoomId)
	}

	for i := pb.PermissionType_Invalid + 1; i < pb.PermissionType_Max; i++ {
		isExist := false

		// 遍历是否已生成数据
		for _, v := range list {
			if v.PermissionType != i {
				continue
			}

			isExist = true
			break
		}

		// 已存在
		if isExist {
			continue
		}

		enabled := 0

		// 房间默认开启
		if i == pb.PermissionType_Mic || i == pb.PermissionType_Room {
			enabled = 1
		}

		// 新增数据
		list = append(list, pb.PermissionInfo{
			PermissionType: i,
			Enabled:        enabled,
		})
	}

	this.permissionList = list
	return
}

// 获取权限列表
func (this *Room) GetPermissionList() []pb.PermissionInfo {
	var permissionList []pb.PermissionInfo
	for _, v := range this.permissionList {
		if v.PermissionType == pb.PermissionType_Room {
			continue
		}
		permissionList = append(permissionList, v)
	}
	return permissionList
}

// 获取游戏权限类型
func (this *Room) GetGamePermissionType(gameId int) int {
	for _, v := range pb.GamePermissions {
		if v.GameId == gameId {
			return v.PermissionType
		}
	}
	return 0
}

// 判断是否有游戏权限
func (this *Room) IsGamePermission(userId, gameId int) bool {
	// 获取游戏权限类型
	permissionType := this.GetGamePermissionType(gameId)

	return this.IsPermission(userId, permissionType)
}

// 判断是否有权限
func (this *Room) IsPermission(userId, permissionType int) bool {
	// 房主拥有所有权限
	if this.UserId == userId {
		return true
	}

	// 会员身份
	if m := this.GetMemberInfo(userId); m.RoleId != pb.Role_Assistant {
		return false
	}

	// 判断是否开启该权限
	for _, v := range this.permissionList {
		// 权限类型
		if v.PermissionType != permissionType {
			continue
		}

		return v.Enabled == 1
	}

	return false
}

// 设置权限
func (this *Room) SetPermission(permissionType, enabled int) int {
	// 判断是否超出范围
	if permissionType <= pb.PermissionType_Invalid || permissionType >= pb.PermissionType_Max {
		return 11
	}

	isNew := true

	for i := 0; i < len(this.permissionList); i++ {
		if this.permissionList[i].PermissionType != permissionType {
			continue
		}

		isNew = false
		this.permissionList[i].Enabled = enabled
		break
	}

	// 新增权限
	if isNew {
		this.permissionList = append(this.permissionList, pb.PermissionInfo{
			PermissionType: permissionType,
			Enabled:        enabled,
		})
	}

	// 数据库操作
	go database.SetRoomPermission(this.RoomId, permissionType, enabled)

	return 1
}

// 是否有设置权限
func (this *Room) IsSetPermission(userId, toUserId int, permissionType int) bool {

	// 不能对自己和房主操作
	if userId == toUserId || this.UserId == toUserId {
		return false
	}

	// 管理员有超级权限
	if this.UserId == userId {
		return true
	}

	// 对象会员身份
	toM := this.GetMemberInfo(toUserId)

	// 会员身份
	m := this.GetMemberInfo(userId)

	// 同级别的角色,没有权限
	if m.RoleId == toM.RoleId {
		return false
	}

	// 创建者,有所有的权限
	if m.RoleId == pb.Role_Administrator {
		return true
	}

	// 不是助理(管理员)
	if m.RoleId != pb.Role_Assistant {
		return false
	}

	// 判断权限
	for _, p := range this.permissionList {
		if p.PermissionType != permissionType {
			continue
		}

		return p.Enabled == pb.Permission_Open
	}

	return false
}
