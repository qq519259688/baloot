package room

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/database"
	"bet24.com/servers/zego"
	"strconv"
)

// 加载成员数据
func (this *Room) loadMembers() {
	memberList := database.GetMembers(this.RoomId)
	if len(memberList) <= 0 {
		return
	}

	this.members = memberList
}

// 是否管理员
func (this *Room) isAdmin(userId int) bool {
	m := this.GetMemberInfo(userId)
	return m.RoleId == pb.Role_Administrator || m.RoleId == pb.Role_Assistant
}

// 添加管理员
func (this *Room) AddAdmin(userId, toUserId int) int {
	// 不是管理员
	if this.UserId != userId {
		return 11
	}

	var roleId int

	for i := 0; i < len(this.members); i++ {
		if this.members[i].UserId != toUserId {
			continue
		}

		// 判断是否是管理员、助手
		if this.members[i].RoleId == pb.Role_Administrator || this.members[i].RoleId == pb.Role_Assistant {
			return 12
		}

		member := this.members[i]
		roleId = member.RoleId

		info := pb.UserRoomInfo{
			UserId: toUserId,
			RoomId: this.RoomId,
			RoleId: pb.Role_Assistant,
			Level:  member.Level,
			Crdate: common.GetNowTimeStr(),
		}

		// 修改会员信息
		this.members[i] = info

		go func() {
			// 给所有用户发送通知
			this.notify(pb.Notify_Action_Refresh_User, pb.ReasonData{
				Reason:    pb.Notify_Reason_User_ChangeRole,
				UserId:    toUserId,
				OldRoleId: roleId,
				NewRoleId: pb.Role_Assistant,
			})

			// 数据库操作
			database.SaveUserRoom(info.UserId, info.RoomId, info.RoleId, info.Crdate)
		}()

		return 1
	}

	return 13
}

// 取消管理员
func (this *Room) DelAdmin(userId, toUserId int) int {
	// 不是管理员
	if this.UserId != userId {
		return 11
	}

	var roleId int

	for i := 0; i < len(this.members); i++ {
		if this.members[i].UserId != toUserId {
			continue
		}

		// 判断是否是管理员、助手
		if this.members[i].RoleId != pb.Role_Assistant {
			return 12
		}

		member := this.members[i]
		roleId = member.RoleId

		info := pb.UserRoomInfo{
			UserId: toUserId,
			RoomId: this.RoomId,
			Level:  member.Level,
			RoleId: pb.Role_Member,
			Crdate: common.GetNowTimeStr(),
		}

		// 修改会员信息
		this.members[i] = info

		go func() {
			// 给所有用户发送通知
			this.notify(pb.Notify_Action_Refresh_User, pb.ReasonData{
				Reason:    pb.Notify_Reason_User_ChangeRole,
				UserId:    toUserId,
				OldRoleId: roleId,
				NewRoleId: pb.Role_Member,
			})

			// 数据库操作
			database.SaveUserRoom(info.UserId, info.RoomId, info.RoleId, info.Crdate)
		}()

		return 1
	}

	return 14
}

// 删除会员,是否踢出房间
func (this *Room) DelMember(userId, toUserId int, isKickOut bool) int {

	log.Debug("room_member.delMember userId=%d toUserId=%d", userId, toUserId)

	// 不是管理层成员
	if !this.isAdmin(userId) {
		return 11
	}

	var roleId int

	for i := 0; i < len(this.members); i++ {
		if this.members[i].UserId != toUserId {
			continue
		}

		roleId = this.members[i].RoleId

		// 删除会员
		this.members = append(this.members[:i], this.members[i+1:]...)
		break
	}

	this.MemberCount = len(this.members)

	// 需要踢出房间
	if isKickOut {
		roomId := strconv.Itoa(this.RoomId)
		uid := strconv.Itoa(toUserId)

		// sdk服务器 ==> 踢出房间用户
		code, message := zego.KickoutUser(roomId, uid)
		log.Debug("room.delMember(zego.KickoutUser) userId=%s roomId=%s code=%d message=%s",
			uid, roomId, code, message)
	}

	go func() {
		// 更新会员数
		database.UpdateMemberCount(this.RoomId, this.MemberCount)

		// 给所有用户发送通知
		this.notify(pb.Notify_Action_Refresh_User, pb.ReasonData{
			Reason:    pb.Notify_Reason_User_ChangeRole,
			UserId:    toUserId,
			OldRoleId: roleId,
			NewRoleId: pb.Role_Guest,
		})
	}()

	return 1
}

// 成员列表
func (this *Room) GetMembers() []pb.UserRoomInfo {
	return this.members
}

// 成员信息
func (this *Room) GetMemberInfo(userId int) pb.UserRoomInfo {
	for _, v := range this.members {
		if v.UserId == userId {
			return v
		}
	}

	return pb.UserRoomInfo{}
}

// 加入
func (this *Room) AddJoin(userId, roleId int) int {
	// log.Debug("room_member.addJoin userId=%d roomId=%d roleId=%d", userId, this.RoomId, roleId)

	if m := this.GetMemberInfo(userId); m.UserId > 0 {
		return 11
	}

	this.members = append(this.members, pb.UserRoomInfo{
		UserId: userId,
		RoomId: this.RoomId,
		RoleId: roleId,
		Level:  1,
		Crdate: common.GetNowTimeStr(),
	})
	this.MemberCount = len(this.members)

	go func() {
		// 给所有用户发送通知
		this.notify(pb.Notify_Action_Refresh_User, pb.ReasonData{
			Reason:    pb.Notify_Reason_User_ChangeRole,
			UserId:    userId,
			OldRoleId: pb.Role_Guest,
			NewRoleId: roleId,
		})

		// 通知房间变化
		this.notify(pb.Notify_Action_Refresh_Room, pb.ReasonData{})

		// 更新会员数
		database.UpdateMemberCount(this.RoomId, this.MemberCount)
	}()

	return 1
}

// 取消加入
func (this *Room) DelJoin(userId int) int {
	var roleId int

	for i := 0; i < len(this.members); i++ {
		if this.members[i].UserId != userId {
			continue
		}

		// 判断是否是管理员
		if this.members[i].RoleId == pb.Role_Administrator {
			return 11
		}

		roleId = this.members[i].RoleId

		this.members = append(this.members[:i], this.members[i+1:]...)
		this.MemberCount = len(this.members)
		break
	}

	go func() {
		// 给所有用户发送通知
		this.notify(pb.Notify_Action_Refresh_User, pb.ReasonData{
			Reason:    pb.Notify_Reason_User_ChangeRole,
			UserId:    userId,
			OldRoleId: roleId,
			NewRoleId: pb.Role_Guest,
		})

		// 通知房间变化
		this.notify(pb.Notify_Action_Refresh_Room, pb.ReasonData{})

		// 更新会员数
		database.UpdateMemberCount(this.RoomId, this.MemberCount)
	}()

	return 1
}

// 更新会员等级(只改缓存数据)
func (this *Room) UpdateMemberLevel(userId, level, exps int) {
	for k, v := range this.members {
		if v.UserId != userId {
			continue
		}

		this.members[k].Level = level
		this.members[k].Exps = exps
		break
	}

	return
}
