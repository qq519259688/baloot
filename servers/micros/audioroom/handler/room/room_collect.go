package room

import (
	"bet24.com/servers/common"
	"bet24.com/servers/micros/audioroom/handler/config"
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/database"
	item "bet24.com/servers/micros/item_inventory/proto"
)

// 加载收集奖励
func (this *Room) loadCollect() {
	list := database.GetCollectList(this.RoomId)
	for i, _ := range list {
		this.collectList = append(this.collectList, &list[i])
	}

	return
}

// 添加收集点数
func (this *Room) AddCollect(point int) {
	if point <= 0 {
		return
	}

	// 今天日索引
	dayIndex := common.GetDayIndex(common.GetTimeStamp())

	for _, v := range this.collectList {
		if v.DayIndex != dayIndex {
			continue
		}

		v.Points += point

		// TODO:更新数据库
		go database.UpdateCollect(this.RoomId, v)
		return
	}

	info := &pb.RoomCollect{
		DayIndex: dayIndex,
		Points:   point,
		Ratio:    0,
		Award:    nil,
		Status:   0,
	}
	this.collectList = append(this.collectList, info)

	// TODO:更新数据库
	go database.UpdateCollect(this.RoomId, info)

	return
}

// 获取收集奖励
func (this *Room) GetCollect() *pb.RoomCollect {
	// 今天日索引
	dayIndex := common.GetDayIndex(common.GetTimeStamp())

	upgradeInfo := config.Mgr.GetUpgradeInfo(this.Level, true)

	for _, v := range this.collectList {
		if v.DayIndex != dayIndex-1 {
			continue
		}

		// 低于显示点数条件
		if v.Points < config.Mgr.GetRoomConfig().CollectShowPoint {
			return v
		}

		// 已完成、已领取,直接返回
		if v.Status == pb.CollectStatus_Complete || v.Status == pb.CollectStatus_Awarded {
			return v
		}

		v.Status = pb.CollectStatus_Complete
		v.Ratio = upgradeInfo.Ratio

		amount := float64(v.Points) * (v.Ratio / 100.00)
		v.Award = append(v.Award, item.ItemPack{
			ItemId: item.Item_Chip,
			Count:  int(amount),
		})

		// TODO：存入数据库
		go database.UpdateCollect(this.RoomId, v)

		return v
	}

	// 初始化数据
	info := &pb.RoomCollect{
		DayIndex: dayIndex - 1,
		Points:   0,
		Ratio:    upgradeInfo.Ratio,
		Award:    nil,
		Status:   0,
	}
	this.collectList = append(this.collectList, info)
	return info
}

// 领取收集奖励
func (this *Room) GiftCollect(userId int) (retCode int, message string, items []item.ItemPack) {
	// 判断是否是管理员
	if this.UserId != userId {
		retCode, message = 11, "not homeowner"
		return
	}

	// 今天日索引
	dayIndex := common.GetDayIndex(common.GetTimeStamp())

	for _, v := range this.collectList {
		if v.DayIndex != dayIndex-1 {
			continue
		}

		// 活动中
		if v.Status == pb.CollectStatus_Active {
			retCode, message = 12, "collecting"
			return
		}

		// 判断是否已经领取
		if v.Status == pb.CollectStatus_Awarded {
			retCode, message = 13, "already received"
			return
		}

		// 设置为领取
		v.Status = pb.CollectStatus_Awarded

		// 统计收集的钻石总数
		for _, a := range v.Award {
			if a.ItemId != item.Item_Chip {
				continue
			}
			this.CollectDiamond += a.Count
		}

		go func() {
			// TODO:保存数据库
			database.UpdateCollect(this.RoomId, v)
			database.UpdateExtInfo(this.RoomId, &this.RoomExtInfo)

			// 给道具
			item.AddItems(this.UserId, v.Award, "语音房收集", common.LOGTYPE_AUDIOROOM_COLLECT)
		}()

		// 返回
		retCode, message, items = 1, "success", v.Award
		return
	}

	retCode, message = 14, "not data"
	return
}
