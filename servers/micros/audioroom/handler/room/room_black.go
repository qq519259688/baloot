package room

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/database"
	"bet24.com/servers/zego"
	"strconv"
	"time"
)

// 加载黑名单列表
func (this *Room) loadBlackList() {
	blackList := database.GetBlackList(this.RoomId)
	if len(blackList) <= 0 {
		return
	}

	this.blackList = blackList
}

// 获取黑名单列表
func (this *Room) GetBlackList() []pb.BlackInfo {
	return this.blackList
}

// 获取黑名单信息
func (this *Room) GetBlackInfo(userId int) *pb.BlackInfo {
	// 黑名单列表
	for i := 0; i < len(this.blackList); i++ {
		if this.blackList[i].UserId == userId {
			return &this.blackList[i]
		}
	}
	return nil
}

// 是否黑名单
func (this *Room) IsBlack(userId, blackType int) bool {
	if blackType <= pb.BlackType_Invalid || blackType >= pb.BlackType_Max {
		return false
	}

	blackInfo := this.GetBlackInfo(userId)
	if blackInfo == nil {
		return false
	}

	for _, t := range blackInfo.BlackTypes {
		if t.BlackType != blackType {
			continue
		}

		// 是否在有效期内
		if t.ExpireTimeStamp >= common.GetTimeStamp() {
			return true
		}
	}

	return false
}

// 是否有黑名单权限
func (this *Room) IsBlackPermission(userId, toUserId, blackType int) bool {
	permissionType := 0

	switch blackType {
	case pb.BlackType_Room:
		permissionType = pb.PermissionType_Room
	case pb.BlackType_Mic:
		permissionType = pb.PermissionType_Mic
	}

	return this.IsSetPermission(userId, toUserId, permissionType)
}

// 添加黑名单
func (this *Room) AddBlack(userId, toUserId, blackType, seconds int) int {

	//log.Debug("room_black.addBlack userId=%d toUserId=%d blackType=%d seconds=%d",
	//	userId, toUserId, blackType, seconds)

	// 判断是否有权限
	if toUserId <= 0 {
		return 12
	}

	// 判断是否有权限
	if !this.IsBlackPermission(userId, toUserId, blackType) {
		return 12
	}

	// 默认值
	expireTimeStamp := common.GetStamp(time.Now().AddDate(10, 0, 0))

	// 计算过期时间戳
	if blackType == pb.BlackType_Room && seconds > 0 {
		expireTimeStamp = common.GetTimeStamp() + seconds
	}

	// 获取黑名单信息
	blackInfo := this.GetBlackInfo(toUserId)

	// 新增黑名单数据
	if blackInfo == nil {
		blackInfo := pb.BlackInfo{
			UserId: toUserId,
		}
		blackInfo.BlackTypes = append(blackInfo.BlackTypes, pb.BlackType{
			BlackType:       blackType,
			ExpireTimeStamp: expireTimeStamp,
		})

		// 添加黑名单列表
		this.blackList = append(this.blackList, blackInfo)

		// 添加黑名单处理
		go this.AddBlackHandler(userId, toUserId, blackType, expireTimeStamp)

		return 1
	}

	// 已在黑名单列表
	for i := 0; i < len(blackInfo.BlackTypes); i++ {
		if blackInfo.BlackTypes[i].BlackType != blackType {
			continue
		}

		// 修改设置过期时间
		blackInfo.BlackTypes[i].ExpireTimeStamp = expireTimeStamp

		// 添加黑名单处理
		go this.AddBlackHandler(userId, toUserId, blackType, expireTimeStamp)

		return 1
	}

	// 新增该黑名单类型
	blackInfo.BlackTypes = append(blackInfo.BlackTypes, pb.BlackType{
		BlackType:       blackType,
		ExpireTimeStamp: expireTimeStamp,
	})

	// 添加黑名单处理
	go this.AddBlackHandler(userId, toUserId, blackType, expireTimeStamp)

	return 1
}

// 添加黑名单处理
func (this *Room) AddBlackHandler(userId, toUserId, blackType, expireTimeStamp int) {

	//log.Debug("room_black.addBlackHandler userId=%d toUserId=%d blackType=%d", userId, toUserId, blackType)

	// 判断是否在麦位
	for i := 0; i < len(this.mics); i++ {
		if this.mics[i].UserId != toUserId {
			continue
		}

		roomId := strconv.Itoa(this.RoomId)
		uid := strconv.Itoa(toUserId * -1)

		// sdk服务器 ==> 删除房间流
		code, message := zego.MuteUser(roomId, uid, this.mics[i].StreamId)
		log.Debug("room.addBlackHandler(zego.MuteUser) i=%d userId=%s roomId=%s streamId=%s code=%d message=%s",
			i, uid, roomId, this.mics[i].StreamId, code, message)

		// 清空麦位
		this.mics[i] = pb.MicInfo{
			UserId:   0,
			StreamId: "",
		}

		// 麦位拉黑，给所有人发
		go this.notify(pb.Notify_Action_Refresh_Mic, pb.ReasonData{
			Reason: pb.Notify_Reason_Mic_AddMicBlack,
			UserId: toUserId,
		})

		break
	}

	operateType := 0

	// 黑名单房间
	if blackType == pb.BlackType_Room {
		operateType = pb.OperateType_AddBlack

		// 给所有用户发送通知
		// this.userNotify(toUserId, pb.UserNotify_Black, pb.Role_Guest)
		this.notify(pb.Notify_Action_Refresh_User, pb.ReasonData{
			Reason: pb.Notify_Reason_User_AddRoomBlack,
			UserId: toUserId,
		})

		// 删除成员
		this.DelMember(userId, toUserId, true)
	} else if blackType == pb.BlackType_Mic {
		operateType = pb.OperateType_ForbidMic

		// 麦位拉黑，刷新用户
		this.notify(pb.Notify_Action_Refresh_User, pb.ReasonData{
			Reason: pb.Notify_Reason_User_AddMicBlack,
			UserId: toUserId,
		})
	}

	// 添加日志
	this.AddOperateLog(this.RoomId, userId, toUserId, operateType)

	// 数据库操作
	database.AddBlack(this.RoomId, toUserId, blackType, expireTimeStamp)

	// 删除麦位申请列表
	go this.delMicApply(toUserId)
}

// 移除黑名单
func (this *Room) RemoveBlack(userId, toUserId, blackType int) int {

	//log.Debug("room_black.removeBlack userId=%d toUserId=%d blackType=%d",
	//	userId, toUserId, blackType)

	// 判断是否有权限
	if !this.IsBlackPermission(userId, toUserId, blackType) {
		return 12
	}

	// 获取黑名单信息
	blackInfo := this.GetBlackInfo(toUserId)
	if blackInfo == nil {
		return 13
	}

	for i := 0; i < len(blackInfo.BlackTypes); i++ {
		if blackInfo.BlackTypes[i].BlackType != blackType {
			continue
		}

		// 移除黑名单
		blackInfo.BlackTypes = append(blackInfo.BlackTypes[:i], blackInfo.BlackTypes[i+1:]...)
		break
	}

	// 没有数据
	if len(blackInfo.BlackTypes) <= 0 {
		for i := 0; i < len(this.blackList); i++ {
			if this.blackList[i].UserId != toUserId {
				continue
			}

			// 删除
			this.blackList = append(this.blackList[:i], this.blackList[i+1:]...)
			break
		}
	}

	operateType := 0

	if blackType == pb.BlackType_Room {
		operateType = pb.OperateType_RemoveBlack
	} else if blackType == pb.BlackType_Mic {
		operateType = pb.OperateType_UnforbidMic

		// 麦位拉黑，给所有人发
		this.notify(pb.Notify_Action_Refresh_User, pb.ReasonData{
			Reason: pb.Notify_Reason_User_RemoveMicBlack,
			UserId: toUserId,
		})
	}

	go func() {
		// 添加日志
		this.AddOperateLog(this.RoomId, userId, toUserId, operateType)

		// 数据库操作
		database.RemoveBlack(this.RoomId, toUserId, blackType)
	}()

	return 1
}
