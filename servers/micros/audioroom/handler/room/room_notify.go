package room

import (
	"encoding/json"

	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	notification "bet24.com/servers/micros/notification/proto"
	user "bet24.com/servers/micros/userservices/proto"
)

// 通知
func (this *Room) notify(action int, data pb.ReasonData) {
	if data.UserId > 0 {
		// 获取用户信息
		if u := user.GetUserInfo(data.UserId); u != nil {
			data.NickName = u.NickName
			data.FaceId = u.FaceId
			data.FaceUrl = u.FaceUrl
		}
	}

	log.Debug("room_notify.notify roomId=%d action=%d data=%+v", this.RoomId, action, data)

	buf, _ := json.Marshal(notification.NotificationAudioRoom{
		NotifyId: action,
		RoomId:   this.RoomId,
		Data:     data,
	})

	// 只通知个人（麦位邀请、游戏强弹）
	if data.Reason == pb.Notify_Reason_Mic_Invite || data.Reason == pb.Notify_Reason_User_Mic_Apply_Agree {
		log.Debug("room_notify.notify roomId=%d notifyUserId=%d action=%d buf=%s", this.RoomId, data.NotifyUserId, action, string(buf))
		notification.AddNotification(data.NotifyUserId, notification.Notification_AudioRoom, string(buf))
		return
	}

	for _, uid := range this.onlineUsers {
		// 在线、离线不通知自己
		if data.Reason == pb.Notify_Reason_User_Online || data.Reason == pb.Notify_Reason_User_Offline {
			if uid == data.NotifyUserId {
				continue
			}
		}

		log.Debug("room_notify.notify notifyUserId=%d roomId=%d action=%d buf=%s", uid, this.RoomId, action, string(buf))
		go notification.AddNotification(uid, notification.Notification_AudioRoom, string(buf))
	}
}
