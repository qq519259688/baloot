package room

import (
	"bet24.com/servers/micros/audioroom/handler/game"
	pb "bet24.com/servers/micros/audioroom/proto"
)

// 创建游戏房间
func (this *Room) CreateGameRoom(userId, gameId int, ruleName string) (retMsg pb.RetMsg, gameRoomInfo *pb.GameRoomInfo) {

	// 判断是否有游戏权限
	if !this.IsGamePermission(userId, gameId) {
		retMsg.RetCode, retMsg.Message = 11, "No permission to create rooms"
		return
	}

	// 创建房间,如果返回nil表示创建失败
	_, gameRoomInfo = game.CreateGameRoom(this.RoomId, gameId, ruleName)

	//判断是否成功
	if gameRoomInfo == nil {
		retMsg.RetCode, retMsg.Message = 13, "Failed to create room"
		return
	}

	// 添加房间
	this.gameRoomList = append(this.gameRoomList, gameRoomInfo)

	// 给所有用户发送通知
	go this.notify(pb.Notify_Action_Refresh_Game, pb.ReasonData{
		Reason:   pb.Notify_Reason_Game_CreateRoom,
		GameId:   gameId,
		RuleName: ruleName,
	})

	// 创建房间成功
	retMsg.RetCode, retMsg.Message = 1, "Create room successfully"
	return
}

// 关闭游戏房间
func (this *Room) CloseGameRoom(userId, gameId, roomNo int) (retMsg pb.RetMsg) {
	// 判断是否有游戏权限
	if !this.IsGamePermission(userId, gameId) {
		retMsg.RetCode, retMsg.Message = 11, "No permission to close the room"
		return
	}

	for i := 0; i < len(this.gameRoomList); i++ {
		if this.gameRoomList[i].RoomNo != roomNo {
			continue
		}

		// 关闭房间
		ok := game.CloseGameRoom(this.RoomId, roomNo)
		if !ok {
			retMsg.RetCode, retMsg.Message = 12, "Failed to close room"
			return
		}

		// 删除
		this.gameRoomList = append(this.gameRoomList[:i], this.gameRoomList[i+1:]...)

		// 关闭房间成功
		retMsg.RetCode, retMsg.Message = 1, "Close the room successfully"
		break
	}

	return
}

// 房间结束处理
func (this *Room) OnRoomEnd(roomNo int) {
	for i := 0; i < len(this.gameRoomList); i++ {
		if this.gameRoomList[i].RoomNo != roomNo {
			continue
		}

		// 删除
		this.gameRoomList = append(this.gameRoomList[:i], this.gameRoomList[i+1:]...)
		break
	}

	return
}

// 获取游戏房间列表
func (this *Room) GetGameRoomList(gameId int) []*pb.GameRoomInfo {
	var list []*pb.GameRoomInfo
	for _, v := range this.gameRoomList {
		if gameId > 0 && v.GameId != gameId {
			continue
		}
		list = append(list, v)
	}
	return list
}

// 获取强弹游戏房间
func (this *Room) StrongBombGame() (gameId int, ruleName string) {
	if len(this.gameRoomList) <= 0 {
		return
	}

	gameId = this.gameRoomList[0].GameId
	ruleName = this.gameRoomList[0].RuleName
	return
}
