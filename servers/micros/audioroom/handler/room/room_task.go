package room

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/micros/audioroom/handler/config"
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/database"
	badge "bet24.com/servers/micros/badge/proto"
	"math"
	"sort"
)

// 加载任务列表
func (this *Room) loadTask() {
	list := database.GetTaskList(this.RoomId)
	for i, _ := range list {
		this.taskList = append(this.taskList, &list[i])
	}

	// 初始化任务
	for _, v := range config.Mgr.GetSysTask(true) {
		if t := this.GetTaskInfo(v.Id); t != nil {
			continue
		}

		info := &pb.RoomTask{
			TaskId:    v.Id,
			Schedule:  0,
			CurrNum:   0,
			TimeStamp: common.GetTimeStamp(),
		}

		this.taskList = append(this.taskList, info)

		// TODO:更新数据库
		go database.UpdateTask(this.RoomId, info)
	}

	sort.SliceStable(this.taskList, func(i, j int) bool {
		return this.taskList[i].TaskId < this.taskList[j].TaskId
	})

	return
}

// 获取任务列表
func (this *Room) GetTaskList() []*pb.RoomTask {
	for k, _ := range this.taskList {
		// 过期重置
		if !common.IsSameDay(this.taskList[k].TimeStamp, common.GetTimeStamp()) {
			this.taskList[k].Schedule = 0
			this.taskList[k].CurrNum = 0
		}
	}
	return this.taskList
}

// 获取任务信息
func (this *Room) GetTaskInfo(taskId int) *pb.RoomTask {
	for _, v := range this.taskList {
		if v.TaskId == taskId {
			return v
		}
	}
	return nil
}

// 触发房间任务
func (this *Room) DoTaskAction(userId, action, num, stillExps int) (ok bool, exps int) {
	//log.Debug("room_task.doTaskAction roomId=%d action=%d num=%d stillExps=%d", this.RoomId, action, num, stillExps)

	// 扩展信息
	ext := this.GetExtInfo()

	// 判断是否达到每天经验上限
	if ext.DayExps >= config.Mgr.GetRoomConfig().DayExpLimit {
		return
	}

	// 遍历房间任务
	for _, v := range this.taskList {

		// 获取系统任务
		cfg := config.Mgr.GetTaskConfig(v.TaskId, true)
		if cfg == nil {
			log.Debug("room_task.doTaskAction roomId=%d taskId %d not found", this.RoomId, v.TaskId)
			continue
		}

		// 判断触发动作
		if cfg.Action != action {
			continue
		}

		// 判断是否过期
		if !common.IsSameDay(v.TimeStamp, common.GetTimeStamp()) {
			v.Schedule = 0
			v.CurrNum = 0
		}

		// 判断任务是否完成
		if v.Schedule >= cfg.Target {
			continue
		}

		// 当前数值
		v.CurrNum += num

		if len(cfg.Exps) <= 0 {
			log.Debug("room_task.doTaskAction roomId=%d taskId exps=%+v is invalid", this.RoomId, v.TaskId, cfg.Exps)
			return
		}

		// 计算经验值
		exps = int(math.Floor(float64(v.CurrNum/cfg.NeedNum)*cfg.Exps[0] + 0.5))

		if cfg.Target > 0 && v.Schedule+exps > cfg.Target {
			exps = cfg.Target - v.Schedule
		}

		// 大于个人上限
		if stillExps > 0 && exps > stillExps {
			exps = stillExps
		}

		// 经验值不够,累计数值积累
		if exps <= 0 {
			// TODO:更新数据库
			go database.UpdateTask(this.RoomId, v)
			continue
		}

		// 扣掉升级的所需数值
		v.CurrNum = v.CurrNum % cfg.NeedNum

		// 任务进度
		v.Schedule += exps

		// 更新时间戳
		v.TimeStamp = common.GetTimeStamp()

		// 添加房间经验
		this.Exps += exps
		this.DayExps += exps

		// 判断是否升级
		if newLevel := config.Mgr.GetLevel(this.Exps, true); newLevel > this.Level {
			this.Level = newLevel
			// 徽章进度
			go badge.DoAction(this.UserId, badge.Action_AudioRoom_RoomGrade, 1, badge.Scope{})
		}

		// TODO:更新数据库
		go database.UpdateTask(this.RoomId, v)
		go database.UpdateExps(this.RoomId, this.Level, this.Exps)
		go database.UpdateExtInfo(this.RoomId, &this.RoomExtInfo)
		go database.AddRoomExpLog(userId, this.RoomId, exps)

		ok = true
		return
	}

	return
}

// 获取用户任务统计
func (this *Room) GetUserRoomTaskStat(userId int) []pb.UserRoomTaskStat {
	if userId != this.UserId {
		return nil
	}

	var ret []pb.UserRoomTaskStat

	// 获取完成的任务统计列表
	list := database.GetUserTaskStat(this.RoomId)

	for _, task := range config.Mgr.GetSysTask(false) {
		finishNum := 0
		for _, v := range list {
			if task.Id != v.TaskId {
				continue
			}

			finishNum = v.FinishNum
			break
		}

		ret = append(ret, pb.UserRoomTaskStat{
			TaskId:    task.Id,
			FinishNum: finishNum,
		})
	}

	return ret
}
