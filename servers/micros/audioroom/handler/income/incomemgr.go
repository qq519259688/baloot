package income

import (
	"bet24.com/log"
	"bet24.com/servers/micros/audioroom/handler/config"
	pb "bet24.com/servers/micros/audioroom/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
	"strconv"
	"sync"
	"time"
)

var mgr *incomemgr

// 收益
type incomemgr struct {
	user_list  map[int]*userIncome
	lock       *sync.RWMutex
	onRoomInfo func(roomId int) *pb.RoomInfo
}

func getIncomeManager() *incomemgr {
	if mgr == nil {
		mgr = new(incomemgr)
		mgr.ctor()
	}

	return mgr
}

func (this *incomemgr) ctor() {
	mgr.user_list = make(map[int]*userIncome)
	mgr.lock = &sync.RWMutex{}

	mgr.doCheck()

	log.Debug("incomemgr.ctor starting ...")
	return
}

// 轮询
func (this *incomemgr) doCheck() {
	ticker := time.NewTicker(60 * time.Second)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				go this.checkUserExpire()
			}
		}
	}(ticker)
}

// 检查过期用户
func (this *incomemgr) checkUserExpire() {
	this.lock.Lock()
	defer this.lock.Unlock()

	// 遍历所有用户
	for uid, v := range this.user_list {
		// 判断是否过期
		if !v.isExpire() {
			continue
		}

		// 删除
		delete(this.user_list, uid)

		log.Debug("incomemgr.checkUserExpire userId=%d 已完成清理", uid)
	}

	return
}

// 获取用户信息
func (this *incomemgr) getUser(userId int) *userIncome {
	this.lock.RLock()
	u, ok := this.user_list[userId]
	this.lock.RUnlock()

	if ok {
		u.updateTimeStamp()
		return u
	}

	u = newUserIncome(userId)
	this.lock.Lock()
	this.user_list[userId] = u
	this.lock.Unlock()
	return u
}

// 获取收益信息
func (this *incomemgr) getIncomeInfo(userId, gameId int) (incomeLevel int, goldIncomeRatio, chipIncomeRatio float64) {
	u := this.getUser(userId)
	if u == nil {
		log.Debug("incomemgr.getIncomeInfo userId=%d not exist", userId)
		return
	}

	return u.getIncomeInfo(gameId)
}

// 触发游戏收益(仅房主)
func (this *incomemgr) triggerGameIncome(userId int, data pb.TriggerData) {
	log.Debug("incomemgr.TriggerGameIncome userId=%d data=%+v", userId, data)

	u := this.getUser(data.RoomOwner)
	if u == nil {
		log.Debug("incomemgr.TriggerGameIncome userId=%d not exist", data.RoomOwner)
		return
	}

	u.triggerGameIncome(userId, data.RoomId, data.GameId, data.ItemType, data.Amount)
}

// 触发礼物收益(接收者、房主)
func (this *incomemgr) triggerGiftIncome(userId int, data pb.TriggerData) {
	log.Debug("incomemgr.TriggerGiftIncome userId=%d data=%+v", userId, data)

	// 接收者收益
	if receiver := this.getUser(data.Receiver); receiver != nil {
		receiver.triggerGiftIncome(userId, data.RoomId, pb.UserType_Receiver, data.ItemType, data.Amount)
	} else {
		log.Debug("incomemgr.TriggerGiftIncome userId=%d not exist", data.Receiver)
	}

	// 房主收益
	if roomOwner := this.getUser(data.RoomOwner); roomOwner != nil {
		roomOwner.triggerGiftIncome(userId, data.RoomId, pb.UserType_RoomOwner, data.ItemType, data.Amount)
	} else {
		log.Debug("incomemgr.TriggerGiftIncome userId=%d not exist", data.RoomOwner)
	}

	return
}

// 获取收益记录
func (this *incomemgr) getIncomeLog(roomId, toUserId, fromUserId, itemType int, beginTime, endTime string, pageIndex, pageSize int) (recordCount int, list []pb.IncomeLog) {
	u := this.getUser(toUserId)
	if u == nil {
		log.Debug("incomemgr.GetLog userId=%d not exist", toUserId)
		return
	}
	recordCount, list = u.getIncomeLog(roomId, fromUserId, itemType, beginTime, endTime, pageIndex, pageSize)
	for i := 0; i < len(list); i++ {
		if u := userservices.GetUserInfo(list[i].FromUserId); u != nil {
			list[i].FromNickName = u.NickName
			list[i].FaceUrl = u.FaceUrl
			list[i].FaceId = u.FaceId
		}
	}
	return
}

// 获取用户收益统计
func (this *incomemgr) getUserIncomeStat(roomId, toUserId, fromUserId, itemType, sortType int, beginTime, endTime string, pageIndex, pageSize int) (recordCount int,
	totalGameProfit, totalGiftProfit float64, list []pb.UserIncomeStat) {
	u := this.getUser(toUserId)
	if u == nil {
		log.Debug("incomemgr.GetUserIncomeStat userId=%d not exist", toUserId)
		return
	}
	recordCount, totalGameProfit, totalGiftProfit, list = u.getUserIncomeStat(roomId, fromUserId, itemType, sortType, beginTime, endTime, pageIndex, pageSize)
	for i := 0; i < len(list); i++ {
		if u := userservices.GetUserInfo(list[i].FromUserId); u != nil {
			list[i].FromNickName = u.NickName
			list[i].FaceUrl = u.FaceUrl
			list[i].FaceId = u.FaceId
		}
	}
	return
}

// 获取游戏收益统计
func (this *incomemgr) getGameIncomeStat(roomId, toUserId, itemType, sortType int, beginTime, endTime string, pageIndex, pageSize int) (recordCount int, list []pb.GameIncomeStat) {
	u := this.getUser(toUserId)
	if u == nil {
		log.Debug("incomemgr.GetGameIncomeStat userId=%d not exist", toUserId)
		return
	}

	recordCount, list = u.getGameIncomeStat(roomId, itemType, sortType, beginTime, endTime, pageIndex, pageSize)
	for i := 0; i < len(list); i++ {
		cfg := config.Mgr.GetGameInfo(list[i].GameId)
		list[i].ChineseName = cfg.Name
	}
	return
}

// 设置房间信息订阅
func (this *incomemgr) setRoomInfoSubscriber(onRoomInfo func(roomId int) *pb.RoomInfo) {
	this.onRoomInfo = onRoomInfo
}

// 获取房间信息
func (this *incomemgr) getRoomInfo(roomId int) *pb.RoomInfo {
	if this.onRoomInfo == nil {
		return nil
	}
	return this.onRoomInfo(roomId)
}

// 打印用户
func (this *incomemgr) dumpUser(param1 string) {
	userId := 0
	if param1 != "" {
		userId, _ = strconv.Atoi(param1)
	}

	log.Debug(" ^_^ 开始打印用户收益数据，用户(%d)人", len(this.user_list))

	this.lock.RLock()
	for uid, v := range this.user_list {
		if userId > 0 && userId != uid {
			continue
		}
		v.DumpUser()
	}
	this.lock.RUnlock()

	log.Debug("完成用户数据打印 ^_^")
}
