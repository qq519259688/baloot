package income

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/micros/audioroom/handler/config"
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/database"
	item "bet24.com/servers/micros/item_inventory/proto"
	"sync"
	"time"
)

const seconds = 600 // 过期时长(秒)

// 收益信息
type userIncome struct {
	userId      int // 用户id
	incomeLevel int // 收益等级
	incomeList  []pb.IncomeInfo
	timeStamp   int // 时间戳（用于清理过期数据）
	lock        *sync.RWMutex
}

func newUserIncome(userId int) *userIncome {
	u := new(userIncome)
	u.userId = userId
	u.timeStamp = common.GetTimeStamp() + seconds
	u.lock = &sync.RWMutex{}

	u.loadIncomeLevel()
	u.loadIncomeList()

	return u
}

// 加载收益等级
func (this *userIncome) loadIncomeLevel() {
	incomeLevel := database.GetUserIncomeLevel(this.userId)
	this.lock.Lock()
	defer this.lock.Unlock()
	this.incomeLevel = incomeLevel
}

// 加载数据
func (this *userIncome) loadIncomeList() {
	list := database.GetUserIncomeList(this.userId)
	this.lock.Lock()
	defer this.lock.Unlock()
	this.incomeList = list
	return
}

// 判断是否过期
func (this *userIncome) isExpire() bool {
	if this.timeStamp >= common.GetTimeStamp() {
		return false
	}

	log.Debug("userIncome.IsExpire userId=%d 过期用户清理", this.userId)
	return true
}

// 更新时间戳
func (this *userIncome) updateTimeStamp() {
	this.timeStamp = common.GetTimeStamp() + seconds
	return
}

// 获取收益信息
func (this *userIncome) getIncomeInfo(gameId int) (incomeLevel int, goldIncomeRatio, chipIncomeRatio float64) {

	// 收益等级
	incomeLevel = this.incomeLevel

	// 金币收益比率
	goldIncomeRatio = config.Mgr.GetGameIncomeRatio(gameId, item.Item_Gold, this.incomeLevel)

	// 钻石收益比率
	chipIncomeRatio = config.Mgr.GetGameIncomeRatio(gameId, item.Item_Chip, this.incomeLevel)

	return
}

// 触发游戏收益(注意并发)
func (this *userIncome) triggerGameIncome(fromUserId, roomId, gameId, itemType, amount int) {
	// 获取收益比率
	incomeRatio := config.Mgr.GetGameIncomeRatio(gameId, itemType, this.incomeLevel)

	// 增加收益
	go this.addProfit(roomId, gameId, pb.UserType_RoomOwner, fromUserId, itemType, amount, pb.IncomeType_Game, incomeRatio)
}

// 触发礼物收益(注意并发)
func (this *userIncome) triggerGiftIncome(fromUserId, roomId, userType, itemType, amount int) {

	// 获取收益比率
	var incomeRatio float64

	receiver, roomOwner := config.Mgr.GetGiftIncomeRatio(this.incomeLevel, itemType)

	switch userType {
	case pb.UserType_Receiver:
		incomeRatio = receiver
	case pb.UserType_RoomOwner:
		incomeRatio = roomOwner
	}

	// 增加收益
	go this.addProfit(roomId, 0, userType, fromUserId, itemType, amount, pb.IncomeType_Gift, incomeRatio)
}

// 增加收益
func (this *userIncome) addProfit(roomId, gameId, userType, fromUserId, itemType, amount, incomeType int, incomeRatio float64) {
	log.Debug("user_income.addProfit userId=%d roomId=%d gameId=%d userType=%d fromUserId=%d itemType=%d amount=%d incomeType=%d incomeRatio=%v",
		this.userId, roomId, gameId, userType, fromUserId, itemType, amount, incomeType, incomeRatio)

	// 计算收益
	profit := incomeRatio * float64(amount)
	if profit <= 0 {
		log.Release("income.triggerGiftIncome incomeRatio invalid userId=%d roomId=%d incomeLevel=%d fromUserId=%d itemType=%d amount=%d ratio=%v profit=%v",
			this.userId, roomId, this.incomeLevel, fromUserId, itemType, amount, incomeRatio, profit)
		return
	}

	var (
		itemCount   int     // 数量
		balance     float64 // 结余
		stillProfit float64 // 剩余收益
		totalProfit float64 // 总收益
		isUpdate    bool    // 是否修改
	)

	this.lock.Lock()
	defer this.lock.Unlock()

	// 加收益
	for i := 0; i < len(this.incomeList); i++ {
		if this.incomeList[i].ItemType != itemType {
			continue
		}

		this.incomeList[i].Balance += profit
		stillProfit = this.incomeList[i].Balance

		this.incomeList[i].TotalProfit += profit

		itemCount = int(this.incomeList[i].Balance)
		this.incomeList[i].Balance -= float64(itemCount)

		balance = this.incomeList[i].Balance
		totalProfit = this.incomeList[i].TotalProfit

		isUpdate = true
		break
	}

	// 新增数据
	if !isUpdate {
		stillProfit = profit
		itemCount = int(profit)
		balance = profit - float64(itemCount)
		totalProfit = profit

		this.incomeList = append(this.incomeList, pb.IncomeInfo{
			ItemType:    itemType,
			Balance:     balance,
			TotalProfit: totalProfit,
		})
	}

	go func() {
		// 给道具
		if itemCount > 0 {
			var items []item.ItemPack
			items = append(items, item.ItemPack{
				ItemId: itemType,
				Count:  itemCount,
			})
			item.AddItems(this.userId, items, "income profit", common.LOGTYPE_AUDIOROOM_INCOME)
		}

		// 保存收益
		database.AddUserIncome(this.userId, itemType, balance, totalProfit)

		// 添加收益记录
		database.AddUserIncomeLog(this.userId, roomId, gameId, fromUserId, itemType, incomeType,
			userType, amount, itemCount, profit, stillProfit)
	}()
}

// 获取收益记录
func (this *userIncome) getIncomeLog(roomId, fromUserId, itemType int, beginTime, endTime string, pageIndex, pageSize int) (recordCount int,
	list []pb.IncomeLog) {
	recordCount, list = database.GetIncomeLog(this.userId, roomId, fromUserId, itemType, beginTime, endTime, pageIndex, pageSize)
	return
}

// 获取用户收益统计
func (this *userIncome) getUserIncomeStat(roomId, fromUserId, itemType, sortType int, beginTime, endTime string, pageIndex, pageSize int) (recordCount int,
	totalGameProfit, totalGiftProfit float64, list []pb.UserIncomeStat) {
	recordCount, totalGameProfit, totalGiftProfit, list = database.GetUserIncomeStat(this.userId, roomId, fromUserId, itemType, sortType, beginTime, endTime,
		pageIndex, pageSize)
	return
}

// 获取游戏收益统计
func (this *userIncome) getGameIncomeStat(roomId, itemType, sortType int, beginTime, endTime string, pageIndex, pageSize int) (recordCount int, list []pb.GameIncomeStat) {
	recordCount, list = database.GetGameIncomeStat(this.userId, roomId, itemType, sortType, beginTime, endTime, pageIndex, pageSize)
	return
}

// 打印用户
func (this *userIncome) DumpUser() {
	log.Debug("用户[%d]信息,收益等级(%d),过期时间[%s]:", this.userId, this.incomeLevel, time.Unix(int64(this.timeStamp), 0).Format(common.Layout))
	log.Debug("收益列表打印(%d)个：", len(this.incomeList))
	for _, info := range this.incomeList {
		log.Debug("	%+v", info)
	}
	log.Debug("++++++++++++++++++++++++++++++++++++++++++++++")
}
