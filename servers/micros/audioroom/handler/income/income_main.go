package income

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	"encoding/json"
	"fmt"
)

func Run() {
	getIncomeManager()
}

// 触发游戏收益(仅房主)
func TriggerGameIncome(userId int, data pb.TriggerData) {
	getIncomeManager().triggerGameIncome(userId, data)
}

// 触发礼物收益(接收者、房主)
func TriggerGiftIncome(userId int, data pb.TriggerData) {
	getIncomeManager().triggerGiftIncome(userId, data)
}

// 获取收益信息
func GetIncomeInfo(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user_audioroom.GetIncomeInfo unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	// 房间信息无效
	if r := getIncomeManager().getRoomInfo(req.RoomId); r == nil {
		return ""
	}

	var ret pb.Response_GetIncomeInfo
	ret.IncomeLevel, ret.GoldIncomeRatio, ret.ChipIncomeRatio = getIncomeManager().getIncomeInfo(userId, req.GameId)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取收益记录
func GetIncomeLog(userId int, data string) string {
	retData := "false"
	var req pb.Request_IncomeList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetIncomeLog unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_IncomeList
	ret.RecordCount, ret.List = getIncomeManager().getIncomeLog(req.RoomId, userId, req.FromUserId, req.ItemType,
		req.BeginTime, req.EndTime, req.PageIndex, req.PageSize)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取用户收益统计
func GetUserIncomeStat(userId int, data string) string {
	retData := "false"
	var req pb.Request_IncomeList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetUserIncomeStat unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_IncomeList
	ret.RecordCount, ret.TotalGameProfit, ret.TotalGiftProfit, ret.List = getIncomeManager().getUserIncomeStat(req.RoomId, userId,
		req.FromUserId, req.ItemType, req.SortType, req.BeginTime, req.EndTime, req.PageIndex, req.PageSize)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取游戏收益统计
func GetGameIncomeStat(userId int, data string) string {
	retData := "false"
	var req pb.Request_IncomeList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetGameIncomeStat unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_IncomeList
	ret.RecordCount, ret.List = getIncomeManager().getGameIncomeStat(req.RoomId, userId, req.ItemType, req.SortType, req.BeginTime, req.EndTime, req.PageIndex, req.PageSize)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 订阅房间信息
func SubscribeRoomInfo(onRoomInfo func(roomId int) *pb.RoomInfo) {
	getIncomeManager().setRoomInfoSubscriber(onRoomInfo)
}

// 打印用户
func Dump(param1, param2 string) {
	getIncomeManager().dumpUser(param1)
}
