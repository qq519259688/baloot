package handler

import (
	"bet24.com/log"
	"bet24.com/servers/micros/audioroom/handler/admin"
	"bet24.com/servers/micros/audioroom/handler/config"
	"bet24.com/servers/micros/audioroom/handler/game"
	"bet24.com/servers/micros/audioroom/handler/income"
	"bet24.com/servers/micros/audioroom/handler/manager"
	"bet24.com/servers/micros/audioroom/handler/message"
	pb "bet24.com/servers/micros/audioroom/proto"
	"golang.org/x/net/context"
	"math/rand"
	"time"
)

var instance *audioroom

type audioroom struct {
	logPrint bool // 日志打印
}

func GetInstance() *audioroom {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func newHandler() *audioroom {
	ret := new(audioroom)
	ret.ctor()
	return ret
}

func (g *audioroom) ctor() {
	rand.Seed(time.Now().Unix())

	// 加载配置管理器
	config.LoadConfigMgr()

	// 加载房间管理器
	manager.Run()

	// 加载游戏管理器
	game.Run()

	// 加载收益管理器
	income.Run()

	// 加载后台控制器
	admin.LoadAdminMgr()

	// 加载后台管理器
	manager.LoadAdminRoomMgr()
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func (this *audioroom) dump(cmd, param1, param2 string) {
	switch cmd {
	case "room":
		manager.DumpRoom(param1, param2)
	case "user":
		manager.DumpUser(param1, param2)
	case "game":
		game.Dump(param1, param2)
	case "income":
		income.Dump(param1, param2)
	case "log":
		if param1 == "open" {
			this.logPrint = true
			log.Debug("消息指令跟踪已开启...")
		} else {
			this.logPrint = false
			log.Debug("消息指令跟踪已关闭！")
		}

	case "exit":
		manager.ClearRoomMic()
	default:
		log.Release("audioroom.Dump unhandled cmd %s", cmd)
	}
}

// 指令消息处理
func (this *audioroom) OnAudioRoomMsg(ctx context.Context, req *pb.Request, resp *pb.Response) error {
	resp.Data = message.MessageHandler(req.UserId, req.Msg, req.Data)

	// 打印日志
	if this.logPrint {
		switch req.Msg {
		case "AudioRoomDealApplyOnMic":
			log.Debug("OnAudioRoomMsg request=%+v ==> resp.Data=%s", req, req.Data)
		default:
			log.Debug("OnAudioRoomMsg request=%+v", req)
		}
	}

	return nil
}

// 获取房间信息
func (this *audioroom) GetRoom(ctx context.Context, req *pb.Request, rsp *pb.Response_RoomInfo) error {
	rsp.RoomInfo = manager.GetRoomInfo(req.RoomId)
	return nil
}

// 上报用户投注日志
func (this *audioroom) ReportUserBet(ctx context.Context, req *pb.Request_ReportUserBet, rsp *pb.Response) error {
	game.ReportUserBet(req.RoomId, req.RoomNo, req.GameId, req.RoomName, req.IsChipRoom, req.UserBets)
	return nil
}
