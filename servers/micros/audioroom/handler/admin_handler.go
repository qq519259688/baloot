package handler

import (
	"bet24.com/public"
	"bet24.com/servers/micros/audioroom/handler/admin"
	pb "bet24.com/servers/micros/audioroom/proto"
	"context"
	"encoding/json"
)

// 获取房间列表 - 后台
func (this *audioroom) AdminGetRoomList(ctx context.Context, req *pb.AdminRequest, rsp *pb.AdminResponse) error {
	roomList := admin.Mgr.AdminGetRoomList(req.RoomName, req.SortName, req.SortType, req.RoomId)
	totalCount := len(roomList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	rsp.RecordCount = totalCount
	rsp.List = roomList[start:end]
	return nil
}

// 获取语聊房详细信息 - 后台
func (this *audioroom) AdminGetRoomDetail(ctx context.Context, req *pb.AdminRequest, rsp *pb.AdminResponse) error {
	rsp.List = admin.Mgr.AdminGetRoomDetail(req.RoomId)
	return nil
}

// 获取语聊房成员 - 后台
func (this *audioroom) AdminGetRoomMember(ctx context.Context, req *pb.AdminRequest, rsp *pb.AdminResponse) error {
	memberList := admin.Mgr.AdminGetRoomMember(req.RoomId)
	totalCount := len(memberList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	rsp.RecordCount = totalCount
	rsp.List = memberList[start:end]
	return nil
}

// 获取语聊房房间的在线用户 - 后台
func (this *audioroom) AdminGetRoomOnlineUsers(ctx context.Context, req *pb.AdminRequest, rsp *pb.AdminResponse) error {
	users := admin.Mgr.AdminGetRoomOnlineUsers(req.RoomId)
	userCount := len(users)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, userCount)
	rsp.RecordCount = userCount
	rsp.List = users[start:end]
	return nil
}

// 获取语聊房麦位 - 后台
func (this *audioroom) AdminGetRoomMic(ctx context.Context, req *pb.AdminRequest, rsp *pb.AdminResponse) error {
	micList := admin.Mgr.AdminGetRoomMic(req.RoomId)
	rsp.RecordCount = len(micList)
	rsp.List = micList
	return nil
}

// 获取黑名单用户列表 - 后台
func (this *audioroom) AdminGetBlackList(ctx context.Context, req *pb.AdminRequest, rsp *pb.AdminResponse) error {
	blackList := admin.Mgr.AdminGetBlackList(req.RoomId, req.BlackType)
	totalCount := len(blackList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	rsp.RecordCount = totalCount
	rsp.List = blackList[start:end]
	return nil
}

// 生成系统任务集合
func (this *audioroom) GenerateSysTaskMap(ctx context.Context, req *pb.AdminRequest, rsp *pb.AdminResponse) error {
	sysTaskMap := admin.Mgr.GenerateSysTaskMap(req.SysFlag)
	d, _ := json.Marshal(sysTaskMap)
	rsp.Data = string(d)
	return nil
}

// 获取房间任务列表 - 后台
func (this *audioroom) AdminGetRoomTask(ctx context.Context, req *pb.AdminRequest, rsp *pb.AdminResponse) error {
	taskList := admin.Mgr.AdminGetRoomTask(req.RoomId)
	totalCount := len(taskList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	rsp.RecordCount = totalCount
	rsp.List = taskList[start:end]
	return nil
}
