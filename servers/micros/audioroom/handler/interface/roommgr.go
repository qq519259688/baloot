package _interface

import pb "bet24.com/servers/micros/audioroom/proto"

// 房间管理器接口
type RoomMgr interface {
	GetRoomInfo(roomId int) *pb.RoomInfo                   // 房间信息
	GetOnlineUsers(roomId int) []pb.UserInfo               // 房间在线列表
	UpdateMemberLevel(userId, roomId, level, exps int)     // 更新会员等级
	AddCollect(userId, roomId, point int)                  // 收集点数
	DoRoomTaskAction(userId, roomId, action, num int)      // 房间任务
	DoUserTaskAction(userId, roomId, action, num, ext int) // 用户房间任务
}
