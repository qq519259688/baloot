package manager

import (
	"bet24.com/log"
	"bet24.com/servers/micros/audioroom/handler/room"
	userRoom "bet24.com/servers/micros/audioroom/handler/user"
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/admin"
	user "bet24.com/servers/micros/userservices/proto"
	"sort"
	"sync"
	"time"
)

var AdminMgr *adminroommgr

type adminroommgr struct {
	room *roommgr

	roomExpend map[int]pb.RoomExpend // 所有房间的消耗
	lock       *sync.RWMutex
}

func LoadAdminRoomMgr() {
	AdminMgr = new(adminroommgr)
	if mgr == nil {
		getRoomManager()
	}
	AdminMgr.room = mgr
	AdminMgr.lock = &sync.RWMutex{}
	AdminMgr.startRoomExpendPolling()
	return
}

// 轮询
func (this *adminroommgr) startRoomExpendPolling() {
	ticker := time.NewTicker(10 * time.Minute)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				go this.loadAllRoomExpend()
			}
		}
	}(ticker)
}

// 加载所有房间的消耗
func (this *adminroommgr) loadAllRoomExpend() {
	roomExpend := admin.GetAllRoomExpend()

	this.roomExpend = make(map[int]pb.RoomExpend)
	this.lock.Lock()
	defer this.lock.Unlock()

	for _, v := range roomExpend {
		data := pb.RoomExpend{
			RoomId:         v.RoomId,
			CollectDiamond: v.CollectDiamond,
			DiamondAmount:  v.DiamondAmount,
			CollectGold:    v.CollectGold,
			GoldAmount:     v.GoldAmount,
		}
		this.roomExpend[v.RoomId] = data
	}
}

// 获取所有房间
func (this *adminroommgr) GetAllRoom() []*room.Room {
	this.room.roomLock.RLock()
	defer this.room.roomLock.RUnlock()

	rooms := make([]*room.Room, 0, len(this.room.room_list))
	for k, _ := range this.room.room_list {
		rooms = append(rooms, this.room.room_list[k])
	}

	sort.SliceStable(rooms, func(i, j int) bool {
		return rooms[i].Crdate > rooms[j].Crdate
	})

	sort.SliceStable(rooms, func(i, j int) bool {
		return rooms[i].TopFlag
	})
	return rooms
}

// 获取所有的房间列表 - 后台
func (this *adminroommgr) GetRoomList(searchKey string, roomId int) []pb.AdminRoomInfo {
	var ret []pb.AdminRoomInfo

	this.room.roomLock.RLock()
	for k := range this.room.room_list {
		v := this.room.room_list[k]

		if pb.IsNotContainName(searchKey, roomId, v.RoomName) {
			continue
		}
		if pb.IsNotContainId(searchKey, roomId, v.RoomId) {
			continue
		}

		userInfo := user.GetUserInfo(v.UserId)
		if userInfo == nil {
			continue
		}

		data := pb.AdminRoomInfo{}
		data.RoomId = v.RoomId
		data.RoomName = v.RoomName
		data.UserId = v.UserId
		data.NickName = userInfo.NickName
		data.Level = v.Level
		data.Exps = v.Exps
		data.MemberCount = v.MemberCount
		data.Crdate = v.Crdate
		ret = append(ret, data)
	}
	this.room.roomLock.RUnlock()

	for k, v := range ret {
		data := this.GetRoomExpend(v.RoomId)
		ret[k].CollectDiamond = data.CollectDiamond
		ret[k].DiamondAmount = data.DiamondAmount
		ret[k].CollectGold = data.CollectGold
		ret[k].GoldAmount = data.GoldAmount
	}
	return ret
}

// 获取房间的消耗
func (this *adminroommgr) GetRoomExpend(roomId int) pb.RoomExpend {
	this.lock.RLock()
	data, ok := this.roomExpend[roomId]
	this.lock.RUnlock()
	if !ok {
		return pb.RoomExpend{}
	}
	return data
}

// 获取房间 - 后台
func (this *adminroommgr) GetRoom(roomId int) *room.Room {
	roomInfo := this.room.getRoom(roomId, true)
	if roomInfo == nil {
		log.Debug("adminroommgr.getRoom roomId=%d not exist", roomId)
		return nil
	}
	return roomInfo
}

// 获取用户 - 后台
func (this *adminroommgr) GetUser(userId int) *userRoom.UserRoom {
	userInfo := this.room.getUser(userId, true)
	if userInfo == nil {
		log.Debug("adminroommgr.GetUser userId=%d not exist", userId)
		return nil
	}
	return userInfo
}
