package manager

import (
	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/micros/audioroom/handler/config"
	pb "bet24.com/servers/micros/audioroom/proto"
	badge "bet24.com/servers/micros/badge/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"fmt"
	"strconv"
)

func Run() {
	getRoomManager()
}

func DumpRoom(param1, param2 string) {
	getRoomManager().dumpRoom(param1)
}

func ClearRoomMic() {
	getRoomManager().clearRoomMic()
}

func DumpUser(param1, param2 string) {
	getRoomManager().dumpUser(param1)
}

// 获取房主信息
func getOwnerInfo(userId int) (badgeIds []badge.BadgePosition, decorations []userservices.UserDecoration) {
	u := userservices.GetUserInfo(userId)
	if u != nil {
		badgeIds = u.Badges
		decorations = u.Decorations
	}
	return
}

// 获取房主信息列表
func getOwnerList(list []*pb.RoomInfo) interface{} {
	var ret []pb.Response_RoomInfo
	for _, v := range list {
		u := userservices.GetUserInfo(v.UserId)
		if u == nil {
			continue
		}

		ret = append(ret, pb.Response_RoomInfo{
			RoomInfo:    v,
			Badges:      u.Badges,
			Decorations: u.Decorations,
		})
	}
	return ret
}

// 退出房间通知
func ExitRoomNotify(userId, roomId int) {
	getRoomManager().exitRoom(userId, roomId)
}

// 上麦回调通知
func OnTheMicNotify(userId, roomId int, streamId string) {
	getRoomManager().onTheMicNotify(userId, roomId, streamId)
}

// 下麦回调通知
func OffTheMicNotify(userId, roomId int, streamId string) {
	getRoomManager().offTheMicNotify(userId, roomId, streamId)
}

// 获取房间基本信息
func GetRoomInfo(roomId int) *pb.RoomInfo {
	return getRoomManager().GetRoomInfo(roomId)
}

// 获取房间基本信息
func GetRoomInfoJson(roomId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.getAudioRoom unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_RoomInfo

	ret.RoomInfo = getRoomManager().GetRoomInfo(req.RoomId)
	if ret.RoomInfo != nil {
		ret.Badges, ret.Decorations = getOwnerInfo(ret.RoomInfo.UserId)
	}

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 登录服务器
func LoginServer(userId int, data string) string {
	var ret pb.Response_LoginServer
	ret.Success, ret.RoomInfo = getRoomManager().loginServer(userId)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 退出服务器
func LogoutServer(userId int, data string) string {
	getRoomManager().logoutServer(userId)
	return "1"
}

// 获取房间信息
func GetRoomHotInfo(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetRoomHotInfo unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_RoomHotInfo
	info := getRoomManager().GetRoomInfo(req.RoomId)
	ret.OnlineCount = info.OnlineCount
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 是否允许进入房间
func IsEnterRoom(userId int, data string) string {
	retData := ""
	var req pb.Request_IsEnterRoom
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.IsEnterRoom unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().isEnterRoom(userId, req.RoomId, req.Password)
	return strconv.Itoa(retCode)
}

// 进入房间
func EnterRoom(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("EnterRoom unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_RoomInfo
	ret.RoomInfo = getRoomManager().enterRoom(userId, req.RoomId)
	if ret.RoomInfo != nil {
		ret.Badges, ret.Decorations = getOwnerInfo(ret.RoomInfo.UserId)
	}

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 退出房间
func ExitRoom(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("ExitRoom unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}
	retCode := getRoomManager().exitRoom(userId, req.RoomId)
	return strconv.Itoa(retCode)
}

// 创建房间
func CreateRoom(userId int, data string) string {
	retData := ""
	var req pb.Request_CreateRoom
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.CreateRoom unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_RoomInfo
	ret.RoomInfo = getRoomManager().createRoom(userId, req.RoomName, req.Country, req.IpAddress, req.RoomImg, req.Announce, req.Tag)
	if ret.RoomInfo != nil {
		ret.Badges, ret.Decorations = getOwnerInfo(ret.RoomInfo.UserId)
	}

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 修改房间
func UpdateRoom(userId int, data string) string {
	retData := ""
	var req pb.Response_RoomInfo
	if err := json.Unmarshal([]byte(data), &req.RoomInfo); err != nil {
		retData = fmt.Sprintf("user_audioroom.UpdateRoom unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().updateRoom(userId, req.RoomInfo)
	return strconv.Itoa(retCode)
}

// 获取房间封面
func GetRoomImg(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.getRoomImg unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	// 房间存在,返回房间封面
	if info := getRoomManager().GetRoomInfo(req.RoomId); info != nil {
		return info.RoomImg
	}

	// 房间不存在,返回封面预览
	return getRoomManager().getRoomImg(req.RoomId)
}

// 修改房间封面
func UpdateRoomImg(userId int, data string) string {
	getRoomManager().updateRoomImg(userId, userId, data)
	return ""
}

// 获取用户在线房间
func GetOnlineRoom(userId int, data string) string {
	retCode := getRoomManager().getOnlineRoom(userId)
	return strconv.Itoa(retCode)
}

// 获取探索列表
func GetExploreList(userId int, data string) string {
	retData := ""
	var req pb.Request_GetPageList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetExploreList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList

	exploreList := getRoomManager().getExploreList()
	totalCount := len(exploreList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	ret.RecordCount = totalCount
	ret.List = getOwnerList(exploreList[start:end])

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取加入房间列表
func GetJoinList(userId int, data string) string {
	retData := ""
	var req pb.Request_GetPageList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetJoinList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList

	joinList := getRoomManager().getJoinList(userId)
	totalCount := len(joinList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	ret.RecordCount = totalCount
	ret.List = getOwnerList(joinList[start:end])

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取关注房间列表
func GetAttentionList(userId int, data string) string {
	retData := ""
	var req pb.Request_GetPageList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetAttentionList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList

	attentionList := getRoomManager().getAttentionList(userId)
	totalCount := len(attentionList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	ret.RecordCount = totalCount
	ret.List = getOwnerList(attentionList[start:end])

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取浏览房间列表
func GetBrowseList(userId int, data string) string {
	retData := ""
	var req pb.Request_GetPageList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetBrowseList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList

	browseList := getRoomManager().getBrowseList(userId)
	totalCount := len(browseList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	ret.RecordCount = totalCount
	ret.List = getOwnerList(browseList[start:end])

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 加入(扣费)
func AddJoin(userId int, data string) string {
	retData := ""
	var req pb.Request_AddJoin
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.AddJoin unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().addJoin(userId, req.RoomId, pb.Role_Member, req.IpAddress)
	return strconv.Itoa(retCode)
}

// 取消加入
func DelJoin(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.userId unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().delJoin(userId, req.RoomId)
	return strconv.Itoa(retCode)
}

// 关注
func AddAttention(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("AddAttention unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().addAttention(userId, req.RoomId)
	return strconv.Itoa(retCode)
}

// 取消关注
func DelAttention(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.DelAttention unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}
	retCode := getRoomManager().delAttention(userId, req.RoomId)
	return strconv.Itoa(retCode)
}

// 发出成员邀请
func InviteJoin(userId int, data string) string {
	retData := ""
	var req pb.Request_InviteJoin
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.InviteJoin unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().inviteJoin(userId, req.ToUserId, req.RoomId)
	return strconv.Itoa(retCode)
}

// 接受成员邀请
func AcceptJoin(userId int, data string) string {
	retData := ""
	var req pb.Request_AcceptJoin
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.AcceptJoin unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().acceptJoin(userId, req.Code, req.IpAddress)
	return strconv.Itoa(retCode)
}

// 获取在线用户列表
func GetOnlineUsers(userId int, data string) string {
	retData := ""
	var req pb.Request_GetPageList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetOnlineUsers unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList
	onlineList := getRoomManager().GetOnlineUsers(req.RoomId)
	totalCount := len(onlineList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	ret.RecordCount = totalCount
	ret.List = onlineList[start:end]

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 查询房间在线用户信息
func SearchUser(userId int, data string) string {
	retData := ""
	var req pb.Request_ToUser
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.SearchUser unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getRoomManager().searchUser(userId, req.ToUserId, req.RoomId)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取成员列表
func GetMembers(userId int, data string) string {
	retData := ""
	var req pb.Request_GetPageList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetMembers unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList
	memberList := getRoomManager().getMembers(req.RoomId)
	totalCount := len(memberList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	ret.RecordCount = totalCount
	ret.List = memberList[start:end]

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 搜索
func SearchRoom(userId int, data string) string {
	retData := ""
	var req pb.Request_GetPageList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.SearchRoom unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList
	roomList := getRoomManager().searchRoom(req.RoomName)
	totalCount := len(roomList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	ret.RecordCount = totalCount
	var list []pb.Response_RoomInfo
	for k := range roomList[start:end] {
		v := &roomList[k]

		u := userservices.GetUserInfo(v.UserId)
		if u == nil {
			continue
		}

		list = append(list, pb.Response_RoomInfo{
			RoomInfo:    v,
			Badges:      u.Badges,
			Decorations: u.Decorations,
		})
	}
	ret.List = list

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 推荐用户
func RecommendUser(userId int, data string) string {
	list := getRoomManager().recommendUser()
	buf, _ := json.Marshal(list)
	return string(buf)
}

// 推荐房间
func RecommendRoom(userId int, data string) string {
	list := getOwnerList(getRoomManager().recommendRoom())
	buf, _ := json.Marshal(list)
	return string(buf)
}

// 根据标签获取房间列表
func RoomListByTag(userId int, data string) string {
	retData := ""
	var req pb.Request_GetPageList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.RoomListByTag unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList
	roomList := getRoomManager().roomListByTag(req.Tag)
	totalCount := len(roomList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	ret.RecordCount = totalCount
	ret.List = getOwnerList(roomList[start:end])

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 上麦
func OnTheMic(userId int, data string) string {
	retData := ""
	var req pb.Request_Mic
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.OnTheMic unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().onTheMic(userId, req.RoomId, req.SN)
	return strconv.Itoa(retCode)
}

// 邀请上麦
func InviteOnTheMic(userId int, data string) string {
	retData := ""
	var req pb.Request_Mic
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.InviteOnTheMic unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().inviteOnMic(userId, req.RoomId, req.ToUserId)
	return strconv.Itoa(retCode)
}

// 踢麦(强制下麦)
func KickMic(userId int, data string) string {
	retData := ""
	var req pb.Request_Mic
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.KickMic unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().kickMic(userId, req.RoomId, req.ToUserId)
	return strconv.Itoa(retCode)
}

// 加解锁麦(status -1=上锁  0=解锁)
func SetMic(userId int, data string) string {
	retData := ""
	var req pb.Request_Mic
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.SetMic unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().setMic(userId, req.RoomId, req.SN, req.Status)
	return strconv.Itoa(retCode)
}

// 获取麦列表
func GetMicList(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetMicList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	list := getRoomManager().getMicList(userId, req.RoomId)
	buf, _ := json.Marshal(list)
	return string(buf)
}

// 获取权限
func GetPermission(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetPermission unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	list := getRoomManager().getPermission(userId, req.RoomId)
	buf, _ := json.Marshal(list)
	return string(buf)
}

// 设置权限
func SetPermission(userId int, data string) string {
	retData := ""
	var req pb.Request_SetPermission
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.SetPermission unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().setPermission(userId, req.RoomId, req.PermissionType, req.Enabled)
	return strconv.Itoa(retCode)
}

// 获取进入房间条件
func GetEnterCondition(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetEnterCondition unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetEnterCondition
	ret.EnterCondition = getRoomManager().getEnterCondition(userId, req.RoomId)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 设置进入房间条件
func SetEnterCondition(userId int, data string) string {
	retData := ""
	var req pb.Request_SetEnterCondition
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.SetEnterCondition unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().setEnterCondition(userId, req.RoomId, req.InviteOnly, req.Password, req.IsHide)
	return strconv.Itoa(retCode)
}

// 获取黑名单用户列表
func GetBlackList(userId int, data string) string {
	retData := ""
	var req pb.Request_GetBlackList
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetBlackList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList
	blackList := getRoomManager().getBlackList(userId, req.RoomId, req.BlackType)
	totalCount := len(blackList)
	start, end := public.SlicePage(req.PageIndex, req.PageSize, totalCount)
	ret.RecordCount = totalCount
	ret.List = blackList[start:end]

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 设置黑名单
func AddBlack(userId int, data string) string {
	retData := ""
	var req pb.Request_Black
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.AddBlack unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().addBlack(userId, req.RoomId, req.ToUserId, req.BlackType, req.Seconds)
	return strconv.Itoa(retCode)
}

// 移除黑名单
func RemoveBlack(userId int, data string) string {
	retData := ""
	var req pb.Request_Black
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.RemoveBlack unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().removeBlack(userId, req.RoomId, req.ToUserId, req.BlackType)
	return strconv.Itoa(retCode)
}

// 操作记录
func GetOperateLog(userId int, data string) string {
	retData := ""
	var req pb.Request_GetOperateLog
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetOperateLog unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList
	ret.RecordCount, ret.List = getRoomManager().getOperateLog(userId, req.RoomId, req.ToUserId, req.OperateType, req.PageIndex, req.PageSize)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 添加管理员
func AddAdmin(userId int, data string) string {
	retData := ""
	var req pb.Request_ToUser
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.AddAdmin unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().addAdmin(userId, req.RoomId, req.ToUserId)
	return strconv.Itoa(retCode)
}

// 取消管理员
func DelAdmin(userId int, data string) string {
	retData := ""
	var req pb.Request_ToUser
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.DelAdmin unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}
	retCode := getRoomManager().delAdmin(userId, req.RoomId, req.ToUserId)
	return strconv.Itoa(retCode)
}

// 取消会员
func DelMember(userId int, data string) string {
	retData := ""
	var req pb.Request_ToUser
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.DelMember unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().delMember(userId, req.RoomId, req.ToUserId)
	return strconv.Itoa(retCode)
}

// 是否关注过房间
func IsAttention(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.IsAttention unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().isAttention(userId, req.RoomId)
	return strconv.Itoa(retCode)
}

// 发送礼物
func SendGiving(userId int, data string) string {
	retData := ""
	var req pb.Request_SendGiving
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.SendGiving unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_SendGiving
	ret.Request_SendGiving = req
	ret.RetCode, ret.Message = getRoomManager().sendGiving(userId, req.RoomId, req.ToUserId, req.GiftId, req.Num)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取系统任务
func GetSysTask(userId int, data string) string {
	retData := ""
	var req pb.Request_SysFlag
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetSysTask unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList
	ret.List = config.Mgr.GetSysTask(req.SysFlag)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取升级配置
func GetUpgradeConfig(userId int, data string) string {
	retData := ""
	var req pb.Request_SysFlag
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetUpgradeConfig unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList
	ret.List = config.Mgr.GetUpgradeConfig(req.SysFlag)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取房间任务列表
func GetTaskList(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetTaskList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList
	ret.List = getRoomManager().getTaskList(userId, req.RoomId)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取昨天收集信息
func GetCollect(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetCollect unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList
	ret.List = getRoomManager().getCollect(userId, req.RoomId)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 领取收集奖励
func GiftCollect(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GiftCollect unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GiftCollect
	ret.RetCode, ret.Message, ret.Items = getRoomManager().giftCollect(userId, req.RoomId)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取扩展信息
func GetExtInfo(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetExtInfo unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_RoomExtInfo
	ret.RoomExtInfo = getRoomManager().getRoomExtInfo(req.RoomId)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取用户房间任务列表
func GetUserTaskList(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetUserTaskList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetPageList
	ret.List = getRoomManager().getUserTaskList(userId, req.RoomId)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 分享
func Share(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.Share unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.RetMsg
	ret.RetCode, ret.Message = getRoomManager().share(userId, req.RoomId)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 发送短信
func SendMessage(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.SendMessage unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.RetMsg
	ret.RetCode, ret.Message = getRoomManager().sendMessage(userId, req.RoomId)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 用户房间信息
func GetUserRoomInfo(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetUserRoomInfo unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_GetUserRoomInfo
	info := getRoomManager().getUserRoomInfo(userId, req.RoomId)
	ret.Level = info.Level
	ret.Exps = info.Exps

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取用户任务统计
func GetUserRoomTaskStat(userId int, data string) string {
	var ret pb.Response_List
	ret.List = getRoomManager().getUserRoomTaskStat(userId)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取游戏列表
func GetGameList(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetGameList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	// 游戏列表
	games := config.Mgr.GetGameList()

	// 拷贝数据
	list := make([]pb.GameConfig, len(games))
	copy(list, games)

	// 房间是否有游戏信息
	for i := 0; i < len(list); i++ {
		v := &list[i]

		if len(v.GameRules) <= 0 {
			continue
		}

		// 是否有权限
		v.IsShow = getRoomManager().isGamePermission(userId, req.RoomId, v.Id)

		// 是否开启游戏
		if g := getRoomManager().getGameRoomList(req.RoomId, v.Id); len(g) > 0 {
			v.StartGameRoom = true
		}
	}

	var ret pb.Response_List
	ret.List = list

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取游戏房间列表
func GetGameRoomList(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user_audioroom.GetGameRoomList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_List
	ret.List = getRoomManager().getGameRoomList(req.RoomId, req.GameId)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 通知房间
func NotifyRoom(userId int, data string) string {
	getRoomManager().notifyRoom(userId)
	return ""
}

// 设置屏幕锁（1=锁定，0=解锁）
func SetScreenLock(userId int, data string) string {
	retData := ""
	var req pb.Request_ScreenLock
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.SetScreenLock unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	retCode := getRoomManager().setScreenLock(userId, req.RoomId, req.ScreenLock)
	return strconv.Itoa(retCode)
}

// 是否禁止发言
func IsBannedSpeak(userId int, data string) string {
	retData := "false"
	var req pb.Request_ScreenLock
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.IsBannedSpeak unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	if success := getRoomManager().isBannedSpeak(userId, req.RoomId); success {
		return "true"
	}

	return "false"
}

// 麦位申请列表
func GetOnMicApplyList(userId int, data string) string {
	retData := ""
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.GetOnMicApplyList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	list := getRoomManager().getOnMicApplyList(req.RoomId)
	buf, _ := json.Marshal(list)
	return string(buf)
}

// 申请上麦
func ApplyOnMic(userId int, data string) string {
	retData := "false"
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.ApplyOnMic unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.RetMsg
	info := getRoomManager().applyOnMic(userId, req.RoomId)
	ret.RetCode = info.RetCode
	ret.Message = info.Message

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 取消申请上麦
func CancelApplyOnMic(userId int, data string) string {
	retData := "false"
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.CancelApplyOnMic unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.RetMsg
	info := getRoomManager().cancelApplyOnMic(userId, req.RoomId)
	ret.RetCode = info.RetCode
	ret.Message = info.Message

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 处理申请上麦(1=同意  2=拒绝)
func DealApplyOnMic(userId int, data string) string {
	retData := "false"
	var req pb.Request_Mic
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.DealApplyOnMic unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.RetMsg
	info := getRoomManager().dealApplyOnMic(userId, req.RoomId, req.ToUserId, req.Status)
	ret.RetCode = info.RetCode
	ret.Message = info.Message

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 创建游戏房间
func CreateGameRoom(userId int, data string) string {
	var req pb.Request_CreateGameRoom
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user_audioroom.CreateGameRoom unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_CreateGameRoom
	ret.RetMsg, ret.GameRoomInfo = getRoomManager().createGameRoom(userId, req.RoomId, req.GameId, req.RuleName)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 关闭游戏房间
func CloseGameRoom(userId int, data string) string {
	var req pb.Request_CloseGameRoom
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("user_audioroom.CloseGameRoom unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_CloseGameRoom

	info := getRoomManager().closeGameRoom(userId, req.RoomId, req.GameId, req.RoomNo)
	ret.RetMsg.RetCode = info.RetCode
	ret.RetMsg.Message = info.Message

	buf, _ := json.Marshal(ret)
	return string(buf)
}
