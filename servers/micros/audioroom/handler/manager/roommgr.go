package manager

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	"bet24.com/servers/micros/audioroom/handler/config"
	"bet24.com/servers/micros/audioroom/handler/game"
	"bet24.com/servers/micros/audioroom/handler/income"
	"bet24.com/servers/micros/audioroom/handler/room"
	"bet24.com/servers/micros/audioroom/handler/user"
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/transaction/database"
	item "bet24.com/servers/micros/item_inventory/proto"
	notification "bet24.com/servers/micros/notification/proto"
	task "bet24.com/servers/micros/task/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"sort"
	"strconv"
	"sync"
	"time"
)

const (
	errorCode_user = 101
	errorCode_room = 102
)

var mgr *roommgr

type roommgr struct {
	room_list map[int]*room.Room // 房间列表
	roomLock  *sync.RWMutex      // 房间锁

	user_list map[int]*user.UserRoom // 用户列表
	userLock  *sync.RWMutex          // 用户锁

	preview_list map[int]string // 预览房间图片
}

func getRoomManager() *roommgr {
	if mgr == nil {
		mgr = new(roommgr)

		mgr.room_list = make(map[int]*room.Room)
		mgr.roomLock = &sync.RWMutex{}

		mgr.user_list = make(map[int]*user.UserRoom)
		mgr.userLock = &sync.RWMutex{}

		mgr.preview_list = make(map[int]string)

		mgr.doCheck()

		game.SubscribeRoomEnd(mgr.onRoomEnd)
		game.SubscribeRoomInfo(mgr.GetRoomInfo)
		income.SubscribeRoomInfo(mgr.GetRoomInfo)
	}

	return mgr
}

// 轮询
func (this *roommgr) doCheck() {
	ticker := time.NewTicker(60 * time.Second)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				go this.checkRoomExpire()
				go this.checkUserExpire()
				go this.refreshRoomFromDB()
			}
		}
	}(ticker)
}

// 检查过期房间
func (this *roommgr) checkRoomExpire() {
	this.roomLock.Lock()
	defer this.roomLock.Unlock()

	// 遍历所有房间
	for _, v := range this.room_list {
		// 判断是否过期
		if !v.IsExpire() {
			continue
		}

		// 删除
		delete(this.room_list, v.RoomId)

		log.Debug("roommgr.checkRoomExpire roomId=%d 已完成清理", v.RoomId)
	}

	return
}

// 检查过期用户
func (this *roommgr) checkUserExpire() {
	this.userLock.Lock()
	defer this.userLock.Unlock()

	// 遍历所有用户
	for uid, v := range this.user_list {
		// 判断是否过期
		if !v.IsExpire() {
			continue
		}

		// 删除
		delete(this.user_list, uid)

		log.Debug("roommgr.checkUserExpire userId=%d 已完成清理", uid)
	}

	return
}

// 拉房间数据
func (this *roommgr) refreshRoomFromDB() {
	previousIds := make(map[int]bool)
	for _, id := range config.Mgr.TopRooms {
		info := this.GetRoomInfo(id)
		if info == nil {
			continue
		}
		this.roomLock.Lock()
		info.TopFlag = true
		this.roomLock.Unlock()
		previousIds[id] = true
	}

	if len(this.room_list) > config.Mgr.MemoryRooms {
		return
	}

	// 直接从数据库查询
	rooms := database.RecommendRoom()

	for _, id := range rooms {
		this.GetRoomInfo(id)
	}

	log.Debug("roomgr.refreshRoomFromDB 当前在线 %d 个房间", len(this.room_list))

	if len(previousIds) == 0 {
		return
	}

	this.roomLock.Lock()
	for roomId, info := range this.room_list {
		if !previousIds[roomId] {
			info.TopFlag = false
		}
	}
	this.roomLock.Unlock()
	return
}

// 获取房间
func (this *roommgr) getRoom(roomId int, isFromDB bool) *room.Room {
	if roomId <= 0 {
		log.Release("roommgr.getRoom roomId=%d is invalid", roomId)
		return nil
	}

	this.roomLock.RLock()
	r, ok := this.room_list[roomId]
	this.roomLock.RUnlock()
	if ok {
		r.UpdateTimeStamp()
		return r
	}

	// 不需要从数据库取
	if !isFromDB {
		return nil
	}

	//log.Debug("roommgr.getRoom.newRoom 执行前 roomId=%d ts=%d", roomId, common.GetTimeStamp())
	r = room.NewRoom(roomId, this)
	//log.Debug("roommgr.getRoom.newRoom 执行完 roomId=%d ts=%d", roomId, common.GetTimeStamp())

	// 无效房间
	if !r.IsValid() {
		return nil
	}

	this.roomLock.Lock()
	this.room_list[roomId] = r
	this.roomLock.Unlock()

	return r
}

// 获取用户信息
func (this *roommgr) getUser(userId int, isFromDB bool) *user.UserRoom {
	this.userLock.RLock()
	u, ok := this.user_list[userId]
	this.userLock.RUnlock()

	if ok {
		u.UpdateTimeStamp()
		return u
	}

	// 不需要从数据库获取
	if !isFromDB {
		return nil
	}

	// 创建一个新用户房间数据
	u = user.NewUserRoom(userId, this)

	this.userLock.Lock()
	this.user_list[userId] = u
	this.userLock.Unlock()

	return u
}

// 根据房间名称查找房间
func (this *roommgr) searchRoom(roomName string) []pb.RoomInfo {
	return database.SearchRoom(roomName)
}

// 获取房间基本信息
func (this *roommgr) GetRoomInfo(roomId int) *pb.RoomInfo {
	r := this.getRoom(roomId, true)
	if r == nil {
		//log.Debug("roommgr.getRoomInfo roomId=%d not exist", roomId)
		return nil
	}
	return &r.RoomInfo
}

// 创建房间
func (this *roommgr) createRoom(userId int, roomName, country, ipAddress, roomImg, announce, tag string) *pb.RoomInfo {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.createRoom userId=%d not exist", userId)
		return nil
	}

	r := this.getRoom(userId, true)
	if r != nil {
		log.Debug("roommgr.createRoom is exist userId=%d roomId=%d roomName=%s country=%s ipAddress=%s",
			userId, userId, roomName, country, ipAddress)
		return nil
	}

	// 获取默认语言
	language := config.Mgr.GetDefaultLanguage()

	// 新创建一个
	r = room.NewRoom(userId, this)

	// 创建房间
	r.CreateRoom(userId, roomName, country, language, ipAddress, roomImg, announce, tag)

	// 加入房间列表进行管理
	this.roomLock.Lock()
	this.room_list[r.RoomId] = r
	delete(this.preview_list, userId)
	this.roomLock.Unlock()

	// 加入关注
	u.AddJoin(r.RoomId, pb.Role_Administrator, true, ipAddress)

	return &r.RoomInfo
}

// 是否允许进入房间
func (this *roommgr) isEnterRoom(userId, roomId int, password string) int {
	// 房间信息
	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.isEnterRoom userId=%d roomId=%d not exist", userId, roomId)
		return errorCode_room
	}

	return r.IsEnterRoom(userId, password)
}

// 进入房间
func (this *roommgr) enterRoom(userId, roomId int) *pb.RoomInfo {
	// 用户信息
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.enterRoom userId=%d not exist", userId)
		return nil
	}

	// 房间信息
	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.enterRoom userId=%d roomId=%d not exist", userId, roomId)
		return nil
	}

	// 进入房间
	if ok := r.EnterRoom(userId); !ok {
		return nil
	}

	// 用户房间处理
	go func() {
		// 设置用户在线房间ID
		u.SetOnlineRoom(roomId)

		// 记录浏览历史
		u.AddBrowse(roomId)
	}()
	// 任务：进入语聊房
	task.DoTaskAction(userId, task.TaskAction_enter_room, 1, task.TaskScope{})

	return &r.RoomInfo
}

// 退出房间
func (this *roommgr) exitRoom(userId, roomId int) int {
	r := this.getRoom(roomId, false)
	if r == nil {
		log.Debug("roommgr.exitRoom userId=%d roomId=%d not exist", userId, roomId)
		return 1
	}

	// 退出房间
	r.ExitRoom(userId)

	u := this.getUser(userId, false)
	if u == nil {
		log.Debug("roommgr.exitRoom userId=%d not exist", userId)
		return 1
	}

	// 设置在线房间空闲
	u.SetOnlineRoom(0)

	return 1
}

// 修改房间信息
func (this *roommgr) updateRoom(roomId int, rInfo *pb.RoomInfo) int {
	r := this.getRoom(rInfo.RoomId, true)
	if r == nil {
		log.Error("roommgr.updateRoom userId=%d roomInfo=%+v", roomId, r)
		return errorCode_room
	}

	return r.UpdateRoom(roomId, rInfo)
}

// 获取房间封面预览
func (this *roommgr) getRoomImg(roomId int) string {
	this.roomLock.RLock()
	defer this.roomLock.RUnlock()
	if roomImg, ok := this.preview_list[roomId]; ok {
		return roomImg
	}
	return ""
}

// 修改房间封面
func (this *roommgr) updateRoomImg(userId, roomId int, roomImg string) {
	r := this.getRoom(roomId, true)
	if r == nil {
		// log.Error("roommgr.updateRoomImg userId=%d roomInfo=%+v", roomId, r)

		this.roomLock.Lock()
		this.preview_list[roomId] = roomImg
		this.roomLock.Unlock()

		// 发送通知
		buf, _ := json.Marshal(notification.NotificationAudioRoom{
			NotifyId: pb.Notify_Action_Refresh_RoomImg,
			RoomId:   roomId,
			Data:     roomImg,
		})

		log.Debug("UpdateRoomImg userId=%d roomId=%d roomImg=%s ==> %s", userId, roomId, roomImg, string(buf))

		go notification.AddNotification(userId, notification.Notification_AudioRoom, string(buf))

		return
	}

	r.UpdateRoomImg(roomImg)
}

// 登录服务器
func (this *roommgr) loginServer(userId int) (bool, *pb.RoomInfo) {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.loginServer userId=%d not exist", userId)
		return false, nil
	}

	// 房间信息
	r := this.getRoom(userId, true)
	if r == nil {
		// log.Debug("roommgr.loginServer userId=%d roomId=%d not exist", userId, userId)
		return true, nil
	}

	return true, r.GetRoomInfo()
}

// 退出服务器
func (this *roommgr) logoutServer(userId int) {
	u := this.getUser(userId, false)
	if u == nil {
		log.Debug("roommgr.logoutServer userId=%d not exist", userId)
		return
	}

	this.userLock.Lock()
	defer this.userLock.Unlock()
	delete(this.user_list, userId)
}

// 获取用户在线房间
func (this *roommgr) getOnlineRoom(userId int) int {
	u := this.getUser(userId, false)
	if u == nil {
		log.Debug("roommgr.getOnlineRoom userId=%d not exist", userId)
		return errorCode_user
	}

	return u.GetOnlineRoom()
}

// 获取探索列表
func (this *roommgr) getExploreList() []*pb.RoomInfo {
	this.roomLock.RLock()
	defer this.roomLock.RUnlock()

	var list []*pb.RoomInfo
	for k := range this.room_list {
		v := this.room_list[k]
		list = append(list, &v.RoomInfo)
	}

	// 排序规则优先级：在线人数 --> 经验值;
	sort.SliceStable(list, func(i, j int) bool {
		if list[i].OnlineCount > list[j].OnlineCount {
			return true
		} else if list[i].OnlineCount < list[j].OnlineCount {
			return false
		}

		return list[i].Exps > list[j].Exps
	})

	// 排序规则优先级：置顶房间
	sort.SliceStable(list, func(i, j int) bool {
		return list[i].TopFlag
	})

	return list
}

// 获取加入房间列表
func (this *roommgr) getJoinList(userId int) []*pb.RoomInfo {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.getJoinList userId=%d not exist", userId)
		return nil
	}

	join_List := u.GetJoinList()

	return this.getRoomList(userId, join_List)
}

// 获取关注房间列表
func (this *roommgr) getAttentionList(userId int) []*pb.RoomInfo {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.getAttentionList userId=%d not exist", userId)
		return nil
	}

	attention_List := u.GetAttentionList()

	return this.getRoomList(userId, attention_List)
}

// 获取浏览房间列表
func (this *roommgr) getBrowseList(userId int) []*pb.RoomInfo {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.getBrowseList userId=%d not exist", userId)
		return nil
	}

	browse_list := u.GetBrowseList()

	return this.getRoomList(userId, browse_list)
}

// 获取房间列表信息
func (this *roommgr) getRoomList(userId int, list []int) []*pb.RoomInfo {
	var ret []*pb.RoomInfo

	for _, roomId := range list {
		// 剔除自己创建的房间
		if userId == roomId {
			continue
		}

		r := this.getRoom(roomId, true)
		if r == nil {
			log.Debug("roommgr.getRoomList roomId=%d not exist", roomId)
			continue
		}

		// 房间在线人数为空,不显示在列表中
		//if r.OnlineCount <= 0 {
		//	continue
		//}

		ret = append(ret, r.GetRoomInfo())
	}

	// 排序规则优先级：在线人数 --> 经验值;
	sort.SliceStable(ret, func(i, j int) bool {
		if ret[i].OnlineCount > ret[j].OnlineCount {
			return true
		} else if ret[i].OnlineCount < ret[j].OnlineCount {
			return false
		}

		return ret[i].Exps > ret[j].Exps
	})

	// 排序规则优先级：置顶房间
	sort.SliceStable(ret, func(i, j int) bool {
		return ret[i].TopFlag
	})

	return ret
}

// 加入(扣费)
func (this *roommgr) addJoin(userId, roomId, roleId int, ipAddress string) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.addJoin userId=%d not exist", userId)
		return errorCode_user
	}

	retCode := u.AddJoin(roomId, roleId, false, ipAddress)
	if retCode != 1 {
		return retCode
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.addJoin roomId=%d not exist", roomId)
		return errorCode_room
	}

	return r.AddJoin(userId, roleId)
}

// 取消加入
func (this *roommgr) delJoin(userId, roomId int) int {
	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.delJoin roomId=%d not exist", roomId)
		return errorCode_room
	}

	retCode := r.DelJoin(userId)

	if retCode != 1 {
		return retCode
	}

	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.delJoin userId=%d not exist", userId)
		return errorCode_user
	}

	u.DelJoin(roomId)

	return retCode
}

// 关注
func (this *roommgr) addAttention(userId, roomId int) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.addAttention userId=%d not exist", userId)
		return errorCode_user
	}

	return u.AddAttention(roomId)
}

// 取消关注
func (this *roommgr) delAttention(userId, roomId int) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.delAttention userId=%d not exist", userId)
		return errorCode_user
	}

	return u.DelAttention(roomId)
}

// 发出成员邀请加入
func (this *roommgr) inviteJoin(userId, toUserId, roomId int) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.inviteJoin userId=%d not exist", userId)
		return errorCode_user
	}

	return u.InviteJoin(toUserId, roomId)
}

// 接受成员邀请
func (this *roommgr) acceptJoin(userId int, code, ipAddress string) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.acceptJoin userId=%d not exist", userId)
		return errorCode_user
	}

	roomId, roleId := u.AcceptJoin(code, ipAddress)
	if roomId <= 0 {
		return 11
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.acceptJoin roomId=%d not exist", roomId)
		return errorCode_room
	}

	return r.AddJoin(userId, roleId)
}

// 在线用户列表
func (this *roommgr) GetOnlineUsers(roomId int) []pb.UserInfo {
	var list []pb.UserInfo

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.getOnlineUsers roomId=%d not exist", roomId)
		return nil
	}

	users := r.GetOnlineUsers()

	for _, uid := range users {
		var userInfo pb.UserInfo
		userInfo.UserHotInfo = userservices.GetUserInfo(uid)

		// 是否麦位黑名单
		if r.IsBlack(uid, pb.BlackType_Mic) {
			userInfo.IsMicBlack = true
		}

		// 成员
		member := r.GetMemberInfo(uid)

		// 角色ID
		userInfo.RoleId = member.RoleId
		userInfo.Level = member.Level

		list = append(list, userInfo)
	}

	return list
}

// 查询房间在线用户信息
func (this *roommgr) searchUser(userId, toUserId, roomId int) pb.UserInfo {
	var ret pb.UserInfo

	r := this.getRoom(roomId, false)
	if r == nil {
		log.Debug("roommgr.searchUser roomId=%d not exist", roomId)
		return ret
	}

	s := r.SearchUser(toUserId)
	ret.UserHotInfo = userservices.GetUserInfo(toUserId)
	ret.RoleId = s.RoleId
	ret.Level = s.Level

	// 判断是否麦位黑名单
	if r.IsBlack(toUserId, pb.BlackType_Mic) {
		ret.IsMicBlack = true
	}

	// 判断是否在线
	if u := this.getUser(toUserId, false); u != nil {
		if u.GetOnlineRoom() == roomId {
			ret.IsOnline = true
		}
	}

	return ret
}

// 成员列表
func (this *roommgr) getMembers(roomId int) []pb.UserInfo {
	var list []pb.UserInfo

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.getMembers roomId=%d not exist", roomId)
		return nil
	}

	members := r.GetMembers()

	for _, member := range members {
		var userInfo pb.UserInfo
		userInfo.UserHotInfo = userservices.GetUserInfo(member.UserId)

		// 是否麦位黑名单
		if r.IsBlack(member.UserId, pb.BlackType_Mic) {
			userInfo.IsMicBlack = true
		}

		// 角色ID
		userInfo.RoleId = member.RoleId
		userInfo.Level = member.Level
		userInfo.Exps = member.Exps

		list = append(list, userInfo)
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i].Exps > list[j].Exps
	})

	return list
}

// 修改会员等级(只改缓存数据)
func (this *roommgr) UpdateMemberLevel(userId, roomId, level, exps int) {
	r := this.getRoom(roomId, false)
	if r == nil {
		log.Debug("roommgr.updateMemberLevel roomId=%d not exist", roomId)
		return
	}

	r.UpdateMemberLevel(userId, level, exps)
	return
}

// 推荐用户
func (this *roommgr) recommendUser() []*userservices.UserHotInfo {
	var list []*userservices.UserHotInfo

	// 直接从数据库查询
	users := database.RecommendUser()

	for _, id := range users {
		u := userservices.GetUserInfo(id)
		list = append(list, u)
	}

	return list
}

// 推荐房间
func (this *roommgr) recommendRoom() []*pb.RoomInfo {
	var list []*pb.RoomInfo

	// 直接从数据库查询
	rooms := database.RecommendRoom()

	for _, id := range rooms {
		info := this.GetRoomInfo(id)
		list = append(list, info)
	}

	return list
}

// 根据标签获取房间列表
func (this *roommgr) roomListByTag(tag string) []*pb.RoomInfo {
	var list []*pb.RoomInfo

	this.roomLock.RLock()
	defer this.roomLock.RUnlock()

	for k := range this.room_list {
		v := this.room_list[k]
		if v.Tag != tag {
			continue
		}
		list = append(list, &v.RoomInfo)
	}
	return list
}

// 上麦
func (this *roommgr) onTheMic(userId, roomId, sn int) int {
	log.Debug("roommgr.onTheMic userId=%d roomId=%d sn=%d", userId, roomId, sn)

	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.onTheMic userId=%d not exist", userId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.onTheMic roomId=%d not exist", roomId)
		return errorCode_room
	}

	return r.OnTheMic(userId, sn)
}

// 邀请上麦
func (this *roommgr) inviteOnMic(userId, roomId, toUserId int) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.inviteOnMic userId=%d not exist", userId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.inviteOnMic roomId=%d not exist", roomId)
		return errorCode_room
	}

	return r.InviteOnMic(userId, toUserId)
}

// 踢麦(强制下麦)
func (this *roommgr) kickMic(userId, roomId, toUserId int) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.kickMic userId=%d not exist", userId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.kickMic roomId=%d not exist", roomId)
		return errorCode_room
	}

	return r.KickMic(userId, toUserId)
}

// 上麦回调通知
func (this *roommgr) onTheMicNotify(userId int, roomId int, streamId string) int {
	log.Debug("roommgr.onTheMicNotify userId=%d roomId=%d streamId=%s", userId, roomId, streamId)

	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.onTheMicNotify userId=%d not exist", userId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.onTheMic_callback roomId=%d not exist", roomId)
		return errorCode_room
	}

	return r.OnTheMicNotify(userId, streamId)
}

// 下麦回调通知
func (this *roommgr) offTheMicNotify(userId, roomId int, streamId string) int {
	log.Debug("roommgr.offTheMic userId=%d roomId=%d streamId=%s", userId, roomId, streamId)

	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.offTheMic userId=%d not exist", userId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.offTheMic roomId=%d not exist", roomId)
		return errorCode_room
	}

	return r.OffTheMicNotify(userId, streamId)
}

// 加解锁麦(status -1=上锁  0=解锁)
func (this *roommgr) setMic(userId, roomId, sn, status int) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.setMic userId=%d not exist", userId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.setMic roomId=%d not exist", roomId)
		return errorCode_room
	}

	return r.SetMic(userId, sn, status)
}

// 获取麦列表
func (this *roommgr) getMicList(userId, roomId int) []pb.UserInfo {
	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.getMicList roomId=%d not exist", roomId)
		return nil
	}

	var list []pb.UserInfo

	micList := r.GetMicList()

	for _, uid := range micList {
		var userInfo pb.UserInfo

		// 用户不存在
		if uid <= 0 {
			userInfo.UserHotInfo = &userservices.UserHotInfo{UserId: uid}
			list = append(list, userInfo)
			continue
		}

		userInfo.UserHotInfo = userservices.GetUserInfo(uid)

		// 是否麦位黑名单
		if r.IsBlack(uid, pb.BlackType_Mic) {
			userInfo.IsMicBlack = true
		}

		// 成员
		member := r.GetMemberInfo(uid)

		// 角色ID
		userInfo.RoleId = member.RoleId
		userInfo.Level = member.Level

		list = append(list, userInfo)
	}

	return list
}

// 获取权限
func (this *roommgr) getPermission(userId, roomId int) []pb.PermissionInfo {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.getPermission userId=%d not exist", userId)
		return nil
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.getPermission roomId=%d not exist", roomId)
		return nil
	}

	return r.GetPermissionList()
}

// 设置权限
func (this *roommgr) setPermission(userId, roomId, permissionType, enabled int) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.setPermission userId=%d not exist", userId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.setPermission roomId=%d not exist", roomId)
		return errorCode_room
	}

	return r.SetPermission(permissionType, enabled)
}

// 获取进入房间条件
func (this *roommgr) getEnterCondition(userId, roomId int) pb.EnterCondition {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.getEnterCondition userId=%d not exist", userId)
		return pb.EnterCondition{}
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.getEnterCondition roomId=%d not exist", roomId)
		return pb.EnterCondition{}
	}

	return r.GetEnterCondition(userId)
}

// 设置进入房间条件
func (this *roommgr) setEnterCondition(userId, roomId int, inviteOnly bool, password string, isHide bool) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.setEnterCondition userId=%d not exist", userId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.setEnterCondition roomId=%d not exist", roomId)
		return errorCode_room
	}

	return r.SetEnterCondition(userId, inviteOnly, password, config.Mgr.PwdLockSeconds, isHide)
}

// 获取黑名单用户列表
func (this *roommgr) getBlackList(userId, roomId, blackType int) []pb.BlackUser {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.getBlackList userId=%d not exist", userId)
		return nil
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.getBlackList roomId=%d not exist", roomId)
		return nil
	}

	var list []pb.BlackUser

	for _, v := range r.GetBlackList() {
		for _, b := range v.BlackTypes {
			if b.BlackType != blackType {
				continue
			}

			// 计算剩余时间
			stillSeconds := b.ExpireTimeStamp - common.GetTimeStamp()
			if stillSeconds <= 0 {
				continue
			}

			// 无效用户
			u := userservices.GetUserInfo(v.UserId)
			if u == nil {
				continue
			}

			// 追加数据
			list = append(list, pb.BlackUser{
				UserHotInfo:  u,
				StillSeconds: stillSeconds,
			})
		}
	}

	return list
}

// 设置黑名单
func (this *roommgr) addBlack(userId, roomId, toUserId, blackType, seconds int) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.addBlack userId=%d not exist", userId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.addBlack roomId=%d not exist", roomId)
		return errorCode_room
	}

	retCode := r.AddBlack(userId, toUserId, blackType, seconds)
	if retCode == 1 && blackType == pb.BlackType_Room {
		this.delJoin(toUserId, r.RoomId)
	}

	return retCode
}

// 移除黑名单
func (this *roommgr) removeBlack(userId, roomId, toUserId, blackType int) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.removeBlack userId=%d not exist", userId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.removeBlack roomId=%d not exist", roomId)
		return errorCode_room
	}

	retCode := r.RemoveBlack(userId, toUserId, blackType)
	log.Debug("roommgr.removeBlack userId=%d roomId=%d toUserId=%d blackType=%d retCode=%d",
		userId, roomId, toUserId, blackType, retCode)

	return retCode
}

// 操作记录
func (this *roommgr) getOperateLog(userId, roomId, toUserId, operateType, pageIndex, pageSize int) (int, []pb.OperateInfo) {
	r := this.getRoom(roomId, false)
	if r == nil {
		log.Debug("roommgr.getOperateLog roomId=%d not exist", roomId)
		return 0, nil
	}

	var list []pb.OperateInfo

	recordCount, logList := r.GetOperateLog(toUserId, operateType, pageIndex, pageSize)

	for _, v := range logList {
		var info pb.OperateInfo
		info.UserId = v.UserId
		info.UserBaseInfo = userservices.GetUserInfo(v.UserId)
		info.ToUserId = v.ToUserId
		info.ToUserBaseInfo = userservices.GetUserInfo(v.ToUserId)
		info.OperateType = v.OperateType
		info.Crdate = v.Crdate
		list = append(list, info)
	}

	return recordCount, list
}

// 添加管理员
func (this *roommgr) addAdmin(userId, roomId, toUserId int) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.addAdmin userId=%d not exist", userId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.addAdmin roomId=%d not exist", roomId)
		return errorCode_room
	}

	return r.AddAdmin(userId, toUserId)
}

// 取消管理员
func (this *roommgr) delAdmin(userId, roomId, toUserId int) int {
	toU := this.getUser(toUserId, true)
	if toU == nil {
		log.Debug("roommgr.delAdmin userId=%d not exist", toUserId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.delAdmin roomId=%d not exist", roomId)
		return errorCode_room
	}

	// 取消房间管理员
	retCode := r.DelAdmin(userId, toUserId)

	// 取消管理员为游客
	if retCode == 1 {
		// 取消加入
		go toU.DelJoin(roomId)
	}

	return retCode
}

// 删除会员
func (this *roommgr) delMember(userId, roomId, toUserId int) int {
	toU := this.getUser(toUserId, true)
	if toU == nil {
		log.Debug("roommgr.delMember userId=%d not exist", toUserId)
		return errorCode_user
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.delMember roomId=%d not exist", roomId)
		return errorCode_room
	}

	// 删除会员
	retCode := r.DelMember(userId, toUserId, false)

	log.Debug("roommgr.delMember userId=%d roomId=%d toUserId=%d retCode=%d", userId, roomId, toUserId, retCode)

	// 取消管理员为游客
	if retCode == 1 {
		// 取消加入
		go toU.DelJoin(roomId)
	}

	return retCode
}

// 是否有关注
func (this *roommgr) isAttention(userId, roomId int) int {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.isAttention userId=%d not exist", userId)
		return -1
	}

	return u.IsAttention(roomId)
}

// 发送礼物
func (this *roommgr) sendGiving(userId, roomId, toUserId, giftId, num int) (retCode int, message string) {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.sendGiving userId=%d not exist", userId)
		return errorCode_user, "user is not exist"
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.SendGiving roomId=%d not exist", roomId)
		return errorCode_room, "room is not exist"
	}

	return u.SendGiving(roomId, r.UserId, toUserId, giftId, num)
}

// 获取房间任务列表
func (this *roommgr) getTaskList(userId, roomId int) []*pb.RoomTask {
	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.getTaskList roomId=%d not exist", roomId)
		return nil
	}

	return r.GetTaskList()
}

// 触发房间任务
func (this *roommgr) DoRoomTaskAction(userId, roomId, action, num int) {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.doTaskAction userId=%d not exist", userId)
		return
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.doTaskAction roomId=%d not exist", roomId)
		return
	}

	// 获取用户贡献余额
	ok, stillExps := u.GetContribute(roomId, action)

	// 判断是否允许贡献
	if !ok {
		return
	}

	// 触发房间任务
	ok, exps := r.DoTaskAction(userId, action, num, stillExps)
	if !ok {
		return
	}

	// 添加用户贡献
	u.AddContribute(roomId, action, exps)
}

// 添加收集点数
func (this *roommgr) AddCollect(userId, roomId, point int) {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.addCollect userId=%d not exist", userId)
		return
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.addCollect roomId=%d not exist", roomId)
		return
	}

	r.AddCollect(point)
	return
}

// 获取昨天收集信息
func (this *roommgr) getCollect(userId, roomId int) *pb.RoomCollect {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.getCollect userId=%d not exist", userId)
		return nil
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.getCollect roomId=%d not exist", roomId)
		return nil
	}

	return r.GetCollect()
}

// 领取收集奖励
func (this *roommgr) giftCollect(userId, roomId int) (retCode int, message string, items []item.ItemPack) {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.giftCollect userId=%d not exist", userId)
		return
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.giftCollect roomId=%d not exist", roomId)
		return
	}

	return r.GiftCollect(userId)
}

// 获取扩展信息
func (this *roommgr) getRoomExtInfo(roomId int) *pb.RoomExtInfo {
	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.giftCollect roomId=%d not exist", roomId)
		return nil
	}

	return r.GetExtInfo()
}

// 获取用户房间任务列表
func (this *roommgr) getUserTaskList(userId, roomId int) []*pb.UserRoomTask {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.getUserTaskList userId=%d not exist", userId)
		return nil
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.getUserTaskList roomId=%d not exist", roomId)
		return nil
	}

	return u.GetTaskList(r.RoomId)
}

// 用户分享
func (this *roommgr) share(userId, roomId int) (retCode int, errorMsg string) {
	retCode, errorMsg = 1, "success"

	// 触发用户任务
	go this.DoUserTaskAction(userId, roomId, pb.UserTask_Action_ShareRoom, 1, 0)

	return
}

// 房间内发送信息
func (this *roommgr) sendMessage(userId, roomId int) (retCode int, errorMsg string) {
	retCode, errorMsg = 1, "success"

	// 触发用户任务
	go this.DoUserTaskAction(userId, roomId, pb.UserTask_Action_SendMessage, 1, 0)

	// 触发大厅任务
	go task.DoTaskAction(userId, task.TaskAction_roomChat, 1, task.TaskScope{})

	return
}

// 获取用户房间信息
func (this *roommgr) getUserRoomInfo(userId, roomId int) *pb.UserRoomInfo {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.getUserRoomInfo userId=%d not exist", userId)
		return nil
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.getUserRoomInfo roomId=%d not exist", roomId)
		return nil
	}

	return u.UserRoomInfo(r.RoomId)
}

// 触发用户房间任务
func (this *roommgr) DoUserTaskAction(userId, roomId, action, num, ext int) {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.doUserTaskAction userId=%d not exist", userId)
		return
	}

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.doUserTaskAction roomId=%d not exist", roomId)
		return
	}

	// 触发用户房间任务
	u.DoTaskAction(r.RoomId, action, num, ext)
}

// 获取用户任务统计
func (this *roommgr) getUserRoomTaskStat(userId int) []pb.UserRoomTaskStat {
	u := this.getUser(userId, true)
	if u == nil {
		log.Debug("roommgr.getUserRoomTaskStat userId=%d not exist", userId)
		return nil
	}

	r := this.getRoom(userId, true)
	if r == nil {
		log.Debug("roommgr.getUserRoomTaskStat roomId=%d not exist", userId)
		return nil
	}

	return r.GetUserRoomTaskStat(userId)
}

// 通知房间
func (this *roommgr) notifyRoom(userId int) {
	r := this.getRoom(userId, false)
	if r == nil {
		log.Debug("roommgr.NotifyRoom roomId=%d not exist", userId)
		return
	}
	r.NotifyRoom()
}

// 设置屏幕锁（1=锁定，0=解锁）
func (this *roommgr) setScreenLock(userId, roomId, screenLock int) int {
	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.SetScreenLock roomId=%d not exist", roomId)
		return errorCode_room
	}
	return r.SetScreenLock(userId, screenLock)
}

// 是否禁止发言（true：禁止）
func (this *roommgr) isBannedSpeak(userId, roomId int) bool {
	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.SetScreenLock roomId=%d not exist", roomId)
		return true
	}
	return r.IsBannedSpeak(userId)
}

// 麦位申请列表
func (this *roommgr) getOnMicApplyList(roomId int) []pb.UserInfo {
	var list []pb.UserInfo

	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.GetOnMicApplyList roomId=%d not exist", roomId)
		return list
	}

	users := r.GetOnMicApplyList()
	for _, uid := range users {
		var userInfo pb.UserInfo
		userInfo.UserHotInfo = userservices.GetUserInfo(uid)

		// 是否麦位黑名单
		if r.IsBlack(uid, pb.BlackType_Mic) {
			userInfo.IsMicBlack = true
		}

		// 成员
		member := r.GetMemberInfo(uid)

		// 角色ID
		userInfo.RoleId = member.RoleId
		userInfo.Level = member.Level

		list = append(list, userInfo)
	}

	return list
}

// 申请上麦
func (this *roommgr) applyOnMic(userId, roomId int) pb.RetMsg {
	var retMsg pb.RetMsg
	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.ApplyOnMic roomId=%d not exist", roomId)
		retMsg.RetCode = errorCode_room
		return retMsg
	}

	return r.ApplyOnMic(userId)
}

// 取消申请上麦
func (this *roommgr) cancelApplyOnMic(userId, roomId int) pb.RetMsg {
	var retMsg pb.RetMsg
	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.CancelApplyOnMic roomId=%d not exist", roomId)
		retMsg.RetCode = errorCode_room
		return retMsg
	}

	return r.CancelApplyOnMic(userId)
}

// 处理申请上麦(1=同意  2=拒绝)
func (this *roommgr) dealApplyOnMic(userId, roomId, toUserId, status int) pb.RetMsg {
	var retMsg pb.RetMsg
	r := this.getRoom(roomId, true)
	if r == nil {
		log.Debug("roommgr.DealApplyOnMic roomId=%d not exist", roomId)
		retMsg.RetCode = errorCode_room
		return retMsg
	}

	return r.DealApplyOnMic(userId, toUserId, status)
}

// 创建游戏房间
func (this *roommgr) createGameRoom(userId, roomId, gameId int, ruleName string) (pb.RetMsg, *pb.GameRoomInfo) {
	var retMsg pb.RetMsg
	r := this.getRoom(roomId, false)
	if r == nil {
		log.Debug("roommgr.CreateGameRoom roomId=%d not exist", roomId)
		retMsg.RetCode = errorCode_room
		return retMsg, nil
	}

	return r.CreateGameRoom(userId, gameId, ruleName)
}

// 关闭游戏房间
func (this *roommgr) closeGameRoom(userId, roomId, gameId, roomNo int) pb.RetMsg {
	var retMsg pb.RetMsg
	r := this.getRoom(roomId, false)
	if r == nil {
		log.Debug("roommgr.CloseGameRoom roomId=%d not exist", roomId)
		retMsg.RetCode = errorCode_room
		return retMsg
	}

	return r.CloseGameRoom(userId, gameId, roomNo)
}

// 房间结束处理
func (this *roommgr) onRoomEnd(roomId, roomNo int) {
	r := this.getRoom(roomId, false)
	if r == nil {
		log.Debug("roommgr.onRoomEnd roomId=%d not exist", roomId)
		return
	}

	r.OnRoomEnd(roomNo)
}

// 获取游戏列表
func (this *roommgr) getGameRoomList(roomId, gameId int) []*pb.GameRoomInfo {
	r := this.getRoom(roomId, false)
	if r == nil {
		log.Debug("roommgr.GetGameRoomList roomId=%d not exist", roomId)
		return nil
	}
	return r.GetGameRoomList(gameId)
}

// 是否有游戏权限
func (this *roommgr) isGamePermission(userId, roomId, gameId int) bool {
	r := this.getRoom(roomId, false)
	if r == nil {
		log.Debug("roommgr.IsPermission roomId=%d not exist", roomId)
		return false
	}

	return r.IsGamePermission(userId, gameId)
}

// 清理房间麦位数据
func (this *roommgr) clearRoomMic() {
	for _, v := range this.room_list {
		v.AddOffMicLog(0, "")
	}
	return
}

// 打印房间
func (this *roommgr) dumpRoom(param1 string) {
	roomId := 0
	if param1 != "" {
		roomId, _ = strconv.Atoi(param1)
	}

	log.Debug(" ^_^ 开始打印房间数据，房间(%d)个", len(this.room_list))

	this.roomLock.RLock()
	for k, v := range this.room_list {
		if roomId > 0 && k != roomId {
			continue
		}
		v.DumpRoom()
	}
	this.roomLock.RUnlock()

	log.Debug("完成房间数据打印 ^_^")
}

// 打印用户
func (this *roommgr) dumpUser(param1 string) {
	userId := 0
	if param1 != "" {
		userId, _ = strconv.Atoi(param1)
	}

	log.Debug(" ^_^ 开始打印用户数据，用户(%d)人", len(this.user_list))

	this.userLock.RLock()
	for uid, v := range this.user_list {
		if userId > 0 && userId != uid {
			continue
		}
		v.DumpUser()
	}
	this.userLock.RUnlock()

	log.Debug("完成用户数据打印 ^_^")
}
