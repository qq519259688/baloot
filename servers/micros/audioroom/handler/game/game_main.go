package game

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	"encoding/json"
	"fmt"
)

func Run() {
	getGameManager()
}

func Dump(param1, param2 string) {
	getGameManager().dump(param1, param2)
}

func StartGame(userId int, data string) string {
	retData := ""
	var req pb.Request_StartGame
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData = fmt.Sprintf("user_audioroom.StartGame unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret pb.Response_StartGame

	info := getGameManager().getRoomInfo(req.RoomId)
	if info == nil {
		log.Error("game_main.StartGame userId=%d roomId=%d gameId=%d playType=%d scene=%d",
			userId, req.RoomId, req.GameId, req.PlayType, req.Scene)
		return ""
	}
	ret.RetCode, ret.Message, ret.Result = getGameManager().startGame(userId, req.RoomId, req.GameId, req.PlayType, req.Scene, req.MessageContent, info.UserId)

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 测试期获取房间自动创建,后续改成房主创建
func GetGameRoomInfo(userId int, data string) string {
	var req struct {
		RoomId   int
		GameId   int
		RuleName string
	}
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("game_main.GetGameRoomInfo unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	roomInfo := getGameManager().getGameRoomInfo(req.RoomId, req.GameId, req.RuleName)
	buf, _ := json.Marshal(roomInfo)
	return string(buf)
}

func ReportUserBet(roomId, roomNo, gameId int, roomName string, isChipRoom bool, userBets []pb.UserBet) {
	info := getGameManager().getRoomInfo(roomId)
	if info == nil {
		log.Error("game_main.ReportUserBet roomId=%d roomNo=%d gameId=%d roomName=%s userBets=%+v",
			roomId, roomNo, gameId, roomName, userBets)
		return
	}
	getGameManager().reportUserBet(roomId, roomNo, gameId, roomName, isChipRoom, userBets, info.UserId)
}

// 创建房间,如果返回nil表示创建失败
func CreateGameRoom(roomId, gameId int, ruleName string) (bool, *pb.GameRoomInfo) {
	ok, gr := getGameManager().createGameRoom(roomId, gameId, ruleName)
	if !ok {
		return ok, nil
	}
	return ok, &gr.GameRoomInfo
}

func CloseGameRoom(roomId, roomNo int) bool {
	return getGameManager().closeGameRoom(roomId, roomNo)
}

func SubscribeRoomEnd(onRoomEnd func(roomId int, roomNo int)) {
	getGameManager().setRoomEndSubscriber(onRoomEnd)
}

// 订阅房间信息
func SubscribeRoomInfo(onRoomInfo func(roomId int) *pb.RoomInfo) {
	getGameManager().setRoomInfoSubscriber(onRoomInfo)
}
