package game

import (
	"bet24.com/log"
	"bet24.com/servers/micros/audioroom/handler/config"
)

func (this *gamemgr) OnGameRuleRegistered(gameId int, gameRule string, desc string, targetOptions []int, userOptions []int, playTimeOptions []int) {
	//log.Debug("gamemgr.OnGameRuleRegistered gameId[%d] gameRule[%s]", gameId, gameRule)
	config.Mgr.AddGameRule(gameId, gameRule)
}

func (this *gamemgr) OnGameRuleDeregistered(gameId int, gameRule string) {
	log.Debug("gamemgr.OnGameRuleDeregistered gameId[%d] gameRule[%s]", gameId, gameRule)
	this.removeRoomsByGameRule(gameId, gameRule)
	config.Mgr.RemoveGameRule(gameId, gameRule)
}

func (this *gamemgr) OnRoomStart(roomNo int) {

}
func (this *gamemgr) OnRoomEnd(roomNo int, winners []int) {
	r := this.getRoom(roomNo)
	if r == nil {
		return
	}
	if this.onRoomEnd != nil {
		this.onRoomEnd(r.RoomId, roomNo)
	}

	this.removeRoom(r)
}
func (this *gamemgr) OnRoomUserScoreChanged(roomNo int, userId int, score int) {

}
func (this *gamemgr) OnRoomStatusChanged(roomNo int, old, new int) {

}
