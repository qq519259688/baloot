package config

import (
	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
	"encoding/json"
	"os"
	"sort"
	"time"
)

const (
	roomConfig_key         = "audioroom_config"
	taskConfig_key         = "audioroom_task_config"
	upgradeConfig_key      = "audioroom_upgrade_config"
	user_taskConfig_key    = "audioroom_user_task_config"
	user_upgradeConfig_key = "audioroom_user_upgrade_config"
	gameConfig_key         = "audioroom_game_config"
	gameIncomeConfig_key   = "audioroom_income_game_config"
	giftIncomeConfig_key   = "audioroom_income_gift_config"
)

func (this *configMgr) loadConfig() {
	this.loadRedisConfig()
	time.AfterFunc(5*time.Minute, this.loadConfig)
}

func (this *configMgr) loadRedisConfig() {

	// 1、获取房间配置
	this.loadRoomConfig()

	// 2、获取房间系统任务配置
	this.loadTaskConfig()

	// 3、获取房间等级升级配置
	this.loadUpgradeConfig()

	// 4、获取用户任务配置
	this.loadUserTaskConfig()

	// 5、获取用户等级升级配置
	this.loadUserUpgradeConfig()

	// 6、获取游戏配置
	this.loadGameConfig()

	// 7、获取游戏收益配置
	this.loadGameIncomeConfig()

	// 8、获取礼物收益配置
	this.loadGiftIncomeConfig()

	return
}

// 1、获取房间配置
func (this *configMgr) loadRoomConfig() {
	if data := platformconfig.GetConfig(roomConfig_key); data != "" {
		err := json.Unmarshal([]byte(data), &this.RoomConfig)
		if err == nil {
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
		return
	}

	if data, err := os.ReadFile("serviceconf/audioRoomConfig.json"); err == nil {
		err = json.Unmarshal([]byte(data), &this.RoomConfig)
		if err == nil {
			platformconfig.SetConfig(roomConfig_key, string(data))
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
	} else {
		log.Release("read config failed serviceconf/audioRoomConfig.json %v", err)
	}
}

// 2、获取房间系统任务配置
func (this *configMgr) loadTaskConfig() {
	if data := platformconfig.GetConfig(taskConfig_key); data != "" {
		err := json.Unmarshal([]byte(data), &this.taskConfigs)
		if err == nil {
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
		return
	}

	if data, err := os.ReadFile("serviceconf/audioRoomTaskConfig.json"); err == nil {
		err = json.Unmarshal([]byte(data), &this.taskConfigs)
		if err == nil {
			platformconfig.SetConfig(taskConfig_key, string(data))
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
	} else {
		log.Release("read config failed serviceconf/audioRoomTaskConfig.json %v", err)
	}

}

// 3、获取房间等级升级配置
func (this *configMgr) loadUpgradeConfig() {
	defer func() {
		sort.SliceStable(this.upgradeConfigs, func(i, j int) bool {
			return this.upgradeConfigs[i].Level < this.upgradeConfigs[j].Level
		})
	}()
	if data := platformconfig.GetConfig(upgradeConfig_key); data != "" {
		err := json.Unmarshal([]byte(data), &this.upgradeConfigs)
		if err == nil {
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
		return
	}

	if data, err := os.ReadFile("serviceconf/audioRoomUpgradeConfig.json"); err == nil {
		err = json.Unmarshal([]byte(data), &this.upgradeConfigs)
		if err == nil {
			platformconfig.SetConfig(upgradeConfig_key, string(data))
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
	} else {
		log.Release("read config failed serviceconf/audioRoomUpgradeConfig.json %v", err)
	}
}

// 4、获取用户任务配置
func (this *configMgr) loadUserTaskConfig() {
	if data := platformconfig.GetConfig(user_taskConfig_key); data != "" {
		err := json.Unmarshal([]byte(data), &this.user_taskConfigs)
		if err == nil {
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
		return
	}

	if data, err := os.ReadFile("serviceconf/audioRoomUserTaskConfig.json"); err == nil {
		err = json.Unmarshal([]byte(data), &this.user_taskConfigs)
		if err == nil {
			platformconfig.SetConfig(user_taskConfig_key, string(data))
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
	} else {
		log.Release("read config failed serviceconf/audioRoomUserTaskConfig.json %v", err)
	}

}

// 5、获取用户等级升级配置
func (this *configMgr) loadUserUpgradeConfig() {
	defer func() {
		sort.SliceStable(this.user_upgradeConfigs, func(i, j int) bool {
			return this.user_upgradeConfigs[i].Level < this.user_upgradeConfigs[j].Level
		})
	}()

	if data := platformconfig.GetConfig(user_upgradeConfig_key); data != "" {
		err := json.Unmarshal([]byte(data), &this.user_upgradeConfigs)
		if err == nil {
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
		return
	}

	if data, err := os.ReadFile("serviceconf/audioRoomUserUpgradeConfig.json"); err == nil {
		err = json.Unmarshal([]byte(data), &this.user_upgradeConfigs)
		if err == nil {
			platformconfig.SetConfig(user_upgradeConfig_key, string(data))
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
	} else {
		log.Release("read config failed serviceconf/audioRoomUserUpgradeConfig.json %v", err)
	}
}

// 6、获取游戏配置
func (this *configMgr) loadGameConfig() {
	defer func() {
		sort.SliceStable(this.gameConfigs, func(i, j int) bool {
			return this.gameConfigs[i].Sort < this.gameConfigs[j].Sort
		})

		for _, v := range this.gameConfigs {
			log.Release("  Id[%d]Name[%s]Rules%v", v.Id, v.Name, v.GameRules)
		}
	}()

	if data := platformconfig.GetConfig(gameConfig_key); data != "" {
		err := json.Unmarshal([]byte(data), &this.gameConfigs)
		if err == nil {
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
		return
	}

	if data, err := os.ReadFile("serviceconf/audioRoomGameConfig.json"); err == nil {
		err = json.Unmarshal([]byte(data), &this.gameConfigs)
		if err == nil {
			platformconfig.SetConfig(gameConfig_key, string(data))
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
	} else {
		log.Release("read config failed serviceconf/audioRoomGameConfig.json %v", err)
		return
	}
}

// 7、获取游戏收益配置
func (this *configMgr) loadGameIncomeConfig() {
	if data := platformconfig.GetConfig(gameIncomeConfig_key); data != "" {
		err := json.Unmarshal([]byte(data), &this.gameIncomeConfigs)
		if err == nil {
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
		return
	}

	if data, err := os.ReadFile("serviceconf/audioRoomGameIncomeConfig.json"); err == nil {
		err = json.Unmarshal([]byte(data), &this.gameIncomeConfigs)
		if err == nil {
			platformconfig.SetConfig(gameIncomeConfig_key, string(data))
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
	} else {
		log.Release("read config failed serviceconf/audioRoomGameIncomeConfig.json %v", err)
	}
}

// 8、获取礼物收益配置
func (this *configMgr) loadGiftIncomeConfig() {
	if data := platformconfig.GetConfig(giftIncomeConfig_key); data != "" {
		err := json.Unmarshal([]byte(data), &this.giftIncomeConfigs)
		if err == nil {
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
		return
	}

	if data, err := os.ReadFile("serviceconf/audioRoomGiftIncomeConfig.json"); err == nil {
		err = json.Unmarshal([]byte(data), &this.giftIncomeConfigs)
		if err == nil {
			platformconfig.SetConfig(giftIncomeConfig_key, string(data))
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
	} else {
		log.Release("read config failed serviceconf/audioRoomGiftIncomeConfig.json %v", err)
	}
}
