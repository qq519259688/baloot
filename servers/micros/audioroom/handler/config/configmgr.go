package config

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	"encoding/json"
)

var Mgr *configMgr

type configMgr struct {
	pb.RoomConfig                      // 房间配置
	taskConfigs    []*pb.TaskConfig    // 任务配置
	upgradeConfigs []*pb.UpgradeConfig // 升级配置

	user_taskConfigs    []*pb.TaskConfig    // 用户任务配置
	user_upgradeConfigs []*pb.UpgradeConfig // 升级配置

	gameConfigs []pb.GameConfig // 游戏配置(小游戏、开房游戏)

	gameIncomeConfigs []*pb.GameIncomeConfig // 游戏收益配置
	giftIncomeConfigs []*pb.GiftIncomeConfig // 礼物收益配置
}

func LoadConfigMgr() {
	Mgr = new(configMgr)
	Mgr.loadConfig()
	return
}

// 获取房间配置
func (this *configMgr) GetRoomConfig() pb.RoomConfig {
	return this.RoomConfig
}

// 获取房间配置
func (this *configMgr) GetRoomConfigJson() string {
	buf, _ := json.Marshal(this.RoomConfig)
	return string(buf)
}

// 获取默认语言
func (this *configMgr) GetDefaultLanguage() string {
	language := ""
	if len(this.RoomConfig.Language) > 0 {
		language = this.RoomConfig.Language[0]
	}
	return language
}

// 获取系统任务配置
func (this *configMgr) GetSysTask(sysFlag bool) []*pb.TaskConfig {
	if sysFlag {
		return this.taskConfigs
	}

	return this.user_taskConfigs
}

// 获取系统任务
func (this *configMgr) GetTaskConfig(taskId int, sysFlag bool) *pb.TaskConfig {
	var list []*pb.TaskConfig

	if sysFlag {
		list = append(list, this.taskConfigs...)
	} else {
		list = append(list, this.user_taskConfigs...)
	}

	for _, v := range list {
		if v.Id == taskId {
			return v
		}
	}

	return nil
}

// 获取升级配置
func (this *configMgr) GetUpgradeConfig(sysFlag bool) []*pb.UpgradeConfig {
	if sysFlag {
		return this.upgradeConfigs
	}
	return this.user_upgradeConfigs
}

// 根据经验获取等级
func (this *configMgr) GetLevel(exps int, sysFlag bool) (level int) {
	var list []*pb.UpgradeConfig

	if sysFlag {
		list = append(list, this.upgradeConfigs...)
	} else {
		list = append(list, this.user_upgradeConfigs...)
	}

	for _, v := range list {
		if exps < v.Exps {
			break
		}

		level = v.Level
	}

	return
}

// 获取升级信息
func (this *configMgr) GetUpgradeInfo(level int, sysFlag bool) *pb.UpgradeConfig {
	var list []*pb.UpgradeConfig

	if sysFlag {
		list = append(list, this.upgradeConfigs...)
	} else {
		list = append(list, this.user_upgradeConfigs...)
	}

	for _, v := range list {
		if v.Level == level {
			return v
		}
	}
	return nil
}

// 获取游戏列表
func (this *configMgr) GetGameList() []pb.GameConfig {
	return this.gameConfigs
}

// 添加规则
func (this *configMgr) AddGameRule(gameId int, ruleName string) {
	for k, v := range this.gameConfigs {
		if v.Id != gameId {
			continue
		}
		for _, rn := range v.GameRules {
			if rn == ruleName {
				return
			}
		}
		this.gameConfigs[k].GameRules = append(this.gameConfigs[k].GameRules, ruleName)
		return
	}
}

// 删除规则
func (this *configMgr) RemoveGameRule(gameId int, ruleName string) {
	for k, v := range this.gameConfigs {
		if v.Id != gameId {
			continue
		}
		for i := 0; i < len(v.GameRules); i++ {
			if v.GameRules[i] == ruleName {
				this.gameConfigs[k].GameRules = append(this.gameConfigs[k].GameRules[:i], this.gameConfigs[k].GameRules[i+1:]...)
				return
			}
		}
		return
	}
}

func (this *configMgr) GetDefaultGameRule(gameId int) string {
	for _, v := range this.gameConfigs {
		if v.Id == gameId {
			if len(v.GameRules) > 0 {
				return v.GameRules[0]
			}
			return ""
		}
	}
	return ""
}

func (this *configMgr) IsGameRuleExist(gameId int, ruleName string) bool {
	for _, v := range this.gameConfigs {
		if v.Id != gameId {
			continue
		}
		for _, rn := range v.GameRules {
			if rn == ruleName {
				return true
			}
		}
		return false
	}
	return false
}

// 获取某款游戏信息
func (this *configMgr) GetGameInfo(id int) pb.GameConfig {
	for _, v := range this.gameConfigs {
		if v.Id == id {
			return v
		}
	}
	return pb.GameConfig{}
}

// 获取游戏收益列表
func (this *configMgr) GetGameIncomeList() []*pb.GameIncomeConfig {
	return this.gameIncomeConfigs
}

// 获取游戏收益率
func (this *configMgr) GetGameIncomeRatio(gameId, itemType, level int) (ratio float64) {
	for _, info := range this.gameIncomeConfigs {
		// 游戏id
		if info.GameId != gameId {
			continue
		}

		// 最大返还率
		var maxRatio float64

		// 游戏类型
		switch info.GameType {
		case pb.GameType_Consume: // 1=消耗类型
			maxRatio = 1.00
		case pb.GameType_Rebate: // 2=返利类型
			maxRatio = 0.02
		}

		// 游戏收益
		for _, v := range info.Incomes {
			// 道具类型
			if v.ItemType != itemType {
				continue
			}

			// 等级
			if v.Level != level {
				continue
			}

			// 收益比率异常(防护)
			if v.Ratio < 0 || v.Ratio >= maxRatio {
				log.Error("config.Mgr.GetGameIncomeRatio gameId=%d gameType=%d itemType=%d level=%d ==>%+v",
					gameId, info.GameType, itemType, level, v)
				return
			}

			ratio = v.Ratio
			return
		}
	}

	return
}

// 获取礼物收益列表
func (this *configMgr) GetGiftIncomeList() []*pb.GiftIncomeConfig {
	return this.giftIncomeConfigs
}

// 获取礼物收益率
func (this *configMgr) GetGiftIncomeRatio(level, itemType int) (receiver, roomOwner float64) {
	for _, info := range this.giftIncomeConfigs {
		// 等级
		if info.Level != level {
			continue
		}

		// 礼物收益
		for _, v := range info.Incomes {
			// 道具类型
			if v.ItemType != itemType {
				continue
			}

			// 收益比率异常(防护)
			if v.Receiver < 0 || v.RoomOwner < 0 || v.Receiver >= 1 || v.RoomOwner >= 1 || v.Receiver+v.RoomOwner >= 1 {
				log.Error("config.Mgr.GetGiftIncomeRatio level=%d itemType=%d ==>%+v",
					level, itemType, v)
				return
			}

			receiver = v.Receiver
			roomOwner = v.RoomOwner
			return
		}
	}

	return
}
