package main

import (
	"fmt"

	"bet24.com/redis"
	"bet24.com/servers/micros/audioroom/config"
	"bet24.com/servers/micros/audioroom/handler"
	pb "bet24.com/servers/micros/audioroom/proto"
	"bet24.com/servers/micros/audioroom/router"
	"bet24.com/servers/micros/common"
	"bet24.com/servers/zego"
)

func main() {
	config.Run(pb.ServiceName)
	go waitInput()
	redis.InitPool(config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB)

	zego.InitZego(config.Server.Zego.AppId, config.Server.Zego.ServerSecret)

	//gin 路由
	go router.Run()

	common.RunService(pb.ServiceName, handler.GetInstance(), fmt.Sprintf("localhost:%d", config.Server.ConsulPort))
}

func waitInput() {
	for {
		var cmd string
		var param1 string
		var param2 string
		fmt.Scanf("%s %s %s", &cmd, &param1, &param2)
		switch cmd {
		case "exit":
			handler.Dump(cmd, param1, param2)
			common.DeregisterService(pb.ServiceName)
		default:
			handler.Dump(cmd, param1, param2)
		}
	}
}
