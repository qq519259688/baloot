package proto

import (
	item "bet24.com/servers/micros/item_inventory/proto"
	user "bet24.com/servers/micros/userservices/proto"
)

const (
	Role_Guest         = iota // 0=游客
	Role_Administrator        // 1=管理员
	Role_Assistant            // 2=助理
	Role_Member               // 3=成员
	Role_MAX                  // 无效值
)

// 动作
const (
	Notify_Action_Invalid         = iota // 0=无效
	Notify_Action_Refresh_Room           // 1=刷新房间
	Notify_Action_Refresh_User           // 2=刷新用户
	Notify_Action_Refresh_Mic            // 3=刷新麦
	Notify_Action_Giving                 // 4=礼物
	Notify_Action_Screen_Lock            // 5=屏幕锁
	Notify_Action_Refresh_RoomImg        // 6=刷新房间封面
	Notify_Action_Refresh_Game           // 7=刷新游戏
)

// 原因
const (
	Notify_Reason_None                  = 0  // 0=没有原因
	Notify_Reason_User_Online           = 11 // 11=用户上线(给用户ID数据)
	Notify_Reason_User_Offline          = 12 // 12=用户离线(给用户ID数据)
	Notify_Reason_User_ChangeRole       = 13 // 13=变更角色
	Notify_Reason_User_Invite           = 14 // 14=邀请
	Notify_Reason_User_AddRoomBlack     = 15 // 15=房间拉黑(不需要进一步数据)
	Notify_Reason_User_RemoveRoomBlack  = 16 // 16=移除房间黑名单(不需要通知,弃用)
	Notify_Reason_User_AddMicBlack      = 17 // 17=麦位拉黑(给用户ID数据)
	Notify_Reason_User_RemoveMicBlack   = 18 // 18=移除麦位黑名单
	Notify_Reason_User_Level_Up         = 19 // 19=等级升级
	Notify_Reason_Mic_Change            = 21 // 21=麦位变化(包括锁麦、解锁麦位和自己上麦、主动下麦、扩麦、缩麦、麦数量)
	Notify_Reason_Mic_Invite            = 22 // 22=麦位邀请
	Notify_Reason_Mic_Kick              = 23 // 23=强制下麦(踢麦)
	Notify_Reason_Mic_AddMicBlack       = 24 // 24=麦位拉黑
	Notify_Reason_Mic_RemoveMicBlack    = 25 // 25=移除麦位黑名单(不需要通知,弃用)
	Notify_Reason_Screen_Lock           = 26 // 26=用户屏幕锁
	Notify_Reason_User_Mic_Apply        = 27 // 27=麦位申请
	Notify_Reason_User_Mic_Apply_Agree  = 28 // 28=麦位申请同意
	Notify_Reason_User_Mic_Apply_Cancel = 29 // 29=麦位申请取消(拒绝)
	Notify_Reason_Game_CreateRoom       = 30 // 30=创建游戏房间
	Notify_Reason_Game_CloseRoom        = 31 // 31=关闭游戏房间
)

type ReasonData struct {
	Reason        int    // 原因类型
	NotifyUserId  int    `json:"-"`          // 通知的用户id
	UserId        int    `json:",omitempty"` // 用户id
	NickName      string `json:",omitempty"` // 昵称
	FaceId        int    `json:",omitempty"` // 头像Id
	FaceUrl       string `json:",omitempty"` // 头像Url
	OldRoleId     int    `json:",omitempty"` // 旧角色id
	NewRoleId     int    `json:",omitempty"` // 新角色Id
	SN            int    `json:",omitempty"` // 麦位
	Code          string `json:",omitempty"` // code码
	ScreenLock    int    `json:",omitempty"` // 屏幕锁的状态（1：是，0：否）
	MicApplyCount int    `json:",omitempty"` // 麦位申请数
	GameId        int    `json:",omitempty"` // 游戏id
	RuleName      string `json:",omitempty"` // 游戏玩法
}

const (
	Permission_Close = iota // 0=关闭
	Permission_Open         // 1=开启
)

const (
	PermissionType_Invalid     = iota // 0=无效
	PermissionType_Mic                // 1=麦位
	PermissionType_Room               // 2=房间
	PermissionType_Game_Pinggo        // 3=pinggo 游戏
	PermissionType_Max
)

// 游戏权限
var GamePermissions = []struct {
	GameId         int
	PermissionType int
}{
	{
		GameId:         41,
		PermissionType: PermissionType_Game_Pinggo,
	},
}

const (
	BlackType_Invalid = iota // =0无效
	BlackType_Room           // 1=房间黑名单
	BlackType_Mic            // 2=麦位黑名单
	BlackType_Max
)

const (
	OnMic_All     = iota // 0=房间内所有人自由上麦
	OnMic_Member         // 1=加入为成员才可以上麦
	OnMic_Manager        // 2=仅限管理员邀请或者管理员才能上麦（包含创建者）
	OnMic_Apply          // 3=申请上麦
)

const (
	OperateType_KickMic     = 11 // 11=踢麦
	OperateType_ForbidMic   = 21 // 21=禁麦
	OperateType_UnforbidMic = 22 // 22=解麦
	OperateType_AddBlack    = 31 // 31=添加房间黑名单
	OperateType_RemoveBlack = 32 // 32=移除房间黑名单
)

// 房间配置
type RoomConfig struct {
	SysNotification  string     // 系统公告
	Tag              []string   // 标签
	Country          []string   // 国家
	Language         []string   // 语言
	MicCount         []int      // 上麦数量
	JoinFee          Fee        // 入场会费
	PwdLockSeconds   int        // 房间密码锁有效时长(秒)
	RoomBlackSeconds int        // 房间黑名单时长(秒)
	GivingCount      []int      // 礼物数量
	GivingDiamondMax int        // 最大礼物
	DayExpLimit      int        // 房间经验上限
	DayLimits        []DayLimit // 每天上限
	CollectShowPoint int        // 收集显示点数
	MemoryRooms      int        // 内存房间数
	TopRooms         []int      // 置顶房间
}

// 每天上限
type DayLimit struct {
	Action int // 动作
	Exps   int // 经验值
}

// 入场会费
type Fee struct {
	Min int // 最低费用
	Max int // 最高费用
	Tax int // 税费(百分比)
}

// 语音房间基本信息
type RoomInfo struct {
	RoomId      int    // 房间ID
	RoomName    string // 房间名称
	RoomImg     string // 房间图片
	UserId      int    // 用户ID
	Family      string // 家族
	Country     string // 国家
	Language    string // 语言
	MicCount    int    // 上麦数量
	MicMode     int    // 麦位模式(1=房间内所有人自由上麦  2=房间内所有人自由上麦  3=仅限管理员邀请或者管理员才能上麦（包含创建者）)
	JoinFee     int    // 入会费
	Announce    string // 公告内容
	Tag         string // 标签(游戏、闲聊、交友、家族、仅限女孩)
	MemberCount int    // 会员数
	Crdate      string // 创建时间
	OnlineCount int    // 在线用户数
	Level       int    // 等级
	Exps        int    // 经验
	TopFlag     bool   // 置顶标识
}

// 语音房扩展信息
type RoomExtInfo struct {
	CollectDiamond int // 收集钻石
	DayExps        int // 日经验值
	ScreenLock     int // 屏幕锁（1：是，0：否）
	UpdateTime     int // 更新时间戳
}

// 用户房间信息
type UserRoomInfo struct {
	UserId     int
	RoomId     int    // 房间id
	RoleId     int    // 角色ID(0=游客 1=管理员  2=助理  3=成员  4=关注)
	Level      int    // 等级
	Exps       int    // 经验
	DayExps    int    // 日经验值
	UpdateTime int    // 更新时间戳
	Crdate     string // 创建时间
}

// 麦位信息
type MicInfo struct {
	UserId    int    //  麦位用户(-1=锁定  0=空闲  >0=坐下)
	StreamId  string // 流ID
	TimeStamp int    // 时间戳
}

// 麦位日志
type MicLog struct {
	UserId     int    // 麦位用户(-1=锁定  0=空闲  >0=坐下)
	StreamId   string // 流ID
	RoleID     int    // 角色id
	SN         int    // 麦位序号
	MicMode    int    // 麦位模式
	OnMicStamp int    // 上麦时间戳
	OpUserId   int    // 操作员id
}

// 黑名单
type BlackInfo struct {
	UserId     int         // 用户ID
	BlackTypes []BlackType // 黑名单类型列表
}

// 黑名单类型
type BlackType struct {
	BlackType       int // 黑名单类型(1=房间黑名单  2=麦位黑名单)
	ExpireTimeStamp int // 过期时间戳
}

// 黑名单用户
type BlackUser struct {
	*user.UserHotInfo     // 用户信息
	StillSeconds      int // 剩余时间(秒)
}

// 房间操作记录
type OperateInfo struct {
	UserId         int // 用户ID
	UserBaseInfo   *user.UserHotInfo
	ToUserId       int // 对象用户ID
	ToUserBaseInfo *user.UserHotInfo
	OperateType    int    // 操作类型
	Crdate         string // 时间
}

// 在线用户信息
type UserInfo struct {
	*user.UserHotInfo
	RoleId     int  // 角色id
	IsMicBlack bool // 是否麦位黑名单
	Level      int  // 等级
	Exps       int  `json:",omitempty"` // 经验值
	IsOnline   bool `json:",omitempty"` // 是否在线
}

// 权限信息
type PermissionInfo struct {
	PermissionType int // 权限类型(1=麦位锁)
	Enabled        int // 是否启用(0=未启用  1=已启用)
}

// 房间进入条件
type EnterCondition struct {
	InviteOnly bool   // 只能邀请加入(暂不使用)
	PwdExpire  int    // 过期时间戳(暂不使用)
	Password   string // 密码锁
	IsHide     bool   // 是否隐藏
}

const (
	_                              = iota
	RoomTask_Action_OnMic          // 1=上麦
	RoomTask_Action_Giving_Diamond // 2=赠送钻石礼物
	RoomTask_Action_Giving_Gold    // 3=赠送金币礼物
)

const (
	_                                    = iota
	UserTask_Action_OnMic                // 1=使用麦克风
	UserTask_Action_SendMessage          // 2=房间内发送消息
	UserTask_Action_ShareRoom            // 3=分享房间
	UserTask_Action_AcceptGiving_Diamond // 4=收到钻石礼物
	UserTask_Action_SendGiving_Diamond   // 5=赠送钻石礼物
)

// 房间任务配置
type TaskConfig struct {
	Id        int       // id
	Name      string    // 名称
	Desc      string    // 描述
	Action    int       // 触发动作
	Target    int       // 目标
	NeedNum   int       // 数值
	Exps      []float64 // 经验值
	StatValue int       `json:",omitempty"` // 统计值
}

// 房间升级配置
type UpgradeConfig struct {
	Level int     // 等级
	Exps  int     // 经验
	Icon  int     `json:",omitempty"` // 图标
	Ratio float64 `json:",omitempty"` // 比率
}

// 房间任务
type RoomTask struct {
	TaskId    int // 任务id
	Schedule  int // 进度
	CurrNum   int // 当前数值
	TimeStamp int // 时间戳
}

const (
	CollectStatus_Active   = iota // 0=进行中
	CollectStatus_Complete        // 1=完成
	CollectStatus_Awarded         // 2=已领取奖励
)

// 房间收集
type RoomCollect struct {
	DayIndex int             // 日索引
	Points   int             // 总点数
	Ratio    float64         // 比率
	Award    []item.ItemPack // 奖励
	Status   int             // 状态
}

// 用户贡献
type UserContribute struct {
	RoomId int // 房间Id
	Action int // 动作
	Exps   int // 经验
	Expire int // 过期时间戳
}

// 用户房间任务
type UserRoomTask struct {
	RoomTask
	Status int // 状态
}

// 房间任务统计
type UserRoomTaskStat struct {
	TaskId    int // 任务ID
	FinishNum int // 完成数
}

// 游戏配置
type GameConfig struct {
	Id            int          // ID
	Name          string       // 名称
	Desc          string       // 描述
	Plays         []PlayConfig `json:",omitempty"`
	Sort          int          // 排序
	StartGameRoom bool         `json:",omitempty"` // 游戏房间是否已开启
	GameRules     []string     `json:",omitempty"` // 创建游戏的玩法列表
	IsShow        bool         // 是否显示
}

// 玩法配置
type PlayConfig struct {
	PlayType int       // 玩法类型
	Desc     string    // 描述
	Numbers  []int     // 数字组合
	Fees     []CombFee // 付费组合
}

// 付费(vip、成员、工会)
type CombFee struct {
	Scene int             // 场景,不同的场景有不同的扣费规则
	Desc  string          // 描述
	Items []item.ItemPack // 游戏费用
}

// 礼物记录
type GiftHistory struct {
	RoomId        int // 房间ID
	Sender        int // 发送者
	Receiver      int // 接收者
	GiftId        int // 礼物ID
	GiftNum       int // 礼物数量
	DiamondAmount int // 钻石数
	GoldAmount    int // 金币数
}

// 操作消息
type RetMsg struct {
	RetCode int    // 操作结果
	Message string // 操作消息
}

// 麦位申请状态
const (
	MicApply_Status_Apply  = iota // 0=申请中
	MicApply_Status_Agree         // 1=同意
	MicApply_Status_Reject        // 2=拒绝
	MicApply_Status_Cancel        // 3=取消
)
