package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
	"golang.org/x/net/context"
)

const ServiceName = "audioroom"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	UserId int `json:",omitempty"`
	RoomId int `json:",omitempty"`
	GameId int `json:",omitempty"`
	Msg    string
	Data   string
}

type Response struct {
	Data string
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

// 语聊房消息处理
func OnAudioRoomMsg(userId int, msg, data string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
		Msg:    msg,
		Data:   data,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "OnAudioRoomMsg", args, reply)
	if err != nil {
		log.Debug("audioroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.Data
}

// 获取房间信息
func GetRoom(roomId int) Response_RoomInfo {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		RoomId: roomId,
	}

	reply := &Response_RoomInfo{}

	err := xclient.Call(context.Background(), "GetRoom", args, reply)
	if err != nil {
		log.Debug("audioroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return *reply
}

func ReportUserBet(roomId, roomNo int, gameId int, roomName string, userBets []UserBet, isChipRoom bool) {
	xclient := getClient()
	args := &Request_ReportUserBet{
		RoomId:     roomId,
		RoomNo:     roomNo,
		GameId:     gameId,
		RoomName:   roomName,
		UserBets:   userBets,
		IsChipRoom: isChipRoom,
	}

	err := xclient.Call(context.Background(), "ReportUserBet", args, nil)
	if err != nil {
		log.Debug("audioroom.ReportUserBet failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}
