package proto

import (
	"strings"
)

// 参数请求 - 后台
type AdminRequest struct {
	UserId    int    `json:",omitempty"` // 用户id
	RoomId    int    `json:",omitempty"` // 房间id
	RoomName  string `json:",omitempty"` // 房间名称
	NickName  string `json:",omitempty"` // 用户昵称
	BlackType int    `json:",omitempty"` // 黑名单的类型（1：房间，2：麦位）
	PageIndex int    `json:",omitempty"` // 第几页
	PageSize  int    `json:",omitempty"` // 请求的数量
	SortName  string `json:",omitempty"` // 排序名称
	SortType  string `json:",omitempty"` // 排序类型
	SysFlag   bool   // true：系统，false：用户
}

// 返回值 - 后台
type AdminResponse struct {
	Data        string `json:",omitempty"`
	RecordCount int    `json:",omitempty"`
	List        interface{}
}

// 是否没包含名称
func IsNotContainName(searchKey string, searchKeyId int, toName string) bool {
	return searchKey != "" && searchKeyId == 0 && !strings.Contains(toName, searchKey)
}

// 是否没包含id
func IsNotContainId(searchKey string, searchKeyId, toId int) bool {
	return searchKey == "" && searchKeyId > 0 && toId != searchKeyId
}

// 房间信息 - 后台
type AdminRoomInfo struct {
	RoomName    string // 房间名称
	UserId      int    // 用户id
	NickName    string // 用户昵称
	Level       int    // 等级
	Exps        int    // 经验值
	MemberCount int    // 成员数量
	RoomExpend
	Crdate string // 创建日期
}

// 房间的消耗 - 后台
type RoomExpend struct {
	RoomId         int // 房间ID
	CollectDiamond int // 钻石收益
	DiamondAmount  int // 钻石消耗数量
	CollectGold    int // 金币收益
	GoldAmount     int // 金币消耗数量
}

// 语聊房详细信息 - 后台
type AdminRoomDetail struct {
	RoomName    string // 房间名称
	RoomImg     string // 房间图片
	Family      string // 家族
	Country     string // 国家
	Language    string // 语言
	JoinFee     int    // 入会费
	Announce    string // 公告
	Tag         string // 标签
	MemberCount int    // 成员数量
	Crdate      string // 创建时间
	Level       int    // 等级
	Exps        int    // 经验值
	MicInfo     string `json:",omitempty"` // 麦位的信息
	UpMicCount  int    // 上麦的数量
	RoomExpend
	OnlineCount        int // 在线人数
	CumulativeDuration int // 累计时长 - 暂不支持
}

// 获取麦位的数量 - 后台
func AdminGetUpMicCount(mics []MicInfo) int {
	var count int
	for _, v := range mics {
		if v.UserId == 0 {
			continue
		}
		count++
	}
	return count
}

// 语聊房黑名单 - 后台
type AdminRoomBlackList struct {
	UserID     int    // 用户id
	NickName   string // 用户昵称
	RoleID     int    // 角色ID(0=关注 1=管理员  2=助理  3=成员)
	Level      int    // 等级
	ExpireTime int    // 过期时间戳
	Crdate     string // 创建时间
}

// 语聊房成员 - 后台
type AdminRoomMember struct {
	RoomID   int    // 房间ID
	UserID   int    // 用户ID
	NickName string // 用户昵称
	RoleID   int    // 用户身份
	Exps     int    // 经验值
	Level    int    // 用户等级
	Crdate   string // 创建时间
}

// 语聊房麦位 - 后台
type AdminRoomMic struct {
	UserID    int    // 用户ID
	NickName  string // 用户昵称
	RoleID    int    // 用户身份
	Level     int    // 用户等级
	MicNum    int    // 麦位编号
	UpMicType int    // 上麦类型 - 暂不支持
	UpMicTime string // 上麦时间
}

// 房内操作日志
type AdminRoomLogRecord struct {
	RoomID      int    // 房间ID
	UserID      int    // 用户ID
	NickName    string // 用户昵称
	ToUserID    int    // 对象用户ID
	ToNickName  string // 对象用户昵称
	OperateType int    // 操作类型(11=踢麦  21=禁麦  22=解麦  31=添加房间黑名单  32=移除房间黑名单)
	Crdate      string // 创建时间
}

// 房间任务列表
type AdminRoomTask struct {
	RoomID     int    // 房间ID
	TaskID     int    // 任务ID
	TaskName   string // 任务名称
	Schedule   int    // 进度
	CurrNum    int    // 当前数值
	UpdateTime string // 更新时间
}

// 用户的房间任务
type AdminUserRoomTask struct {
	UserID     int    // 用户id
	RoomID     int    // 房间id
	TaskID     int    // 任务id
	TaskName   string // 任务名称
	Schedule   int    // 进度
	CurrNum    int    // 当前数值
	Status     int    // 状态(0=进行中  1=完成)
	UpdateTime string // 更新的时间
}

// 房间上下麦日志
type AdminRoomMicLog struct {
	RoomID       int    // 房间ID
	RoomName     string // 房间名称
	UserID       int    // 用户ID
	NickName     string // 用户昵称
	RoleID       int    // 用户身份 【角色ID】 (0=关注 1=管理员  2=助理  3=成员)
	MicMode      int    // 麦位模式(上麦方式)
	OnMicTime    string // 上麦时间
	OffMicTime   string // 下麦时间
	Seconds      int    // 时长
	OpUserID     int    // 操作人ID
	OpUserRoleID int    // 操作人身份 【角色ID】 (0=关注 1=管理员  2=助理  3=成员)
}
