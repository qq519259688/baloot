package proto

// 收益类型
const (
	IncomeType_Invalid = iota // 0=无效类型
	IncomeType_Game           // 1=游戏收益
	IncomeType_Gift           // 2=礼物收益
)

// 用户类型
const (
	UserType_Invalid   = iota // 0=无效类型
	UserType_Receiver         // 1=普通用户
	UserType_RoomOwner        // 2=房主用户
)

// 游戏类型
const (
	GameType_Invalid = iota // 无效类型
	GameType_Consume        // 1=消耗类型
	GameType_Rebate         // 2=返利类型
)

// 触发数据
type TriggerData struct {
	RoomId    int // 房间id
	GameId    int `json:",omitempty"` // 游戏id
	Receiver  int // 接收方id
	RoomOwner int // 房主id
	ItemType  int // 道具类型(1=金币  2=钻石)
	Amount    int // 数量
}

// 游戏收益配置
type GameIncomeConfig struct {
	GameId   int          // 游戏id
	GameType int          // 游戏类型
	Incomes  []GameIncome // 游戏收益
}

// 游戏收益信息
type GameIncome struct {
	ItemType int     // 道具类型(1=金币  2=钻石)
	Level    int     // 等级(收益档次)
	Ratio    float64 // 收益比率
}

// 礼物收益配置
type GiftIncomeConfig struct {
	Level   int          // 等级
	Incomes []GiftIncome // 收益信息
}

// 礼物收益信息
type GiftIncome struct {
	ItemType  int     // 道具类型(1=金币  2=钻石)
	Receiver  float64 // 接收者收益
	RoomOwner float64 // 房主收益
}

// 收益信息
type IncomeInfo struct {
	ItemType    int     // 道具类型(1=金币  2=钻石)
	Balance     float64 // 结余收益
	TotalProfit float64 // 累计收益
}

// 用户收益记录
type IncomeLog struct {
	FromUserId   int    // 源用户id
	FromNickName string // 昵称
	FaceUrl      string
	FaceId       int
	RoomId       int     // 房间id
	GameId       int     // 游戏id
	ItemType     int     // 道具类型(1=金币  2=钻石)
	IncomeType   int     // 收益类型(1=游戏收益  2=礼物收益)
	UserType     int     // 用户类型(1=普通用户 2=房主用户)
	UserLevel    int     // 用户等级
	Amount       int     // 数量
	Profit       float64 // 收益
}

// 用户收益统计
type UserIncomeStat struct {
	FromUserId   int    // 源用户ID
	FromNickName string // 昵称
	FaceUrl      string
	FaceId       int
	GiftProfit   float64 // 礼物收益
	GameProfit   float64 // 游戏收益
}

// 游戏收益统计
type GameIncomeStat struct {
	GameId      int     // 游戏id
	ChineseName string  // 游戏名称
	Amount      int     // 投注数量
	Profit      float64 // 收益
}
