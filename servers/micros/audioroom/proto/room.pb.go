package proto

import (
	badge "bet24.com/servers/micros/badge/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	user "bet24.com/servers/micros/userservices/proto"
)

type Request_IsEnterRoom struct {
	RoomId   int
	Password string
}

type Response_RoomInfo struct {
	*RoomInfo
	Badges      []badge.BadgePosition `json:",omitempty"` // 佩戴与展示的徽章列表
	Decorations []user.UserDecoration `json:",omitempty"` // 房主装扮
}

type Response_RoomExtInfo struct {
	*RoomExtInfo
}

type Response_RoomHotInfo struct {
	OnlineCount int // 在线人数
}

type Request_GetPageList struct {
	RoomId    int
	RoomName  string
	Tag       string
	PageIndex int
	PageSize  int
}

type Response_GetPageList struct {
	RecordCount int `json:",omitempty"`
	List        interface{}
}

type Request_InviteJoin struct {
	RoomId   int
	ToUserId int
}

type Request_AcceptJoin struct {
	Code      string
	IpAddress string
}

type Request_AddJoin struct {
	RoomId    int
	RoleId    int
	IpAddress string
}

type Response_List struct {
	List interface{}
}

type Request_Mic struct {
	RoomId   int
	ToUserId int
	SN       int
	Status   int
	StreamId string
}

type Request_SetPermission struct {
	RoomId         int
	PermissionType int
	Enabled        int
}

type Response_GetEnterCondition struct {
	EnterCondition
}

type Request_SetEnterCondition struct {
	RoomId     int
	InviteOnly bool
	Password   string
	IsHide     bool
}

type Request_GetBlackList struct {
	RoomId    int
	BlackType int
	PageIndex int
	PageSize  int
}

type Request_Black struct {
	RoomId    int
	ToUserId  int
	BlackType int
	Seconds   int
}

type Request_GetOperateLog struct {
	RoomId      int
	ToUserId    int
	OperateType int
	PageIndex   int
	PageSize    int
}

type Request_ToUser struct {
	RoomId   int
	ToUserId int
}

type Request_SendGiving struct {
	RoomId     int
	ToUserId   int
	ToNickName string
	GiftId     int
	Num        int
}

type Response_SendGiving struct {
	RetCode int
	Message string
	Request_SendGiving
}

type Response_GiftCollect struct {
	RetCode int             // 操作结果
	Message string          // 消息
	Items   []item.ItemPack // 奖励
}

type Request_SysFlag struct {
	SysFlag bool
}

type Response_GetUserRoomInfo struct {
	Level int // 等级
	Exps  int // 经验
}

type Request_StartGame struct {
	RoomId         int    // 房间id
	GameId         int    // 游戏id
	PlayType       int    // 玩法类型
	Scene          int    // 场景
	MessageContent string // 消息内容
}

type Response_StartGame struct {
	RetCode  int    // 操作结果
	Message  string // 消息
	Result   int    // 结果
	PlayType int    // 玩法
}

// 请求：屏幕锁
type Request_ScreenLock struct {
	RoomId     int // 房间id
	ScreenLock int // 状态（1：锁定，0：解锁）
}

type Request_CreateRoom struct {
	RoomName  string
	Country   string
	IpAddress string
	RoomImg   string
	Announce  string
	Tag       string
}

type Request_IncomeList struct {
	RoomId     int
	FromUserId int
	ItemType   int
	BeginTime  string
	EndTime    string
	PageIndex  int
	PageSize   int
	SortType   int // 排序类型 (11=收益降序  12=收益升序   21=礼物收益降序  22=礼物收益升序  31=游戏收益降序  32=游戏收益升序  41=下注降序  42=下注升序)
}

type Response_IncomeList struct {
	RecordCount     int
	List            interface{}
	TotalGameProfit float64 `json:",omitempty"`
	TotalGiftProfit float64 `json:",omitempty"`
}

type Response_GetIncomeInfo struct {
	IncomeLevel     int     // 收益等级
	GoldIncomeRatio float64 // 金币流水收益比率
	ChipIncomeRatio float64 // 钻石流水收益比率
}

type Response_LoginServer struct {
	Success bool
	*RoomInfo
}
