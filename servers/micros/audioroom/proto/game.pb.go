package proto

// 上报用户投注日志
type UserBet struct {
	UserId   int
	TotalBet int
	TotalWin int
}
type Request_ReportUserBet struct {
	RoomId     int    // 房主ID
	RoomNo     int    // 房间号
	GameId     int    // 游戏ID
	RoomName   string // 房间名
	IsChipRoom bool   // 是否钻石场
	UserBets   []UserBet
}

// 返回RoomNo,IP:Port,TableId
type GameRoomInfo struct {
	RoomNo     int    // 私人场号码
	ServerAddr string // 房间地址
	TableId    int    // 桌子号
	GameId     int    // 游戏id
	RuleName   string // 玩法
}

type Request_CreateGameRoom struct {
	RoomId   int
	GameId   int
	RuleName string
}

type Response_CreateGameRoom struct {
	RetMsg
	*GameRoomInfo
}

type Request_CloseGameRoom struct {
	RoomId int
	GameId int
	RoomNo int
}

type Response_CloseGameRoom struct {
	RetMsg
}
