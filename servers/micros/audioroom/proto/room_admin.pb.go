package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

// 获取房间列表 - 后台
func AdminGetRoomList(roomName, sortName, sortType string, roomId, pageIndex, pageSize int) (int, interface{}) {
	xclient := getClient()
	//defer xclient.Close()

	args := &AdminRequest{
		RoomId:    roomId,
		RoomName:  roomName,
		PageIndex: pageIndex,
		PageSize:  pageSize,
		SortName:  sortName,
		SortType:  sortType,
	}

	reply := &AdminResponse{}

	err := xclient.Call(context.Background(), "AdminGetRoomList", args, reply)
	if err != nil {
		log.Debug("proto.AdminGetRoomList failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.RecordCount, reply.List
}

// 获取语聊房详细信息 - 后台
func AdminGetRoomDetail(roomId int) interface{} {
	xclient := getClient()
	//defer xclient.Close()

	args := &AdminRequest{
		RoomId: roomId,
	}

	reply := &AdminResponse{}

	err := xclient.Call(context.Background(), "AdminGetRoomDetail", args, reply)
	if err != nil {
		log.Debug("proto.AdminGetRoomDetail failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.List
}

// 获取语聊房成员 - 后台
func AdminGetRoomMember(roomId, pageIndex, pageSize int) (int, interface{}) {
	xclient := getClient()
	//defer xclient.Close()

	args := &AdminRequest{
		RoomId:    roomId,
		PageIndex: pageIndex,
		PageSize:  pageSize,
	}

	reply := &AdminResponse{}

	err := xclient.Call(context.Background(), "AdminGetRoomMember", args, reply)
	if err != nil {
		log.Debug("proto.AdminGetRoomMember failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.RecordCount, reply.List
}

// 获取语聊房房间的在线用户 - 后台
func AdminGetRoomOnlineUsers(roomId, pageIndex, pageSize int) (int, interface{}) {
	xclient := getClient()
	//defer xclient.Close()

	args := &AdminRequest{
		RoomId:    roomId,
		PageIndex: pageIndex,
		PageSize:  pageSize,
	}

	reply := &AdminResponse{}

	err := xclient.Call(context.Background(), "AdminGetRoomOnlineUsers", args, reply)
	if err != nil {
		log.Debug("proto.AdminGetRoomOnlineUsers failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.RecordCount, reply.List
}

// 获取语聊房麦位 - 后台
func AdminGetRoomMic(roomId int) (int, interface{}) {
	xclient := getClient()
	//defer xclient.Close()

	args := &AdminRequest{
		RoomId: roomId,
	}

	reply := &AdminResponse{}

	err := xclient.Call(context.Background(), "AdminGetRoomMic", args, reply)
	if err != nil {
		log.Debug("proto.AdminGetRoomMic failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.RecordCount, reply.List
}

// 获取黑名单用户列表 - 后台
func AdminGetBlackList(roomId, blackType, pageIndex, pageSize int) (int, interface{}) {
	xclient := getClient()
	//defer xclient.Close()

	args := &AdminRequest{
		RoomId:    roomId,
		BlackType: blackType,
		PageIndex: pageIndex,
		PageSize:  pageSize,
	}

	reply := &AdminResponse{}

	err := xclient.Call(context.Background(), "AdminGetBlackList", args, reply)
	if err != nil {
		log.Debug("proto.AdminGetBlackList failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.RecordCount, reply.List
}

// 生成系统任务集合 - 后台
func GenerateSysTaskMap(sysFlag bool) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &AdminRequest{
		SysFlag: sysFlag,
	}

	reply := &AdminResponse{}

	err := xclient.Call(context.Background(), "GenerateSysTaskMap", args, reply)
	if err != nil {
		log.Debug("proto.GenerateSysTaskMap failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.Data
}

// 获取房间任务列表 - 后台
func AdminGetRoomTask(roomId, pageIndex, pageSize int) (int, interface{}) {
	xclient := getClient()
	//defer xclient.Close()

	args := &AdminRequest{
		RoomId:    roomId,
		PageIndex: pageIndex,
		PageSize:  pageSize,
	}

	reply := &AdminResponse{}

	err := xclient.Call(context.Background(), "AdminGetRoomTask", args, reply)
	if err != nil {
		log.Debug("proto.AdminGetRoomTask failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.RecordCount, reply.List
}
