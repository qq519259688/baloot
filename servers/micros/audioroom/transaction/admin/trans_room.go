package admin

import (
	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"encoding/json"
	"runtime/debug"
)

// 获取所有房间的消耗
func GetAllRoomExpend() []pb.RoomExpend {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_AudioRoom_GetAllRoomExpend")
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	var ret []pb.RoomExpend
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("transaction.GetAllRoomExpend json unmarshal err %v", err)
	}
	return ret
}
