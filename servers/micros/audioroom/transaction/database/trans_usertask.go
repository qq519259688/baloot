package database

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

// 获取房间任务列表
func GetUserTaskList(userId int, roomId int) []*pb.UserRoomTask {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetUserTask")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	var ret []*pb.UserRoomTask
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("transaction.GetUserTaskList json unmarshal err %v", err)
	}
	return ret
}

// 修改任务
func UpdateUserTask(userId, roomId int, info *pb.UserRoomTask) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_UpdateUserTask")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@TaskID", database.AdParamInput, database.AdInteger, 4, info.TaskId)
	statement.AddParamter("@Schedule", database.AdParamInput, database.AdInteger, 4, info.Schedule)
	statement.AddParamter("@CurrNum", database.AdParamInput, database.AdInteger, 4, info.CurrNum)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, info.Status)
	statement.AddParamter("@TimeStamp", database.AdParamInput, database.AdInteger, 4, info.TimeStamp)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}

// 房间任务统计
func GetUserTaskStat(roomId int) []pb.UserRoomTaskStat {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetUserTaskStat")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	var ret []pb.UserRoomTaskStat
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("transaction.GetUserTaskList json unmarshal err %v", err)
	}
	return ret
}
