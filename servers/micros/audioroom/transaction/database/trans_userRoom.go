package database

import (
	"encoding/json"
	"runtime/debug"

	pb "bet24.com/servers/micros/audioroom/proto"

	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

// 获取用户房间
func GetUserJoin(userId int) []pb.UserRoomInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var list []pb.UserRoomInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetUserJoin")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
		log.Error("trans_userRoom.GetUserJoin json unmarshal err %v", err)
	}
	return list
}

// 加入/关注房间
func SaveUserRoom(userId, roomId, roleId int, crdate string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_SaveUserRoom")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@RoleID", database.AdParamInput, database.AdInteger, 4, roleId)
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdVarChar, 20, crdate)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return 1
}

// 取消加入
func DelUserRoom(userId, roomId int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_DelUserRoom")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return 1
}

// 保存用户浏览信息
func SaveUserBrowse(userId int, roomSet string) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_SaveUserBrowse")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomSet", database.AdParamInput, database.AdVarChar, 1024, roomSet)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}

// 获取用户浏览信息
func GetUserBrowse(userId int) []int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var list []int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetUserBrowse")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return list
	}

	roomSet := retRows[0][0].(string)
	if err := json.Unmarshal([]byte(roomSet), &list); err != nil {
		log.Error("transaction.getUserBrowse json unmarshal err %v", err)
	}

	return list
}

// 保存用户关注信息
func SaveUserAttention(userId int, roomSet string) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_SaveUserAttention")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomSet", database.AdParamInput, database.AdVarChar, 2048, roomSet)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}

// 获取用户关注信息
func GetUserAttention(userId int) []int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var list []int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetUserAttention")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return list
	}

	roomSet := retRows[0][0].(string)
	if err := json.Unmarshal([]byte(roomSet), &list); err != nil {
		log.Error("transaction.getUserBrowse json unmarshal err %v", err)
	}

	return list
}

// 更新经验
func UpdateUserExps(userId, roomId, level, exps int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_UpdateUserExps")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@Level", database.AdParamInput, database.AdInteger, 4, level)
	statement.AddParamter("@Exps", database.AdParamInput, database.AdInteger, 4, exps)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}
