package database

import (
	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"runtime/debug"
)

// 添加礼物记录
func AddGiftHistory(info pb.GiftHistory) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_AddGiftHistory")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, info.RoomId)
	statement.AddParamter("@Sender", database.AdParamInput, database.AdInteger, 4, info.Sender)
	statement.AddParamter("@Receiver", database.AdParamInput, database.AdInteger, 4, info.Receiver)
	statement.AddParamter("@GiftID", database.AdParamInput, database.AdInteger, 4, info.GiftId)
	statement.AddParamter("@GiftNum", database.AdParamInput, database.AdInteger, 4, info.GiftNum)
	statement.AddParamter("@DiamondAmount", database.AdParamInput, database.AdInteger, 4, info.DiamondAmount)
	statement.AddParamter("@GoldAmount", database.AdParamInput, database.AdInteger, 4, info.GoldAmount)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}
