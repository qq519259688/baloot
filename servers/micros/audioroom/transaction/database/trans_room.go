package database

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

// 获取房间
func GetRoom(roomId int) pb.RoomInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var ret []pb.RoomInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetInfo")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		// log.Error("transaction.getRoom json unmarshal roomId=%d err %v", roomId, err)
		return pb.RoomInfo{}
	}

	if len(ret) <= 0 {
		// log.Debug("transaction.getRoom roomId=%d is not exist", roomId)
		return pb.RoomInfo{}
	}

	return ret[0]
}

// 查找房间
func SearchRoom(roomName string) []pb.RoomInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var ret []pb.RoomInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_Search")
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdNVarChar, 32, roomName)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("transaction.searchRoom json unmarshal roomName=%s err %v", roomName, err)
		return nil
	}

	return ret
}

// 创建房间
func CreateRoom(r *pb.RoomInfo) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_Create")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, r.RoomId)
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdNVarChar, 32, r.RoomName)
	statement.AddParamter("@RoomImg", database.AdParamInput, database.AdVarChar, 256, r.RoomImg)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, r.UserId)
	statement.AddParamter("@Family", database.AdParamInput, database.AdNVarChar, 32, r.Family)
	statement.AddParamter("@Country", database.AdParamInput, database.AdNVarChar, 32, r.Country)
	statement.AddParamter("@Language", database.AdParamInput, database.AdNVarChar, 32, r.Language)
	statement.AddParamter("@MicCount", database.AdParamInput, database.AdInteger, 4, r.MicCount)
	statement.AddParamter("@MicMode", database.AdParamInput, database.AdInteger, 4, r.MicMode)
	statement.AddParamter("@JoinFee", database.AdParamInput, database.AdInteger, 4, r.JoinFee)
	statement.AddParamter("@Announce", database.AdParamInput, database.AdNVarChar, 256, r.Announce)
	statement.AddParamter("@Tag", database.AdParamInput, database.AdNVarChar, 32, r.Tag)
	statement.AddParamter("@MemberCount", database.AdParamInput, database.AdInteger, 4, r.MemberCount)
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdVarChar, 20, r.Crdate)
	statement.AddParamter("@Level", database.AdParamInput, database.AdInteger, 4, r.Level)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return 0
	}
	return int(retRows[0][0].(int64))
}

// 修改房间
func UpdateRoom(r *pb.RoomInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_Update")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, r.RoomId)
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdNVarChar, 32, r.RoomName)
	statement.AddParamter("@RoomImg", database.AdParamInput, database.AdVarChar, 256, r.RoomImg)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, r.UserId)
	statement.AddParamter("@Family", database.AdParamInput, database.AdNVarChar, 32, r.Family)
	statement.AddParamter("@Country", database.AdParamInput, database.AdNVarChar, 32, r.Country)
	statement.AddParamter("@Language", database.AdParamInput, database.AdNVarChar, 32, r.Language)
	statement.AddParamter("@MicCount", database.AdParamInput, database.AdInteger, 4, r.MicCount)
	statement.AddParamter("@MicMode", database.AdParamInput, database.AdInteger, 4, r.MicMode)
	statement.AddParamter("@JoinFee", database.AdParamInput, database.AdInteger, 4, r.JoinFee)
	statement.AddParamter("@Announce", database.AdParamInput, database.AdNVarChar, 256, r.Announce)
	statement.AddParamter("@Tag", database.AdParamInput, database.AdNVarChar, 32, r.Tag)
	sqlString := statement.GenSql()
	dbengine.ExecuteRs(sqlString)
}

// 获取语音房进入条件
func GetEnterCondition(roomId int) pb.EnterCondition {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var info pb.EnterCondition
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetEnterCondition")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return info
	}

	ret := retRows[0]
	info.InviteOnly = int(ret[0].(int64)) == 1
	info.Password = ret[1].(string)
	info.PwdExpire = int(ret[2].(int64))
	info.IsHide = int(ret[3].(int64)) > 0
	return info
}

// 设置语音房进入条件
func SetEnterCondition(roomId int, info pb.EnterCondition) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	inviteOnly := 0
	if info.InviteOnly {
		inviteOnly = 1
	}

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_SetEnterCondition")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@InviteOnly", database.AdParamInput, database.AdInteger, 4, inviteOnly)
	statement.AddParamter("@Password", database.AdParamInput, database.AdVarChar, 16, info.Password)
	statement.AddParamter("@PwdExpire", database.AdParamInput, database.AdInteger, 4, info.PwdExpire)
	statement.AddParamter("@IsHide", database.AdParamInput, database.AdInteger, 4, info.IsHide)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return 0
	}
	return int(retRows[0][0].(int64))
}

// 更新经验
func UpdateExps(roomId, level, exps int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_UpdateExps")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@Level", database.AdParamInput, database.AdInteger, 4, level)
	statement.AddParamter("@Exps", database.AdParamInput, database.AdInteger, 4, exps)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}

// 获取扩展信息
func GetExtInfo(roomId int) pb.RoomExtInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var ret []pb.RoomExtInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetExtInfo")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		return pb.RoomExtInfo{}
	}
	if len(ret) <= 0 {
		return pb.RoomExtInfo{}
	}

	return ret[0]
}

// 更新扩展信息
func UpdateExtInfo(roomId int, info *pb.RoomExtInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_UpdateExt")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@DayExps", database.AdParamInput, database.AdInteger, 4, info.DayExps)
	statement.AddParamter("@CollectDiamond", database.AdParamInput, database.AdInteger, 4, info.CollectDiamond)
	statement.AddParamter("@ScreenLock", database.AdParamInput, database.AdTinyInt, 4, info.ScreenLock)
	statement.AddParamter("@UpdateTime", database.AdParamInput, database.AdInteger, 4, info.UpdateTime)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}

// 房间经验日志
func AddRoomExpLog(userId, roomId, exps int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_AddRoomExpLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@Exps", database.AdParamInput, database.AdInteger, 4, exps)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}
