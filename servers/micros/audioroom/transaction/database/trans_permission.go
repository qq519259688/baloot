package database

import (
	"encoding/json"
	"runtime/debug"

	pb "bet24.com/servers/micros/audioroom/proto"

	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

// 获取房间权限
func GetPermissionList(roomId int) []pb.PermissionInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []pb.PermissionInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetPermission")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
		log.Error("trans_permission.GetPermissionList json unmarshal err %v", err)
	}
	return list
}

// 设置房间权限
func SetRoomPermission(roomId, permissionType, enabled int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_SetPermission")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@PermissionType", database.AdParamInput, database.AdInteger, 4, permissionType)
	statement.AddParamter("@Enabled", database.AdParamInput, database.AdInteger, 4, enabled)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return 0
	}
	return int(retRows[0][0].(int64))
}
