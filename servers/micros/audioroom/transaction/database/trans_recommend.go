package database

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

// 推荐房间
func RecommendRoom() []int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var list []int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_RecommendRoom")
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	for i := 0; i < len(retRows); i++ {
		ret := retRows[i]
		list = append(list, int(ret[0].(int64)))
	}

	return list
}

// 推荐用户
func RecommendUser() []int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	var list []int
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_RecommendUser")
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	for i := 0; i < len(retRows); i++ {
		ret := retRows[i]
		list = append(list, int(ret[0].(int64)))
	}

	return list
}
