package database

import (
	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"runtime/debug"
	"strconv"
)

// 获取用户收益等级
func GetUserIncomeLevel(userId int) (incomeLevel int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetIncomeLevel")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	incomeLevel = int(retRows[0][0].(int64))
	return
}

// 修改用户收益等级
func UpdateUserIncomeLevel(userId, incomeLevel int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_UpdateIncomeLevel")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@IncomeLevel", database.AdParamInput, database.AdInteger, 4, incomeLevel)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}

// 获取用户收益列表
func GetUserIncomeList(userId int) []pb.IncomeInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []pb.IncomeInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetIncomeList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info pb.IncomeInfo
		info.ItemType = int(ret[0].(int64))

		balanceStr := string(ret[1].([]byte))
		info.Balance, _ = strconv.ParseFloat(balanceStr, 64)

		totalProfitStr := string(ret[2].([]byte))
		info.TotalProfit, _ = strconv.ParseFloat(totalProfitStr, 64)

		list = append(list, info)
	}

	return list
}

// 添加用户收益
func AddUserIncome(userId, itemType int, balance, totalProfit float64) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_AddIncome")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ItemType", database.AdParamInput, database.AdInteger, 4, itemType)
	statement.AddParamter("@Balance", database.AdParamInput, database.AdFloat, 64, balance)
	statement.AddParamter("@TotalProfit", database.AdParamInput, database.AdFloat, 64, totalProfit)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}

// 添加收益日志
func AddUserIncomeLog(userId, roomId, gameId, fromUserId, itemType, incomeType, userType, amount, realProfit int, profit, stillProfit float64) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_AddIncomeLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	statement.AddParamter("@FromUserID", database.AdParamInput, database.AdInteger, 4, fromUserId)
	statement.AddParamter("@ItemType", database.AdParamInput, database.AdInteger, 4, itemType)
	statement.AddParamter("@IncomeType", database.AdParamInput, database.AdInteger, 4, incomeType)
	statement.AddParamter("@UserType", database.AdParamInput, database.AdInteger, 4, userType)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdInteger, 4, amount)
	statement.AddParamter("@Profit", database.AdParamInput, database.AdFloat, 64, profit)
	statement.AddParamter("@StillProfit", database.AdParamInput, database.AdFloat, 64, stillProfit)
	statement.AddParamter("@RealProfit", database.AdParamInput, database.AdInteger, 4, realProfit)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}

// 获取收益日志
func GetIncomeLog(userId, roomId, fromUserId, itemType int, beginTime, endTime string,
	pageIndex, pageSize int) (recordCount int, list []pb.IncomeLog) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetIncomeLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@FromUserID", database.AdParamInput, database.AdInteger, 4, fromUserId)
	statement.AddParamter("@ItemType", database.AdParamInput, database.AdInteger, 4, itemType)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info pb.IncomeLog
			info.FromUserId = int(ret[0].(int64))
			info.GameId = int(ret[1].(int64))
			info.IncomeType = int(ret[2].(int64))
			info.UserType = int(ret[3].(int64))
			info.UserLevel = int(ret[4].(int64))
			info.Amount = int(ret[5].(int64))
			profit := string(ret[6].([]byte))
			info.Profit, _ = strconv.ParseFloat(profit, 64)
			list = append(list, info)
		}
	}

	recordCount = int(retRows[rowLen-1][0].(int64))
	return
}

// 获取用户收益统计
func GetUserIncomeStat(userId, roomId, fromUserId, itemType, sortType int, beginTime, endTime string,
	pageIndex, pageSize int) (recordCount int, totalGameProfit, totalGiftProfit float64, list []pb.UserIncomeStat) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetUserIncomeStat")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@FromUserID", database.AdParamInput, database.AdInteger, 4, fromUserId)
	statement.AddParamter("@ItemType", database.AdParamInput, database.AdInteger, 4, itemType)
	statement.AddParamter("@SortType", database.AdParamInput, database.AdInteger, 4, sortType)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	statement.AddParamter("@TotalGameProfit", database.AdParamOutput, database.AdFloat, 64, totalGameProfit)
	statement.AddParamter("@TotalGiftProfit", database.AdParamOutput, database.AdFloat, 64, totalGiftProfit)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info pb.UserIncomeStat
			info.FromUserId = int(ret[0].(int64))

			gameProfitStr := string(ret[1].([]byte))
			info.GameProfit, _ = strconv.ParseFloat(gameProfitStr, 64)

			giftProfitStr := string(ret[2].([]byte))
			info.GiftProfit, _ = strconv.ParseFloat(giftProfitStr, 64)

			list = append(list, info)
		}
	}

	recordCount = int(retRows[rowLen-1][0].(int64))

	totalGameProfitStr := string(retRows[rowLen-1][1].([]byte))
	totalGameProfit, _ = strconv.ParseFloat(totalGameProfitStr, 64)

	totalGiftProfitStr := string(retRows[rowLen-1][2].([]byte))
	totalGiftProfit, _ = strconv.ParseFloat(totalGiftProfitStr, 64)

	return
}

// 获取游戏收益统计
func GetGameIncomeStat(userId, roomId, itemType, sortType int, beginTime, endTime string,
	pageIndex, pageSize int) (recordCount int, list []pb.GameIncomeStat) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetGameIncomeStat")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@ItemType", database.AdParamInput, database.AdInteger, 4, itemType)
	statement.AddParamter("@SortType", database.AdParamInput, database.AdInteger, 4, sortType)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info pb.GameIncomeStat
			info.GameId = int(ret[0].(int64))
			info.Amount = int(ret[1].(int64))

			profitStr := string(ret[2].([]byte))
			info.Profit, _ = strconv.ParseFloat(profitStr, 64)

			list = append(list, info)
		}
	}

	recordCount = int(retRows[rowLen-1][0].(int64))
	return
}
