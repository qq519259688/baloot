package database

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

// 获取麦位信息
func GetMicInfo(roomId int) []pb.MicInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []pb.MicInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetMicInfo")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return list
	}

	micsInfo := retRows[0][0].(string)
	if err := json.Unmarshal([]byte(micsInfo), &list); err != nil {
		log.Error("transaction.getMicInfo json unmarshal err %v", err)
	}

	return list
}

// 设置麦位信息
func SetMicInfo(roomId int, mics []pb.MicInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	buf, _ := json.Marshal(mics)
	micInfo := string(buf)

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_SetMicInfo")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@MicInfo", database.AdParamInput, database.AdVarChar, 1024, micInfo)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}

// 添加麦位日志
func AddMicLog(userId, roomId, roleId, sn, micMode, onMicStamp, offMicStamp, seconds, opUserId int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_AddMicLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@RoleID", database.AdParamInput, database.AdInteger, 4, roleId)
	statement.AddParamter("@SN", database.AdParamInput, database.AdInteger, 4, sn)
	statement.AddParamter("@MicMode", database.AdParamInput, database.AdInteger, 4, micMode)
	statement.AddParamter("@OnMicTime", database.AdParamInput, database.AdInteger, 4, onMicStamp)
	statement.AddParamter("@OffMicTime", database.AdParamInput, database.AdInteger, 4, offMicStamp)
	statement.AddParamter("@Seconds", database.AdParamInput, database.AdInteger, 4, seconds)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, opUserId)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	dbengine.Execute(sqlString)
	return
}
