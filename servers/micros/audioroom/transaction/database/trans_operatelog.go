package database

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

// 获取房间操作记录
func OperateLog(roomId, toUserId, operateType, pageIndex, pageSize int) (int, []pb.OperateInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	recordCount := 0
	var list []pb.OperateInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetOperateLog")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	statement.AddParamter("@OperateType", database.AdParamInput, database.AdInteger, 4, operateType)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return recordCount, list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info pb.OperateInfo
			info.UserId = int(ret[0].(int64))
			info.ToUserId = int(ret[1].(int64))
			info.OperateType = int(ret[2].(int64))
			info.Crdate = ret[3].(string)
			list = append(list, info)
		}
	}

	recordCount = int(retRows[rowLen-1][0].(int64))

	return recordCount, list
}

// 添加房间操作记录
func AddOperateLog(roomId, userId, toUserId, operateType int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_AddOperateLog")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ToUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	statement.AddParamter("@OperateType", database.AdParamInput, database.AdInteger, 4, operateType)
	sqlString := statement.GenSql()
	dbengine.ExecuteRs(sqlString)
	return
}
