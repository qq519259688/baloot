package database

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

// 获取用户贡献
func GetUserContribute(userId int) []pb.UserContribute {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetUserContribute")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	var ret []pb.UserContribute
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("transaction.GetUserContribute json unmarshal err %v", err)
	}
	return ret
}

// 修改收集
func UpdateUserContribute(userId int, info *pb.UserContribute) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_UpdateUserContribute")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, info.RoomId)
	statement.AddParamter("@Action", database.AdParamInput, database.AdInteger, 4, info.Action)
	statement.AddParamter("@Exps", database.AdParamInput, database.AdInteger, 4, info.Exps)
	statement.AddParamter("@Expire", database.AdParamInput, database.AdInteger, 4, info.Expire)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}
