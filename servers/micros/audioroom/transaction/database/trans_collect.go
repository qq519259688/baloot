package database

import (
	"encoding/json"
	"runtime/debug"
	"strconv"

	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

// 获取房间收集列表
func GetCollectList(roomId int) []pb.RoomCollect {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetCollectList")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}
	var list []pb.RoomCollect
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info pb.RoomCollect
		info.DayIndex = int(ret[0].(int64))
		info.Points = int(ret[1].(int64))
		ratio := string(ret[2].([]byte))
		info.Ratio, _ = strconv.ParseFloat(ratio, 64)
		award := ret[3].(string)
		if err := json.Unmarshal([]byte(award), &info.Award); err != nil {
			log.Error("trans_collect.GetCollect json unmarshal err %v", err)
		}
		info.Status = int(ret[4].(int64))
		list = append(list, info)
	}
	return list
}

// 修改收集
func UpdateCollect(roomId int, info *pb.RoomCollect) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	buf, _ := json.Marshal(info.Award)

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_UpdateCollect")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@DayIndex", database.AdParamInput, database.AdInteger, 4, info.DayIndex)
	statement.AddParamter("@Points", database.AdParamInput, database.AdInteger, 4, info.Points)
	statement.AddParamter("@Ratio", database.AdParamInput, database.AdInteger, 4, info.Ratio)
	statement.AddParamter("@Award", database.AdParamInput, database.AdVarChar, 32, string(buf))
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, info.Status)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}
