package database

import (
	"runtime/debug"

	"bet24.com/servers/common"

	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/audioroom/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

// 黑名单列表
func GetBlackList(roomId int) []pb.BlackInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []pb.BlackInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_GetBlackList")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		userId := int(ret[0].(int64))
		blackType := int(ret[1].(int64))
		expireTimeStamp := int(ret[2].(int64))

		// 过期数据
		if expireTimeStamp <= common.GetTimeStamp() {
			continue
		}

		isNew := true

		// 判断是否已存在
		for j := 0; j < len(list); j++ {
			if list[j].UserId != userId {
				continue
			}

			// 追加到数组里
			list[j].BlackTypes = append(list[j].BlackTypes, pb.BlackType{
				BlackType:       blackType,
				ExpireTimeStamp: expireTimeStamp,
			})

			isNew = false
			break
		}

		if !isNew {
			continue
		}

		// 新增数据
		var info pb.BlackInfo
		info.UserId = userId
		info.BlackTypes = append(info.BlackTypes, pb.BlackType{
			BlackType:       blackType,
			ExpireTimeStamp: expireTimeStamp,
		})
		list = append(list, info)
	}

	return list
}

// 拉入黑名单
func AddBlack(roomId, userId, blackType, expireTime int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_AddBlack")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@BlackType", database.AdParamInput, database.AdInteger, 4, blackType)
	statement.AddParamter("@ExpireTime", database.AdParamInput, database.AdInteger, 4, expireTime)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return 0
	}
	return int(retRows[0][0].(int64))
}

// 移除黑名单
func RemoveBlack(roomId, userId, blackType int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AudioRoom_RemoveBlack")
	statement.AddParamter("@RoomID", database.AdParamInput, database.AdInteger, 4, roomId)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@BlackType", database.AdParamInput, database.AdInteger, 4, blackType)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return 0
	}
	return int(retRows[0][0].(int64))
}
