package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

type Request_GetItem struct {
	ItemId int
}

type Response_GetItem struct {
	Itm *Item
}

func GetItem(itemId int) *Item {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GetItem{
		ItemId: itemId,
	}

	reply := &Response_GetItem{}

	err := xclient.Call(context.Background(), "GetItem", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.Itm
}

type Response_GetItems struct {
	Itms map[int]*Item
}

func GetItems() map[int]*Item {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{}

	reply := &Response_GetItems{}

	err := xclient.Call(context.Background(), "GetItems", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.Itms
}

type Request_GetItemValue struct {
	ItemId int
	Count  int
}

type Response_GetItemValue struct {
	Value int
}

func GetItemValue(itemId int, count int) int {
	xclient := getClient()

	args := &Request_GetItemValue{
		ItemId: itemId,
		Count:  count,
	}

	reply := &Response_GetItemValue{}

	err := xclient.Call(context.Background(), "GetItemValue", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.Value
}

type Request_ItemPack struct {
	Items []ItemPack
}

func ChangeItemCountToDuracion(items []ItemPack) []ItemPack {
	xclient := getClient()
	args := &Request_ItemPack{
		Items: items,
	}

	reply := &Request_ItemPack{}

	err := xclient.Call(context.Background(), "ChangeItemCountToDuracion", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return items
	}
	return reply.Items
}

func GetItemEffect(itemId int) EffectInfo {
	itm := GetItem(itemId)
	if itm == nil {
		return EffectInfo{}
	}
	return itm.EffectInfo
}
