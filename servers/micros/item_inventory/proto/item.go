package proto

// Item.Type
const (
	_                  = iota //无效值
	Item_Gold                 // 1=金币
	Item_Chip                 // 2=钻石
	Item_Vitality             // 3=活跃度
	Item_Decoration           // 4=装扮类
	Item_Speaker              // 5=大喇叭
	Item_Consume              // 6=其他消耗品
	Item_VirtualGift          // 7=虚拟礼物
	Item_Addminutes           // 8=充值话费
	Item_Physical             // 9=实物道具
	Item_GiftCard             // 10=购物卡
	Item_Cash                 // 11=现金
	Item_BattlePassExp        // 12=battlepass经验
	Item_MatchTicket          // 13=比赛门票
	Item_CashCoupon           // 14=现金券(用来提现)
	Item_Vip                  // 15=vip时长
)

// sub type
const (
	Item_Decoration_Invalid             = iota
	Item_Decoration_HeadFrame           // 1=头像框
	Item_Decoration_ChatBubble          // 2=聊天气泡
	Item_Decoration_AudioRoomDataCard   // 3=语聊房资料卡
	Item_Decoration_AudioRoomBackground // 4=语聊房背景
	Item_Decoration_EntryEffects        // 5=进场特效
	Item_Decoration_CardBack            // 6=牌背
	Item_Decoration_PokerTable          // 7=牌桌
	Item_Decoration_Dice                // 8=骰子
	Item_Decoration_Chessman            // 9=棋子
	Item_Decoration_Chessboard          // 10=棋盘
	Item_Decoration_FishCannon          // 11=炮台
)

type Item struct {
	Id             int    // 道具ID
	ActiveId       int    // 限时道具使用ID（BaseId）
	Type           int    // 道具类型
	Name           string // 道具名称
	Desc           string // 道具描述，获取来源描述
	Start          int    `json:",omitempty"` // 开始时间戳 秒 0表示无时效
	Duration       int    `json:",omitempty"` // 持续时间 秒
	Value          int    `json:",omitempty"` // 如果是时效道具，可折算成金币
	IsShow         int    // 是否在背包显示
	IsGift         int    // 是否允许赠送(0=不允许赠送  1=允许赠送)
	ShowPrice      int    // 显示（出售）价格
	IconUrl        string `json:",omitempty"` // 自定义Icon或动画资源
	DecorationType int    `json:",omitempty"` // 是否装扮类型
	EffectInfo
}

// 实体道具
type ItemPack struct {
	ItemId int `json:",omitempty"`
	Count  int `json:",omitempty"`
}

type UserItem struct {
	ItemId   int // 道具ID
	Count    int // 道具数量
	Start    int // 开始时间戳 秒 0表示还未开始使用
	Duration int // 持续时间 秒
}

func GroupItems(items []ItemPack) []ItemPack {
	mapItems := make(map[int]int)
	for _, v := range items {
		mapItems[v.ItemId] += v.Count
	}

	var ret []ItemPack
	for itemId, count := range mapItems {
		ret = append(ret, ItemPack{ItemId: itemId, Count: count})
	}
	return ret
}

// 获取item 的额外数量
func GetItemExtraCount(items []ItemPack, itemId int) int {
	for _, v := range items {
		if v.ItemId == itemId {
			return v.Count
		}
	}
	return 0
}

type GiftCard struct {
	ItemId        int    // 存在不同面额
	Key           string // 兑换码
	UserId        int    // 持有者ID
	OperateStatus int    // 操作状态
	ActivateTime  int64  // 激活时间，0表示未被激活
}
