package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

type Request_User struct {
	UserId    int
	IpAddress string
}

func AddUser(userId int, ipAddress string) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_User{
		UserId:    userId,
		IpAddress: ipAddress,
	}

	err := xclient.Call(context.Background(), "AddUser", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("item_invenroty failed to call: %v", err)
	}
}

func RemoveUser(userId int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_User{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "RemoveUser", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("item_invenroty failed to call: %v", err)
	}
}

type Request_Consume struct {
	UserId   int
	ItemId   int
	BulletId int
	Count    int
	IsGift   int
}

type Response_Consume struct {
	Success  bool
	ErrorMsg string
}

func Consume(userId, itemId, bulletId, count, isGift int) (bool, string) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_Consume{
		UserId:   userId,
		ItemId:   itemId,
		BulletId: bulletId,
		Count:    count,
		IsGift:   isGift,
	}

	reply := &Response_Consume{}

	err := xclient.Call(context.Background(), "Consume", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false, "Comsume failed rpc call"
	}

	return reply.Success, reply.ErrorMsg
}

type Request_Sell struct {
	UserId  int
	ItemId  int
	Count   int
	LogType int
}

type Response_Sell struct {
	Success  bool
	ErrorMsg string
}

func Sell(userId, itemId, count, logType int) (bool, string) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_Sell{
		UserId:  userId,
		ItemId:  itemId,
		Count:   count,
		LogType: logType,
	}

	reply := &Response_Sell{}

	err := xclient.Call(context.Background(), "Sell", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false, "Sell failed rpc call"
	}

	return reply.Success, reply.ErrorMsg
}

type Request_ConsumeBulk struct {
	UserId  int
	Items   []ItemPack
	LogType int
}

type Response_ConsumeBulk struct {
	Success bool
}

func ConsumeBulk(userId int, items []ItemPack, logType int) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_ConsumeBulk{
		UserId:  userId,
		Items:   items,
		LogType: logType,
	}

	reply := &Response_ConsumeBulk{}

	err := xclient.Call(context.Background(), "ConsumeBulk", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}

	return reply.Success
}

type Request_ConsumeLottery struct {
	UserId int
	Count  int
}

type Response_ConsumeLottery struct {
	Success  bool
	ErrorMsg string
}

func ConsumeLottery(userId, itemId, count, logType int) (bool, string) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_ConsumeLottery{
		UserId: userId,
		Count:  count,
	}

	reply := &Response_ConsumeLottery{}

	err := xclient.Call(context.Background(), "ConsumeLottery", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false, "failed rpc call"
	}

	return reply.Success, reply.ErrorMsg
}

type Request_AddItems struct {
	UserId     int
	Items      []ItemPack
	LogType    int
	Desc       string
	ExpireTime int
}

type Response_AddItems struct {
	Success bool
}

func AddItems(userId int, items []ItemPack, desc string, logType int) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_AddItems{
		UserId:  userId,
		Items:   items,
		LogType: logType,
		Desc:    desc,
	}

	reply := &Response_AddItems{}

	err := xclient.Call(context.Background(), "AddItems", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}

	return reply.Success
}

// vip激活或升级时，根据vip时效添加时效道具
func AddItemsWithExpireTime(userId int, items []ItemPack, desc string, logType int, expireTime int) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_AddItems{
		UserId:     userId,
		Items:      items,
		LogType:    logType,
		Desc:       desc,
		ExpireTime: expireTime,
	}

	reply := &Response_AddItems{}

	err := xclient.Call(context.Background(), "AddItemsWithExpireTime", args, reply)
	if err != nil {
		log.Debug("item_invenroty.AddItemsWithExpireTime failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}

	return reply.Success
}

type Response_GetUserItems struct {
	Itms []UserItem
}

func GetUserItems(userId int) []UserItem {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response_GetUserItems{}

	err := xclient.Call(context.Background(), "GetUserItems", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}

	return reply.Itms
}

type Response_GetUserLottery struct {
	Itm *UserItem
}

func GetUserLottery(userId int) *UserItem {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response_GetUserLottery{}

	err := xclient.Call(context.Background(), "GetUserLottery", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}

	return reply.Itm
}

type Request_GetItemCount struct {
	UserId int
	ItemId int
}

type Response_GetItemCount struct {
	Count int
}

func GetItemCount(userId int, itemId int) int {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GetItemCount{
		UserId: userId,
		ItemId: itemId,
	}

	reply := &Response_GetItemCount{}

	err := xclient.Call(context.Background(), "GetItemCount", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}

	return reply.Count
}

type Request_Gift struct {
	UserId   int
	ToUserId int
	ItemId   int
	Count    int
}

type Response_Gift struct {
	Success  bool
	ErrorMsg string
}

func Gift(userId, toUserId, itemId, count int) (bool, string) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_Gift{
		UserId:   userId,
		ToUserId: toUserId,
		ItemId:   itemId,
		Count:    count,
	}

	reply := &Response_Gift{}

	err := xclient.Call(context.Background(), "Gift", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false, ""
	}

	return reply.Success, reply.ErrorMsg
}

type Request_DeduceByAdmin struct {
	OpUserId int
	OpName   string
	UserId   int
	ItemId   int
	Count    int
}

type Response_DeduceByAdmin struct {
	Success  bool
	ErrorMsg string
}

func ReduceItemByAdmin(opUserID int, opUserName string, userId, itemId, count int) (bool, string) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_DeduceByAdmin{
		OpUserId: opUserID,
		OpName:   opUserName,
		UserId:   userId,
		ItemId:   itemId,
		Count:    count,
	}

	reply := &Response_DeduceByAdmin{}

	err := xclient.Call(context.Background(), "ReduceItemByAdmin", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false, ""
	}

	return reply.Success, reply.ErrorMsg
}

type Request_CheckDecoration struct {
	UserId int
	ItemId int
	Type   int
}

type Response_CheckDecoration struct {
	Success bool
}

func CheckDecortaionType(decorationType int, itemId, userId int) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_CheckDecoration{
		UserId: userId,
		ItemId: itemId,
		Type:   decorationType,
	}

	reply := &Response_CheckDecoration{}

	err := xclient.Call(context.Background(), "CheckDecortaionType", args, reply)
	if err != nil {
		log.Debug("item_invenroty failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.Success
}
