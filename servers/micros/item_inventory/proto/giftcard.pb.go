package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

func GetGiftCardHistory(userId int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetGiftCardHistory", args, reply)
	if err != nil {
		log.Debug("GetGiftCardHistory failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}
