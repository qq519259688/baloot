package handler

import (
	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	"encoding/json"
)

// 获取用户道具列表
func getUserItemList(userId int) map[int]*item.UserItem {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserItem_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	userItems := make(map[int]*item.UserItem)

	var itemList []item.UserItem
	jsonData := dbengine.Execute(sqlstring)
	if err := json.Unmarshal([]byte(jsonData), &itemList); err != nil {
		log.Release("inventory.getUserItemList load json unmarshal err %v", err)
		return userItems
	}

	for i := 0; i < len(itemList); i++ {
		userItems[itemList[i].ItemId] = &itemList[i]
	}

	return userItems
}

// 用户道具更新
func updateUserItem(userId int, userItem *item.UserItem) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserItem_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ItemID", database.AdParamInput, database.AdInteger, 4, userItem.ItemId)
	statement.AddParamter("@ItemCount", database.AdParamInput, database.AdInteger, 4, userItem.Count)
	statement.AddParamter("@Start", database.AdParamInput, database.AdInteger, 4, userItem.Start)
	statement.AddParamter("@Duration", database.AdParamInput, database.AdInteger, 4, userItem.Duration)
	statement.AddParamter("@EnegyPoint", database.AdParamInput, database.AdInteger, 4, 0)
	sqlstring := statement.GenSql()
	dbengine.Execute(sqlstring)
	return
}

// 用户道具删除
func delUserItem(userId int, userItem *item.UserItem) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserItem_Del")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ItemID", database.AdParamInput, database.AdInteger, 4, userItem.ItemId)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	dbengine.Execute(sqlstring)
	return
}

// 添加道具日志
func addLog(userId, itemId, currCount, wantCount, stillCount int, remark, ipAddress string) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserItem_AddLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ItemID", database.AdParamInput, database.AdInteger, 4, itemId)
	statement.AddParamter("@CurrCount", database.AdParamInput, database.AdInteger, 4, currCount)
	statement.AddParamter("@WantCount", database.AdParamInput, database.AdInteger, 4, wantCount)
	statement.AddParamter("@StillCount", database.AdParamInput, database.AdInteger, 4, stillCount)
	statement.AddParamter("@Remark", database.AdParamInput, database.AdNVarChar, 128, remark)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	sqlstring := statement.GenSql()
	dbengine.Execute(sqlstring)
}
