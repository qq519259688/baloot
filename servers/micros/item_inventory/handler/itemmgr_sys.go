package handler

import (
	"encoding/json"
	"os"
	"time"

	"bet24.com/log"
	pb "bet24.com/servers/micros/item_inventory/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "item_config"
const refresh_config_sec = 600

func (im *itemmgr) loadSysItemFromJson() bool {
	time.AfterFunc(refresh_config_sec*time.Second, func() {
		im.loadSysItemFromJson()
	})
	configString := platformconfig.GetConfig(config_key)
	if configString == "" {
		data, err := os.ReadFile("serviceconf/systemitems.json")
		if err != nil {
			return false
		}
		configString = string(data)
		platformconfig.SetConfig(config_key, configString)
	} else {
		log.Release("item.itemmgr loading config from redis")
	}

	if configString == im.lastConfig {
		return false
	}
	im.lastConfig = configString

	var list []pb.Item

	err := json.Unmarshal([]byte(configString), &list)
	if err != nil {
		log.Release("itemmgr.loadSysItemFromJson Unmarshal item failed err:%v", err)
		return false
	}

	if len(list) == 0 {
		log.Release("itemmgr.loadSysItemFromJson Unmarshal item failed err:%v", err)
		return false
	}

	im.lock.Lock()
	for k, v := range list {
		im.itemlist[v.Id] = &list[k]
	}
	im.lock.Unlock()

	return true
}
