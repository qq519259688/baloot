package handler

import (
	_ "encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/item_inventory/proto"
)

// 获取系统道具数值列表
func getSysItemList() map[int]*pb.Item {
	/*statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Item_GetList")
	sqlstring := statement.GenSql()
	var items []pb.Item
	jsonData := dbengine.Execute(sqlstring)

	if err := json.Unmarshal([]byte(jsonData), &items); err != nil {
		log.Error("transaction.getSysItemList json unmarshal err %v", err)
		return nil
	}

	rowLen := len(items)
	ret := make(map[int]*pb.Item)
	for i := 0; i < rowLen; i++ {
		itm := items[i]
		ret[itm.Id] = &itm
	}

	return ret*/

	defer func() {
		if err := recover(); err != nil {
			log.Error("item.getSysItemList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Item_GetList")
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)

	items := make(map[int]*pb.Item)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out pb.Item

		out.Id = int((ret[0]).(int64))
		out.Name = (ret[1]).(string)
		out.Type = int((ret[2]).(int64))
		out.Desc = (ret[3]).(string)
		out.Start = int((ret[4]).(int64))
		out.Duration = int((ret[5]).(int64))
		out.ActiveId = int((ret[6]).(int64))
		out.Value = int((ret[7]).(int64))
		out.IsShow = int((ret[8]).(int64))
		out.IsGift = int((ret[9]).(int64))
		out.ShowPrice = int((ret[10]).(int64))
		out.IconUrl = ret[11].(string)
		out.DecorationType = int(ret[12].(int64))

		items[out.Id] = &out
	}

	return items
}
