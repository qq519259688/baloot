package handler

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/item_inventory/proto"
	"context"
	"encoding/json"
	"fmt"
)

var instance *ItemInventory

func GetInstance() *ItemInventory {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *ItemInventory {
	ret := new(ItemInventory)
	ret.ctor()
	return ret
}

type ItemInventory struct {
}

func (h *ItemInventory) ctor() {
	getItemManager()
	getInventoryManager()
	getGiftCardManager()
}

func (d *ItemInventory) dump(cmd, param1, param2 string) {
	switch cmd {
	case "sys":
		getInventoryManager().dumpSys(param1)
	case "user":
		getInventoryManager().dumpUser(param1)
	default:
		log.Release("ItemInventory.Dump unhandled cmd %s", cmd)
	}
}

func (h *ItemInventory) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s %d %s", pb.ServiceName, req.Name, req.UserId, req.ExtraData)
	return nil
}

// item handlers
func (h *ItemInventory) GetItem(ctx context.Context, req *pb.Request_GetItem, rsp *pb.Response_GetItem) error {
	rsp.Itm = getItemManager().getItem(req.ItemId)
	return nil
}
func (h *ItemInventory) GetItems(ctx context.Context, req *pb.Request, rsp *pb.Response_GetItems) error {
	rsp.Itms = getItemManager().getItems()
	return nil
}

func (h *ItemInventory) CheckDecortaionType(ctx context.Context, req *pb.Request_CheckDecoration, rsp *pb.Response_CheckDecoration) error {
	rsp.Success = getInventoryManager().checkDecortaionType(req.ItemId, req.Type, req.UserId)
	return nil
}

func (h *ItemInventory) GetItemValue(ctx context.Context, req *pb.Request_GetItemValue, rsp *pb.Response_GetItemValue) error {
	rsp.Value = getItemManager().getItemValue(req.ItemId, req.Count)
	return nil
}

func (h *ItemInventory) ChangeItemCountToDuracion(ctx context.Context, req *pb.Request_ItemPack, rsp *pb.Request_ItemPack) error {
	rsp.Items = getItemManager().changeItemCountToDuracion(req.Items)
	return nil
}

// inventory handlers
func (h *ItemInventory) AddUser(ctx context.Context, req *pb.Request_User, rsp *pb.Response) error {
	getInventoryManager().onUserEnter(req.UserId, req.IpAddress)
	return nil
}

func (h *ItemInventory) RemoveUser(ctx context.Context, req *pb.Request_User, rsp *pb.Response) error {
	getInventoryManager().onUserExit(req.UserId)
	return nil
}

func (h *ItemInventory) Consume(ctx context.Context, req *pb.Request_Consume, rsp *pb.Response_Consume) error {
	rsp.Success, rsp.ErrorMsg = getInventoryManager().consume(req.UserId, req.ItemId, req.BulletId, req.Count, req.IsGift)
	return nil
}

func (h *ItemInventory) Sell(ctx context.Context, req *pb.Request_Sell, rsp *pb.Response_Sell) error {
	rsp.Success, rsp.ErrorMsg = getInventoryManager().sell(req.UserId, req.ItemId, req.Count, req.LogType)
	return nil
}

func (h *ItemInventory) ConsumeBulk(ctx context.Context, req *pb.Request_ConsumeBulk, rsp *pb.Response_ConsumeBulk) error {
	rsp.Success = getInventoryManager().consumeBulk(req.UserId, req.Items, req.LogType)
	return nil
}

func (h *ItemInventory) ConsumeLottery(ctx context.Context, req *pb.Request_ConsumeLottery, rsp *pb.Response_ConsumeLottery) error {
	rsp.Success, rsp.ErrorMsg = getInventoryManager().consumeLottery(req.UserId, req.Count)
	return nil
}

func (h *ItemInventory) AddItems(ctx context.Context, req *pb.Request_AddItems, rsp *pb.Response_AddItems) error {
	rsp.Success = getInventoryManager().addItems(req.UserId, req.Items, req.Desc, req.LogType)
	return nil
}

func (h *ItemInventory) AddItemsWithExpireTime(ctx context.Context, req *pb.Request_AddItems, rsp *pb.Response_AddItems) error {
	rsp.Success = getInventoryManager().addItemsWithExpireTime(req.UserId, req.Items, req.Desc, req.LogType, req.ExpireTime)
	return nil
}

func (h *ItemInventory) GetUserItems(ctx context.Context, req *pb.Request, rsp *pb.Response_GetUserItems) error {
	itms := getInventoryManager().getUserItems(req.UserId)
	for _, v := range itms {
		rsp.Itms = append(rsp.Itms, *v)
	}
	return nil
}

func (h *ItemInventory) GetUserLottery(ctx context.Context, req *pb.Request, rsp *pb.Response_GetUserLottery) error {
	rsp.Itm = getInventoryManager().getUserLottery(req.UserId)

	return nil
}

func (h *ItemInventory) GetItemCount(ctx context.Context, req *pb.Request_GetItemCount, rsp *pb.Response_GetItemCount) error {
	rsp.Count = getInventoryManager().getItemCount(req.UserId, req.ItemId)
	return nil
}

func (h *ItemInventory) Gift(ctx context.Context, req *pb.Request_Gift, rsp *pb.Response_Gift) error {
	rsp.Success, rsp.ErrorMsg = getInventoryManager().gift(req.UserId, req.ToUserId, req.ItemId, req.Count)
	return nil
}
func (h *ItemInventory) ReduceItemByAdmin(ctx context.Context, req *pb.Request_DeduceByAdmin, rsp *pb.Response_DeduceByAdmin) error {
	rsp.Success, rsp.ErrorMsg = getInventoryManager().reduceItemByAdmin(req.OpUserId, req.OpName, req.UserId, req.ItemId, req.Count)
	return nil
}

func (h *ItemInventory) GetGiftCardHistory(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	ret := getGiftCardManager().getHistory(req.UserId)
	d, _ := json.Marshal(ret)
	rsp.Data = string(d)
	return nil
}
