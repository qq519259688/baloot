package handler

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/item_inventory/proto"
	"sync"
)

type itemmgr struct {
	itemlist   map[int]*pb.Item
	lock       *sync.RWMutex
	lastConfig string

	lockDuration  *sync.RWMutex
	itemDurations map[int]int
}

var item_manager *itemmgr

func getItemManager() *itemmgr {
	if item_manager == nil {
		item_manager = newItemMgr()
	}

	return item_manager
}

func newItemMgr() *itemmgr {
	ret := new(itemmgr)
	ret.itemlist = make(map[int]*pb.Item)
	ret.lock = &sync.RWMutex{}

	ret.itemDurations = make(map[int]int)
	ret.lockDuration = &sync.RWMutex{}
	ret.loadItems()
	return ret
}

func (im *itemmgr) loadItems() {
	if im.loadSysItemFromJson() {
		return
	}
	items := getSysItemList()

	im.lock.Lock()
	im.itemlist = items
	im.lock.Unlock()

	im.lockDuration.Lock()
	im.itemDurations = make(map[int]int)
	for _, v := range items {
		if v.Duration > 0 {
			im.itemDurations[v.Id] = v.Duration
		}
	}
	im.lockDuration.Unlock()
}

func (im *itemmgr) getItem(itemId int) *pb.Item {
	im.lock.RLock()
	defer im.lock.RUnlock()
	item, ok := im.itemlist[itemId]
	if !ok {
		return nil
	}
	return item
}

func (im *itemmgr) getItems() map[int]*pb.Item {
	im.lock.RLock()
	defer im.lock.RUnlock()
	return im.itemlist
}

func (im *itemmgr) getItemValue(itemId int, count int) int {
	itm := im.getItem(itemId)
	if itm == nil {
		return 0
	}

	if itm.Id == pb.Item_Gold || itm.Id == pb.Item_Chip {
		return count
	}

	return itm.ShowPrice * count
}

func (im *itemmgr) checkDecortaionType(itemId int, t int) bool {
	itm := im.getItem(itemId)
	if itm == nil {
		log.Release("itemmgr.checkDecortaionType item[%d] not found", itemId)
		return false
	}

	if itm.Type != pb.Item_Decoration {
		log.Release("itemmgr.checkDecortaionType item[%d] not a decoration", itemId)
		return false
	}
	if itm.DecorationType != t {
		log.Release("itemmgr.checkDecortaionType item[%d] mismatch type [%d] is not [%d]", itemId, t, itm.DecorationType)
		return false
	}
	return true
}

func (im *itemmgr) isVirtualItem(itemId int) bool {
	im.lock.RLock()
	defer im.lock.RUnlock()
	for _, v := range im.itemlist {
		if v.ActiveId == itemId {
			return true
		}
	}
	return false
}

func (im *itemmgr) changeItemCountToDuracion(items []pb.ItemPack) []pb.ItemPack {
	for k, v := range items {
		duration := im.getItemDuration(v.ItemId)
		if duration > 0 {
			items[k].Count = -duration
		}
	}
	return items
}

func (im *itemmgr) getItemDuration(itemId int) int {
	im.lockDuration.RLock()
	d, ok := im.itemDurations[itemId]
	im.lockDuration.RUnlock()
	if !ok {
		return 0
	}
	return d
}
