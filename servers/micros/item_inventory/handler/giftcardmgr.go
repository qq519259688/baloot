package handler

import (
	"bet24.com/log"
	"bet24.com/public"
	item "bet24.com/servers/micros/item_inventory/proto"
)

var giftCardMgr *giftcard_manager

type giftcard_manager struct {
}

func getGiftCardManager() *giftcard_manager {
	if giftCardMgr == nil {
		giftCardMgr = newGiftCardManager()
	}
	return giftCardMgr
}

func newGiftCardManager() *giftcard_manager {
	ret := new(giftcard_manager)
	ret.ctor()
	return ret
}

func (gm *giftcard_manager) ctor() {
	log.Debug("giftcard_manager.run")
}

// 生成未激活卡
func (gm *giftcard_manager) genCard(userId, itemId, count int) {
	log.Debug("giftcard_manager.genCard userId[%d],itemId[%d],count[%d]", userId, itemId, count)
	for i := count; i > 0; i-- {
		key := public.GenCode(6)
		trans_GenGiftCard(userId, itemId, key)
	}
}

// 写入使用记录
func (gm *giftcard_manager) useCard(userId int, itemId int) (bool, string) {
	// 通知数据库使用
	return trans_UseGiftCard(userId, itemId)
}

// 获取用户使用记录
func (gm *giftcard_manager) getHistory(userId int) []item.GiftCard {
	return trans_GetGiftCardHistory(userId)
}
