package handler

import (
	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	"encoding/json"
)

// 生成礼品卡
func trans_GenGiftCard(userId, itemId int, key string) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_GiftCard_Gen")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ItemID", database.AdParamInput, database.AdInteger, 4, itemId)
	statement.AddParamter("@Key", database.AdParamInput, database.AdNVarChar, 128, key)
	sqlString := statement.GenSql()
	log.Debug(sqlString)
	dbengine.Execute(sqlString)
}

// 使用礼品卡
func trans_UseGiftCard(userId, itemId int) (bool, string) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_GiftCard_Use")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ItemID", database.AdParamInput, database.AdInteger, 4, itemId)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return false, ""
	}
	ret := int(retRows[0][0].(int64)) > 0
	key := retRows[0][1].(string)
	return ret, key
}

// 获取使用记录
func trans_GetGiftCardHistory(userId int) []item.GiftCard {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_GiftCard_GetHistory")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	var ret []item.GiftCard
	jsonData := dbengine.Execute(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Release("inventory.getGiftCardList load json unmarshal err %v", err)
		return nil
	}
	return ret
}
