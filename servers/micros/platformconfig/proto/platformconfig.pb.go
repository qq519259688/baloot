package proto

import (
	"context"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "platformconfig"

const NOTIFY_MESSAGE = "RefreshRedisConfig"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name  string
	Value string
}

type Response struct {
	Data string
	List []string
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("platformconfig failed to call1: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

func GetConfig(name string) string {
	xclient := getClient()
	args := &Request{
		Name: name,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "GetConfig", args, reply)
	if err != nil {
		log.Debug("platformconfig failed to call2: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func SetConfig(name string, value string) {
	xclient := getClient()
	args := &Request{
		Name:  name,
		Value: value,
	}

	err := xclient.Call(context.Background(), "SetConfig", args, nil)
	if err != nil {
		log.Debug("platformconfig failed to call3: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return
	}
}

func GetConfigList() []string {
	xclient := getClient()
	args := &Request{}
	reply := &Response{}
	err := xclient.Call(context.Background(), "GetConfigList", args, reply)
	if err != nil {
		log.Debug("platformconfig failed to call4: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return []string{}
	}
	return reply.List
}

func GetChangeLog(key string) string {
	xclient := getClient()
	args := &Request{Name: key}
	reply := &Response{}
	err := xclient.Call(context.Background(), "GetChangeLog", args, reply)
	if err != nil {
		log.Debug("platformconfig failed to call5: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}
