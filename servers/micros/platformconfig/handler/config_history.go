package handler

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	"encoding/json"
	"sync"
	"time"
)

const max_change_log = 5

type history struct {
	Key        string
	OldContent string
	Content    string
	ChangeTime string
}

type config_history struct {
	lock      *sync.RWMutex
	changeLog map[string][]history
}

var configHistory *config_history

func getHistoryInstance() *config_history {
	if configHistory == nil {
		configHistory = new(config_history)
		configHistory.ctor()
	}
	return configHistory
}

func (ch *config_history) ctor() {
	log.Debug("config_history.ctor")
	ch.changeLog = make(map[string][]history)
	ch.lock = &sync.RWMutex{}
}

func (ch *config_history) dump(param1, param2 string) {
	log.Release("-------------------------------")
	log.Release("config_history.dump [%s,%s]", param1, param2)
	log.Release("+++++++++++++++++++++++++++++++")
	if param1 == "" {
		// dump all keys
		log.Release("  dump all keys")
		ch.lock.RLock()
		for k, v := range ch.changeLog {
			log.Release("    [%s] %d changes", k, len(v))
		}
		ch.lock.RUnlock()
		return
	}

	ch.lock.RLock()
	changes, ok := ch.changeLog[param1]
	ch.lock.RUnlock()
	if !ok {
		log.Release("  key[%s] not found", param1)
		return
	} else {
		log.Release("  key[%s] changeLog:", param1)
	}

	for _, v := range changes {
		log.Release("    [%s][%s]->[%s]", v.ChangeTime, v.OldContent, v.Content)
	}
}

func (ch *config_history) addBackup(key, oldValue, newValue string) {
	ch.lock.Lock()
	defer ch.lock.Unlock()
	changes, ok := ch.changeLog[key]

	h := history{Key: key, OldContent: oldValue, Content: newValue, ChangeTime: common.TimeStampToShortString(time.Now().Unix())}

	if !ok {
		ch.changeLog[key] = []history{h}
		return
	}
	changes = append([]history{h}, changes...)
	if len(changes) > max_change_log {
		changes = changes[:max_change_log]
	}

	ch.changeLog[key] = changes
}

func (ch *config_history) getChangeLogs(key string) string {
	ch.lock.RLock()
	defer ch.lock.RUnlock()
	logs, ok := ch.changeLog[key]
	if !ok {
		return ""
	}
	d, _ := json.Marshal(logs)
	return string(d)
}
