package handler

import (
	"bet24.com/log"
	"bet24.com/redis"
	pb "bet24.com/servers/micros/platformconfig/proto"
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

var instance *PlatformConfig

func GetInstance() *PlatformConfig {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *PlatformConfig {
	ret := new(PlatformConfig)
	ret.ctor()
	return ret
}

type PlatformConfig struct{}

func (h *PlatformConfig) ctor() {

}

func (d *PlatformConfig) dump(cmd, param1, param2 string) {
	//log.Debug("DbEngine.Dump %s,%s,%s", cmd, param1, param2)
	if cmd == "history" {
		getHistoryInstance().dump(param1, param2)
		return
	}
	if cmd == "" {
		d.dumpKeys()
		return
	}
	log.Release("-------------------------------")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	log.Release("    platformconfig key[%s] = %s", cmd, d.getConfig(cmd))
}

const redis_key = "platformconfig"

const cacheUpdateSecond = 60

var keyscache struct {
	keys       []string
	lastUpdate int64
}

func getRedisKey(key string) string {
	return fmt.Sprintf("%s:%s", redis_key, key)
}

func (h *PlatformConfig) getConfig(key string) string {
	ret, ok := redis.String_Get(getRedisKey(key))
	if !ok {
		return ""
	}
	return ret
}

func (h *PlatformConfig) setConfig(key, value string) {
	if !json.Valid([]byte(value)) {
		log.Debug("platformconfig.setConfig invalid data:%s", value)
		return
	}
	oldValue := h.getConfig(key)
	redis.String_Set(getRedisKey(key), value)

	getHistoryInstance().addBackup(key, oldValue, value)

	// 广播一个通知
	var d redis.Channel_msg
	d.Message = pb.NOTIFY_MESSAGE
	d.Data = key
	js, _ := json.Marshal(d)
	redis.Publish(string(js))
}

func getCopyList(src []string) []string {
	var ret []string
	if len(src) > 0 {
		ret = make([]string, len(src))
		copy(ret, src)
	}
	return ret
}

func (h *PlatformConfig) getConfigList() []string {
	now := time.Now().Unix()
	if now-keyscache.lastUpdate < cacheUpdateSecond {
		return getCopyList(keyscache.keys)
	}
	keyscache.keys = []string{}
	keys := redis.Key_GetKeys(fmt.Sprintf("%s:*", redis_key))
	for _, v := range keys {
		parts := strings.Split(v, ":")
		if len(parts) > 1 {
			keyscache.keys = append(keyscache.keys, parts[1])
		}
	}
	keyscache.lastUpdate = now
	return getCopyList(keyscache.keys)
}

func (h *PlatformConfig) dumpKeys() {
	log.Release("-------------------------------")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	keys := h.getConfigList()
	log.Release("platformconfig.dumpKeys count [%d], lastUpdate past %d", len(keys), time.Now().Unix()-keyscache.lastUpdate)

	for _, v := range keys {
		log.Release("    %s", v)
	}
}
