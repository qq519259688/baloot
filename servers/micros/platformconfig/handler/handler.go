package handler

import (
	pb "bet24.com/servers/micros/platformconfig/proto"
	"context"
	"fmt"
)

func (h *PlatformConfig) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (h *PlatformConfig) GetConfig(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = h.getConfig(req.Name)
	return nil
}

func (h *PlatformConfig) SetConfig(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	h.setConfig(req.Name, req.Value)
	return nil
}

func (h *PlatformConfig) GetConfigList(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.List = h.getConfigList()
	return nil
}

func (h *PlatformConfig) GetChangeLog(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = getHistoryInstance().getChangeLogs(req.Name)
	return nil
}
