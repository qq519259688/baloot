package proto

const (
	RoomType_Normal      = "PrivateRoom"
	RoomType_SimpleMatch = "Match"
)

const (
	PrivateRoomStatus_Invalid = iota
	PrivateRoomStatus_Free
	PrivateRoomStatus_Playing
	PrivateRoomStatus_Ended
)

const (
	Method_Create        = "privateroom.create"
	Method_Dismiss       = "privateroom.onRoomDismiss"
	Method_StatucChanged = "privateroom.onRoomStatusChanged"
	Method_ForceEnter    = "privateroom.forceEnter"
)

type Request_Create struct {
	MethodName string
	Creator    int    // 创建者
	GameRule   string // 游戏规则
	Target     int    // 局数或分数
	UserCount  int    // 对局人数
	//Fee       int    // 门票费用
	PlayTimeout int
}

type Request_OnRoomDismiss struct {
	MethodName string
	RoomNo     int
}

type Request_OnRoomStatusChanged struct {
	MethodName string
	RoomNo     int
	OldStatus  int
	NewStatus  int
}

type Request_ForceEnter struct {
	MethodName string
	RoomNo     int
	UserId     int
	ChairId    int
}

type RoomUser struct {
	UserId   int
	NickName string
	ChairId  int
	Score    int
	// 可能还有其他信息
	FaceId    int
	FaceUrl   string
	BaseScore int
	SetCount  int
}

type RoomUserBrief struct {
	UserId    int
	NickName  string
	ChairId   int
	Score     int
	BaseScore int
	SetCount  int `json:",omitempty"`
}

func (ru *RoomUser) ToBrief() *RoomUserBrief {
	return &RoomUserBrief{
		UserId:    ru.UserId,
		NickName:  ru.NickName,
		ChairId:   ru.ChairId,
		Score:     ru.Score,
		BaseScore: ru.BaseScore,
		SetCount:  ru.SetCount,
	}
}

type RoomInfo struct {
	RoomNo     int
	GameId     int
	ServerAddr string
	TableId    int
	GameName   string
	RuleName   string
	UserCount  int // 限制人数
	Fee        int
	Status     int
	Target     int
	Prize      int // 奖金
	IsPublic   bool
	UserList   []*RoomUser
	PlayTime   int
	ExtraInfo  string
}

type RoomInfoBrief struct {
	RoomNo     int
	GameId     int
	ServerAddr string
	TableId    int
	RuleName   string
	UserCount  int // 限制人数
	Fee        int `json:",omitempty"`
	Target     int
	Prize      int // 奖金
	IsPublic   bool
	UserList   []*RoomUserBrief
}

func (ru *RoomInfo) ToBrief() *RoomInfoBrief {
	ret := &RoomInfoBrief{
		RoomNo:     ru.RoomNo,
		GameId:     ru.GameId,
		ServerAddr: ru.ServerAddr,
		TableId:    ru.TableId,
		RuleName:   ru.RuleName,
		UserCount:  ru.UserCount,
		Target:     ru.Target,
		Prize:      ru.Prize,
		IsPublic:   ru.IsPublic,
		Fee:        ru.Fee,
	}
	for i := 0; i < len(ru.UserList); i++ {
		ret.UserList = append(ret.UserList, ru.UserList[i].ToBrief())
	}
	return ret
}
