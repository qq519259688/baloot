package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	status_client "bet24.com/servers/micros/privateroom/status/client"
	status_server "bet24.com/servers/micros/privateroom/status/server"
	"context"
	"fmt"
	"github.com/smallnest/rpcx/client"
	"os"
	"time"
)

const ServiceName = "privateroom"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name     string
	UserId   int
	GameId   int
	GameName string
	Addr     string
	TableId  int
	RoomNo   int
	Status   int

	RuleName string
	RuleDesc string
	RuleData string

	Online int

	Target         int
	UserCount      int
	Fee            int
	Prize          int
	RoomType       string
	PlayTimeout    int
	TableUserCount int

	NickName string
	FaceId   int
	FaceUrl  string

	ChairId int

	Score     int
	BaseScore int
	SetCount  int

	Winners []int

	SecAfter  int
	IsPublic  bool
	ExtraInfo string
}

type Response struct {
	Data     string
	RoomNo   int
	ErrorMsg string
	Status   int
	Success  bool
	RetCode  int
	User     *RoomUser
	Fee      int
	Prize    int
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

func CreatePrivateRoomByGameServer(userId int, gameId int, gameName string, addr string, tableId int) (int, string) {
	xclient := getClient()
	args := &Request{
		UserId:   userId,
		GameId:   gameId,
		GameName: gameName,
		Addr:     addr,
		TableId:  tableId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "CreatePrivateRoomByGameServer", args, reply)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, "server error"
	}

	return reply.RoomNo, reply.ErrorMsg
}

func GetPrivateRoomInfo(roomNo int) string {
	xclient := getClient()
	args := &Request{
		RoomNo: roomNo,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetPrivateRoomInfo", args, reply)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	return reply.Data
}

func GetUserRooms(userId int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserRooms", args, reply)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		return ""
	}

	return reply.Data
}

func SetRoomStatus(roomNo, status int) {
	xclient := getClient()
	args := &Request{
		RoomNo: roomNo,
		Status: status,
	}

	err := xclient.Call(context.Background(), "SetRoomStatus", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
	}
}

func GetRoomStatus(roomNo int) int {
	xclient := getClient()
	args := &Request{
		RoomNo: roomNo,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetRoomStatus", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
		return 0
	}
	return reply.Status
}

func RegisterGameRule(addr string, gameId int, gameName string, ruleName, ruleDesc, ruleData string) {
	//log.Debug("RegisterGameRule %s", addr)
	xclient := getClient()
	args := &Request{
		Addr:     addr,
		GameId:   gameId,
		GameName: gameName,
		RuleName: ruleName,
		RuleDesc: ruleDesc,
		RuleData: ruleData,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "RegisterGameRule", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
	}
}

func UpdateServerOnline(addr string, online int) {
	xclient := getClient()
	args := &Request{
		Addr:   addr,
		Online: online,
	}

	err := xclient.Call(context.Background(), "UpdateServerOnline", args, nil)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
	}
}

func UnregisterServer(addr string) {
	xclient := getClient()
	args := &Request{
		Addr: addr,
	}

	err := xclient.Call(context.Background(), "UnregisterServer", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
	}
}

func GetPrivateRoomRules(gameId int) string {
	xclient := getClient()
	args := &Request{
		GameId: gameId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetPrivateRoomRules", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
		return ""
	}
	return reply.Data
}

func CreatePrivateRoomByUser(userId int, gameId int, ruleName string, target int, userCount int, fee int,
	prize int, roomType string, playTimeout int, isPublic bool, extraInfo string) (int, string) {
	xclient := getClient()
	args := &Request{
		UserId:      userId,
		GameId:      gameId,
		RuleName:    ruleName,
		Target:      target,
		UserCount:   userCount,
		Fee:         fee,
		Prize:       prize,
		RoomType:    roomType,
		PlayTimeout: playTimeout,
		IsPublic:    isPublic,
		ExtraInfo:   extraInfo,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "CreatePrivateRoomByUser", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
		return 0, "server error"
	}

	return reply.RoomNo, reply.ErrorMsg
}

func ClosePrivateRoom(roomNo int) bool {
	if roomNo == 0 {
		return false
	}
	xclient := getClient()
	args := &Request{
		RoomNo: roomNo,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "ClosePrivateRoom", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
		return false
	}
	return reply.Success
}

func DismissPrivateRoom(roomNo int) bool {
	xclient := getClient()
	args := &Request{
		RoomNo: roomNo,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "DismissPrivatRoom", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
		return false
	}
	return reply.Success
}

func UserRequestSit(roomNo int, userId int, nickName string, faceId int, faceUrl string, chairId int, score, baseScore, setCount int) string {
	xclient := getClient()
	args := &Request{
		RoomNo:    roomNo,
		UserId:    userId,
		NickName:  nickName,
		FaceId:    faceId,
		FaceUrl:   faceUrl,
		ChairId:   chairId,
		Score:     score,
		BaseScore: baseScore,
		SetCount:  setCount,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "UserRequestSit", args, reply)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		return "server error"
	}
	return reply.Data
}

func UserSit(roomNo int, userId int, chairId int) int {
	xclient := getClient()
	args := &Request{
		RoomNo:  roomNo,
		UserId:  userId,
		ChairId: chairId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "UserSit", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
		return -1
	}
	return reply.RetCode
}

func UserChangeChair(roomNo int, userId int, chairId int) bool {
	xclient := getClient()
	args := &Request{
		RoomNo:  roomNo,
		UserId:  userId,
		ChairId: chairId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "UserChangeChair", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
		return false
	}
	return reply.Success
}

func UserLeave(roomNo int, userId int) bool {
	xclient := getClient()
	args := &Request{
		RoomNo: roomNo,
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "UserLeave", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
		return false
	}
	return reply.Success
}

func UpdateUserGameScore(roomNo, userId, scoreDelta int) {
	xclient := getClient()
	args := &Request{
		RoomNo: roomNo,
		UserId: userId,
		Score:  scoreDelta,
	}

	err := xclient.Call(context.Background(), "UpdateUserGameScore", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
	}
}

func SetWinners(roomNo int, winners []int) {
	xclient := getClient()
	args := &Request{
		RoomNo:  roomNo,
		Winners: winners,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "SetWinners", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
	}
}

func GetHistory(userId int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetHistory", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
		return ""
	}
	return reply.Data
}

func GetPlayingRoomNo(userId int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetPlayingRoomNo", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
		return ""
	}
	return reply.Data
}

func IsGameRuleValid(gameId int, gameRule string, target int, tableUserCount int, playTime int) bool {
	xclient := getClient()
	args := &Request{
		GameId:         gameId,
		RuleName:       gameRule,
		Target:         target,
		TableUserCount: tableUserCount,
		PlayTimeout:    playTime,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "IsGameRuleValid", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
		return false
	}
	return reply.Success
}

func ForceUserEnter(userId int, roomNo int, chairId int, secAfter int) {
	xclient := getClient()
	args := &Request{
		RoomNo:   roomNo,
		UserId:   userId,
		ChairId:  chairId,
		SecAfter: secAfter,
	}

	err := xclient.Call(context.Background(), "ForceUserEnter", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("privateroom failed to call: %v", err)
	}
}

func GetPrivateRoomsByGameId(gameId int, userId int) string {
	xclient := getClient()
	args := &Request{
		GameId: gameId,
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetPrivateRoomsByGameId", args, reply)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func RegisterServerStatus(receiver status_client.IPrivateServerStatusReceiver, roomstatus_receiver IPrivateRoomStatusReceiver, port int, moduleName string) {
	if port == 0 {
		port = os.Getpid()
	}
	addr := fmt.Sprintf("%s_%d_%s", common.GetLocalIp(), port, moduleName)
	//log.Debug("privateroom.RegisterServerStatus %s", addr)
	go status_server.Run(addr, consulAddr, receiver, roomstatus_receiver)

	for {
		xclient := getClient()
		args := &Request{
			Addr: addr,
		}

		//log.Release("notification.Subscribe %s", addr)
		err := xclient.Call(context.Background(), "RegisterServerStatus", args, nil)
		if err != nil {
			log.Debug("Subscribe failed to call: %v", err)
			common.GetClientPool().RemoveClient(ServiceName)
		}
		time.Sleep(10 * time.Second)
	}
}

func UserSubscribeRoomStatus(userId int) {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "UserSubscribeRoomStatus", args, reply)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func UserDesubscribeRoomStatus(userId int) {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "UserDesubscribeRoomStatus", args, nil)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func GetCallList() string {
	xclient := getClient()
	args := &Request{}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetCallList", args, reply)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	return reply.Data
}

func PrivateRoomCall(userId int, roomNo int) {
	xclient := getClient()
	args := &Request{
		UserId: userId,
		RoomNo: roomNo,
	}

	err := xclient.Call(context.Background(), "PrivateRoomCall", args, nil)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func GetUserRoomInfo(userId int, roomNo int) *RoomUser {
	xclient := getClient()
	args := &Request{
		UserId: userId,
		RoomNo: roomNo,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserRoomInfo", args, reply)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.User
}

func GetRoomType(roomNo int) string {
	xclient := getClient()
	args := &Request{
		RoomNo: roomNo,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetRoomType", args, reply)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetFeeAndPrize(roomNo int, userId int) (int, int) {
	xclient := getClient()
	args := &Request{
		RoomNo: roomNo,
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserFeeAndPrize", args, reply)
	if err != nil {
		log.Debug("privateroom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, 0
	}
	return reply.Fee, reply.Prize
}
