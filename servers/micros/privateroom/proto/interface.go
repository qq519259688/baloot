package proto

type IPrivateRoomStatusReceiver interface {
	OnRoomStart(roomNo int)
	OnRoomEnd(roomNo int, winners []int)
	OnRoomUserScoreChanged(roomNo int, userId int, score int)
	OnRoomStatusChanged(roomNo int, old, new int)
}

type IPrivateServerStatusReceiver interface {
	OnGameRuleRegistered(gameId int, gameRule string, desc string,
		targetOptions []int, userOptions []int, playTimeOptions []int)
	OnGameRuleDeregistered(gameId int, gameRule string)
}
