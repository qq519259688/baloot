package server

import (
	"bet24.com/servers/micros/common"
	"bet24.com/servers/micros/privateroom/game/client"
	"context"
	"fmt"
)

var _addr string

func Run(gameName string, port int, consulPort int, sink IPrivateRoomSink) {
	_addr = fmt.Sprintf("privateroom_%s_%s_%d", gameName, common.GetLocalIp(), port)
	common.RunWithoutChangeTitle(_addr, newHandler(sink), fmt.Sprintf("8.141.80.205:%d", consulPort))
}

func GetAddr() string {
	return _addr
}

func newHandler(sink IPrivateRoomSink) *PrivateRoomServer {
	ret := new(PrivateRoomServer)
	ret._sink = sink
	return ret
}

type PrivateRoomServer struct {
	_sink IPrivateRoomSink
}

func (s *PrivateRoomServer) CreatePrivateRoom(ctx context.Context, req *client.Request_Create, reply *client.Reply) error {
	reply.RetCode = s._sink.CreatePrivateRoom(req.Creator, req.GameRule, req.Target, req.UserCount, req.PlayTimeout)
	return nil
}

func (s *PrivateRoomServer) OnRoomStatusChanged(ctx context.Context, req *client.Request_StatusChanged, reply *client.Reply) error {
	s._sink.OnRoomStatusChanged(req.RoomNo, req.OldStatus, req.NewStatus)
	return nil
}

func (s *PrivateRoomServer) OnRoomDismissed(ctx context.Context, req *client.Request_StatusChanged, reply *client.Reply) error {
	s._sink.OnRoomDismissed(req.RoomNo)
	return nil
}

func (s *PrivateRoomServer) ForceEnterUser(ctx context.Context, req *client.Request_ForceUserEnter, reply *client.Reply) error {
	s._sink.ForceEnterUser(req.UserId, req.RoomNo, req.ChairId)
	return nil
}

func (s *PrivateRoomServer) SetExtraParam(ctx context.Context, req *client.Request_ExtraParams, reply *client.Reply) error {
	reply.Data = s._sink.SetExtraParam(req.RoomNo, req.UserId, req.ParamId, req.Data)
	return nil
}
