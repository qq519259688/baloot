package server

type IPrivateRoomSink interface {
	CreatePrivateRoom(creator int, gameRule string, target int, userCount int, playTimeout int) int
	OnRoomStatusChanged(roomNo int, oldStatus, newStatus int)
	OnRoomDismissed(roomNo int)
	ForceEnterUser(userId int, roomNo int, chairId int)
	SetExtraParam(roomNo int, userId, paramId int, data string) string
}
