package client

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
	rpcx_client "github.com/smallnest/rpcx/client"
)

var consulAddr = common.Default_Consul_Addr

func getClient(serviceName string) rpcx_client.XClient {
	return common.GetClientPool().GetClient(serviceName, consulAddr)
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

type Reply struct {
	Data    string
	RetCode int
}

type Request_Create struct {
	Creator   int    // 创建者
	GameRule  string // 游戏规则
	Target    int    // 局数或分数
	UserCount int    // 对局人数
	//Fee       int    // 门票费用
	PlayTimeout int
}

type Request_StatusChanged struct {
	RoomNo    int
	OldStatus int
	NewStatus int
}

type Request_ForceUserEnter struct {
	UserId  int
	RoomNo  int
	ChairId int
}

type Request_ExtraParams struct {
	RoomNo  int
	UserId  int
	ParamId int
	Data    string
}

func CreatePrivateRoom(addr string, param Request_Create) int {
	log.Debug("CreatePrivateRoom addr = %s,Game = %s", addr, param.GameRule)
	xclient := getClient(addr)
	reply := &Reply{}
	err := xclient.Call(context.Background(), "CreatePrivateRoom", &param, reply)
	if err != nil {
		log.Debug("CreatePrivateRoom failed to call: %v", err)
		common.GetClientPool().RemoveClient(addr)
		return 0
	}

	return reply.RetCode
}

func OnRoomStatusChanged(addr string, roomNo, oldStatus, newStatus int) {
	log.Debug("OnRoomStatusChanged %d %d->%d", roomNo, oldStatus, newStatus)
	xclient := getClient(addr)
	param := &Request_StatusChanged{
		RoomNo:    roomNo,
		OldStatus: oldStatus,
		NewStatus: newStatus,
	}
	err := xclient.Call(context.Background(), "OnRoomStatusChanged", param, nil)
	if err != nil {
		log.Debug("OnRoomStatusChanged failed to call: %v", err)
		common.GetClientPool().RemoveClient(addr)
	}
}

func OnRoomDismissed(addr string, roomNo int) {
	xclient := getClient(addr)
	param := &Request_StatusChanged{
		RoomNo: roomNo,
	}
	err := xclient.Call(context.Background(), "OnRoomDismissed", param, nil)
	if err != nil {
		log.Debug("OnRoomDismissed failed to call: %v", err)
		common.GetClientPool().RemoveClient(addr)
	}
}

// 强制玩家进入，比赛撮合用
func ForceEnterUser(addr string, userId int, roomNo int, chairId int) {
	xclient := getClient(addr)
	param := &Request_ForceUserEnter{
		UserId:  userId,
		RoomNo:  roomNo,
		ChairId: chairId,
	}
	err := xclient.Call(context.Background(), "ForceEnterUser", param, nil)
	if err != nil {
		log.Debug("ForceEnterUser failed to call: %v", err)
		common.GetClientPool().RemoveClient(addr)
	}
}

func SetExtraParam(addr string, roomNo int, userId, paramId int, data string) {
	xclient := getClient(addr)
	param := &Request_ExtraParams{
		RoomNo:  roomNo,
		ParamId: paramId,
		UserId:  userId,
		Data:    data,
	}
	err := xclient.Call(context.Background(), "SetExtraParam", param, nil)
	if err != nil {
		log.Debug("SetExtraParam failed to call: %v", err)
		common.GetClientPool().RemoveClient(addr)
	}
}
