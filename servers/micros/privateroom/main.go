package main

import (
	"fmt"

	_ "bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/micros/common"
	"bet24.com/servers/micros/privateroom/config"
	
	"bet24.com/servers/micros/privateroom/handler"
	pb "bet24.com/servers/micros/privateroom/proto"
)

func main() {
	
	
	config.Run(pb.ServiceName)
	fmt.Printf("config success")
	//go waitInput()
	redis.InitPool(config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB)
	fmt.Printf("redis success")
	common.RunService(pb.ServiceName, handler.GetInstance(), fmt.Sprintf("8.141.80.205:%d", config.Server.ConsulPort))
	fmt.Printf("common success")
}

func waitInput() {
	for {
		var cmd string
		var param1 string
		var param2 string
		fmt.Scanf("%s %s %s", &cmd, &param1, &param2)
		switch cmd {
		case "":
			break
		case "exit":
			common.DeregisterService(pb.ServiceName)
		case "clientpool":
			common.DumpClientPools()
		default:
			handler.Dump(cmd, param1, param2)
		}
	}
}
