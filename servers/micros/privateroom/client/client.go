package main

import (
	"log"

	slog "bet24.com/log"
	pb "bet24.com/servers/micros/privateroom/proto"
)

type TestT struct {
	A int
	B string
}

func main() {

	var t1 []*TestT
	t1 = append(t1, &TestT{A: 1, B: "test1"})
	t1 = append(t1, &TestT{A: 2, B: "test2"})
	for _, v := range t1 {
		slog.Debug("t1 : %+v", *v)
	}

	t2 := make([]*TestT, len(t1))
	copy(t2, t1)

	for _, v := range t2 {
		slog.Debug("t2 : %+v", *v)
	}

	t2[0].A = 100

	for _, v := range t1 {
		slog.Debug("t1 : %+v", *v)
	}

	logger, err := slog.New("debug", "debug", "log/client", log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	slog.Debug(pb.SayHello("client"))
}
