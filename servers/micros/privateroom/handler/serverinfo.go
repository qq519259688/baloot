package handler

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/privateroom/proto"
	"encoding/json"
	"time"
)

const (
	server_timeout_sec = 60
)

type gameRule_option struct {
	TargetOptions    []int
	UserCountOptions []int
	PlayTimeOptions  []int
	EnrollFeeOptions []int
}

type gameRule struct {
	GameId  int
	Name    string
	Desc    string
	Data    string
	Options gameRule_option
}

func (gr *gameRule) isDual(userCount int) bool {
	if userCount != 2 {
		log.Debug("gameRule.isDual userCount = %d", userCount)
		return false
	}

	if len(gr.Options.UserCountOptions) != 2 {
		log.Debug("gameRule.isDual len(gr.Options.UserCountOptions) = %d", len(gr.Options.UserCountOptions))
		return false
	}

	ret := gr.Options.UserCountOptions[1] == 4
	if !ret {
		log.Debug("gameRule.isDual gr.Options.UserCountOptions[1] != 4 = %d", gr.Options.UserCountOptions[1])
	}
	return ret
}

func (gr *gameRule) isOptionValid(target, userCount, playTime, enrollFee int, roomType string) bool {
	// target == 1 为
	if len(gr.Options.TargetOptions) > 0 && target > 1 {
		found := false
		for _, v := range gr.Options.TargetOptions {
			if v == target {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}

	if len(gr.Options.UserCountOptions) > 0 {
		found := false
		for _, v := range gr.Options.UserCountOptions {
			if v == userCount {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}

	if len(gr.Options.PlayTimeOptions) > 0 {
		found := false
		for _, v := range gr.Options.PlayTimeOptions {
			if v == playTime {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}

	if len(gr.Options.EnrollFeeOptions) > 0 && roomType == pb.RoomType_Normal {
		found := false
		for _, v := range gr.Options.EnrollFeeOptions {
			if v == enrollFee {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}

	return true
}

func (gr *gameRule) queryOptions() {
	err := json.Unmarshal([]byte(gr.Data), &gr.Options)
	if err != nil {
		log.Release("gameRule.queryOptions Unmarshal failed %s", gr.Data)
	}
	//log.Debug("gameRule.queryOptions options=%v", gr.Options)
}

type serverInfo struct {
	GameId     int
	GameName   string
	Addr       string
	GameRules  []gameRule
	OnlineUser int
	lastPing   int64
	isOnline   bool
}

func (si *serverInfo) isTimeout() bool {
	return time.Now().Unix()-si.lastPing >= server_timeout_sec
}

func (si *serverInfo) getRule(ruleName string) *gameRule {
	for i := 0; i < len(si.GameRules); i++ {
		if si.GameRules[i].Name == ruleName {
			return &si.GameRules[i]
		}
	}
	return nil
}

func (si *serverInfo) addGameRule(name, desc, data string) {
	for i := 0; i < len(si.GameRules); i++ {
		if si.GameRules[i].Name == name {
			si.GameRules[i].Desc = desc
			si.GameRules[i].Data = data
			si.GameRules[i].GameId = si.GameId
			si.GameRules[i].queryOptions()
			return
		}
	}
	newGameRule := gameRule{GameId: si.GameId, Name: name, Desc: desc, Data: data}
	newGameRule.queryOptions()
	si.GameRules = append(si.GameRules, newGameRule)
}

func (si *serverInfo) dump() {
	log.Release("    Server[%d][%s][%s] online[%d] idled[%d]", si.GameId, si.GameName, si.Addr, si.OnlineUser, time.Now().Unix()-si.lastPing)
	for k, v := range si.GameRules {
		log.Release("        Rule[%d]:%v", k, v)
	}
	log.Release("    ------")
}

func (si *serverInfo) updateOnline(online int) {
	si.isOnline = true
	si.lastPing = time.Now().Unix()
	si.OnlineUser = online
}
