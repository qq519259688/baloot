package handler

import (
	"bet24.com/log"
	notification "bet24.com/servers/micros/notification/proto"
	pb "bet24.com/servers/micros/privateroom/proto"
	"encoding/json"
	"sync"
	"time"
)

var subsmgr *subsmanager

const subs_timeout = 60

func getSubsManager() *subsmanager {
	if subsmgr == nil {
		subsmgr = new(subsmanager)
		subsmgr.run()
	}
	return subsmgr
}

type subsmanager struct {
	userlist map[int]int64
	lock     *sync.RWMutex
}

func (sm *subsmanager) run() {
	sm.userlist = make(map[int]int64)
	sm.lock = &sync.RWMutex{}
	go sm.checkTimeout()
}

func (sm *subsmanager) checkTimeout() {
	time.AfterFunc(20*time.Second, sm.checkTimeout)
	now := time.Now().Unix()
	var toRemove []int
	sm.lock.RLock()
	for k, v := range sm.userlist {
		if now-v >= subs_timeout {
			toRemove = append(toRemove, k)
		}
	}
	sm.lock.RUnlock()
	if len(toRemove) <= 0 {
		return
	}

	log.Debug("subsmanager.checkTimeout removing users %v", toRemove)

	sm.lock.Lock()
	for _, v := range toRemove {
		delete(sm.userlist, v)
	}
	sm.lock.Unlock()
}

func (sm *subsmanager) userSubscribeRoomStatus(userId int) {
	sm.lock.Lock()
	sm.userlist[userId] = time.Now().Unix()
	sm.lock.Unlock()
}

func (sm *subsmanager) userDesubscribeRoomStatus(userId int) {
	sm.lock.Lock()
	delete(sm.userlist, userId)
	sm.lock.Unlock()
}

func (sm *subsmanager) publishRawData(data string) {
	sm.lock.RLock()
	for k := range sm.userlist {
		go notification.AddNotification(k, notification.Notification_PrivateRoom, data)
	}

	sm.lock.RUnlock()
}

func (sm *subsmanager) publishRoomCreated(roomNo int) {
	r := getRoomManager().getRoomInfo(roomNo)
	if r == nil {
		log.Release("subsmanager publishRoomCreated [%d] not found", roomNo)
		return
	}
	msg := Match_notificationInfo{Msg: room_created, RoomNo: roomNo, RoomInfo: &r.RoomInfo}
	d, _ := json.Marshal(msg)
	sm.publishRawData(string(d))
}

func (sm *subsmanager) publishRoomClosed(roomNo int) {
	msg := Match_notificationInfo{Msg: room_closed, RoomNo: roomNo}
	d, _ := json.Marshal(msg)
	sm.publishRawData(string(d))
}

func (sm *subsmanager) publishRoomInfo(roomInfo pb.RoomInfo) {
	msg := Match_notificationInfo{Msg: room_info, RoomNo: roomInfo.RoomNo, RoomInfo: &roomInfo}
	d, _ := json.Marshal(msg)
	sm.publishRawData(string(d))
}

func (sm *subsmanager) publishRoomCallAdded(roomNo int) {
	r := getRoomManager().getRoomInfo(roomNo)
	if r == nil {
		log.Release("subsmanager publishRoomCallAdded [%d] not found", roomNo)
		return
	}
	msg := Match_notificationInfo{Msg: room_call_added, RoomNo: roomNo}
	d, _ := json.Marshal(msg)
	sm.publishRawData(string(d))
}

func (sm *subsmanager) publishRoomCallRemoved(roomNo int) {
	msg := Match_notificationInfo{Msg: room_call_removed, RoomNo: roomNo}
	d, _ := json.Marshal(msg)
	sm.publishRawData(string(d))
}
