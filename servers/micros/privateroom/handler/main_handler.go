package handler

import (
	_ "bet24.com/log"
	pb "bet24.com/servers/micros/privateroom/proto"
	"context"
	"encoding/json"
	"fmt"
)

var instance *PrivateRoomService

func GetInstance() *PrivateRoomService {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *PrivateRoomService {
	ret := new(PrivateRoomService)
	ret.ctor()
	return ret
}

type PrivateRoomService struct{}

func (h *PrivateRoomService) ctor() {
	getRoomManager().run()
	getServerManager().run()
	getHistoryManager().run()
}

func (d *PrivateRoomService) dump(cmd, param1, param2 string) {
	//log.Debug("DbEngine.Dump %s,%s,%s", cmd, param1, param2)
	switch cmd {
	case "server":
		getServerManager().dump(param1)
		return
	case "history":
		getHistoryManager().dump(param1)
		return
	case "flush":
		getHistoryManager().forceFlush()
		getRoomManager().flush()
	default:
		getRoomManager().dump(param1)
	}
}

func (h *PrivateRoomService) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (h *PrivateRoomService) CreatePrivateRoomByGameServer(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.RoomNo, rsp.ErrorMsg = getRoomManager().addRoom(req.UserId, req.GameId, req.GameName, req.Addr, req.TableId)
	return nil
}

func (h *PrivateRoomService) GetPrivateRoomInfo(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	ri := getRoomManager().getRoomInfo(req.RoomNo)
	if ri == nil {
		return nil
	}

	d, _ := json.Marshal(ri)
	rsp.Data = string(d)
	return nil
}

func (h *PrivateRoomService) GetUserRooms(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = getRoomManager().getUserRooms(req.UserId)
	return nil
}

func (h *PrivateRoomService) SetRoomStatus(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getRoomManager().setRoomStatus(req.RoomNo, req.Status)
	return nil
}

func (h *PrivateRoomService) GetRoomStatus(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Status = getRoomManager().getRoomStatus(req.RoomNo)
	return nil
}

func (h *PrivateRoomService) RegisterGameRule(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getServerManager().addGameRule(req.Addr, req.GameId, req.GameName, req.RuleName, req.RuleDesc, req.RuleData)
	return nil
}

func (h *PrivateRoomService) UpdateServerOnline(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getServerManager().updateServerOnline(req.Addr, req.Online)
	return nil
}

func (h *PrivateRoomService) UnregisterServer(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getServerManager().removeServer(req.Addr, false)
	return nil
}

func (h *PrivateRoomService) GetPrivateRoomRules(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = getServerManager().getRules(req.GameId)
	return nil
}

func (h *PrivateRoomService) CreatePrivateRoomByUser(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.RoomNo, rsp.ErrorMsg = getServerManager().createRoom(req.UserId, req.GameId, req.RuleName, req.Target,
		req.UserCount, req.Fee, req.Prize, req.RoomType, req.PlayTimeout, req.IsPublic, req.ExtraInfo)
	return nil
}

func (h *PrivateRoomService) ClosePrivateRoom(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = getRoomManager().closeRoom(req.RoomNo, "Creator close")
	return nil
}

func (h *PrivateRoomService) DismissPrivatRoom(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = getRoomManager().closeRoom(req.RoomNo, "Player dismiss")
	return nil
}

func (h *PrivateRoomService) UserRequestSit(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = getRoomManager().userRequestSit(req.RoomNo, req.UserId, req.NickName, req.FaceId, req.FaceUrl, req.ChairId,
		req.Score, req.BaseScore, req.SetCount)
	return nil
}

func (h *PrivateRoomService) UserSit(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.RetCode = getRoomManager().userSit(req.RoomNo, req.UserId, req.ChairId)
	return nil
}

func (h *PrivateRoomService) UserChangeChair(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = getRoomManager().changeChair(req.RoomNo, req.UserId, req.ChairId)
	return nil
}

func (h *PrivateRoomService) UserLeave(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = getRoomManager().userLeave(req.RoomNo, req.UserId)
	return nil
}

func (h *PrivateRoomService) UpdateUserGameScore(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getRoomManager().updateUserScore(req.RoomNo, req.UserId, req.Score)
	return nil
}

func (h *PrivateRoomService) SetWinners(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getRoomManager().setWinners(req.RoomNo, req.Winners)
	return nil
}

func (h *PrivateRoomService) GetHistory(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = getHistoryManager().getHistory(req.UserId)
	return nil
}

func (h *PrivateRoomService) GetPlayingRoomNo(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = getRoomManager().getPlayingRoomNo(req.UserId)
	return nil
}

func (h *PrivateRoomService) IsGameRuleValid(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = getServerManager().isGameRuleValid(req.GameId, req.RuleName, req.Target, req.TableUserCount, req.PlayTimeout)
	return nil
}

func (h *PrivateRoomService) ForceUserEnter(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getRoomManager().forceUserEnter(req.UserId, req.RoomNo, req.ChairId, req.SecAfter)
	return nil
}

func (h *PrivateRoomService) GetPrivateRoomsByGameId(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = getRoomManager().getRoomsByGameId(req.GameId, req.UserId)
	return nil
}

/*func (h *PrivateRoomService) RegisterRoomStatus(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getRoomManager().registerRoomStatus(req.Addr, req.RoomNo)
	return nil
}*/

func (h *PrivateRoomService) RegisterServerStatus(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getServerManager().registerServerStatus(req.Addr)
	getRoomManager().registerRoomStatus(req.Addr)
	return nil
}

func (h *PrivateRoomService) UserSubscribeRoomStatus(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getSubsManager().userSubscribeRoomStatus(req.UserId)
	return nil
}

func (h *PrivateRoomService) UserDesubscribeRoomStatus(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getSubsManager().userDesubscribeRoomStatus(req.UserId)
	return nil
}

func (h *PrivateRoomService) GetCallList(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	d, _ := json.Marshal(getCallManager().getRoomCallList())
	rsp.Data = string(d)
	return nil
}

func (h *PrivateRoomService) PrivateRoomCall(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	getCallManager().addRoomCall(req.UserId, req.RoomNo)
	return nil
}

func (h *PrivateRoomService) GetUserRoomInfo(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.User = getRoomManager().getUserRoomInfo(req.UserId, req.RoomNo)
	return nil
}

func (h *PrivateRoomService) GetRoomType(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	ri := getRoomManager().getRoomInfo(req.RoomNo)
	if ri != nil {
		rsp.Data = ri.RoomType
	}
	return nil
}

func (h *PrivateRoomService) GetUserFeeAndPrize(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	ri := getRoomManager().getRoomInfo(req.RoomNo)
	if ri != nil {
		rsp.Fee = ri.Fee
		rsp.Prize = ri.getPrize(req.UserId)
	}
	return nil
}
