package handler

import (
	"bet24.com/log"
	"sort"
	"sync"
	"time"
)

var callmgr *callmanager

const call_timeout = 120

func getCallManager() *callmanager {
	if callmgr == nil {
		callmgr = new(callmanager)
		callmgr.run()
	}
	return callmgr
}

type callmanager struct {
	calllist map[int]int64
	lock     *sync.RWMutex
}

type roomCall struct {
	roomNo   int
	lastPing int64
}

func (cm *callmanager) run() {
	cm.calllist = make(map[int]int64)
	cm.lock = &sync.RWMutex{}
	go cm.checkTimeout()
}

func (cm *callmanager) checkTimeout() {
	time.AfterFunc(20*time.Second, cm.checkTimeout)
	now := time.Now().Unix()
	var toRemove []int
	cm.lock.RLock()
	for k, v := range cm.calllist {
		if now-v >= call_timeout {
			toRemove = append(toRemove, k)
		}
	}
	cm.lock.RUnlock()
	if len(toRemove) <= 0 {
		return
	}

	log.Debug("callmanager.checkTimeout removing rooms %v", toRemove)

	cm.lock.Lock()
	for _, v := range toRemove {
		delete(cm.calllist, v)
	}
	cm.lock.Unlock()
}

func (cm *callmanager) removeRoomCall(roomNo int) {
	cm.lock.Lock()
	delete(cm.calllist, roomNo)
	cm.lock.Unlock()
	getSubsManager().publishRoomCallRemoved(roomNo)
}

func (cm *callmanager) addRoomCall(userId, roomNo int) {
	// 判断下是否存在，用户是否存在
	r := getRoomManager().getRoomInfo(roomNo)
	if r == nil {
		log.Release("callmanager.addRoomCall room [%d] not exist", roomNo)
		return
	}
	if !r.isUserExist(userId) {
		log.Release("callmanager.addRoomCall room [%d] user[%d] not exist", roomNo, userId)
		return
	}

	cm.lock.Lock()
	cm.calllist[roomNo] = time.Now().Unix()
	cm.lock.Unlock()
	getSubsManager().publishRoomCallAdded(roomNo)
}

func (cm *callmanager) getRoomCallList() []int {
	var list []roomCall
	cm.lock.RLock()
	for k, v := range cm.calllist {
		list = append(list, roomCall{roomNo: k, lastPing: v})
	}
	cm.lock.RUnlock()
	if len(list) == 0 {
		return []int{}
	}

	sort.Slice(list, func(i, j int) bool {
		return list[i].lastPing < list[j].lastPing
	})

	ret := make([]int, len(list))
	for i := 0; i < len(list); i++ {
		ret[i] = list[i].roomNo
	}
	return ret
}
