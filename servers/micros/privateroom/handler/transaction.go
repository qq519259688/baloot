package handler

import (
	"bet24.com/database"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"fmt"
	"time"
)

// 月卡领取
func writeRoomRecordToDB(ri *roomInfo) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_PrivateRoom_WriteHistory")
	statement.AddParamter("@RoomNo", database.AdParamInput, database.AdVarChar, 32, fmt.Sprintf("%d", ri.RoomNo))
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, ri.GameId)
	statement.AddParamter("@OwnerUserID", database.AdParamInput, database.AdInteger, 4, ri.Owner)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdInteger, 4, ri.gameStartTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdInteger, 4, time.Now().Unix())
	statement.AddParamter("@TotalAwardAmount", database.AdParamInput, database.AdBigint, 8, ri.Prize)
	statement.AddParamter("@TotalFee", database.AdParamInput, database.AdBigint, 8, ri.Fee*ri.UserCount)
	statement.AddParamter("@TotalTax", database.AdParamInput, database.AdBigint, 8, ri.getTotalTax())
	statement.AddParamter("@UserInfo", database.AdParamInput, database.AdVarChar, 1024, ri.getUsersDescForDB())
	statement.AddParamter("@ExtInfo", database.AdParamInput, database.AdVarChar, 1024, ri.getUsersDesc())
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdInteger, 4, ri.createTime)
	sqlstring := statement.GenSql()
	dbengine.Execute(sqlstring)
}
