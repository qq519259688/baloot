package handler

import (
	"bet24.com/log"
	"sync"
	"time"
)

type privateroomReceiver struct {
	name   string
	active bool
}

type receiverMgr struct {
	receivers    []privateroomReceiver
	lockReceiver *sync.RWMutex
}

func newReceiverMgr() *receiverMgr {
	ret := new(receiverMgr)
	ret.ctor()
	return ret
}

func (rm *receiverMgr) ctor() {
	rm.lockReceiver = &sync.RWMutex{}
	go rm.checkRemoveReceivers()
}

func (rm *receiverMgr) checkRemoveReceivers() {
	time.AfterFunc(5*time.Minute, rm.checkRemoveReceivers)
	rm.removeInactiveReceivers()
}

func (rm *receiverMgr) dump() {
	rm.lockReceiver.RLock()
	for _, v := range rm.receivers {
		log.Release("    %s[%v]", v.name, v.active)
	}
	rm.lockReceiver.RUnlock()
}

func (rm *receiverMgr) setReceiver(addr string, active bool) {
	rm.lockReceiver.Lock()
	defer rm.lockReceiver.Unlock()
	for k, v := range rm.receivers {
		if v.name == addr {
			rm.receivers[k].active = active
			return
		}
	}
	if active {
		rm.receivers = append(rm.receivers, privateroomReceiver{name: addr, active: true})
	}
}

func (rm *receiverMgr) getActiveReceivers() []string {
	var ret []string
	rm.lockReceiver.RLock()
	for _, v := range rm.receivers {
		if v.active {
			ret = append(ret, v.name)
		}
	}
	rm.lockReceiver.RUnlock()
	return ret
}

func (rm *receiverMgr) removeInactiveReceivers() {
	rm.lockReceiver.Lock()
	defer rm.lockReceiver.Unlock()
	for i := 0; i < len(rm.receivers); {
		if !rm.receivers[i].active {
			log.Release("receiverMgr.removeInactiveReceivers %s", rm.receivers[i].name)
			rm.receivers = append(rm.receivers[:i], rm.receivers[i+1:]...)
			continue
		}
		i++
	}
}
