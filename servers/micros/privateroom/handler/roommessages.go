package handler

import (
	pb "bet24.com/servers/micros/privateroom/proto"
)

const (
	room_info           = "room_info"
	room_created        = "room_created"
	room_closed         = "room_closed"
	room_call_added     = "room_call_added"   // 新增一个召集
	room_call_removed   = "room_call_removed" // 删除一个召集
	room_blasklist_user = "room_blasklist_user"
)

type Match_notificationInfo struct {
	Msg      string       `json:",omitempty"`
	RoomNo   int          `json:",omitempty"`
	RoomInfo *pb.RoomInfo `json:",omitempty"`
	UserId   int          `json:",omitempty"`
}
