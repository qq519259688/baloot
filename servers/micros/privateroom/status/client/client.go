package client

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
	_ "encoding/json"
	rpcx_client "github.com/smallnest/rpcx/client"
	_ "github.com/smallnest/rpcx/protocol"
)

var consulAddr = common.Default_Consul_Addr

func getClient(serviceName string) rpcx_client.XClient {
	return common.GetClientPool().GetClient(serviceName, consulAddr)
}

type Reply struct {
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

type ServerStatus struct {
	GameId          int
	GameRule        string
	Desc            string
	TargetOptions   []int
	UserOptions     []int
	PlayTimeOptions []int
}

const (
	Roomstatus_start         = "roomstatus_start"
	Roomstatus_end           = "roomstatus_end"
	Roomstatus_scorechanged  = "roomstatus_scorechanged"
	Roomstatus_statuschanged = "roomstatus_statuschanged"
)

type RoomStatus struct {
	Method    string
	RoomNo    int
	UserId    int   `json:",omitempty"`
	Score     int   `json:",omitempty"`
	OldStatus int   `json:",omitempty"`
	NewStatus int   `json:",omitempty"`
	Winners   []int `json:",omitempty"`
}

func OnGameRuleRegistered(addr string, gameId int, gameRule, desc string, targetOptions, userOptions, playTimeOpions []int) bool {
	//log.Debug("OnGameRuleRegistered %s", addr)
	xclient := getClient(addr)
	err := xclient.Call(context.Background(), "OnGameRuleRegistered", &ServerStatus{
		GameId:          gameId,
		GameRule:        gameRule,
		Desc:            desc,
		TargetOptions:   targetOptions,
		UserOptions:     userOptions,
		PlayTimeOptions: playTimeOpions,
	}, nil)
	if err != nil {
		log.Debug("OnGameRuleRegistered failed to call: %v", err)
		common.GetClientPool().RemoveClient(addr)
		return false
	}
	return true
}

func OnGameRuleDeregistered(addr string, gameId int, gameRule string) bool {
	//log.Debug("OnGameRuleRegistered %s", addr)
	xclient := getClient(addr)
	err := xclient.Call(context.Background(), "OnGameRuleDeregistered", &ServerStatus{
		GameId:   gameId,
		GameRule: gameRule,
	}, nil)
	if err != nil {
		log.Debug("OnGameRuleDeregistered failed to call: %v", err)
		common.GetClientPool().RemoveClient(addr)
		return false
	}
	return true
}

func OnRoomStatusInfo(addr string, method string, roomNo int, userId int, score int, oldStatus, newStatus int, winners []int) bool {
	xclient := getClient(addr)
	err := xclient.Call(context.Background(), "OnRoomStatusInfo", &RoomStatus{
		Method:    method,
		RoomNo:    roomNo,
		UserId:    userId,
		Score:     score,
		OldStatus: oldStatus,
		NewStatus: newStatus,
		Winners:   winners,
	}, nil)
	if err != nil {
		log.Debug("OnRoomStatus failed to call: %v", err)
		common.GetClientPool().RemoveClient(addr)
		return false
	}
	return true
}
