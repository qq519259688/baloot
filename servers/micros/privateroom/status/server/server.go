package server

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"bet24.com/servers/micros/privateroom/status/client"
	"context"
	"github.com/smallnest/rpcx/server"
)

func Run(addr, consulAddr string,
	iServerReceiver client.IPrivateServerStatusReceiver,
	iStatusReceiver client.IPrivateRoomStatusReceiver) {
	s := newHandler(iServerReceiver, iStatusReceiver, addr)
	rpcserver := common.RunWithoutChangeTitle(addr, s, consulAddr)
	s.rpcserver = rpcserver
}

func newHandler(iServerReceiver client.IPrivateServerStatusReceiver,
	iStatusReceiver client.IPrivateRoomStatusReceiver, addr string) *SubscribeServer {
	ret := new(SubscribeServer)
	ret.iServerReceiver = iServerReceiver
	ret.iStatusReceiver = iStatusReceiver
	ret._addr = addr
	return ret
}

type SubscribeServer struct {
	_addr           string
	rpcserver       *server.Server
	iServerReceiver client.IPrivateServerStatusReceiver
	iStatusReceiver client.IPrivateRoomStatusReceiver
}

func (s *SubscribeServer) OnGameRuleRegistered(ctx context.Context, ss *client.ServerStatus, reply *client.Reply) error {
	if s.iServerReceiver == nil {
		return nil
	}
	s.iServerReceiver.OnGameRuleRegistered(ss.GameId, ss.GameRule, ss.Desc, ss.TargetOptions, ss.UserOptions, ss.PlayTimeOptions)
	return nil
}

func (s *SubscribeServer) OnGameRuleDeregistered(ctx context.Context, ss *client.ServerStatus, reply *client.Reply) error {
	if s.iServerReceiver == nil {
		return nil
	}
	s.iServerReceiver.OnGameRuleDeregistered(ss.GameId, ss.GameRule)
	return nil
}

func (s *SubscribeServer) OnRoomStatusInfo(ctx context.Context, rs *client.RoomStatus, reply *client.Reply) error {
	receiver := s.iStatusReceiver
	if receiver == nil {
		return nil
	}
	switch rs.Method {
	case client.Roomstatus_start:
		receiver.OnRoomStart(rs.RoomNo)
	case client.Roomstatus_end:
		receiver.OnRoomEnd(rs.RoomNo, rs.Winners)
	case client.Roomstatus_scorechanged:
		receiver.OnRoomUserScoreChanged(rs.RoomNo, rs.UserId, rs.Score)
	case client.Roomstatus_statuschanged:
		receiver.OnRoomStatusChanged(rs.RoomNo, rs.OldStatus, rs.NewStatus)
	default:
		log.Release("OnRoomStatusInfo method:%s not found", rs.Method)
	}
	return nil
}

/*func (s *SubscribeServer) CloseStatusServer(ctx context.Context, ss *client.ServerStatus, reply *client.Reply) error {
	if s.rpcserver != nil {
		log.Release("CloseStatusServer %s", s._addr)
		s.rpcserver.Shutdown(ctx)
	}
	return nil
}*/
