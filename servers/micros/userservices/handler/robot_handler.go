package handler

import (
	"bet24.com/servers/micros/userservices/handler/robot"
	pb "bet24.com/servers/micros/userservices/proto"
	"context"
)

func (h *Userservices) IsRobot(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = robot.IsRobot(req.UserId)
	return nil
}

func (h *Userservices) GetARobot(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.RetCode = robot.GetARobot()
	return nil
}
