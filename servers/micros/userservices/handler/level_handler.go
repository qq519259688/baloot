package handler

import (
	"bet24.com/servers/micros/userservices/handler/level"
	pb "bet24.com/servers/micros/userservices/proto"
	"context"
)

func (h *Userservices) GetLevelList(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = level.GetLevelList()
	return nil
}

func (h *Userservices) GetUserLevel(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.LevelBase = level.GetUserLevel(req.UserId)
	return nil
}

func (h *Userservices) IsShowGameHall(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = level.IsShowGameHall(req.UserId)
	return nil
}

func (h *Userservices) AddGameExp(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	level.AddGameExp(req.UserId, req.GameId, req.Score)
	return nil
}

func (h *Userservices) LoginAward(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success, rsp.RetCode = level.LoginAward(req.UserId)
	return nil
}

func (h *Userservices) AddExperience(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.RetCode = level.AddExperience(req.UserId, req.Exp)
	return nil
}
