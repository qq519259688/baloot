package handler

import (
	"bet24.com/log"
	"bet24.com/servers/micros/userservices/handler/level"
	"bet24.com/servers/micros/userservices/handler/mail"
	"bet24.com/servers/micros/userservices/handler/robot"
	"bet24.com/servers/micros/userservices/handler/userinfo"
	"bet24.com/servers/micros/userservices/handler/vip"
	pb "bet24.com/servers/micros/userservices/proto"
	"context"
	"fmt"
)

var instance *Userservices

func GetInstance() *Userservices {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *Userservices {
	ret := new(Userservices)
	ret.ctor()
	return ret
}

type Userservices struct{}

func (h *Userservices) ctor() {
	level.Run()
	robot.Run()
	userinfo.Run()
	mail.Run()
	vip.Run()
}

func (d *Userservices) dump(cmd, param1, param2 string) {
	//log.Debug("DbEngine.Dump %s,%s,%s", cmd, param1, param2)
	switch cmd {
	case "userinfo":
		userinfo.Dump(param1, param2)
	case "level":
		level.Dump(param1, param2)
	case "robot":
		robot.Dump(param1, param2)
	case "mail":
		mail.Dump(param1, param2)
	case "vip":
		vip.Dump(param1, param2)

	default:
		log.Release("Userservices.Dump unhandled cmd %s", cmd)
	}
}

func (h *Userservices) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (d *Userservices) AddUser(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	level.AddUser(req.UserId)
	vip.AddUser(req.UserId)
	return nil
}

func (d *Userservices) RemoveUser(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	level.RemoveUser(req.UserId)
	vip.RemoveUser(req.UserId)
	return nil
}
