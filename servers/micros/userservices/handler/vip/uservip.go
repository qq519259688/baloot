package vip

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	notification "bet24.com/servers/micros/notification/proto"

	//"bet24.com/servers/micros/userservices/handler/userinfo"
	"encoding/json"
	"time"

	pb "bet24.com/servers/micros/userservices/proto"
)

type userVip struct {
	userId            int                  // 用户ID
	pb.UserVip                             // 用户vip 信息
	purchaseHistories []pb.PurchaseHistory // 购买历史记录
}

// 为防止获取vip数据的时候进行领取，改在onUserEter触发领取动作
func newUserVip(userId int) *userVip {
	user := new(userVip)
	user.userId = userId
	user.loadDataFromDB()
	return user
}
func (uv *userVip) onUserEntered() {
	//uv.checkDailyAward()
	uv.checkVipExpired()
}

// 加载用户vip 信息
func (uv *userVip) loadDataFromDB() {
	uv.UserVip = trans_getVipInfo(uv.userId)
	uv.purchaseHistories = trans_getPackagePurchaseHistory(uv.userId)
}

// 检查每日礼包是否能领取
func (uv *userVip) checkDailyAward() (bool, *pb.VipInfo, int) {
	if !uv.IsVip() {
		return false, nil, pb.VipAward_Nonmember
	}
	if common.GetDayIndex(uv.DailyPackageClaimDay) >= common.GetDayIndex(common.GetTimeStamp()) {
		return false, nil, pb.VipAward_ReceivedAward
	}

	// 获取当前等级的每日礼包
	sysVip := mgr.getVipByLevel(uv.Level)
	if sysVip == nil {
		return false, nil, pb.VipAward_ConfigNotExist
	}

	if len(sysVip.DailyPackage) == 0 {
		return false, nil, pb.VipAward_NoReward
	}
	return true, sysVip, pb.VipAward_Success
}

// 领取每日奖励
func (uv *userVip) giftDailyAward() bool {
	b, sysVip, _ := uv.checkDailyAward()
	if !b {
		return false
	}
	// 更新领取时间
	uv.DailyPackageClaimDay = common.GetTimeStamp()
	// 写入数据库
	go uv.updateUserVip()
	inventory.AddItems(uv.userId, sysVip.DailyPackage, "vip daily award", common.LOGTYPE_LOGIN_AWARD)
	d, _ := json.Marshal(sysVip.DailyPackage)
	log.Debug("giftDailyAward send award UserId[%d] Items%v", uv.userId, sysVip.DailyPackage)
	notification.AddNotification(uv.userId, notification.Notification_VipGiftPack, string(d))
	return true
}

// 检查vip是否过期
func (uv *userVip) checkVipExpired() {
	// 本来就不是vip
	if uv.Expire == 0 {
		return
	}

	// expire有数值，表示上次登录还是vip
	if !uv.IsVip() {
		uv.onVipExpired()
		return
	}

	time.AfterFunc(time.Duration(uv.Expire-common.GetTimeStamp())*time.Second, uv.onVipExpired)
}

// vip过期
func (uv *userVip) onVipExpired() {
	// 如果定时器期间重新充值了vip，则再启定时器
	if uv.Expire-int(time.Now().Unix()) > 5 {
		time.AfterFunc(time.Duration(uv.Expire-int(time.Now().Unix()))*time.Second, uv.onVipExpired)
		return
	}
	uv.Expire = 0
	go uv.updateUserVip()
	log.Debug("onVipExpired %d", uv.userId)
	notification.AddNotification(uv.userId, notification.Notification_VipExpire, "")
}

// 会员时间
func (uv *userVip) dumpVipTime() {
	expire := "not vip"
	if uv.Expire > 0 {
		expire = common.TimeStampToString(int64(uv.Expire))
	}
	claim := common.TimeStampToString(int64(uv.DailyPackageClaimDay))
	log.Debug("userVip.dumpVipTime %d:[lv:%d (%d)] Expire[%s] Claim[%s]", uv.userId, uv.Level, uv.Point, expire, claim)
}

// 判断是否能购买礼包
func (uv *userVip) canBuyPackage(productId string) bool {
	if !uv.IsVip() {
		return false
	}

	purchasePackage := mgr.getPurchasePackageInfo(productId)
	// 礼包不存在
	if purchasePackage == nil {
		log.Debug("userVip.canBuyPackage [%s] not found", productId)
		return false
	}

	if purchasePackage.PurchaseLimitType == pb.PurchaseLimit_none {
		return true
	}

	var purchaseCount int
	if purchasePackage.PurchaseLimitType == pb.PurchaseLimit_daily {
		nowDayIndex := common.GetDayIndex(common.GetTimeStamp())
		for _, v := range uv.purchaseHistories {
			if v.ProductId != productId {
				continue
			}
			if nowDayIndex == common.GetDayIndex(int(v.PurchaseTime)) {
				purchaseCount++
			}
		}
	} else if purchasePackage.PurchaseLimitType == pb.PurchaseLimit_weekly {
		nowWeekIndex := common.GetWeekIndex(common.GetTimeStamp())
		for _, v := range uv.purchaseHistories {
			if v.ProductId != productId {
				continue
			}
			if nowWeekIndex == common.GetWeekIndex(int(v.PurchaseTime)) {
				purchaseCount++
			}
		}
	} else if purchasePackage.PurchaseLimitType == pb.PurchaseLimit_monthly {
		nowMonthIndex := common.GetMonthIndex(common.GetTimeStamp())
		for _, v := range uv.purchaseHistories {
			if v.ProductId != productId {
				continue
			}
			if nowMonthIndex == common.GetMonthIndex(int(v.PurchaseTime)) {
				purchaseCount++
			}
		}
	}
	return purchaseCount < purchasePackage.PurchaseLimitCount
}

// 实际购买礼包（已完成支付），发放道具，添加购买历史记录
func (uv *userVip) buyPackage(productId string) {
	purchasePackage := mgr.getPurchasePackageInfo(productId)
	if purchasePackage == nil {
		log.Release("userVip.buyPackage [%s] not found", productId)
		return
	}
	// 发放道具
	inventory.AddItems(uv.userId, purchasePackage.Items, "vip package", common.LOGTYPE_PACK_VIP)
	d, _ := json.Marshal(purchasePackage.Items)
	notification.AddNotification(uv.userId, notification.Notification_VipGiftBagPurchase, string(d))
	// 添加购买记录历史
	trans_addPackagePurchaseHistory(uv.userId, productId)
	uv.purchaseHistories = append(uv.purchaseHistories, pb.PurchaseHistory{
		ProductId:    productId,
		PurchaseTime: common.GetNowTime().Unix(),
	})
}

// 添加vip点数，可能触发升级,升级如果当前是vip，则需要发放登录道具和升级道具
func (uv *userVip) addVipPoint(point int) {
	if point <= 0 {
		log.Release("uservip.addVipPoint user [%d] point[%d]", uv.userId, point)
		return
	}
	//log.Debug("userVip.addVipPoint %d", point)
	uv.Point += point
	//log.Debug("userVip.addVipPoint after %d", uv.Point)
	defer uv.sendNotification(notification.Notification_VipAttributeChange)
	defer func() {
		go uv.updateUserVip()
	}()

	// 检查是否升级
	newLevel := mgr.getVipByPoint(uv.Point)
	// 级别没有变化
	if newLevel <= uv.Level {
		return
	}
	uv.Level = newLevel

	// 不是vip
	if !uv.IsVip() {
		return
	}

	// 升级并发放道具
	uv.sendUpgradeItems(true)
}

// 升级，发放道具
func (uv *userVip) sendUpgradeItems(isNotice bool) {
	// 处理跨等级
	var giftPack []item.ItemPack
	for i := 0; i <= uv.Level; i++ {
		vipInfo := mgr.getVipByLevel(i)
		if vipInfo == nil {
			continue
		}
		giftPack = append(giftPack, vipInfo.UpgragePackage...)
	}
	if len(giftPack) == 0 {
		return
	}

	// 合并同类道具
	giftPack = item.GroupItems(giftPack)
	// 把所有的道具设置成一个
	for i := 0; i < len(giftPack); i++ {
		giftPack[i].Count = 1
	}

	inventory.AddItemsWithExpireTime(uv.userId, giftPack, "vip upgrade", common.LOGTYPE_PACK_TIMELIMITED, uv.Expire)

	if isNotice {
		d, _ := json.Marshal(giftPack)
		notification.AddNotification(uv.userId, notification.Notification_VipGiftBagUpgrade, string(d))
	}
}

// 添加vip时长(已完成支付)
func (uv *userVip) addVipSeconds(seconds int) {
	if seconds <= 0 {
		log.Release("uservip.addSeconds user [%d] seconds[%d]", uv.userId, seconds)
		return
	}
	var isNotice bool
	if uv.Expire == 0 {
		// 重新恢复vip状态
		uv.Expire = common.GetTimeStamp() + seconds
		isNotice = true
		// 启动定时器
		time.AfterFunc(time.Duration(uv.Expire-int(time.Now().Unix()))*time.Second, uv.onVipExpired)
	} else {
		// 原本就是vip，不需要处理道具发放
		uv.Expire += seconds
	}
	// 发放升级礼包的时效道具
	uv.sendUpgradeItems(isNotice)

	uv.updateUserVip()

	uv.sendNotification(notification.Notification_VipAddDuration)
}

// 修改用户的vip信息
func (uv *userVip) updateUserVip() {
	trans_updateUserVip(uv.userId, uv.Level, uv.Point, uv.Expire, uv.DailyPackageClaimDay)
	pb.UpdateUserInfo(uv.userId)
}

// vip属性变化
func (uv *userVip) sendNotification(noticeCode int) {
	d, _ := json.Marshal(uv.UserVip)
	notification.AddNotification(uv.userId, noticeCode, string(d))
}
