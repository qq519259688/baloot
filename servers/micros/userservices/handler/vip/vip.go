package vip

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/userservices/proto"
)

func Run() {
	mgr = newVipManager()
}

func Dump(cmd, param1 string) {
	switch cmd {
	case "user":
		mgr.dumpGetUserVip(param1)
	case "list":
		mgr.dumpGetVipList()
	case "level":
		mgr.dumpGetVipByLevel(param1)
	case "packageInfo":
		mgr.dumpGetPurchasePackageInfo(param1)
	case "purchaseList":
		mgr.dumpGetPurchasePackageList(param1)
	default:
		log.Release("vipservice.Dump unhandled cmd %s", cmd)
	}
}

func GetUserVip(userId int) *pb.UserVip {
	ret := mgr.getUserVip(userId, true)
	if ret == nil {
		return nil
	}
	return &ret.UserVip
}

func AddUser(userId int) {
	mgr.onUserEnter(userId)
}

func RemoveUser(userId int) {
	mgr.onUserExit(userId)
}

func GetVipList() []pb.VipInfo {
	return mgr.getVipList()
}

func GetPurchasePackageList(userId int) []pb.PurchasePackage {
	return mgr.getPurchasePackageList(userId)
}

func GetVipByLevel(level int) *pb.VipInfo {
	return mgr.getVipByLevel(level)
}

func GetExtraBankruptcy(userId int) int {
	return mgr.getPrivilegeValue(userId, pb.VipPrivilege_ExtraBankruptcy)
}

func GetExtraExperience(userId int) int {
	return mgr.getPrivilegeValue(userId, pb.VipPrivilege_ExtraExperience)
}

func GetExtraFriendCount(userId int) int {
	return mgr.getPrivilegeValue(userId, pb.VipPrivilege_ExtraFriend)
}

func CanBuyPackage(userId int, productId string) bool {
	return mgr.canBuyPackage(userId, productId)
}

func BuyPackage(userId int, productId string) {
	mgr.buyPackage(userId, productId)
}

func AddVipPoint(userId int, point int) {
	mgr.addVipPoint(userId, point)
}

func AddVipSeconds(userId int, purchaseSeconds int) {
	mgr.addVipSeconds(userId, purchaseSeconds)
}

func CheckDailyAward(userId int) (bool, int) {
	return mgr.checkDailyAward(userId)
}

func GiftDailyAward(userId int) bool {
	return mgr.giftDailyAward(userId)
}

func GetUserPrivilegeValue(userId int, privilegeId int) int {
	return mgr.getPrivilegeValue(userId, privilegeId)
}

func CanKickPrivateRoomUser(userId int, toUserId int) bool {
	return mgr.canKickPrivateRoomUser(userId, toUserId)
}
