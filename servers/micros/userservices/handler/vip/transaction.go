package vip

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
)

// 新版用户vip 获取用户vip信息
func trans_getVipInfo(userId int) pb.UserVip {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_NewUserVip_GetInfo")
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)

	var out []pb.UserVip
	if err := json.Unmarshal([]byte(jsonData), &out); err != nil {
		log.Error("transaction.trans_getVipInfo json unmarshal UserID=%d err %v", userId, err)
	}

	if len(out) <= 0 {
		return pb.UserVip{}
	}

	return out[0]
}

// 新版用户vip 修改用户vip信息
func trans_updateUserVip(userId int, level int, point int, expire int, dailyPackageClaimDay int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_NewUserVip_Update")
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Level", database.AdParamInput, database.AdSmallInt, 4, level)
	statement.AddParamter("@Point", database.AdParamInput, database.AdInteger, 8, point)
	statement.AddParamter("@Expire", database.AdParamInput, database.AdInteger, 16, expire)
	statement.AddParamter("@DailyPackageClaimDay", database.AdParamInput, database.AdInteger, 8, dailyPackageClaimDay)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}

// 新版用户vip 获取购买礼包记录
func trans_getPackagePurchaseHistory(userId int) []pb.PurchaseHistory {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_NewUserVip_GetPurchaseList")
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)

	var out []pb.PurchaseHistory
	if err := json.Unmarshal([]byte(jsonData), &out); err != nil {
		log.Error("transaction.trans_getPackagePurchaseHistory json unmarshal UserID=%d err %v", userId, err)
	}
	return out
}

// 新版用户vip 添加购买礼包记录
func trans_addPackagePurchaseHistory(userId int, productId string) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_NewUserVip_AddPurchaseLog")
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ProductId", database.AdParamInput, database.AdVarChar, 32, productId)
	statement.AddParamter("@PurchaseTime", database.AdParamInput, database.AdInteger, 16, common.GetTimeStamp())
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}
