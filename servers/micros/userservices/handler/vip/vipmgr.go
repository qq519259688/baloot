package vip

import (
	"encoding/json"
	"strconv"
	"sync"

	"bet24.com/log"
	pb "bet24.com/servers/micros/userservices/proto"
)

var mgr *vipmgr

type vipmgr struct {
	lock             *sync.RWMutex
	userList         map[int]*userVip     // 个人vip信息
	VipList          []pb.VipInfo         // 系统vip配置
	PurchasePackages []pb.PurchasePackage // 礼包
}

func newVipManager() *vipmgr {
	mgr = new(vipmgr)
	mgr.lock = &sync.RWMutex{}
	mgr.userList = make(map[int]*userVip)

	mgr.loadConfig()
	return mgr
}

// 根据用户ID查询用户vip状态
func (this *vipmgr) getUserVip(userId int, forceCreate bool) *userVip {
	this.lock.RLock()
	u, ok := this.userList[userId]
	this.lock.RUnlock()
	if ok {
		return u
	}

	if !forceCreate {
		//log.Debug("vipmgr.getUserVip user not found, userId[%d]", userId)
		return nil
	}

	u = newUserVip(userId)

	this.lock.Lock()
	this.userList[userId] = u
	this.lock.Unlock()
	return u
}

// 用户进入，创建用户信息，读取用户礼包购买历史，如果是vip，判断是否领取VIP奖励
func (this *vipmgr) onUserEnter(userId int) {
	uv := this.getUserVip(userId, true)
	uv.onUserEntered()
}

// 将用户标记为离线
func (this *vipmgr) onUserExit(userId int) {
	this.lock.Lock()
	defer this.lock.Unlock()
	delete(this.userList, userId)
}

// 获取vip列表，前端展现用
func (this *vipmgr) getVipList() []pb.VipInfo {
	return this.VipList
}

// 获取购买礼包列表
func (this *vipmgr) getPurchasePackageList(userId int) []pb.PurchasePackage {
	if len(this.PurchasePackages) == 0 {
		return nil
	}
	ret := make([]pb.PurchasePackage, len(this.PurchasePackages))
	copy(ret, this.PurchasePackages)
	for i := 0; i < len(ret); i++ {
		ret[i].BuyAble = this.canBuyPackage(userId, ret[i].ProductId)
		//ret[i].Price = ret[i].Price
	}
	return ret
}

// 根据等级获取vip配置信息
func (this *vipmgr) getVipByLevel(level int) *pb.VipInfo {
	for k, v := range this.VipList {
		if v.Level == level {
			return &this.VipList[k]
		}
	}
	return nil
}

// 获取vip点数对应的等级
func (this *vipmgr) getVipByPoint(point int) int {
	ret := 0
	for _, v := range this.VipList {
		if point < v.Point {
			continue
		}
		if v.Level <= ret {
			continue
		}
		ret = v.Level
	}
	return ret
}

// 获取礼包信息
func (this *vipmgr) getPurchasePackageInfo(productId string) *pb.PurchasePackage {
	for k, v := range this.PurchasePackages {
		if v.ProductId == productId {
			return &this.PurchasePackages[k]
		}
	}
	return nil
}

// 获取等级特权的数值
func (this *vipmgr) getPrivilegeValue(userId int, privilegeType int) int {
	user := this.getUserVip(userId, false)
	if user == nil {
		//log.Debug("vipmgr.getPrivilegeValue userId=%d not exist", userId)
		return pb.VipPrivilege_Invalid
	}
	if !user.IsVip() {
		return pb.VipPrivilege_Invalid
	}

	vipInfo := mgr.getVipByLevel(user.Level)
	if vipInfo == nil {
		//log.Debug("vipmgr.getPrivilegeValue vip config not found, level=%d", user.Level)
		return pb.VipPrivilege_Invalid
	}

	return vipInfo.GetPrivilegeParam(privilegeType)
}

// 判断是否能购买礼包
func (this *vipmgr) canBuyPackage(userId int, productId string) bool {
	user := this.getUserVip(userId, false)
	if user == nil {
		log.Debug("vipmgr.canBuyPackage userId=%d not exist", userId)
		return false
	}
	return user.canBuyPackage(productId)
}

// 实际购买礼包（已完成支付），发放道具，添加购买历史记录
func (this *vipmgr) buyPackage(userId int, productId string) {
	user := this.getUserVip(userId, true)
	// 用户不存在时，不需要直接拦截
	user.buyPackage(productId)
}

// 添加vip点数，可能触发升级,升级如果当前是vip，则需要发放登录道具和升级道具
func (this *vipmgr) addVipPoint(userId int, point int) {
	user := this.getUserVip(userId, true)
	// 用户不存在时，不需要直接拦截
	user.addVipPoint(point)
}

// 添加vip时长(已完成支付)
func (this *vipmgr) addVipSeconds(userId int, purchaseSeconds int) {
	user := this.getUserVip(userId, true)
	// 用户不存在时，不需要直接拦截
	user.addVipSeconds(purchaseSeconds)
}

// 检查是否能够领取每日礼包
func (this *vipmgr) checkDailyAward(userId int) (bool, int) {
	user := this.getUserVip(userId, true)
	if user == nil {
		log.Debug("vipmgr.checkDailyAward userId=%d not exist", userId)
		return false, -1
	}
	success, _, code := user.checkDailyAward()
	return success, code
}

// 领取每日奖励
func (this *vipmgr) giftDailyAward(userId int) bool {
	user := this.getUserVip(userId, true)
	if user == nil {
		log.Debug("vipmgr.giftDailyAward userId=%d not exist", userId)
		return false
	}
	return user.giftDailyAward()
}

func (this *vipmgr) dumpGetUserVip(param string) {
	log.Release("-------------------------------")
	log.Release("vipmgr.dumpGetUserVip param:%s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	var userId = 0
	var err error

	if param != "" {
		userId, err = strconv.Atoi(param)
		if err != nil {
			log.Release("vipmgr.dumpGetUserVip param error. err:%v", err)
			return
		}
	}
	this.lock.RLock()
	for _, value := range this.userList {
		if userId > 0 && userId != value.userId {
			continue
		}
		log.Release("vipmgr.dumpGetUserVip userId:%d, userVip:%+v", value.userId, value.UserVip)
		value.dumpVipTime()
	}
	this.lock.RUnlock()
}

func (this *vipmgr) dumpGetVipList() {
	log.Release("-------------------------------")
	log.Release("vipmgr.dumpGetVipList")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	list := this.getVipList()
	d, _ := json.Marshal(list)
	log.Release(string(d))
}

func (this *vipmgr) dumpGetVipByLevel(param string) {
	log.Release("-------------------------------")
	log.Release("vipmgr.dumpGetVipByLevel %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	level, err := strconv.Atoi(param)
	if err != nil {
		log.Release("vipmgr.dumpGetVipByLevel Parameter error. err:%v", err)
		return
	}
	levelInfo := this.getVipByLevel(level)
	d, _ := json.Marshal(levelInfo)
	log.Release(string(d))
}

func (this *vipmgr) dumpGetPurchasePackageInfo(param string) {
	log.Release("-------------------------------")
	log.Release("vipmgr.dumpGetPurchasePackageInfo %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	info := this.getPurchasePackageInfo(param)
	d, _ := json.Marshal(info)
	log.Release(string(d))
}

func (this *vipmgr) dumpGetPurchasePackageList(param string) {
	log.Release("-------------------------------")
	log.Release("vipmgr.dumpGetPurchasePackageList %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	var userId = 0
	var err error
	if param != "" {
		userId, err = strconv.Atoi(param)
		if err != nil {
			log.Release("vipmgr.dumpGetPurchasePackageList Parameter error. err:%v", err)
			return
		}
	}
	list := this.getPurchasePackageList(userId)
	d, _ := json.Marshal(list)
	log.Release(string(d))
}

func (this *vipmgr) canKickPrivateRoomUser(userId int, toUserId int) bool {
	u1 := this.getUserVip(userId, true)
	if u1 == nil || !u1.IsVip() {
		log.Debug("canKickPrivateRoomUser [%d] not vip", userId)
		return false
	}
	if this.getPrivilegeValueByLevel(u1.Level, pb.VipPrivilege_PrivateRoomKick) == 0 {
		log.Debug("canKickPrivateRoomUser no privilege Level[%d]", u1.Level)
		return false
	}
	u2 := this.getUserVip(toUserId, true)
	if u2 == nil {
		return true
	}
	if u2.IsVip() && u2.Level >= u1.Level {
		log.Debug("canKickPrivateRoomUser [%d]'s Level[%d] >= [%d]'s Level[%d]", toUserId, u2.Level, userId, u1.Level)
		return false
	}
	return true
}

func (this *vipmgr) getPrivilegeValueByLevel(level int, privilegeType int) int {
	vipInfo := mgr.getVipByLevel(level)
	if vipInfo == nil {
		//log.Debug("vipmgr.getPrivilegeValue vip config not found, level=%d", user.Level)
		return pb.VipPrivilege_Invalid
	}

	return vipInfo.GetPrivilegeParam(privilegeType)
}
