package vip

import (
	"encoding/json"
	"os"
	"time"

	"bet24.com/log"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/shop"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "newvip_config"

// 加载配置信息
func (this *vipmgr) loadConfig() {
	this.loadRedisConfig()
	this.loadProductItem()
	go time.AfterFunc(5*time.Minute, this.loadConfig)
}

// 加载redis配置
func (this *vipmgr) loadRedisConfig() {
	if data := platformconfig.GetConfig(config_key); data != "" {
		if err := json.Unmarshal([]byte(data), &this); err == nil {
			return
		}
		return
	}

	data, err := os.ReadFile("serviceconf/newvip_config.json")
	if err != nil {
		log.Release("read config failed serviceconf/newvip_config.json %v", err)
	}
	if err = json.Unmarshal(data, &this); err == nil {
		platformconfig.SetConfig(config_key, string(data))
		return
	}
	log.Release("Unmarshal config [%s] err:%v", string(data), err)
}

// 加载对应产品的道具
func (this *vipmgr) loadProductItem() {
	for key, value := range this.PurchasePackages {
		respProduct := coreClient.GetProduct(0, 0, value.ProductId)
		if respProduct.RetCode != 1 {
			continue
		}
		var item shop.Shop_Item
		if err := json.Unmarshal([]byte(respProduct.Data), &item); err != nil {
			log.Error("%s GetProduct json unmarshal err %v", "vipmgr.loadProductItem", err)
			continue
		}
		this.PurchasePackages[key].Price = item.Price
		this.PurchasePackages[key].Items = item.Extra
	}
}
