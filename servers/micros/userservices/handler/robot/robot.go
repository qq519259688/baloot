package robot

import (
	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"encoding/json"
	"math/rand"
	"sync"
)

var instance *RobotQuery

func Run() {
	if instance == nil {
		instance = newHandler()
	}
}

func Dump(param1, param2 string) {
	instance.dump(param1, param2)
}

func IsRobot(userId int) bool {
	return instance.isRobot(userId)
}

func GetARobot() int {
	return instance.getARobot()
}

func newHandler() *RobotQuery {
	ret := new(RobotQuery)
	ret.ctor()
	return ret
}

type RobotQuery struct {
	robots       []int
	lock         *sync.RWMutex
	currentIndex int
}

func (h *RobotQuery) ctor() {
	h.robots = h.getRobotList()
	h.lock = &sync.RWMutex{}
	// 打乱
	count := len(h.robots)
	if count == 0 {
		return
	}
	for i := count - 1; i > 1; i-- {
		place := rand.Intn(i)
		tmp := h.robots[place]
		h.robots[place] = h.robots[i]
		h.robots[i] = tmp
	}
}

func (h *RobotQuery) dump(cmd, param1 string) {
	switch cmd {
	case "count":
		log.Release("robot count = %d", len(h.robots))
	case "refresh":
		h.lock.Lock()
		h.robots = h.getRobotList()
		h.lock.Unlock()
		log.Release("robots refreshed")
	default:
		log.Release("RobotQuery.Dump unhandled cmd %s", cmd)
	}
}

func (h *RobotQuery) isRobot(userId int) bool {
	h.lock.RLock()
	defer h.lock.RUnlock()
	for _, v := range h.robots {
		if v == userId {
			return true
		}
	}
	return false
}

func (h *RobotQuery) getARobot() int {
	h.lock.RLock()
	defer h.lock.RUnlock()
	robotCount := len(h.robots)
	if robotCount == 0 {
		return 0
	}
	if h.currentIndex >= robotCount {
		h.currentIndex = 0
	}

	ret := h.robots[h.currentIndex]
	h.currentIndex++
	return ret
}

type record struct {
	UserId int
}

func (h *RobotQuery) getRobotList() []int {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_MachineUser_GetList")
	sqlString := statement.GenSql()
	retData := dbengine.Execute(sqlString)
	var records []record
	e := json.Unmarshal([]byte(retData), &records)
	if e != nil {
		log.Release("RobotQuery.getRobotList Unmarshal failed %v", e)
	}

	var ret []int
	for _, v := range records {
		ret = append(ret, v.UserId)
	}

	return ret
}
