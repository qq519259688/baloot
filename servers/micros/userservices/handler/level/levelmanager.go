package level

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"strconv"
	"sync"
)

type levelmgr struct {
	level_list []pb.LevelInfo
	game_list  []pb.GameExpInfo
	user_list  map[int]*userlevel
	maxLevel   int
	lock       *sync.RWMutex
}

func newLevelMgr() *levelmgr {
	ret := new(levelmgr)
	ret.user_list = make(map[int]*userlevel)
	ret.lock = &sync.RWMutex{}
	ret.loadLevelInfo()
	ret.loadGameExpInfo()
	return ret
}

func (lm *levelmgr) loadLevelInfo() {
	lm.lock.Lock()
	defer lm.lock.Unlock()
	lm.maxLevel, lm.level_list = getSysLevelList()
}

func (lm *levelmgr) loadGameExpInfo() {
	lm.lock.Lock()
	defer lm.lock.Unlock()
	lm.game_list = getGameExpList()
}

func (lm *levelmgr) onUserEnter(userId int) {
	lm.lock.RLock()
	_, ok := lm.user_list[userId]
	lm.lock.RUnlock()
	if ok {
		return
	}
	usr := newUserLevel(userId)
	lm.lock.Lock()
	lm.user_list[userId] = usr
	lm.lock.Unlock()
}

func (lm *levelmgr) onUserExit(userId int) {
	lm.lock.Lock()
	defer lm.lock.Unlock()
	delete(lm.user_list, userId)
}

func (lm *levelmgr) getUser(userId int) *userlevel {
	lm.lock.RLock()
	user, ok := lm.user_list[userId]
	lm.lock.RUnlock()
	if !ok {
		user = newUserLevel(userId)
		if user == nil {
			return nil
		}
		lm.lock.Lock()
		lm.user_list[userId] = user
		lm.lock.Unlock()
	}
	return user
}

func (lm *levelmgr) getUserLevel(userId int) pb.LevelBaseInfo {
	user := lm.getUser(userId)
	if user == nil {
		log.Debug("levelmgr.getUserLevel userId=%d not exist", userId)
		return user.LevelBaseInfo
	}
	return user.LevelBaseInfo
}

func (lm *levelmgr) addExperience(userId, exp int) int {
	lm.lock.Lock()
	user, ok := lm.user_list[userId]
	lm.lock.Unlock()
	if !ok {
		// 用户不存在，直接入库
		user = newUserLevel(userId)
	}
	return user.addExperience(exp)
}

func (lm *levelmgr) addGameExperience(userId, gameId, score int) {
	// log.Debug("addGameExperience userId=%d gameId=%d score=%d", userId, gameId, score)
	lm.lock.Lock()
	user, ok := lm.user_list[userId]
	lm.lock.Unlock()
	if !ok {
		// 用户不存在，直接入库
		user = newUserLevel(userId)
	}

	// 获取经验值
	exp := lm.getExps(gameId, score)
	// log.Debug("addGameExperience userId=%d gameId=%d score=%d exp=%d", userId, gameId, score, exp)
	if exp <= 0 {
		return
	}

	// 加经验
	user.addExperience(exp)
}

// 根据游戏ID获取经验值
func (lm *levelmgr) getExps(gameId, score int) int {
	for _, v := range lm.game_list {
		if v.GameID == gameId {
			if score > 0 {
				return v.WinExp
			}
			return v.LoseExp
		}
	}
	return 0
}

func (lm *levelmgr) getLevelList() string {
	lm.lock.RLock()
	d, _ := json.Marshal(lm.level_list)
	lm.lock.RUnlock()
	return string(d)
}

func (lm *levelmgr) getLevelByExperience(exp int) int {
	lm.lock.RLock()
	defer lm.lock.RUnlock()
	ret := 0
	for _, v := range lm.level_list {
		if exp < v.Exp {
			continue
		}
		if v.Level <= ret {
			continue
		}
		ret = v.Level
	}
	return ret
}

func (lm *levelmgr) getLevelInfo(level int) pb.LevelInfo {
	for _, v := range lm.level_list {
		if v.Level == level {
			return v
		}
	}
	log.Debug("levelmgr.getLevelInfo level not found [%d]", level)
	return pb.LevelInfo{}
}

// 是否显示游戏大厅
func (lm *levelmgr) isShowGameHall(userId int) bool {
	// log.Debug("isShowGameHall userId=%d", userId)
	user := lm.getUser(userId)
	if user == nil {
		log.Debug("levelmgr.isShowGameHall userId=%d not exist", userId)
		return false
	}
	return user.isShow()
}

// 判断是否开启元宝大厅
func (lm *levelmgr) isOpen(level int) bool {
	for _, v := range lm.level_list {
		if v.Level != level {
			continue
		}

		for _, k := range v.Privileges {
			if k.Privilege == pb.LevelPrivilege_ShowGameHall {
				return true
			}
		}
		return false
	}
	return false
}

func (lm *levelmgr) loginAward(userId int) (bool, int) {
	user := lm.getUser(userId)
	if user == nil {
		log.Debug("levelmgr.loginAward userId=%d not exist", userId)
		return false, 0
	}
	return user.loginAward()
}

func (lm *levelmgr) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("levelmgr.dumpSys %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	log.Release(lm.getLevelList())
}

func (lm *levelmgr) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("levelmgr.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}
	uv := lm.getUser(userId)
	if uv == nil {
		log.Release("user %d not exist", userId)
		return
	}
	uv.dump()
}
