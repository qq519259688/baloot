package level

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/userservices/proto"
)

var mgr *levelmgr

func Dump(cmd, param1 string) {
	switch cmd {
	case "sys":
		mgr.dumpSys(param1)
	case "user":
		mgr.dumpUser(param1)
	default:
		log.Release("LevelService.Dump unhandled cmd %s", cmd)
	}
}

func Run() {
	mgr = newLevelMgr()
}

func GetLevelList() string {
	return mgr.getLevelList()
}

func GetUserLevel(userId int) pb.LevelBaseInfo {
	return mgr.getUserLevel(userId)
}

func IsShowGameHall(userId int) bool {
	return mgr.isShowGameHall(userId)
}

func AddGameExp(userId, gameId, score int) {
	mgr.addGameExperience(userId, gameId, score)
}

func LoginAward(userId int) (bool, int) {
	return mgr.loginAward(userId)
}

func AddUser(userId int) {
	mgr.onUserEnter(userId)
}
func RemoveUser(userId int) {
	mgr.onUserExit(userId)
}

func AddExperience(userId, exp int) int {
	return mgr.addExperience(userId, exp)
}
