package level

import (
	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"runtime/debug"
)

// 等级系统数值
func getSysLevelList() (int, []pb.LevelInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var (
		maxLevel int
		list     []pb.LevelInfo
	)
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Level_SysList")
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return maxLevel, list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out pb.LevelInfo

		out.Level = int((ret[0]).(int64))
		out.Exp = int((ret[1]).(int64))
		out.Desc = (ret[2]).(string)
		giftPack := (ret[3]).(string)
		if giftPack != "" {
			if err := json.Unmarshal([]byte(giftPack), &out.GiftPack); err != nil {
				log.Error("level.getSysLevelList giftPack unmarshal data fail %v", err)
			}
		}

		privilege := (ret[4]).(string)
		if privilege != "" {
			if err := json.Unmarshal([]byte(privilege), &out.Privileges); err != nil {
				log.Error("level.getSysLevelList privilege unmarshal data fail %v", err)
			}
		}

		if out.Level > maxLevel {
			maxLevel = out.Level
		}

		list = append(list, out)
	}

	return maxLevel, list
}

// 游戏经验数值
func getGameExpList() []pb.GameExpInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var list []pb.GameExpInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Level_SysExpList")
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out pb.GameExpInfo

		out.GameID = int((ret[0]).(int64))
		out.WinExp = int((ret[1]).(int64))
		out.LoseExp = int((ret[2]).(int64))

		list = append(list, out)
	}
	return list
}

// 用户等级信息
func getUserLevel(userId int) pb.LevelBaseInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var info pb.LevelBaseInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Level_GetUserInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return info
	}

	info.Level = int((retRows[0][0]).(int64))
	info.Experience = int((retRows[0][1]).(int64))
	info.IsWhite = int((retRows[0][2]).(int64))
	info.LoginAward = int((retRows[0][3]).(int64))
	info.IsFacebook = int((retRows[0][4]).(int64)) == 1
	info.IsAdd = int((retRows[0][5]).(int64)) == 1
	return info
}

// 修改用户等级信息
func updateUserLevel(userId, level, experience, loginAward int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Level_UpdateUserInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Lv", database.AdParamInput, database.AdInteger, 4, level)
	statement.AddParamter("@Exp", database.AdParamInput, database.AdInteger, 4, experience)
	statement.AddParamter("@LoginAward", database.AdParamInput, database.AdInteger, 4, loginAward)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	dbengine.ExecuteRs(sqlstring)
	return
}

// 登录奖励
func loginAward(userId int) (bool, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Level_LoginAward")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return false, 0
	}

	retCode := int((retRows[0][0]).(int64))
	amount := int((retRows[0][1]).(int64))
	return retCode == 1, amount
}
