package level

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	activityservice "bet24.com/servers/micros/activityservice/proto"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	notification "bet24.com/servers/micros/notification/proto"
	"bet24.com/servers/micros/userservices/handler/vip"
	pb "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"fmt"
	"sync"
	"time"
)

type userlevel struct {
	userId           int // 用户ID
	pb.LevelBaseInfo     // 基本信息
	lock             *sync.RWMutex
}

func newUserLevel(userId int) *userlevel {
	ul := new(userlevel)
	ul.userId = userId
	ul.lock = &sync.RWMutex{}
	ul.loadUserLevelInfo()
	return ul
}

// 初始化用户Vip信息
func (ul *userlevel) loadUserLevelInfo() {
	ul.LevelBaseInfo = getUserLevel(ul.userId)
	// 如果等级不匹配，保留等级的情况下，清理经验值
	if ul.Level != mgr.getLevelByExperience(ul.Experience) {
		li := mgr.getLevelInfo(ul.Level)
		//log.Release("userlevel.loadUserLevelInfo update user[%d]'s Experience [%d]->[%d] of Level[%d]",
		//	ul.userId, ul.Experience, li.Exp, ul.Level)
		ul.Experience = li.Exp
		go ul.updateUserLevel()
	}
	if ul.LevelBaseInfo.LoginAward == pb.Award_Notify {
		go func() {
			time.Sleep(2 * time.Second)

			// 通知客户端
			notification.AddNotification(ul.userId, notification.Notification_Chip, "")
		}()
	}
}

func (ul *userlevel) addExperience(value int) int {
	// 非facebook账号
	/*if !ul.IsAdd && !ul.IsFacebook {
		return
	}*/

	if ul.Level >= mgr.maxLevel {
		return 0
	}
	// 额外经验加成
	vipAddRate := vip.GetExtraExperience(ul.userId)
	if vipAddRate > 0 {
		value = value * (100 + vipAddRate) / 100
	}
	ul.Experience += value
	newLevel := mgr.getLevelByExperience(ul.Experience)
	defer ul.updateUserLevel()
	if newLevel == ul.Level {
		ul.broadcastExperience(value)
		return value
	}

	// 处理跨级问题
	var giftPack []item.ItemPack
	for i := ul.Level + 1; i <= newLevel; i++ {
		li := mgr.getLevelInfo(i)
		giftPack = append(giftPack, li.GiftPack...)
	}

	// 等级礼包
	go activityservice.OnUserLevelChange(ul.userId, ul.Level, newLevel)
	ul.Level = newLevel
	ul.broadcastExperience(value)
	if len(giftPack) > 0 {
		ul.sendAward(giftPack, fmt.Sprintf("user level up[%d] award", newLevel), common.LOGTYPE_LEVEL_UPGRADE)
	}

	var levelInfo pb.LevelInfo

	levelInfo.Level = ul.Level
	levelInfo.Exp = ul.Experience
	//levelInfo.GiftPack = giftPack

	d, _ := json.Marshal(levelInfo)
	go notification.AddNotification(ul.userId, notification.Notification_Level, string(d))
	return value
}

func (ul *userlevel) broadcastExperience(delta int) {
	var expChange struct {
		Delta int
		Exp   int
	}
	expChange.Delta = delta
	expChange.Exp = ul.Experience
	d, _ := json.Marshal(expChange)

	go notification.AddNotification(ul.userId, notification.Notification_Experience, string(d))
}

func (ul *userlevel) sendAward(items []item.ItemPack, desc string, logType int) {
	if len(items) <= 0 {
		return
	}
	inventory.AddItems(ul.userId, items, desc, logType)
}

func (ul *userlevel) updateUserLevel() {
	go updateUserLevel(ul.userId, ul.Level, ul.Experience, ul.LoginAward)
}

// 是否显示大厅
func (ul *userlevel) isShow() bool {
	// 白名单
	if ul.IsWhite == pb.WhiteList_Black {
		return true
	}

	// 达到条件
	if ul.LoginAward >= pb.Award_Available {
		return true
	}

	return false
}

func (ul *userlevel) loginAward() (bool, int) {
	if ul.LoginAward != pb.Award_Available {
		return false, 0
	}

	ul.LoginAward = pb.Award_Received

	retCode, amount := loginAward(ul.userId)
	if !retCode {
		return retCode, 0
	}

	var items []item.ItemPack
	items = append(items, item.ItemPack{
		ItemId: item.Item_Chip,
		Count:  amount,
	})
	inventory.AddItems(ul.userId, items, "元宝大厅登录奖励", common.LOGTYPE_LOGIN_AWARD)
	return retCode, amount
}

func (ul *userlevel) dump() {
	ul.lock.RLock()
	defer ul.lock.RUnlock()
	log.Debug("    userlevel.dump %d:[%d.%d] IsWhite[%d]", ul.userId, ul.Level, ul.Experience, ul.IsWhite)
}
