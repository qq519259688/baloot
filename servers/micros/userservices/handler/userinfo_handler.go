package handler

import (
	"bet24.com/servers/micros/userservices/handler/userinfo"
	pb "bet24.com/servers/micros/userservices/proto"
	"context"
	"fmt"
)

func (h *Userservices) GetUserInfo(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.UserInfo = userinfo.GetInfo(req.UserId)
	return nil
}

func (h *Userservices) GetUserInfoInBulk(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.UserInfos = userinfo.GetUserInfoInBulk(req.UserIds)
	return nil
}

func (h *Userservices) SaveCountry(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("%d", userinfo.SaveCountry(req.UserId, req.CountryName, req.Currency))
	return nil
}

func (h *Userservices) LockCountry(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.RetCode = userinfo.LockCountry(req.UserId, req.Currency)
	return nil
}

func (h *Userservices) GetUserFaceUrl(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	u := userinfo.GetInfo(req.UserId)
	if u == nil {
		return nil
	}
	rsp.Data = u.FaceUrl
	return nil
}

func (h *Userservices) UpdateUserInfo(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	userinfo.UpdateUserInfo(req.UserId)
	return nil
}

func (h *Userservices) SetUserDecoration(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = userinfo.SetUserDecoration(req.UserId, req.DecorationType, req.ItemId)
	return nil
}

func (h *Userservices) OnDecorationExpired(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	userinfo.OnDecorationExpired(req.UserId, req.ItemId)
	return nil
}

func (h *Userservices) ChangeSwitchStatus(ctx context.Context, req *pb.Request_Switch, rsp *pb.Response) error {
	userinfo.ChangeSwitchStatus(req.UserId, req.SwitchInfo.SwitchType, req.SwitchInfo.SwitchStatus)
	return nil
}

func (h *Userservices) AddUserCharm(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	userinfo.AddUserCharm(req.UserId, req.Charm)
	return nil
}
