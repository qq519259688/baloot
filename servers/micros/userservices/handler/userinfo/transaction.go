package userinfo

import (
	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"runtime/debug"
)

// 获取用户基本信息
func trans_getInfo(userId int) *userInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_GetBaseInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return nil
	}

	var out userInfo
	ret := retRows[0]
	out.UserId = userId
	out.NickName = (ret[0]).(string)
	out.Vip = int((ret[1]).(int64))
	out.FaceId = int((ret[2]).(int64))
	out.FaceUrl = (ret[3]).(string)
	out.Sex = int((ret[4]).(int64))
	out.UserWords = (ret[5]).(string)
	// out.IsRobot = int((ret[6]).(int64))
	out.PrivateRoomCount = int((ret[7]).(int64))
	out.Currency = ret[8].(string)
	out.Gold = int((ret[9]).(int64))
	out.Charm = int((ret[10]).(int64))
	if len(ret) > 11 {
		out.Diamond = int((ret[11]).(int64))
	}

	return &out
}

// 保存国家地区
func saveCountry(userId int, countryName, currency string, isModify int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_SaveCountry")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@CountryName", database.AdParamInput, database.AdNVarChar, 32, countryName)
	statement.AddParamter("@Currency", database.AdParamInput, database.AdVarChar, 32, currency)
	statement.AddParamter("@IsModify", database.AdParamInput, database.AdInteger, 4, isModify)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return 0
	}

	return int((retRows[0][0]).(int64))
}

// 获取用户装扮列表
func getDecorationList(userId int) []pb.UserDecoration {
	var list []pb.UserDecoration
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserDecoration_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
		log.Error("userinfo.transaction getDecorationList json unmarshal err %v", err)
	}
	return list
}

// 保存用户装扮
func saveDecorations(userId, decorationType, itemId int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserDecoration_SaveInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@DecorationType", database.AdParamInput, database.AdInteger, 4, decorationType)
	statement.AddParamter("@ItemID", database.AdParamInput, database.AdInteger, 4, itemId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return 0
	}

	return int(retRows[0][0].(int64))
}

// 改变个人开关的状态
func changeSwitchStatus(userId int, switchType string, switchStatus int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UserSwitch_ChangeStatus")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SwitchType", database.AdParamInput, database.AdNVarChar, 32, switchType)
	statement.AddParamter("@SwitchStatus", database.AdParamInput, database.AdTinyInt, 4, switchStatus)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}

// 获取（个人）开关信息
func getSwitchInfo(userId int) []pb.SwitchInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction.getSwitchInfo recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserSwitch_GetSwitchInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	var list []pb.SwitchInfo
	if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
		log.Debug("transaction.getSwitchInfo userId=%d jsonData=%s err=%v", userId, jsonData, err)
	}
	return list
}

func updateUserCharm(userId, charm int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UserCharm_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Charm", database.AdParamInput, database.AdInteger, 4, charm)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}
