package userinfo

import (
	"bet24.com/log"
	badge "bet24.com/servers/micros/badge/proto"
	"bet24.com/servers/micros/userservices/handler/vip"
	pb "bet24.com/servers/micros/userservices/proto"
	"time"
)

type userInfo struct {
	pb.UserHotInfo
	tick int64
}

func (u *userInfo) isTimeout() bool {
	return time.Now().Unix()-u.tick >= 600
}

func (u *userInfo) dump() {
	log.Release("   UserId[%d],Idle[%d]", u.UserId, time.Now().Unix()-u.tick)
	log.Release("     Vip[%d],Decorations%v,Charm[%d]", u.Vip, u.Decorations, u.Charm)
}

func newUserHotInfo(userId int) *userInfo {
	if userId <= 0 {
		return nil
	}
	u := newUserBaseInfo(userId)
	if u == nil {
		return nil
	}
	u.Decorations = getDecorationList(userId)
	u.Switches = getSwitchInfo(userId)
	uv := vip.GetUserVip(userId)
	if uv == nil {
		u.Vip = 0
	} else {
		u.Vip = uv.Level
		u.VipPoint = uv.Point
		u.VipExpire = uv.Expire
	}
	u.Badges = badge.GetBadgeWearAndShow(userId)
	u.tick = time.Now().Unix()
	return u
}

func (u *userInfo) updateTick() {
	u.tick = time.Now().Unix()
}

func newUserBaseInfo(userId int) *userInfo {
	if userId < 0 {
		return nil
	}
	u := trans_getInfo(userId)
	if u == nil {
		log.Error("user.newUserHotInfo userId=%d is not exist", userId)
		return nil
	}
	return u
}
