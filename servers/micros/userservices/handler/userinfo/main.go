package userinfo

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/userservices/proto"
)

var mgr *usermgr

func Run() {
	log.Debug("user running")
	mgr = newUserMgr()
}

func GetInfo(userId int) *pb.UserHotInfo {
	if userId <= 0 {
		return nil
	}
	if mgr == nil {
		u := newUserBaseInfo(userId)
		if u == nil {
			return nil
		}
		return &u.UserHotInfo
	}
	return mgr.getInfo(userId)
}

func GetUserInfoInBulk(userIds []int) []pb.UserHotInfo {
	return mgr.getUserInfoInBulk(userIds)
}

// 保存国家地区
func SaveCountry(userId int, countryName, currency string) int {
	return mgr.saveCountry(userId, countryName, currency)
}

// 锁定国家地区
func LockCountry(userId int, currency string) int {
	return mgr.lockCountry(userId, currency)
}

func UpdateUserInfo(userId int) {
	mgr.clear(userId)
}

func Dump(param1, param2 string) {
	mgr.dumpUser(param1)
}

func SetUserDecoration(userId int, decorationType int, itemId int) bool {
	return mgr.setUserDecoration(userId, decorationType, itemId)
}

func OnDecorationExpired(userId int, itemId int) {
	mgr.onDecorationExpired(userId, itemId)
}

func ChangeSwitchStatus(userId int, switchType string, switchStatus int) {
	mgr.changeSwitchStatus(userId, switchType, switchStatus)
}

func AddUserCharm(userId int, charm int) {
	mgr.addUserCharm(userId, charm)
}
