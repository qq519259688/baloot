package handler

import (
	"context"

	"bet24.com/servers/micros/userservices/handler/mail"
	pb "bet24.com/servers/micros/userservices/proto"
)

func (h *Userservices) SendUserMail(ctx context.Context, req *pb.Request_mail, rsp *pb.Response_mail) error {
	rsp.RetCode, rsp.UserMails = mail.SendUserMail(req.UserId, req.Title, req.Content, req.Image)
	return nil
}

func (h *Userservices) GetUserMails(ctx context.Context, req *pb.Request_mail, rsp *pb.Response_mail) error {
	rsp.RetCode, rsp.UserMails = mail.GetUserMails(req.UserId, req.MailId)
	return nil
}

func (h *Userservices) GetUserMailTip(ctx context.Context, req *pb.Request_mail, rsp *pb.Response_mail) error {
	rsp.Success = mail.GetUserMailTip(req.UserId)
	return nil
}

func (h *Userservices) SendSysMail(ctx context.Context, req *pb.Request_mail, rsp *pb.Response_mail) error {
	rsp.Success = mail.SendSysMail(req.UserId, req.SysMailOne)
	return nil
}

func (h *Userservices) GetSysMails(ctx context.Context, req *pb.Request_mail, rsp *pb.Response_mail) error {
	rsp.SysMails = mail.GetSysMails(req.UserId, req.MailId)
	return nil
}

func (h *Userservices) GetSysMail(ctx context.Context, req *pb.Request_mail, rsp *pb.Response_mail) error {
	rsp.SysMailOne = mail.GetSysMail(req.UserId, req.MailId)
	return nil
}

func (h *Userservices) UpdateSysMail(ctx context.Context, req *pb.Request_mail, rsp *pb.Response_mail) error {
	rsp.RetCode, rsp.Items = mail.UpdateSysMail(req.UserId, req.MailId, req.Status)
	return nil
}

func (h *Userservices) DelSysMail(ctx context.Context, req *pb.Request_mail, rsp *pb.Response_mail) error {
	rsp.RetCode = mail.DelSysMail(req.UserId, req.MailId)
	return nil
}

func (h *Userservices) UpdateAllSysMail(ctx context.Context, req *pb.Request, rsp *pb.Response_mail) error {
	rsp.RetCode, rsp.Items = mail.UpdateAllSysMail(req.UserId)
	return nil
}
