package handler

import (
	"bet24.com/servers/micros/userservices/handler/vip"
	pb "bet24.com/servers/micros/userservices/proto"
	"context"
)

func (h *Userservices) GetUserVip(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.UserVip = vip.GetUserVip(req.UserId)
	return nil
}

func (h *Userservices) GetVipList(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.VipList = vip.GetVipList()
	return nil
}

func (h *Userservices) GetPurchasePackageList(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.PurchasePackages = vip.GetPurchasePackageList(req.UserId)
	return nil
}

func (h *Userservices) GetVipByLevel(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.VipInfo = vip.GetVipByLevel(req.Level)
	return nil
}

func (h *Userservices) GetExtraBankruptcy(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.Value = vip.GetExtraBankruptcy(req.UserId)
	return nil
}

func (h *Userservices) GetExtraExperience(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.Value = vip.GetExtraExperience(req.UserId)
	return nil
}

func (h *Userservices) GetExtraFriendCount(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.Value = vip.GetExtraFriendCount(req.UserId)
	return nil
}

func (h *Userservices) CanBuyPackage(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.Success = vip.CanBuyPackage(req.UserId, req.ProductId)
	return nil
}

func (h *Userservices) BuyPackage(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	vip.BuyPackage(req.UserId, req.ProductId)
	return nil
}

func (h *Userservices) AddVipPoint(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	vip.AddVipPoint(req.UserId, req.Point)
	return nil
}

func (h *Userservices) AddVipSeconds(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	vip.AddVipSeconds(req.UserId, req.PurchaseSeconds)
	return nil
}

func (h *Userservices) CheckDailyAward(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.Success, rsp.Code = vip.CheckDailyAward(req.UserId)
	return nil
}

func (h *Userservices) GiftDailyAward(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.Success = vip.GiftDailyAward(req.UserId)
	return nil
}

func (h *Userservices) GetUserPrivilegeValue(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.Value = vip.GetUserPrivilegeValue(req.UserId, req.PrivilegeType)
	return nil
}

func (h *Userservices) CanKickPrivateRoomUser(ctx context.Context, req *pb.Request_vip, rsp *pb.Reponse_vip) error {
	rsp.Success = vip.CanKickPrivateRoomUser(req.UserId, req.ToUserId)
	return nil
}
