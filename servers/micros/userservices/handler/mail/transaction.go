package mail

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/userservices/proto"
)

// 发送用户邮件
func sendUserMail(userId int, title, content, img string) (int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("mail.sendUserMail transaction recover %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	newMsgId, sendImgCount := 0, 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_ServiceMessage_Send")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Title", database.AdParamInput, database.AdNVarChar, 256, title)
	statement.AddParamter("@Msg", database.AdParamInput, database.AdNVarChar, 512, content)
	statement.AddParamter("@Img", database.AdParamInput, database.AdVarChar, 1024, img)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return newMsgId, sendImgCount
	}

	newMsgId = int((retRows[0][0]).(int64))
	sendImgCount = int((retRows[0][1]).(int64))
	return newMsgId, sendImgCount
}

// 获取用户邮件
func getUserMails(userId, mailId int) (int, []*pb.UserMail) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("mail.getUserMails transaction recover %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var mails []*pb.UserMail
	var sendImgCount int

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_ServiceMessage_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@MessageID", database.AdParamInput, database.AdInteger, 4, mailId)
	statement.AddParamter("@SendImgCount", database.AdParamOutput, database.AdInteger, 4, sendImgCount)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	rowsLen := len(retRows)

	if rowsLen > 1 {
		for i := 0; i < rowsLen-1; i++ {
			ret := retRows[i]
			var out pb.UserMail
			out.Id = int((ret[0]).(int64))
			out.Title = (ret[1]).(string)
			out.FromUserId = int((ret[2]).(int64))
			out.ToUserId = int((ret[3]).(int64))
			out.Content = (ret[4]).(string)
			crdateStr := (ret[5]).(string)
			crdate := common.ParseTime(crdateStr)
			out.Crdate = common.GetStamp(crdate)
			mails = append(mails, &out)
		}
	}

	sendImgCount = int((retRows[rowsLen-1][0]).(int64))
	return sendImgCount, mails
}

// 获取用户邮件提示
func getUserMailTip(userId int) bool {
	defer func() {
		if err := recover(); err != nil {
			log.Error("mail.getUserMailTip transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_ServiceMessage_GetTip")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.Execute(sqlstring)
	return len(retRows) > 0
}

// 发送系统邮件(含附件)
func sendSysMail(userId int, sm *pb.SysMail) bool {
	defer func() {
		if err := recover(); err != nil {
			log.Error("mail.sendSysMail transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	tools := ""
	if len(sm.Tools) > 0 {
		buf, err := json.Marshal(sm.Tools)
		if err != nil {
			log.Error("mail.sendSysMail transaction json marshal fail err %v", err)
			return false
		}
		tools = string(buf)
	}

	sysMailId := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SysMessage_Send")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Title", database.AdParamInput, database.AdNVarChar, 32, sm.Title)
	statement.AddParamter("@Content", database.AdParamInput, database.AdNVarChar, 256, sm.Content)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, sm.Status)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 64, sm.SourceName)
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdInteger, 4, sm.Crdate)
	statement.AddParamter("@Tools", database.AdParamInput, database.AdVarChar, 1024, tools)
	statement.AddParamter("@SysMsgID", database.AdParamOutput, database.AdInteger, 4, sysMailId)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return false
	}

	sysMailId = int((retRows[0][0]).(int64))
	return sysMailId > 0
}

// 获取系统邮件列表
func getSysMails(userId, sysMsgId int) []*pb.SysMail {
	defer func() {
		if err := recover(); err != nil {
			log.Error("mail.getSysMails transaction recover %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SysMessage_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SysMsgID", database.AdParamInput, database.AdInteger, 4, sysMsgId)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	rowsLen := len(retRows)

	var sysMails []*pb.SysMail
	for i := 0; i < rowsLen; i++ {
		ret := retRows[i]
		var out pb.SysMail

		out.Id = int((ret[0]).(int64))
		out.Title = (ret[1]).(string)
		out.Content = (ret[2]).(string)
		out.Status = int((ret[3]).(int64))
		out.SourceName = (ret[4]).(string)
		crdateStr := (ret[5]).(string)
		crdate := common.ParseTime(crdateStr)
		out.Crdate = common.GetStamp(crdate)
		tools := (ret[6]).(string)

		if tools != "" {
			if err := json.Unmarshal([]byte(tools), &out.Tools); err != nil {
				log.Error("mail.getSysMails transaction json unmarshal fail err %v", err)
				return nil
			}
		}

		sysMails = append(sysMails, &out)
	}

	return sysMails
}

// 获取系统邮件信息
func getSysMail(userId, sysMsgId int) *pb.SysMail {
	defer func() {
		if err := recover(); err != nil {
			log.Error("mail.getSysMail transaction recover %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SysMessage_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SysMsgID", database.AdParamInput, database.AdInteger, 4, sysMsgId)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	rowsLen := len(retRows)

	out := new(pb.SysMail)
	for i := 0; i < rowsLen; i++ {
		ret := retRows[i]

		out.Id = int((ret[0]).(int64))
		out.Title = (ret[1]).(string)
		out.Content = (ret[2]).(string)
		out.Status = int((ret[3]).(int64))
		out.SourceName = (ret[4]).(string)
		crdateStr := (ret[5]).(string)
		crdate := common.ParseTime(crdateStr)
		out.Crdate = common.GetStamp(crdate)
		tools := (ret[6]).(string)

		if tools != "" {
			if err := json.Unmarshal([]byte(tools), &out.Tools); err != nil {
				log.Error("mail.getSysMails transaction json unmarshal fail err %v", err)
				return nil
			}
		}
	}

	return out
}

// 修改系统邮件
func updateSysMail(userId, sysMsgId, status int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("mail.updateSysMail transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SysMessage_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SysMsgId", database.AdParamInput, database.AdInteger, 4, sysMsgId)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, status)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}
	return int((retRows[0][0]).(int64))
}

// 删除系统邮件
func delSysMail(userId, sysMsgId int) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("mail.delSysMail transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SysMessage_Del")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SysMsgID", database.AdParamInput, database.AdInteger, 4, sysMsgId)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}

	return int((retRows[0][0]).(int64))
}

// 获取系统邮件列表
func getGiftMails(userId int) []*pb.SysMail {
	defer func() {
		if err := recover(); err != nil {
			log.Error("mail.getSysMails transaction recover %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SysMessage_GetGiftList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	rowsLen := len(retRows)

	var sysMails []*pb.SysMail
	for i := 0; i < rowsLen; i++ {
		ret := retRows[i]
		var out pb.SysMail

		out.Id = int((ret[0]).(int64))
		out.Title = (ret[1]).(string)
		out.Content = (ret[2]).(string)
		out.Status = int((ret[3]).(int64))
		out.SourceName = (ret[4]).(string)
		crdateStr := (ret[5]).(string)
		crdate := common.ParseTime(crdateStr)
		out.Crdate = common.GetStamp(crdate)
		tools := (ret[6]).(string)

		if tools != "" {
			if err := json.Unmarshal([]byte(tools), &out.Tools); err != nil {
				log.Error("mail.getSysMails transaction json unmarshal fail err %v", err)
				return nil
			}
		}

		sysMails = append(sysMails, &out)
	}

	return sysMails
}
