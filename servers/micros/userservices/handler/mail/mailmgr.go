package mail

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	pb "bet24.com/servers/micros/userservices/proto"
)

type mailmgr struct {
}

func newMailMgr() *mailmgr {
	log.Debug("mail manager running")
	return &mailmgr{}
}

// 发送用户邮件(客服留言)
func (this *mailmgr) sendUserMail(userId int, title, content, img string) (int, []*pb.UserMail) {
	var mails []*pb.UserMail
	msgId, sendImgCount := sendUserMail(userId, title, content, img)
	if msgId <= 0 {
		return sendImgCount, mails
	}
	mails = append(mails, &pb.UserMail{
		Id:         msgId,
		Title:      title,
		FromUserId: userId,
		ToUserId:   -1,
		Content:    content,
		IsRead:     false,
		Crdate:     common.GetTimeStamp(),
		OpUserId:   -1,
		OpUserName: "",
	})
	return sendImgCount, mails
}

// 获取用户邮件(客服留言)
func (this *mailmgr) getUserMails(userId, mailId int) (int, []*pb.UserMail) {
	return getUserMails(userId, mailId)
}

// 获取用户邮件提示
func (this *mailmgr) getUserMailTip(userId int) bool {
	return getUserMailTip(userId)
}

// 发送系统邮件(含附件)
func (this *mailmgr) sendSysMail(userId int, sm *pb.SysMail) bool {
	return sendSysMail(userId, sm)
}

// 获取系统邮件(含附件)
func (this *mailmgr) getSysMails(userId, sysMsgId int) []*pb.SysMail {
	return getSysMails(userId, sysMsgId)
}

// 获取系统邮件信息(含附件)
func (this *mailmgr) getSysMail(userId, sysMsgId int) *pb.SysMail {
	return getSysMail(userId, sysMsgId)
}

// 领取所有邮件
func (this *mailmgr) updateAllSysMail(userId int) (int, []item.ItemPack) {
	//领取邮件附件
	mails := getGiftMails(userId)
	if mails == nil {
		log.Debug("updateAllSysMail userId=%d not exist", userId)
		return 0, nil
	}

	var items []item.ItemPack
	for _, v := range mails {
		// 2=领取、3=删除状态
		if v.Status == pb.SysMail_Status_Gift || v.Status == pb.SysMail_Status_Del {
			continue
		}

		// 没有附件的邮件
		if v.Tools == nil {
			// 未读
			if v.Status == pb.SysMail_Status_UnRead {
				updateSysMail(userId, v.Id, pb.SysMail_Status_Read)
			}
			continue
		}

		//修改为已领取状态
		if retCode := updateSysMail(userId, v.Id, pb.SysMail_Status_Gift); retCode != 1 {
			log.Debug("updateAllSysMail fail userId=%d sysMsgId=%d status=%d", userId, v.Id, pb.SysMail_Status_Gift)
			continue
		}

		//发送道具
		if success := inventory.AddItems(userId, v.Tools, "领取系统邮件附件", common.LOGTYPE_ATTACHMENT_GIFT); !success {
			log.Debug("updateAllSysMail AddItems fail userId=%d sysMsgId=%d status=%d", userId, v.Id, pb.SysMail_Status_Gift)
			continue
		}

		items = append(items, v.Tools...)
	}

	return 1, item.GroupItems(items)
}

// 修改系统邮件
func (this *mailmgr) updateSysMail(userId, sysMsgId, status int) (int, []item.ItemPack) {
	//领取邮件附件
	m := this.getSysMail(userId, sysMsgId)
	if m == nil {
		log.Debug("updateSysMail userId=%d sysMsgId=%d status=%d not exist", userId, sysMsgId, status)
		return 0, nil
	}

	//读系统邮件
	if status == pb.SysMail_Status_Read {

		//不是未读状态
		if m.Status != pb.SysMail_Status_UnRead {
			return 0, nil
		}

		//置为读状态
		ret := updateSysMail(userId, sysMsgId, status)
		return ret, nil
	}

	//已领取
	if m.Status == pb.SysMail_Status_Gift {
		log.Debug("updateSysMail 不能重复领取, userId=%d sysMsgId=%d status=%d", userId, sysMsgId, status)
		return 0, nil
	}

	//邮件附件
	if m.Tools == nil {
		log.Debug("updateSysMail 没有附件, userId=%d sysMsgId=%d status=%d", userId, sysMsgId, status)
		return 0, nil
	}

	//修改为已领取状态
	if ret := updateSysMail(userId, sysMsgId, status); ret != 1 {
		log.Debug("updateSysMail fail userId=%d sysMsgId=%d status=%d", userId, sysMsgId, status)
		return 0, nil
	}

	//发送道具
	if success := inventory.AddItems(userId, m.Tools, "领取系统邮件附件", common.LOGTYPE_ATTACHMENT_GIFT); !success {
		log.Debug("updateSysMail AddItems fail userId=%d sysMsgId=%d status=%d", userId, sysMsgId, status)
		return 0, nil
	}

	return 1, m.Tools
}

// 删除系统邮件
func (this *mailmgr) delSysMail(userId, sysMsgId int) int {
	return delSysMail(userId, sysMsgId)
}
