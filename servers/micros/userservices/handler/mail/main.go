package mail

import (
	item "bet24.com/servers/micros/item_inventory/proto"
	pb "bet24.com/servers/micros/userservices/proto"
)

var mgr *mailmgr

func Run() {
	mgr = newMailMgr()
}

func Dump(param1, param2 string) {

}

func SendUserMail(userId int, title, content, img string) (int, []*pb.UserMail) {
	return mgr.sendUserMail(userId, title, content, img)
}

func GetUserMails(userId, mailId int) (int, []*pb.UserMail) {
	return mgr.getUserMails(userId, mailId)
}

func GetUserMailTip(userId int) bool {
	return mgr.getUserMailTip(userId)
}

// 发送系统邮件(含附件)
func SendSysMail(userId int, sm *pb.SysMail) bool {
	return mgr.sendSysMail(userId, sm)
}

// 获取系统邮件(含附件)
func GetSysMails(userId, sysMsgId int) []*pb.SysMail {
	return mgr.getSysMails(userId, sysMsgId)
}

// 获取系统邮件信息(含附件)
func GetSysMail(userId, sysMsgId int) *pb.SysMail {
	return mgr.getSysMail(userId, sysMsgId)
}

func UpdateSysMail(userId, sysMsgId, status int) (int, []item.ItemPack) {
	return mgr.updateSysMail(userId, sysMsgId, status)
}

func DelSysMail(userId, sysMsgId int) int {
	return mgr.delSysMail(userId, sysMsgId)
}

func UpdateAllSysMail(userId int) (int, []item.ItemPack) {
	return mgr.updateAllSysMail(userId)
}
