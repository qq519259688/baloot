package main

import (
	slog "bet24.com/log"
	_ "bet24.com/servers/micros/userservices/proto"
	"log"
)

func main() {
	logger, err := slog.New("debug", "debug", "log/client", log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	for {
	}
}
