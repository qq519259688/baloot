package proto

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

// 系统邮件
type SysMail struct {
	Id         int             //标识
	Title      string          //标题
	Content    string          //内容
	Status     int             //状态(0=未读 1=已读  2=领取)
	SourceName string          //源名称
	Crdate     int             //时间
	Tools      []item.ItemPack //附件内容
}

const (
	SysMail_Status_UnRead = iota //0=未读
	SysMail_Status_Read          //1=已读
	SysMail_Status_Gift          //2=领取
	SysMail_Status_Del           //3=已删除
)
