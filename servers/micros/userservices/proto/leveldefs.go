package proto

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	Award_Invalid   = iota // 0=无效
	Award_Available        // 1=可领取
	Award_Received         // 2=已领取
	Award_Notify           // 3=通知刷新
)

const (
	WhiteList_Normal = iota // 无属性
	WhiteList_White         // 白名单
	WhiteList_Black         // 黑名单
)

const (
	LevelPrivilege_Transfer     = iota
	LevelPrivilege_ShowGameHall // 显示游戏大厅
)

type LevelPrivilege struct {
	Privilege int `json:"pid"`
	Param     int `json:"p"`
}

type LevelInfo struct {
	Level      int              // 等级
	Exp        int              // 所需经验
	Desc       string           // 描述
	GiftPack   []item.ItemPack  // 升级礼包(json格式)
	Privileges []LevelPrivilege `json:",omitempty"` // 对应解锁权限
}

type GameExpInfo struct {
	GameID  int // 游戏ID
	WinExp  int // 赢获取经验值
	LoseExp int // 输获取经验值
}

// 基本信息
type LevelBaseInfo struct {
	Level      int  // Vip等级
	Experience int  // 分数
	IsWhite    int  // 是否白名单 0=普通，1=白名单 2=黑名单
	IsFacebook bool // 是否facebook账号
	LoginAward int  // 登录奖励(0=无效  1=有奖励领取  2=已领取  3=通知刷新)
	IsAdd      bool // 是否可以加经验
}
