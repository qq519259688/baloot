package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

func GetLevelList() string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetLevelList", args, reply)
	if err != nil {
		log.Debug("level failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetUserLevel(userId int) LevelBaseInfo {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserLevel", args, reply)
	if err != nil {
		log.Debug("level failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
	return reply.LevelBase
}

func IsWhiteList(userId int) bool {
	lv := GetUserLevel(userId)
	return lv.IsWhite == WhiteList_White
}

func IsBlackList(userId int) bool {
	lv := GetUserLevel(userId)
	return lv.IsWhite == WhiteList_Black
}

func IsShowGameHall(userId int) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "IsShowGameHall", args, reply)
	if err != nil {
		log.Debug("level failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
	return reply.Success
}

func AddGameExp(userId, gameId, score int) {
	// 不适用
	if true {
		return
	}
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
		GameId: gameId,
		Score:  score,
	}

	err := xclient.Call(context.Background(), "AddGameExp", args, nil)
	if err != nil {
		log.Debug("level failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func LoginAward(userId int) (bool, int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "LoginAward", args, reply)
	if err != nil {
		log.Debug("level failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
	return reply.Success, reply.RetCode
}

func AddExperience(userId int, exp int) int {
	xclient := getClient()
	args := &Request{
		UserId: userId,
		Exp:    exp,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "AddExperience", args, reply)
	if err != nil {
		log.Debug("level failed to call AddExperience :  %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
	return reply.RetCode
}
