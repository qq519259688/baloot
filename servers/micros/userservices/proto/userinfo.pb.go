package proto

import (
	badge "bet24.com/servers/micros/badge/proto"
	"context"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
)

// 用户装扮信息
// 挡道具消失后，需要更新装扮设置，或者在获取装扮中判断时效
type UserDecoration struct {
	Type   int // 见item.go中DecorationType定义
	ItemId int // 生效ID
}

// 开关的信息

const (
	SwitchType_Watch = iota
	SwitchType_Track
	SwitchType_Max
)

// 用户信息
type UserHotInfo struct {
	UserId           int
	NickName         string
	Vip              int
	FaceUrl          string
	FaceId           int
	Sex              int
	UserWords        string           `json:",omitempty"`
	PrivateRoomCount int              `json:",omitempty"` // 私人场局数
	Decorations      []UserDecoration `json:",omitempty"`
	Switches         []SwitchInfo     // 开关
	Currency         string           // 国家币种
	Gold             int              // 金币
	VipPoint         int
	VipExpire        int
	Badges           []badge.BadgePosition // 佩戴与展示的徽章列表
	Charm            int                   // 魅力值
	Diamond          int                   // 钻石数量
}

func (ui *UserHotInfo) GetUserGold() int {
	return ui.Gold
}

// 开关信息
type SwitchInfo struct {
	SwitchType   string // 开关类型
	SwitchStatus int    // 开关状态
}

func GetUserInfo(userId int) *UserHotInfo {
	xclient := getClient()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserInfo", args, reply)
	if err != nil {
		log.Debug("userservices failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}

	return reply.UserInfo
}

func GetUserInfoInBulk(userIds []int) []UserHotInfo {
	xclient := getClient()

	args := &Request{
		UserIds: userIds,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserInfoInBulk", args, reply)
	if err != nil {
		log.Debug("userservices failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}

	return reply.UserInfos
}

func SaveCountry(userId int, countryName, currency string) string {
	args := &Request{
		UserId:      userId,
		CountryName: countryName,
		Currency:    currency,
	}

	reply := &Response{}

	err := getClient().Call(context.Background(), "SaveCountry", args, reply)
	if err != nil {
		log.Debug("userservices failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return "0"
	}

	return reply.Data
}

func LockCountry(userId int, currency string) int {
	args := &Request{
		UserId:   userId,
		Currency: currency,
	}

	reply := &Response{}

	err := getClient().Call(context.Background(), "LockCountry", args, reply)
	if err != nil {
		log.Debug("userservices failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.RetCode
}

func GetUserFaceUrl(userId int) string {
	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := getClient().Call(context.Background(), "GetUserFaceUrl", args, reply)
	if err != nil {
		log.Debug("userservices failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func UpdateUserInfo(userId int) {
	args := &Request{
		UserId: userId,
	}

	err := getClient().Call(context.Background(), "UpdateUserInfo", args, nil)
	if err != nil {
		log.Debug("userservices failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return
	}
}

func SetUserDecoration(userId int, decorationType int, itemId int) bool {
	args := &Request{
		UserId:         userId,
		DecorationType: decorationType,
		ItemId:         itemId,
	}
	reply := &Response{}
	err := getClient().Call(context.Background(), "SetUserDecoration", args, reply)
	if err != nil {
		log.Debug("userservices failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.Success
}

func GetUserDecoration(userId int) []UserDecoration {
	var ret []UserDecoration
	usr := GetUserInfo(userId)
	if usr != nil {
		return usr.Decorations
	}
	return ret
}

func OnDecorationExpired(userId int, itemId int) {
	args := &Request{
		UserId: userId,
		ItemId: itemId,
	}

	err := getClient().Call(context.Background(), "OnDecorationExpired", args, nil)
	if err != nil {
		log.Debug("userservices.OnDecorationExpired failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return
	}
}

// 设置开关状态
func ChangeSwitchStatus(userId int, switchInfo SwitchInfo) {
	args := &Request_Switch{
		UserId:     userId,
		SwitchInfo: switchInfo,
	}

	// 设置后同时设置变量
	if err := getClient().Call(context.Background(), "ChangeSwitchStatus", args, nil); err != nil {
		log.Debug("userservices.ChangeSwitchStatus switchInfo: %v, failed to call: %v", switchInfo, err)
		common.GetClientPool().RemoveClient(ServiceName)
		return
	}
	return
}

func AddUserCharm(userId int, charm int) {
	args := &Request{
		UserId: userId,
		Charm:  charm,
	}

	// 设置后同时设置变量
	if err := getClient().Call(context.Background(), "AddUserCharm", args, nil); err != nil {
		log.Debug("userservices.AddUserCharm : %d:%d, failed to call: %v", userId, charm, err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}
