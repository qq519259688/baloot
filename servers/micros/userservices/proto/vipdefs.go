package proto

import (
	"bet24.com/servers/common"
	_ "bet24.com/servers/common"
	item "bet24.com/servers/micros/item_inventory/proto"
)

// 获取日期或者星期Index，用于每日礼包领取，或者其他礼包购买限制
// common.GetDayIndex()
// common.GetWeekIndex()

// 用户vip信息，信息存入数据库
type UserVip struct {
	Level                int // 等级
	Point                int // 当前累积点数
	Expire               int // 结束时间Time.Unix() 0表示不生效
	DailyPackageClaimDay int // 上一次领取每日礼包的日期Index
}

func (uv *UserVip) IsVip() bool {
	return uv.Expire > common.GetTimeStamp()
}

// 系统vip配置，由json配置提供，进入platformconfig
type VipInfo struct {
	Level          int             // 等级
	Point          int             // 所需点数
	Privileges     []VipPrivilege  // 特权配置，可多项
	DailyPackage   []item.ItemPack // 每日礼包
	UpgragePackage []item.ItemPack // 升级礼包，礼包内容为时效道具，需要遍历所有低等级赠送一次
}

// VIP特权列表
const (
	VipPrivilege_Invalid                   = iota // 0 无效值
	VipPrivilege_ExtraBankruptcy                  // 1 额外破产补助（百分比）
	VipPrivilege_ExtraExperience                  // 2 额外经验获取（百分比）
	VipPrivilege_ExtraFriend                      // 3 额外好友上限
	VipPrivilege_EntranceRemind                   // 4 入场提醒
	VipPrivilege_PrivateRoomKick                  // 5 私人场踢人权限
	VipPrivilege_ExtraGoldTransferCount           // 6 额外金币转账次数
	VipPrivilege_ExtraDiamondTransferCount        // 7 额外钻石转账次数
)

const (
	VipAward_Success        = iota // 0 可以领取
	VipAward_Nonmember             // 1 您不是vip
	VipAward_ReceivedAward         // 2 今日已领取奖励
	VipAward_ConfigNotExist        // 3 当前级别配置不存在
	VipAward_NoReward              // 4 当前配置没有奖励
)

// 获取特权的参数
func (vi *VipInfo) GetPrivilegeParam(privilegeType int) int {
	for k, v := range vi.Privileges {
		if v.Privilege == privilegeType {
			return vi.Privileges[k].Param
		}
	}
	return VipPrivilege_Invalid
}

type VipPrivilege struct {
	Privilege int `json:"pid"`
	Param     int `json:"p"`
}

// 可购买礼包
// 礼包购买历史
type PurchaseHistory struct {
	ProductId    string // 产品id
	PurchaseTime int64  // 时间戳
}

const (
	PurchaseLimit_none    = iota // 0不限购
	PurchaseLimit_daily          // 1每日限购
	PurchaseLimit_weekly         // 2每周限购
	PurchaseLimit_monthly        // 3每月限购
)

type PurchaseLimit struct {
	PurchaseLimitType  int `json:",omitempty"` // 限购类型
	PurchaseLimitCount int `json:",omitempty"` // 限购数量
}

type PurchasePackage struct {
	PackageId int             // 礼包ID
	Name      string          // 礼包名称
	Desc      string          // 描述
	ProductId string          // 商品ID，需要同步商城配置
	Price     float64         // 价格
	Items     []item.ItemPack // 礼包内容
	PurchaseLimit
	BuyAble bool // 是否可购买，发给前端用
}

type VipInterface interface {
	Run()                              // 创建对象，加载vip配置信息，开启定时器刷新配置
	Dump(cmd, param string)            // dump系统信息，用户信息
	OnUserEnter(userId int)            // 用户进入，创建用户信息，读取用户礼包购买历史，如果是vip，判断是否领取VIP奖励
	OnUserExit(userId int)             // 将用户标记为离线，等待10分钟后删除数据
	AddVipPoint(userId int, point int) // 添加vip点数，可能触发升级,升级如果当前是vip，则需要发放登录道具和升级道具
	/*
		添加vip时长(已完成支付)，如果当前Expire==0，则expire=time.Now().Unix()+purchaseSeconds
		否则Expire+=purchaseSeconds
		需要处理:判断并领取每日礼包，发放道具，如果为时效道具则设置道具时效(交由item_inventory模块处理)
	*/
	AddVipSeconds(userId int, purchaseSeconds int64)
	GetExtraSubsidy(userId int) int     // 获取额外每日补助百分比
	GetExtraExperience(userId int) int  // 获取额外经验百分比
	GetExtraFriendCount(userId int) int // 获取额外好友上限数量

	CanBuyPackage(userId int, packageId int) bool // 判断是否能购买礼包
	// 实际购买礼包（已完成支付），发放道具，添加购买历史记录
	BuyPackage(userId int, packageId int)

	GetVipList() []VipInfo            // 获取vip列表，前端展现用
	GetVipByLevel(level int) *VipInfo // 根据等级获取vip配置信息

	GetUserVip(userId int) UserVip // 根据用户ID查询用户vip状态
}

/*
	VIP失效检查
	1、登录时，判断time.Now.Unix() > Expire ,触发OnVipExpired
	2、登录后，如果vip时效还在，启动定时器,time.AfterFunc((Expire-time.Now.Unix()) * time.Second,OnVipExpired)

	失效后，updateUserVip设置Expire为0，发通知(notification)
*/

/*
	数据库表：
	UserVip表: UserId,Level,Point,Expire,DailyPackageClaimDay
	VipPackagePurchaseHistory表: UserId,PackageId,PurchaseTime
	提供操作：
	getUserVip(userId int) UserVip
	updateUserVip(userId int,vipInfo UserVip)
	getPackagePurchaseHistory(userId int) []PurchaseHistory
	addPackagePurchaseHistory(userId int,packageId int,time.Now().Unix())
*/

/*
	vip配置
	{
		"VipList":[
			{
				"Level":1,
				"Point":10,
				"Privileges":[{"pid":1,"p":10},{"pid":2,"p":5}],
				"DailyPackage":[
					{
						"ItemId":1,
						"Count":20000
					},
					{
						"ItemId":2,
						"Count":30
					}
				],
				"UpgragePackage":[
					{
						"ItemId":40003,
						"Count":1
					},
					{
						"ItemId":40013,
						"Count":1
					}
				]
			}
		],
		"PurchasePackages":[
			{
				"PackageId":1,
				"Name":"vip礼包1",
				"Desc":"vip礼包1",
				"ProductId":70001,
				"Price":29.99,
				"Items":[
					{
						"ItemId":1,
						"Count":20000
					},
					{
						"ItemId":2,
						"Count":30
					}
				]
			}
		]
	}
*/
