package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

func GetARobot() int {
	xclient := getClient()

	args := &Request{}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetARobot", args, reply)
	if err != nil {
		log.Debug("GetARobot failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.RetCode
}

func IsRobot(userId int) bool {
	xclient := getClient()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "IsRobot", args, reply)
	if err != nil {
		log.Debug("IsRobot failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
	return reply.Success
}
