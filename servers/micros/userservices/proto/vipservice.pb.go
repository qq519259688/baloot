package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
	"encoding/json"
)

type Request_vip struct {
	ProductId       string
	PrivilegeType   int
	PurchaseSeconds int
	Point           int
	UserId          int
	Level           int
	Name            string
	ToUserId        int
}

type Reponse_vip struct {
	Value            int
	Code             int
	Data             string
	Success          bool
	UserVip          *UserVip
	VipList          []VipInfo
	VipInfo          *VipInfo
	PurchasePackages []PurchasePackage
}

// 根据用户ID查询用户vip状态
func GetUserVip(userId int) string {
	xclient := getClient()

	args := &Request_vip{
		UserId: userId,
	}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "GetUserVip", args, reply)
	if err != nil {
		log.Debug("vipservice.GetUserVip failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	if reply.UserVip == nil {
		return ""
	}
	d, _ := json.Marshal(reply.UserVip)
	return string(d)
}

func GetUserVipInfo(userId int) *UserVip {
	xclient := getClient()

	args := &Request_vip{
		UserId: userId,
	}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "GetUserVip", args, reply)
	if err != nil {
		log.Debug("vipservice.GetUserVip failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.UserVip
}

// 获取vip列表，前端展现用
func GetVipList() string {
	xclient := getClient()

	args := &Request_vip{}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "GetVipList", args, reply)
	if err != nil {
		log.Debug("vipservice.GetVipList failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	if reply.VipList == nil {
		return ""
	}
	d, _ := json.Marshal(reply.VipList)
	return string(d)
}

// 获取购买礼包列表
func GetPurchasePackageList(userId int) string {
	xclient := getClient()

	args := &Request_vip{UserId: userId}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "GetPurchasePackageList", args, reply)
	if err != nil {
		log.Debug("vipsvc.GetPurchasePackageList failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	if reply.PurchasePackages == nil {
		return ""
	}
	d, _ := json.Marshal(reply.PurchasePackages)
	return string(d)
}

// 根据等级获取vip配置信息
func GetVipByLevel(level int) *VipInfo {
	xclient := getClient()

	args := &Request_vip{
		Level: level,
	}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "GetVipByLevel", args, reply)
	if err != nil {
		log.Debug("vipservice.GetVipByLevel failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}

	log.Debug("GetVipByLevel return %+v", reply.VipInfo)
	return reply.VipInfo
}

// 获取额外补助百分比
func GetExtraBankruptcy(userId int) int {
	xclient := getClient()

	args := &Request_vip{
		UserId: userId,
	}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "GetExtraBankruptcy", args, reply)
	if err != nil {
		log.Debug("vipservice.GetExtraBankruptcy failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.Value
}

// 获取额外经验百分比
func GetExtraExperience(userId int) int {
	xclient := getClient()

	args := &Request_vip{
		UserId: userId,
	}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "GetExtraExperience", args, reply)
	if err != nil {
		log.Debug("vipservice.GetExtraExperience failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.Value
}

// 获取额外好友上限数量
func GetExtraFriendCount(userId int) int {
	xclient := getClient()

	args := &Request_vip{
		UserId: userId,
	}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "GetExtraFriendCount", args, reply)
	if err != nil {
		log.Debug("vipservice.GetExtraFriendCount failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.Value
}

func GetUserPrivilegeValue(userId, privilegeType int) int {
	xclient := getClient()

	args := &Request_vip{
		UserId:        userId,
		PrivilegeType: privilegeType,
	}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "GetUserPrivilegeValue", args, reply)
	if err != nil {
		log.Debug("vipservice.GetUserPrivilegeValue failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.Value
}

func CanKickPrivateRoomUser(userId, toUserId int) bool {
	xclient := getClient()
	args := &Request_vip{
		UserId:   userId,
		ToUserId: toUserId,
	}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "CanKickPrivateRoomUser", args, reply)
	if err != nil {
		log.Debug("vipservice.CanKickPrivateRoomUser failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.Success
}

// 判断是否能购买礼包
func CanBuyPackage(userId int, productId string) bool {
	xclient := getClient()

	args := &Request_vip{
		UserId:    userId,
		ProductId: productId,
	}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "CanBuyPackage", args, reply)
	if err != nil {
		log.Debug("vipservice.CanBuyPackage failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.Success
}

// 实际购买礼包（已完成支付），发放道具，添加购买历史记录
func BuyPackage(userId int, productId string) {
	xclient := getClient()

	args := &Request_vip{
		UserId:    userId,
		ProductId: productId,
	}

	err := xclient.Call(context.Background(), "BuyPackage", args, nil)
	if err != nil {
		log.Debug("vipservice.BuyPackage failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

// 添加vip点数，可能触发升级,升级如果当前是vip，则需要发放登录道具和升级道具
func AddVipPoint(userId int, point int) {
	xclient := getClient()

	args := &Request_vip{
		UserId: userId,
		Point:  point,
	}

	err := xclient.Call(context.Background(), "AddVipPoint", args, nil)
	if err != nil {
		log.Debug("vipservice.AddVipPoint failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

// 添加vip时长(已完成支付)
func AddVipSeconds(userId int, purchaseSeconds int) {
	xclient := getClient()

	args := &Request_vip{
		UserId:          userId,
		PurchaseSeconds: purchaseSeconds,
	}

	err := xclient.Call(context.Background(), "AddVipSeconds", args, nil)
	if err != nil {
		log.Debug("vipservice.AddVipSeconds failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

// 检查是否能够领取每日礼包
func CheckDailyAward(userId int) (bool, int) {
	xclient := getClient()

	args := &Request_vip{
		UserId: userId,
	}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "CheckDailyAward", args, reply)
	if err != nil {
		log.Debug("vipservice.CheckDailyAward failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false, -1
	}
	return reply.Success, reply.Code
}

// 领取每日奖励
func GiftDailyAward(userId int) string {
	xclient := getClient()

	args := &Request_vip{
		UserId: userId,
	}

	reply := &Reponse_vip{}

	err := xclient.Call(context.Background(), "GiftDailyAward", args, reply)
	if err != nil {
		log.Debug("vipservice.GiftDailyAward failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return "fail"
	}
	return "ok"
}
