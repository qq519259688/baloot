package proto

// 客服回复、用户邮件(不含附件)
type UserMail struct {
	Id         int    //标识
	Title      string //标题
	FromUserId int    //用户ID
	ToUserId   int    //目标用户ID
	Content    string //消息
	IsRead     bool   //是否已读
	Crdate     int    //时间

	OpUserId   int    //客服账号id
	OpUserName string //客服账号名称
}
