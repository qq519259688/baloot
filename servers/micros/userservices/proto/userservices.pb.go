package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "userservices"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	UserId         int
	Name           string
	CountryName    string
	Currency       string
	Point          int
	Level          int
	GameId         int
	Score          int
	DecorationType int
	ItemId         int
	TaskID         int
	Status         int
	Charm          int
	Exp            int
	UserIds        []int
}

type Request_Switch struct {
	UserId int
	SwitchInfo
}

type Response struct {
	RetCode  int
	Data     string
	UserInfo *UserHotInfo

	Success    bool
	Level      int
	Point      int
	Param      int
	LevelBase  LevelBaseInfo
	SwitchList []int

	UserInfos []UserHotInfo
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("userservices failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

func AddUser(userId int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "AddUser", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("userservice failed to call: %v", err)

	}
}

func RemoveUser(userId int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "RemoveUser", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("level failed to call: %v", err)
	}
}
