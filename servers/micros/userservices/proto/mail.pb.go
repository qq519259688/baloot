package proto

import (
	"context"
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
	item "bet24.com/servers/micros/item_inventory/proto"
)

type Request_mail struct {
	Name       string
	UserId     int
	Title      string
	Content    string
	Image      string
	MailId     int
	SysMailOne *SysMail
	Status     int
}

type Response_mail struct {
	Data       string
	Success    bool
	RetCode    int
	UserMails  []*UserMail
	SysMails   []*SysMail
	SysMailOne *SysMail
	Items      []item.ItemPack
}

const (
	MailType_sys = iota
	MailType_service
	MailType_charge
)

// 发送用户邮件(客服留言)
func SendUserMail(userId int, title, content, img string) (int, []*UserMail) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_mail{
		UserId:  userId,
		Title:   title,
		Content: content,
		Image:   img,
	}

	reply := &Response_mail{}

	err := xclient.Call(context.Background(), "SendUserMail", args, reply)
	if err != nil {
		log.Debug("mail failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, nil
	}

	return reply.RetCode, reply.UserMails
}

// 获取用户邮件(客服留言)
func GetUserMails(userId int, mailId int) (int, []*UserMail) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_mail{
		UserId: userId,
		MailId: mailId,
	}

	reply := &Response_mail{}

	err := xclient.Call(context.Background(), "GetUserMails", args, reply)
	if err != nil {
		log.Debug("mail failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, nil
	}

	return reply.RetCode, reply.UserMails
}

// 获取用户邮件提示(客服留言)
func GetUserMailTip(userId int) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_mail{
		UserId: userId,
	}

	reply := &Response_mail{}

	err := xclient.Call(context.Background(), "GetUserMailTip", args, reply)
	if err != nil {
		log.Debug("mail failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}

	return reply.Success
}

// 发送系统邮件(含附件)
func SendSysMail(userId int, sm *SysMail) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_mail{
		UserId:     userId,
		SysMailOne: sm,
	}

	reply := &Response_mail{}

	err := xclient.Call(context.Background(), "SendSysMail", args, reply)
	if err != nil {
		log.Debug("mail failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}

	return reply.Success
}

// 获取系统邮件列表
func GetSysMails(userId, sysMsgId int) []*SysMail {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_mail{
		UserId: userId,
		MailId: sysMsgId,
	}

	reply := &Response_mail{}

	err := xclient.Call(context.Background(), "GetSysMails", args, reply)
	if err != nil {
		log.Debug("mail failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}

	return reply.SysMails
}

func GetSysMail(userId, sysMsgId int) *SysMail {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_mail{
		UserId: userId,
		MailId: sysMsgId,
	}

	reply := &Response_mail{}

	err := xclient.Call(context.Background(), "GetSysMail", args, reply)
	if err != nil {
		log.Debug("mail failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}

	return reply.SysMailOne
}

// 修改系统邮件
func UpdateSysMail(userId, sysMsgId, status int) (int, []item.ItemPack) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_mail{
		UserId: userId,
		MailId: sysMsgId,
		Status: status,
	}

	reply := &Response_mail{}

	err := xclient.Call(context.Background(), "UpdateSysMail", args, reply)
	if err != nil {
		log.Debug("mail failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, nil
	}

	return reply.RetCode, reply.Items
}

// 删除系统邮件
func DelSysMail(userId, sysMsgId int) int {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_mail{
		UserId: userId,
		MailId: sysMsgId,
	}

	reply := &Response_mail{}

	err := xclient.Call(context.Background(), "DelSysMail", args, reply)
	if err != nil {
		log.Debug("mail failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}

	return reply.RetCode
}

// 修改系统邮件
func UpdateAllSysMail(userId int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_mail{
		UserId: userId,
	}

	reply := &Response_mail{}

	err := xclient.Call(context.Background(), "UpdateAllSysMail", args, reply)
	if err != nil {
		log.Debug("mail failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	buf, _ := json.Marshal(reply)
	return string(buf)
}
