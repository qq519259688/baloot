package handler

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/example/proto"
	"context"
	"fmt"
)

var instance *MainHandler

func GetInstance() *MainHandler {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *MainHandler {
	ret := new(MainHandler)
	ret.ctor()
	return ret
}

type MainHandler struct{}

func (h *MainHandler) ctor() {

}

func (d *MainHandler) dump(cmd, param1, param2 string) {
	//log.Debug("DbEngine.Dump %s,%s,%s", cmd, param1, param2)
	switch cmd {
	default:
		log.Release("MainHandler.Dump unhandled cmd %s", cmd)
	}
}

func (h *MainHandler) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}
