package main

import (
	slog "bet24.com/log"
	_ "bet24.com/servers/micros/example/proto"
	"log"
	"strconv"
	"strings"
)

type Test struct {
	Index int
}

func (t *Test) Print() {
	slog.Release("Test.Print %d", t.Index)
}

var chars = []string{"A", "B", "C", "D", "E", "F", "G",
	"H", "I", "J", "K", "L", "M", "N",
	"O", "P", "Q", "R", "S", "T",
	"U", "V", "W", "X", "Y", "Z"}

func getString(s string) string {
	index, err := strconv.Atoi(s)
	if err != nil {
		return ""
	}

	if index >= len(chars) {
		return ""
	}
	return chars[index]
}

func decode(in string) []string {
	if len(in) == 0 {
		return []string{}
	}
	var ret []string

	// 如果第一位是0
	if strings.HasPrefix(in, "0") {
		s := chars[0]
		//ret = append(ret, chars[0])
		next := decode(in[1:])
		for _, v := range next {
			ret = append(ret, s+v)
		}
		return ret
	}

	if len(in) == 1 {
		if value, err := strconv.Atoi(in); err == nil {
			ret = append(ret, chars[value])
		}
		return ret
	}

	_, err := strconv.Atoi(in)
	if err != nil {
		return ret
	}

	s := getString(string(in[0]))
	next := decode(in[1:])
	for _, v := range next {
		ret = append(ret, s+v)
	}

	str2 := getString(in[:2])
	if str2 != "" {
		next := decode(in[2:])
		for _, v := range next {
			ret = append(ret, str2+v)
		}
	}

	return ret
}

func main() {
	logger, err := slog.New("debug", "debug", "log/client", log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	s := "1213407"
	slog.Debug("decode %v", decode(s))

	for {
	}
}
