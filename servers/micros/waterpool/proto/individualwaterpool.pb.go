package proto

import (
	"context"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
)

type IndividualWaterPool_req struct {
	UserId    int
	GameId    int
	GameType  int
	Amount    int
	GenType   string
	BaseScore int
	Gold      int
	IsMatch   bool
	RoomType  int
	RoomID    int
	Param     int
}

type IndividualWaterPoolGrantRecord_req struct {
	UserID    int
	BeginTime int
	EndTime   int
	PageIndex int
	PageSize  int
}

type IndividualWaterPool_resp struct {
	PoolType  int
	PoolValue int
	PoolProb  int
	GrantRecords
}

// 个人奖池生成方式，目前其他类型还没有确定
const (
	GenerationType_ByHand = iota // 手动生成
)

const (
	GameType_Normal   = 100 // 普通模式
	GameType_Doubling = 101 // 加倍模式
	GameType_Correct  = 102 // 纠错模式
)

const (
	RoomType_Normal   = 1 // 普通
	RoomType_Doubling = 2 // 加倍
	RoomType_Quick    = 3 // 快速
	RoomType_Ladder   = 4 // 排位
	RoomType_Hundred  = 5 // 百人
	RoomType_Slot     = 6 // slot
)

// 生成类型
const (
	GenType_ByHand        = "GenType_ByHand"        // 手动生成
	GenType_NewUserSigned = "GenType_NewUserSigned" // 新用户注册
)

// 玩家个人奖池信息
type UserWaterPoolInfo struct {
	UserID    int // 用户ID
	PoolType  int // 调控模式
	PoolValue int // 奖池数据
}

type UserWaterPoolInfoGrantRecord struct {
	Rid       int
	UserID    int
	NickName  string
	PoolValue int
	WantValue int
	GenType   string
	RoomType  int
	RoomName  string
	Crdate    string
}

type GrantRecords struct {
	RecordCount int                            // 记录数
	List        []UserWaterPoolInfoGrantRecord // 返回的数据
}

// 用户登录，创建用户信息
func AddUser(userId int) {
	xclient := getClient()

	args := &IndividualWaterPool_req{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "AddUser", args, nil)
	if err != nil {
		log.Debug("individualWaterPool.AddUser failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

// 用户离线，删除用户数据
func RemoveUser(userId int) {
	xclient := getClient()

	args := &IndividualWaterPool_req{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "RemoveUser", args, nil)
	if err != nil {
		log.Debug("individualWaterPool.RemoveUser failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

// 获取个人奖池类型，是正向还是负向的体验
func GetUserPoolType(userId, gameId, gameType, baseScore, gold int, isMatch bool, param ...int) (int, int) {
	xclient := getClient()
	value := 0
	if len(param) != 0 {
		value = param[0]
	}
	args := &IndividualWaterPool_req{
		UserId:    userId,
		GameId:    gameId,
		GameType:  gameType,
		BaseScore: baseScore,
		Gold:      gold,
		IsMatch:   isMatch,
		Param:     value,
	}

	reply := &IndividualWaterPool_resp{}

	err := xclient.Call(context.Background(), "GetUserPoolType", args, reply)
	if err != nil {
		log.Debug("individualWaterPool.GetUserPoolType failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, 0
	}

	return reply.PoolType, reply.PoolProb
}

// 给玩家发放奖池
func GrantUserNewWaterPool(userId, value int, genType string) {
	xclient := getClient()

	args := &IndividualWaterPool_req{
		UserId:  userId,
		Amount:  value,
		GenType: genType,
	}

	err := xclient.Call(context.Background(), "GrantUserNewWaterPool", args, nil)
	if err != nil {
		log.Debug("individualWaterPool.GrantUserNewWaterPool failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

// 玩家游戏后更新玩家奖池，供游戏内调用
func UpdataUserWaterPool(userId, amount int, genType string, roomType int, roomID int) {
	xclient := getClient()
	args := &IndividualWaterPool_req{
		UserId:   userId,
		Amount:   amount,
		GenType:  genType,
		RoomType: roomType,
		RoomID:   roomID,
	}

	err := xclient.Call(context.Background(), "UpdataUserWaterPool", args, nil)
	if err != nil {
		log.Debug("individualWaterPool.UpdataUserWaterPool failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

// 获取玩家奖池数据,数据库查询使用
func GetUserWaterPool(userId int) int {
	xclient := getClient()

	args := &IndividualWaterPool_req{
		UserId: userId,
	}

	reply := &IndividualWaterPool_resp{}

	err := xclient.Call(context.Background(), "GetUserWaterPool", args, reply)
	if err != nil {
		log.Debug("individualWaterPool.GetUserWaterPool failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.PoolValue
}

func GetUserWaterPoolGrantRecords(userId, beginTime, endTime, pageIndex, pageSize int) GrantRecords {
	xclient := getClient()

	args := &IndividualWaterPoolGrantRecord_req{
		UserID:    userId,
		BeginTime: beginTime,
		EndTime:   endTime,
		PageIndex: pageIndex,
		PageSize:  pageSize,
	}

	reply := &IndividualWaterPool_resp{}

	err := xclient.Call(context.Background(), "GetUserWaterPoolGrantRecords", args, reply)
	if err != nil {
		log.Debug("individualWaterPool.GetUserWaterPoolGrantRecords failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.GrantRecords
}
