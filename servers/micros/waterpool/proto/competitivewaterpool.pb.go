package proto

import (
	"context"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
)

type Inventory_req struct {
	GameID            int    // 游戏id
	RoomName          string // 房间名称
	RoomType          int    // 房间类型
	InventoryValue    int    // 实时库存值
	MinInventoryValue int    // 库存取值下限
	MaxInventoryValue int    // 库存取值上限
	MaxControlRate    int    // 控制率上限
	ReduceValue       int    // 减少值
}

type Inventory_resp struct {
	InventoryType int // 库存类型
	InventoryList
}

// 库存信息
type InventoryInfo struct {
	GameID            int     // 游戏ID
	RoomName          string  // 房间名称
	RoomType          int     // 房间类型
	InventoryValue    int     // 实时库存值
	ControlRate       float64 // 实时控制率
	MinInventoryValue int     // 库存取值下限
	MaxInventoryValue int     // 库存取值上限
	MaxControlRate    int     // 控制率上限
}

// 库存列表
type InventoryList struct {
	Count int             // 库存数
	List  []InventoryInfo // 库存列表数据
}

// 获取库存类型，用于服务游戏系统机器人
func GetInventoryType(gameId int, roomName string) int {
	xclient := getClient()

	args := &Inventory_req{
		GameID:   gameId,
		RoomName: roomName,
	}

	reply := &Inventory_resp{}

	err := xclient.Call(context.Background(), "GetInventoryType", args, reply)
	if err != nil {
		log.Debug("inventory.GetInventoryType failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}

	return reply.InventoryType
}

// 减少库存值
func ReduceInventoryValue(gameId int, roomName string, reduceValue, roomType int) {
	xclient := getClient()

	args := &Inventory_req{
		GameID:      gameId,
		RoomName:    roomName,
		RoomType:    roomType,
		ReduceValue: reduceValue,
	}

	err := xclient.Call(context.Background(), "ReduceInventoryValue", args, nil)
	if err != nil {
		log.Debug("inventory.ReduceInventoryValue failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

// 后台查询获取库存列表
func GetInventoryList(gameId int) InventoryList {
	xclient := getClient()

	args := &Inventory_req{
		GameID: gameId,
	}
	reply := &Inventory_resp{}

	err := xclient.Call(context.Background(), "GetInventoryList", args, reply)
	if err != nil {
		log.Debug("inventory.GetInventoryList failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.InventoryList
}

// 后台修改库存各参数数据
func UpdateInventoryList(gameId int, roomName string, roomType, value, minValue, maxValue, maxRate int) InventoryList {
	xclient := getClient()

	args := &Inventory_req{
		GameID:            gameId,
		RoomName:          roomName,
		RoomType:          roomType,
		InventoryValue:    value,
		MinInventoryValue: minValue,
		MaxInventoryValue: maxValue,
		MaxControlRate:    maxRate,
	}

	reply := &Inventory_resp{}

	err := xclient.Call(context.Background(), "UpdateInventoryList", args, reply)
	if err != nil {
		log.Debug("inventory.UpdateInventoryList failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.InventoryList
}
