package proto

import (
	"context"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "waterpool"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name     string
	UserGold int
	Amount   int
	GameId   int
	IsSlots  bool
	RoomName string
}

type Response struct {
	Data      string
	RetCode   int
	WaterType int
	WaterProb int
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("waterpool failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

// 调控模式
const (
	PoolControl_Invalid = -1
	PoolControl_Normal  = 0 // 0不调控
	PoolControl_Lose    = 1 // 1扣减
	PoolControl_Win     = 2 // 2放水
)

func AddBet(userGold int, betAmount int, isSlots bool, gameId int, param ...string) {
	xclient := getClient()
	//defer xclient.Close()
	roomName := ""
	if len(param) > 0 {
		roomName = param[0]
	}

	args := &Request{
		UserGold: userGold,
		Amount:   betAmount,
		GameId:   gameId,
		IsSlots:  isSlots,
		RoomName: roomName,
	}

	err := xclient.Call(context.Background(), "AddBet", args, nil)
	if err != nil {
		log.Debug("waterpool.AddBet failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func ReducePool(userGold int, amount int, isSlots bool, gameId int, param ...string) {
	xclient := getClient()
	roomName := ""
	if len(param) > 0 {
		roomName = param[0]
	}
	args := &Request{
		UserGold: userGold,
		Amount:   amount,
		GameId:   gameId,
		IsSlots:  isSlots,
		RoomName: roomName,
	}

	err := xclient.Call(context.Background(), "ReducePool", args, nil)
	if err != nil {
		log.Debug("waterpool.ReducePool failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func GetControlType(userGold int, isSlots bool, gameId int) int {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserGold: userGold,
		GameId:   gameId,
		IsSlots:  isSlots,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetControlType", args, reply)
	if err != nil {
		log.Debug("waterpool.GetControlType failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.RetCode
}

func GetControlProb(userGold int, isSlots bool, gameId int, roomName string) (int, int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserGold: userGold,
		GameId:   gameId,
		IsSlots:  isSlots,
		RoomName: roomName,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetControlProb", args, reply)
	if err != nil {
		log.Debug("waterpool.GetControlProb failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, 0
	}
	return reply.WaterType, reply.WaterProb
}
