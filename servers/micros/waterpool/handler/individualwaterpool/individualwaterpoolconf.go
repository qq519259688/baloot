package individualwaterpool

import (
	"encoding/json"
	"os"
	"time"

	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "individualwaterpool_config"

// 游戏系数
type GameCoefficient struct {
	GameId      int     // 游戏Id
	GameType    int     // 游戏类型
	BaseScore   int     // 房间底分
	MinValue    int     // 保底分数
	Coefficient float64 // 系数值
	IsMatch     bool    // 是否比赛场
}

func (g *GameCoefficient) dump() {
	log.Release("GameId[%d] GameType[%d] BaseScore[%d] minValue[%d]Coefficient[%v] IsMatch[%v]",
		g.GameId, g.GameType, g.BaseScore, g.MinValue, g.Coefficient, g.IsMatch)
}

// 个人奖池系数配置
type IndividualPoolConfig struct {
	ConstantK        int               // 常量系数K
	MinProb          int               // 保底概率
	MaxProb          int               // 概率上限
	GameCoefficients []GameCoefficient // 所有游戏系数
}

func (h *IndividualPoolConfig) getGameCoefficient(gameId, gameType, baseScore int, isMatch bool) (float64, int) {
	for i := 0; i < len(h.GameCoefficients); i++ {
		if h.GameCoefficients[i].GameId == gameId &&
			isMatch == h.GameCoefficients[i].IsMatch &&
			h.GameCoefficients[i].GameType == gameType &&
			((h.GameCoefficients[i].BaseScore == baseScore && !isMatch) || isMatch) {
			return h.GameCoefficients[i].Coefficient, h.GameCoefficients[i].MinValue
		}
	}
	return 0, 0
}

func (h *IndividualPoolConfig) dump() {
	log.Release("ConstantK[%d] MinProb[%d] MaxProb[%d]", h.ConstantK, h.MinProb, h.MaxProb)
	log.Release("GameCoefficients:")
	for _, g := range h.GameCoefficients {
		g.dump()
	}
}

func (wpm *waterPoolMgr) loadConfig() {
	wpm.loadRedisConfig()
	go time.AfterFunc(5*time.Minute, wpm.loadConfig)
}

func (wpm *waterPoolMgr) loadRedisConfig() {
	configString := platformconfig.GetConfig(config_key)
	if configString == "" {
		data, err := os.ReadFile("serviceconf/individualwaterpool_extra.json")
		if err != nil {
			log.Release("IndividualWaterPool.readConf read individualwaterpool_extra failed")
			return
		}
		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(config_key, configString)
		}
	}
	err := json.Unmarshal([]byte(configString), &wpm.config)
	if err != nil {
		log.Release("IndualWaterPool.readConf Unmarshal failed err:%v", err)
	}
}
