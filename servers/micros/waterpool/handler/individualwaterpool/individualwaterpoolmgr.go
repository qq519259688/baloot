package individualwaterpool

import (
	"math/rand"
	"strconv"
	"sync"

	"bet24.com/log"
	pb "bet24.com/servers/micros/waterpool/proto"
)

var mgr *waterPoolMgr

type waterPoolMgr struct {
	lock     *sync.RWMutex
	userList map[int]*userWaterPool // 个人奖池信息
	config   IndividualPoolConfig
}

func newWaterPoolManager() *waterPoolMgr {
	mgr = new(waterPoolMgr)
	mgr.lock = &sync.RWMutex{}
	mgr.userList = make(map[int]*userWaterPool)

	mgr.loadConfig()
	return mgr
}

// 用户进入，创建用户信息
func (wpm *waterPoolMgr) addUser(userId int) {
	wpm.lock.RLock()
	_, ok := wpm.userList[userId]
	wpm.lock.RUnlock()
	if ok {
		return
	}

	wpm.lock.Lock()
	wpm.userList[userId] = newUserWaterPool(userId)
	wpm.lock.Unlock()
}

// 将用户标记为离线
func (wpm *waterPoolMgr) removeUser(userId int) {
	wpm.lock.Lock()
	defer wpm.lock.Unlock()
	delete(wpm.userList, userId)
}

func (wpm *waterPoolMgr) getUserPoolType(userId, gameId, gameType, baseScore, gold int, isMatch bool, param int) (int, int) {
	if wpm.config.ConstantK <= 0 || baseScore <= 0 || gold <= 0 {
		return pb.PoolControl_Normal, 0
	}
	wpm.lock.RLock()
	u, ok := wpm.userList[userId]
	wpm.lock.RUnlock()
	if !ok {
		wpm.lock.Lock()
		wpm.userList[userId] = newUserWaterPool(userId)
		wpm.lock.Unlock()

		wpm.lock.RLock()
		u, ok = wpm.userList[userId]
		wpm.lock.RUnlock()
		if !ok {
			return pb.PoolControl_Normal, 0
		}
	}

	if u.poolType <= pb.PoolControl_Normal || u.poolValue == 0 {
		return pb.PoolControl_Normal, 0
	}

	gameCoefficient, minValue := wpm.config.getGameCoefficient(gameId, gameType, baseScore, isMatch)
	gameCoefficient *= 10000
	poolValue := u.poolValue
	if poolValue < 0 {
		poolValue = -poolValue
	}
	if poolValue < minValue {
		return pb.PoolControl_Normal, 0
	}
	value := baseScore
	if param > 0 {
		value = param
	}
	prob := (poolValue*value*(int(gameCoefficient)))/(gold*wpm.config.ConstantK) + wpm.config.MinProb*100
	if prob > wpm.config.MaxProb*100 && wpm.config.MaxProb > 0 {
		prob = wpm.config.MaxProb * 100
	}

	r := rand.Intn(10000)
	if r >= prob {
		return pb.PoolControl_Normal, 0
	}

	if u.poolType == pb.PoolControl_Lose {
		prob = -prob
	}

	return u.poolType, prob
}

func (wpm *waterPoolMgr) grantNewWaterPool(userId, value int, genType string) {
	wpm.lock.RLock()
	u, ok := wpm.userList[userId]
	wpm.lock.RUnlock()
	if !ok {
		wpm.lock.Lock()
		wpm.userList[userId] = newUserWaterPool(userId)
		wpm.lock.Unlock()

		wpm.lock.RLock()
		u, ok = wpm.userList[userId]
		wpm.lock.RUnlock()
		if !ok {
			return
		}
	}

	u.grantNewWaterPool(value, genType)
	if value > 0 {
		go trans_updataTotalWaterPoolInfo(value, 0, 0, 0, value)
	} else {
		go trans_updataTotalWaterPoolInfo(0, 0, -value, 0, -value)
	}
}

func (wpm *waterPoolMgr) updataUserWaterPool(userId, amount int, genType string, roomType, roomID int) {
	if amount == 0 {
		return
	}
	wpm.lock.RLock()
	u, ok := wpm.userList[userId]
	wpm.lock.RUnlock()
	if !ok {
		wpm.lock.Lock()
		wpm.userList[userId] = newUserWaterPool(userId)
		wpm.lock.Unlock()

		wpm.lock.RLock()
		u, ok = wpm.userList[userId]
		wpm.lock.RUnlock()
		if !ok {
			return
		}
	}
	u.updataWaterPool(amount, genType, roomType, roomID)
}

func (wpm *waterPoolMgr) getUserWaterPool(userId int) int {
	wpm.lock.RLock()
	u, ok := wpm.userList[userId]
	wpm.lock.RUnlock()
	if !ok {
		wpm.lock.Lock()
		wpm.userList[userId] = newUserWaterPool(userId)
		wpm.lock.Unlock()

		wpm.lock.RLock()
		u, ok = wpm.userList[userId]
		wpm.lock.RUnlock()
		if !ok {
			return 0
		}
	}
	return u.poolValue
}

func (wpm *waterPoolMgr) getUserWaterPoolGrantRecords(userId, beginTime, endTime, pageIndex, pageSize int) pb.GrantRecords {
	return trans_getUserWaterPoolInfoGrantRecord(userId, beginTime, endTime, pageIndex, pageSize)
}

func (wpm *waterPoolMgr) dumpUser(param string) {
	userId, _ := strconv.Atoi(param)
	wpm.lock.RLock()
	u, ok := wpm.userList[userId]
	wpm.lock.RUnlock()
	if !ok {
		return
	}

	log.Release("UserId[%d] poolType[%s] poolValue[%d]", u.userId, u.getPoolTypeDesc(), u.poolValue)
}
