package individualwaterpool

import (
	pb "bet24.com/servers/micros/waterpool/proto"
)

const (
	PoolControl_NormalValue = 0
)

type userWaterPool struct {
	userId    int // 用户ID
	poolType  int // 调控模式
	poolValue int // 奖池数据
}

func newUserWaterPool(userId int) *userWaterPool {
	user := new(userWaterPool)
	user.userId = userId
	user.loadDataFromDB()
	return user
}

func (u *userWaterPool) loadDataFromDB() {
	usr := trans_getUserWaterPoolInfo(u.userId)
	u.poolType = usr.PoolType
	u.poolValue = usr.PoolValue
}

func (u *userWaterPool) grantNewWaterPool(value int, genType string) {
	u.poolValue += value
	if u.poolValue == PoolControl_NormalValue {
		u.poolType = pb.PoolControl_Normal
	} else if u.poolValue < PoolControl_NormalValue {
		u.poolType = pb.PoolControl_Lose
	} else {
		u.poolType = pb.PoolControl_Win
	}
	go trans_grantIndividualWaterPoolRecord(u.userId, u.poolValue, value, genType, 0, 0)
	go trans_updataUserWaterPoolInfo(u.userId, u.poolType, u.poolValue)
}

func (u *userWaterPool) updataWaterPool(amount int, genType string, roomType, roomID int) {
	execAmount := amount
	if u.poolType == pb.PoolControl_Win {
		if execAmount < 0 {
			execAmount = 0
		}
		u.poolValue -= amount
		go trans_updataTotalWaterPoolInfo(0, execAmount, 0, 0, 0)
	} else {
		if execAmount > 0 {
			execAmount = 0
		} else {
			execAmount = -execAmount
		}
		u.poolValue -= amount
		go trans_updataTotalWaterPoolInfo(0, 0, 0, execAmount, 0)
	}

	if u.poolType == pb.PoolControl_Lose && u.poolValue >= PoolControl_NormalValue {
		u.poolType = pb.PoolControl_Normal
		if u.poolValue > PoolControl_NormalValue {
			u.poolType = pb.PoolControl_Win
		}
	}

	if u.poolType == pb.PoolControl_Win && u.poolValue <= PoolControl_NormalValue {
		u.poolType = pb.PoolControl_Normal
		if u.poolValue < PoolControl_NormalValue {
			u.poolType = pb.PoolControl_Lose
		}
	}

	go trans_updataUserWaterPoolInfo(u.userId, u.poolType, u.poolValue)
	go trans_grantIndividualWaterPoolRecord(u.userId, u.poolValue, -amount, genType, roomType, roomID)
}

func (u *userWaterPool) getPoolTypeDesc() string {
	switch u.poolType {
	case pb.PoolControl_Lose:
		return "Lose"
	case pb.PoolControl_Win:
		return "Win"
	case pb.PoolControl_Normal:
		return "Normal"
	}

	return "Invild"
}
