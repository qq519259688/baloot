package individualwaterpool

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/waterpool/proto"
)

func Run() {
	mgr = newWaterPoolManager()
}

func Dump(cmd, param1 string) {
	switch cmd {
	case "user":
		mgr.dumpUser(param1)
	case "config":
		mgr.config.dump()
	case "updata":
		mgr.loadRedisConfig()
	default:
		log.Release("individual.Dump unhandled cmd %s", cmd)
	}
}

// 用户进入，创建用户信息
func AddUser(userId int) {
	mgr.addUser(userId)
}

// 将用户标记为离线
func RemoveUser(userId int) {
	mgr.removeUser(userId)
}

func GetUserPoolType(userId, gameId, gameType, baseScore, gold int, isMatch bool, param int) (int, int) {
	return mgr.getUserPoolType(userId, gameId, gameType, baseScore, gold, isMatch, param)
}

func GrantUserNewWaterPool(userId, value int, genType string) {
	mgr.grantNewWaterPool(userId, value, genType)
}

func UpdataUserWaterPool(userId, amount int, genType string, roomType, roomID int) {
	mgr.updataUserWaterPool(userId, amount, genType, roomType, roomID)
}

func GetUserWaterPool(userId int) int {
	return mgr.getUserWaterPool(userId)
}

func GetUserWaterPoolGrantRecords(userId, beginTime, endTime, pageIndex, pageSize int) pb.GrantRecords {
	return mgr.getUserWaterPoolGrantRecords(userId, beginTime, endTime, pageIndex, pageSize)
}
