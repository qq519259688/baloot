package individualwaterpool

import (
	"encoding/json"
	"strconv"

	"bet24.com/servers/common"

	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/waterpool/proto"
)

// 获取玩家
func trans_getUserWaterPoolInfo(userId int) pb.UserWaterPoolInfo {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_WaterPool_GetUserInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)

	var usr []pb.UserWaterPoolInfo
	err := json.Unmarshal([]byte(jsonData), &usr)
	if err != nil {
		log.Error("transaction.trans_getUserWaterPoolInfo json unmarshal UserID=%d err %v", userId, err)
	}

	if len(usr) <= 0 {
		return pb.UserWaterPoolInfo{}
	}

	return usr[0]
}

func trans_grantIndividualWaterPoolRecord(userId, poolValue, addValue int, genType string, roomType, roomID int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_WaterPool_GrantRecord")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@PoolValue", database.AdParamInput, database.AdBigint, 8, poolValue)
	statement.AddParamter("@WantValue", database.AdParamInput, database.AdBigint, 8, addValue)
	statement.AddParamter("@GenType", database.AdParamInput, database.AdVarChar, 32, genType)
	statement.AddParamter("@RoomType", database.AdParamInput, database.AdInteger, 4, roomType)
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdVarChar, 32, strconv.Itoa(roomID))
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}

func trans_updataUserWaterPoolInfo(userId, poolType, poolValue int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_WaterPool_UpdateUserInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@PoolType", database.AdParamInput, database.AdInteger, 4, poolType)
	statement.AddParamter("@PoolValue", database.AdParamInput, database.AdBigint, 8, poolValue)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}

func trans_updataTotalWaterPoolInfo(positiveGenAmount, positiveExecAmount, nagetiveGenAmount, nagetiveExecAmount, grantAmount int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_WaterPool_UpdateTotalInfo")
	statement.AddParamter("@PositiveGenAmount", database.AdParamInput, database.AdBigint, 8, positiveGenAmount)
	statement.AddParamter("@PositiveExecAmount", database.AdParamInput, database.AdBigint, 8, positiveExecAmount)
	statement.AddParamter("@NagetiveGenAmount", database.AdParamInput, database.AdBigint, 8, nagetiveGenAmount)
	statement.AddParamter("@NagetiveExecAmount", database.AdParamInput, database.AdBigint, 8, nagetiveExecAmount)
	statement.AddParamter("@GrantAmount", database.AdParamInput, database.AdBigint, 8, grantAmount)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}

func trans_getUserWaterPoolInfoGrantRecord(userId, beginTime, endTime, pageIndex, pageSize int) pb.GrantRecords {
	recordCount := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_WaterPool_GetRecordList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, common.TimeStampToString(int64(beginTime)))
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, common.TimeStampToString(int64(endTime)))
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)

	rowLen := len(retRows)
	var records pb.GrantRecords
	if rowLen <= 0 {
		return records
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var data pb.UserWaterPoolInfoGrantRecord
			data.Rid = int(ret[0].(int64))
			data.UserID = int(ret[1].(int64))
			data.NickName = ret[2].(string)
			data.PoolValue = int(ret[3].(int64))
			data.WantValue = int(ret[4].(int64))
			data.GenType = ret[5].(string)
			data.Crdate = ret[6].(string)
			data.RoomType = int(ret[7].(int64))
			data.RoomName = ret[8].(string)
			records.List = append(records.List, data)
		}
	}

	if records.List == nil {
		records.List = make([]pb.UserWaterPoolInfoGrantRecord, 0)
	}
	recordCount = int(retRows[rowLen-1][0].(int64))
	records.RecordCount = recordCount
	return records
}
