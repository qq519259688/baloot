package bacwaterpool

import (
	"encoding/json"
	//"fmt"
	"os"
	"strconv"
	"time"

	"bet24.com/log"
	//"bet24.com/redis"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
	pb "bet24.com/servers/micros/waterpool/proto"
)

var mgr *bacWaterPoolMgr

type bacWaterPoolMgr struct {
	Pools      []*poolInfo
	lastConfig string
}

const config_key = "waterpool_config"
const refresh_config_sec = 600
const sync_sec = 5

func newbacWaterPoolMgr() *bacWaterPoolMgr {
	ret := new(bacWaterPoolMgr)
	ret.readConf()
	//ret.syncWaterPoolValue()
	return ret
}

func (pm *bacWaterPoolMgr) readConf() {
	time.AfterFunc(refresh_config_sec*time.Second, pm.readConf)
	configString := platformconfig.GetConfig(config_key)
	if configString == "" {
		data, err := os.ReadFile("serviceconf/waterpool_extra.json")
		if err != nil {
			log.Release("WaterPool.readConf read waterpool_extra failed")
			return
		}
		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(config_key, configString)
		}
	}

	if configString == pm.lastConfig {
		return
	}

	pm.lastConfig = configString
	var configs []poolConfig
	err := json.Unmarshal([]byte(configString), &configs)
	if err != nil {
		log.Release("WaterPool.readConf Unmarshal failed err:%v", err)
	}
	pools := trans_getWaterPoolList(0)
	for _, v := range configs {
		p := pm.getPool(v.PoolId)
		if p == nil {
			p = newPoolInfo(v)
			for i := 0; i < pools.Count; i++ {
				if pools.List[i].GameID == p.GameId && pools.List[i].RoomName == p.PoolName {
					p.PoolValue = pools.List[i].InventoryValue
					break
				}
			}
			//pm.saveValueToRedis(p.PoolId, p.PoolValue)
			pm.Pools = append(pm.Pools, p)
		} else {
			// 更新配置
			p.poolConfig = v
			p.sortLevels()
			for i := 0; i < pools.Count; i++ {
				if pools.List[i].GameID == p.GameId && pools.List[i].RoomName == p.PoolName {
					p.PoolValue = pools.List[i].InventoryValue
					break
				}
			}
		}
	}
}

func (pm *bacWaterPoolMgr) syncWaterPoolValue() {
	time.AfterFunc(sync_sec*time.Second, pm.syncWaterPoolValue)
	pools := trans_getWaterPoolList(0)
	for _, v := range pm.Pools {
		for i := 0; i < pools.Count; i++ {
			if pools.List[i].GameID == v.GameId && pools.List[i].RoomName == v.PoolName &&
				pools.List[i].InventoryValue != v.PoolValue {
				go trans_updateWaterPoolList(v.GameId, v.PoolName, v.PoolType, v.PoolValue)
			}
		}
	}
}

func (pm *bacWaterPoolMgr) refreshPoolInfo() {
	pools := trans_getWaterPoolList(0)
	for _, v := range pm.Pools {
		for i := 0; i < pools.Count; i++ {
			if pools.List[i].GameID == v.GameId && pools.List[i].RoomName == v.PoolName {
				v.PoolValue = pools.List[i].InventoryValue
				break
			}
		}
	}
}

func (pm *bacWaterPoolMgr) showPoolInfo(param string) {
	if param == "" {
		for _, v := range pm.Pools {
			v.dump()
		}
	} else {
		id, _ := strconv.Atoi(param)
		for _, v := range pm.Pools {
			if v.PoolId == id {
				v.dump()
			}
		}
	}
}

func (pm *bacWaterPoolMgr) getPool(poolId int) *poolInfo {
	for _, v := range pm.Pools {
		if v.PoolId == poolId {
			return v
		}
	}
	return nil
}

/*func (pm *bacWaterPoolMgr) getRedisKey(poolId int) string {
	return fmt.Sprintf("waterpool:pool%d", poolId)
}

 func (pm *bacWaterPoolMgr) getValueFromRedis(poolId int) int {
// 	return redis.String_GetInt(pm.getRedisKey(poolId))
// }

func (pm *bacWaterPoolMgr) saveValueToRedis(poolId int, value int) {
	redis.String_Set(pm.getRedisKey(poolId), fmt.Sprintf("%d", value))
}
*/

func (pm *bacWaterPoolMgr) addBet(userGold int, betAmount int, isSlots bool, gameId int, poolName string) {
	for _, v := range pm.Pools {
		if v.addBet(betAmount, userGold, isSlots, gameId, poolName) {
			//pm.saveValueToRedis(v.PoolId, v.PoolValue)
		}
	}
}

func (pm *bacWaterPoolMgr) reducePool(userGold int, amount int, isSlots bool, gameId int, poolName string) {
	for _, v := range pm.Pools {
		if v.reducePool(amount, userGold, isSlots, gameId, poolName) {
			//pm.saveValueToRedis(v.PoolId, v.PoolValue)
		}
	}
}

func (pm *bacWaterPoolMgr) getControlType(userGold int, isSlots bool, gameId int) int {
	for _, v := range pm.Pools {
		t := v.getControlType(userGold, isSlots, gameId)
		if t != pb.PoolControl_Invalid {
			log.Release("poolId:%d, poolValue:%d, poolType:%d", v.PoolId, v.PoolValue, t)
			return t
		}
	}

	return pb.PoolControl_Normal
}

func (pm *bacWaterPoolMgr) getControlProb(userGold int, isSlots bool, gameId int, poolName string) (int, int) {
	for _, v := range pm.Pools {
		if v.isConcerned(userGold, isSlots, gameId) && poolName == v.PoolName {
			return v.getControlProb(userGold, isSlots, gameId, poolName)
		}
	}

	return pb.PoolControl_Normal, 0
}

// 修改库存时，修改水池数值
func (pm *bacWaterPoolMgr) updatePool(gameId, waterpoolValue int, poolName string) {
	for _, v := range pm.Pools {
		if v.GameId == gameId && poolName == v.PoolName {
			v.PoolValue = waterpoolValue
			//pm.saveValueToRedis(v.PoolId, v.PoolValue)
			break
		}
	}
}
