package bacwaterpool

func Run() {
	mgr = newbacWaterPoolMgr()
}

func Dump(param1 string) {
	switch param1 {
	case "refresh":
		mgr.refreshPoolInfo()
		return
	}
	mgr.showPoolInfo(param1)
}

func AddBet(userGold int, betAmount int, isSlots bool, gameId int, roomName string) {
	mgr.addBet(userGold, betAmount, isSlots, gameId, roomName)
}

func ReducePool(userGold int, amount int, isSlots bool, gameId int, roomName string) {
	mgr.reducePool(userGold, amount, isSlots, gameId, roomName)
}

func GetControlType(userGold int, isSlots bool, gameId int) int {
	return mgr.getControlType(userGold, isSlots, gameId)
}

func GetControlProb(userGold int, isSlots bool, gameId int, poolName string) (int, int) {
	return mgr.getControlProb(userGold, isSlots, gameId, poolName)
}

func UpdatePool(gameId, waterpoolValue int, poolName string) {
	mgr.updatePool(gameId, waterpoolValue, poolName)
}
