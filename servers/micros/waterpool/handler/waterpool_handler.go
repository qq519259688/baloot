package handler

import (
	"bet24.com/log"

	"bet24.com/servers/micros/waterpool/handler/bacwaterpool"
	"bet24.com/servers/micros/waterpool/handler/competitivewaterpool"
	"bet24.com/servers/micros/waterpool/handler/individualwaterpool"
)

var instance *WaterPool

func GetInstance() *WaterPool {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *WaterPool {
	ret := new(WaterPool)
	ret.ctor()
	return ret
}

type WaterPool struct{}

func (h *WaterPool) ctor() {
	individualwaterpool.Run()
	competitivewaterpool.Run()
	bacwaterpool.Run()
}

func (pm *WaterPool) dump(cmd, param1, param2 string) {
	log.Release("-------------------------------")
	log.Release("WaterPool.dump")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	if cmd == "pool" {
		bacwaterpool.Dump(param1)
	} else if cmd == "inventory" {
		competitivewaterpool.Dump(param1)
	} else {
		individualwaterpool.Dump(cmd, param1)
	}
}
