package competitivewaterpool

import (
	"strconv"

	"bet24.com/database"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/waterpool/proto"
)

func trans_getInventoryList(gameId int) pb.InventoryList {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_GameInventory_GetList")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	var list pb.InventoryList
	list.Count = rowLen
	list.List = []pb.InventoryInfo{}
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var data pb.InventoryInfo
		data.GameID = int(ret[0].(int64))
		data.RoomName = ret[1].(string)
		data.RoomType = int(ret[2].(int64))
		data.InventoryValue = int(ret[3].(int64))
		controlRate, _ := strconv.ParseFloat(string(ret[4].([]byte)), 64)
		data.ControlRate = controlRate
		data.MinInventoryValue = int(ret[5].(int64))
		data.MaxInventoryValue = int(ret[6].(int64))
		data.MaxControlRate = int(ret[7].(int64))
		list.List = append(list.List, data)
	}

	return list
}

func trans_updateInventoryList(info pb.InventoryInfo) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_GameInventory_Update")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, info.GameID)
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdVarChar, 32, info.RoomName)
	statement.AddParamter("@RoomType", database.AdParamInput, database.AdInteger, 4, info.RoomType)
	statement.AddParamter("@InventoryValue", database.AdParamInput, database.AdBigint, 8, info.InventoryValue)
	statement.AddParamter("@ControlRate", database.AdParamInput, database.AdFloat, 8, info.ControlRate)
	statement.AddParamter("@MinInventoryValue", database.AdParamInput, database.AdBigint, 8, info.MinInventoryValue)
	statement.AddParamter("@MaxInventoryValue", database.AdParamInput, database.AdBigint, 8, info.MaxInventoryValue)
	statement.AddParamter("@MaxControlRate", database.AdParamInput, database.AdBigint, 8, info.MaxControlRate)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}

func trans_inventoryChangeRecord(gameID int, roomName string, changeValue, roomType int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_GameInventory_AddRecord")
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameID)
	statement.AddParamter("@RoomName", database.AdParamInput, database.AdNVarChar, 64, roomName)
	statement.AddParamter("@RoomType", database.AdParamInput, database.AdInteger, 4, roomType)
	statement.AddParamter("@InventoryValue", database.AdParamInput, database.AdBigint, 8, changeValue)
	statement.AddParamter("@SysRecover", database.AdParamInput, database.AdInteger, 4, 0)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}
