package competitivewaterpool

import (
	"encoding/json"
	"math/rand"
	"os"
	"sync"
	"time"

	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
	pb "bet24.com/servers/micros/waterpool/proto"
)

const config_key = "inventory_config"

var mgr *competitiveWaterPoolMgr

type competitiveWaterPoolMgr struct {
	inventoryList []pb.InventoryInfo
	lock          *sync.RWMutex
	config        string
}

type Room struct {
	RoomName string
	RoomType int
}

type InventoryConf struct {
	GameID   int
	RoomList []Room
}

func newCompetitiveWaterPoolMgr() *competitiveWaterPoolMgr {
	ret := new(competitiveWaterPoolMgr)
	ret.lock = &sync.RWMutex{}
	var data = trans_getInventoryList(0)
	if data.Count > 0 {
		ret.inventoryList = data.List
	}
	ret.loadConfig()
	return ret
}

func (m *competitiveWaterPoolMgr) loadConfig() {
	m.loadRedisConfig(false)
	go time.AfterFunc(5*time.Minute, m.loadConfig)
}

func (m *competitiveWaterPoolMgr) loadRedisConfig(isRefresh bool) {
	configString := platformconfig.GetConfig(config_key)
	if configString == "" {
		data, err := os.ReadFile("serviceconf/inventory_extra.json")
		if err != nil {
			log.Release("Inventory.readConf read inventory_extra failed")
			return
		}
		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(config_key, configString)
		}
	}
	if m.config == configString && !isRefresh {
		return
	}

	m.config = configString
	var conf []InventoryConf
	err := json.Unmarshal([]byte(configString), &conf)
	if err != nil {
		log.Release("Inventory.readConf Unmarshal failed err:%v", err)
		return
	}
	m.lock.Lock()
	defer m.lock.Unlock()
	for i := 0; i < len(conf); i++ {
		if len(conf[i].RoomList) == 0 {
			continue
		}
		for j := 0; j < len(conf[i].RoomList); j++ {
			bExist := false
			for n := 0; n < len(m.inventoryList); n++ {
				if m.inventoryList[n].GameID == conf[i].GameID && m.inventoryList[n].RoomName == conf[i].RoomList[j].RoomName {
					if m.inventoryList[n].RoomType != conf[i].RoomList[j].RoomType {
						m.inventoryList[n].RoomType = conf[i].RoomList[j].RoomType
					}
					bExist = true
					break
				}
			}

			if !bExist {
				m.inventoryList = append(m.inventoryList, pb.InventoryInfo{
					GameID:            conf[i].GameID,
					RoomName:          conf[i].RoomList[j].RoomName,
					RoomType:          conf[i].RoomList[j].RoomType,
					InventoryValue:    0,
					ControlRate:       0,
					MinInventoryValue: 0,
					MaxInventoryValue: 0,
					MaxControlRate:    0,
				})
			}
		}
	}

	for i := 0; i < len(m.inventoryList); i++ {
		trans_updateInventoryList(m.inventoryList[i])
	}
}

func (m *competitiveWaterPoolMgr) getInventoryType(gameId int, roomName string) int {
	m.lock.RLock()
	defer m.lock.RUnlock()
	var controlRate float64 = 0
	inventoryValue := 0
	for i := 0; i < len(m.inventoryList); i++ {
		if m.inventoryList[i].RoomName == roomName && m.inventoryList[i].GameID == gameId {
			controlRate = m.inventoryList[i].ControlRate
			inventoryValue = m.inventoryList[i].InventoryValue
			break
		}
	}

	if controlRate <= 0 {
		return pb.PoolControl_Normal
	}

	r := rand.Intn(10000)
	prob := int(controlRate * 100)
	if r >= prob {
		return pb.PoolControl_Normal
	}

	if inventoryValue > 0 {
		return pb.PoolControl_Lose
	}

	return pb.PoolControl_Win
}

func (m *competitiveWaterPoolMgr) getControlRate(min, max, value, maxRate int) float64 {
	if max <= min {
		return 0
	}

	inventoryValue := value
	if inventoryValue < 0 {
		inventoryValue = -inventoryValue
	}

	if inventoryValue < min {
		return 0
	}

	rate := float64(maxRate) * float64(inventoryValue-min) / float64(max-min)
	if rate > float64(maxRate) {
		rate = float64(maxRate)
	}
	return rate
}

func (m *competitiveWaterPoolMgr) reduceInventoryValue(gameId int, roomName string, reduceValue, roomType int) {
	if reduceValue == 0 {
		return
	}
	go trans_inventoryChangeRecord(gameId, roomName, -reduceValue, roomType)
	m.lock.Lock()
	defer m.lock.Unlock()
	for i := 0; i < len(m.inventoryList); i++ {
		if m.inventoryList[i].RoomName == roomName && m.inventoryList[i].GameID == gameId && m.inventoryList[i].RoomType == roomType {
			m.inventoryList[i].InventoryValue -= reduceValue
			m.inventoryList[i].ControlRate = m.getControlRate(m.inventoryList[i].MinInventoryValue,
				m.inventoryList[i].MaxInventoryValue, m.inventoryList[i].InventoryValue, m.inventoryList[i].MaxControlRate)
			go trans_updateInventoryList(m.inventoryList[i])
			return
		}
	}

	var info pb.InventoryInfo
	info.RoomName = roomName
	info.RoomType = roomType
	info.InventoryValue = -reduceValue
	info.ControlRate = 0
	info.MinInventoryValue = 0
	info.MaxControlRate = 0
	info.MaxInventoryValue = 0
	m.inventoryList = append(m.inventoryList, info)
	go trans_updateInventoryList(info)
}

func (m *competitiveWaterPoolMgr) getInventoryList(gameId int) pb.InventoryList {
	return trans_getInventoryList(gameId)
}

func (m *competitiveWaterPoolMgr) updateInventoryList(gameId int, roomName string, roomType, inventoryValue, minValue, maxValue, maxRate int) pb.InventoryList {
	m.lock.Lock()
	defer m.lock.Unlock()
	bFound := false
	for i := 0; i < len(m.inventoryList); i++ {
		if m.inventoryList[i].RoomName == roomName && m.inventoryList[i].GameID == gameId && roomType == m.inventoryList[i].RoomType {
			bFound = true
			m.inventoryList[i].InventoryValue = inventoryValue
			m.inventoryList[i].MinInventoryValue = minValue
			m.inventoryList[i].MaxInventoryValue = maxValue
			m.inventoryList[i].MaxControlRate = maxRate
			rate := m.getControlRate(minValue, maxValue, inventoryValue, maxRate)
			m.inventoryList[i].ControlRate = rate
			go trans_updateInventoryList(m.inventoryList[i])
			break
		}
	}

	if !bFound {
		var info pb.InventoryInfo
		info.GameID = gameId
		info.RoomName = roomName
		info.RoomType = roomType
		info.InventoryValue = inventoryValue
		info.MinInventoryValue = minValue
		info.MaxControlRate = maxRate
		info.MaxInventoryValue = maxValue
		rate := m.getControlRate(minValue, maxValue, inventoryValue, maxRate)
		info.ControlRate = rate
		m.inventoryList = append(m.inventoryList, info)
		go trans_updateInventoryList(info)
	}

	var list pb.InventoryList
	list.Count = len(m.inventoryList)
	list.List = append(list.List, m.inventoryList...)

	return list
}

func (m *competitiveWaterPoolMgr) getInventoryValue(gameId, roomType int, roomName string) int {
	for i := 0; i < len(m.inventoryList); i++ {
		if m.inventoryList[i].GameID == gameId && roomType == m.inventoryList[i].RoomType &&
			roomName == m.inventoryList[i].RoomName {
			return m.inventoryList[i].InventoryValue
		}
	}
	return 0
}

func (m *competitiveWaterPoolMgr) dump() {
	m.lock.RLock()
	defer m.lock.RUnlock()
	for i := 0; i < len(m.inventoryList); i++ {
		log.Release("GameID:%d, RoomName:%s, RoomType:%d, InventoryValue:%d, ControlRate:%g, MinInventoryValue:%d, MaxInventoryValue:%d,MaxControlRate:%d",
			m.inventoryList[i].GameID, m.inventoryList[i].RoomName, m.inventoryList[i].RoomType, m.inventoryList[i].InventoryValue, m.inventoryList[i].ControlRate,
			m.inventoryList[i].MinInventoryValue, m.inventoryList[i].MaxInventoryValue, m.inventoryList[i].MaxControlRate)
	}
}
