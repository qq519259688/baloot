package competitivewaterpool

import (
	pb "bet24.com/servers/micros/waterpool/proto"
)

func Run() {
	mgr = newCompetitiveWaterPoolMgr()
}

func Dump(param1 string) {
	switch param1 {
	case "info":
		mgr.dump()
	case "refresh":
		mgr.loadRedisConfig(true)
	}
}

func GetInventoryType(gameId int, roomName string) int {
	return mgr.getInventoryType(gameId, roomName)
}

func ReduceInventoryValue(gameId int, roomName string, reduceValue, roomType int) {
	mgr.reduceInventoryValue(gameId, roomName, reduceValue, roomType)
}

func GetInventoryList(gameId int) pb.InventoryList {
	return mgr.getInventoryList(gameId)
}

func UpdateInventoryList(gameId int, roomName string, roomType, inventoryValue, minInventoryValue, maxInventoryValue,
	maxControlRate int) pb.InventoryList {
	return mgr.updateInventoryList(gameId, roomName, roomType, inventoryValue, minInventoryValue, maxInventoryValue, maxControlRate)
}

func GetInventoryValue(gameId, roomType int, roomName string) int {
	return mgr.getInventoryValue(gameId, roomType, roomName)
}
