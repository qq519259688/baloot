package handler

import (
	"context"

	"bet24.com/servers/micros/waterpool/handler/individualwaterpool"
	pb "bet24.com/servers/micros/waterpool/proto"
)

func (h *WaterPool) AddUser(ctx context.Context, req *pb.IndividualWaterPool_req, rsp *pb.IndividualWaterPool_resp) error {
	individualwaterpool.AddUser(req.UserId)
	return nil
}

func (h *WaterPool) RemoveUser(ctx context.Context, req *pb.IndividualWaterPool_req, rsp *pb.IndividualWaterPool_resp) error {
	individualwaterpool.RemoveUser(req.UserId)
	return nil
}

func (h *WaterPool) GetUserPoolType(ctx context.Context, req *pb.IndividualWaterPool_req, rsp *pb.IndividualWaterPool_resp) error {
	rsp.PoolType, rsp.PoolProb = individualwaterpool.GetUserPoolType(req.UserId, req.GameId, req.GameType, req.BaseScore, req.Gold, req.IsMatch, req.Param)
	return nil
}

func (h *WaterPool) GrantUserNewWaterPool(ctx context.Context, req *pb.IndividualWaterPool_req, rsp *pb.IndividualWaterPool_resp) error {
	individualwaterpool.GrantUserNewWaterPool(req.UserId, req.Amount, req.GenType)
	return nil
}

func (h *WaterPool) UpdataUserWaterPool(ctx context.Context, req *pb.IndividualWaterPool_req, rsp *pb.IndividualWaterPool_resp) error {
	individualwaterpool.UpdataUserWaterPool(req.UserId, req.Amount, req.GenType, req.RoomType, req.RoomID)
	return nil
}

func (h *WaterPool) GetUserWaterPool(ctx context.Context, req *pb.IndividualWaterPool_req, rsp *pb.IndividualWaterPool_resp) error {
	rsp.PoolValue = individualwaterpool.GetUserWaterPool(req.UserId)
	return nil
}

func (h *WaterPool) GetUserWaterPoolGrantRecords(ctx context.Context, req *pb.IndividualWaterPoolGrantRecord_req, rsp *pb.IndividualWaterPool_resp) error {
	rsp.GrantRecords = individualwaterpool.GetUserWaterPoolGrantRecords(req.UserID, req.BeginTime, req.EndTime, req.PageIndex, req.PageSize)
	return nil
}
