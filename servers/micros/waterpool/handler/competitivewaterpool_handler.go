package handler

import (
	"context"

	"bet24.com/servers/micros/waterpool/handler/bacwaterpool"
	"bet24.com/servers/micros/waterpool/handler/competitivewaterpool"
	pb "bet24.com/servers/micros/waterpool/proto"
)

func (h *WaterPool) GetInventoryType(ctx context.Context, req *pb.Inventory_req, rsp *pb.Inventory_resp) error {
	rsp.InventoryType = competitivewaterpool.GetInventoryType(req.GameID, req.RoomName)
	return nil
}

func (h *WaterPool) ReduceInventoryValue(ctx context.Context, req *pb.Inventory_req, rsp *pb.Inventory_resp) error {
	competitivewaterpool.ReduceInventoryValue(req.GameID, req.RoomName, req.ReduceValue, req.RoomType)
	return nil
}

func (h *WaterPool) GetInventoryList(ctx context.Context, req *pb.Inventory_req, rsp *pb.Inventory_resp) error {
	rsp.InventoryList = competitivewaterpool.GetInventoryList(req.GameID)
	return nil
}

func (h *WaterPool) UpdateInventoryList(ctx context.Context, req *pb.Inventory_req, rsp *pb.Inventory_resp) error {
	rsp.InventoryList = competitivewaterpool.UpdateInventoryList(req.GameID, req.RoomName, req.RoomType, req.InventoryValue,
		req.MinInventoryValue, req.MaxInventoryValue, req.MaxControlRate)
	if req.GameID != 0 {
		bacwaterpool.UpdatePool(req.GameID, req.InventoryValue, req.RoomName)
	}
	return nil
}
