package handler

import (
	"context"
	"fmt"

	"bet24.com/servers/micros/waterpool/handler/bacwaterpool"
	pb "bet24.com/servers/micros/waterpool/proto"
)

func (h *WaterPool) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (h *WaterPool) AddBet(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	bacwaterpool.AddBet(req.UserGold, req.Amount, req.IsSlots, req.GameId, req.RoomName)
	return nil
}

func (h *WaterPool) ReducePool(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	bacwaterpool.ReducePool(req.UserGold, req.Amount, req.IsSlots, req.GameId, req.RoomName)
	return nil
}

func (h *WaterPool) GetControlType(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.RetCode = bacwaterpool.GetControlType(req.UserGold, req.IsSlots, req.GameId)
	return nil
}

func (h *WaterPool) GetControlProb(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.WaterType, rsp.WaterProb = bacwaterpool.GetControlProb(req.UserGold, req.IsSlots, req.GameId, req.RoomName)
	return nil
}
