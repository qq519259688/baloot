package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
	"golang.org/x/net/context"
)

const ServiceName = "guess"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	UserId       int                 `json:",omitempty"` // 用户id
	SerialNumber string              `json:",omitempty"` // 赛事流水号
	Title        string              `json:",omitempty"` // 赛事标题
	Result       string              `json:",omitempty"` // 赛事结果
	Status       int                 `json:",omitempty"` // 赛事状态
	Type         int                 `json:",omitempty"` // 赛事类型
	StartAt      string              `json:",omitempty"` // 赛事开始时间
	EndAt        string              `json:",omitempty"` // 赛事结束时间
	ShowStartAt  string              `json:",omitempty"` // 赛事展示开始时间
	ShowEndAt    string              `json:",omitempty"` // 赛事展示截止时间
	TeamId       int                 `json:",omitempty"` // 球队id
	BetId        int                 `json:",omitempty"` // 投注id
	Amount       int                 `json:",omitempty"` // 投注金额
	BetName      string              `json:",omitempty"` // 投注名称
	BetOdds      float64             `json:",omitempty"` // 投注赔率
	PageIndex    int                 `json:",omitempty"` // 页索引
	PageSize     int                 `json:",omitempty"` // 页大小
	Rid          int                 `json:",omitempty"` // 标识
	OpUser       `json:",omitempty"` // 操作员
	Msg          string
	Data         string
}

type Response struct {
	Data string
}

type Response_Match struct {
	Match
	UserBets []UserBet
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

// 消息处理
func OnGuessMsg(userId int, msg, data string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
		Msg:    msg,
		Data:   data,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "OnGuessMsg", args, reply)
	if err != nil {
		log.Debug("OnGuessMsg failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return reply.Data
}
