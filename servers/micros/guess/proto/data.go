package proto

// 竞猜配置
type GuessCfg struct {
	BetAmount   []int  // 投注金额
	MailTitle   string // 邮件标题
	MailSource  string // 邮件源
	MailContent string // 邮件内容
}

const (
	MatchStatus_Invalid = iota // 0=无效状态
	MatchStatus_Open           // 1=开启状态
	MatchStatus_End            // 2=结束状态（停止下注）
	MatchStatus_Award          // 3=已派奖
	MatchStatus_Max
)

const (
	MatchType_All     = iota // 0=所有赛事
	MatchType_Show           // 1=展示的赛事(不可投注)
	MatchType_Bet            // 2=正在进行的赛事(可以投注)
	MatchType_End            // 3=结束的赛事(已出结果)
	MatchType_Preheat        // 4=预热的赛事
)

// RetMsg 返回消息
type RetMsg struct {
	RetCode int
	Message string
}

// Match 赛事
type Match struct {
	SerialNumber string // 流水号
	Title        string // 标题(如：让球)
	Status       int    // 状态（0=无效状态 1=开启状态  2=结束状态）
	Teams        []Team // 球队（主、客队）
	Bets         []Bet  // 投注
	Result       string // 赛事结果
	StartAt      int    `json:",omitempty"` // 开始时间戳
	EndAt        int    `json:",omitempty"` // 结束时间戳
	ShowStartAt  int    `json:",omitempty"` // 展示开始时间戳
	ShowEndAt    int    `json:",omitempty"` // 展示结束时间戳
}

// Team 球队
type Team struct {
	Rid       int    // 记录ID
	Id        int    // 球队ID
	Name      string // 球队名称
	Icon      string // 球队图标
	ShortName string `json:",omitempty"` // 球队简称
}

// 操作员
type OpUser struct {
	OpUserId   int    `json:",omitempty"` // 操作员id
	OpUserName string `json:",omitempty"` // 操作员名称
	IpAddress  string `json:",omitempty"` // ip地址
}

// Bet 投注
type Bet struct {
	Id     int     // 投注id
	Name   string  // 投注名称
	Odds   float64 // 投注赔率
	Amount int     // 投注金额
	IsWin  bool    // 是否胜
}

// BetRecord 投注记录
type BetRecord struct {
	SerialNumber string `json:"-"` // 流水号
	BetId        int    `json:"-"` // 投注id
	BetName      string // 投注名称
	BetAmount    int    // 投注金额
	ResultAmount int    // 派奖金额
	Title        string // 标题(如：让球)
	Status       int    // 状态（0=无效状态 1=开启状态  2=结束状态）
	Teams        []Team // 球队（主、客队）
	Result       string // 赛事结果
	StartTime    int    // 开始时间
}

// 下注结算
type BetSettle struct {
	Rid          int // 标识id
	UserId       int // 用户ID
	BetAmount    int // 下注金额
	ResultAmount int // 派奖金额
}

// UserBet 用户投注
type UserBet struct {
	BetId     int // 投注id
	BetAmount int // 投注金额
}
