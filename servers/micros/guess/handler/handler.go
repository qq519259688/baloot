package handler

import (
	"bet24.com/log"
	"bet24.com/servers/micros/guess/handler/match"
	"bet24.com/servers/micros/guess/handler/team"
	pb "bet24.com/servers/micros/guess/proto"
	"golang.org/x/net/context"
	"math/rand"
	"time"
)

var instance *guess

type guess struct {
	logPrint bool // 日志打印
}

func GetInstance() *guess {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func newHandler() *guess {
	ret := new(guess)
	ret.ctor()
	return ret
}

func (g *guess) ctor() {
	rand.Seed(time.Now().Unix())

	// 球队
	team.Run()

	// 赛事
	match.Run()
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func (this *guess) dump(cmd, param1, param2 string) {
	switch cmd {
	case "match": // 打印赛事
		match.Dump(param1, param2)
	case "team": // 打印球队
		team.Dump(param1, param2)
	case "log":
		if param1 == "open" {
			this.logPrint = true
			log.Debug("消息指令跟踪已开启...")
		} else {
			this.logPrint = false
			log.Debug("消息指令跟踪已关闭！")
		}
	case "exit":
		// do nothing
	default:
		log.Release("guess.Dump unhandled cmd %s", cmd)
	}
}

// 指令消息处理
func (this *guess) OnGuessMsg(ctx context.Context, req *pb.Request, resp *pb.Response) error {
	resp.Data = this.messageHandler(req.UserId, req.Msg, req.Data)

	// 打印日志
	if this.logPrint {
		switch req.Msg {
		case "guessGetMatchList":
			log.Debug("OnAudioRoomMsg request=%+v ==> resp.Data=%s", req, req.Data)
		default:
			log.Debug("OnAudioRoomMsg request=%+v", req)
		}
	}

	return nil
}

// 消息分发处理器
func (this *guess) messageHandler(userId int, msg, data string) string {
	switch msg {
	case "guessGetConfig": // 配置
		return match.GetConfig(userId, data)

	case "guessGetTeam": // 获取球队
		return team.GetTeamJson(userId, data)
	case "guessGetTeamList": // 球队列表
		return team.GetTeamList(userId, data)
	case "guessAddTeam": // 添加球队
		return team.AddTeam(userId, data)
	case "guessUpdateTeam": // 修改球队
		return team.UpdateTeam(userId, data, match.RefreshMatchTeam)

	case "guessGetMatchList": // 获取赛事列表
		return match.GetMatchList(userId, data)
	case "guessGetMatchInfo": // 获取赛事信息
		return match.GetMatchInfo(userId, data)
	case "guessBet": // 投注
		return match.Bet(userId, data)

	case "guessSetResult": // 设置赛事结果
		return match.SetResult(userId, data)
	case "guessAward": // 派奖
		return match.Award(userId, data)

	case "guessAddMatch": // 添加赛事
		return match.AddMatch(userId, data)
	case "guessUpdateMatch": // 修改赛事
		return match.UpdateMatch(userId, data)

	case "guessAddMatchTeam": // 添加赛事球队
		return match.AddMatchTeam(userId, data)
	case "guessUpdateMatchTeam": // 修改赛事球队
		return match.UpdateMatchTeam(userId, data)

	case "guessAddMatchBet": // 添加赛事投注选项
		return match.AddMatchBet(userId, data)
	case "guessUpdateMatchBet": // 修改赛事投注选项
		return match.UpdateMatchBet(userId, data)

	case "guessSetMatchOpen": // 设置赛事开启
		return match.SetMatchOpen(userId, data)
	case "guessGetUserBetRecordList": // 用户投注记录
		return match.GetUserBetRecordList(userId, data)
	default:
		// do nothing
	}

	log.Error("handler.messageHandler unprocessed userId=%d msg=%s data=%s", userId, msg, data)
	return ""
}
