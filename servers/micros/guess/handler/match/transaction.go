package match

import (
	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/guess/proto"
	"encoding/json"
	"strconv"
)

// 获取赛事列表
func trans_GetMatchList() []string {
	var list []string
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Guess_GetMatchList")
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		sn := ret[0].(string)
		list = append(list, sn)
	}
	return list
}

// 获取赛事信息
func trans_GetMatchInfo(serialNumber string) pb.Match {
	var list []pb.Match
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Guess_GetMatchInfo")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	jsonData := dbengine.Execute(sqlString)
	if len(jsonData) > 0 {
		if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
			log.Release("trans_GetMatchInfo json unmarshal serialNumber=%s err %v", serialNumber, err)
		}
	}

	if len(list) > 0 {
		return list[0]
	}

	return pb.Match{}
}

// 获取赛事球队列表
func trans_GetMatchTeamList(serialNumber string) []pb.Team {
	var list []pb.Team
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Guess_GetMatchTeamList")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		team := pb.Team{
			Rid: int(ret[0].(int64)),
			Id:  int(ret[1].(int64)),
		}
		list = append(list, team)
	}
	return list
}

// 获取赛事投注列表
func trans_GetMatchBetList(serialNumber string) []pb.Bet {
	var list []pb.Bet
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Guess_GetMatchBetList")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var b pb.Bet
		b.Id = int(ret[0].(int64))
		b.Name = ret[1].(string)
		odds := string(ret[2].([]byte))
		b.Odds, _ = strconv.ParseFloat(odds, 64)
		b.Amount = int(ret[3].(int64))
		b.IsWin = ret[4].(int64) == 1
		list = append(list, b)
	}

	return list
}

// 修改赛事状态
func trans_UpdateMatchStatus(serialNumber string, status int) int {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_Guess_UpdateMatchStatus")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, status)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return 501
	}

	return int(retRows[0][0].(int64))
}

// 修改竞猜投注项金额
func trans_UpdateBetAmount(serialNumber string, betId, betAmount int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_Guess_UpdateBetAmount")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@BetID", database.AdParamInput, database.AdInteger, 4, betId)
	statement.AddParamter("@BetAmount", database.AdParamInput, database.AdInteger, 4, betAmount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	dbengine.Execute(sqlString)
}

// 添加投注记录
func trans_AddBetRecord(userId int, serialNumber string, betId, betAmount int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_Guess_AddBetRecord")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@BetID", database.AdParamInput, database.AdInteger, 4, betId)
	statement.AddParamter("@BetAmount", database.AdParamInput, database.AdInteger, 4, betAmount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	dbengine.Execute(sqlString)
}

// 设置赛事结果
func trans_SetMatchResult(serialNumber string, betId int, result string, op pb.OpUser) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Manage_Guess_SetMatchResult")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@BetID", database.AdParamInput, database.AdInteger, 4, betId)
	statement.AddParamter("@Result", database.AdParamInput, database.AdNVarChar, 32, result)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, op.OpUserId)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, op.OpUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, op.IpAddress)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	dbengine.Execute(sqlString)
}

// 获取赛事投注结算列表
func trans_GetBetSettleList(serialNumber string, betId int) []pb.BetSettle {
	var list []pb.BetSettle
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Guess_GetBetSettleList")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@BetID", database.AdParamInput, database.AdInteger, 4, betId)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	jsonData := dbengine.Execute(sqlString)
	if len(jsonData) > 0 {
		if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
			log.Release("trans_GetBetRecordList json unmarshal serialNumber=%s err %v", serialNumber, err)
		}
	}
	return list
}

// 赛事派奖
func trans_BetRecordAward(rid, resultAmount int, op pb.OpUser) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Manage_Guess_BetRecordAward")
	statement.AddParamter("@Rid", database.AdParamInput, database.AdInteger, 4, rid)
	statement.AddParamter("@ResultAmount", database.AdParamInput, database.AdInteger, 4, resultAmount)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, op.OpUserId)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, op.OpUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, op.IpAddress)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	dbengine.Execute(sqlString)
}

// 添加比赛
func trans_AddMatch(title, startAt, endAt, showStartAt, showEndAt string, op pb.OpUser) pb.RetMsg {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Guess_AddMatch")
	statement.AddParamter("@Title", database.AdParamInput, database.AdNVarChar, 32, title)
	statement.AddParamter("@StartAt", database.AdParamInput, database.AdVarChar, 20, startAt)
	statement.AddParamter("@EndAt", database.AdParamInput, database.AdVarChar, 20, endAt)
	statement.AddParamter("@ShowStartAt", database.AdParamInput, database.AdVarChar, 20, showStartAt)
	statement.AddParamter("@ShowEndAt", database.AdParamInput, database.AdVarChar, 20, showEndAt)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, op.OpUserId)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, op.OpUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, op.IpAddress)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return pb.RetMsg{RetCode: 11, Message: ""}
	}

	return pb.RetMsg{
		RetCode: 1,
		Message: retRows[0][0].(string),
	}
}

// 修改比赛
func trans_UpdateMatch(serialNumber, title, startAt, endAt, showStartAt, showEndAt string, op pb.OpUser) pb.RetMsg {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Guess_UpdateMatch")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@Title", database.AdParamInput, database.AdNVarChar, 32, title)
	statement.AddParamter("@StartAt", database.AdParamInput, database.AdVarChar, 20, startAt)
	statement.AddParamter("@EndAt", database.AdParamInput, database.AdVarChar, 20, endAt)
	statement.AddParamter("@ShowStartAt", database.AdParamInput, database.AdVarChar, 20, showStartAt)
	statement.AddParamter("@ShowEndAt", database.AdParamInput, database.AdVarChar, 20, showEndAt)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, op.OpUserId)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, op.OpUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, op.IpAddress)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return pb.RetMsg{RetCode: 11, Message: ""}
	}

	return pb.RetMsg{
		RetCode: int(retRows[0][0].(int64)),
		Message: "",
	}
}

// 添加赛事球队
func trans_AddMatchTeam(serialNumber string, teamId int, op pb.OpUser) pb.RetMsg {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Guess_AddMatchTeam")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@TeamID", database.AdParamInput, database.AdInteger, 4, teamId)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, op.OpUserId)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, op.OpUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, op.IpAddress)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return pb.RetMsg{RetCode: 11, Message: ""}
	}

	return pb.RetMsg{
		RetCode: int(retRows[0][0].(int64)),
		Message: "",
	}
}

// 修改赛事球队
func trans_UpdateMatchTeam(rId int, serialNumber string, teamId int, op pb.OpUser) pb.RetMsg {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Guess_UpdateMatchTeam")
	statement.AddParamter("@Rid", database.AdParamInput, database.AdInteger, 4, rId)
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@TeamID", database.AdParamInput, database.AdInteger, 4, teamId)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, op.OpUserId)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, op.OpUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, op.IpAddress)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return pb.RetMsg{RetCode: 11, Message: ""}
	}

	return pb.RetMsg{
		RetCode: int(retRows[0][0].(int64)),
		Message: "",
	}
}

// 添加赛事投注选项
func trans_AddMatchBet(serialNumber, betName string, betOdds float64, op pb.OpUser) pb.RetMsg {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Guess_AddMatchBet")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@BetName", database.AdParamInput, database.AdNVarChar, 32, betName)
	statement.AddParamter("@BetOdds", database.AdParamInput, database.AdFloat, 20, betOdds)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, op.OpUserId)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, op.OpUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, op.IpAddress)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return pb.RetMsg{RetCode: 11, Message: ""}
	}

	return pb.RetMsg{
		RetCode: int(retRows[0][0].(int64)),
		Message: "",
	}
}

// 修改赛事投注选项
func trans_UpdateMatchBet(betId int, serialNumber, betName string, betOdds float64, op pb.OpUser) pb.RetMsg {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Guess_UpdateMatchBet")
	statement.AddParamter("@BetID", database.AdParamInput, database.AdInteger, 4, betId)
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@BetName", database.AdParamInput, database.AdNVarChar, 32, betName)
	statement.AddParamter("@BetOdds", database.AdParamInput, database.AdFloat, 20, betOdds)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, op.OpUserId)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, op.OpUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, op.IpAddress)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return pb.RetMsg{RetCode: 11, Message: ""}
	}

	return pb.RetMsg{
		RetCode: int(retRows[0][0].(int64)),
		Message: "",
	}
}

// 用户投注
func trans_GetUserBetAmount(serialNumber string) map[int][]pb.UserBet {
	user_list := make(map[int][]pb.UserBet)
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Guess_GetUserBetAmount")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return user_list
	}

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]

		userId := int(ret[0].(int64))
		betId := int(ret[1].(int64))
		betAmount := int(ret[2].(int64))

		if bets, ok := user_list[userId]; ok {
			var isExist bool
			for k, v := range bets {
				if v.BetId != betId {
					continue
				}

				isExist = true
				user_list[userId][k].BetAmount += betAmount
				break
			}

			if !isExist {
				user_list[userId] = append(user_list[userId], pb.UserBet{
					BetId:     betId,
					BetAmount: betAmount,
				})
			}

			continue
		}

		var list []pb.UserBet
		list = append(list, pb.UserBet{
			BetId:     betId,
			BetAmount: betAmount,
		})

		user_list[userId] = list
	}

	return user_list
}

// 用户投注记录
func trans_GetUserBetRecordList(userId int, serialNumber string, pageIndex, pageSize int) (recordCount int, list []pb.BetRecord) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Guess_GetUserBetRecordList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info pb.BetRecord
			info.SerialNumber = ret[0].(string)
			info.BetId = int(ret[1].(int64))
			info.BetAmount = int(ret[2].(int64))
			info.ResultAmount = int(ret[3].(int64))
			list = append(list, info)
		}
	}

	recordCount = int(retRows[rowLen-1][0].(int64))
	return
}
