package match

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/guess/proto"
	"encoding/json"
	"fmt"
)

func Run() {
	getMatchManager()
}

func Dump(param1, param2 string) {
	getMatchManager().dumpMatch(param1)
}

// 获取配置
func GetConfig(userId int, data string) string {
	ret := getMatchManager().getConfig()
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取赛事列表(0=所有赛事  1=展示的赛事  2=正在进行的赛事  3=结束的赛事  4=预热的赛事
func GetMatchList(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.GetMatchList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	list := getMatchManager().getMatchList(userId, req.Type)
	buf, _ := json.Marshal(list)
	return string(buf)
}

// 获取赛事信息
func GetMatchInfo(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.GetMatchInfo unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getMatchManager().getMatchInfo(userId, req.SerialNumber)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 投注
func Bet(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.Bet unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getMatchManager().bet(req.SerialNumber, userId, req.BetId, req.Amount, req.IpAddress)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 设置赛事结果
func SetResult(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.SetResult unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getMatchManager().setResult(req.SerialNumber, req.BetId, req.Result, req.OpUser)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 派奖
func Award(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.Award unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getMatchManager().award(req.SerialNumber, req.OpUser)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 添加赛事
func AddMatch(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.AddMatch unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getMatchManager().addMatch(req.Title, req.StartAt, req.EndAt, req.ShowStartAt, req.ShowEndAt, req.OpUser)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 修改赛事
func UpdateMatch(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.UpdateMatch unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getMatchManager().updateMatch(req.SerialNumber, req.Title, req.StartAt, req.EndAt, req.ShowStartAt, req.ShowEndAt, req.OpUser)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 添加赛事球队
func AddMatchTeam(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.AddMatchTeam unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getMatchManager().addMatchTeam(req.SerialNumber, req.TeamId, req.OpUser)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 修改赛事球队
func UpdateMatchTeam(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.UpdateMatchTeam unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getMatchManager().updateMatchTeam(req.Rid, req.SerialNumber, req.TeamId, req.OpUser)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 刷新赛事球队
func RefreshMatchTeam(teamId int) {
	getMatchManager().refreshMatchTeam(teamId)
	return
}

// 添加赛事投注选项
func AddMatchBet(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.AddMatchBet unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getMatchManager().addMatchBet(req.SerialNumber, req.BetName, req.BetOdds, req.OpUser)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 修改赛事投注选项
func UpdateMatchBet(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.UpdateMatchBet unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getMatchManager().updateMatchBet(req.BetId, req.SerialNumber, req.BetName, req.BetOdds, req.OpUser)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 设置赛事开启
func SetMatchOpen(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.SetMatchOpen unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getMatchManager().setMatchOpen(req.SerialNumber, req.Status)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 获取用户投注记录
func GetUserBetRecordList(userId int, data string) string {
	var req pb.Request
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("match.GetUserBetRecordList unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	var ret struct {
		RecordCount int
		List        interface{}
	}
	ret.RecordCount, ret.List = getMatchManager().getUserBetRecordList(userId, req.PageIndex, req.PageSize)
	buf, _ := json.Marshal(ret)
	return string(buf)
}
