package team

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/guess/proto"
	"strconv"
)

var mgr *teammgr

// 球队
type teammgr struct {
	teamList []pb.Team
}

func getTeamManager() *teammgr {
	if mgr == nil {
		mgr = new(teammgr)
		mgr.ctor()
	}
	return mgr
}

func (this *teammgr) ctor() {
	list := trans_GetTeamList()
	this.teamList = list
	return
}

// 获取球队信息
func (this *teammgr) getTeam(teamId int) pb.Team {
	for _, v := range this.teamList {
		if v.Id == teamId {
			return v
		}
	}
	return pb.Team{}
}

// 获取球队列表
func (this *teammgr) getTeamList() []pb.Team {
	return this.teamList
}

// 添加球队信息
func (this *teammgr) addTeam(name, icon, shortName string, op pb.OpUser) {
	// GOTO：存入数据库
	teamId := trans_AddTeam(name, icon, shortName, op)
	if teamId <= 0 {
		log.Error("addTeam.trans_AddTeam name=%s icon=%s shortName=%s", name, icon, shortName)
		return
	}

	// 新增
	this.teamList = append(this.teamList, pb.Team{
		Id:        teamId,
		Name:      name,
		Icon:      icon,
		ShortName: shortName,
	})
	return
}

// 修改球队信息
func (this *teammgr) updateTeam(id int, name, icon, shortName string, op pb.OpUser, freshMatchTeam func(teamId int)) {
	for i := 0; i < len(this.teamList); i++ {
		if this.teamList[i].Id != id {
			continue
		}

		info := pb.Team{
			Id:        id,
			Name:      name,
			Icon:      icon,
			ShortName: shortName,
		}

		// 修改
		this.teamList[i] = info

		// GOTO：存入数据库
		trans_UpdateTeam(info, op)

		// 刷新球赛队伍
		go freshMatchTeam(info.Id)

		return
	}
}

// 打印球队
func (this *teammgr) dumpTeam(param1 string) {
	var teamId int
	if param1 != "" {
		teamId, _ = strconv.Atoi(param1)
	}

	log.Debug(" ^_^ 开始打印球队数据，房间(%d)个", len(this.teamList))

	for _, v := range this.teamList {
		if teamId > 0 && v.Id != teamId {
			continue
		}
		log.Debug("%+v", v)
	}

	log.Debug("完成球队数据打印 ^_^")
}
