package team

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/guess/proto"
	"encoding/json"
	"fmt"
)

func Run() {
	getTeamManager()
}

func Dump(param1, param2 string) {
	getTeamManager().dumpTeam(param1)
}

// 获取球队
func GetTeam(teamId int) pb.Team {
	return getTeamManager().getTeam(teamId)
}

// 获取球队Json格式
func GetTeamJson(userId int, data string) string {
	var req pb.Team
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("team.GetTeamJson unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	ret := getTeamManager().getTeam(req.Id)
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 球队列表
func GetTeamList(userId int, data string) string {
	ret := getTeamManager().getTeamList()
	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 添加球队
func AddTeam(userId int, data string) string {
	var req struct {
		pb.Team
		pb.OpUser
	}
	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("team.AddTeam unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	getTeamManager().addTeam(req.Name, req.Icon, req.ShortName, req.OpUser)
	return "success"
}

// 修改球队
func UpdateTeam(userId int, data string, freshMatchTeam func(teamId int)) string {
	var req struct {
		pb.Team
		pb.OpUser
	}

	if err := json.Unmarshal([]byte(data), &req); err != nil {
		retData := fmt.Sprintf("team.UpdateTeam unmarshal fail %v", err)
		log.Release(retData)
		return retData
	}

	getTeamManager().updateTeam(req.Id, req.Name, req.Icon, req.ShortName, req.OpUser, freshMatchTeam)
	return "success"
}
