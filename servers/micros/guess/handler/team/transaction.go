package team

import (
	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/guess/proto"
	"encoding/json"
)

// 获取球队列表
func trans_GetTeamList() []pb.Team {
	var list []pb.Team
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Guess_GetTeamList")
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	if len(jsonData) > 0 {
		if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
			log.Release("trans_GetTeamList json unmarshal err %v", err)
		}
	}
	return list
}

// 添加球队信息
func trans_AddTeam(name, icon, shortName string, op pb.OpUser) int {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Guess_AddTeam")
	statement.AddParamter("@Name", database.AdParamInput, database.AdNVarChar, 32, name)
	statement.AddParamter("@Icon", database.AdParamInput, database.AdVarChar, 128, icon)
	statement.AddParamter("@ShortName", database.AdParamInput, database.AdNVarChar, 32, shortName)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, op.OpUserId)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, op.OpUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, op.IpAddress)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return 0
	}
	return int(retRows[0][0].(int64))
}

// 修改球队信息
func trans_UpdateTeam(info pb.Team, op pb.OpUser) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_Guess_UpdateTeam")
	statement.AddParamter("@Id", database.AdParamInput, database.AdInteger, 4, info.Id)
	statement.AddParamter("@Name", database.AdParamInput, database.AdNVarChar, 32, info.Name)
	statement.AddParamter("@Icon", database.AdParamInput, database.AdVarChar, 128, info.Icon)
	statement.AddParamter("@ShortName", database.AdParamInput, database.AdNVarChar, 32, info.ShortName)
	statement.AddParamter("@OpUserID", database.AdParamInput, database.AdInteger, 4, op.OpUserId)
	statement.AddParamter("@OpUserName", database.AdParamInput, database.AdVarChar, 32, op.OpUserName)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, op.IpAddress)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}
