package common

import (
	"bet24.com/log"
	cclient "github.com/rpcxio/rpcx-consul/client"
	"github.com/smallnest/rpcx/client"
	//"runtime/debug"
	"sync"
)

type discoveryinfo struct {
	discovery client.ServiceDiscovery
	//plugin    client.Plugin
}

type discoverypool struct {
	clients map[string]*discoveryinfo
	lock    *sync.RWMutex
}

var discovery_mgr *discoverypool

func getDiscoveryMgr() *discoverypool {
	if discovery_mgr == nil {
		discovery_mgr = new(discoverypool)
		discovery_mgr.ctor()
	}
	return discovery_mgr
}

func getDiscovery(serviceName string, consulAddr string) client.ServiceDiscovery {
	ret := getDiscoveryMgr().getDiscovery(serviceName, consulAddr)
	if ret == nil {
		return nil
	}
	return ret.discovery
}

func (dm *discoverypool) ctor() {
	dm.clients = make(map[string]*discoveryinfo)
	dm.lock = &sync.RWMutex{}
}

func (dm *discoverypool) getDiscovery(serviceName string, consulAddr string) *discoveryinfo {
	dm.lock.Lock()
	defer dm.lock.Unlock()
	ret, ok := dm.clients[serviceName]

	if !ok || ret == nil {
		ret = dm.openConnection(serviceName, consulAddr)
		dm.clients[serviceName] = ret
	}
	return ret
}

func (dm *discoverypool) openConnection(serviceName, consulAddr string) *discoveryinfo {
	d, err := cclient.NewConsulDiscovery(Service_base_path, serviceName, []string{consulAddr}, nil)
	if err != nil {
		if d != nil {
			d.Close()
		}
		log.Release("discoverypool.openConnection [%s] [%s] failed", serviceName, consulAddr)
		return nil
	}
	log.Debug("discoverypool openConnection %s", serviceName)
	//log.Debug("%s", debug.Stack())
	return &discoveryinfo{discovery: d}
}
