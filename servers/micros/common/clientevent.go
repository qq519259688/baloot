package common

import (
	"bet24.com/log"
	"net"
)

type client_event struct {
	serviceName string
}

func newClientEvent(serviceName string) *client_event {
	ce := new(client_event)
	ce.serviceName = serviceName
	return ce
}

func (ce *client_event) ClientConnectionClose(conn net.Conn) error {
	log.Debug("client_event.ClientConnectionClose [%s]", ce.serviceName)
	mgr.RemoveClient(ce.serviceName)
	return nil
}
