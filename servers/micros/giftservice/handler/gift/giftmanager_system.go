package gift

import (
	"encoding/json"
	"os"
	"time"

	"bet24.com/log"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/shop"
	pb "bet24.com/servers/micros/giftservice/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "giftservice_config"

// 加载配置信息
func (gm *giftmanager) loadConfig() {
	time.AfterFunc(5*time.Minute, gm.loadConfig)

	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("serviceconf/giftservice_config.json")
		if err != nil {
			log.Release("giftmanager.loadConfig.loadData read json failed")
			return
		}
		configString = string(data)
		platformconfig.SetConfig(config_key, configString)
	}
	if configString == gm.lastConfigString {
		return
	}
	gm.lastConfigString = configString

	err := json.Unmarshal([]byte(configString), &gm.giftlist)
	if err == nil {
		log.Release("giftmanager.loadConfig Unmarshal config [%s] err:%v", configString, err)
		return
	}

	for k, v := range gm.giftlist {
		// 根据用户ID转换货币
		if v.PayType == pb.PayType_RMB && v.ProductId != "" {
			respProduct := coreClient.GetProduct(0, 0, v.ProductId)
			if respProduct.RetCode != 1 {
				continue
			}
			var item shop.Shop_Item
			if err := json.Unmarshal([]byte(respProduct.Data), &item); err != nil {
				log.Error("%s GetProduct json unmarshal err %v", "giftmanager.loadProductItem", err)
				continue
			}
			gm.giftlist[k].Price = item.Price
			gm.giftlist[k].Items = item.Extra
		}
	}
}
