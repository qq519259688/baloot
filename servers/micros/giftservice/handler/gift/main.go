package gift

import (
	_ "bet24.com/log"
	pb "bet24.com/servers/micros/giftservice/proto"
)

func Run() {
	getGiftManager().run()
}

func Dump(cmd, param1 string) {
	getGiftManager().dump(cmd, param1)
}

func GetGiftList(userId int) []pb.Gift {
	return getGiftManager().getGiftList(userId)
}

func GetGiftListString(userId int) string {
	return getGiftManager().getGiftListString(userId)
}

func GetGift(giftId int, userId int) *pb.Gift {
	return getGiftManager().getGift(giftId, userId)
}

func GetUnclaimedGifts(userId int) []pb.UserGift {
	return getGiftManager().getUnclaimedGifts(userId)
}

func ClaimGift(userId int, rid int) string {
	return getGiftManager().claimGift(userId, rid)
}

func SendGift(userId, toUserId, giftId int) (int, string) {
	return getGiftManager().sendGift(userId, toUserId, giftId)
}

func SendGiftBulk(userId int, toUserIds []int, giftId, num int) (int, string, *pb.Gift) {
	return getGiftManager().sendGiftBulk(userId, toUserIds, giftId, num)
}

func CheckGiftCharge(userId int, productId string) bool {
	return getGiftManager().checkGiftCharge(userId, productId)
}

func CancelChargeGift(userId int, toUserId int, productId string) {
	getGiftManager().removeChargeInfo(userId, toUserId, productId)
}

func GetReceivedGiftRecord(userId int) string {
	return getGiftManager().getReceivedGiftRecord(userId)
}
