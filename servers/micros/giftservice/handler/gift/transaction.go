package gift

import (
	"encoding/json"

	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/giftservice/proto"
)

func getUnclaimedGifts(userId int) []pb.UserGift {
	var ret []pb.UserGift
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_GiftService_getUnclaimed")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	jsonData := dbengine.Execute(statement.GenSql())
	if jsonData != "[]" {
		log.Debug("getUnclaimedGifts [%d] %s", userId, jsonData)
	}
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("gift.transaction.getUnclaimedGifts json unmarshal UserID=%d err %v", userId, err)
	}

	return ret
}

func addUserGift(ug pb.UserGift, giftType int) int {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_GiftService_AddUserGift")
	statement.AddParamter("@Sender", database.AdParamInput, database.AdInteger, 4, ug.Sender)
	statement.AddParamter("@Receiver", database.AdParamInput, database.AdInteger, 4, ug.Receiver)
	statement.AddParamter("@GiftID", database.AdParamInput, database.AdInteger, 4, ug.GiftId)
	statement.AddParamter("@SendTime", database.AdParamInput, database.AdInteger, 4, ug.SendTime)
	statement.AddParamter("@Items", database.AdParamInput, database.AdVarChar, 128, ug.Items)
	statement.AddParamter("@GiftType", database.AdParamInput, database.AdInteger, 4, giftType)
	var ret []pb.UserGift
	jsonData := dbengine.Execute(statement.GenSql())
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("gift.transaction.addUserGift json unmarshal UserID=%d err %v", ug.Sender, err)
	}

	return ret[0].Rid
}

func claimUserGift(userId int, rid int, claimTime int) string {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_GiftService_ClaimUserGift")
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Rid", database.AdParamInput, database.AdInteger, 4, rid)
	statement.AddParamter("@ClaimTime", database.AdParamInput, database.AdInteger, 4, claimTime)

	jsonData := dbengine.Execute(statement.GenSql())

	return jsonData
}

func getReceivedGiftHistory(userId int, giftType, count int) string {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_GiftService_GetClaimRecord")
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@GiftType", database.AdParamInput, database.AdInteger, 4, giftType)
	statement.AddParamter("@Count", database.AdParamInput, database.AdInteger, 4, count)

	jsonData := dbengine.Execute(statement.GenSql())

	return jsonData
}
