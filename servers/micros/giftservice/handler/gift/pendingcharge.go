package gift

import (
	"time"
)

const max_pending_time = 300

type pendingcharge struct {
	userId     int
	toUserId   int
	productId  string
	createTime int64
}

func (pc *pendingcharge) isTimeout() bool {
	return time.Now().Unix()-pc.createTime >= max_pending_time
}

func (pc *pendingcharge) isSame(userId, toUserId int, productId string) bool {
	if userId != pc.userId {
		return false
	}

	if toUserId != 0 && toUserId != pc.toUserId {
		return false
	}

	if productId != "" && productId != pc.productId {
		return false
	}
	return true
}
