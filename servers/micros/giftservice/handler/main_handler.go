package handler

import (
	"bet24.com/servers/micros/giftservice/handler/gift"
	pb "bet24.com/servers/micros/giftservice/proto"
	"context"
	"fmt"
)

var instance *MainService

func GetInstance() *MainService {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *MainService {
	ret := new(MainService)
	ret.ctor()
	return ret
}

type MainService struct{}

func (h *MainService) ctor() {
	gift.Run()
}

func (d *MainService) dump(cmd, param1, param2 string) {
	gift.Dump(cmd, param1)
}

func (h *MainService) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (h *MainService) GetGiftList(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.List = gift.GetGiftList(req.UserId)
	return nil
}

func (h *MainService) GetGiftListString(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = gift.GetGiftListString(req.UserId)
	return nil
}

func (h *MainService) GetGift(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.OneGift = gift.GetGift(req.GiftId, req.UserId)
	return nil
}

func (h *MainService) SendGift(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.IntValue, rsp.Data = gift.SendGift(req.UserId, req.ToUserId, req.GiftId)
	return nil
}

func (h *MainService) SendGiftBulk(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.IntValue, rsp.Data, rsp.OneGift = gift.SendGiftBulk(req.UserId, req.ToUserIds, req.GiftId, req.Num)
	return nil
}

func (h *MainService) CheckGiftCharge(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.BoolValue = gift.CheckGiftCharge(req.UserId, req.ProductId)
	return nil
}

func (h *MainService) GetUnclaimedGifts(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.UserGifts = gift.GetUnclaimedGifts(req.UserId)
	return nil
}

func (h *MainService) ClaimUserGift(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = gift.ClaimGift(req.UserId, req.RID)
	return nil
}

func (h *MainService) CancelChargeGift(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	gift.CancelChargeGift(req.UserId, req.ToUserId, req.ProductId)
	return nil
}

func (h *MainService) GetReceivedGiftRecord(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = gift.GetReceivedGiftRecord(req.UserId)
	return nil
}
