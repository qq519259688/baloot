package proto

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

// 支付类型，从shop拷贝
const (
	PayType_Diamond = iota // 0=钻石
	PayType_RMB            // 1=人民币购买
	PayType_Gold           // 2=金币购买
)

const (
	GiftType_Nomal     = iota // 0普通礼物
	GiftType_AudioRoom        // 1语聊房专用礼物
	GiftType_Both             // 2通用
)

const (
	AnimationType_Pic = iota
	AnimationType_Normal
	AnimationType_Global
)

const (
	UserType_Normal = iota
	UserType_Member
)

const (
	SendGiftRet_Failed = iota // 0赠送失败
	SendGiftRet_OK            // 1赠送成功
	SendGiftRet_Charge        // 2需要充值
)

type Gift struct {
	GiftId        int             // 礼物ID
	GiftType      int             // 礼物类型
	Name          string          // 名称
	Desc          string          `json:",omitempty"` // 描述
	Price         float64         // 价格
	PayType       int             // 支付方式
	ProductId     string          `json:",omitempty"` // 现金支付产品ID
	SenderCharm   int             // 发送者魅力值
	ReceiverCharm int             // 接收方魅力值
	Icon          string          `json:",omitempty"` // 图标
	Items         []item.ItemPack // 接收者得到的物品
	AnimationType int             // 动画类型
	UserType      int             `json:",omitempty"` // 成员类型
	IsBottom      bool            `json:",omitempty"` // 是否置底
	UnlockLevel   int             `json:",omitempty"` // 解锁等级
}

type UserGift struct {
	Rid       int    // 流水号
	Sender    int    // 发送方
	Receiver  int    // 接收方
	GiftId    int    // 礼物ID
	SendTime  int    // 发送时间
	ClaimTime int    // 领取时间，0表示未领取
	Items     string // 物品信息，防止系统表修改后变化
}
