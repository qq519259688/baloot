package proto

import (
	"context"
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "giftservice"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name      string
	UserId    int
	GiftId    int
	ToUserId  int
	ProductId string
	RID       int
	ToUserIds []int
	Num       int
}

type Response struct {
	Data      string
	List      []Gift
	OneGift   *Gift
	BoolValue bool
	IntValue  int
	UserGifts []UserGift
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("giftservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

func GetGiftList(userId int) []Gift {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "GetGiftList", args, reply)
	if err != nil {
		log.Debug("giftservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.List
}

func GetGiftListString(userId int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "GetGiftListString", args, reply)
	if err != nil {
		log.Debug("giftservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetGift(giftId, userId int) *Gift {
	xclient := getClient()
	args := &Request{
		UserId: userId,
		GiftId: giftId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "GetGift", args, reply)
	if err != nil {
		log.Debug("giftservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.OneGift
}

// 赠送礼物 返回类型以及错误信息
/*
	const (
	SendGiftRet_Failed = iota // 0赠送失败
	SendGiftRet_OK            // 1赠送成功
	SendGiftRet_Charge        // 2需要充值
)
*/
func SendGift(userId int, toUserId int, giftId int) (int, string) {
	xclient := getClient()
	args := &Request{
		UserId:   userId,
		ToUserId: toUserId,
		GiftId:   giftId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "SendGift", args, reply)
	if err != nil {
		log.Debug("giftservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return SendGiftRet_Failed, "server error"
	}
	return reply.IntValue, reply.Data
}

func SendGiftBulk(userId int, toUserIds []int, giftId, num int) (int, string, *Gift) {
	xclient := getClient()
	args := &Request{
		UserId:    userId,
		ToUserIds: toUserIds,
		GiftId:    giftId,
		Num:       num,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "SendGiftBulk", args, reply)
	if err != nil {
		log.Debug("giftservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return SendGiftRet_Failed, "server error", reply.OneGift
	}
	return reply.IntValue, reply.Data, reply.OneGift
}

func CheckGiftCharge(userId int, productId string) bool {
	xclient := getClient()
	args := &Request{
		UserId:    userId,
		ProductId: productId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "CheckGiftCharge", args, reply)
	if err != nil {
		log.Debug("giftservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.BoolValue
}

// 获取我未领取的礼物
func GetUnclaimedGifts(userId int) []UserGift {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "GetUnclaimedGifts", args, reply)
	if err != nil {
		log.Debug("giftservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.UserGifts
}

func GetUnclaimedGiftsString(userId int) string {
	ret := GetUnclaimedGifts(userId)
	d, _ := json.Marshal(ret)
	return string(d)
}

// 领取我的礼物, 返回获得的道具列表
func ClaimUserGift(userId int, rid int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
		RID:    rid,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "ClaimUserGift", args, reply)
	if err != nil {
		log.Debug("giftservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

// 取消充值赠送
func CancelChargeGift(userId int, toUserId int, productId string) {
	xclient := getClient()
	args := &Request{
		UserId:    userId,
		ToUserId:  toUserId,
		ProductId: productId,
	}

	err := xclient.Call(context.Background(), "CancelChargeGift", args, nil)
	if err != nil {
		log.Debug("giftservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

// 获取收取礼物记录
func GetReceivedGiftRecord(userId int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetReceivedGiftRecord", args, reply)
	if err != nil {
		log.Debug("giftservice failed to call GetReceivedGiftRecord: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}
