package main

import (
	"log"

	pb "bet24.com/servers/micros/giftservice/proto"

	slog "bet24.com/log"
)

func main() {
	logger, err := slog.New("debug", "debug", "log/client", log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}
	slog.Debug(pb.SayHello("client"))
	for {
	}
}
