package novicewelfare

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	pb "bet24.com/servers/micros/activityservice/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"encoding/json"
)

// 获取用户任务列表
func transGetUserTaskList(userId int) []*pb.UserNoviceTaskSchedule {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_NoviceWelfare_GetUserTaskList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	//log.Debug(sqlstring)
	jsonData := dbengine.Execute(sqlString)

	var out []*pb.UserNoviceTaskSchedule
	if err := json.Unmarshal([]byte(jsonData), &out); err != nil {
		log.Error("novicewelfare.transaction.transGetUserTaskList json unmarshal UserID=%d err %v", userId, err)
	}
	return out
}

// 更新用户任务
func transUpdateUserTask(userId int, taskInfo *pb.UserNoviceTaskSchedule) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_NoviceWelfare_UpdateUserTask")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@DayIndex", database.AdParamInput, database.AdInteger, 4, taskInfo.DayIndex)
	statement.AddParamter("@Scheduled", database.AdParamInput, database.AdInteger, 4, taskInfo.Scheduled)
	statement.AddParamter("@Status", database.AdParamInput, database.AdTinyInt, 4, taskInfo.Status)
	statement.AddParamter("@UnlockDayIndex", database.AdParamInput, database.AdSmallInt, 8, taskInfo.UnlockDayIndex)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}

// 获取用户大奖状态
func transGetUserBigAwardStatus(userId int) int {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_NoviceWelfare_GetBigAward")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return 0
	}
	return int(retRows[0][0].(int64))
}

// 更新用户大奖
func transUpdateUserBigAward(userId int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_NoviceWelfare_AddBigAward")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdVarChar, 20, common.GetNowTimeStr())
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}
