package novicewelfare

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/activityservice/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	task "bet24.com/servers/micros/task/proto"
	"encoding/json"
	"strconv"
	"sync"
	"time"
)

var mgr *novicewelfareMgr

func getManager() *novicewelfareMgr {
	if mgr == nil {
		mgr = newNoviceWelfareMgr()
	}
	return mgr
}

type novicewelfareMgr struct {
	lock     *sync.RWMutex
	sysData  pb.NoviceWelfare  // 系统新手福利
	userList map[int]*userTask // 用户的任务列表
}

func newNoviceWelfareMgr() *novicewelfareMgr {
	mgr = new(novicewelfareMgr)
	mgr.lock = &sync.RWMutex{}
	mgr.userList = make(map[int]*userTask)
	mgr.loadConfig()
	mgr.doCheck()
	return mgr
}

// 轮询
func (nw *novicewelfareMgr) doCheck() {
	ticker := time.NewTicker(1 * time.Minute)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				nw.checkUserExpire()
			}
		}
	}(ticker)
}

// 检查用户过期
func (nw *novicewelfareMgr) checkUserExpire() {
	var toRemove []int
	nw.lock.RLock()

	// 遍历所有用户
	for _, u := range nw.userList {
		// 判断是否过期
		if !u.isExpire() {
			continue
		}
		toRemove = append(toRemove, u.userId)
		u.destructor()
	}
	nw.lock.RUnlock()
	if len(toRemove) == 0 {
		return
	}

	// 删除
	log.Debug("novicewelfareMgr.checkUserExpire 清理用户%v", toRemove)
	nw.lock.Lock()
	for _, v := range toRemove {
		delete(nw.userList, v)
	}
	nw.lock.Unlock()

}

// 获取系统新手福利
func (nw *novicewelfareMgr) getSysNoviceWelfare() pb.NoviceWelfare {
	return nw.sysData
}

// 获取活动时长
func (nw *novicewelfareMgr) getActivityDuration() int {
	return nw.sysData.DurationDay
}

// 获取大礼包
func (nw *novicewelfareMgr) getGiftPacks() []item.ItemPack {
	return nw.sysData.Prizes
}

// 获取系统新手任务
func (nw *novicewelfareMgr) getSysNoviceTasks() []*pb.NoviceTask {
	return nw.sysData.Tasks
}

// 获取新手任务的配置
func (nw *novicewelfareMgr) getNoviceTaskConfig(dayIndex int) *pb.NoviceTask {
	for k, v := range nw.sysData.Tasks {
		if dayIndex == v.DayIndex {
			return nw.sysData.Tasks[k]
		}
	}
	return nil
}

// 用户进入
func (nw *novicewelfareMgr) onUserEnter(userId int) {
	nw.lock.RLock()
	u, ok := nw.userList[userId]
	nw.lock.RUnlock()
	if ok {
		return
	}
	u = newUserTask(userId)
	nw.lock.Lock()
	nw.userList[userId] = u
	nw.lock.Unlock()
}

// 用户退出
func (nw *novicewelfareMgr) onUserExit(userId int) {

}

// 获取用户任务
func (nw *novicewelfareMgr) getUserTask(userId int) *userTask {
	nw.lock.RLock()
	u, ok := nw.userList[userId]
	nw.lock.RUnlock()
	if ok {
		u.updateTimeStamp()
		return u
	}
	u = newUserTask(userId)
	nw.lock.Lock()
	nw.userList[userId] = u
	nw.lock.Unlock()
	return u
}

// 获取用户新手福利
func (nw *novicewelfareMgr) getUserNoviceWelfare(userId int) pb.UserNoviceWelfare {
	u := nw.getUserTask(userId)
	return u.getUserNoviceWelfare()
}

// 领取新手福利奖励
func (nw *novicewelfareMgr) claimNoviceWelfareAward(userId, dayIndex int, isFinalPackage bool) bool {
	u := nw.getUserTask(userId)
	return u.claimNoviceWelfareAward(dayIndex, isFinalPackage)
}

// 是否新手福利提醒
func (nw *novicewelfareMgr) isNoviceWelfareTip(userId int) bool {
	u := nw.getUserTask(userId)
	return u.isNoviceWelfareTip()
}

// 用户动作
func (nw *novicewelfareMgr) doAction(userId, action int, progress int, param task.TaskScope) {
	u := nw.getUserTask(userId)
	u.doAction(action, progress, param)
	return
}

// 打印 系统列表
func (nw *novicewelfareMgr) dumpSys() {
	log.Release("-------------------------------")
	log.Release("novicewelfareMgr.dumpSys")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	d, _ := json.Marshal(nw.sysData)
	log.Release(string(d))
}

// 打印用户
func (nw *novicewelfareMgr) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("novicewelfareMgr.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	if param == "" {
		nw.lock.RLock()
		log.Release("  Total User Count:%d", len(nw.userList))
		nw.lock.RUnlock()
		return
	}

	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}
	nw.lock.RLock()
	u, ok := nw.userList[userId]
	nw.lock.RUnlock()
	if !ok {
		log.Release("User does not exist!")
		return
	}
	u.dump()
}
