package novicewelfare

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/activityservice/proto"
	task "bet24.com/servers/micros/task/proto"
)

func Run() {
	getManager()
}

func OnUserEnter(userId int) {
	getManager().onUserEnter(userId)
}

func OnUserExit(userId int) {
	getManager().onUserExit(userId)
}

func Dump(cmd, param1 string) {
	switch cmd {
	case "sys":
		getManager().dumpSys()
	case "user":
		getManager().dumpUser(param1)
	default:
		log.Release("novicewelfare.Dump unhandled cmd %s", cmd)
	}
}

func GetSysNoviceWelfare() pb.NoviceWelfare {
	return getManager().getSysNoviceWelfare()
}

func GetUserNoviceWelfare(userId int) pb.UserNoviceWelfare {
	return getManager().getUserNoviceWelfare(userId)
}

func ClaimNoviceWelfareAward(userId int, dayIndex int, isFinalPackage bool) bool {
	return getManager().claimNoviceWelfareAward(userId, dayIndex, isFinalPackage)
}

func IsNoviceWelfareTip(userId int) bool {
	return getManager().isNoviceWelfareTip(userId)
}

func DoAction(userId, action, progress int, param task.TaskScope) {
	getManager().doAction(userId, action, progress, param)
}
