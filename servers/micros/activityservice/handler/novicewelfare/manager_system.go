package novicewelfare

import (
	"encoding/json"
	"os"
	"time"

	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "novicewelfare_config"

// 加载配置信息
func (nw *novicewelfareMgr) loadConfig() {
	nw.loadRedisConfig()
	go time.AfterFunc(5*time.Minute, nw.loadConfig)
}

// 加载redis配置
func (nw *novicewelfareMgr) loadRedisConfig() {
	if data := platformconfig.GetConfig(config_key); data != "" {
		if err := json.Unmarshal([]byte(data), &nw.sysData); err == nil {
			return
		}
		return
	}

	data, err := os.ReadFile("serviceconf/novicewelfare_config.json")
	if err != nil {
		log.Release("read config failed serviceconf/novicewelfare_config.json %v", err)
	}
	if err = json.Unmarshal(data, &nw.sysData); err == nil {
		platformconfig.SetConfig(config_key, string(data))
		return
	}
	log.Release("Unmarshal config [%s] err:%v", string(data), err)
}
