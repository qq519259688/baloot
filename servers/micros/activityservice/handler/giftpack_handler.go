package handler

import (
	"bet24.com/servers/micros/activityservice/handler/giftpack"
	pb "bet24.com/servers/micros/activityservice/proto"
	"context"
)

func (h *activityservice) ClaimGiftPack(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success, rsp.Data = giftpack.ClaimGiftPack(req.UserId, req.PackageId, req.TermId)
	return nil
}

func (h *activityservice) GetUserGiftPackages(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = giftpack.GetUserGiftPackages(req.UserId)
	return nil
}

func (h *activityservice) GetGiftPackages(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = giftpack.GetGiftPackages(req.UserId)
	return nil
}

func (h *activityservice) BuyGiftPackage(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	giftpack.BuyGiftPackage(req.UserId, req.ProductId)
	return nil
}
