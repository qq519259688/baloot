package dailywheel

import (
	"bet24.com/log"
	"bet24.com/redis"
	"encoding/json"
	"sync"
	"time"
)

const redis_key = "dailywheel_freeinfo"

var fm *freemgr

type freemgr struct {
	lockUser *sync.RWMutex
	lockItem *sync.RWMutex

	UserFreeInfo    map[int]int
	ItemResultCount map[int]int

	LastDay int

	isDirty bool
}

func getFreeMgr() *freemgr {
	if fm == nil {
		fm = new(freemgr)
		fm.ctor()
	}

	return fm
}

func (fm *freemgr) ctor() {
	fm.lockUser = &sync.RWMutex{}
	fm.lockItem = &sync.RWMutex{}
	fm.UserFreeInfo = make(map[int]int)
	fm.ItemResultCount = make(map[int]int)
	fm.LastDay = time.Now().Day()
	fm.loadFreeInfo()

	time.AfterFunc(10*time.Minute, fm.saveFreeInfo)
}

func (fm *freemgr) checkDayChanged() {
	nowDay := time.Now().Day()
	if nowDay == fm.LastDay {
		return
	}

	fm.isDirty = true

	fm.LastDay = nowDay
	fm.lockUser.Lock()
	fm.UserFreeInfo = make(map[int]int)
	fm.lockUser.Unlock()
	fm.lockItem.Lock()
	fm.ItemResultCount = make(map[int]int)
	fm.lockItem.Unlock()
}

func (fm *freemgr) isFree(userId int) bool {
	fm.checkDayChanged()
	fm.lockUser.RLock()
	defer fm.lockUser.RUnlock()
	count, ok := fm.UserFreeInfo[userId]
	if !ok || count == 0 {
		return true
	}
	return false
}

func (fm *freemgr) useFree(userId int) bool {
	if !fm.isFree(userId) {
		return false
	}
	fm.lockUser.Lock()
	fm.UserFreeInfo[userId] = 1
	fm.lockUser.Unlock()
	fm.isDirty = true
	return true
}

func (fm *freemgr) addItemResult(idx int, maxCount int) {
	if maxCount == 0 {
		return
	}
	fm.lockItem.Lock()
	fm.ItemResultCount[idx]++
	fm.lockItem.Unlock()
	fm.isDirty = true
}

func (fm *freemgr) getItemResultCount(idx int) int {
	fm.lockItem.RLock()
	c, ok := fm.ItemResultCount[idx]
	fm.lockItem.RUnlock()
	if !ok {
		return 0
	}
	return c
}

func (fm *freemgr) isItemAvailable(idx int, maxCount int) bool {
	if maxCount == 0 {
		return true
	}
	fm.checkDayChanged()
	return fm.getItemResultCount(idx) < maxCount
}

func (fm *freemgr) loadFreeInfo() {
	data, ok := redis.String_Get(redis_key)
	if data == "" || !ok {
		return
	}

	err := json.Unmarshal([]byte(data), &fm)
	if err != nil {
		log.Release("freemgr.loadFreeInfo Unmarshal faied %s", data)
		return
	}

	fm.checkDayChanged()
}

func (fm *freemgr) saveFreeInfo() {
	time.AfterFunc(10*time.Minute, fm.saveFreeInfo)
	if !fm.isDirty {
		return
	}

	d, _ := json.Marshal(fm)
	redis.String_SetEx(redis_key, string(d), 86400)
}

func (fm *freemgr) dump(param string) {
	log.Release("-------------------------------")
	log.Release("freemgr.dump %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	d, _ := json.Marshal(fm)
	log.Release(string(d))
}
