package dailywheel

import (
	"encoding/json"
	"math/rand"
	"os"
	"strconv"
	"time"

	"bet24.com/log"
	"bet24.com/servers/common"
	pb "bet24.com/servers/micros/activityservice/proto"
	dotservice "bet24.com/servers/micros/dotservice/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
	task "bet24.com/servers/micros/task/proto"
)

var mgr *wheelmanager

const config_key = "activity_dailywheel"
const refresh_config_sec = 600

func getManager() *wheelmanager {
	if mgr == nil {
		mgr = new(wheelmanager)
		mgr.ctor()
	}
	return mgr
}

type wheelmanager struct {
	configString    string
	WheelItems      []pb.WheelItem
	Price           item.ItemPack
	DailyTaskCount  int
	RefreshTaskCost item.ItemPack
}

func (wm *wheelmanager) ctor() {
	log.Debug("dailywheel running")
	wm.readConf()
	getFreeMgr()
}

func (wm *wheelmanager) readConf() {
	time.AfterFunc(time.Second*refresh_config_sec, wm.readConf)
	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("serviceconf/activity_dailywheel.json")
		if err != nil {
			log.Release("wheelmanager.loadData read config failed")
			return
		}
		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(config_key, configString)
		}
	}

	if configString == wm.configString {
		return
	}

	wm.configString = configString
	// 读取配置

	err := json.Unmarshal([]byte(configString), &wm)
	if err != nil {
		log.Release("wheelmanager.loadData Unmarshal failed %s", configString)
	}
}

func (wm *wheelmanager) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("wheelmanager.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}

	log.Release("userId = %d", userId)
	log.Release("info = %s", wm.getWheelInfo(userId))
}

func (wm *wheelmanager) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("wheelmanager.dumpSys")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	log.Release("  Price:%v", wm.Price)
	for _, v := range wm.WheelItems {
		log.Release("   Index[%d]Odds[%d]Items%v", v.Index, v.Odds, v.Prize)
	}
}

func (wm *wheelmanager) onUserEnter(userId int) {
	task.CreateRandomTasksByScene(userId, task.TaskScene_DailyWheel, wm.DailyTaskCount, false)
}

func (wm *wheelmanager) getWheelInfoBase(userId int) pb.UserWheelInfo {
	var info pb.UserWheelInfo
	info.Price = wm.Price
	info.Items = make([]pb.WheelItem, len(wm.WheelItems))
	copy(info.Items, wm.WheelItems)
	for k := range info.Items {
		info.Items[k].Odds = 0
		info.Items[k].MaxCount = 0
	}
	info.IsFree = getFreeMgr().isFree(userId)
	info.Owned = item.GetItemCount(userId, wm.Price.ItemId)
	info.RefreshTaskCost = wm.RefreshTaskCost
	return info
}

func (wm *wheelmanager) getWheelInfo(userId int) string {
	info := wm.getWheelInfoBase(userId)

	// 打点统计
	go dotservice.AddDot(userId, dotservice.DotScope{
		Scene:  dotservice.Scene_DailyWheel,
		Action: dotservice.Action_Click,
	})

	d, _ := json.Marshal(info)
	return string(d)
}

func (wm *wheelmanager) doWheel(userId int) string {
	log.Debug("wheelmanager.doWheel userId[%d]", userId)
	var result pb.WheelResult
	// 先扣道具
	if !getFreeMgr().isFree(userId) {
		result.Success, result.ErrorMsg = item.Consume(userId, wm.Price.ItemId, common.LOGTYPE_DAILY_WHEEL, wm.Price.Count, 0)
		if !result.Success {
			log.Debug("wheelmanager.doWheel userId[%d] failed %s", userId, result.ErrorMsg)
			d, _ := json.Marshal(result)
			return string(d)
		}
	} else {
		result.Success = true
		getFreeMgr().useFree(userId)
	}
	var itm *pb.WheelItem
	for {
		itm = wm.GetRandomItem()
		if itm == nil {
			log.Debug("wheelmanager.doWheel userId[%d] failed no random item gen", userId)
			continue
		}

		if !getFreeMgr().isItemAvailable(itm.Index, itm.MaxCount) {
			continue
		} else {
			getFreeMgr().addItemResult(itm.Index, itm.MaxCount)
		}

		break
	}

	prize := itm.Prize
	if prize.Count == 0 {
		log.Debug("wheelmanager.doWheel userId[%d] failed no prize gen", userId)
		return ""
	}

	result.Index = itm.Index
	result.Items = prize

	// 添加道具
	item.AddItems(userId, []item.ItemPack{prize}, "dailywheel", common.LOGTYPE_DAILY_WHEEL)

	// 打点统计
	go dotservice.AddDot(userId, dotservice.DotScope{
		Scene:  dotservice.Scene_DailyWheel,
		Action: dotservice.Action_Complete,
	})

	d, _ := json.Marshal(result)
	log.Debug("wheelmanager.doWheel userId[%d] %s", userId, string(d))
	return string(d)
}

func (wm *wheelmanager) GetRandomItem() *pb.WheelItem {
	total := 0
	for _, v := range wm.WheelItems {
		total += v.Odds
	}

	if total == 0 {
		return nil
	}

	r := rand.Intn(total)

	inc := 0
	for i := 0; i < len(wm.WheelItems); i++ {
		inc += wm.WheelItems[i].Odds
		if r < inc {
			return &wm.WheelItems[i]
		}
	}

	log.Release("wheelmanager.GetRandomItem return nil total[%d]r[%d]inc[%d]", total, r, inc)
	return nil
}

func (wm *wheelmanager) refreshTask(userId int) bool {
	if task.IsAllTaskFinished(userId, task.TaskScene_DailyWheel) {
		log.Debug("wheelmanager.refreshTask [%d] all task finished", userId)
		return false
	}

	// 扣钱
	if wm.RefreshTaskCost.Count > 0 {
		ok, errMsg := item.Consume(userId, wm.RefreshTaskCost.ItemId, common.LOGTYPE_DAILY_WHEEL, wm.RefreshTaskCost.Count, 0)
		if !ok {
			log.Release("refreshTask userId[%d] consume failed %s", userId, errMsg)
			return false
		}
	}
	task.CreateRandomTasksByScene(userId, task.TaskScene_DailyWheel, wm.DailyTaskCount, true)
	return true
}

func (wm *wheelmanager) isDailyWheelFree(userId int) bool {
	return getFreeMgr().isFree(userId)
}
