package dailywheel

import (
	"bet24.com/log"
)

func Run() {
	getManager()
}

func Dump(cmd, param1 string) {
	switch cmd {
	case "sys":
		getManager().dumpSys(param1)
	case "user":
		getManager().dumpUser(param1)
	case "free":
		getFreeMgr().dump(param1)
	default:
		log.Release("daylywheel.Dump unhandled cmd %s", cmd)
	}
}

func GetDailyWheelInfo(userId int) string {
	return getManager().getWheelInfo(userId)
}

func DoDailyWheel(userId int) string {
	return getManager().doWheel(userId)
}

func OnUserEnter(userId int) {
	getManager().onUserEnter(userId)
}

func RefreshTask(userId int) bool {
	return getManager().refreshTask(userId)
}

func IsDailyWheelFree(userId int) bool {
	return getManager().isDailyWheelFree(userId)
}
