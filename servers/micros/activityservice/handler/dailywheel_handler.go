package handler

import (
	"bet24.com/servers/micros/activityservice/handler/dailywheel"
	pb "bet24.com/servers/micros/activityservice/proto"
	"context"
)

func (h *activityservice) GetDailyWheelInfo(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = dailywheel.GetDailyWheelInfo(req.UserId)
	return nil
}

func (h *activityservice) DoDailyWheel(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = dailywheel.DoDailyWheel(req.UserId)
	return nil
}

func (h *activityservice) RefreshDailyWheelTask(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = dailywheel.RefreshTask(req.UserId)
	return nil
}

func (h *activityservice) IsDailyWheelFree(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = dailywheel.IsDailyWheelFree(req.UserId)
	return nil
}
