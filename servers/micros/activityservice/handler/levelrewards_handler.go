package handler

import (
	"bet24.com/servers/micros/activityservice/handler/levelrewards"
	pb "bet24.com/servers/micros/activityservice/proto"
	"context"
)

func (h *activityservice) UpdateUserInfoToLevelRewards(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	levelrewards.UpdateUserInfo(req.UserId)
	return nil
}

func (h *activityservice) GetLevelRewards(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = levelrewards.GetLevelRewards(req.UserId)
	return nil
}

func (h *activityservice) ClaimLevelRewards(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success, rsp.Items = levelrewards.ClaimLevelRewards(req.UserId, req.Level, req.IsAll)
	return nil
}

func (h *activityservice) IsLevelRewardsTip(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = levelrewards.IsLevelRewardsTip(req.UserId)
	return nil
}

func (h *activityservice) OnUserLevelChange(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	levelrewards.OnUserLevelChange(req.UserId, req.Level, req.NewLevel)
	return nil
}
