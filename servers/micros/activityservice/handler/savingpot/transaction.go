package savingpot

import (
	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/activityservice/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"encoding/json"
)

// 获取存钱罐信息
func trans_GetInfo(userId int) pb.SavingPotInfo {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SavingPot_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	var list []pb.SavingPotInfo
	if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
		log.Debug("savingpot.trans_GetInfo json unmarshal userId=%d err %v", userId, err)
	}
	if len(list) <= 0 {
		return pb.SavingPotInfo{}
	}

	return list[0]
}

// 更新存钱罐信息
func trans_UpdateInfo(userId int, info pb.SavingPotInfo) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_SavingPot_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@BuyTimes", database.AdParamInput, database.AdInteger, 4, info.BuyTimes)
	statement.AddParamter("@BuyAmount", database.AdParamInput, database.AdInteger, 4, info.BuyAmount)
	statement.AddParamter("@UpdateTime", database.AdParamInput, database.AdInteger, 4, info.UpdateTime)
	sqlString := statement.GenSql()
	log.Debug(sqlString)
	dbengine.Execute(sqlString)
	return
}
