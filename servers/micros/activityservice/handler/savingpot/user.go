package savingpot

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	pb "bet24.com/servers/micros/activityservice/proto"
	"time"
)

const idle_seconds = 600 // 过期时长(秒)

// 用户信息
type userSavingPot struct {
	UserId           int // 用户id
	timeStamp        int // 当前时间戳
	pb.SavingPotInfo     // 存钱罐信息
}

func newUserSavingPot(userId int) *userSavingPot {
	ret := new(userSavingPot)
	ret.UserId = userId
	ret.loadData()
	ret.updateTimeStamp()
	return ret
}

func (this *userSavingPot) loadData() {
	info := trans_GetInfo(this.UserId)
	this.SavingPotInfo = info
	return
}

// 更新时间戳
func (this *userSavingPot) updateTimeStamp() {
	this.timeStamp = common.GetTimeStamp() + idle_seconds

	// 同一天不需要重置
	if common.IsSameDay(common.GetTimeStamp(), this.UpdateTime) {
		return
	}

	// 需要重置
	this.BuyTimes = 0
	this.BuyAmount = 0
	this.UpdateTime = common.GetTimeStamp()
	return
}

// 是否过期
func (this *userSavingPot) isExpire() bool {
	return this.timeStamp < common.GetTimeStamp()
}

// 获取信息
func (this *userSavingPot) getInfo() pb.SavingPotInfo {
	return this.SavingPotInfo
}

// 购买
func (this *userSavingPot) buy() bool {

	// 购买次数+1
	this.BuyTimes++

	// 扣减购买
	this.BuyAmount = 0

	// 存入数据库
	trans_UpdateInfo(this.UserId, this.SavingPotInfo)

	return true
}

// 添加累计金币
func (this *userSavingPot) addAccumulateAmount(gameId, winAmount int, cfg pb.SavingPotCfg) {
	log.Debug("savingpot.user.addAccumulateAmount userId=%d gameId=%d winAmount=%d cfg=%+v",
		this.UserId, gameId, winAmount, cfg)

	var isExist bool
	for _, id := range cfg.GameIds {
		if id != gameId {
			continue
		}
		isExist = true
		break
	}

	// 该游戏不计入存钱罐
	if !isExist {
		return
	}

	// 达到累计值
	if this.BuyAmount >= cfg.AccumulateLimit {
		return
	}

	// 计算要操作的金币
	wantAmount := int(float64(winAmount) * cfg.WinRatio)
	log.Debug("savingpot.user.addAccumulateAmount userId=%d wantAmount=%d", this.UserId, wantAmount)

	// 无效值
	if wantAmount <= 0 {
		return
	}

	// 可购买的金币
	this.BuyAmount += wantAmount

	log.Debug("savingpot.user.addAccumulateAmount 1 userId=%d BuyAmount=%d", this.UserId, this.BuyAmount)

	// 超过累计值上限
	if this.BuyAmount > cfg.AccumulateLimit {
		this.BuyAmount = cfg.AccumulateLimit
	}

	log.Debug("savingpot.user.addAccumulateAmount 2 userId=%d BuyAmount=%d", this.UserId, this.BuyAmount)

	// 存入数据库
	go trans_UpdateInfo(this.UserId, this.SavingPotInfo)

	return
}

func (this *userSavingPot) dump() {
	log.Debug("userSavingPot.userId=%d 过期时间:%s ==> %+v",
		this.UserId,
		time.Unix(int64(this.timeStamp), 0).Format(common.Layout),
		this.SavingPotInfo)
	return
}
