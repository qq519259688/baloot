package savingpot

import (
	"bet24.com/log"
	"encoding/json"
)

func Run() {
	getManager()
}

func Dump(cmd, param1 string) {
	switch cmd {
	case "sys":
		getManager().dumpSys()
	case "user":
		getManager().dumpUser(param1)
	default:
		log.Debug("savingPot.Dump unhandled cmd %s", cmd)
	}
}

// 获取存钱罐
func GetBuyAmount(userId int) int {
	// 存钱罐信息
	return getManager().getBuyAmount(userId)
}

// 获取存钱罐
func GetUserSavingPot(userId int) string {
	var ret struct {
		BuyTimesLimit   int // 购买次数上限
		BuyTimes        int // 已购买次数
		AccumulateLimit int // 累计上限
	}

	// 配置信息
	cfg := getManager().getConfigInfo()
	ret.BuyTimesLimit = cfg.BuyTimesLimit
	ret.AccumulateLimit = cfg.ShowAccumulateLimit

	// 存钱罐信息
	info := getManager().getUserSavingPot(userId)
	ret.BuyTimes = info.BuyTimes

	buf, _ := json.Marshal(ret)
	return string(buf)
}

// 触发存钱罐
func TriggerSavingPot(userId, gameId, winAmount int) {
	getManager().addAccumulateAmount(userId, gameId, winAmount)
	return
}

// 存钱罐购买
func SavingPotBuy(userId int) bool {
	return getManager().buy(userId)
}
