package savingpot

import (
	"encoding/json"
	"os"
	"strconv"
	"sync"
	"time"

	"bet24.com/log"
	pb "bet24.com/servers/micros/activityservice/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "activity_savingpot"

func getManager() *savingPotMgr {
	if mgr == nil {
		mgr = new(savingPotMgr)
		mgr.ctor()
	}
	return mgr
}

var mgr *savingPotMgr

type savingPotMgr struct {
	configString string
	userList     map[int]*userSavingPot
	lock         *sync.RWMutex
	pb.SavingPotCfg
}

func (this *savingPotMgr) ctor() {
	this.lock = &sync.RWMutex{}
	this.userList = make(map[int]*userSavingPot)

	this.readConf()
	this.checkExpiredUsers()
}

func (this *savingPotMgr) readConf() {
	time.AfterFunc(time.Second*600, this.readConf)
	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("serviceconf/activity_savingPot.json")
		if err != nil {
			log.Release("savingPot.readConf read config failed")
			return
		}
		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(config_key, configString)
		}
	}

	if configString == this.configString {
		return
	}

	this.configString = configString
	// 读取配置

	err := json.Unmarshal([]byte(configString), &this.SavingPotCfg)
	if err != nil {
		log.Release("savingPot.readConf Unmarshal failed %s", configString)
	}
}

func (this *savingPotMgr) checkExpiredUsers() {
	time.AfterFunc(time.Second*600, this.checkExpiredUsers)
	var toRemove []int
	this.lock.RLock()
	for k, v := range this.userList {
		if v.isExpire() {
			toRemove = append(toRemove, k)
		}
	}
	this.lock.RUnlock()

	if len(toRemove) == 0 {
		return
	}

	log.Debug("savingPot.checkExpiredUsers removing %v", toRemove)

	this.lock.Lock()
	for _, v := range toRemove {
		delete(this.userList, v)
	}
	this.lock.Unlock()
}

// 获取配置信息
func (this *savingPotMgr) getConfigInfo() pb.SavingPotCfg {
	return this.SavingPotCfg
}

// 获取用户
func (this *savingPotMgr) getUser(userId int) *userSavingPot {
	this.lock.RLock()
	u, ok := this.userList[userId]
	this.lock.RUnlock()
	if ok {
		u.updateTimeStamp()
		return u
	}

	u = newUserSavingPot(userId)
	this.lock.Lock()
	this.userList[userId] = u
	this.lock.Unlock()
	return u
}

// 获取用户信息
func (this *savingPotMgr) getUserSavingPot(userId int) pb.SavingPotInfo {
	u := this.getUser(userId)
	if u == nil {
		log.Error("savingPot.getUserSavingPot userId[%d] is not exist", userId)
		return pb.SavingPotInfo{}
	}

	return u.getInfo()
}

// 获取存钱罐金币
func (this *savingPotMgr) getBuyAmount(userId int) int {
	u := this.getUser(userId)
	if u == nil {
		log.Error("savingPot.getBuyAmount userId[%d] is not exist", userId)
		return 0
	}

	// 配置
	cfg := this.getConfigInfo()

	// 用户信息
	info := u.getInfo()

	// 判断是否超过购买次数上限
	if info.BuyTimes >= cfg.BuyTimesLimit {
		return 0
	}

	return info.BuyAmount
}

// 存钱罐购买
func (this *savingPotMgr) buy(userId int) bool {
	u := this.getUser(userId)
	if u == nil {
		log.Error("savingPot.buy userId[%d] is not exist", userId)
		return false
	}

	return u.buy()
}

// 添加累计金币
func (this *savingPotMgr) addAccumulateAmount(userId, gameId, winAmount int) {
	u := this.getUser(userId)
	if u == nil {
		log.Error("savingPot.addAccumulateAmount userId[%d] is not exist", userId)
		return
	}

	u.addAccumulateAmount(gameId, winAmount, this.SavingPotCfg)
	return
}

func (this *savingPotMgr) dumpSys() {
	log.Release("-------------------------------")
	log.Release("savingPotConfig.dumpSys")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	d, _ := json.Marshal(this.SavingPotCfg)
	log.Release(string(d))
	return
}

func (this *savingPotMgr) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("savingPotConfig.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	if param == "" {
		this.lock.RLock()
		log.Release("  Total User Count:%d", len(this.userList))
		this.lock.RUnlock()
		return
	}

	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}
	this.lock.RLock()
	u, ok := this.userList[userId]
	this.lock.RUnlock()
	if !ok {
		log.Release("User does not exist!")
		return
	}
	u.dump()
}
