package handler

import (
	"bet24.com/log"
	"bet24.com/servers/micros/activityservice/handler/dailywheel"
	"bet24.com/servers/micros/activityservice/handler/giftpack"
	"bet24.com/servers/micros/activityservice/handler/levelrewards"
	"bet24.com/servers/micros/activityservice/handler/novicewelfare"
	"bet24.com/servers/micros/activityservice/handler/savingpot"
	"bet24.com/servers/micros/activityservice/handler/signin"
	pb "bet24.com/servers/micros/activityservice/proto"
	robot "bet24.com/servers/micros/userservices/proto"
	"context"
	"fmt"
)

var instance *activityservice

type activityservice struct {
}

func GetInstance() *activityservice {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func newHandler() *activityservice {
	ret := new(activityservice)
	ret.ctor()
	return ret
}

func (h *activityservice) ctor() {
	novicewelfare.Run()
	dailywheel.Run()
	levelrewards.Run()
	signin.Run()
	savingpot.Run()
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func (h *activityservice) dump(cmd, param1, param2 string) {
	switch cmd {
	case "novicewelfare":
		novicewelfare.Dump(param1, param2)
	case "giftpack":
		giftpack.Dump(param1, param2)
	case "dailywheel":
		dailywheel.Dump(param1, param2)
	case "levelrewards":
		levelrewards.Dump(param1, param2)
	case "signin":
		signin.Dump(param1, param2)
	case "savingpot":
		savingpot.Dump(param1, param2)
	default:
		log.Release("activityservice.Dump unhandled cmd %s", cmd)
	}
}

func (h *activityservice) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (h *activityservice) AddUser(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	if robot.IsRobot(req.UserId) {
		return nil
	}
	novicewelfare.OnUserEnter(req.UserId)
	giftpack.OnUserEnter(req.UserId)
	dailywheel.OnUserEnter(req.UserId)
	signin.OnUserEnter(req.UserId)
	return nil
}

func (h *activityservice) RemoveUser(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	novicewelfare.OnUserExit(req.UserId)
	signin.OnUserExit(req.UserId)
	return nil
}
