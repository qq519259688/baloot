package handler

import (
	"bet24.com/servers/micros/activityservice/handler/novicewelfare"
	pb "bet24.com/servers/micros/activityservice/proto"
	robot "bet24.com/servers/micros/userservices/proto"
	"context"
)

func (h *activityservice) GetSysNoviceWelfare(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.SysData = novicewelfare.GetSysNoviceWelfare()
	return nil
}

func (h *activityservice) GetUserNoviceWelfare(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.UserWelfare = novicewelfare.GetUserNoviceWelfare(req.UserId)
	return nil
}

func (h *activityservice) ClaimNoviceWelfareAward(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = novicewelfare.ClaimNoviceWelfareAward(req.UserId, req.DayIndex, req.IsFinalPackage)
	return nil
}

func (h *activityservice) IsNoviceWelfareTip(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = novicewelfare.IsNoviceWelfareTip(req.UserId)
	return nil
}

func (h *activityservice) DoAction(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	if robot.IsRobot(req.UserId) {
		return nil
	}
	novicewelfare.DoAction(req.UserId, req.Action, req.Progress, req.Param)
	return nil
}
