package giftpack

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	pb "bet24.com/servers/micros/activityservice/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	"encoding/json"
	"time"
)

const idle_seconds = 600

type user_giftpack struct {
	userId   int
	packs    []*pb.UserGiftPack
	lastPing int64
}

func newUser(userId int) *user_giftpack {
	ret := new(user_giftpack)
	ret.userId = userId
	ret.lastPing = time.Now().Unix()
	ret.ctor()
	return ret
}

func (u *user_giftpack) ctor() {
	userPacks := transGetUserGiftPacks(u.userId)
	for k := range userPacks {
		u.packs = append(u.packs, &userPacks[k])
	}
	// 如果有新增礼包，则创建
	for _, v := range getManager().packs {
		pack := u.getPack(v.Id)
		if pack == nil {
			go u.createPackage(v.Id)
		}
	}
	// 读取所有terms
	terms := transGetUserTerms(u.userId)
	for _, v := range terms {
		p := u.getPack(v.PackageId)
		if p == nil {
			log.Release("user_giftpack.ctor[%d] packageId[%d] not found", u.userId, v.PackageId)
			continue
		}
		p.Terms = append(p.Terms, v.UserGrowthTerm)
	}
}

func (u *user_giftpack) isExpired() bool {
	return time.Now().Unix()-u.lastPing > idle_seconds
}

func (u *user_giftpack) createPackage(packageId int) {
	pack := &pb.UserGiftPack{
		PackageId: packageId,
		StartDay:  int(time.Now().Unix()),
		Status:    pb.PackageStatus_TOBUY,
	}
	u.packs = append(u.packs, pack)
	go transSetUserGiftPack(u.userId, packageId, pack.StartDay, pack.Status)
}

func (u *user_giftpack) getGiftPackStatus(packageId int) int {
	u.lastPing = time.Now().Unix()
	for _, v := range u.packs {
		if v.PackageId == packageId {
			return v.Status
		}
	}
	log.Release("user_giftpack.getGiftPackStatus[%d] packageId[%d] not found", u.userId, packageId)
	return pb.PackageStatus_TOBUY
}

func (u *user_giftpack) getGiftPackStartTime(packageId int) int {
	for _, v := range u.packs {
		if v.PackageId == packageId {
			return v.StartDay
		}
	}
	log.Release("user_giftpack.getGiftPackStartDay[%d] packageId[%d] not found", u.userId, packageId)
	return 0
}

func (u *user_giftpack) getAllTerms() string {
	u.lastPing = time.Now().Unix()
	for _, v := range u.packs {
		if v.Status == pb.PackageStatus_FINISHED {
			for i := 0; i < len(v.Terms); i++ {
				v.Terms[i].ClaimStatus = pb.ClaimStatus_Claimed
			}
			continue
		}
		if v.Status != pb.PackageStatus_BOUGHT {
			continue
		}
		// 设置下是否可领取
		for i := 0; i < len(v.Terms); i++ {
			isClaimable := u.isTermClaimable(v.PackageId, v.Terms[i].TermIndex)
			if isClaimable {
				v.Terms[i].ClaimStatus = pb.ClaimStatus_Valid
			} else {
				if v.Terms[i].ClaimDay == 0 {
					v.Terms[i].ClaimStatus = pb.ClaimStatus_Invalid
				} else {
					v.Terms[i].ClaimStatus = pb.ClaimStatus_Claimed
				}
			}
		}
	}
	d, _ := json.Marshal(u.packs)
	return string(d)
}

func (u *user_giftpack) claimGiftPack(packageId int, termId int) (bool, string) {
	u.lastPing = time.Now().Unix()
	pack := u.getPack(packageId)
	if pack == nil {
		log.Release("user_giftpack.claimGiftPack not found %d", packageId)
		return false, "package not found"
	}

	if pack.Status != pb.PackageStatus_BOUGHT {
		log.Release("user_giftpack.claimGiftPack not proper status %d", pack.Status)
		return false, "not proper status"
	}

	// 是否可领？
	if !u.isTermClaimable(packageId, termId) {
		return false, "unclaimable"
	}

	// 加道具
	items := getManager().getTermItems(packageId, termId)
	if len(items) > 0 {
		item.AddItems(u.userId, items, "novicewelfare_giftpack", common.LOGTYPE_NOVICE_WELFARE_GIFTPACK)
	}
	// 更新状态
	for i := 0; i < len(pack.Terms); i++ {
		if pack.Terms[i].TermIndex == termId {
			pack.Terms[i].ClaimDay = common.GetNowDayIndex()
			go transSetUserTerm(u.userId, packageId, termId, pack.Terms[i].ClaimDay)
		}
	}
	// 如果已经领取完该礼包，则结束
	if u.isPackageFinished(packageId) {
		pack.Status = pb.PackageStatus_FINISHED
		go transSetUserGiftPack(u.userId, packageId, pack.StartDay, pack.Status)
	}
	return true, "ok"
}

func (u *user_giftpack) getPack(packageId int) *pb.UserGiftPack {
	for _, v := range u.packs {
		if v.PackageId == packageId {
			return v
		}
	}
	return nil
}

func (u *user_giftpack) getTerm(packageId int, termId int) *pb.UserGrowthTerm {
	for _, v := range u.packs {
		if v.PackageId != packageId {
			continue
		}
		for i := 0; i < len(v.Terms); i++ {
			if v.Terms[i].TermIndex == termId {
				return &v.Terms[i]
			}
		}
	}
	return nil
}

func (u *user_giftpack) isTermClaimable(packageId, termId int) bool {
	curTerm := u.getTerm(packageId, termId)
	if curTerm == nil {
		log.Release("user_giftpack.isTermClaimable term not found %d.%d", packageId, termId)
		return false
	}

	if curTerm.ClaimDay != 0 {
		log.Release("user_giftpack.isTermClaimable term claimed %d.%d", packageId, termId)
		return false
	}

	lastTerm := u.getTerm(packageId, termId-1)
	if lastTerm != nil {
		if lastTerm.ClaimDay == 0 {
			return false
		}
		if common.GetNowDayIndex() == lastTerm.ClaimDay {
			return false
		}
	}
	return true
}

func (u *user_giftpack) isPackageFinished(packageId int) bool {
	pack := u.getPack(packageId)
	if pack == nil {
		log.Release("user_giftpack.isPackageFinished not found %d", packageId)
		return false
	}
	for _, v := range pack.Terms {
		if v.ClaimDay == 0 {
			return false
		}
	}
	return true
}

func (u *user_giftpack) buyGiftPack(productId string) {
	u.lastPing = time.Now().Unix()
	sysPack := getManager().getSysPackageByProductId(productId)
	if sysPack == nil {
		log.Release("user_giftpack.buyGiftPack[%d] syspack not found %s", u.userId, productId)
		return
	}
	packageId := sysPack.Id
	pack := u.getPack(packageId)
	if pack == nil {
		log.Release("user_giftpack.buyGiftPack[%d] not found %d", u.userId, packageId)
		return
	}
	if pack.Status != pb.PackageStatus_TOBUY {
		log.Release("user_giftpack.buyGiftPack[%d] not proper status [%d][%d]", u.userId, packageId, pack.Status)
		return
	}

	// 激活
	pack.Status = pb.PackageStatus_BOUGHT
	pack.Terms = []pb.UserGrowthTerm{}
	go transSetUserGiftPack(u.userId, packageId, pack.StartDay, pack.Status)
	for _, v := range sysPack.Terms {
		term := pb.UserGrowthTerm{TermIndex: v.TermIndex}
		pack.Terms = append(pack.Terms, term)
		go transSetUserTerm(u.userId, packageId, v.TermIndex, 0)
	}
}
