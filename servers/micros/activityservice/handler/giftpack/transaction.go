package giftpack

import (
	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/activityservice/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"encoding/json"
)

/*
	table user_package {UserId,PackageId,StartDay,Status}
	table user_term {UserId,PackageId,TermIndex,ClaimDay}
*/

/*
数据库输出[{PackageId,StartDay,Status}]
*/
func transGetUserGiftPacks(userId int) []pb.UserGiftPack {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_NoviceWelfare_GetUserGiftPacks")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	//log.Debug(sqlstring)
	jsonData := dbengine.Execute(sqlString)

	var out []pb.UserGiftPack
	if err := json.Unmarshal([]byte(jsonData), &out); err != nil {
		log.Error("transaction.transGetUserTaskList json unmarshal UserID=%d err %v", userId, err)
	}
	return out
}

// 更新或插入用户礼包状态
func transSetUserGiftPack(userId int, packageId int, startDay int, status int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_NoviceWelfare_SetUserGiftPacks")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@PackageId", database.AdParamInput, database.AdInteger, 4, packageId)
	statement.AddParamter("@StartDay", database.AdParamInput, database.AdInteger, 4, startDay)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, status)

	sqlString := statement.GenSql()
	//log.Debug(sqlstring)
	dbengine.Execute(sqlString)
}

// 获取已购买礼包的领取列表
type userPackageTerms struct {
	PackageId int
	pb.UserGrowthTerm
}

func transGetUserTerms(userId int) []userPackageTerms {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_NoviceWelfare_GetUserTerms")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	//log.Debug(sqlstring)
	jsonData := dbengine.Execute(sqlString)

	var out []userPackageTerms
	if err := json.Unmarshal([]byte(jsonData), &out); err != nil {
		log.Error("transaction.transGetUserTerms json unmarshal UserID=%d err %v", userId, err)
	}
	return out
}

func transSetUserTerm(userId int, packageId int, termIndex int, ClaimDay int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_NoviceWelfare_SetUserTerm")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@PackageId", database.AdParamInput, database.AdInteger, 4, packageId)
	statement.AddParamter("@TermIndex", database.AdParamInput, database.AdInteger, 4, termIndex)
	statement.AddParamter("@ClaimDay", database.AdParamInput, database.AdInteger, 4, ClaimDay)

	sqlString := statement.GenSql()
	//log.Debug(sqlstring)
	dbengine.Execute(sqlString)
}
