package giftpack

import (
	"bet24.com/log"
)

func Dump(cmd, param1 string) {
	switch cmd {
	case "sys":
		getManager().dumpSys(param1)
	case "user":
		getManager().dumpUser(param1)
	default:
		log.Release("giftpack.Dump unhandled cmd %s", cmd)
	}
}

func GetGiftPackages(userId int) string {
	return getManager().getGiftPackages(userId)
}

func GetUserGiftPackages(userId int) string {
	return getManager().getUserGiftPackages(userId)
}
func ClaimGiftPack(userId int, packageId int, termId int) (bool, string) {
	return getManager().claimGiftPack(userId, packageId, termId)
}

func OnUserEnter(userId int) {
	getManager().onUserEnter(userId)
}

func BuyGiftPackage(userId int, productId string) {
	getManager().buyGiftPack(userId, productId)
}
