package giftpack

import (
	"encoding/json"
	"os"
	"strconv"
	"sync"
	"time"

	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/shop"
	pb "bet24.com/servers/micros/activityservice/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

var mgr *giftpack_manager

const config_key = "novice_giftpacks"
const refresh_config_sec = 1800

func getManager() *giftpack_manager {
	if mgr == nil {
		mgr = new(giftpack_manager)
		mgr.ctor()
	}
	return mgr
}

type giftpack_manager struct {
	configString string
	packs        []pb.GrowthPack
	users        map[int]*user_giftpack
	lock         *sync.RWMutex
}

func (gm *giftpack_manager) ctor() {
	log.Debug("giftpack running")
	gm.users = make(map[int]*user_giftpack)
	gm.lock = &sync.RWMutex{}
	gm.readConf()
	gm.checkExpiredUsers()
}

func (gm *giftpack_manager) checkExpiredUsers() {
	time.AfterFunc(time.Second*600, gm.checkExpiredUsers)
	var toRemove []int
	gm.lock.RLock()
	for k, v := range gm.users {
		if v.isExpired() {
			toRemove = append(toRemove, k)
		}
	}
	gm.lock.RUnlock()
	if len(toRemove) == 0 {
		return
	}
	log.Debug("giftpack_manager.checkExpiredUsers removing %v", toRemove)
	gm.lock.Lock()
	for _, v := range toRemove {
		delete(gm.users, v)
	}
	gm.lock.Unlock()
}

func (gm *giftpack_manager) readConf() {
	time.AfterFunc(time.Second*600, gm.readConf)
	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("serviceconf/novice_giftpacks.json")
		if err != nil {
			log.Release("giftpack_manager.loadData read config failed")
			return
		}
		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(config_key, configString)
		}
	}

	if configString == gm.configString {
		return
	}

	gm.configString = configString
	var packs []pb.GrowthPack

	err := json.Unmarshal([]byte(configString), &packs)
	if err != nil {
		log.Release("giftpack_manager.manager.loadData Unmarshal failed err:%v", err)
		return
	}
	if len(packs) == 0 {
		return
	}

	for i := 0; i < len(packs); i++ {
		respProduct := coreservice.GetProduct(0, 0, packs[i].ProductId)
		if respProduct.RetCode != 1 {
			continue
		}
		var item shop.Shop_Item
		if err := json.Unmarshal([]byte(respProduct.Data), &item); err != nil {
			log.Release("giftpack_manager.readConf GetProduct json unmarshal err %s", respProduct.Data)
			continue
		}
		packs[i].Price = item.Price
	}

	gm.lock.Lock()
	gm.packs = make([]pb.GrowthPack, len(packs))
	copy(gm.packs, packs)
	gm.lock.Unlock()
}

func (gm *giftpack_manager) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("giftpack_manager.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}

	si := gm.getUserGiftPackages(userId)
	log.Release(si)
}

func (gm *giftpack_manager) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("giftpack_manager.dumpSys")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		userId = 0
	}
	log.Release(gm.getGiftPackages(userId))
}

func (gm *giftpack_manager) getGiftPackages(userId int) string {
	if len(gm.packs) == 0 {
		return ""
	}
	packs := make([]pb.GrowthPack, len(gm.packs))
	copy(packs, gm.packs)
	u := gm.getUser(userId)
	if u != nil {
		for i := 0; i < len(packs); i++ {
			packs[i].Status = u.getGiftPackStatus(packs[i].Id)
			packs[i].RemainSeconds = packs[i].DurationDays*86400 + u.getGiftPackStartTime(packs[i].Id) - int(time.Now().Unix())
			if packs[i].RemainSeconds < 0 {
				packs[i].RemainSeconds = 0
			}
		}
	}
	d, _ := json.Marshal(packs)
	return string(d)
}

func (gm *giftpack_manager) getUserGiftPackages(userId int) string {
	u := gm.getUser(userId)
	if u == nil {
		return ""
	}
	return u.getAllTerms()
}
func (gm *giftpack_manager) claimGiftPack(userId int, packageId int, termId int) (bool, string) {
	u := gm.getUser(userId)
	if u == nil {
		log.Release("giftpack_manager.claimGiftPack userId[%d] not found", userId)
		return false, "user not found"
	}
	return u.claimGiftPack(packageId, termId)
}

func (gm *giftpack_manager) onUserEnter(userId int) {
	gm.getUser(userId)
}

func (gm *giftpack_manager) getUser(userId int) *user_giftpack {
	if userId <= 0 {
		log.Release("giftpack_manager.getUser userId <= 0 %d", userId)
		return nil
	}
	gm.lock.RLock()
	u, _ := gm.users[userId]
	gm.lock.RUnlock()
	if u == nil {
		u = newUser(userId)
	}

	gm.lock.Lock()
	gm.users[userId] = u
	gm.lock.Unlock()

	return u
}

func (gm *giftpack_manager) getSysPackage(packageId int) *pb.GrowthPack {
	for i := 0; i < len(gm.packs); i++ {
		if gm.packs[i].Id == packageId {
			return &gm.packs[i]
		}
	}
	return nil
}

func (gm *giftpack_manager) getSysPackageByProductId(productId string) *pb.GrowthPack {
	for i := 0; i < len(gm.packs); i++ {
		if gm.packs[i].ProductId == productId {
			return &gm.packs[i]
		}
	}
	return nil
}

func (gm *giftpack_manager) getTermItems(packageId, termIndex int) []item.ItemPack {
	pack := gm.getSysPackage(packageId)
	if pack == nil {
		log.Release("giftpack_manager.getTermItems [%d] not found", packageId)
		return nil
	}

	for _, v := range pack.Terms {
		if v.TermIndex == termIndex {
			return v.Items
		}
	}

	log.Release("giftpack_manager.getTermItems [%d].[%d] not found", packageId, termIndex)
	return nil
}

func (gm *giftpack_manager) buyGiftPack(userId int, productId string) {
	u := gm.getUser(userId)
	if u == nil {
		log.Release("giftpack_manager.buyGiftPack userId[%d] not found")
		return
	}

	u.buyGiftPack(productId)
}
