package levelrewards

import (
	"encoding/json"
	"os"
	"strconv"
	"sync"
	"time"

	"bet24.com/log"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/shop"
	pb "bet24.com/servers/micros/activityservice/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

var mgr *levelrewardsMgr

const config_key = "activity_levelrewards"

func getManager() *levelrewardsMgr {
	if mgr == nil {
		mgr = new(levelrewardsMgr)
		mgr.ctor()
	}
	return mgr
}

type levelrewardsMgr struct {
	configString string
	lock         *sync.RWMutex
	userList     map[int]*userTask
	pb.LevelRewards
}

func (lvr *levelrewardsMgr) ctor() {
	lvr.lock = &sync.RWMutex{}
	lvr.userList = make(map[int]*userTask)
	lvr.readConf()
	lvr.checkExpiredUsers()
}

func (lvr *levelrewardsMgr) readConf() {
	time.AfterFunc(time.Second*600, lvr.readConf)
	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("serviceconf/activity_levelrewards.json")
		if err != nil {
			log.Release("levelrewards.readConf read config failed")
			return
		}
		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(config_key, configString)
		}
	}

	if configString == lvr.configString {
		return
	}

	lvr.configString = configString
	// 读取配置

	err := json.Unmarshal([]byte(configString), &lvr.LevelRewards)
	if err != nil {
		log.Release("levelrewards.readConf Unmarshal failed %s", configString)
	}
	lvr.loadProductItem()
}

func (lvr *levelrewardsMgr) checkExpiredUsers() {
	time.AfterFunc(time.Second*600, lvr.checkExpiredUsers)
	var toRemove []int
	lvr.lock.RLock()
	for k, v := range lvr.userList {
		if v.isExpire() {
			toRemove = append(toRemove, k)
		}
	}
	lvr.lock.RUnlock()
	if len(toRemove) == 0 {
		return
	}
	log.Debug("levelrewards.checkExpiredUsers removing %v", toRemove)
	lvr.lock.Lock()
	for _, v := range toRemove {
		delete(lvr.userList, v)
	}
	lvr.lock.Unlock()
}

// 加载产品道具
func (lvr *levelrewardsMgr) loadProductItem() {
	respProduct := coreClient.GetProduct(0, 0, lvr.LevelRewards.ProductPrice.ProductId)
	if respProduct.RetCode != 1 {
		return
	}
	var itemInfo shop.Shop_Item
	if err := json.Unmarshal([]byte(respProduct.Data), &itemInfo); err != nil {
		log.Error("%s GetProduct json unmarshal err %v", "userTask.loadProductItem", err)
		return
	}
	lvr.LevelRewards.ProductPrice.Price = itemInfo.Price
	lvr.LevelRewards.ProductPrice.Items = itemInfo.Extra
}

func (lvr *levelrewardsMgr) dumpSys() {
	log.Release("-------------------------------")
	log.Release("levelrewards.dumpSys")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	d, _ := json.Marshal(lvr.LevelRewards)
	log.Release(string(d))
	return
}

func (lvr *levelrewardsMgr) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("levelrewards.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	if param == "" {
		lvr.lock.RLock()
		log.Release("  Total User Count:%d", len(lvr.userList))
		lvr.lock.RUnlock()
		return
	}

	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}
	lvr.lock.RLock()
	u, ok := lvr.userList[userId]
	lvr.lock.RUnlock()
	if !ok {
		log.Release("User does not exist!")
		return
	}
	u.dump()
}

func (lvr *levelrewardsMgr) clear(userId int) {
	lvr.lock.Lock()
	defer lvr.lock.Unlock()
	delete(lvr.userList, userId)
}

func (lvr *levelrewardsMgr) getSysLevelRewards() pb.LevelRewards {
	return lvr.LevelRewards
}

func (lvr *levelrewardsMgr) getSysTask(level int) pb.LevelTask {
	for k, v := range lvr.LevelRewards.Tasks {
		if v.Level == level {
			return lvr.LevelRewards.Tasks[k]
		}
	}
	return pb.LevelTask{}
}

func (lvr *levelrewardsMgr) getSysTaskLevel(level int) int {
	var sysLevel int
	for _, v := range lvr.LevelRewards.Tasks {
		if level < v.Level {
			continue
		}
		sysLevel = v.Level
	}
	return sysLevel
}

func (lvr *levelrewardsMgr) getUserTask(userId int) *userTask {
	lvr.lock.RLock()
	u, ok := lvr.userList[userId]
	lvr.lock.RUnlock()
	if ok {
		u.updateTimeStamp()
		return u
	}
	u = newUserTask(userId)
	lvr.lock.Lock()
	lvr.userList[userId] = u
	lvr.lock.Unlock()
	return u
}

func (lvr *levelrewardsMgr) getLevelRewards(userId int) string {
	u := lvr.getUserTask(userId)
	ret := u.getLevelRewards()
	jsonRet, _ := json.Marshal(ret)
	return string(jsonRet)
}

func (lvr *levelrewardsMgr) claimLevelRewards(userId, level int, isAll bool) (bool, []item.ItemPack) {
	u := lvr.getUserTask(userId)
	return u.claimLevelRewards(level, isAll)
}

func (lvr *levelrewardsMgr) isLevelRewardsTip(userId int) bool {
	u := lvr.getUserTask(userId)
	return u.isLevelRewardsTip()
}

func (lvr *levelrewardsMgr) onUserLevelChange(userId, oldLevel, newLevel int) {
	u := lvr.getUserTask(userId)
	u.onUserLevelChange(oldLevel, newLevel)
	return
}
