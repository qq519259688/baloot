package levelrewards

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/coreservice/shop"
	pb "bet24.com/servers/micros/activityservice/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"encoding/json"
)

// 获取用户任务列表
func transGetUserTaskList(userId int) []pb.UserLevelTaskSchedule {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_LevelRewards_GetUserTaskList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	//log.Debug(sqlstring)
	jsonData := dbengine.Execute(sqlString)

	var out []pb.UserLevelTaskSchedule
	if err := json.Unmarshal([]byte(jsonData), &out); err != nil {
		log.Error("levelrewards.transaction.transGetUserTaskList json unmarshal UserID=%d err %v", userId, err)
	}
	return out
}

// 是否购买成长礼包
func transIsPurchaseGrowthPack(userId int) bool {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Shop_GetShopTypeCount")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@ShopType", database.AdParamInput, database.AdInteger, 4, shop.ShopType_LevelRewards)
	sqlString := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return false
	}
	count := int(retRows[0][0].(int64))
	if count > 0 {
		return true
	}
	return false
}

// 更新用户任务
func transUpdateUserTask(userId int, taskInfo pb.UserLevelTaskSchedule) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_LevelRewards_UpdateUserTask")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Level", database.AdParamInput, database.AdInteger, 4, taskInfo.Level)
	statement.AddParamter("@PaidPack", database.AdParamInput, database.AdTinyInt, 4, taskInfo.PaidPack)
	statement.AddParamter("@ClaimStatus", database.AdParamInput, database.AdTinyInt, 4, taskInfo.ClaimStatus)
	statement.AddParamter("@CreateDayIndex", database.AdParamInput, database.AdInteger, 4, taskInfo.CreateDayIndex)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}
