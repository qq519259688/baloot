package levelrewards

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	pb "bet24.com/servers/micros/activityservice/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	notification "bet24.com/servers/micros/notification/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"sort"
)

const idle_seconds = 600 // 过期时长(秒)

type userTask struct {
	userId              int // 用户id
	timeStamp           int // 当前时间戳
	pb.UserLevelRewards     // 任务列表
}

func newUserTask(userId int) *userTask {
	ret := new(userTask)
	ret.userId = userId
	ret.loadData()
	ret.checkTaskStatus()
	ret.updateTimeStamp()
	return ret
}

func (ut *userTask) loadData() {
	ut.loadSysConfig()
	ut.Level = userservices.GetUserLevel(ut.userId).Level
	if ut.isEndRestriction() {
		return
	}
	ut.Schedule = transGetUserTaskList(ut.userId)
	ut.IsPaid = transIsPurchaseGrowthPack(ut.userId)
	ut.generateLevelTasks()
	ut.daysRemainingChange()
	if ut.IsPaid {
		ut.DaysRemaining = 0
	}
	return
}

// 加载系统配置
func (ut *userTask) loadSysConfig() {
	sys := mgr.getSysLevelRewards()
	ut.DurationDay = sys.DurationDay
	ut.LvRestriction = sys.LvRestriction
	ut.Tasks = sys.Tasks
	ut.ProductPrice.ProductId = sys.ProductPrice.ProductId
	ut.ProductPrice.Name = sys.ProductPrice.Name
	ut.ProductPrice.Desc = sys.ProductPrice.Desc
	ut.ProductPrice.Price = sys.ProductPrice.Price
	ut.ProductPrice.Items = sys.ProductPrice.Items
}

// 是否结束限制
func (ut *userTask) isEndRestriction() bool {
	return ut.Level < mgr.LvRestriction
}

// 生成等级任务
func (ut *userTask) generateLevelTasks() {
	for k, v := range mgr.getSysLevelRewards().Tasks {
		freeTask := ut.getUserLevelTask(v.Level, pb.Free_Pack)
		if freeTask.Level == 0 {
			// 免费
			freeTask.Level = v.Level
			freeTask.PaidPack = pb.Free_Pack
			freeTask.ClaimStatus = pb.ClaimStatus_Invalid
			if freeTask.CreateDayIndex == 0 && k == 0 {
				freeTask.CreateDayIndex = common.GetDayIndex(common.GetTimeStamp())
			}
			ut.Schedule = append(ut.Schedule, freeTask)
			ut.updateTaskInfo(freeTask)
		}
		paidTask := ut.getUserLevelTask(v.Level, pb.Paid_Pack)
		if paidTask.Level == 0 {
			// 付费
			paidTask.Level = v.Level
			paidTask.PaidPack = pb.Paid_Pack
			paidTask.ClaimStatus = pb.ClaimStatus_Invalid
			paidTask.CreateDayIndex = 0
			ut.Schedule = append(ut.Schedule, paidTask)
			ut.updateTaskInfo(paidTask)
		}
	}
	sort.SliceStable(ut.Schedule, func(i, j int) bool {
		if ut.Schedule[i].Level == ut.Schedule[j].Level {
			return ut.Schedule[i].PaidPack < ut.Schedule[j].PaidPack
		}
		return ut.Schedule[i].Level < ut.Schedule[j].Level
	})
	return
}

// 获取用户等级的任务
func (ut *userTask) getUserLevelTask(level, paidPack int) pb.UserLevelTaskSchedule {
	for k, v := range ut.Schedule {
		if v.Level == level && v.PaidPack == paidPack {
			return ut.Schedule[k]
		}
	}
	return pb.UserLevelTaskSchedule{}
}

// 剩余天数索引变更
func (ut *userTask) daysRemainingChange() {
	toDayIndex := common.GetDayIndex(common.GetTimeStamp())
	endDayIndex := ut.Schedule[0].CreateDayIndex + mgr.getSysLevelRewards().DurationDay
	daysRemaining := endDayIndex - toDayIndex
	if daysRemaining < 0 && !ut.IsPaid {
		ut.clearActivity()
		return
	}
	ut.DaysRemaining = daysRemaining
	return
}

// 是否结束
func (ut *userTask) isEnd() bool {
	return len(ut.Schedule) == 0
}

// 检查任务状态
func (ut *userTask) checkTaskStatus() {
	var dataLen int
	for k, v := range ut.Schedule {
		if ut.Level < v.Level {
			continue
		}
		if v.ClaimStatus > pb.ClaimStatus_Invalid {
			if ut.IsPaid && v.ClaimStatus == pb.ClaimStatus_Claimed {
				dataLen++
			}
			continue
		}
		if v.IsPaidPack() && !ut.IsPaid {
			continue
		}
		ut.Schedule[k].ClaimStatus = pb.ClaimStatus_Valid
		if ut.Schedule[k].CreateDayIndex == 0 {
			ut.Schedule[k].CreateDayIndex = common.GetDayIndex(common.GetTimeStamp())
		}
		ut.updateTaskInfo(ut.Schedule[k])
	}
	if dataLen == len(ut.Schedule) {
		ut.clearActivity()
	}
}

// 清除活动
func (ut *userTask) clearActivity() {
	ut.Schedule = nil
	ut.DaysRemaining = 0
}

// 更新时间戳
func (ut *userTask) updateTimeStamp() {
	ut.timeStamp = common.GetTimeStamp() + idle_seconds
	return
}

// 是否过期
func (ut *userTask) isExpire() bool {
	return ut.timeStamp < common.GetTimeStamp()
}

// 获取任务奖励
func (ut *userTask) getLevelRewards() pb.UserLevelRewards {
	return ut.UserLevelRewards
}

// 领取等级奖励
func (ut *userTask) claimLevelRewards(level int, isAll bool) (bool, []item.ItemPack) {
	if ut.isEnd() {
		return false, []item.ItemPack{}
	}
	if isAll {
		return ut.claimAllRewards()
	}
	var items []item.ItemPack
	sysTask := mgr.getSysTask(level)
	if sysTask.Level == 0 {
		log.Debug("userTask.claimLevelRewards system Level mismatch! UserID=%d, Level=%d", ut.userId, level)
		return false, items
	}
	for k, v := range ut.Schedule {
		if level != v.Level {
			continue
		}
		if v.ClaimStatus <= pb.ClaimStatus_Invalid || v.ClaimStatus >= pb.ClaimStatus_Claimed {
			continue
		}
		if v.IsPaidPack() {
			if !ut.IsPaid {
				continue
			}
			items = append(items, sysTask.Paid...)
		} else {
			items = append(items, sysTask.Free...)
		}
		ut.Schedule[k].ClaimStatus = pb.ClaimStatus_Claimed
		ut.updateTaskInfo(ut.Schedule[k])
	}
	return ut.addItems(items)
}

// 领取所有奖励
func (ut *userTask) claimAllRewards() (bool, []item.ItemPack) {
	var items []item.ItemPack
	for k, v := range ut.Schedule {
		if ut.Level < v.Level {
			continue
		}
		if v.ClaimStatus <= pb.ClaimStatus_Invalid || v.ClaimStatus >= pb.ClaimStatus_Claimed {
			continue
		}
		sysTask := mgr.getSysTask(v.Level)
		if sysTask.Level == 0 {
			log.Debug("userTask.claimAllRewards system Level mismatch! UserID=%d, Level=%d", ut.userId, v.Level)
			continue
		}
		if v.IsPaidPack() {
			if !ut.IsPaid {
				continue
			}
			items = append(items, sysTask.Paid...)
		} else {
			items = append(items, sysTask.Free...)
		}
		ut.Schedule[k].ClaimStatus = pb.ClaimStatus_Claimed
		ut.updateTaskInfo(ut.Schedule[k])
	}
	return ut.addItems(items)
}

// 更新任务信息
func (ut *userTask) updateTaskInfo(taskInfo pb.UserLevelTaskSchedule) {
	go transUpdateUserTask(ut.userId, taskInfo)
}

// 添加道具
func (ut *userTask) addItems(items []item.ItemPack) (bool, []item.ItemPack) {
	if len(items) == 0 {
		return false, items
	}
	items = item.GroupItems(items)
	go item.AddItems(ut.userId, items, "levelrewards_levelpack", common.LOGTYPE_LEVEL_PACK)
	return true, items
}

// 是否等级礼包提醒
func (ut *userTask) isLevelRewardsTip() bool {
	for _, v := range ut.Schedule {
		if v.ClaimStatus == pb.ClaimStatus_Valid {
			return true
		}
	}
	return false
}

func (ut *userTask) onUserLevelChange(oldLevel, newLevel int) {
	if ut.isEnd() {
		return
	}
	ut.Level = newLevel
	if mgr.getSysTaskLevel(oldLevel) == mgr.getSysTaskLevel(newLevel) {
		return
	}

	ut.checkTaskStatus()
	d, _ := json.Marshal(ut.getLevelRewards())
	go notification.AddNotification(ut.userId, notification.Notification_LevelRewards, string(d))
	return
}

func (ut *userTask) dump() {
	log.Release("timeout timeStamp: [%s], "+
		"Activity duration: [%d], "+
		"Remaining days of activity: [%d], "+
		"Whether you purchased a growth package: [%t], "+
		"User current level: [%d]",
		common.TimeStampToString(int64(ut.timeStamp)), ut.DurationDay, ut.DaysRemaining, ut.IsPaid, ut.Level)
	for _, v := range ut.Schedule {
		log.Release("Level:[%d], ClaimStatus[%d], CreateDayIndex[%d]",
			v.Level, v.ClaimStatus, v.CreateDayIndex)
	}
}
