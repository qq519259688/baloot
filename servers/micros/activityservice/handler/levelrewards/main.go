package levelrewards

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
)

func Run() {
	getManager()
}

func Dump(cmd, param1 string) {
	switch cmd {
	case "sys":
		getManager().dumpSys()
	case "user":
		getManager().dumpUser(param1)
	default:
		log.Release("levelrewards.Dump unhandled cmd %s", cmd)
	}
}

func UpdateUserInfo(userId int) {
	getManager().clear(userId)
	return
}

func GetLevelRewards(userId int) string {
	return getManager().getLevelRewards(userId)
}

func ClaimLevelRewards(userId, level int, isAll bool) (bool, []item.ItemPack) {
	return getManager().claimLevelRewards(userId, level, isAll)
}

func IsLevelRewardsTip(userId int) bool {
	return getManager().isLevelRewardsTip(userId)
}

func OnUserLevelChange(userId, oldLevel, newLevel int) {
	getManager().onUserLevelChange(userId, oldLevel, newLevel)
	return
}
