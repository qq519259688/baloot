package handler

import (
	"bet24.com/servers/micros/activityservice/handler/signin"
	pb "bet24.com/servers/micros/activityservice/proto"
	"context"
)

func (h *activityservice) CheckUserSignTip(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = signin.CheckUserSignTip(req.UserId)
	return nil
}

func (h *activityservice) GetUserSigninInfo(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = signin.GetUserSigninInfo(req.UserId)
	return nil
}

func (h *activityservice) DoUserSignin(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = signin.DoUserSignin(req.UserId)
	return nil
}
