package handler

import (
	"bet24.com/servers/micros/activityservice/handler/savingpot"
	pb "bet24.com/servers/micros/activityservice/proto"
	"golang.org/x/net/context"
)

// 获取用户存钱罐
func (h *activityservice) GetUserSavingPotBuyAmount(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Amount = savingpot.GetBuyAmount(req.UserId)
	return nil
}

// 获取用户存钱罐
func (h *activityservice) GetUserSavingPot(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = savingpot.GetUserSavingPot(req.UserId)
	return nil
}

// 触发存钱罐
func (h *activityservice) TriggerSavingPot(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	savingpot.TriggerSavingPot(req.UserId, req.GameId, req.WinAmount)
	return nil
}

// 触发存钱罐
func (h *activityservice) SavingPotBuy(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = savingpot.SavingPotBuy(req.UserId)
	return nil
}
