package signin

import (
	"bet24.com/log"
	"encoding/json"
)

func Run() {
	getManager()
}

func OnUserEnter(userId int) {
	getManager().getUser(userId)
}

func OnUserExit(userId int) {
	getManager().removeUser(userId)
}

func Dump(cmd, param1 string) {
	switch cmd {
	case "sys":
		getManager().dumpSys(param1)
	case "user":
		getManager().dumpUser(param1)
	default:
		log.Release("signin.Dump unhandled cmd %s", cmd)
	}
}

func CheckUserSignTip(userId int) bool {
	return getManager().checkSignTip(userId)
}

func GetUserSigninInfo(userId int) string {
	ret := getManager().getUserSigninInfo(userId)
	if ret == nil {
		return ""
	}
	d, _ := json.Marshal(ret)
	return string(d)
}

func DoUserSignin(userId int) string {
	ok, items := getManager().doSignin(userId)
	if !ok {
		log.Debug("DoUserSignin [%d] failed", userId)
		return ""
	}
	d, _ := json.Marshal(items)
	return string(d)
}
