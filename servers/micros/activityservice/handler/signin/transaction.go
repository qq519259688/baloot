package signin

import (
	"bet24.com/database"
	"bet24.com/log"
	pb "bet24.com/servers/micros/activityservice/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"runtime/debug"
)

// 用户签到列表
func userSignList(userId int) []*pb.Signin {
	defer func() {
		if err := recover(); err != nil {
			log.Error("sign.userSignList transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserSign_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)

	var signs []*pb.Signin
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out pb.Signin

		out.Id = int((ret[0]).(int64))
		out.SignTime = int((ret[1]).(int64))
		if out.SignTime < 0 {
			out.SignTime = 0
		}

		signs = append(signs, &out)
	}

	return signs
}

// 更新用户签到信息
func updateUserSign(userId, signId, signTime int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserSign_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SignID", database.AdParamInput, database.AdInteger, 4, signId)
	statement.AddParamter("@SignTime", database.AdParamInput, database.AdInteger, 4, signTime)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	dbengine.Execute(sqlstring)
}
