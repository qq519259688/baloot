package signin

import (
	badge "bet24.com/servers/micros/badge/proto"
	"fmt"

	"bet24.com/log"
	"bet24.com/servers/common"
	pb "bet24.com/servers/micros/activityservice/proto"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	vip "bet24.com/servers/micros/userservices/proto"
)

type userSignin struct {
	userId     int
	signinList []pb.Signin
}

func newUserSignin(userId int) *userSignin {
	us := new(userSignin)
	us.userId = userId
	us.loadSigninInfo()
	return us
}

func (us *userSignin) loadSigninInfo() {
	//1、签到列表
	signlist := userSignList(us.userId)
	for _, v := range getManager().getSignList(true) {
		signTime := 0
		for _, s := range signlist {
			if v.Id == s.Id {
				signTime = s.SignTime
				break
			}
		}
		us.signinList = append(us.signinList, pb.Signin{
			Id:             v.Id,
			Award:          v.Award,
			DoubleVipLevel: v.DoubleVipLevel,
			SignTime:       signTime,
		})
	}

	us.checkFinishSequence()
}

// 检查是否重新开启新一轮签到
func (us *userSignin) checkFinishSequence() {
	count := len(us.signinList)
	if count == 0 {
		return
	}
	if us.signinList[count-1].SignTime == 0 {
		return
	}
	lastSignTime := us.signinList[count-1].SignTime
	now := common.GetTimeStamp()
	if common.IsSameDay(lastSignTime, now) {
		return
	}
	// 一轮结束，连续签到也清零
	us.signinList = mgr.getSignList(true)
	go us.updateSigninAll()
}

// 签到
func (us *userSignin) signin() (bool, *item.ItemPack) {
	var items *item.ItemPack
	if len(us.signinList) == 0 {
		log.Debug("userSignin.signin userId[%d] no signinList", us.userId)
		return false, items
	}

	signin := us.getCurrentSignin()
	if signin == nil {
		log.Debug("userSignin.signin userId[%d] getCurrentSignin == nil", us.userId)
		return false, items
	}
	signin.SignTime = common.GetTimeStamp()
	items = getManager().getPrizeByDayIndex(signin.Id)
	us.sendAward(items, fmt.Sprintf("签到ID:[%d]", signin.Id))

	vipLevel := 0

	userVip := vip.GetUserVipInfo(us.userId)
	if userVip != nil && userVip.IsVip() {
		vipLevel = userVip.Level
	}

	if vipLevel >= signin.DoubleVipLevel && signin.DoubleVipLevel > 0 {
		us.sendAward(items, fmt.Sprintf("签到ID:[%d] vip doubled", signin.Id))
		items.Count *= 2
	}
	// 新增徽章签到次数
	go badge.DoAction(us.userId, badge.Action_SignInCount, 1, badge.Scope{})
	us.updateSignin(signin.Id, signin.SignTime)
	return true, items
}

func (us *userSignin) getLastSignTime() int {
	count := len(us.signinList)
	for i := count - 1; i >= 0; i-- {
		if us.signinList[i].SignTime != 0 {
			return us.signinList[i].SignTime
		}
	}
	return 0
}

func (us *userSignin) getCurrentSignin() *pb.Signin {
	count := len(us.signinList)
	lastSignTime := 0
	lastSignIndex := -1
	for i := count - 1; i >= 0; i-- {
		if us.signinList[i].SignTime != 0 {
			lastSignIndex = i
			lastSignTime = us.signinList[i].SignTime
			break
		}
	}
	if lastSignIndex == count-1 {
		//log.Debug("userSignin.getCurrentSignin [%d] sequence has done", us.userId)
		return nil
	}
	// 当日已签到？
	if lastSignTime > 0 {
		now := common.GetTimeStamp()
		if common.IsSameDay(lastSignTime, now) {
			//log.Debug("userSignin.signin [%d] has signed today", us.userId)
			return nil
		}
	}

	return &us.signinList[lastSignIndex+1]
}

func (us *userSignin) sendAward(items *item.ItemPack, desc string) {
	if items == nil {
		log.Release("userSignin.sendAward userId[%d] items == nil", us.userId)
		return
	}
	inventory.AddItems(us.userId, []item.ItemPack{*items}, desc, common.LOGTYPE_SEND_SIGN)
}

func (us *userSignin) updateSigninAll() {
	for _, v := range us.signinList {
		us.updateSignin(v.Id, v.SignTime)
	}
}

func (us *userSignin) updateSignin(id, signTime int) {
	updateUserSign(us.userId, id, signTime)
}

func (us *userSignin) getSigninInfo() *pb.SigninInfo {
	signin := us.getCurrentSignin()
	return &pb.SigninInfo{
		SigninTable: us.signinList,
		Signable:    signin != nil,
	}
}

func (us *userSignin) checkSignTip() bool {
	sign := us.getCurrentSignin()
	return sign != nil
}
