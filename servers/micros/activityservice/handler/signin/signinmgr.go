package signin

import (
	"encoding/json"
	"os"
	"strconv"
	"sync"
	"time"

	"bet24.com/log"
	pb "bet24.com/servers/micros/activityservice/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

var mgr *signinmgr

const config_key = "activity_signin"

func getManager() *signinmgr {
	if mgr == nil {
		mgr = new(signinmgr)
		mgr.ctor()
	}
	return mgr
}

type signinmgr struct {
	configString string
	lock         *sync.RWMutex
	signSequence []pb.Signin
	userlist     map[int]*userSignin
}

func (sm *signinmgr) ctor() {
	sm.lock = &sync.RWMutex{}
	sm.userlist = make(map[int]*userSignin)
	sm.loadSigninInfo()
	log.Debug("signin manager running")
}

func (sm *signinmgr) loadSigninInfo() {
	time.AfterFunc(time.Second*600, sm.loadSigninInfo)
	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("serviceconf/activity_signin.json")
		if err != nil {
			log.Release("signinmgr.loadSigninInfo read config failed")
			return
		}
		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(config_key, configString)
		}
	}

	if configString == sm.configString {
		return
	}

	sm.configString = configString

	err := json.Unmarshal([]byte(configString), &sm.signSequence)
	if err != nil {
		log.Release("signinmgr.loadSigninInfo Unmarshal failed err:%v", err)
		return
	}
}

func (sm *signinmgr) getSignList(removeOdds bool) []pb.Signin {
	sm.lock.RLock()
	d, _ := json.Marshal(sm.signSequence)
	sm.lock.RUnlock()
	var ret []pb.Signin
	json.Unmarshal(d, &ret)
	if removeOdds {
		for k, v := range ret {
			for k1 := range v.Award {
				ret[k].Award[k1].Odds = 0
			}
		}
	}
	return ret
}

// 获取或创建用户
func (sm *signinmgr) getUser(userId int) *userSignin {
	sm.lock.RLock()
	us, ok := sm.userlist[userId]
	sm.lock.RUnlock()
	if !ok {
		// 用户不存在
		us = newUserSignin(userId)
		sm.lock.Lock()
		sm.userlist[userId] = us
		sm.lock.Unlock()
	}
	return us
}

func (sm *signinmgr) removeUser(userId int) {
	sm.lock.Lock()
	defer sm.lock.Unlock()
	delete(sm.userlist, userId)
}

func (sm *signinmgr) getUserSigninInfo(userId int) *pb.SigninInfo {
	user := sm.getUser(userId)
	return user.getSigninInfo()
}

func (sm *signinmgr) checkSignTip(userId int) bool {
	user := sm.getUser(userId)
	return user.checkSignTip()
}

func (sm *signinmgr) doSignin(userId int) (bool, *item.ItemPack) {
	user := sm.getUser(userId)
	return user.signin()
}

func (sm *signinmgr) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("signinmgr.dumpSys %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	d, _ := json.Marshal(sm.signSequence)
	log.Release(string(d))
}

func (sm *signinmgr) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("signinmgr.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}
	si := sm.getUserSigninInfo(userId)
	if si == nil {
		log.Release("user %d not exist", userId)
		return
	}
	d, _ := json.Marshal(si)
	log.Release(string(d))
}

func (sm *signinmgr) getPrizeByDayIndex(dayIndex int) *item.ItemPack {
	for _, v := range sm.signSequence {
		if v.Id == dayIndex {
			return v.GetPrize()
		}
	}
	return nil
}
