package main

import (
	_ "bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/micros/activityservice/config"
	"bet24.com/servers/micros/activityservice/handler"
	pb "bet24.com/servers/micros/activityservice/proto"
	"bet24.com/servers/micros/common"
	"fmt"
)

func main() {
	config.Run(pb.ServiceName)
	go waitInput()
	redis.InitPool(config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB)
	common.RunService(pb.ServiceName, handler.GetInstance(), fmt.Sprintf("localhost:%d", config.Server.ConsulPort))
}

func waitInput() {
	for {
		var cmd string
		var param1 string
		var param2 string
		fmt.Scanf("%s %s %s", &cmd, &param1, &param2)
		switch cmd {
		case "":
			break
		case "exit":
			common.DeregisterService(pb.ServiceName)
		default:
			handler.Dump(cmd, param1, param2)
		}
	}
}
