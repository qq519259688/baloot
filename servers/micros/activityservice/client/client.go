package main

import (
	slog "bet24.com/log"
	pb "bet24.com/servers/micros/activityservice/proto"
	"log"
)

func main() {
	logger, err := slog.New("debug", "debug", "log/client", log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}
	pb.SayHello("client")
}
