package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	"context"
)

func UpdateUserInfoToLevelRewards(userId int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "UpdateUserInfoToLevelRewards", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return
}

func GetLevelRewards(userId int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetLevelRewards", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return reply.Data
}

func ClaimLevelRewards(userId, Level int, isAll bool) (bool, []item.ItemPack) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
		Level:  Level,
		IsAll:  isAll,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "ClaimLevelRewards", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return reply.Success, reply.Items
}

func IsLevelRewardsTip(userId int) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "IsLevelRewardsTip", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return reply.Success
}

func OnUserLevelChange(userId, oldLevel, newLevel int) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId:   userId,
		Level:    oldLevel,
		NewLevel: newLevel,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "OnUserLevelChange", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return reply.Success
}
