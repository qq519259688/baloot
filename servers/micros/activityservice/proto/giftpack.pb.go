package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

func GetGiftPackages(userId int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "GetGiftPackages", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice.GetGiftPackages failed to call: %v", err)
		return ""
	}
	return reply.Data
}

func GetUserGiftPackages(userId int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserGiftPackages", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice.GetUserGiftPackages failed to call: %v", err)
		return ""
	}
	return reply.Data
}

func ClaimGiftPack(userId int, packageId, termId int) (bool, string) {
	xclient := getClient()
	args := &Request{
		UserId:    userId,
		PackageId: packageId,
		TermId:    termId,
	}
	reply := &Response{}

	err := xclient.Call(context.Background(), "ClaimGiftPack", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice.ClaimGiftPack failed to call: %v", err)
		return false, "server error"
	}
	return reply.Success, reply.Data
}

func BuyGiftPackage(userId int, productId string) string {
	xclient := getClient()
	args := &Request{
		UserId:    userId,
		ProductId: productId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "BuyGiftPackage", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice.BuyGiftPackage failed to call: %v", err)
		return ""
	}
	return reply.Data
}
