package proto

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	PackageStatus_TOBUY    = iota // 0未购买
	PackageStatus_BOUGHT          // 1已购买
	PackageStatus_FINISHED        // 2已结束
)

const (
	ClaimStatus_Invalid = iota // 0不可领取
	ClaimStatus_Valid          // 1可领取
	ClaimStatus_Claimed        // 2已领取
)

type GrowthPack struct {
	Id           int          // 礼包ID
	ProductId    string       // 商城产品ID
	Name         string       // 名称
	Price        float64      // 价格
	Terms        []GrowthTerm // 礼包列表
	DurationDays int          // 持续天数
	// 给客户端补充的数据
	Status        int // 用户的礼包状态
	RemainSeconds int // 剩余秒数
}

type GrowthTerm struct {
	TermIndex int             // index
	Name      string          // 名称
	Items     []item.ItemPack // 礼包内容
}

type UserGiftPack struct {
	PackageId int
	StartDay  int // 生成时间，用于显示倒计时
	Status    int // 用户的礼包状态
	Terms     []UserGrowthTerm
}

type UserGrowthTerm struct {
	TermIndex   int // index
	ClaimDay    int // 领取时间,0表示未领取
	ClaimStatus int // 是否可领取，客户端显示用
}
