package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

func CheckUserSignTip(userId int) bool {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "CheckUserSignTip", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice.CheckUserSignTip failed to call: %v", err)
		return false
	}
	return reply.Success
}

func GetUserSigninInfo(userId int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "GetUserSigninInfo", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice.GetUserSigninInfo failed to call: %v", err)
		return ""
	}
	return reply.Data
}

func DoUserSignin(userId int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "DoUserSignin", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice.DoUserSignin failed to call: %v", err)
		return ""
	}
	return reply.Data
}
