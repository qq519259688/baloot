package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	task "bet24.com/servers/micros/task/proto"
	"context"
)

func GetSysNoviceWelfare() NoviceWelfare {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetSysNoviceWelfare", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return reply.SysData
}

func GetUserNoviceWelfare(userId int) UserNoviceWelfare {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserNoviceWelfare", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return reply.UserWelfare
}

func ClaimNoviceWelfareAward(userId, dayIndex int, isFinalPackage bool) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId:         userId,
		DayIndex:       dayIndex,
		IsFinalPackage: isFinalPackage,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "ClaimNoviceWelfareAward", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return reply.Success
}

func IsNoviceWelfareTip(userId int) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "IsNoviceWelfareTip", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return reply.Success
}

func DoAction(userId, action, progress int, param task.TaskScope) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId:   userId,
		Action:   action,
		Progress: progress,
		Param:    param,
	}

	err := xclient.Call(context.Background(), "DoAction", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}
	return
}
