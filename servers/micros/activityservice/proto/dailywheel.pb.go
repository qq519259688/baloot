package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

func GetDailyWheelInfo(userId int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "GetDailyWheelInfo", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice.GetDailyWheelInfo failed to call: %v", err)
		return ""
	}
	return reply.Data
}

func DoDailyWheel(userId int) string {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}

	err := xclient.Call(context.Background(), "DoDailyWheel", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice.DoDailyWheel failed to call: %v", err)
		return ""
	}
	return reply.Data
}

func RefreshDailyWheelTask(userId int) bool {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "RefreshDailyWheelTask", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice.RefreshDailyWheelTask failed to call: %v", err)
		return false
	}
	return reply.Success
}

func IsDailyWheelFree(userId int) bool {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "IsDailyWheelFree", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice.IsDailyWheelFree failed to call: %v", err)
		return false
	}
	return reply.Success
}
