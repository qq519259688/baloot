package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	task "bet24.com/servers/micros/task/proto"
	"context"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "activityservice"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name           string
	UserId         int
	DayIndex       int
	Action         int
	Progress       int
	IsFinalPackage bool
	Param          task.TaskScope
	TermId         int
	PackageId      int
	ProductId      string
	Level          int // 当前等级
	NewLevel       int // 等级变化
	IsAll          bool
	GameId         int // 游戏id
	WinAmount      int // 赢取金币
}

type Response struct {
	RetCode     int
	Success     bool
	Data        string
	SysData     NoviceWelfare
	UserWelfare UserNoviceWelfare
	Items       []item.ItemPack
	Amount      int
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("%s failed to call: %v", ServiceName, err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("activityservice return %s", reply.Data)
	return reply.Data
}

func AddUser(userId int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "AddUser", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}
}

func RemoveUser(userId int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "RemoveUser", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}
}
