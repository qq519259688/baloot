package proto

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

type WheelItem struct {
	Index    int
	Odds     int `json:",omitempty"` // 概率
	Prize    item.ItemPack
	MaxCount int `json:",omitempty"` // 每天最多出多少个
}

type UserWheelInfo struct {
	Price           item.ItemPack // 转盘所需道具
	Owned           int           // 当前拥有道具数量
	IsFree          bool
	Items           []WheelItem
	RefreshTaskCost item.ItemPack // 刷新任务所需道具
}

type WheelResult struct {
	Success  bool
	ErrorMsg string
	Index    int           // 中奖项目Index
	Items    item.ItemPack // 中奖内容
}
