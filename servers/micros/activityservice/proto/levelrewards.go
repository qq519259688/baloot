package proto

import item "bet24.com/servers/micros/item_inventory/proto"

const (
	Free_Pack = 0 // 免费礼包
	Paid_Pack = 1 // 付费礼包
)

// 等级礼包
type LevelRewards struct {
	DurationDay   int         // 活动持续时间（天）
	LvRestriction int         // 等级限制
	Tasks         []LevelTask // 等级任务
	ProductPrice  PaidPrice   // 产品价格
}

// 付费的价格
type PaidPrice struct {
	ProductId string          // 商品ID，需要同步商城配置
	Name      string          // 礼包名称
	Desc      string          // 描述
	Price     float64         // 价格
	Items     []item.ItemPack // 礼包内容
}

// 等级任务
type LevelTask struct {
	Level int             // 等级
	Free  []item.ItemPack // 免费的
	Paid  []item.ItemPack // 付费的
}

// 用户的等级礼包
type UserLevelRewards struct {
	LevelRewards
	DaysRemaining int                     // 剩余的天数
	IsPaid        bool                    // 是否付费
	Level         int                     // 当前等级
	Schedule      []UserLevelTaskSchedule // 任务时间表
}

// 用户等级任务时间表
type UserLevelTaskSchedule struct {
	Level          int // 等级要求
	PaidPack       int // 付费礼包（0=不是，1=是）
	ClaimStatus    int // 领取状态（0不可领取，1可领取，2已领取）
	CreateDayIndex int // 创建天的索引
}

// 是否付费礼包
func (ult UserLevelTaskSchedule) IsPaidPack() bool {
	return ult.PaidPack == Paid_Pack
}
