package proto

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
	"math/rand"
)

type Signin struct {
	Id             int
	Award          []RandomPrize
	DoubleVipLevel int // vip？加倍，0表示没有
	SignTime       int // 签到时间，0表示未签
}

type SigninInfo struct {
	SigninTable []Signin
	Signable    bool
}

type RandomPrize struct {
	item.ItemPack
	Odds int `json:",omitempty"` // 概率
}

func (s *Signin) GetPrize() *item.ItemPack {
	if len(s.Award) == 0 {
		return nil
	}
	if len(s.Award) == 1 {
		return &s.Award[0].ItemPack
	}
	total := 0
	for _, v := range s.Award {
		total += v.Odds
	}

	if total == 0 {
		return nil
	}

	r := rand.Intn(total)

	inc := 0
	for i := 0; i < len(s.Award); i++ {
		inc += s.Award[i].Odds
		if r < inc {
			return &s.Award[i].ItemPack
		}
	}

	log.Release("Signin.GetPrize return nil total[%d]r[%d]inc[%d]", total, r, inc)
	return nil
}
