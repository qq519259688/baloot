package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"golang.org/x/net/context"
)

// 获取用户存钱罐金币
func GetUserSavingPotBuyAmount(userId int) int {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserSavingPotBuyAmount", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return reply.Amount
}

// 获取用户存钱罐
func GetUserSavingPot(userId int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserSavingPot", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return reply.Data
}

// 触发存钱罐
func TriggerSavingPot(userId, gameId, winAmount int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId:    userId,
		GameId:    gameId,
		WinAmount: winAmount,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "TriggerSavingPot", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return
}

// 存钱罐购买
func SavingPotBuy(userId int) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SavingPotBuy", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("activityservice failed to call: %v", err)
	}

	return reply.Success
}
