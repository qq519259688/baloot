package proto

// SavingPotCfg 存钱罐配置
type SavingPotCfg struct {
	GameIds             []int   // 游戏id
	WinRatio            float64 // 赢比率
	AccumulateLimit     int     // 累计上限
	ShowAccumulateLimit int     // 展示累计上限
	BuyTimesLimit       int     // 购买次数上限
}

// SavingPotInfo 存钱罐信息
type SavingPotInfo struct {
	BuyTimes   int // 已购买次数
	BuyAmount  int // 可购买金币
	UpdateTime int // 时间戳
}
