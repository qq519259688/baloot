package proto

import (
	item "bet24.com/servers/micros/item_inventory/proto"
	task "bet24.com/servers/micros/task/proto"
)

// 新手福利 - 配置
type NoviceWelfare struct {
	DurationDay int             // 活动持续时间（天）
	Prizes      []item.ItemPack // 大礼包（完美完成的奖励）
	Tasks       []*NoviceTask   // 任务
}

// 新手任务
type NoviceTask struct {
	DayIndex       int             // 天数索引
	Action         int             // 动作
	Guide          string          // 跳转
	Target         int             // 目标达成数
	Awards         []item.ItemPack // 福利奖励
	task.TaskScope                 // 游戏限制
}

// 是否适用
func (t *NoviceTask) IsApplicative(param task.TaskScope) bool {
	if len(t.GameNames) == 0 {
		return false
	}
	for _, v := range t.GameNames {
		if v == param.GameName {
			return true
		}
	}
	return false
}

const (
	TaskStatus_Locked      = iota // 0=未解锁
	TaskStatus_Uncompleted        // 1=未完成
	TaskStatus_Completed          // 2=完成
	TaskStatus_Claimed            // 3=已领取奖励
)

// 用户新手任务时间表
type UserNoviceTaskSchedule struct {
	DayIndex       int // 天数索引
	Scheduled      int // 完成进度
	Status         int // 任务的状态
	UnlockDayIndex int // 解锁天索引
}

// 用户新手福利
type UserNoviceWelfare struct {
	DaysRemaining  int                       // 剩余天数
	IsFinalPackage bool                      // 是否最终礼包
	Schedule       []*UserNoviceTaskSchedule // 任务时间表
}
