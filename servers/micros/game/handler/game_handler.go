package handler

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/cz88ip"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/game/proto"
	"bet24.com/servers/transaction"
	"bet24.com/utils"
	"context"
	"encoding/json"
	"fmt"
	"sort"
	"sync"
	"time"
)

var instance *Game
var mgr *gamemgr

func GetInstance() *Game {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func (g *Game) dump(cmd, param1, param2 string) {
	switch cmd {
	case "list":
		for _, v := range g.games {
			log.Release("%+v", v)
		}
	default:
		log.Release("game.Dump unhandled cmd %s", cmd)
	}
}

func (g *Game) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (g *Game) GetGame(ctx context.Context, req *pb.Request_GetGame, rsp *pb.Response_GetGame) error {
	g.lock.RLock()
	defer g.lock.RUnlock()
	for k, v := range g.games {
		if req.GameID == v.GameID {
			rsp.Info = &g.games[k]
			return nil
		}
	}
	return nil
}

func (g *Game) GetGameJson(ctx context.Context, req *pb.Request_GetGameJson, rsp *pb.Response) error {
	g.lock.RLock()
	defer g.lock.RUnlock()
	if len(g.games) <= 0 {
		return nil
	}

	var games []pb.GameInfo
	games = append(games, g.games...)

	/* 暂时注释
	// 非元宝场才显示元宝大厅
	if config.Server.IsChipRoom == 0 && GameReq.GetReviewGame(userIp, partnerId, versionCode) == "" {
		if addr := coreservice.GetGameAddr(userId); addr != "" {
			games = append(games, transaction.GameInfo{
				GameID:      chip_gameid,
				ChineseName: "chiphall",
				EnglishName: "chiphall",
				ServerAddr:  addr,
			})
		}
	}
	*/

	retData, err := json.Marshal(games)
	if err != nil {
		log.Release("GetGameJson GetJson Marshal failed")
		return err
	}

	rsp.Data = string(retData)
	return nil
}

func (g *Game) GetGameList(ctx context.Context, req *pb.Request, rsp *pb.Response_GetGameList) error {
	g.lock.RLock()
	defer g.lock.RUnlock()
	rsp.List = g.games
	return nil
}

func newHandler() *Game {
	ret := new(Game)
	ret.ctor()
	return ret
}

type Game struct {
	games        []pb.GameInfo
	lock         *sync.RWMutex
	gameRequests []transaction.GameRequestInfo
}

func (g *Game) ctor() {
	mgr = newGameMgr()
	g.lock = &sync.RWMutex{}
	g.refreshData()
}

func (g *Game) refreshData() {
	go g.doRefresh()
	time.AfterFunc(5*time.Minute, g.refreshData)
}

func (g *Game) doRefresh() {
	gameList := g.getGameListFromDB(false)
	objList := transaction.NewTransGameRequest()
	objList.DoAction(nil)
	g.lock.Lock()
	defer g.lock.Unlock()
	g.games = gameList

	if len(objList.OUT) > 0 {
		g.gameRequests = objList.OUT
	} else {
		g.gameRequests = []transaction.GameRequestInfo{}
	}
}

func (g *Game) getGameList(partnerId int) []pb.GameInfo {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllGame_GetList")
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, partnerId)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	jsonData := dbengine.Execute(sqlstring)
	var list []pb.GameInfo
	json.Unmarshal([]byte(jsonData), &list)
	for i := 0; i < len(list); i++ {
		displaySort := list[i].DisplaySort
		if len(displaySort) >= 5 {
			list[i].GoldSort = displaySort[1:3]
			list[i].ChipSort = displaySort[3:]
		}
	}
	return list
}

func (g *Game) getGameListFromDB(isChipRoom bool) []pb.GameInfo {
	act := g.getGameList(0)
	var ret []pb.GameInfo
	for _, v := range act {
		if isChipRoom && v.GameType == pb.GameType_Gold {
			continue
		}
		if !isChipRoom && v.GameType == pb.GameType_Chip {
			continue
		}
		ret = append(ret, v)
	}

	if isChipRoom {
		sort.SliceStable(ret, func(i, j int) bool {
			return ret[i].ChipSort < ret[j].ChipSort
		})
	} else {
		sort.SliceStable(ret, func(i, j int) bool {
			return ret[i].GoldSort < ret[j].GoldSort
		})
	}

	return ret
}

// 获取游戏地址
func (g *Game) GetGameAddr(ctx context.Context, req *pb.Request_GetGameAddr, rsp *pb.Response) error {
	rsp.Data = mgr.getAddr(req.UserId)
	return nil
}

// 游戏轮询
func (g *Game) GamePolling(ctx context.Context, req *pb.Request_GamePolling, rsp *pb.Response) error {
	mgr.polling(req.Addr, req.Players)
	return nil
}

func (g *Game) GetReviewGame(ctx context.Context, req *pb.Request_IsInReview, rsp *pb.Response) error {
	rsp.Data = g.getReviewGame(req.UserIp, req.PartnerId, req.VersionCode)
	return nil
}

func (g *Game) getReviewGame(userIp string, PartnerID, VersionCode int) string {
	defer utils.TimeCost(fmt.Sprintf("game.getReviewGame"))()
	country, _ := cz88ip.GetCountryAndRegion(userIp)
	if country == "美国" {
		log.Debug("userIp[%s] is USA", userIp)
		return "game"
	}

	g.lock.Lock()
	defer g.lock.Unlock()
	for _, val := range g.gameRequests {
		if val.PartnerID == PartnerID && val.VersionCode == VersionCode {
			log.Debug("GameRequest[%s,%d,%d] return %s", userIp, PartnerID, VersionCode, val.GameName)
			return val.GameName
		}
	}

	return ""
}

// 根据IP获取地理位置
func (g *Game) GetCountryAndRegion(ctx context.Context, req *pb.Request_GetCountryAndRegion, rsp *pb.Response_GetCountryAndRegion) error {
	rsp.Country, rsp.Region = cz88ip.GetCountryAndRegion(req.IpAddress)
	return nil
}
