package handler

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/game/proto"
	level "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"sync"
	"time"
)

const timeout = 300 // 5分钟

type gamemgr struct {
	lock *sync.RWMutex
	list []*pb.GameAddrInfo
}

func newGameMgr() *gamemgr {
	mgr = &gamemgr{}
	mgr.lock = &sync.RWMutex{}
	go mgr.checkRefresh()
	return mgr
}

func (this *gamemgr) checkRefresh() {
	ticker := time.NewTicker(10 * time.Second)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				this.checkTimeOut()
			}
		}
	}(ticker)
}

// 检查过期
func (this *gamemgr) checkTimeOut() {
	this.lock.Lock()
	defer this.lock.Unlock()
	for idx, val := range this.list {
		if time.Now().Unix()-val.Ts >= timeout {
			this.list = append(this.list[:idx], this.list[idx+1:]...)
			continue
		}
	}
}

// 轮询
func (this *gamemgr) polling(addr string, players int) {
	if addr == "" {
		return
	}

	this.lock.Lock()
	defer this.lock.Unlock()

	for _, v := range this.list {
		if v.Addr != addr {
			continue
		}
		v.Players = players
		v.Ts = time.Now().Unix()
		return
	}

	this.list = append(this.list, &pb.GameAddrInfo{
		Addr:    addr,
		Players: players,
		Ts:      time.Now().Unix(),
	})

	return
}

// 获取地址
func (this *gamemgr) getAddr(userId int) string {
	// 判断是否显示游戏大厅
	if !level.IsShowGameHall(userId) {
		return ""
	}

	this.lock.RLock()
	defer this.lock.RUnlock()
	for _, v := range this.list {
		// 后期加负载均衡算法
		return v.Addr
	}

	return ""
}

func (this *gamemgr) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("gamemgr.dumpSys %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	d, _ := json.Marshal(this.list)
	log.Release(string(d))
}
