package proto

type Request_GetGame struct {
	GameID int
}

type Response_GetGame struct {
	Info *GameInfo
}

type Request_GetGameJson struct {
	UserId      int
	UserIp      string
	PartnerId   int
	VersionCode int
}

type Request_GetGameAddr struct {
	UserId int
}

type Request_GamePolling struct {
	Addr    string
	Players int
}

type Request_IsInReview struct {
	UserIp      string
	VersionCode int
	PartnerId   int
}

type Response_GetGameList struct {
	List []GameInfo
}

type Request_GetCountryAndRegion struct {
	IpAddress string
}

type Response_GetCountryAndRegion struct {
	Country string
	Region  string
}
