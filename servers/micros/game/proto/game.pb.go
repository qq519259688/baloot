package proto

import (
	"context"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "game"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name string
}

type Response struct {
	Data string
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("game failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

func GetGame(gameId int) *GameInfo {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GetGame{
		GameID: gameId,
	}

	reply := &Response_GetGame{}

	err := xclient.Call(context.Background(), "GetGame", args, reply)
	if err != nil {
		log.Debug("game.GetGame failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.Info
}

func GetGameJson(userId int, userIp string, partnerId, versionCode int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GetGameJson{
		UserId:      userId,
		UserIp:      userIp,
		PartnerId:   partnerId,
		VersionCode: versionCode,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetGameJson", args, reply)
	if err != nil {
		log.Debug("game.GetGameJson failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return reply.Data
	}
	return reply.Data
}

// 获取游戏地址
func GetGameAddr(userId int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GetGameAddr{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetGameAddr", args, reply)
	if err != nil {
		log.Debug("game.GetGameAddr failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return reply.Data
	}
	return reply.Data
}

// 游戏轮询
func GamePolling(addr string, players int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GamePolling{
		Addr:    addr,
		Players: players,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GamePolling", args, reply)
	if err != nil {
		log.Debug("game.GamePolling failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return
	}
	return
}

// GetReviewGame
func GetReviewGame(ip string, partnerId, versionCode int) string {
	xclient := getClient()

	args := &Request_IsInReview{
		UserIp:      ip,
		PartnerId:   partnerId,
		VersionCode: versionCode,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetReviewGame", args, reply)
	if err != nil {
		log.Debug("game.GamePolling failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetGameList() []GameInfo {
	xclient := getClient()

	args := &Request{}

	reply := &Response_GetGameList{}

	err := xclient.Call(context.Background(), "GetGameList", args, reply)
	if err != nil {
		log.Debug("game.GamePolling failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
	return reply.List
}

func GetGameNameByGameId(gameId int) string {
	g := GetGame(gameId)
	if g == nil {
		return ""
	}
	return g.EnglishName
}

// 根据IP地址获取国家地区地理位置
func GetCountryAndRegion(ipAddress string) (country, region string) {
	xClient := getClient()

	args := &Request_GetCountryAndRegion{
		IpAddress: ipAddress,
	}

	reply := &Response_GetCountryAndRegion{}

	err := xClient.Call(context.Background(), "GetCountryAndRegion", args, reply)
	if err != nil {
		log.Debug("game.GamePolling failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return
	}

	country = reply.Country
	region = reply.Region
	return
}
