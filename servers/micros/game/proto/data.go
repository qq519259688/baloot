package proto

const (
	GameType_All  = iota // 所有类型
	GameType_Gold        // 金币类型
	GameType_Chip        // 元宝类型
)

type GameInfo struct {
	GameID      int
	ChineseName string
	EnglishName string
	ServerAddr  string
	GameType    int // 游戏类型
	DisplaySort string
	GoldSort    string
	ChipSort    string
	NeedLevel   int
}

type GameAddrInfo struct {
	Addr    string // 地址
	Players int    // 玩家数
	Ts      int64  // 时间戳
}
