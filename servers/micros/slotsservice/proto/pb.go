package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "slotsservice"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	GameName string
	UserId   int
	IsChip   bool
	Amount   int
	Extra    string
}

type Response struct {
	Data    string
	Success bool
	Amount  int
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()

	args := &Request{
		GameName: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("slotsservice.SayHello failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

func GetSlotsConfig(gameName string, userId int) string {
	xclient := getClient()

	args := &Request{
		GameName: gameName,
		UserId:   userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetSlotsConfig", args, reply)
	if err != nil {
		log.Debug("slotsservice.GetSlotsConfig failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	return reply.Data
}

func Spin(gameName string, userId int, amount int, isChip bool, extra string) (bool, int, string) {
	xclient := getClient()

	args := &Request{
		GameName: gameName,
		UserId:   userId,
		Amount:   amount,
		IsChip:   isChip,
		Extra:    extra,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "Spin", args, reply)
	if err != nil {
		log.Debug("slotsservice.Spin failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false, 0, "server error"
	}

	return reply.Success, reply.Amount, reply.Data
}
