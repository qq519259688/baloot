package handler

import (
	"bet24.com/log"
	"bet24.com/servers/micros/slotsservice/handler/slotpanda"
	pb "bet24.com/servers/micros/slotsservice/proto"
	"context"
	"fmt"
)

var instance *Handler

func GetInstance() *Handler {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *Handler {
	ret := new(Handler)
	ret.ctor()
	return ret
}

type Handler struct{}

func (h *Handler) ctor() {
	slotpanda.Run()
}

func (d *Handler) dump(cmd, param1, param2 string) {
	switch cmd {
	case slotpanda.GAME_NAME:
		slotpanda.Dump(param1, param2)

	default:
		log.Release("Handler.Dump unhandled cmd %s", cmd)
	}
}

func (h *Handler) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.GameName)
	return nil
}

func (h *Handler) GetSlotsConfig(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = GetSlotConfig(req.GameName, req.UserId)
	return nil
}

func (h *Handler) Spin(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success, rsp.Amount, rsp.Data = Spin(req.GameName, req.UserId, req.Amount, req.IsChip, req.Extra)
	return nil
}
