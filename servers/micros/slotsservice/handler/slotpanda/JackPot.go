package slotpanda

import (
	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/micros/slotsservice/handler/slotcommon/betlevel"
	"bet24.com/servers/micros/slotsservice/handler/slotcommon/slotcount"
	"math/rand"
)

const (
	JackpotLevelNone = iota // 不产生jackpot
	JackpotLevelMini
	JackpotLevelMinor
	JackpotLevelMajor
	JackpotLevelGrand
)

type JackpotManager struct {
	jackpotSlotId         int
	jackpotSlotCount      int
	grandJackpotPercent   int
	inGrandJackpotPercent int
	slotCounts            *slotcount.MultipleSlotCountManager
	betLevelManager       *betlevel.BetLevelManager
}

func newJackpotManager(slotId, count int, slotCounts *slotcount.MultipleSlotCountManager, levelManager *betlevel.BetLevelManager) *JackpotManager {
	ret := new(JackpotManager)
	ret.jackpotSlotCount = count
	ret.jackpotSlotId = slotId
	ret.slotCounts = slotCounts
	ret.betLevelManager = levelManager
	ret.loadData()
	return ret
}

func (jm *JackpotManager) loadData() {
	// 从redis读取jackpot数量
	jm.inGrandJackpotPercent = 1
	jm.grandJackpotPercent = 10
}

func (jm *JackpotManager) getAmount(isChipRoom bool) int {
	return coreservice.GetJackpotAmount(GAMEID, isChipRoom)
}

func (jm *JackpotManager) isJackpot(slots []int) bool {
	bonusCount := 0
	for _, v := range slots {
		if v%100 == jm.jackpotSlotId {
			bonusCount++
		}
	}
	return bonusCount >= jm.jackpotSlotCount
}

func (jm *JackpotManager) addJackpot(slots []int, betAmount int, userId int, isChipRoom bool) (bool, *Bonus) {
	bonusCount := 0
	for _, v := range slots {
		if v%100 == jm.jackpotSlotId {
			bonusCount++
		}
	}
	if bonusCount > 0 {
		jm.modifyAmount(betAmount*jm.inGrandJackpotPercent/100, userId, isChipRoom)
	}

	// 触发bonus
	if bonusCount >= jm.jackpotSlotCount {
		return bonusCount > 0, jm.createBonus(slots, betAmount, userId, isChipRoom)
	}
	return bonusCount > 0, nil
}

func (jm *JackpotManager) modifyAmount(amount int, userId int, isChipRoom bool) {
	coreservice.ModifyJackpot(amount, GAMEID, userId, "slotpanda bet", isChipRoom)
}

func (jm *JackpotManager) obtainJackpot(betAmount int, jackpotLevel int, userId int, isChipRoom bool) int {
	switch jackpotLevel {
	case JackpotLevelMini:
		return betAmount * 40
	case JackpotLevelMinor:
		return betAmount * 70
	case JackpotLevelMajor:
		return betAmount * 100
	case JackpotLevelGrand:
		amount := jm.getAmount(isChipRoom) / 100 * jm.grandJackpotPercent
		log.Debug("JackpotManager grand jackpot %d", amount)
		jm.modifyAmount(-amount, userId, isChipRoom)
		return betAmount*200 + amount
	}
	return 0
}

func (jm *JackpotManager) createBonus(slots []int, betAmount int, userId int, isChipRoom bool) *Bonus {
	level := 0
	sc := jm.slotCounts.GetMgr(level)

	bonus := newBonus()
	bonus.RespinCount = 3
	bonus.leftRespinCount = bonus.RespinCount
	bonus.BetAmount = betAmount
	var initFrame Bonus_Frame
	for i := 0; i < len(slots); i++ {
		if slots[i]%100 == jm.jackpotSlotId {
			initFrame.addBonus(i, slots[i]/100)
		}
	}
	bonus.addFrame(initFrame)
	//betLevel := jm.betLevelManager.getBetLevel(betAmount)

	// 生成所有frame
	for {
		if bonus.leftRespinCount <= 0 {
			break
		}
		var frame Bonus_Frame
		for i := 0; i < 4; i++ {
			for j := 0; j < 15; j++ {
				pos := i*15 + j
				if bonus.isPass(pos) {
					continue
				}
				slotId := sc.GetOneSlot(level, j)
				if slotId == jm.jackpotSlotId {
					frame.addBonus(pos, jm.getRandomMultiple())
				}
			}
		}
		bonus.addFrame(frame)

		if bonus.getJackpotLevel() == JackpotLevelGrand {
			break
		}

		if frame.isEmpty() {
			bonus.leftRespinCount--
		} else {
			bonus.leftRespinCount = bonus.RespinCount
		}
	}

	bonus.JackpotLevel = bonus.getJackpotLevel()
	bonus.JackpotResult = jm.obtainJackpot(betAmount, bonus.JackpotLevel, userId, isChipRoom)

	return bonus
}

func (jm *JackpotManager) getRandomMultiple() int {
	r := rand.Intn(100)
	if r < 30 {
		return 0
	}
	r -= 30

	if r < 12 {
		return 1
	}
	r -= 12

	if r < 10 {
		return 2
	}
	r -= 10

	if r < 9 {
		return 3
	}
	r -= 9

	if r < 9 {
		return 4
	}
	r -= 9

	if r < 7 {
		return 5
	}
	r -= 7

	if r < 6 {
		return 6
	}
	r -= 6

	if r < 6 {
		return 7
	}
	r -= 6

	if r < 5 {
		return 8
	}
	r -= 5

	if r < 4 {
		return 9
	}

	return 10
}
