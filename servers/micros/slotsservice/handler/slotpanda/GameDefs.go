package slotpanda

import (
	"bet24.com/log"
)

const (
	COLUMN_COUNT = 5
	ROW_COUNT    = 3
	RESULT_COUNT = 15
	GAMEID       = 55
	GAME_NAME    = "bambooslot"
	GAME_MESSAGE = "pandaslot"
	LogColor     = log.Blue
)

type SlotCount struct {
	SlotID int
	Count  int
}

// 中奖连线信息
type Result struct {
	SlotID     int // 格子ID
	SlotCount  int // 数量
	WinShapeID int // 连线ID
	WinRate    int // 赢的倍数
}

// 错误码
const (
	ErrCode_InvalidBet     = 1 // 玩家投注不合理
	ErrCode_NotEnoughMoney = 2 // 玩家金币不足
	ErrCode_NotExist       = 3 // 玩家已退出不存在
)

type SlotPanda_Bet struct {
	Amount   int
	ErrorMsg string
	ErrCode  int
}

type SlotPanda_Result struct {
	BetAmount     int      // 下注金额
	WinAmount     int      // 共赢金额
	Slots         []int    // 15个格子ID
	Lines         []Result // 中奖连线
	FreeSpinInner int      // 如果产生freespin，内圈倍数
	FreeSpinOuter int      // 如果产生freespin，外圈倍数  不产生为0
	Bonus         *Bonus
}

type TestSlots struct {
	Slots []int
}
