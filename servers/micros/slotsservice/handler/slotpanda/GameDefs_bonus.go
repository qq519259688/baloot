package slotpanda

// respin触发时单个Bonus信息
type BonusInfo struct {
	Pos      int // 位置：0-60 ，分4个区域
	Multiple int // 倍率
}

// respin每次触发产生的一帧中包含bonus的信息，如果没有则表示本轮不产生bonus
type Bonus_Frame struct {
	Bonuses []BonusInfo
}

func (bf *Bonus_Frame) addBonus(pos, mul int) {
	bf.Bonuses = append(bf.Bonuses, BonusInfo{Pos: pos, Multiple: mul})
}

func (bf *Bonus_Frame) isEmpty() bool {
	return len(bf.Bonuses) == 0
}

type Bonus struct {
	BetAmount       int           // 下注额
	RespinCount     int           // 最大respin次数
	Frames          []Bonus_Frame // 中间动画帧
	ResultBonuses   []BonusInfo   // 最终结果
	BonusResult     int           // bonus产生的赔付
	JackpotResult   int           // Jackpot赔付
	JackpotLevel    int           // Jackpot等级
	leftRespinCount int
	bonusIndex      []bool
}

func newBonus() *Bonus {
	ret := new(Bonus)
	ret.bonusIndex = make([]bool, 60)
	return ret
}

func (b *Bonus) addFrame(frame Bonus_Frame) {
	b.Frames = append(b.Frames, frame)
	b.ResultBonuses = append(b.ResultBonuses, frame.Bonuses...)
	for _, v := range frame.Bonuses {
		b.bonusIndex[v.Pos] = true
		b.BonusResult += v.Multiple * b.BetAmount
	}
}

func (b *Bonus) isPass(pos int) bool {
	return b.bonusIndex[pos]
}

func (b *Bonus) getJackpotLevel() int {
	fullCount := 0
	for i := 0; i < 4; i++ {
		full := true
		for j := 0; j < 15; j++ {
			if !b.bonusIndex[i*15+j] {
				full = false
				break
			}
		}
		if full {
			fullCount++
		}
	}
	if fullCount == 4 {
		return JackpotLevelGrand
	}
	if fullCount == 3 {
		return JackpotLevelMajor
	}
	if fullCount == 2 {
		return JackpotLevelMinor
	}
	if fullCount == 1 {
		return JackpotLevelMini
	}
	return JackpotLevelNone
}
