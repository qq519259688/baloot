package slotpanda

func Run() {
	getGameLogic()
}

func GetSlotConfig(userId int) string {
	return getGameLogic().getSlotConfig(userId)
}

func Spin(userId int, amount int, isChip bool, extra string) (bool, int, string) {
	return getGameLogic().spin(userId, amount, isChip, extra)
}

func Dump(param1, param2 string) {
	getGameLogic().dump(param1, param2)
}
