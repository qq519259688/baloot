package slotpanda

import (
	"encoding/json"

	"bet24.com/log"
)

func (this *GameLogic) spin(userId int, amount int, isChip bool, extra string) (bool, int, string) {
	// 是否有免费次数
	free, freeSpinResult, isChipFree := this.useFreeSpin(userId)
	if free {
		return true, 0, this.handleFreeResult(userId, freeSpinResult, isChipFree)
	}

	if amount <= 0 {
		return false, 0, this.sendSlotBet(userId, "Invalid Bet!", ErrCode_InvalidBet, amount)
	}

	scoreType := GAMEID*100 + 1
	status := 1
	isSuceeded := this.slotCommon.WriteMoney(userId, -amount, 0, status, scoreType, isChip)

	if !isSuceeded {
		log.Release("handleSlotBet WriteMoney failed,UserId[%d],amount[%d]", userId, amount)
		// 扣金币失败
		return false, 0, this.sendSlotBet(userId, "Not enough money!", ErrCode_NotEnoughMoney, amount)
	}
	// 获取结果并写分
	return true, amount, this.handleSlotResult(userId, amount, isChip)
}

func (this *GameLogic) sendSlotBet(userId int, errMsg string, errCode, amount int) string {
	data, _ := json.Marshal(SlotPanda_Bet{Amount: amount, ErrorMsg: errMsg, ErrCode: errCode})
	return string(data)
}

func (this *GameLogic) handleFreeResult(userId int, result FreeSpinResult, isChip bool) string {
	// 写分
	amount := result.getWinAmount()
	this.slotCommon.WriteResult(userId, result.BetAmount, amount, true, isChip, result.getDesc(), GAMEID)

	// 发送结果给客户端
	return this.sendFreeSlotResult(userId, result)
}

func (this *GameLogic) sendFreeSlotResult(userId int, result FreeSpinResult) string {
	data, _ := json.Marshal(result)
	return string(data)
}

func (this *GameLogic) handleSlotResult(userId int, amount int, isChip bool) string {
	//defer utils.TimeCost(fmt.Sprintf("slotpanda.handleSlotResult %d", userId))()
	controlType := this.slotCommon.GetControlType(userId, GAMEID)
	result, ok := this.getResult(userId, amount, controlType, isChip)
	if !ok {
		return this.sendSlotBet(userId, "User Not Exist", ErrCode_NotExist, amount)
	}
	bonusAmount := 0
	if result.Bonus != nil {
		bonusAmount = result.Bonus.BonusResult + result.Bonus.JackpotResult
	}

	this.slotCommon.WriteResult(userId, amount, result.WinAmount+bonusAmount, false, isChip, this.getResultDesc(result), GAMEID)

	// 发送结果给客户端
	return this.sendSlotResult(userId, result)
}

func (this *GameLogic) sendSlotResult(userId int, result SlotPanda_Result) string {
	data, _ := json.Marshal(result)
	return string(data)
}
