package handler

import (
	"bet24.com/log"
	"bet24.com/servers/micros/slotsservice/handler/slotpanda"
)

// 获取slot配置，包括Slots，WinShapres，Wins，BetLevels
func GetSlotConfig(slotGame string, userId int) string {
	log.Debug("GetSlotConfig %s,%d", slotGame, userId)
	switch slotGame {
	case slotpanda.GAME_NAME:
		return slotpanda.GetSlotConfig(userId)
	default:
		log.Release("GetSlotConfig %s not exist", slotGame)
	}
	return ""
}

// 下注并返回结果
func Spin(slotGame string, userId int, amount int, isChip bool, extra string) (success bool, spinAmount int, resultData string) {
	switch slotGame {
	case slotpanda.GAME_NAME:
		return slotpanda.Spin(userId, amount, isChip, extra)
	default:
		log.Release("Spin %s not exist", slotGame)
	}
	return false, 0, "invalid game"
}
