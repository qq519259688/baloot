package slotcount

import (
	"encoding/json"
	"fmt"
	"os"

	"bet24.com/log"
)

type mulcounts struct {
	Level      int
	SlotCounts [][][]int
}

type MultipleSlotCountManager struct {
	mgr_list []*SlotCountManager
}

func NewMultipleSlotCountManager(configFile string) *MultipleSlotCountManager {
	ret := new(MultipleSlotCountManager)
	ret.loadData(configFile, 0, 0)
	return ret
}

func NewFafafaMultipleSlotCountManager(configFile string, goldFaId int, freeSlotId int) *MultipleSlotCountManager {
	ret := new(MultipleSlotCountManager)
	if ret.loadData(configFile, goldFaId, freeSlotId) {
		return ret
	}
	return nil
}

func (mscm *MultipleSlotCountManager) loadData(configFile string, goldFaId int, freeSlotId int) bool {
	defer func() {
		//log.Debug("MultipleSlotCountManager.loadData(%s) manager count = %d", configFile, len(mscm.mgr_list))
	}()
	data, err := os.ReadFile(fmt.Sprintf("slotconf/%s.json", configFile))
	if err != nil {
		log.Debug("MultipleSlotCountManager.loadData read %s.json failed", configFile)
		return false
	}

	var multipleCounts []mulcounts

	err = json.Unmarshal(data, &multipleCounts)
	if err != nil {
		log.Release("MultipleSlotCountManager.loadData Unmarshal %s.json failed err:%v", configFile, err)
		return false
	}

	for _, v := range multipleCounts {
		sc := NewSlotCountManagerByConfig(v.Level, v.SlotCounts)
		sc.setFafafaInfo(goldFaId, freeSlotId)
		mscm.mgr_list = append(mscm.mgr_list, sc)
	}
	return true
}

func (mscm *MultipleSlotCountManager) GetMgr(level int) *SlotCountManager {
	count := len(mscm.mgr_list)
	if count == 0 {
		return nil
	}

	if count == 1 {
		return mscm.mgr_list[0]
	}

	for i := 0; i < len(mscm.mgr_list); i++ {
		if level == mscm.mgr_list[i].Level {
			return mscm.mgr_list[i]
		}
	}
	return mscm.mgr_list[0]
}
