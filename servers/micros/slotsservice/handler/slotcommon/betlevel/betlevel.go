package betlevel

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"

	"bet24.com/log"
)

// 下注level
type BetLevel struct {
	Bet     int // 下注额
	Level   int
	Base    int  // 赔付低分
	IsValid bool // 是否可用，新手玩家只有一个投注额可用
}

type BetLevelManager struct {
	levels       []BetLevel
	newbieLevels []BetLevel
}

func NewBetLevelManagerByData(levels []BetLevel) *BetLevelManager {
	ret := new(BetLevelManager)
	ret.levels = levels
	ret.sortLevels()
	return ret
}

func NewBetLevelManager(configFile string) *BetLevelManager {
	//log.Debug("newBetLevelManager %s start", configFile)
	ret := new(BetLevelManager)
	if !ret.loadData(configFile) {
		return nil
	}
	log.Debug("newBetLevelManager %s ok", configFile)
	return ret
}

func (bm *BetLevelManager) loadData(configFile string) bool {
	data, err := os.ReadFile(fmt.Sprintf("slotconf/%s.json", configFile))
	if err != nil {
		log.Release("BetLevelManager.loadData read %s failed", configFile)
		return false
	}

	err = json.Unmarshal(data, &bm.levels)
	if err != nil {
		log.Release("BetLevelManager.loadData Unmarshal %s failed err:%v", configFile, err)
		return false
	}
	bm.sortLevels()
	return true
}

func (bm *BetLevelManager) sortLevels() {
	// 从大到小排序
	sort.Slice(bm.levels, func(i, j int) bool {
		return bm.levels[i].Bet > bm.levels[j].Bet
	})
	//

	bm.newbieLevels = make([]BetLevel, len(bm.levels))
	copy(bm.newbieLevels, bm.levels)

	for i := 0; i < len(bm.levels); i++ {
		bm.levels[i].IsValid = true
		bm.newbieLevels[i].IsValid = false
	}
	bm.newbieLevels[len(bm.newbieLevels)-1].IsValid = true

	//log.Debug("BetLevelManager sortLevels %v,%v", bm.levels, bm.newbieLevels)
}

func (bm *BetLevelManager) GetLevel(amount int) int {
	for _, v := range bm.levels {
		if amount == v.Bet {
			return v.Level
		}
	}
	return -1
}

func (bm *BetLevelManager) GetLevelAndBase(amount int) (int, int) {
	for _, v := range bm.levels {
		if amount == v.Bet {
			base := v.Base
			if base == 0 {
				base = amount
			}
			return v.Level, base
		}
	}
	return 0, 0
}

func (bm *BetLevelManager) GetAmountByLevel(level int) int {
	for _, v := range bm.levels {
		if level == v.Level {
			return v.Bet
		}
	}

	return bm.GetLowestBet()
}

func (bm *BetLevelManager) GetLowestBet() int {
	if len(bm.levels) == 0 {
		return 0
	}

	return bm.levels[len(bm.levels)-1].Bet
}

func (bm *BetLevelManager) GetLowestLevel() BetLevel {
	if len(bm.levels) == 0 {
		return BetLevel{}
	}
	return bm.levels[len(bm.levels)-1]
}

func (bm *BetLevelManager) Levels() []BetLevel {
	return bm.levels
}

func (bm *BetLevelManager) NewbieLevels() []BetLevel {
	return bm.newbieLevels
}
