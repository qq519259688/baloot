package main

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/slotsservice/proto"
)

func main() {
	log.Debug(pb.SayHello("slotsservice client"))
}
