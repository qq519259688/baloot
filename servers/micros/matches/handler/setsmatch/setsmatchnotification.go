package setsmatch

import (
	"bet24.com/log"
	"bet24.com/servers/micros/matches/handler/matchbase"
	notification "bet24.com/servers/micros/notification/proto"
	"encoding/json"
)

func postUserEnterNotification(userId int, matchNo int, nickname string, faceId int, faceUrl string) {
	mi := getMatchManager().getMatch(matchNo)
	if mi == nil {
		log.Release("postUserEnterNotification matchNo[%d] not found", matchNo)
		return
	}
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_userenter, UserId: userId,
		FaceId: faceId, NickName: nickname, FaceUrl: faceUrl}
	d, _ := json.Marshal(ni)
	mi.sendNotification(-1, string(d))
}

func postUserExitNotification(userId int, matchNo int) {
	mi := getMatchManager().getMatch(matchNo)
	if mi == nil {
		log.Release("postUserExitNotification matchNo[%d] not found", matchNo)
		return
	}
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_userexit, UserId: userId}
	d, _ := json.Marshal(ni)
	mi.sendNotification(-1, string(d))
	// 发给自己
	notification.AddNotification(userId, notification.Notification_Match, string(d))
}

func postMatchStatusChangedNotificaiton(matchNo int, oldStatus, newStatus int) {
	mi := getMatchManager().getMatch(matchNo)
	if mi == nil {
		log.Release("postMatchStatusChangedNotificaiton matchNo[%d] not found", matchNo)
		return
	}
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_statuschanged, OldStatus: oldStatus, NewStatus: newStatus}
	d, _ := json.Marshal(ni)
	mi.sendNotification(-1, string(d))
}

func postUserScoreChangedNotification(userId int, matchNo int, score int) {
	mi := getMatchManager().getMatch(matchNo)
	if mi == nil {
		log.Release("postUserScoreChangedNotification matchNo[%d] not found", matchNo)
		return
	}
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_scorechanged, UserId: userId, Score: score}
	d, _ := json.Marshal(ni)
	mi.sendNotification(-1, string(d))
}

func postUserPromotionNotification(userId int, matchNo int) {
	mi := getMatchManager().getMatch(matchNo)
	if mi == nil {
		log.Release("postUserPromotionNotification matchNo[%d] not found", matchNo)
		return
	}
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_promoted}
	d, _ := json.Marshal(ni)
	mi.sendNotification(userId, string(d))
}
func postUserEliminationNotification(userId int, matchNo int, rank int) {
	mi := getMatchManager().getMatch(matchNo)
	if mi == nil {
		log.Release("postUserEliminationNotification matchNo[%d] not found", matchNo)
		return
	}
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_eliminated, Rank: rank}
	d, _ := json.Marshal(ni)
	mi.sendNotification(userId, string(d))
}

func postUserRankNotification(userId int, matchNo int, rank int, prize int) {
	mi := getMatchManager().getMatch(matchNo)
	if mi == nil {
		log.Release("postUserRankNotification matchNo[%d] not found", matchNo)
		return
	}
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_rank, Rank: rank, Prize: prize}
	d, _ := json.Marshal(ni)
	mi.sendNotification(userId, string(d))
}

func postUserMatchRoomNotification(userId int, matchNo int, serverAddr string, tableId, chairId int) {
	mi := getMatchManager().getMatch(matchNo)
	if mi == nil {
		log.Release("postUserMatchRoomNotification matchNo[%d] not found", matchNo)
		return
	}
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_matchroom, ServerAddr: serverAddr, TableId: tableId, ChairId: chairId}
	d, _ := json.Marshal(ni)
	mi.sendNotification(userId, string(d))
}
