package setsmatch

import (
	"bet24.com/servers/micros/matches/handler/matchbase"
	"time"
)

type matchuser struct {
	matchbase.MatchUser
	RoomNo int
	// 私人场房间信息
	ServerAddr string
	TableId    int
	ChairId    int

	prize          int
	tax            int
	lastEndTime    int // 上次比赛结束时间
	lastScore      int
	lastArrageTime int // 上次被拉去匹配时间
}

func newMatchUser(userId int, nickName string, faceId int, faceUrl string, initialScore int, baseScore int) *matchuser {
	return &matchuser{
		MatchUser: matchbase.MatchUser{
			UserId:    userId,
			NickName:  nickName,
			FaceUrl:   faceUrl,
			FaceId:    faceId,
			Score:     initialScore,
			BaseScore: baseScore,
		},
	}
}

func (mu *matchuser) arrangeRoom(roomNo int, serverAddr string, tableId int, chairId int) {
	mu.RoomNo = roomNo
	mu.ServerAddr = serverAddr
	mu.TableId = tableId
	mu.ChairId = chairId
}

func (mu *matchuser) clearRoomInfo() {
	mu.ServerAddr = ""
	mu.TableId = -1
	mu.ChairId = -1
	mu.RoomNo = 0
	//mu.Score = 0
	//mu.TotalScore += mu.Score
	mu.lastEndTime = int(time.Now().Unix())
}

func (mu *matchuser) getTotalScore() int {
	return mu.Score
}

func (mu *matchuser) getUserLastScore() int {
	return mu.lastScore
}

func (mu *matchuser) addScore(score int) {
	mu.lastScore = score
	mu.Score += score
}
