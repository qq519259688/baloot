package setsmatch

import (
	"bet24.com/database"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"fmt"
	"time"
)

// 月卡领取
func writeRoomRecordToDB(hi *matchinstance) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SetsMatch_WriteHistory")
	statement.AddParamter("@MatchNo", database.AdParamInput, database.AdVarChar, 32, fmt.Sprintf("%d", hi.MatchNo))
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, hi.GameId)
	statement.AddParamter("@OwnerUserID", database.AdParamInput, database.AdInteger, 4, hi.Owner)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdInteger, 4, hi.StartTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdInteger, 4, time.Now().Unix())
	statement.AddParamter("@TotalAwardAmount", database.AdParamInput, database.AdBigint, 8, hi.Prize)
	statement.AddParamter("@TotalFee", database.AdParamInput, database.AdBigint, 8, hi.Fee*hi.TotalUser)
	statement.AddParamter("@TotalTax", database.AdParamInput, database.AdBigint, 8, hi.getTotalTax())
	statement.AddParamter("@UserInfo", database.AdParamInput, database.AdVarChar, 1024, hi.getUsersDescForDB())
	statement.AddParamter("@ExtInfo", database.AdParamInput, database.AdVarChar, 1024, hi.getUsersDesc())
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdInteger, 4, hi.createTime)
	sqlstring := statement.GenSql()
	dbengine.Execute(sqlstring)
}
