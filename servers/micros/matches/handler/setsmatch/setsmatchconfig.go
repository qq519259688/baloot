package setsmatch

import (
	"bet24.com/log"
	"bet24.com/servers/micros/matches/handler/matchbase"
)

type set_config struct {
	SetIndex       int // 局数
	EliminateScore int // 淘汰分
	BaseScore      int // 低分
}

type matchconfig struct {
	GameId       int
	GameName     string
	UserOptions  []matchbase.UserOption
	GameRules    []matchbase.MatchRule // 从privateroom取
	InitialScore int                   // 初始分数
	SetConfigs   []set_config
}

func (mc *matchconfig) isEliminated(setCount int, score int) bool {
	for i := 0; i < len(mc.SetConfigs); i++ {
		if setCount == mc.SetConfigs[i].SetIndex {
			return score < mc.SetConfigs[i].EliminateScore
		}
	}
	return false
}

func (mc *matchconfig) getBaseScore(setCount int) int {
	count := len(mc.SetConfigs)
	if count == 0 {
		return 0
	}
	for i := 0; i < len(mc.SetConfigs); i++ {
		if setCount == mc.SetConfigs[i].SetIndex {
			return mc.SetConfigs[i].BaseScore
		}
	}
	// 没找到，取最后一个
	return mc.SetConfigs[count-1].BaseScore
}

func (mc *matchconfig) addGameRule(gameRule string, ruleDesc string, targetOptions []int, userOptions []int, playTimeOptions []int) {
	//log.Release("matchconfig addGameRule gameRule = %s,ruleDesc = %s", gameRule, ruleDesc)
	for i := 0; i < len(mc.GameRules); i++ {
		if mc.GameRules[i].Name == gameRule {
			mc.GameRules[i].Desc = ruleDesc
			mc.GameRules[i].UserOptions = userOptions
			mc.GameRules[i].PlayTimeOptions = playTimeOptions
			return
		}
	}

	mc.GameRules = append(mc.GameRules, matchbase.MatchRule{Name: gameRule, Desc: ruleDesc, TargetOptions: targetOptions, UserOptions: userOptions, PlayTimeOptions: playTimeOptions})
}

func (mc *matchconfig) dump() {
	log.Release("    GameId:%d GameName:%s,UserOptions[%v]", mc.GameId, mc.GameName, mc.UserOptions)
	log.Release("    %v", mc.SetConfigs)
}

func (mc *matchconfig) isValidParams(gameRule string, totalUserCount, tableUserCount, playTime int) bool {
	var mr *matchbase.MatchRule
	for i := 0; i < len(mc.GameRules); i++ {
		if mc.GameRules[i].Name == gameRule {
			mr = &mc.GameRules[i]
			break
		}
	}
	if mr == nil {
		log.Release("setsmatch.matchconfig.isValidParams gamerRule[%s] not found", gameRule)
		return false
	}

	if !mc.isValueIn(tableUserCount, mr.UserOptions) {
		log.Release("matchconfig.isValidParams tableUserCount[%d] not of [%v]", tableUserCount, mr.UserOptions)
		return false
	}

	return true
}

func (mc *matchconfig) isValueIn(value int, values []int) bool {
	if len(values) == 0 {
		return true
	}
	for i := 0; i < len(values); i++ {
		if values[i] == value {
			return true
		}
	}
	return false
}
