package setsmatch

import (
	"bet24.com/log"
	"bet24.com/servers/micros/matches/handler/matchbase"
)

func Run() {
	log.Release("setsmatch.Run")
	getMatchManager().run()
	getHistoryManager().run()
}

func Dump(param1, param2 string) {
	if param1 == "history" {
		getHistoryManager().dump(param2)
		return
	}
	getMatchManager().dump(param1, param2)
}

// 接口
// 创建比赛，返回比赛ID
func CreateMatch(userId int, gameId int, gameRule string, totalUserCount int, tableUserCount int,
	enrollFee int, prize int, playTimeout int, winnerCount int) (int, string) {
	return getMatchManager().createMatch(userId, gameId, gameRule, totalUserCount, tableUserCount, enrollFee, prize, playTimeout,
		winnerCount)
}

func EnrollMatch(userId int, nickname string, faceId int, faceUrl string, matchNo int) (int, string) {
	ok, errMsg := getMatchManager().enrollMatch(userId, nickname, faceId, faceUrl, matchNo)
	if ok {
		return 1, errMsg
	}
	return 0, errMsg
}

func CloseMatch(userId int, matchNo int) (int, string) {
	ok, errMsg := getMatchManager().closeMatchByOwner(userId, matchNo)
	if ok {
		return 1, errMsg
	}
	return 0, errMsg
}

func QuitMatch(userId int, matchNo int) bool {
	return getMatchManager().quitMatch(userId, matchNo)
}

func GetMatchInfo(matchNo int) string {
	return getMatchManager().getMatchInfo(matchNo)
}

func GetUserMatches(userId int) string {
	return getMatchManager().getUserMatches(userId)
}

func GetMatchConfigs() string {
	return getMatchManager().getMatchConfigs()
}

func Flush() {
	getHistoryManager().forceFlush()
}

func GetEnrolledMatch(userId int) string {
	return getMatchManager().getEnrolledMatch(userId)
}

func GetMatchHistory(userId int) string {
	return getHistoryManager().getHistory(userId)
}

func GetMatchInstance(matchNo int) matchbase.MatchInstance {
	return getMatchManager().getMatchInstance(matchNo)
}

func DismissMatch(matchNo int) {
	getMatchManager().dismissMatch(matchNo)
}
