package combomatch

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
	"bet24.com/servers/micros/matches/handler/matchbase"
	"time"
)

type matchroundconfig struct {
	MatchType               int
	Target                  int // 目标，淘汰赛
	WinnerCount             int // 本轮晋级玩家
	EliminateScore          int // 淘汰分数
	ShrinkSec               int // 缩圈秒数
	ShrinkScore             int // 缩圈淘汰分数
	ScorePercentToNextRound int // 本轮累积多少分数进入下一轮
	EleminateByScore        bool
}

type matchconfig struct {
	MatchId   int
	Name      string
	Desc      string
	GameId    int
	GameName  string
	GameRule  string
	TotalUser int // 实际比赛人数，会根据最终报名人数改变
	TableUser int
	PlayTime  int
	Rounds    []matchroundconfig // 轮次组合
	EnrollFee []item.ItemPack
	Prizes    []matchbase.Prize_config
	matchbase.Time_config
	EnrollMin int // 最小报名人数
	EnrollMax int // 最大报名人数

	// 扩展部分
	OnlineUser       int
	TotalPrizeAmount int
	DailyFreeCount   int // 每日免费次数
	LeftFreeCount    int // 剩余免费次数

	RobotConfig *matchbase.Robot_config `json:",omitempty"`

	StartSec   int    `json:",omitempty"` // 多少秒后开始，只有预报名才有
	EnrollOpen bool   // 当前是否可报名
	MatchIcon  string `json:",omitempty"` // 比赛Icon，空表示没有
}

func (mc *matchconfig) getScorePercentToNext(round int) int {
	if round < 0 || round >= len(mc.Rounds) {
		return 0
	}
	return mc.Rounds[round].ScorePercentToNextRound
}

func (mc *matchconfig) isEleminateByScore(round int) bool {
	if round < 0 {
		round = 0
	}
	return mc.Rounds[round].EleminateByScore
}

func (mc *matchconfig) dump() {
	log.Release("    MatchId[%d],Name[%s],GameName[%s],TotalUser[%d],MinMax[%d,%d],TableUser[%d],PlayTime[%d],Fee%v,Prizes%v,Online[%d],TotalPrize[%d]",
		mc.MatchId, mc.Name, mc.GameName, mc.TotalUser, mc.EnrollMin, mc.EnrollMax, mc.TableUser, mc.PlayTime, mc.EnrollFee, mc.Prizes, mc.OnlineUser, mc.TotalPrizeAmount)

	for i := 0; i < len(mc.Rounds); i++ {
		log.Release("      Round[%d] Type[%d] WinnerCount[%d] [t:%d,e:%d,ssec:%d,sscore:%d]",
			i, mc.Rounds[i].MatchType, mc.Rounds[i].WinnerCount,
			mc.Rounds[i].Target, mc.Rounds[i].EliminateScore, mc.Rounds[i].ShrinkSec, mc.Rounds[i].ShrinkScore)
	}
	if mc.RobotConfig != nil {
		log.Release("      RobotConfig:%v", *mc.RobotConfig)
	}
}

func (mc *matchconfig) getPrizes(rank int) []item.ItemPack {
	var ret []item.ItemPack
	for _, v := range mc.Prizes {
		if v.Rank == rank {
			return v.Prize
		}
	}
	return ret
}

func (mc *matchconfig) calTotalPrize() {
	if len(mc.EnrollFee) == 0 {
		mc.DailyFreeCount = 0
	}
	mc.TotalPrizeAmount = 0
	for _, v := range mc.Prizes {
		mc.TotalPrizeAmount += v.GetPrizeValue()
	}

	// 如果不是预报名
	if !mc.IsPreEnroll() {
		mc.EnrollMin = mc.TotalUser
		mc.EnrollMax = mc.TotalUser
	}
}

func (mc *matchconfig) addOnline(add int) {
	mc.OnlineUser += add
	if mc.OnlineUser < 0 {
		mc.OnlineUser = 0
	}
}

func (mc *matchconfig) setLeftFreeCount(userId int) {
	mc.RobotConfig = nil
	mc.StartSec = mc.GetNextStartTime() - int(time.Now().Unix())
	mc.EnrollOpen = mc.IsInTime()
	if mc.DailyFreeCount == 0 {
		return
	}
	mc.LeftFreeCount = mc.DailyFreeCount - getFreeCountManager().getUserFreeCount(userId, mc.MatchId)
}

func (mc *matchconfig) getMatchType(round int) int {
	if round >= len(mc.Rounds) {
		log.Release("combomatchconfig.isSimpleMatch round >= len %d,%d", round, len(mc.Rounds))
		return matchbase.MatchType_Invalid
	}
	return mc.Rounds[round].MatchType
}

func (mc *matchconfig) getTarget(round int) int {
	if round >= len(mc.Rounds) {
		log.Release("combomatchconfig.getTarget round >= len %d,%d", round, len(mc.Rounds))
		return 0
	}
	return mc.Rounds[round].Target
}

func (mc *matchconfig) getWinnerCount(round int) int {
	if round >= len(mc.Rounds) {
		log.Release("combomatchconfig.getWinnerCount round >= len %d,%d", round, len(mc.Rounds))
		return 0
	}
	return mc.Rounds[round].WinnerCount
}

func (mc *matchconfig) getPointMatchParams(round int) (int, int, int, int) {
	if round >= len(mc.Rounds) {
		log.Release("combomatchconfig.getTarget round >= len %d,%d ", round, len(mc.Rounds))
		return 0, 0, 0, 0
	}
	return mc.Rounds[round].EliminateScore, mc.Rounds[round].ShrinkSec, mc.Rounds[round].ShrinkScore, mc.Rounds[round].WinnerCount
}

func (mc *matchconfig) getTotalRound() int {
	return len(mc.Rounds)
}
