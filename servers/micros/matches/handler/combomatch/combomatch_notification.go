package combomatch

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	ComboMatch_noti_rank        = "combomatch_rank"
	ComboMatch_noti_userenter   = "combomatch_userenter"
	ComboMatch_noti_userexit    = "combomatch_userexit"
	ComboMatch_noti_matchfailed = "combomatch_matchfailed"
	ComboMatch_noti_matchlocked = "combomatch_matchlocked" // 报名截至
	ComboMatch_noti_matchcall   = "combomatch_matchcall"   // 比赛开赛催促提示
	ComboMatch_noti_promoted    = "combomatch_promoted"
)

type ComboMatch_notificationInfo struct {
	Msg      string `json:",omitempty"`
	MatchId  int
	UserId   int             `json:",omitempty"`
	Rank     int             `json:",omitempty"`
	Prize    []item.ItemPack `json:",omitempty"`
	FaceId   int             `json:",omitempty"`
	FaceUrl  string          `json:",omitempty"`
	Nickname string          `json:",omitempty"`
	Seconds  int             `json:",omitempty"`
}
