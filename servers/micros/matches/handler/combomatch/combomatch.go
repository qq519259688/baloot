package combomatch

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/matches/proto"
)

func Run() {
	log.Release("combomatch.Run")
	getMatchManager().run()
	getHistoryManager().run()
}

func Dump(param1, param2 string) {
	if param1 == "free" {
		getFreeCountManager().dump(param2)
		return
	}
	getMatchManager().dump(param1, param2)
}

// 参加比赛，成功后返回MatchNo
func EnrollComboMatch(matchId int, userId int, nickname string, faceId int, faceUrl string, feeIndex int) (int, string) {
	return getMatchManager().enrollComboMatch(matchId, userId, nickname, faceId, faceUrl, feeIndex, false)
}

func QuitComboMatch(matchId int, userId int) bool {
	return getMatchManager().quitComboMatch(matchId, userId)
}

func GetMatchList(userId int) string {
	return getMatchManager().getMatchList(userId)
}

func Flush() {
	getHistoryManager().forceFlush()
}

func GetComboMatchHistory(userId int) string {
	return getHistoryManager().getHistory(userId)
}

func GetUserComboMatchId(userId int) []pb.UserComboMatchId {
	return getMatchManager().getUserMatchId(userId)
}

func GetComboMatchInfo(matchId int, userId int) string {
	return getMatchManager().getUserMatchInfo(matchId, userId)
}

func GetComboMatchConfirmCount(matchId int) int {
	return getMatchManager().getConfirmCount(matchId)
}

func ComboMatchConfirm(matchId, userId int) bool {
	return getMatchManager().confirmMatch(matchId, userId)
}
