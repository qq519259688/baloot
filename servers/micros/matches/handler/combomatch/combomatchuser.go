package combomatch

import (
	"bet24.com/servers/micros/matches/handler/matchbase"
)

type combomatchuser struct {
	matchbase.MatchUser
	confirmed bool // 是否确认开赛，用于预报名比赛最终确认
	isRobot   bool
}
