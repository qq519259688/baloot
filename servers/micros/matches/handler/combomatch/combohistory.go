package combomatch

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	"bet24.com/servers/micros/matches/handler/matchbase"
)

type historyRound struct {
	MatchType   int
	Target      int
	WinnerCount int
	MatchNo     int
	StartTime   int64
	EndTime     int64
}

type historyUser struct {
	matchbase.MatchUserBrief
	enrollTime int
	prize      []item.ItemPack
}

type combohistory struct {
	MatchId   int
	Name      string
	GameId    int
	GameName  string
	GameRule  string
	TotalUser int
	TableUser int
	EnrollFee []item.ItemPack
	Prizes    []matchbase.Prize_config
	// 开始结束时间
	StartTime int64
	EndTime   int64
	// 参赛玩家
	EnrollUsers []historyUser
	Winners     []int
	Rounds      []historyRound

	noShowUsers []matchbase.EnrollUser
}

func (h *combohistory) dump(detail bool) {
	log.Release("    MatchId[%d] GameRule[%s] TotalUser[%d],TableUser[%d],Fee%v,Prizes[%d]",
		h.MatchId, h.GameRule, h.TotalUser, h.TableUser, h.EnrollFee, h.Prizes)
	log.Release("    Start[%s] End[%s] Winners %v", common.TimeStampToString(h.StartTime), common.TimeStampToString(h.EndTime), h.Winners)
	if !detail {
		return
	}
	for i := 0; i < len(h.EnrollUsers); i++ {
		v := h.EnrollUsers[i]
		log.Release("      User[%d]: %d.%s", v.Rank, v.UserId, v.NickName)
	}

	for i := 0; i < len(h.Rounds); i++ {
		v := h.Rounds[i]
		log.Release("      Round[%d]: %d.%d", i, v.MatchType, v.MatchNo)
	}
}

func (h *combohistory) isUserEnrolled(userId int) bool {
	for _, v := range h.EnrollUsers {
		if v.UserId == userId {
			return true
		}
	}
	return false
}

func (h *combohistory) setAllEnrolledUsers(users []matchbase.EnrollUser) {
	if len(users) == 0 {
		return
	}

	for i := 0; i < len(users); {
		if h.isUserEnrolled(users[i].UserId) {
			users = append(users[:i], users[i+1:]...)
		} else {
			i++
		}
	}

	if len(users) == 0 {
		return
	}

	h.noShowUsers = make([]matchbase.EnrollUser, len(users))
	copy(h.noShowUsers, users)
}
