package combomatch

import (
	"bet24.com/servers/micros/matches/handler/matchbase"
)

func writeRoomRecordToDB(hi *combohistory) {
	var record matchbase.MatchRecord
	record.MatchId = hi.MatchId
	record.MatchType = matchbase.MatchType_Combo
	record.MatchName = hi.Name
	record.GameId = hi.GameId
	record.GameName = hi.GameName
	record.MaxUserCount = hi.TotalUser
	record.TableUserCount = hi.TableUser
	record.EnrollFee = hi.EnrollFee
	record.StartTime = int(hi.StartTime)
	record.EndTime = int(hi.EndTime)
	record.Extra = hi.GameRule
	for _, v := range hi.EnrollUsers {
		record.Users = append(record.Users, matchbase.MatchRecord_User{
			UserId:     v.UserId,
			Fee:        hi.EnrollFee,
			EnrollTime: v.enrollTime, // 报名时间
			Rank:       v.Rank,       // -1表示未参赛只报名
			Score:      v.Score,
			Prize:      v.prize,
		})
	}

	// 报名但未参赛的待补充
	for _, v := range hi.noShowUsers {
		record.Users = append(record.Users, matchbase.MatchRecord_User{
			UserId: v.UserId,

			EnrollTime: int(v.EnrollTime), // 报名时间
			Rank:       -1,                // -1表示未参赛只报名
		})
	}
	matchbase.WriteRecordToDb(record)
}
