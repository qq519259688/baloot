package singlematch

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
	ladder "bet24.com/servers/micros/ladderservice/proto"
	"bet24.com/servers/micros/matches/handler/matchbase"
	pb "bet24.com/servers/micros/matches/proto"
	"math/rand"
)

type RobotMultiple struct {
	Odds        int
	MultipleMin int
	MultipleMax int
}

func (rm *RobotMultiple) getMultiple() int {
	if rm.MultipleMin <= rm.MultipleMax {
		return rm.MultipleMin
	}

	return rm.MultipleMin + rand.Intn(rm.MultipleMax-rm.MultipleMin+1)
}

type RoundInfo struct {
	pb.SingleMatchRoundInfo
	EliminatUser   int           // 淘汰人数
	BaseScore      int           // 底分
	SetCount       int           // 局数，默认1
	ReviveCost     item.ItemPack // 复活消耗，空表示不可复活
	EliminatByLose bool          // 是否按胜负淘汰
}

type SingleMatchConfig struct {
	MatchId                 int
	Name                    string
	Desc                    string
	GameId                  int
	GameName                string
	GameRule                string // 从privateroom取
	InitScore               int    // 初始分数
	TableUser               int
	PlayTime                int
	EnrollFee               item.ItemPack
	ScoreShrinkPercent      int // 分数衰减至下一轮
	RankPrizes              []matchbase.Range_Rank_Prize
	RobotMultiples          []RobotMultiple
	Rounds                  []RoundInfo
	LadderAdditionalPercent int // 天梯额外加成
	ReviveTimeoutSec        int // 复活倒计时
}

func (sc *SingleMatchConfig) dump() {
	log.Release("  GameRule[%s]Init[%d]TableUser[%d]Fee[%d:%d]Shrink[%d]LAP[%d]",
		sc.GameRule, sc.InitScore, sc.TableUser, sc.EnrollFee.ItemId, sc.EnrollFee.Count, sc.ScoreShrinkPercent, sc.LadderAdditionalPercent)
	log.Release("  Prizes:")
	for _, v := range sc.RankPrizes {
		log.Release("    Rank[%d-%d]:%v", v.RankMin, v.RankMax, v.Prize)
	}
	log.Release("  Rounds:")
	for _, v := range sc.Rounds {
		t := ""
		if v.EliminatByLose {
			t = "[LOSE]"
		}
		log.Release("    User[%d-%d]BaseScore[%d]SetCount[%d]ReviveCost[%d:%d]Prize%v%s",
			v.TotalUser, v.TotalUser-v.EliminatUser, v.BaseScore, v.SetCount, v.ReviveCost.ItemId, v.ReviveCost.Count, v.Prize, t)
	}
}

func (sc *SingleMatchConfig) getRound(index int) *RoundInfo {
	if len(sc.Rounds) == 0 {
		log.Release("SingleMatchConfig.getRound no rounds has been configed")
		return nil
	}

	if index <= sc.Rounds[0].Index {
		return &sc.Rounds[0]
	}

	for k, v := range sc.Rounds {
		if index == v.Index {
			return &sc.Rounds[k]
		}
	}

	log.Release("SingleMatchConfig.getRound Round[%d] not found in rounds maxIndex = %d",
		index, sc.Rounds[len(sc.Rounds)-1].Index)

	return nil
}

func (sc *SingleMatchConfig) getRandomScore(roundIndex int) int {
	round := sc.getRound(roundIndex)
	if round == nil {
		log.Release("SingleMatchConfig.getRandomScore roundIndex[%d] not found", roundIndex)
		return 0
	}
	oddsTotal := 0
	for _, v := range sc.RobotMultiples {
		oddsTotal += v.Odds
	}

	if oddsTotal == 0 {
		log.Release("SingleMatchConfig.getRandomScore oddsTotal == 0")
		return 0
	}
	r := rand.Intn(oddsTotal)

	// 取倍数
	tmpOdds := 0
	for _, v := range sc.RobotMultiples {
		tmpOdds += v.Odds
		if r < tmpOdds {
			return v.getMultiple() * round.BaseScore
		}
	}

	log.Release("SingleMatchConfig.getRandomScore failure oddsTotal[%d],r[%d]", oddsTotal, r)
	return 0
}

func (sc *SingleMatchConfig) isFinalRound(roundIndex int) bool {
	roundCount := len(sc.Rounds)
	if roundCount == 0 {
		return false
	}

	return roundIndex >= sc.Rounds[roundCount-1].Index
}

func (sc *SingleMatchConfig) getFinalPrize(rank int) []item.ItemPack {
	var prizes []item.ItemPack
	for _, v := range sc.RankPrizes {
		if rank >= v.RankMin && rank <= v.RankMax {
			prizes = append(prizes, v.Prize...)
			break
		}
	}
	return prizes
}

func (sc *SingleMatchConfig) createRoundIndex() {
	sc.LadderAdditionalPercent = ladder.GetLadderRoomAdditionalPercent(sc.GameId, sc.GameRule)
	for i := 0; i < len(sc.Rounds); i++ {
		sc.Rounds[i].Index = i + 1
	}
	if sc.ReviveTimeoutSec == 0 {
		sc.ReviveTimeoutSec = 60
	}
}
