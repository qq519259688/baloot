package singlematch

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	micro_common "bet24.com/servers/micros/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	privateroom "bet24.com/servers/micros/privateroom/proto"
	"fmt"
	"strconv"
	"sync"
	"time"
)

var matchMgr *singlematchmanager

func getMatchManager() *singlematchmanager {
	if matchMgr == nil {
		matchMgr = new(singlematchmanager)
		matchMgr.ctor()
	}
	return matchMgr
}

type singlematchmanager struct {
	matchlist map[int]*singlematchinstance
	lock      *sync.RWMutex
}

func (sm *singlematchmanager) ctor() {
	sm.matchlist = make(map[int]*singlematchinstance)
	sm.lock = &sync.RWMutex{}
	go privateroom.RegisterServerStatus(sm, sm, micro_common.GetServicePort(), "singlematchmanager")
	sm.checkTimeout()
}

func (sm *singlematchmanager) checkTimeout() {
	time.AfterFunc(30*time.Second, sm.checkTimeout)
	var toRemove []int
	sm.lock.Lock()
	for k, v := range sm.matchlist {
		if v.isTimeout() {
			toRemove = append(toRemove, k)
		}
	}
	sm.lock.Unlock()

	if len(toRemove) == 0 {
		return
	}

	log.Debug("singlematchmanager.checkTimeout removing %v", toRemove)
	sm.lock.Lock()
	for _, v := range toRemove {
		delete(sm.matchlist, v)
	}
	sm.lock.Unlock()
}

func (sm *singlematchmanager) dump(param1, param2 string) {
	log.Release("-------------------------------")
	log.Release("singlematch.singlematchmanager.dump[%s,%s]", param1, param2)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	if param1 == "config" {
		getConfigManager().dumpSys(param2)
		return
	}

	if param1 == "user" {
		userId, err := strconv.Atoi(param2)
		if err != nil {
			log.Release("    invalid param %s", param2)
			return
		}
		m := sm.getUserMatch(userId)
		if m == nil {
			log.Release("    user[%d] has no match playing", userId)
			return
		}

		m.dump()
		return
	}

	sm.lock.RLock()
	for k, v := range sm.matchlist {
		log.Release("    UserId[%d]", k)
		v.dump()
		log.Release("    -------------")
	}
	sm.lock.RUnlock()
}

func (sm *singlematchmanager) getUserMatch(userId int) *singlematchinstance {
	sm.lock.RLock()
	si, _ := sm.matchlist[userId]
	sm.lock.RUnlock()
	if si != nil {
		si.ReviveSecs = si.calReviveTimeout()
	}
	return si
}

func (sm *singlematchmanager) createMatch(userId int, matchId int) (string, *singlematchinstance) {
	existMatch := sm.getUserMatch(userId)
	if existMatch != nil && !existMatch.isEnded() {
		log.Release("singlematchmanager.createMatch failed,already exist user match [%d]", userId)
		return "playing", nil
	}

	matchConfig := getConfigManager().getMatch(matchId)
	if matchConfig == nil {
		log.Release("newMatchInstance matchConfig not found[%d]", matchId)
		return "no config", nil
	}
	if matchConfig.EnrollFee.Count > 0 {
		ok, err := item.Consume(userId, matchConfig.EnrollFee.ItemId,
			common.LOGTYPE_SINGLEMATCH_TICKET, matchConfig.EnrollFee.Count, 0)
		if !ok {
			errMsg := fmt.Sprintf("not enough fee[%v] for play err = %s", matchConfig.EnrollFee, err)
			log.Release("singlematchmanager.createMatch failed %s", errMsg)
			return "not enough fee", nil
		}
	}
	mi := newMatchInstance(userId, matchConfig)
	sm.lock.Lock()
	sm.matchlist[userId] = mi
	sm.lock.Unlock()
	mi.startRound()
	return "ok", mi
}

func (sm *singlematchmanager) revive(userId int) bool {
	um := sm.getUserMatch(userId)
	if um == nil {
		log.Release("singlematchmanager.revive userId[%d] match not found", userId)
		return false
	}

	return um.revive()
}

func (sm *singlematchmanager) noRevive(userId int) bool {
	um := sm.getUserMatch(userId)
	if um == nil {
		log.Release("singlematchmanager.noRevive userId[%d] match not found", userId)
		return false
	}

	return um.noRevive()
}
