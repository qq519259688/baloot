package singlematch

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/matches/proto"
	"encoding/json"
)

func Run() {
	log.Release("singlematch.Run")
	getConfigManager()
	getMatchManager()
}

func Dump(param1, param2 string) {
	getMatchManager().dump(param1, param2)
}

func CreateUserSingleMatch(userId int, matchId int) (string, *pb.SingleMatchInfo) {
	errMsg, smi := getMatchManager().createMatch(userId, matchId)
	if smi == nil {
		return errMsg, nil
	}

	return errMsg, &smi.SingleMatchInfo
}

func GetUserSingleMatch(userId int) *pb.SingleMatchInfo {
	smi := getMatchManager().getUserMatch(userId)
	if smi == nil {
		return nil
	}
	return &smi.SingleMatchInfo
}

func GetSingleMatchList() string {
	d, _ := json.Marshal(getConfigManager().Matches)
	return string(d)
}

func SingleMatchRevive(userId int) bool {
	return getMatchManager().revive(userId)
}

func SingleMatchNoRevive(userId int) bool {
	return getMatchManager().noRevive(userId)
}
