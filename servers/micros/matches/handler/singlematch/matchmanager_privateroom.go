package singlematch

import (
	"bet24.com/log"
)

func (sm *singlematchmanager) OnGameRuleRegistered(gameId int, gameRule string, desc string, targetOptions []int, userOptions []int, playTimeOptions []int) {
	//log.Debug("singlematchmanager.OnGameRuleRegistered gameId[%d] gameRule[%s]", gameId, gameRule)
	//config.Mgr.AddGameRule(gameId, gameRule)
}

func (sm *singlematchmanager) OnGameRuleDeregistered(gameId int, gameRule string) {
	//log.Debug("singlematchmanager.OnGameRuleDeregistered gameId[%d] gameRule[%s]", gameId, gameRule)
	//sm.removeRoomsByGameRule(gameId, gameRule)
	//config.Mgr.RemoveGameRule(gameId, gameRule)
}

func (sm *singlematchmanager) OnRoomStart(roomNo int) {
	log.Debug("singlematchmanager.OnRoomStart roomNo[%d] ", roomNo)
}
func (sm *singlematchmanager) OnRoomEnd(roomNo int, winners []int) {
	log.Debug("singlematchmanager.OnRoomEnd roomNo[%d] winners%v ", roomNo, winners)
	sm.lock.RLock()
	for _, v := range sm.matchlist {
		if v.onRoomEnd(roomNo, winners) {
			break
		}
	}
	sm.lock.RUnlock()
}
func (sm *singlematchmanager) OnRoomUserScoreChanged(roomNo int, userId int, score int) {
	log.Debug("singlematchmanager.OnRoomEnd roomNo[%d] UserId[%d] Score[%d] ", roomNo, userId, score)

	sm.lock.RLock()
	for _, v := range sm.matchlist {
		if v.onRoomUserScoreChanged(roomNo, userId, score) {
			break
		}
	}
	sm.lock.RUnlock()
}
func (sm *singlematchmanager) OnRoomStatusChanged(roomNo int, old, new int) {
	log.Debug("singlematchmanager.OnRoomStatusChanged roomNo[%d] [%d->%d] ", roomNo, old, new)
}
