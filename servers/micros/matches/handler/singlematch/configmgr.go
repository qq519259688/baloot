package singlematch

import (
	"encoding/json"
	"os"
	"time"

	"bet24.com/log"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "matches_singlematch"
const refresh_config_sec = 1800

var cfgmgr *configmgr

func getConfigManager() *configmgr {
	if cfgmgr == nil {
		cfgmgr = new(configmgr)
		cfgmgr.ctor()
	}
	return cfgmgr
}

type configmgr struct {
	Matches      []SingleMatchConfig
	configString string
}

func (cm *configmgr) ctor() {
	cm.loadConfig()
}

func (cm *configmgr) loadConfig() {
	time.AfterFunc(time.Second*600, cm.loadConfig)
	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("serviceconf/matches_singlematch.json")
		if err != nil {
			log.Release("configmgr.loadData read config failed")
			return
		}
		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(config_key, configString)
		}
	}

	if configString == cm.configString {
		return
	}

	cm.configString = configString
	// 读取配置
	err := json.Unmarshal([]byte(configString), &cm.Matches)
	if err != nil {
		log.Release("configmgr.loadData Unmarshal failed %s", configString)
	}

	// 设置Index
	for i := 0; i < len(cm.Matches); i++ {
		cm.Matches[i].createRoundIndex()
	}
}

func (cm *configmgr) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("configmgr.dumpSys")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	for _, v := range cm.Matches {
		log.Release("MatchId:%d", v.MatchId)
		v.dump()
	}
}

func (cm *configmgr) getMatch(matchId int) *SingleMatchConfig {
	for _, v := range cm.Matches {
		if v.MatchId == matchId {
			return &v
		}
	}
	return nil
}
