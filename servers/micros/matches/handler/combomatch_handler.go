package handler

import (
	"bet24.com/servers/micros/matches/handler/combomatch"
	pb "bet24.com/servers/micros/matches/proto"
	"context"
	"encoding/json"
)

func (h *MatchesService) EnrollComboMatch(ctx context.Context, req *pb.EnrollSngMatch_req, rsp *pb.Response) error {
	rsp.RetCode, rsp.Data = combomatch.EnrollComboMatch(req.MatchId, req.UserId, req.NickName, req.FaceId,
		req.FaceUrl, req.FeeIndex)
	return nil
}

func (h *MatchesService) QuitComboMatch(ctx context.Context, req *pb.ComboMatch_req, rsp *pb.Response) error {
	rsp.BoolValue = combomatch.QuitComboMatch(req.MatchId, req.UserId)
	return nil
}

func (h *MatchesService) GetComboMatchList(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = combomatch.GetMatchList(req.UserId)
	return nil
}

func (h *MatchesService) GetComboMatchHistory(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = combomatch.GetComboMatchHistory(req.UserId)
	return nil
}

func (h *MatchesService) GetUserComboMatchId(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	ret := combomatch.GetUserComboMatchId(req.UserId)
	d, _ := json.Marshal(ret)
	rsp.Data = string(d)
	return nil
}

func (h *MatchesService) GetComboMatchInfo(ctx context.Context, req *pb.ComboMatch_req, rsp *pb.Response) error {
	rsp.Data = combomatch.GetComboMatchInfo(req.MatchId, req.UserId)
	return nil
}

func (h *MatchesService) GetComboMatchConfirmCount(ctx context.Context, req *pb.ComboMatch_req, rsp *pb.Response) error {
	rsp.RetCode = combomatch.GetComboMatchConfirmCount(req.MatchId)
	return nil
}

func (h *MatchesService) ComboMatchConfirm(ctx context.Context, req *pb.ComboMatch_req, rsp *pb.Response) error {
	rsp.BoolValue = combomatch.ComboMatchConfirm(req.MatchId, req.UserId)
	return nil
}
