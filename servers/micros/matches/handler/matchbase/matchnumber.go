package matchbase

import (
	"math/rand"
	"sync"
)

const (
	min_match_no = 100000
	max_match_no = 999999
)

var mn *matchnumber

func getMatchNumber() *matchnumber {
	if mn == nil {
		mn = new(matchnumber)
		mn.run()
	}
	return mn
}

type matchnumber struct {
	lock         *sync.RWMutex
	matchNumbers []int32
	matchIndex   int
}

func (mn *matchnumber) run() {
	mn.lock = &sync.RWMutex{}
	count := max_match_no - min_match_no + 1
	mn.matchNumbers = make([]int32, count)
	for i := int32(min_match_no); i < int32(max_match_no); i++ {
		mn.matchNumbers[i-min_match_no] = i
	}

	for i := count - 1; i > 1; i-- {
		rep := rand.Intn(i)
		mn.matchNumbers[i], mn.matchNumbers[rep] = mn.matchNumbers[rep], mn.matchNumbers[i]
	}
	mn.matchIndex = 0
}

func (mn *matchnumber) getMatchNo() int {
	mn.lock.Lock()
	defer mn.lock.Unlock()
	ret := int(mn.matchNumbers[mn.matchIndex])
	mn.matchIndex = (mn.matchIndex + 1) % len(mn.matchNumbers)
	return ret
}
