package matchbase

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	MatchStatus_Invalid = iota
	MatchStatus_Free
	MatchStatus_Playing
	MatchStatus_Ended
)

var status_desc = []string{"n/a", "free", "playing", "ended"}

func GetMatchStatusDesc(status int) string {
	if status > MatchStatus_Ended || status < MatchStatus_Invalid {
		status = MatchStatus_Invalid
	}
	return status_desc[status]
}

const (
	MatchType_Invalid     = -1      // 无效值
	MatchType_SimpleMatch = 0       // 淘汰晋级赛
	MatchType_PointMatch  = 1       // 定时淘汰赛
	MatchType_SetsMatch   = 2       // 局数配置赛
	MatchType_SNG         = "SNG"   // sng赛事集合
	MatchType_Combo       = "Combo" // 配置赛集合
)

const (
	Match_noti_userenter     = "userenter"
	Match_noti_userexit      = "userexit"
	Match_noti_statuschanged = "statuschanged"
	Match_noti_scorechanged  = "scorechanged"
	Match_noti_promoted      = "promoted"
	Match_noti_eliminated    = "eliminated"
	Match_noti_rank          = "rank"
	Match_noti_rank_changed  = "rank_changed"
	Match_noti_matchroom     = "matchroom"
	Match_noti_shrink        = "shrink"
)

type Match_notificationInfo struct {
	Msg              string `json:",omitempty"`
	UserId           int    `json:",omitempty"`
	OldStatus        int    `json:",omitempty"`
	NewStatus        int    `json:",omitempty"`
	Score            int    `json:",omitempty"`
	Rank             int    `json:",omitempty"`
	Prize            int    `json:",omitempty"`
	ServerAddr       string `json:",omitempty"`
	TableId          int    `json:",omitempty"`
	ChairId          int
	FaceId           int             `json:",omitempty"`
	FaceUrl          string          `json:",omitempty"`
	NickName         string          `json:",omitempty"`
	ShrinkScore      int             `json:",omitempty"` // 这里命名错误，实际是EliminatScore,淘汰分数
	PrizeItems       []item.ItemPack `json:",omitempty"`
	ReviveCost       item.ItemPack   `json:",omitempty"`
	MatchData        string          `json:",omitempty"`
	ReviveTimeoutSec int
}

func GetMatchNo() int {
	return getMatchNumber().getMatchNo()
}

func Run() {
	getMatchNumber()
}

func IsElementIn(ele int, list []int) bool {
	for _, v := range list {
		if ele == v {
			return true
		}
	}
	return false
}
