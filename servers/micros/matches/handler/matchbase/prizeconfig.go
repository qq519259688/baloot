package matchbase

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

type Prize_config struct {
	Rank  int
	Prize []item.ItemPack
	Desc  string
}

func (pc *Prize_config) GetPrizeValue() int {
	ret := 0
	for _, v := range pc.Prize {
		ret += item.GetItemValue(v.ItemId, v.Count)
	}
	return ret
}

type Range_Rank_Prize struct {
	RankMin int
	RankMax int
	Prize   []item.ItemPack
}
