package matchbase

type MatchInstance interface {
	GetStatus() int
	IsFull() bool
	IsUserEnrolled(userId int) bool
	RegisterReceiver(receiver MatchInstanceReceiver)
	GetWinners() []int
	GetAllMatchUsers() []MatchUser
	SendNotification(userId int, data string)
	GetUserList() []int
	GetUser(userId int) *MatchUser
}

type MatchInstanceReceiver interface {
	OnMatchStart(matchNo int)
	OnMatchEnd(matchNo int)
	OnMatchCancelled(matchNo int)
	OnUserEliminated(matchNo int, userId int, rank int)
}
