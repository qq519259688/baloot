package matchbase

type UserOption struct {
	TableUser int
	TotalUser []int
}

type MatchRule struct {
	Name            string
	Desc            string
	TargetOptions   []int
	UserOptions     []int
	PlayTimeOptions []int
}
