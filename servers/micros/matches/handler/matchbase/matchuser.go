package matchbase

type EnrollUser struct {
	UserId     int
	EnrollTime int64
}

type MatchUser struct {
	UserId     int
	NickName   string
	FaceId     int
	FaceUrl    string
	Rank       int
	Score      int
	GameCount  int
	BaseScore  int
	WinCount   int
	EnrollTime int64
}

type MatchUserBrief struct {
	UserId   int
	NickName string
	Rank     int
	Score    int
}

func (mu *MatchUser) IsBetterThan(usr *MatchUser) bool {
	if mu.Score > usr.Score {
		return true
	} else if mu.Score < usr.Score {
		return false
	}
	if mu.WinCount > usr.WinCount {
		return true
	} else if mu.WinCount < usr.WinCount {
		return false
	}
	return mu.EnrollTime < usr.EnrollTime
}

func (mu *MatchUser) ToBrief() *MatchUserBrief {
	return &MatchUserBrief{
		UserId:   mu.UserId,
		NickName: mu.NickName,
		Rank:     mu.Rank,
		Score:    mu.Score,
	}
}
