package matchbase

import (
	"math/rand"
)

type Robot_config struct {
	Max     int
	WaitMin int
	WaitMax int
}

func (rc *Robot_config) GetWaitSec() int {
	if rc.WaitMax <= rc.WaitMin {
		return rc.WaitMin
	}
	return rc.WaitMin + rand.Intn(rc.WaitMax-rc.WaitMin)
}
