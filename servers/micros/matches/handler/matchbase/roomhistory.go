package matchbase

import (
	"bet24.com/log"
	"bet24.com/servers/common"
)

type RoomHistoryUser struct {
	UserId    int
	Score     int
	IsPromote bool
}

type RoomHistory struct {
	RoomNo  int
	Users   []RoomHistoryUser
	EndTime int64
}

func (rh *RoomHistory) Dump() {
	log.Release("        RoomNo[%d] End[%s]", rh.RoomNo, common.TimeStampToString(rh.EndTime))
	for _, v := range rh.Users {
		promote := ""
		if v.IsPromote {
			promote = "V"
		}
		log.Release("          UserId[%d] Score[%d] %s", v.UserId, v.Score, promote)
	}
}
