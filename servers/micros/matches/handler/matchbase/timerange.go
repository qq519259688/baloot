package matchbase

import (
	_ "bet24.com/log"
	"time"
)

type time_range struct {
	Start int `json:",omitempty"`
	End   int `json:",omitempty"`
}

func (tr *time_range) isValid() bool {
	return tr.Start != 0 && tr.End != 0
}

type Time_config struct {
	RangeAll        *time_range `json:",omitempty"`
	RangeWeekDay    *time_range `json:",omitempty"`
	RangeTime       *time_range `json:",omitempty"`
	EnrollRange     *time_range `json:",omitempty"` // 报名限制，Start和End表示提前多少秒
	ExtraNotifySecs []int       `json:",omitempty"` // 添加额外通知，分别表示提前多少秒提示
}

func (tc *Time_config) IsInTime() bool {
	if tc.EnrollRange != nil {
		nextTime := tc.GetNextStartTime()
		if nextTime == 0 {
			return false
		}
		nowUnix := int(time.Now().Unix())
		return nowUnix >= nextTime-tc.EnrollRange.Start && nowUnix <= nextTime-tc.EnrollRange.End
	}

	if tc.RangeAll != nil && tc.RangeAll.isValid() {
		now := time.Now()
		nowDay := now.Year()*10000 + int(now.Month())*100 + now.Day()
		if nowDay < tc.RangeAll.Start || nowDay > tc.RangeAll.End {
			return false
		}
	}

	if tc.RangeWeekDay != nil && tc.RangeWeekDay.isValid() {
		now := time.Now()
		nowWeekDay := int(now.Weekday())
		if tc.RangeWeekDay.Start < tc.RangeWeekDay.End {
			if nowWeekDay < tc.RangeWeekDay.Start || nowWeekDay > tc.RangeWeekDay.End {
				return false
			}
		} else {
			if nowWeekDay < tc.RangeWeekDay.Start && nowWeekDay > tc.RangeWeekDay.End {
				return false
			}
		}
	}

	if tc.RangeTime != nil && tc.RangeTime.isValid() {
		now := time.Now()
		nowTime := now.Hour()*100 + now.Minute()
		if tc.RangeTime.Start < tc.RangeTime.End {
			if nowTime < tc.RangeTime.Start || nowTime > tc.RangeTime.End {
				return false
			}
		} else {
			if nowTime < tc.RangeTime.Start && nowTime > tc.RangeTime.End {
				return false
			}
		}
	}

	return true
}

// 获取下一次比赛开始时间
func (tc *Time_config) GetNextStartTime() int {
	if tc.RangeTime == nil {
		return 0
	}
	dayDelta := 0
	now := time.Now()
	nowTime := now.Hour()*100 + now.Minute()
	if nowTime > tc.RangeTime.Start {
		// 明天
		dayDelta = 1
	}

	if tc.RangeAll != nil {
		nowDay := now.Year()*10000 + int(now.Month())*100 + now.Day() + dayDelta
		if nowDay < tc.RangeAll.Start || nowDay > tc.RangeAll.End {
			return 0
		}
	}

	if tc.RangeWeekDay != nil {
		nowWeekDay := (int(now.Weekday()) + dayDelta) % 7
		if tc.RangeWeekDay.Start < tc.RangeWeekDay.End {
			if nowWeekDay < tc.RangeWeekDay.Start || nowWeekDay > tc.RangeWeekDay.End {
				return 0
			}
		} else {
			if nowWeekDay < tc.RangeWeekDay.Start && nowWeekDay > tc.RangeWeekDay.End {
				return 0
			}
		}
	}

	// 天数检测过了
	nextTime := time.Date(now.Year(), now.Month(), now.Day(), tc.RangeTime.Start/100, tc.RangeTime.Start%100, 0, 0, now.Location())
	//log.Debug("Time_config.getNextStartTime nextTime = %v", nextTime)
	return int(nextTime.Unix()) + dayDelta*86400
}

// 是否提前报名
func (tc *Time_config) IsPreEnroll() bool {
	return tc.EnrollRange != nil
}
