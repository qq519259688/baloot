package matchbase

import (
	"bet24.com/database"
	"bet24.com/servers/common"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	"fmt"
	"strings"
)

type MatchRecord struct {
	MatchId        int
	MatchType      string // 101 SNG,102 配置赛
	MatchName      string
	GameId         int
	GameName       string
	MaxUserCount   int             // 最多报名人数
	TableUserCount int             // 每张桌子人数
	EnrollFee      []item.ItemPack // 报名费
	StartTime      int
	EndTime        int
	Extra          string // 规则
	Users          []MatchRecord_User
}

type MatchRecord_User struct {
	UserId     int
	Fee        []item.ItemPack // 报名费，为空表示免费
	EnrollTime int             // 报名时间
	Rank       int             // -1表示未参赛只报名
	Score      int
	Prize      []item.ItemPack
}

func WriteRecordToDb(mr MatchRecord) {
	var serialNumber string
	enrollFee := itemPackToString(mr.EnrollFee)
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_MatchData_AddStatistics")
	statement.AddParamter("@MatchID", database.AdParamInput, database.AdInteger, 4, mr.MatchId)
	statement.AddParamter("@MatchType", database.AdParamInput, database.AdVarChar, 64, mr.MatchType)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, mr.GameId)
	statement.AddParamter("@MaxUserCount", database.AdParamInput, database.AdInteger, 4, mr.MaxUserCount)
	statement.AddParamter("@TableUserCount", database.AdParamInput, database.AdInteger, 4, mr.TableUserCount)
	statement.AddParamter("@JoinUserCount", database.AdParamInput, database.AdInteger, 4, len(mr.Users))
	statement.AddParamter("@EnrollFee", database.AdParamInput, database.AdVarChar, 256, enrollFee)
	statement.AddParamter("@StartTime", database.AdParamInput, database.AdInteger, 4, mr.StartTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdInteger, 4, mr.EndTime)
	statement.AddParamter("@SerialNumber", database.AdParamOutput, database.AdVarChar, 32, serialNumber)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}
	serialNumber = retRows[rowLen-1][0].(string)
	writeUserRecordToDb(serialNumber, mr.GameId, mr.Users)
	return
}

// 将道具转成字符串
func itemPackToString(ef []item.ItemPack) string {
	var builder strings.Builder
	for _, v := range ef {
		builder.WriteString(fmt.Sprintf("%d,%d;", v.ItemId, v.Count))
	}
	return builder.String()
}

// 将用户记录写入数据库
func writeUserRecordToDb(serialNumber string, gameID int, users []MatchRecord_User) {
	if serialNumber == "" {
		return
	}
	for _, v := range users {
		var itemID, itemCount int
		if len(v.Fee) > 0 {
			itemID = v.Fee[0].ItemId
			itemCount = v.Fee[0].Count
		}
		prizeStr := itemPackToString(v.Prize)
		enrollTime := common.TimeStampToString(int64(v.EnrollTime))
		saveUserDataToDb(gameID, v.Rank, v.UserId, itemID, itemCount, v.Score, serialNumber, enrollTime, prizeStr)
	}
}

// 将用户数据保存到数据库
func saveUserDataToDb(gameID, rank, userID, itemID, itemCount, score int, serialNumber, enrollTime, prize string) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_MatchData_AddUser")
	statement.AddParamter("@SerialNumber", database.AdParamInput, database.AdVarChar, 32, serialNumber)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameID)
	statement.AddParamter("@Rank", database.AdParamInput, database.AdInteger, 4, rank)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userID)
	statement.AddParamter("@ItemID", database.AdParamInput, database.AdInteger, 4, itemID)
	statement.AddParamter("@ItemCount", database.AdParamInput, database.AdInteger, 4, itemCount)
	statement.AddParamter("@EnrollTime", database.AdParamInput, database.AdVarChar, 20, enrollTime)
	statement.AddParamter("@Score", database.AdParamInput, database.AdInteger, 4, score)
	statement.AddParamter("@Prize", database.AdParamInput, database.AdVarChar, 128, prize)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}
