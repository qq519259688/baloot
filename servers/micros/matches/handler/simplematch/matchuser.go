package simplematch

import (
	"bet24.com/servers/micros/matches/handler/matchbase"
)

type matchuser struct {
	matchbase.MatchUser
	RoomNo int
	// 私人场房间信息
	ServerAddr string
	TableId    int
	ChairId    int

	Round int // 止步第几轮

	prize int
	tax   int

	teamId int // 如果有对家的玩法，第一轮会分配用户teamId，方便后续继续分配的时候保证坐一起
}

func newMatchUser(userId int, nickName string, faceId int, faceUrl string, score int, baseScore int) *matchuser {
	return &matchuser{
		MatchUser: matchbase.MatchUser{
			UserId:    userId,
			NickName:  nickName,
			FaceUrl:   faceUrl,
			FaceId:    faceId,
			Score:     score,
			BaseScore: baseScore,
		},
	}
}

func (mu *matchuser) arrangeRoom(roomNo int, serverAddr string, tableId int, chairId int) {
	mu.RoomNo = roomNo
	mu.ServerAddr = serverAddr
	mu.TableId = tableId
	mu.ChairId = chairId
}

func (mu *matchuser) clearRoomInfo() {
	mu.ServerAddr = ""
	mu.TableId = -1
	mu.ChairId = -1
}
