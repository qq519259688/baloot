package simplematch

import (
	"bet24.com/servers/micros/matches/handler/matchbase"
	notification "bet24.com/servers/micros/notification/proto"
	"encoding/json"
)

func (mi *matchinstance) postUserEnterNotification(userId int, nickname string, faceId int, faceUrl string, score int) {
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_userenter, UserId: userId,
		FaceId: faceId, NickName: nickname, FaceUrl: faceUrl, Score: score}
	d, _ := json.Marshal(ni)
	mi.sendNotification(-1, string(d))
}

func (mi *matchinstance) postUserExitNotification(userId int) {
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_userexit, UserId: userId}
	d, _ := json.Marshal(ni)
	mi.sendNotification(-1, string(d))
	// 发给自己
	notification.AddNotification(userId, notification.Notification_Match, string(d))
}

func (mi *matchinstance) postMatchStatusChangedNotificaiton(oldStatus, newStatus int) {
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_statuschanged, OldStatus: oldStatus, NewStatus: newStatus}
	d, _ := json.Marshal(ni)
	mi.sendNotification(-1, string(d))
}

func (mi *matchinstance) postUserScoreChangedNotification(userId int, score int) {
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_scorechanged, UserId: userId, Score: score}
	d, _ := json.Marshal(ni)
	mi.sendNotification(-1, string(d))
}

func (mi *matchinstance) postUserPromotionNotification(userId int) {
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_promoted}
	d, _ := json.Marshal(ni)
	mi.sendNotification(userId, string(d))
}
func (mi *matchinstance) postUserEliminationNotification(userId int, rank int) {
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_eliminated, Rank: rank}
	d, _ := json.Marshal(ni)
	mi.sendNotification(userId, string(d))
}

func (mi *matchinstance) postUserRankNotification(userId int, rank int, prize int) {
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_rank, Rank: rank, Prize: prize}
	d, _ := json.Marshal(ni)
	mi.sendNotification(userId, string(d))
}

func (mi *matchinstance) postUserMatchRoomNotification(userId int, serverAddr string, tableId, chairId int) {
	// 发送给所有人
	ni := matchbase.Match_notificationInfo{Msg: matchbase.Match_noti_matchroom, ServerAddr: serverAddr, TableId: tableId, ChairId: chairId}
	d, _ := json.Marshal(ni)
	mi.sendNotification(userId, string(d))
}
