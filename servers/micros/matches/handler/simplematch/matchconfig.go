package simplematch

import (
	"bet24.com/log"
	"bet24.com/servers/micros/matches/handler/matchbase"
)

type matchconfig struct {
	GameId        int
	GameName      string
	UserOptions   []matchbase.UserOption
	GameRules     []matchbase.MatchRule // 从privateroom取
	Round_timeoff int64                 // 每轮中间的延迟秒数
	TeamGame      bool                  // 是否一直同队伍撮合
	BaseScore     int
	//EleminateByScore bool                  // 是否根据分数淘汰
}

func (mc *matchconfig) getRoundTimeOff() int64 {
	return mc.Round_timeoff
}

func (mc *matchconfig) addGameRule(gameRule string, ruleDesc string, targetOptions []int, userOptions []int, playTimeOptions []int) {
	//log.Release("matchconfig addGameRule gameRule = %s,ruleDesc = %s", gameRule, ruleDesc)
	for i := 0; i < len(mc.GameRules); i++ {
		if mc.GameRules[i].Name == gameRule {
			mc.GameRules[i].Desc = ruleDesc
			mc.GameRules[i].TargetOptions = targetOptions
			mc.GameRules[i].UserOptions = userOptions
			mc.GameRules[i].PlayTimeOptions = playTimeOptions
			return
		}
	}

	mc.GameRules = append(mc.GameRules, matchbase.MatchRule{Name: gameRule, Desc: ruleDesc, TargetOptions: targetOptions, UserOptions: userOptions, PlayTimeOptions: playTimeOptions})
}

func (mc *matchconfig) dump() {
	log.Release("    GameId:%d GameName:%s,UserOptions[%v] Round_timeoff[%d]", mc.GameId, mc.GameName, mc.UserOptions, mc.Round_timeoff)
	for _, v := range mc.GameRules {
		log.Release("      Rule[%s] TargetOptions[%v] UserOptions[%v],PlayTimeOptions[%v]", v.Name, v.TargetOptions, v.UserOptions, v.PlayTimeOptions)
	}
}

func (mc *matchconfig) isValidParams(gameRule string, totalUserCount, tableUserCount, target, playTime int) bool {
	var mr *matchbase.MatchRule
	for i := 0; i < len(mc.GameRules); i++ {
		if mc.GameRules[i].Name == gameRule {
			mr = &mc.GameRules[i]
			break
		}
	}
	if mr == nil {
		log.Release("simplematch.matchconfig.isValidParams gamerRule[%s] not found", gameRule)
		return false
	}

	if !mc.isValueIn(tableUserCount, mr.UserOptions) {
		log.Release("simplematch.matchconfig.isValidParams tableUserCount[%d] not of [%v]", tableUserCount, mr.UserOptions)
		return false
	}

	if target > 0 && !mc.isValueIn(target, mr.TargetOptions) {
		log.Release("simplematch.matchconfig.isValidParams target[%d] not of [%v]", target, mr.TargetOptions)
		return false
	}

	// 人数限制
	for i := 0; i < len(mr.UserOptions); i++ {
		if mc.UserOptions[i].TableUser == tableUserCount {
			if mc.isValueIn(totalUserCount, mc.UserOptions[i].TotalUser) {
				return true
			}
		}
	}
	log.Release("simplematch.matchconfig.isValidParams TotalUser.TableUser not in config [%d.%d]", totalUserCount, tableUserCount)
	log.Release("rule:%v", *mr)
	return false
}

func (mc *matchconfig) isValueIn(value int, values []int) bool {
	if len(values) == 0 {
		return true
	}
	for i := 0; i < len(values); i++ {
		if values[i] == value {
			return true
		}
	}
	return false
}
