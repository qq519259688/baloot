package handler

import (
	"bet24.com/servers/micros/matches/handler/simplematch"
	pb "bet24.com/servers/micros/matches/proto"
	"context"
)

func (h *MatchesService) CreateSimpleMatch(ctx context.Context, req *pb.CreateSimpleMatch_req, rsp *pb.Response) error {
	rsp.RetCode, rsp.Data = simplematch.CreateMatch(req.UserId, req.GameId, req.GameRule,
		req.TotalUser, req.Target, req.TableUser, req.EnrollFee, req.Prize, req.PlayTime, req.ElimiantedByScore, req.WinnerCount)
	return nil
}

func (h *MatchesService) CloseSimpleMatch(ctx context.Context, req *pb.SimpleMatch_req, rsp *pb.Response) error {
	rsp.RetCode, rsp.Data = simplematch.CloseMatch(req.UserId, req.MatchNo)
	return nil
}

func (h *MatchesService) EnrollSimpleMatch(ctx context.Context, req *pb.EnrollSimpleMatch_req, rsp *pb.Response) error {
	rsp.RetCode, rsp.Data = simplematch.EnrollMatch(req.UserId, req.NickName, req.FaceId,
		req.FaceUrl, req.MatchNo)
	return nil
}

func (h *MatchesService) QuitSimpleMatch(ctx context.Context, req *pb.SimpleMatch_req, rsp *pb.Response) error {
	rsp.BoolValue = simplematch.QuitMatch(req.UserId, req.MatchNo)
	return nil
}

func (h *MatchesService) GetSimpleMatchInfo(ctx context.Context, req *pb.SimpleMatch_req, rsp *pb.Response) error {
	rsp.Data = simplematch.GetMatchInfo(req.MatchNo)
	return nil
}

func (h *MatchesService) GetUserSimpleMatches(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = simplematch.GetUserMatches(req.UserId)
	return nil
}

func (h *MatchesService) GetSimpleMatchConfigs(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = simplematch.GetMatchConfigs()
	return nil
}

func (h *MatchesService) GetEnrolledSimpleMatch(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = simplematch.GetEnrolledMatch(req.UserId)
	return nil
}

func (h *MatchesService) GetSimpleMatchHistory(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = simplematch.GetMatchHistory(req.UserId)
	return nil
}
