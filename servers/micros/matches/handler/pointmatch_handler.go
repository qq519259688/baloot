package handler

import (
	"bet24.com/servers/micros/matches/handler/pointmatch"
	pb "bet24.com/servers/micros/matches/proto"
	"context"
)

func (h *MatchesService) CreatePointMatch(ctx context.Context, req *pb.CreatePointMatch_req, rsp *pb.Response) error {
	rsp.RetCode, rsp.Data = pointmatch.CreateMatch(req.UserId, req.GameId, req.GameRule,
		req.TotalUser, req.TableUser, req.EnrollFee, req.Prize, req.PlayTime,
		req.EliminateScore, req.ShrinkSec, req.ShrinkScore, req.WinnerCount)
	return nil
}

func (h *MatchesService) ClosePointMatch(ctx context.Context, req *pb.PointMatch_req, rsp *pb.Response) error {
	rsp.RetCode, rsp.Data = pointmatch.CloseMatch(req.UserId, req.MatchNo)
	return nil
}

func (h *MatchesService) EnrollPointMatch(ctx context.Context, req *pb.EnrollPointMatch_req, rsp *pb.Response) error {
	rsp.RetCode, rsp.Data = pointmatch.EnrollMatch(req.UserId, req.NickName, req.FaceId,
		req.FaceUrl, req.MatchNo)
	return nil
}

func (h *MatchesService) QuitPointMatch(ctx context.Context, req *pb.PointMatch_req, rsp *pb.Response) error {
	rsp.BoolValue = pointmatch.QuitMatch(req.UserId, req.MatchNo)
	return nil
}

func (h *MatchesService) GetPointMatchInfo(ctx context.Context, req *pb.PointMatch_req, rsp *pb.Response) error {
	rsp.Data = pointmatch.GetMatchInfo(req.MatchNo)
	return nil
}

func (h *MatchesService) GetUserPointMatches(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = pointmatch.GetUserMatches(req.UserId)
	return nil
}

func (h *MatchesService) GetPointMatchConfigs(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = pointmatch.GetMatchConfigs()
	return nil
}

func (h *MatchesService) GetEnrolledPointMatch(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = pointmatch.GetEnrolledMatch(req.UserId)
	return nil
}

func (h *MatchesService) GetPointMatchHistory(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = pointmatch.GetMatchHistory(req.UserId)
	return nil
}
