package pointmatch

import (
	"bet24.com/log"
	privateroom "bet24.com/servers/micros/privateroom/proto"
	"fmt"
)

type matchroom struct {
	RoomNo int
	Users  []int
	Status int
	mi     *matchinstance
}

func newMatchRoom(roomNo int, mi *matchinstance) *matchroom {
	ret := new(matchroom)
	ret.RoomNo = roomNo
	ret.mi = mi
	return ret
}

func (mr *matchroom) addUser(userId int) {
	for i := 0; i < len(mr.Users); i++ {
		if mr.Users[i] == userId {
			log.Release("pointmatch.matchroom[%d] addUser %d already exist", mr.RoomNo, userId)
			return
		}
	}

	mr.Users = append(mr.Users, userId)
}

func (mr *matchroom) removeUser(userId int) {
	for i := 0; i < len(mr.Users); i++ {
		if mr.Users[i] == userId {
			mr.Users = append(mr.Users[:i], mr.Users[i+1:]...)
			return
		}
	}
	log.Release("pointmatch.matchroom[%d] removeUser %d not exist", mr.RoomNo, userId)
}

func (mr *matchroom) isEnded() bool {
	return mr.Status == privateroom.PrivateRoomStatus_Ended
}

func (mr *matchroom) isStarted() bool {
	return mr.Status >= privateroom.PrivateRoomStatus_Playing
}

func (mr *matchroom) dump() {
	var users string
	for i := 0; i < len(mr.Users); i++ {
		users = fmt.Sprintf("%s%d:%d;", users, mr.Users[i], mr.mi.getUserScore(mr.Users[i]))
	}
	log.Release("      RoomNo[%d] Status[%d] Users[%s]", mr.RoomNo, mr.Status, users)
}

func (mr *matchroom) isValidUsers(users []int) bool {
	for _, v := range users {
		if !mr.isUserExist(v) {
			log.Release("matchroom.isValidUsers %d not exist in", v)
			return false
		}
	}
	return true
}

func (mr *matchroom) isUserExist(userId int) bool {
	for _, v := range mr.Users {
		if v == userId {
			return true
		}
	}
	return false
}
