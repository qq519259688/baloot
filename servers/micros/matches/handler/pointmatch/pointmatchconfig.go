package pointmatch

import (
	"bet24.com/log"
	"bet24.com/servers/micros/matches/handler/matchbase"
)

type matchconfig struct {
	GameId      int
	GameName    string
	UserOptions []matchbase.UserOption
	GameRules   []matchbase.MatchRule // 从privateroom取

	// 比赛配置
	EliminateScores []int // 起始淘汰分数
	ShrinkSecs      []int // 改变淘汰分数时间
	ShrinkScores    []int // 改变淘汰分数delta
	WinnerCounts    []int // 最终剩下多少人赢
}

func (mc *matchconfig) addGameRule(gameRule string, ruleDesc string, targetOptions []int, userOptions []int, playTimeOptions []int) {
	//log.Release("matchconfig addGameRule gameRule = %s,ruleDesc = %s", gameRule, ruleDesc)
	for i := 0; i < len(mc.GameRules); i++ {
		if mc.GameRules[i].Name == gameRule {
			mc.GameRules[i].Desc = ruleDesc
			mc.GameRules[i].UserOptions = userOptions
			mc.GameRules[i].PlayTimeOptions = playTimeOptions
			return
		}
	}

	mc.GameRules = append(mc.GameRules, matchbase.MatchRule{Name: gameRule, Desc: ruleDesc, TargetOptions: targetOptions, UserOptions: userOptions, PlayTimeOptions: playTimeOptions})
}

func (mc *matchconfig) dump() {
	log.Release("    GameId:%d GameName:%s,UserOptions[%v]", mc.GameId, mc.GameName, mc.UserOptions)
	log.Release("    EliminateScores:%v ShrinkSecs:%v,ShrinkScores:%v,WinnerCounts:%v", mc.EliminateScores, mc.ShrinkSecs, mc.ShrinkScores, mc.WinnerCounts)
	for _, v := range mc.GameRules {
		log.Release("      Rule[%s] UserOptions[%v],PlayTimeOptions[%v]", v.Name, v.UserOptions, v.PlayTimeOptions)
	}
}

func (mc *matchconfig) isValidParams(gameRule string, totalUserCount, tableUserCount, playTime int,
	eliminateScore, shrinkSec, shrinkScore, winnerCount int) bool {
	var mr *matchbase.MatchRule
	for i := 0; i < len(mc.GameRules); i++ {
		if mc.GameRules[i].Name == gameRule {
			mr = &mc.GameRules[i]
			break
		}
	}
	if mr == nil {
		log.Release("pointmatch.matchconfig.isValidParams gamerRule[%s] not found", gameRule)
		return false
	}

	if totalUserCount <= tableUserCount || totalUserCount <= winnerCount {
		log.Release("pointmatch.matchconfig.isValidParams totalUserCount invalid %d,%d,%d", totalUserCount, tableUserCount, winnerCount)
		return false
	}

	if !mc.isValueIn(tableUserCount, mr.UserOptions) {
		log.Release("matchconfig.isValidParams tableUserCount[%d] not of [%v]", tableUserCount, mr.UserOptions)
		return false
	}

	if !mc.isValueIn(eliminateScore, mc.EliminateScores) {
		log.Release("matchconfig.isValidParams eliminateScore[%d] not of [%v]", eliminateScore, mc.EliminateScores)
		return false
	}

	if !mc.isValueIn(shrinkSec, mc.ShrinkSecs) {
		log.Release("matchconfig.isValidParams shrinkSec[%d] not of [%v]", shrinkSec, mc.ShrinkSecs)
		return false
	}
	if !mc.isValueIn(shrinkScore, mc.ShrinkScores) {
		log.Release("matchconfig.isValidParams shrinkScore[%d] not of [%v]", shrinkScore, mc.ShrinkScores)
		return false
	}
	if !mc.isValueIn(winnerCount, mc.WinnerCounts) {
		log.Release("matchconfig.isValidParams winnerCount[%d] not of [%v]", winnerCount, mc.WinnerCounts)
		return false
	}

	// 人数限制,有上下限，暂不限制
	/*for i := 0; i < len(mr.UserOptions); i++ {
		if mc.UserOptions[i].TableUser == tableUserCount {
			if mc.isValueIn(totalUserCount, mc.UserOptions[i].TotalUser) {
				return true
			}
		}
	}
	*/
	return true
}

func (mc *matchconfig) isValueIn(value int, values []int) bool {
	if len(values) == 0 {
		return true
	}
	for i := 0; i < len(values); i++ {
		if values[i] == value {
			return true
		}
	}
	return false
}
