package pointmatch

type arrangeuser struct {
	UserId  int
	Level   int
	IP      string
	History []int // 组合历史记录
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func (au *arrangeuser) isConflict(a *arrangeuser) bool {
	if a.IP == au.IP && a.IP != "" {
		return true
	}

	historyLen := min(len(a.History), len(au.History))
	for i := 0; i < historyLen; i++ {
		if a.History[i] == au.History[i] {
			return true
		}
	}
	return false
}
