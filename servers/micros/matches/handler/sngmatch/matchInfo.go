package sngmatch

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	"bet24.com/servers/micros/matches/handler/matchbase"
	task "bet24.com/servers/micros/task/proto"
	"sync"
	"time"
)

type matchInfo struct {
	MatchId       int
	MatchNo       int
	matchInstance matchbase.MatchInstance
	EndTime       int64
	StartTime     int64
	createTime    int64

	mm *sngmatchMgr

	lock    *sync.RWMutex
	userFee map[int]item.ItemPack

	robotConfig *matchbase.Robot_config

	robotCount int

	userAwarded map[int]bool

	enrolledUsers []matchbase.EnrollUser
}

func newMatchInfo(matchId, matchNo int, instance matchbase.MatchInstance, mm *sngmatchMgr, robotConfig *matchbase.Robot_config) *matchInfo {
	ret := &matchInfo{
		MatchId:       matchId,
		MatchNo:       matchNo,
		matchInstance: instance,
		mm:            mm,
		createTime:    time.Now().Unix(),
	}
	ret.userFee = make(map[int]item.ItemPack)
	ret.userAwarded = make(map[int]bool)
	ret.lock = &sync.RWMutex{}
	ret.robotConfig = robotConfig
	if robotConfig != nil && robotConfig.Max > 0 {
		sec := robotConfig.GetWaitSec()
		time.AfterFunc(time.Second*time.Duration(sec), ret.checkRobot)
	}
	return ret
}

func (mi *matchInfo) getStatus() int {
	if mi.matchInstance == nil {
		return 0
	}
	return mi.matchInstance.GetStatus()
}

func (mi *matchInfo) addEnrollUser(userId int) {
	for k, v := range mi.enrolledUsers {
		if v.UserId == userId {
			mi.enrolledUsers[k].EnrollTime = time.Now().Unix()
			return
		}
	}
	mi.enrolledUsers = append(mi.enrolledUsers, matchbase.EnrollUser{UserId: userId, EnrollTime: time.Now().Unix()})
}

func (mi *matchInfo) isFull() bool {
	if mi.matchInstance == nil {
		return true
	}
	return mi.matchInstance.IsFull()
}

func (mi *matchInfo) isUserEnrolled(userId int) bool {
	if mi.matchInstance == nil {
		return false
	}
	return mi.matchInstance.IsUserEnrolled(userId)
}

func (mi *matchInfo) isUserPlaying(userId int) bool {
	if mi.getStatus() == matchbase.MatchStatus_Ended {
		return false
	}
	if !mi.isUserEnrolled(userId) {
		return false
	}
	instance := mi.matchInstance
	if instance == nil {
		return true
	}
	userlist := instance.GetUserList()
	for _, v := range userlist {
		if v == userId {
			return true
		}
	}
	return false
}

func (mi *matchInfo) isTimeout() bool {
	if mi.matchInstance == nil {
		return true
	}
	if mi.matchInstance.GetStatus() != matchbase.MatchStatus_Ended {
		return false
	}

	return time.Now().Unix()-mi.EndTime >= match_time_out_ended
}

func (mi *matchInfo) dump() {
	status := mi.getStatus()
	if status <= matchbase.MatchStatus_Free {
		log.Release("      MatchNo[%d] Status[Waiting] IsFull[%v] CreateTime[%s]",
			mi.MatchNo, mi.isFull(), common.TimeStampToString(mi.createTime))
	} else if status == matchbase.MatchStatus_Playing {
		log.Release("      MatchNo[%d] Status[Playing] StartTime[%s]",
			mi.MatchNo, common.TimeStampToString(mi.StartTime))
	} else {
		log.Release("      MatchNo[%d] Status[Ended] StartTime[%s] EndTime[%s]",
			mi.MatchNo, common.TimeStampToString(mi.StartTime), common.TimeStampToString(mi.EndTime))
	}
}

func (mi *matchInfo) checkRobot() {
	log.Debug("sngmatch.matchInfo.checkRobot robotCount = %d ", mi.robotCount)
	if mi.getStatus() > matchbase.MatchStatus_Free || mi.robotCount >= mi.robotConfig.Max {
		log.Debug("sngmatch.matchInfo.checkRobot playing or max[%d]", mi.robotConfig.Max)
		return
	}
	sec := mi.robotConfig.GetWaitSec()
	time.AfterFunc(time.Second*time.Duration(sec), mi.checkRobot)

	if mi.isFull() {
		log.Debug("sngmatch.matchInfo.checkRobot match is full")
		return
	}

	if mi.mm.addARobot(mi.MatchId) {
		mi.robotCount++
	} else {
		log.Debug("sngmatch.matchInfo.checkRobot addARobot failed")
	}
}

func (mi *matchInfo) sendNotification(userId int, data string) {
	if mi.matchInstance == nil {
		return
	}
	mi.matchInstance.SendNotification(userId, data)
}

func (mi *matchInfo) setUserFee(userId int, fee item.ItemPack) {
	mi.lock.Lock()
	mi.userFee[userId] = fee
	mi.lock.Unlock()
}

func (mi *matchInfo) getUserFee(userId int, remove bool) item.ItemPack {
	mi.lock.Lock()
	defer mi.lock.Unlock()
	ret, ok := mi.userFee[userId]
	if !ok {
		return item.ItemPack{}
	}
	if remove {
		delete(mi.userFee, userId)
	}
	return ret
}

func (mi *matchInfo) isUserAwarded(userId int) bool {
	mi.lock.RLock()
	defer mi.lock.RUnlock()
	ret, ok := mi.userAwarded[userId]
	if !ok {
		return false
	}
	return ret
}

func (mi *matchInfo) setUserAwarded(userId int) {
	mi.lock.Lock()
	defer mi.lock.Unlock()
	mi.userAwarded[userId] = true
}

func (mi *matchInfo) getUserList() []int {
	if mi.matchInstance == nil {
		return []int{}
	}
	return mi.matchInstance.GetUserList()
}

func (mi *matchInfo) getOnlineUserCount() int {
	if mi.matchInstance == nil {
		return 0
	}
	if mi.getStatus() == matchbase.MatchStatus_Ended {
		return 0
	}
	return len(mi.matchInstance.GetUserList())
}

func (mi *matchInfo) getUser(userId int) *matchbase.MatchUser {
	if mi.matchInstance != nil {
		return mi.matchInstance.GetUser(userId)
	}
	return nil
}

func (mi *matchInfo) onMatchStart() {
	userList := mi.getUserList()
	for _, v := range userList {
		task.DoTaskAction(v, task.TaskAction_playSNG, 1, task.TaskScope{})
	}
}
