package sngmatch

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
	"encoding/json"
)

const (
	SngMatch_noti_rank = "sngmatch_rank"
)

type SngMatch_notificationInfo struct {
	Msg     string `json:",omitempty"`
	MatchId int
	UserId  int             `json:",omitempty"`
	Rank    int             `json:",omitempty"`
	Prize   []item.ItemPack `json:",omitempty"`
}

func postUserRankNotification(userId int, matchId int, matchNo int, rank int, items []item.ItemPack) {
	mi := getMatchManager().getMatchByMatchNo(matchNo)
	if mi == nil {
		log.Release("postUserRankNotification matchNo[%d] not found", matchNo)
		return
	}
	ni := SngMatch_notificationInfo{Msg: SngMatch_noti_rank, UserId: userId, MatchId: matchId, Rank: rank, Prize: items}
	d, _ := json.Marshal(ni)
	mi.sendNotification(userId, string(d))
}
