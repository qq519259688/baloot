package sngmatch

import (
	item "bet24.com/servers/micros/item_inventory/proto"
	"bet24.com/servers/micros/matches/handler/matchbase"
)

// 月卡领取
func writeRoomRecordToDB(hi *snghistory) {
	var record matchbase.MatchRecord
	record.MatchId = hi.MatchId
	record.MatchType = matchbase.MatchType_SNG
	record.MatchName = hi.Name
	record.GameId = hi.GameId
	record.GameName = hi.GameName
	record.MaxUserCount = hi.TotalUser
	record.TableUserCount = hi.TableUser
	record.EnrollFee = hi.EnrollFee
	record.StartTime = int(hi.StartTime)
	record.EndTime = int(hi.EndTime)
	record.Extra = hi.GameRule
	for _, v := range hi.EnrollUsers {
		var enrollFee []item.ItemPack
		if v.enrollFeeCount > 0 {
			enrollFee = append(enrollFee, item.ItemPack{ItemId: v.enrollFeeItemId, Count: v.enrollFeeCount})
		}
		record.Users = append(record.Users, matchbase.MatchRecord_User{
			UserId:     v.UserId,
			Fee:        enrollFee,
			EnrollTime: v.enrollTime, // 报名时间
			Rank:       v.Rank,       // -1表示未参赛只报名
			Score:      v.Score,
			Prize:      v.prize,
		})
	}
	// 报名但未参赛的待补充
	for _, v := range hi.noShowUsers {
		record.Users = append(record.Users, matchbase.MatchRecord_User{
			UserId: v.UserId,

			EnrollTime: int(v.EnrollTime), // 报名时间
			Rank:       -1,                // -1表示未参赛只报名
		})
	}
	matchbase.WriteRecordToDb(record)
}
