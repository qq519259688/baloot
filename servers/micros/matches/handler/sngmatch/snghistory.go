package sngmatch

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	"bet24.com/servers/micros/matches/handler/matchbase"
	"fmt"
)

type historyUser struct {
	matchbase.MatchUserBrief
	enrollFeeItemId int
	enrollFeeCount  int
	enrollTime      int
	prize           []item.ItemPack
}

type snghistory struct {
	// 房间信息
	matchconfig
	MatchNo int
	// 开始结束时间
	StartTime int64
	EndTime   int64
	// 参赛玩家
	EnrollUsers []historyUser
	Winners     []int

	noShowUsers []matchbase.EnrollUser
}

func (h *snghistory) dump(detail bool) {
	log.Release("    MatchId[%d] MatchNo[%d] GameName[%s] GameRule[%s] TotalUser[%d],TableUser[%d],Target[%d],Fee[%d],Prizes[%d]",
		h.MatchId, h.MatchNo, h.GameName, h.GameRule, h.TotalUser, h.TableUser, h.Target, h.EnrollFee, h.Prizes)
	log.Release("    Start[%s] End[%s] Winners %v", common.TimeStampToString(h.StartTime), common.TimeStampToString(h.EndTime), h.Winners)
	if !detail {
		return
	}
	for i := 0; i < len(h.EnrollUsers); i++ {
		v := h.EnrollUsers[i]
		log.Release("      [%d]: %d.%s", v.Rank, v.UserId, v.NickName)
	}
}

func (h *snghistory) isUserEnrolled(userId int) bool {
	for _, v := range h.EnrollUsers {
		if v.UserId == userId {
			return true
		}
	}
	return false
}

func (h *snghistory) getUsersDescForDB() string {
	var ret string
	for _, v := range h.EnrollUsers {
		ret = fmt.Sprintf("%s%d,%d,%d,%d,%d;", ret, v.UserId, v.Rank, v.Score, v.enrollFeeItemId, v.enrollFeeCount)
	}
	return ret
}

func (h *snghistory) setAllEnrolledUsers(users []matchbase.EnrollUser) {
	if len(users) == 0 {
		return
	}

	for i := 0; i < len(users); {
		if h.isUserEnrolled(users[i].UserId) {
			users = append(users[:i], users[i+1:]...)
		} else {
			i++
		}
	}

	if len(users) == 0 {
		return
	}

	h.noShowUsers = make([]matchbase.EnrollUser, len(users))
	copy(h.noShowUsers, users)
}
