package sngmatch

import (
	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
	"bet24.com/servers/micros/matches/handler/matchbase"
)

type matchconfig struct {
	MatchId   int
	Name      string
	Desc      string
	GameId    int
	GameName  string
	GameRule  string
	TotalUser int
	TableUser int
	Target    int
	PlayTime  int
	EnrollFee []item.ItemPack
	Prizes    []matchbase.Prize_config
	matchbase.Time_config

	// 扩展部分
	OnlineUser       int
	TotalPrizeAmount int    // 奖励总金币数量
	PrizeDesc        string // 奖励描述，如果为空，显示TotalPrizeAmount

	DailyFreeCount int // 每日免费次数
	LeftFreeCount  int // 剩余免费次数

	RobotConfig *matchbase.Robot_config `json:",omitempty"`

	Level            int
	EleminateByScore bool
	WinnerCount      int
}

func (mc *matchconfig) dump() {
	log.Release("    MatchId[%d],Name[%s],GameName[%s],TotalUser[%d],TableUser[%d],Target[%d],PlayTime[%d],Fee%v,Prizes%v,Online[%d],TotalPrize[%d]",
		mc.MatchId, mc.Name, mc.GameName, mc.TotalUser, mc.TableUser, mc.Target, mc.PlayTime, mc.EnrollFee, mc.Prizes, mc.OnlineUser, mc.TotalPrizeAmount)
	if mc.RobotConfig != nil {
		log.Release("      RobotConfig:%v", *mc.RobotConfig)
	}
}

func (mc *matchconfig) getPrizes(rank int) []item.ItemPack {
	var ret []item.ItemPack
	for _, v := range mc.Prizes {
		if v.Rank == rank {
			return v.Prize
		}
	}
	return ret
}

func (mc *matchconfig) calTotalPrize() {
	if len(mc.EnrollFee) == 0 {
		mc.DailyFreeCount = 0
	}
	mc.TotalPrizeAmount = 0
	for _, v := range mc.Prizes {
		mc.TotalPrizeAmount += v.GetPrizeValue()
	}
}

func (mc *matchconfig) addOnline(add int) {
	mc.OnlineUser += add
	if mc.OnlineUser < 0 {
		mc.OnlineUser = 0
	}
}

func (mc *matchconfig) setLeftFreeCount(userId int) {
	mc.RobotConfig = nil
	if mc.DailyFreeCount == 0 {
		return
	}

	mc.LeftFreeCount = mc.DailyFreeCount - getFreeCountManager().getUserFreeCount(userId, mc.MatchId)
}
