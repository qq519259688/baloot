package sngmatch

import (
	"bet24.com/log"
)

func Run() {
	log.Release("sngmatch.Run")
	getMatchManager().run()
	getHistoryManager().run()
	getFreeCountManager().run()
}

func Dump(param1, param2 string) {
	if param1 == "free" {
		getFreeCountManager().dump(param2)
		return
	}
	getMatchManager().dump(param1, param2)
}

// 参加比赛，成功后返回MatchNo
func EnrollSngMatch(matchId int, userId int, nickname string, faceId int, faceUrl string, feeIndex int) (int, string) {
	return getMatchManager().enrollSngMatch(matchId, userId, nickname, faceId, faceUrl, feeIndex)
}

func QuitSngMatch(matchId int, userId int) bool {
	return getMatchManager().quitSngMatch(matchId, userId)
}

func GetMatchList(userId int) string {
	return getMatchManager().getMatchList(userId)
}

func Flush() {
	getHistoryManager().forceFlush()
}

func GetSngMatchHistory(userId int) string {
	return getHistoryManager().getHistory(userId)
}

func GetUserSngMatchId(userId int) int {
	ret := getMatchManager().getUserMatchId(userId)
	log.Debug("GetUserSngMatchId [%d] returns %d", userId, ret)
	return ret
}
