package handler

import (
	"bet24.com/servers/micros/matches/handler/singlematch"
	pb "bet24.com/servers/micros/matches/proto"
	"context"
)

func (h *MatchesService) CreateUserSingleMatch(ctx context.Context, req *pb.SingleMatch_req, rsp *pb.SingleMatch_resp) error {
	rsp.ErrorMsg, rsp.MatchData = singlematch.CreateUserSingleMatch(req.UserId, req.MatchId)
	if rsp.MatchData != nil {
		rsp.Success = true
	}
	return nil
}

func (h *MatchesService) GetUserSingleMatch(ctx context.Context, req *pb.SingleMatch_req, rsp *pb.SingleMatch_resp) error {
	rsp.MatchData = singlematch.GetUserSingleMatch(req.UserId)
	return nil
}

func (h *MatchesService) GetSingleMatchList(ctx context.Context, req *pb.SingleMatch_req, rsp *pb.SingleMatch_resp) error {
	rsp.ErrorMsg = singlematch.GetSingleMatchList()
	return nil
}

func (h *MatchesService) SingleMatchRevive(ctx context.Context, req *pb.SingleMatch_req, rsp *pb.SingleMatch_resp) error {
	rsp.Success = singlematch.SingleMatchRevive(req.UserId)
	return nil
}

func (h *MatchesService) SingleMatchNoRevive(ctx context.Context, req *pb.SingleMatch_req, rsp *pb.SingleMatch_resp) error {
	rsp.Success = singlematch.SingleMatchNoRevive(req.UserId)
	return nil
}
