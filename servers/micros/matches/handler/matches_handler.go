package handler

import (
	"bet24.com/log"
	"bet24.com/servers/micros/matches/handler/combomatch"
	"bet24.com/servers/micros/matches/handler/matchbase"
	"bet24.com/servers/micros/matches/handler/pointmatch"
	"bet24.com/servers/micros/matches/handler/setsmatch"
	"bet24.com/servers/micros/matches/handler/simplematch"
	"bet24.com/servers/micros/matches/handler/singlematch"
	"bet24.com/servers/micros/matches/handler/sngmatch"
	pb "bet24.com/servers/micros/matches/proto"
	"context"
	"fmt"
)

var instance *MatchesService

func GetInstance() *MatchesService {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *MatchesService {
	ret := new(MatchesService)
	ret.ctor()
	return ret
}

type MatchesService struct{}

func (h *MatchesService) ctor() {
	// 等服务启动后再启动自己
	go func() {
		matchbase.Run()
		simplematch.Run()
		pointmatch.Run()
		setsmatch.Run()
		sngmatch.Run()
		combomatch.Run()
		singlematch.Run()
	}()
}

func (d *MatchesService) dump(cmd, param1, param2 string) {
	switch cmd {
	case "simplematch":
		simplematch.Dump(param1, param2)
	case "pointmatch":
		pointmatch.Dump(param1, param2)
	case "sngmatch":
		sngmatch.Dump(param1, param2)
	case "combomatch":
		combomatch.Dump(param1, param2)
	case "setsmatch":
		setsmatch.Dump(param1, param2)
	case "singlematch":
		singlematch.Dump(param1, param2)
	default:
		log.Debug("MatchesService.dump %s,%s,%s", cmd, param1, param2)
	}
}

func (h *MatchesService) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}
