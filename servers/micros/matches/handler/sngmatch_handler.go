package handler

import (
	"bet24.com/servers/micros/matches/handler/sngmatch"
	pb "bet24.com/servers/micros/matches/proto"
	"context"
)

func (h *MatchesService) EnrollSngMatch(ctx context.Context, req *pb.EnrollSngMatch_req, rsp *pb.Response) error {
	rsp.RetCode, rsp.Data = sngmatch.EnrollSngMatch(req.MatchId, req.UserId, req.NickName, req.FaceId,
		req.FaceUrl, req.FeeIndex)
	return nil
}

func (h *MatchesService) QuitSngMatch(ctx context.Context, req *pb.SngMatch_req, rsp *pb.Response) error {
	rsp.BoolValue = sngmatch.QuitSngMatch(req.MatchId, req.UserId)
	return nil
}

func (h *MatchesService) GetSngMatchList(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = sngmatch.GetMatchList(req.UserId)
	return nil
}

func (h *MatchesService) GetSngMatchHistory(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = sngmatch.GetSngMatchHistory(req.UserId)
	return nil
}

func (h *MatchesService) GetUserSngMatchId(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.RetCode = sngmatch.GetUserSngMatchId(req.UserId)
	return nil
}
