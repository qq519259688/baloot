package handler

import (
	"bet24.com/servers/micros/matches/handler/setsmatch"
	pb "bet24.com/servers/micros/matches/proto"
	"context"
)

func (h *MatchesService) QuitSetsMatch(ctx context.Context, req *pb.SetsMatch_req, rsp *pb.Response) error {
	rsp.BoolValue = setsmatch.QuitMatch(req.UserId, req.MatchNo)
	return nil
}

func (h *MatchesService) GetSetsMatchInfo(ctx context.Context, req *pb.SetsMatch_req, rsp *pb.Response) error {
	rsp.Data = setsmatch.GetMatchInfo(req.MatchNo)
	return nil
}

func (h *MatchesService) GetUserSetsMatches(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = setsmatch.GetUserMatches(req.UserId)
	return nil
}

func (h *MatchesService) GetSetsMatchConfigs(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = setsmatch.GetMatchConfigs()
	return nil
}

func (h *MatchesService) GetEnrolledSetsMatch(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = setsmatch.GetEnrolledMatch(req.UserId)
	return nil
}

func (h *MatchesService) GetSetsMatchHistory(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = setsmatch.GetMatchHistory(req.UserId)
	return nil
}
