package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

type CreateSimpleMatch_req struct {
	Request
	GameId            int
	GameRule          string
	TotalUser         int
	TableUser         int
	Target            int
	EnrollFee         int
	Prize             int
	PlayTime          int
	ElimiantedByScore bool
	WinnerCount       int
}

type SimpleMatch_req struct {
	Request
	MatchNo int
}

type EnrollSimpleMatch_req struct {
	SimpleMatch_req
	NickName string
	FaceId   int
	FaceUrl  string
}

func CreateSimpleMatch(userId int, gameId int, gameRule string, totalUserCount int,
	target int, tableUserCount int, enrollFee int, prize int, playTime int) (int, string) {
	var req CreateSimpleMatch_req
	req.UserId = userId
	req.GameId = gameId
	req.GameRule = gameRule
	req.TotalUser = totalUserCount
	req.TableUser = tableUserCount
	req.Target = target
	req.EnrollFee = enrollFee
	req.Prize = prize
	req.PlayTime = playTime

	reply := &Response{}
	err := getClient().Call(context.Background(), "CreateSimpleMatch", &req, reply)
	if err != nil {
		log.Debug("matches.CreateSimpleMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, "server error"
	}
	return reply.RetCode, reply.Data
}

func CloseSimpleMatch(userId int, matchNo int) (int, string) {
	var req SimpleMatch_req
	req.UserId = userId
	req.MatchNo = matchNo
	reply := &Response{}
	err := getClient().Call(context.Background(), "CloseSimpleMatch", &req, reply)
	if err != nil {
		log.Debug("matches.CloseSimpleMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, "server error"
	}
	return reply.RetCode, reply.Data
}

func EnrollSimpleMatch(userId int, matchNo int, nickname string, faceId int, faceUrl string) (int, string) {
	var req EnrollSimpleMatch_req
	req.UserId = userId
	req.MatchNo = matchNo
	req.NickName = nickname
	req.FaceId = faceId
	req.FaceUrl = faceUrl
	reply := &Response{}
	err := getClient().Call(context.Background(), "EnrollSimpleMatch", &req, reply)
	if err != nil {
		log.Debug("matches.EnrollSimpleMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, "server error"
	}
	return reply.RetCode, reply.Data
}

func QuitSimpleMatch(userId int, matchNo int) bool {
	var req SimpleMatch_req
	req.UserId = userId
	req.MatchNo = matchNo
	reply := &Response{}
	err := getClient().Call(context.Background(), "QuitSimpleMatch", &req, reply)
	if err != nil {
		log.Debug("matches.QuitSimpleMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.BoolValue
}

func GetSimpleMatchInfo(matchNo int) string {
	log.Debug("GetSimpleMatchInfo %d", matchNo)
	var req SimpleMatch_req
	req.MatchNo = matchNo
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetSimpleMatchInfo", &req, reply)
	if err != nil {
		log.Debug("matches.GetSimpleMatchInfo failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetUserSimpleMatches(userId int) string {
	log.Debug("GetUserSimpleMatches %d", userId)
	var req Request
	req.UserId = userId
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetUserSimpleMatches", &req, reply)
	if err != nil {
		log.Debug("matches.GetUserSimpleMatches failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetSimpleMatchConfigs() string {
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetSimpleMatchConfigs", nil, reply)
	if err != nil {
		log.Debug("matches.GetSimpleMatchConfigs failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetEnrolledSimpleMatch(userId int) string {
	var req Request
	req.UserId = userId
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetEnrolledSimpleMatch", req, reply)
	if err != nil {
		log.Debug("matches.GetEnrolledSimpleMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetSimpleMatchHistory(userId int) string {
	var req Request
	req.UserId = userId
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetSimpleMatchHistory", req, reply)
	if err != nil {
		log.Debug("matches.GetSimpleMatchHistory failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}
