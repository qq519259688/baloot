package proto

import (
	"context"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "matches"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name   string
	UserId int
}

type Response struct {
	RetCode   int
	Data      string
	BoolValue bool
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	args := &Request{
		Name: name,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("matches failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}
