package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

type CreatePointMatch_req struct {
	Request
	GameId         int
	GameRule       string
	TotalUser      int
	TableUser      int
	EnrollFee      int
	Prize          int
	PlayTime       int
	EliminateScore int
	ShrinkSec      int
	ShrinkScore    int
	WinnerCount    int
}

type PointMatch_req struct {
	Request
	MatchNo int
}

type EnrollPointMatch_req struct {
	PointMatch_req
	NickName string
	FaceId   int
	FaceUrl  string
}

func CreatePointMatch(userId int, gameId int, gameRule string, totalUserCount int,
	tableUserCount int, enrollFee int, prize int, playTime int,
	eliminateScore, shrinkSec, shrinkScore, winnerCount int) (int, string) {
	var req CreatePointMatch_req
	req.UserId = userId
	req.GameId = gameId
	req.GameRule = gameRule
	req.TotalUser = totalUserCount
	req.TableUser = tableUserCount
	req.EnrollFee = enrollFee
	req.Prize = prize
	req.PlayTime = playTime
	req.EliminateScore = eliminateScore
	req.ShrinkSec = shrinkSec
	req.ShrinkScore = shrinkScore
	req.WinnerCount = winnerCount

	reply := &Response{}
	err := getClient().Call(context.Background(), "CreatePointMatch", &req, reply)
	if err != nil {
		log.Debug("matches.CreatePointMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, "server error"
	}
	return reply.RetCode, reply.Data
}

func ClosePointMatch(userId int, matchNo int) (int, string) {
	var req PointMatch_req
	req.UserId = userId
	req.MatchNo = matchNo
	reply := &Response{}
	err := getClient().Call(context.Background(), "ClosePointMatch", &req, reply)
	if err != nil {
		log.Debug("matches.ClosePointMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, "server error"
	}
	return reply.RetCode, reply.Data
}

func EnrollPointMatch(userId int, matchNo int, nickname string, faceId int, faceUrl string) (int, string) {
	var req EnrollPointMatch_req
	req.UserId = userId
	req.MatchNo = matchNo
	req.NickName = nickname
	req.FaceId = faceId
	req.FaceUrl = faceUrl
	reply := &Response{}
	err := getClient().Call(context.Background(), "EnrollPointMatch", &req, reply)
	if err != nil {
		log.Debug("matches.EnrollPointMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, "server error"
	}
	return reply.RetCode, reply.Data
}

func QuitPointMatch(userId int, matchNo int) bool {
	var req PointMatch_req
	req.UserId = userId
	req.MatchNo = matchNo
	reply := &Response{}
	err := getClient().Call(context.Background(), "QuitPointMatch", &req, reply)
	if err != nil {
		log.Debug("matches.QuitPointMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.BoolValue
}

func GetPointMatchInfo(matchNo int) string {
	var req PointMatch_req
	req.MatchNo = matchNo
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetPointMatchInfo", &req, reply)
	if err != nil {
		log.Debug("matches.GetPointMatchInfo failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetUserPointMatches(userId int) string {
	var req Request
	req.UserId = userId
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetUserPointMatches", &req, reply)
	if err != nil {
		log.Debug("matches.GetUserPointMatches failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetPointMatchConfigs() string {
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetPointMatchConfigs", nil, reply)
	if err != nil {
		log.Debug("matches.GetPointMatchConfigs failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetEnrolledPointMatch(userId int) string {
	var req Request
	req.UserId = userId
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetEnrolledPointMatch", req, reply)
	if err != nil {
		log.Debug("matches.GetEnrolledPointMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetPointMatchHistory(userId int) string {
	var req Request
	req.UserId = userId
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetPointMatchHistory", req, reply)
	if err != nil {
		log.Debug("matches.GetPointMatchHistory failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}
