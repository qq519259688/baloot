package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	"context"
)

type SingleMatchRoundInfo struct {
	Index     int             // 轮次
	TotalUser int             // 总人数
	Prize     []item.ItemPack // 晋级奖励
}

type SingleMatchInfo struct {
	MatchId    int
	UserId     int // 谁的比赛
	EndTime    int64
	StartTime  int64
	RoundIndex int
	UserScore  int
	Revived    bool
	Rank       int
	RoundInfo  []SingleMatchRoundInfo
	WinCount   int
	LoseCount  int
	ReviveCost item.ItemPack // 处于复活状态，复活cost
	ReviveSecs int           // 复活剩余秒数
}

type SingleMatch_req struct {
	Request
	MatchId int
}

type SingleMatch_resp struct {
	Success   bool
	ErrorMsg  string
	MatchData *SingleMatchInfo
}

func CreateUserSingleMatch(userId int, matchId int) (string, *SingleMatchInfo) {
	var req SingleMatch_req
	req.UserId = userId
	req.MatchId = matchId
	reply := &SingleMatch_resp{}
	err := getClient().Call(context.Background(), "CreateUserSingleMatch", &req, reply)
	if err != nil {
		log.Debug("matches.CreateUserSingleMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return "server error", nil
	}
	return reply.ErrorMsg, reply.MatchData
}

func GetUserSingleMatch(userId int) *SingleMatchInfo {
	var req SingleMatch_req
	req.UserId = userId
	reply := &SingleMatch_resp{}
	err := getClient().Call(context.Background(), "GetUserSingleMatch", &req, reply)
	if err != nil {
		log.Debug("matches.GetUserSingleMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.MatchData
}

func GetSingleMatchList() string {
	var req SingleMatch_req
	reply := &SingleMatch_resp{}
	err := getClient().Call(context.Background(), "GetSingleMatchList", &req, reply)
	if err != nil {
		log.Debug("matches.GetSingleMatchList failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.ErrorMsg
}

func SingleMatchRevive(userId int, giveup bool) bool {
	var req SingleMatch_req
	req.UserId = userId
	reply := &SingleMatch_resp{}
	method := "SingleMatchRevive"
	if giveup {
		method = "SingleMatchNoRevive"
	}
	err := getClient().Call(context.Background(), method, &req, reply)
	if err != nil {
		log.Debug("matches.%s failed to call: %v", method, err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.Success
}
