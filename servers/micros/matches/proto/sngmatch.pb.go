package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

type SngMatch_req struct {
	Request
	MatchId int
}

type EnrollSngMatch_req struct {
	SngMatch_req
	NickName string
	FaceId   int
	FaceUrl  string
	FeeIndex int
}

func EnrollSngMatch(userId int, matchId int, nickname string, faceId int, faceUrl string, feeIndex int) (int, string) {
	var req EnrollSngMatch_req
	req.UserId = userId
	req.MatchId = matchId
	req.NickName = nickname
	req.FaceId = faceId
	req.FaceUrl = faceUrl
	req.FeeIndex = feeIndex

	reply := &Response{}
	err := getClient().Call(context.Background(), "EnrollSngMatch", &req, reply)
	if err != nil {
		log.Debug("matches.EnrollSngMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, "server error"
	}
	return reply.RetCode, reply.Data
}

func QuitSngMatch(userId int, matchId int) bool {
	var req SngMatch_req
	req.UserId = userId
	req.MatchId = matchId

	reply := &Response{}
	err := getClient().Call(context.Background(), "QuitSngMatch", &req, reply)
	if err != nil {
		log.Debug("matches.QuitSngMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.BoolValue
}

func GetSngMatchList(userId int) string {
	var req Request
	req.UserId = userId

	reply := &Response{}
	err := getClient().Call(context.Background(), "GetSngMatchList", &req, reply)
	if err != nil {
		log.Debug("matches.GetSngMatchList failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetSngMatchHistory(userId int) string {
	var req Request
	req.UserId = userId

	reply := &Response{}
	err := getClient().Call(context.Background(), "GetSngMatchHistory", &req, reply)
	if err != nil {
		log.Debug("matches.GetSngMatchHistory failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetUserSngMatchId(userId int) int {
	var req Request
	req.UserId = userId

	reply := &Response{}
	err := getClient().Call(context.Background(), "GetUserSngMatchId", &req, reply)
	if err != nil {
		log.Debug("matches.GetUserSngMatchId failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.RetCode
}
