package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

type SetsMatch_req struct {
	Request
	MatchNo int
}

type EnrollSetsMatch_req struct {
	SetsMatch_req
	NickName string
	FaceId   int
	FaceUrl  string
}

func QuitSetsMatch(userId int, matchNo int) bool {
	var req SetsMatch_req
	req.UserId = userId
	req.MatchNo = matchNo
	reply := &Response{}
	err := getClient().Call(context.Background(), "QuitSetsMatch", &req, reply)
	if err != nil {
		log.Debug("matches.QuitSetsMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.BoolValue
}

func GetSetsMatchInfo(matchNo int) string {
	log.Debug("GetSetsMatchInfo %d", matchNo)
	var req SetsMatch_req
	req.MatchNo = matchNo
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetSetsMatchInfo", &req, reply)
	if err != nil {
		log.Debug("matches.GetSetsMatchInfo failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetUserSetsMatches(userId int) string {
	log.Debug("GetUserSetsMatches %d", userId)
	var req Request
	req.UserId = userId
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetUserSetsMatches", &req, reply)
	if err != nil {
		log.Debug("matches.GetUserSetsMatches failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetSetsMatchConfigs() string {
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetSetsMatchConfigs", nil, reply)
	if err != nil {
		log.Debug("matches.GetSetsMatchConfigs failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetEnrolledSetsMatch(userId int) string {
	var req Request
	req.UserId = userId
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetEnrolledSetsMatch", req, reply)
	if err != nil {
		log.Debug("matches.GetEnrolledSetsMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetSetsMatchHistory(userId int) string {
	var req Request
	req.UserId = userId
	reply := &Response{}
	err := getClient().Call(context.Background(), "GetSetsMatchHistory", req, reply)
	if err != nil {
		log.Debug("matches.GetSetsMatchHistory failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}
