package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

type ComboMatch_req struct {
	Request
	MatchId int
	MatchNo int
}

type EnrollComboMatch_req struct {
	ComboMatch_req
	NickName string
	FaceId   int
	FaceUrl  string
	FeeIndex int
}

type UserComboMatchId struct {
	MatchId     int
	MatchNo     int
	MatchType   int
	SecsToStart int
}

func EnrollComboMatch(userId int, matchId int, nickname string, faceId int, faceUrl string, feeIndex int) (int, string) {
	var req EnrollComboMatch_req
	req.UserId = userId
	req.MatchId = matchId
	req.NickName = nickname
	req.FaceId = faceId
	req.FaceUrl = faceUrl
	req.FeeIndex = feeIndex

	reply := &Response{}
	err := getClient().Call(context.Background(), "EnrollComboMatch", &req, reply)
	if err != nil {
		log.Debug("matches.EnrollComboMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, "server error"
	}
	return reply.RetCode, reply.Data
}

func QuitComboMatch(userId int, matchId int) bool {
	var req ComboMatch_req
	req.UserId = userId
	req.MatchId = matchId

	reply := &Response{}
	err := getClient().Call(context.Background(), "QuitComboMatch", &req, reply)
	if err != nil {
		log.Debug("matches.QuitComboMatch failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.BoolValue
}

func GetComboMatchList(userId int) string {
	var req Request
	req.UserId = userId

	reply := &Response{}
	err := getClient().Call(context.Background(), "GetComboMatchList", &req, reply)
	if err != nil {
		log.Debug("matches.GetComboMatchList failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetComboMatchHistory(userId int) string {
	var req Request
	req.UserId = userId

	reply := &Response{}
	err := getClient().Call(context.Background(), "GetComboMatchHistory", &req, reply)
	if err != nil {
		log.Debug("matches.GetComboMatchHistory failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetUserComboMatchId(userId int) string {
	var req Request
	req.UserId = userId

	reply := &Response{}
	err := getClient().Call(context.Background(), "GetUserComboMatchId", &req, reply)
	if err != nil {
		log.Debug("matches.GetUserComboMatchId failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetComboMatchInfo(matchId int, userId int) string {
	var req ComboMatch_req
	req.UserId = userId
	req.MatchId = matchId

	reply := &Response{}
	err := getClient().Call(context.Background(), "GetComboMatchInfo", &req, reply)
	if err != nil {
		log.Debug("matches.GetComboMatchInfo failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func GetComboMatchConfirmCount(matchId int) int {
	var req ComboMatch_req
	req.MatchId = matchId

	reply := &Response{}
	err := getClient().Call(context.Background(), "GetComboMatchConfirmCount", &req, reply)
	if err != nil {
		log.Debug("matches.GetComboMatchConfirmCount failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.RetCode
}

func ComboMatchConfirm(matchId int, userId int) bool {
	var req ComboMatch_req
	req.MatchId = matchId
	req.UserId = userId
	reply := &Response{}
	err := getClient().Call(context.Background(), "ComboMatchConfirm", &req, reply)
	if err != nil {
		log.Debug("matches.ComboMatchConfirm failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}

	return reply.BoolValue
}
