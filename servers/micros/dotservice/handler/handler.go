package handler

import (
	"context"
	"fmt"
	"strconv"

	"bet24.com/log"
	pb "bet24.com/servers/micros/dotservice/proto"
)

var instance *dot

type dot struct {
}

func GetInstance() *dot {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func newHandler() *dot {
	ret := new(dot)
	ret.ctor()
	return ret
}

func (g *dot) ctor() {
	newDotMgr()
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func (g *dot) dump(cmd, param1, param2 string) {
	switch cmd {
	case "config":
		mgr.dumpConfig()
	case "user":
		mgr.dumpUser(param1)
	case "exit":
		mgr.dumpClear()
	default:
		log.Release("dotservice.Dump unhandled cmd %s", cmd)
	}
}

func (g *dot) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

// 添加打点
func (g *dot) AddDot(ctx context.Context, req *pb.Request_AddDot, rsp *pb.Response) error {
	u := ""
	if req.UserId > 0 {
		u = strconv.Itoa(req.UserId)
	} else {
		u = req.DotScope.IpAddress
	}

	mgr.addDot(u, req.DotScope)
	return nil
}

// 获取打点统计
func (g *dot) GetStatList(ctx context.Context, req *pb.Request_List, rsp *pb.Response_StatList) error {
	rsp.List = mgr.getStatList(req.BeginTime, req.EndTime, req.Event)
	return nil
}

// 获取打点任务统计
func (g *dot) GetTaskStatList(ctx context.Context, req *pb.Request_List, rsp *pb.Response_StatList) error {
	rsp.List = mgr.getTaskStatList(req.BeginTime, req.EndTime)
	return nil
}

// 打点列表
func (g *dot) GetConfigListNotTask(ctx context.Context, req *pb.Request, rsp *pb.Response_StatList) error {
	rsp.List = mgr.getConfigListNotTask()
	return nil
}
