package handler

import (
	"sync"
	"time"

	"bet24.com/servers/common"

	pb "bet24.com/servers/micros/dotservice/proto"

	"bet24.com/log"
)

var mgr *dotMgr

type dotMgr struct {
	config_list []*pb.DotConfig
	user_list   map[string]*userInfo // 用户列表
	lock        *sync.RWMutex        // 用户锁
}

func newDotMgr() {
	mgr = new(dotMgr)
	mgr.lock = &sync.RWMutex{}
	mgr.user_list = make(map[string]*userInfo)

	mgr.loadConfig()
	mgr.doCheck()

	return
}

// 轮询
func (this *dotMgr) doCheck() {
	ticker := time.NewTicker(30 * time.Second)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				this.checkExpire()
			}
		}
	}(ticker)
}

// 检查用户
func (this *dotMgr) checkExpire() {
	this.lock.Lock()
	defer this.lock.Unlock()

	// 遍历所有用户
	for _, u := range this.user_list {
		// 判断是否过期
		if !u.dealExpire() {
			continue
		}

		// 删除
		delete(this.user_list, u.userId)

		log.Debug("manager.checkExpire userId=%d 已完成清理", u.userId)
	}

	return
}

// 获取用户
func (this *dotMgr) getUser(userId string) *userInfo {
	this.lock.RLock()
	u, ok := this.user_list[userId]
	this.lock.RUnlock()
	if ok {
		return u
	}

	u = newUserInfo(userId)
	this.lock.Lock()
	this.user_list[userId] = u
	this.lock.Unlock()
	return u
}

// 添加打点
func (this *dotMgr) addDot(userId string, scope pb.DotScope) {
	u := this.getUser(userId)
	if u == nil {
		log.Debug("manager.addDot userId=%s scope=%+v", userId, scope)
		return
	}

	log.Debug("manager.addDot userId=%s scope=%+v", userId, scope)

	// 添加打点
	u.add(scope)

	return
}

// 获取打点统计
func (this *dotMgr) getStatList(beginTime, endTime, event string) []pb.StatInfo {
	list := trans_statList(beginTime, endTime, event)
	return list
}

// 获取打点任务统计
func (this *dotMgr) getTaskStatList(beginTime, endTime string) []pb.TaskStatInfo {
	list := trans_taskStatList(beginTime, endTime)
	return list
}

// 关服前保存数据
func (this *dotMgr) dumpClear() {
	this.lock.Lock()
	defer this.lock.Unlock()

	// 遍历所有用户
	for _, u := range this.user_list {

		// 保存并清理数据
		u.saveAndClearData()

		// 删除
		delete(this.user_list, u.userId)

		log.Debug("manager.dumpClear userId=%s 已完成清理", u.userId)
	}
}

func (this *dotMgr) dumpConfig() {

	log.Debug(" ^_^ 开始打印配置数据，(%d)个", len(this.config_list))

	for _, v := range this.config_list {
		log.Debug("%+v", v)
	}

	log.Debug("完成数据打印 ^_^")
}

func (this *dotMgr) dumpUser(param1 string) {
	userId := param1

	log.Debug(" ^_^ 开始打印用户数据，用户(%d)人", len(this.user_list))

	this.lock.RLock()
	for uid, v := range this.user_list {
		if userId != "" && userId != uid {
			continue
		}

		log.Debug("用户[%s]信息,缓存数据=%+v,过期时间[%s]:", uid, v.DotScope, time.Unix(int64(v.timeStamp), 0).Format(common.Layout))
		log.Debug("打点列表打印(%d)个：", len(v.dot_list))

		for _, d := range v.dot_list {
			log.Debug("%+v", d)
		}

		log.Debug("**********************************************")
	}
	this.lock.RUnlock()

	log.Debug("完成用户数据打印 ^_^")
}
