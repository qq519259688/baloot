package handler

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/dotservice/proto"
)

// 添加数据
func trans_insert(userId string, info pb.DotInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserDot_Insert")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdVarChar, 16, userId)
	statement.AddParamter("@DateFlag", database.AdParamInput, database.AdVarChar, 20, info.DateFlag)
	statement.AddParamter("@Scene", database.AdParamInput, database.AdVarChar, 32, info.Scene)
	statement.AddParamter("@Event", database.AdParamInput, database.AdVarChar, 32, info.Event)
	statement.AddParamter("@Action", database.AdParamInput, database.AdInteger, 4, info.Action)
	statement.AddParamter("@Times", database.AdParamInput, database.AdInteger, 4, info.Times)
	sqlString := statement.GenSql()
	log.Debug(sqlString)
	dbengine.Execute(sqlString)
	return
}

// 获取打点统计
func trans_statList(beginTime, endTime, event string) []pb.StatInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserDot_GetStat")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	statement.AddParamter("@Event", database.AdParamInput, database.AdVarChar, 32, event)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	var ret []pb.StatInfo
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("trans_statList json unmarshal err %v", err)
	}
	return ret
}

// 获取打点任务统计
func trans_taskStatList(beginTime, endTime string) []pb.TaskStatInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserDot_GetTaskStat")
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 20, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 20, endTime)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	var ret []pb.TaskStatInfo
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("trans_taskStatList json unmarshal err %v", err)
	}
	return ret
}
