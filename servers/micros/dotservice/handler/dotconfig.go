package handler

import (
	"encoding/json"
	"os"
	"time"

	"bet24.com/log"
	pb "bet24.com/servers/micros/dotservice/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "dot_config"

func (this *dotMgr) loadConfig() {
	this.loadRedisConfig()
	go time.AfterFunc(5*time.Minute, this.loadConfig)
}

func (this *dotMgr) loadRedisConfig() {
	if data := platformconfig.GetConfig(config_key); data != "" {
		err := json.Unmarshal([]byte(data), &this.config_list)
		if err == nil {
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
		return
	}

	if data, err := os.ReadFile("serviceconf/dotConfig.json"); err == nil {
		err = json.Unmarshal([]byte(data), &this.config_list)
		if err == nil {
			platformconfig.SetConfig(config_key, string(data))
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
	} else {
		log.Release("read config failed serviceconf/dotConfig.json %v", err)
	}

	return
}

// 获取标签配置列表
func (this *dotMgr) getConfigList() []*pb.DotConfig {
	return this.config_list
}

// 获取标签配置列表(任务除外)
func (this *dotMgr) getConfigListNotTask() []*pb.DotConfig {
	var ret []*pb.DotConfig
	for _, v := range this.config_list {
		if v.Scene == pb.Scene_Task {
			continue
		}

		ret = append(ret, v)
	}
	return ret
}
