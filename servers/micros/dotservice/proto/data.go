package proto

const (
	Action_Invalid  = iota // 0=无效
	Action_Click           // 1=点击
	Action_Complete        // 2=完成
	Action_Awarded         // 3=已领取
)

const (
	Scene_Login              = "登录"
	Scene_Shop               = "商城"
	Scene_Game               = "游戏"
	Scene_Match              = "赛事"
	Scene_Task               = "任务"
	Scene_HotUpdate          = "热更"
	Scene_NoviceWelfare_Task = "新手任务"
	Scene_DailyWheel         = "每日转盘"
)

// 配置
type DotConfig struct {
	Scene string `json:",omitempty"` // 场景
	Event string `json:",omitempty"` // 事件
	Extra string `json:",omitempty"` // 扩展参数
}

// 打点信息
type DotInfo struct {
	DateFlag string // 日期标识
	Scene    string // 场景
	Event    string // 事件
	Action   int    // 动作(1=点击  2=完成  3=领取)
	Times    int    // 次数
}

// 范围
type DotScope struct {
	Scene     string `json:",omitempty"` // 场景
	Action    int    `json:",omitempty"` // 动作(1=点击  2=完成  3=领取)
	Extra     string `json:",omitempty"` // 扩展参数
	IpAddress string `json:",omitempty"` // ip地址
}

// 统计信息
type StatInfo struct {
	DateFlag          string // 日期
	Event             string // 事件
	LabelID           string // 标签
	Days              int    // 天数(0=当天  7=过去7天  30=过去30天)
	ClickTimes        int    // 点击次数
	ClickUserCount    int    // 点击人数
	CompleteUserCount int    // 完成人数
}

// 任务统计信息
type TaskStatInfo struct {
	DateFlag  string // 日期
	Event     string // 事件
	Action    int    // 动作(1=点击  2=完成  3=领取)
	UserCount int    // 用户人数
}
