package proto

import (
	"context"
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "dotservice"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	UserId    int    `json:",omitempty"`
	Name      string `json:",omitempty"`
	IpAddress string `json:",omitempty"`
}

type Response struct {
	RetCode int
	Data    string
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("dotservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	//log.Debug("dotservice return %s", reply.Data)
	return reply.Data
}

type Request_AddDot struct {
	UserId   int // 用户id
	DotScope     // 范围
}

// 添加打点
func AddDot(userId int, scope DotScope) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_AddDot{
		UserId:   userId,
		DotScope: scope,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "AddDot", args, reply)
	if err != nil {
		log.Debug("dotservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return
	}

	//log.Debug("dotservice return %s", reply.Data)
	return
}

type Request_List struct {
	BeginTime string // 开始时间
	EndTime   string // 截止时间
	Event     string // 事件
}

type Response_StatList struct {
	RecordCount int
	List        interface{}
}

// 获取打点统计
func GetStatList(beginTime, endTime, event string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_List{
		BeginTime: beginTime,
		EndTime:   endTime,
		Event:     event,
	}

	reply := &Response_StatList{}

	err := xclient.Call(context.Background(), "GetStatList", args, reply)
	if err != nil {
		log.Debug("dotservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	buf, _ := json.Marshal(reply)
	return string(buf)
}

// 获取打点任务统计
func GetTaskStatList(beginTime, endTime string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_List{
		BeginTime: beginTime,
		EndTime:   endTime,
	}

	reply := &Response_StatList{}

	err := xclient.Call(context.Background(), "GetTaskStatList", args, reply)
	if err != nil {
		log.Debug("dotservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	buf, _ := json.Marshal(reply)
	return string(buf)
}

// 打点列表
func GetConfigListNotTask() string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{}

	reply := &Response_StatList{}

	err := xclient.Call(context.Background(), "GetConfigListNotTask", args, reply)
	if err != nil {
		log.Debug("dotservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	buf, _ := json.Marshal(reply)
	return string(buf)
}
