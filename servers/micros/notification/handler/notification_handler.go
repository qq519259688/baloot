package handler

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/notification/proto"
	"context"
	"fmt"
)

var instance *Notification

func GetInstance() *Notification {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *Notification {
	ret := new(Notification)
	ret.ctor()
	return ret
}

type Notification struct {
	mgr *manager
}

func (h *Notification) ctor() {
	h.mgr = newManager()
}

func (h *Notification) dump(cmd, param1, param2 string) {
	//log.Debug("DbEngine.Dump %s,%s,%s", cmd, param1, param2)
	switch cmd {
	case "user":
		h.mgr.dumpUser(param1)
	case "receiver":
		h.mgr.dumpReceiver()
	default:
		log.Debug("notification.Dump unhandled %s:%s", cmd, param1)
	}
}

func (h *Notification) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (h *Notification) AddNotification(ctx context.Context, req *pb.Request_AddNotification, rsp *pb.Response) error {
	if h.mgr.addNotification(req.UserId, req.NotificationId, req.Data) {
		rsp.RetCode = 1
	}
	return nil
}

func (h *Notification) GetNotifications(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = h.mgr.getNotifications(req.UserId)
	return nil
}

func (h *Notification) Subscribe(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	h.mgr.subscribe(req.Name)
	return nil
}

func (d *Notification) AddUser(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	d.mgr.onUserEnter(req.UserId)
	return nil
}

func (d *Notification) RemoveUser(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	d.mgr.onUserExit(req.UserId)
	return nil
}
