package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	subscribe "bet24.com/servers/micros/notification/subscribe/server"
	"context"
	"fmt"
	"github.com/smallnest/rpcx/client"
	"time"
)

const ServiceName = "notification"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name   string
	UserId int
}

type Response struct {
	Data    string
	RetCode int
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("notification failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

type Request_AddNotification struct {
	UserId         int
	NotificationId int
	Data           string
}

func AddNotification(userId int, notificationId int, data string) bool {
	xclient := getClient()
	args := &Request_AddNotification{
		UserId:         userId,
		NotificationId: notificationId,
		Data:           data,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "AddNotification", args, reply)
	if err != nil {
		log.Debug("notification failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.RetCode == 1
}

func GetNotifications(userId int) string {
	xclient := getClient()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetNotifications", args, reply)
	if err != nil {
		log.Debug("notification failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func Subscribe(callback func(int, string), port int) {
	addr := fmt.Sprintf("notification_subscriber_%s_%d", common.GetLocalIp(), port)
	go subscribe.Run(addr, consulAddr, callback)

	for {
		xclient := getClient()
		args := &Request{
			Name: addr,
		}

		//log.Release("notification.Subscribe %s", addr)
		err := xclient.Call(context.Background(), "Subscribe", args, nil)
		if err != nil {
			log.Debug("Subscribe failed to call: %v", err)
			common.GetClientPool().RemoveClient(ServiceName)

		}
		time.Sleep(10 * time.Second)
	}

}

func AddUser(userId int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "AddUser", args, nil)
	if err != nil {
		log.Debug("AddUser failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func RemoveUser(userId int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	err := xclient.Call(context.Background(), "RemoveUser", args, nil)
	if err != nil {
		log.Debug("RemoveUser failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}
