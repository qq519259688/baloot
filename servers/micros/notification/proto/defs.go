package proto

import (
	item "bet24.com/servers/micros/item_inventory/proto"
)

const (
	_                                  = iota //无效
	Notification_Gold                         // 1=金币
	Notification_Chip                         // 2=元宝
	Notification_Vitality                     // 3=活跃度
	_                                         // 4=空闲(保留)
	_                                         // 5=空闲(保留)
	_                                         // 6=空闲(保留)
	_                                         // 7=空闲(保留)
	_                                         // 8=空闲(保留)
	_                                         // 9=空闲(保留)
	Notification_Invalid                      // 10=空闲(保留)
	Notification_Task                         // 11=任务
	Notification_Vip                          // 12=vip等级变化
	Notification_Inventory                    // 13=背包
	Notification_Cannon                       // 14=炮台
	Notification_Recharge                     // 15=充值
	Notification_Broadcast                    // 16=广播
	Notification_Redpoint                     // 17=小红点
	Notification_Chat                         // 18=聊天
	Notification_Friend                       // 19=好友
	Notification_Agent                        // 20=代理
	Notification_CouponTask                   // 21=红包券任务
	Notification_Level                        // 22=等级变化
	Notification_Experience                   // 23=经验变化
	Notification_ChannelChat                  // 24=私人聊天
	Notification_Return                       // 25=回归奖励
	Notification_SlotScore                    // 26=slot积分
	Notification_BattlePass                   // 27=battlepass完成任务
	Notification_Match                        // 28=比赛提示
	Notification_AudioRoom                    // 29=语音房
	Notification_UserInfoChanged              // 30=自己的用户信息已改变
	Notification_PrivateRoom                  // 31=私人场消息
	Notification_RoomImageChanged             // 32=房间封面变化
	Notification_VipExpire                    // 33=vip过期
	Notification_VipGiftBagUpgrade            // 34=vip礼包升级（等级发生变化后，遍历当前等级以上的道具）
	Notification_VipGiftBagPurchase           // 35=vip礼包购买
	Notification_VipGiftPack                  // 36=vip每日礼包
	Notification_VipAttributeChange           // 37=vip属性变化（等级、经验、时长）
	Notification_Exchange                     // 38=兑换
	Notification_BadgeChange                  // 39=成就徽章变化（经验、等级）
	Notification_ReceiveGift                  // 40=收到礼物通知
	Notification_GiftSent                     // 41=发送礼物成功
	Notification_NoviceWelfare                // 42=新手福利变化
	Notification_LevelRewards                 // 43=等级礼包变化
	Notification_LadderChanged                // 44=排位赛等级信息变化 {Old{Ladder,Level,Star,Point},New{Ladder,Level,Star,Point}}
	Notification_VipAddDuration               // 45=vip添加时长
	Notification_HighlyProfitableChage        // 46=一本万利变化
)

/*type NotificationTask struct {
	TaskId int
}*/

type Notification struct {
	Id   int    //通知id
	Data string //数据
}
type NotificationInventory struct {
	ItemIds []int
	Action  int //动作
}

type NotificationCannon struct {
	ItemId int
}

type NotificationVitality struct {
	DayPoint  int //日活跃度
	WeekPoint int //周活跃度
}

type NotificationRecharge struct {
	RetCode   int //操作结果
	ProductId string
	ShopType  int
	Items     []item.ItemPack //物品
	BasePrice float64
	IsRefresh bool
	IsGift    bool // 是否赠送别人礼物
}

type NotificationBroadcast struct {
	GameID   int //游戏ID 0=所有游戏
	UserId   int
	Msg      string //消息
	Priority int    //优先级
	Ext      string //扩展信息
}

type Notification_Channel struct {
	Key         string
	New         int // 未读消息数量
	LatestMsg   string
	LatestTime  string // 最近消息时间
	MessageType int
}

/*type NotificationRedpoint struct {
	MsgTip           bool // 客服留言
	MailTip          bool // 邮件(含附件)
	MailVipTip       bool // 重要邮件(含附件)
	SignTip          bool // 签到
	MonthTip         bool // 月卡
	SpreadTip        bool // 邀请
	TaskTip          bool // 任务
	TaskScene        int  // 任务场景
	FriendTip        bool // 好友
	VideoSignTip     bool // 视频广告(大厅)
	PrizeWheelTip    bool // 彩票
	ReviewTip        bool // 五星好评
	GameVideoTip     bool // 游戏返还视频
	AgentUpTip       bool // 代理升级提醒
	PurchaseTip      bool // 100K购提醒
	NoviceWelfareTip bool // 新手福利任务提醒
	DailWheelFreeTip bool // 转盘免费
}*/

type NotificationFriend struct {
	NotifyId int         //通知ID
	UserId   int         //用户ID
	Data     interface{} //数据
}

type NotificationAgent struct {
	Grade int // 代理等级
}

type NotificationCouponTask struct {
	Action     int         // 动作 1=活动状态  2=任务完成
	TodayCount int         // 今天券数量
	Data       interface{} // 数据
}

type UserNotification struct {
	UserId int
	Data   string
}

type NotificationAudioRoom struct {
	NotifyId int         // 通知id
	RoomId   int         // 房间id
	Data     interface{} // 数据
}
