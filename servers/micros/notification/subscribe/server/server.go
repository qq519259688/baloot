package server

import (
	"bet24.com/servers/micros/common"
	"bet24.com/servers/micros/notification/subscribe/client"
	"context"
)

var _addr string

func Run(addr, consulAddr string, sink func(int, string)) {
	_addr = addr
	common.RunWithoutChangeTitle(_addr, newHandler(sink), consulAddr)
}

func GetAddr() string {
	return _addr
}

func newHandler(sink func(int, string)) *SubscribeServer {
	ret := new(SubscribeServer)
	ret._callback = sink
	return ret
}

type SubscribeServer struct {
	_callback func(int, string)
}

func (s *SubscribeServer) OnNotification(ctx context.Context, req *client.Request, reply *client.Reply) error {
	s._callback(req.UserId, req.Data)
	return nil
}
