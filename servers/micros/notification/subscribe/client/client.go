package client

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
	rpcx_client "github.com/smallnest/rpcx/client"
)

var consulAddr = common.Default_Consul_Addr

func getClient(serviceName string) rpcx_client.XClient {
	return common.GetClientPool().GetClient(serviceName, consulAddr)
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

type Reply struct {
	Data    string
	RetCode int
}

type Request struct {
	UserId int
	Data   string
}

func OnNotification(addr string, userId int, data string) bool {
	//log.Debug("OnNotification addr = %s", addr)
	xclient := getClient(addr)
	reply := &Reply{}
	err := xclient.Call(context.Background(), "OnNotification", &Request{UserId: userId, Data: data}, reply)
	if err != nil {
		log.Debug("OnNotification failed to call: %v", err)
		common.GetClientPool().RemoveClient(addr)
		return false
	}
	return true
}
