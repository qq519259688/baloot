package handler

import (
	pb "bet24.com/servers/micros/money/proto"
	"context"
)

var cash *cashmgr

func RunCash() {
	cash = newCashMgr()
}

// 金币日志
func (this *Money) CashLog(ctx context.Context, req *pb.Request_CashLog, rsp *pb.Response_CashLog) error {
	rsp.RecordCount, rsp.List = cash.cashLog(req.UserId, req.BeginTime, req.EndTime, req.PageIndex, req.PageSize)
	return nil
}

// 获取金币
func (this *Money) GetMoney(ctx context.Context, req *pb.Request_base, rsp *pb.Response_GetMoney) error {
	_, rsp.Gold, _ = cash.getMoney(req.UserId)
	return nil
}

// 保险柜查询
func (this *Money) BankQuery(ctx context.Context, req *pb.Request_BankInOrOut, rsp *pb.Response_BankInOrOut) error {
	rsp.RetCode, rsp.BankAmount = cash.BankQuery(req.UserId)
	return nil
}

// 保险柜存入并通知客户端
func (this *Money) BankIn(ctx context.Context, req *pb.Request_BankInOrOut, rsp *pb.Response_BankInOrOut) error {
	rsp.RetCode, rsp.Gold, rsp.BankAmount, rsp.OutMsg = cash.BankIn(req.UserId, req.Amount, req.GameID, req.ServerName, req.IpAddress)
	return nil
}

// 保险柜取出并通知客户端
func (this *Money) BankOut(ctx context.Context, req *pb.Request_BankInOrOut, rsp *pb.Response_BankInOrOut) error {
	rsp.RetCode, rsp.Gold, rsp.BankAmount, rsp.OutMsg = cash.BankOut(req.UserId, req.Amount, req.GameID, req.ServerName, req.IpAddress)
	return nil
}

// 充值、提现记录
func (this *Money) FinanceLog(ctx context.Context, req *pb.Request_CashLog, rsp *pb.Response_FinanceLog) error {
	rsp.RecordCount, rsp.List = cash.FinanceLog(req.UserId, req.PageIndex, req.PageSize)
	return nil
}

// 加金币
func (this *Money) GiveMoney(ctx context.Context, req *pb.Request_GiveReduceMoney, rsp *pb.Response_GiveReduceMoney) error {
	rsp.Gold = cash.giveMoney(req.UserId, req.Amount, req.LogType, req.SourceName, req.Remark, req.IpAddress)
	return nil
}

// 减金币
func (this *Money) ReduceMoney(ctx context.Context, req *pb.Request_GiveReduceMoney, rsp *pb.Response_GiveReduceMoney) error {
	rsp.Success = cash.reduceMoney(req.UserId, req.Amount, req.LogType, req.SourceName, req.Remark, req.IpAddress) == 1
	return nil
}

func (this *Money) ModifyMoneyWithTax(ctx context.Context, req *pb.Request_GiveReduceMoney, rsp *pb.Response_GiveReduceMoney) error {
	rsp.Gold = cash.modifyMoneyWithTax(req.UserId, req.Amount, req.Tax, req.LogType, req.SourceName, req.Remark, req.IpAddress)
	return nil
}

// 保险柜转账
func (this *Money) BankTransfer(ctx context.Context, req *pb.Request_Transfer, rsp *pb.Response_Transfer) error {
	rsp.RetCode, rsp.StillAmount, rsp.ErrMsg = cash.BankTransfer(req.UserId, req.ToUserId, req.Amount, req.IpAddress)
	return nil
}

// 保险柜记录
func BankLog(ctx context.Context, req *pb.Request_CashLog, rsp *pb.Response_BankLog) error {
	rsp.RecordCount, rsp.List = cash.BankLog(req.UserId, req.BeginTime, req.EndTime, req.PageIndex, req.PageSize)
	return nil
}
