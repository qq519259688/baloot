package handler

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/money/proto"
)

// 筹码日志
func chipLog(userId int, beginTime, endTime string, pageIndex, pageSize int) (int, []*pb.CashInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	recordCount := 0
	var list []*pb.CashInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Chip_GetLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 32, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 32, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return recordCount, list
	}

	if rowLen > 1 {

		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info pb.CashInfo

			info.Desc = (ret[0]).(string)
			info.Amount = int((ret[1]).(int64))
			info.Balance = int((ret[2]).(int64))
			info.Date = (ret[3]).(string)
			list = append(list, &info)
		}
	}

	recordCount = int((retRows[rowLen-1][0]).(int64))
	return recordCount, list
}

// 获取筹码
func getChip(userId int) (bool, int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("inventory.getMoney transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	gold, bank := 0, 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Chip_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Gold", database.AdParamOutput, database.AdBigint, 8, gold)
	statement.AddParamter("@Bank", database.AdParamOutput, database.AdBigint, 8, bank)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return false, gold, bank
	}

	gold = int((retRows[0][0]).(int64))
	bank = int((retRows[0][1]).(int64))
	return true, gold, bank
}

// 加筹码
func giveChip(userId, amount, logType int, sourceName, remark, ipAddress string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("inventory.giveMoney transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Chip_Give")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@LogType", database.AdParamInput, database.AdInteger, 4, logType)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 32, sourceName)
	statement.AddParamter("@Remark", database.AdParamInput, database.AdNVarChar, 128, remark)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return retCode
	}

	retCode = int((retRows[0][0]).(int64))
	return retCode
}

// 减筹码
func reduceChip(userId, amount, logType int, sourceName, remark, ipAddress string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("inventory.reduceMoney transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Chip_Reduce")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@LogType", database.AdParamInput, database.AdInteger, 4, logType)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 64, sourceName)
	statement.AddParamter("@Remark", database.AdParamInput, database.AdNVarChar, 128, remark)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return retCode
	}

	retCode = int((retRows[0][0]).(int64))
	return retCode
}

// 保险柜存入
func chipBankIn(userId, amount, gameId int, serverName, ipAddress string) (int, int, int, string) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	retCode, cash, bank, outMsg := 0, 0, 0, ""

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Chip_BankIn")
	statement.AddParamter("@userid", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@servername", database.AdParamInput, database.AdNVarChar, 64, serverName)
	statement.AddParamter("@cashamount", database.AdParamOutput, database.AdBigint, 8, cash)
	statement.AddParamter("@bankamount", database.AdParamOutput, database.AdBigint, 8, bank)
	statement.AddParamter("@outmsg", database.AdParamOutput, database.AdNVarChar, 512, outMsg)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	statement.AddParamter("@EPassword", database.AdParamInput, database.AdVarChar, 32, "")
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return retCode, cash, bank, outMsg
	}

	ret := retRows[0]
	cash = int((ret[0]).(int64))
	bank = int((ret[1]).(int64))
	outMsg = (ret[2]).(string)
	retCode = int((ret[3]).(int64))
	return retCode, cash, bank, outMsg
}

// 保险柜取出
func chipBankOut(userId, amount, gameId int, serverName, ipAddress string) (int, int, int, string) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	retCode, cash, bank, outMsg := 0, 0, 0, ""
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Chip_BankOut")
	statement.AddParamter("@userid", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@safeboxpwd", database.AdParamInput, database.AdVarChar, 16, "")
	statement.AddParamter("@servername", database.AdParamInput, database.AdNVarChar, 64, serverName)
	statement.AddParamter("@cashamount", database.AdParamOutput, database.AdBigint, 8, amount)
	statement.AddParamter("@bankamount", database.AdParamOutput, database.AdBigint, 8, bank)
	statement.AddParamter("@outmsg", database.AdParamOutput, database.AdVarChar, 512, outMsg)
	statement.AddParamter("@EPassword", database.AdParamInput, database.AdVarChar, 32, "")
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return retCode, cash, bank, outMsg
	}

	ret := retRows[0]
	cash = int((ret[0]).(int64))
	bank = int((ret[1]).(int64))
	outMsg = (ret[2]).(string)
	retCode = int((ret[3]).(int64))
	return retCode, cash, bank, outMsg
}

// 保险柜查询
func chipBankQuery(userId int) (int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	bank := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Game_Chip_BankQuery")
	statement.AddParamter("@userid", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@bankamount", database.AdParamOutput, database.AdBigint, 8, bank)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return 0, bank
	}

	bank = int((retRows[0][0]).(int64))
	return 1, bank
}

// 转账(操作结果/操作描述/剩余金额/返还金额)
func chipBankTransfer(userId, toUserId, amount int, ipAddress string) (int, int, string, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	retCode, stillAmount, outMsg, refund := 0, 0, "", 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Chip_Transfer")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@AcceptUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	statement.AddParamter("@WantAmount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return retCode, stillAmount, outMsg, refund
	}

	ret := retRows[0]
	outMsg = (ret[0]).(string)
	stillAmount = int((ret[1]).(int64))
	retCode = int((ret[2]).(int64))
	refund = int((ret[3]).(int64))

	return retCode, stillAmount, outMsg, refund
}

// 赠送记录
func chipTransferLog(userId int) []*pb.TransferInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Chip_TransferLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []*pb.TransferInfo

	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var out pb.TransferInfo
		out.UserID = int((ret[0]).(int64))
		out.NickName = (ret[1]).(string)
		out.Amount = int((ret[2]).(int64))
		out.Crdate = (ret[3]).(string)
		list = append(list, &out)
	}
	return list
}

func getTransferCfg() *pb.TransferCfg {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_TransferCfg_GetInfo")
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return &pb.TransferCfg{}
	}

	ret := retRows[0]
	return &pb.TransferCfg{
		Fee:             int((ret[0]).(int64)),
		FreeBeginTime:   (ret[1]).(string),
		FreeEndTime:     (ret[2]).(string),
		FreeFlag:        int((ret[3]).(int64)),
		RefundBeginTime: (ret[4]).(string),
		RefundEndTime:   (ret[5]).(string),
		RefundRatio:     int((ret[6]).(int64)),
		RefundMaxAmount: int((ret[7]).(int64)),
		RefundFlag:      int((ret[8]).(int64)),
		MinBeginTime:    (ret[9]).(string),
		MinEndTime:      (ret[10]).(string),
		MinAmount:       int((ret[11]).(int64)),
		MinFlag:         int((ret[12]).(int64)),
	}
}

// 保险柜记录
func chipBankLog(userId int, beginTime, endTime string, pageIndex, pageSize int) (int, []*pb.BankLogInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	recordCount := 0
	var list []*pb.BankLogInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Chip_GetLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 32, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 32, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	rowsLen := len(retRows)
	if rowsLen <= 0 {
		return recordCount, list
	}

	// 这里带输出和结果集，最后一行是输出参数
	if rowsLen > 1 {
		//这里是结果集
		for i := 0; i < rowsLen-1; i++ {
			ret := retRows[i]
			var info pb.BankLogInfo

			info.From = (ret[0]).(string)
			info.To = (ret[1]).(string)
			info.WantedAmount = int((ret[2]).(int64))
			info.StillAmount = int((ret[3]).(int64))
			info.Crdate = (ret[4]).(string)

			list = append(list, &info)
		}
	}

	//这里是输出参数
	recordCount = int((retRows[rowsLen-1][0]).(int64))
	return recordCount, list
}

// 获取银行信息
func getBankInfo(userId int) *pb.BankInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_GetBankInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return &pb.BankInfo{}
	}

	ret := retRows[0]
	return &pb.BankInfo{
		RealName: (ret[0]).(string),
		BankName: (ret[1]).(string),
		BankCode: (ret[2]).(string),
		BankCard: (ret[3]).(string),
		Mobile:   (ret[4]).(string),
	}
}

// 保存银行信息
func saveBankInfo(userId int, info *pb.BankInfo) bool {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_SaveBankInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@RealName", database.AdParamInput, database.AdVarChar, 32, info.RealName)
	statement.AddParamter("@BankName", database.AdParamInput, database.AdVarChar, 32, info.BankName)
	statement.AddParamter("@BankCode", database.AdParamInput, database.AdVarChar, 16, info.BankCode)
	statement.AddParamter("@BankCard", database.AdParamInput, database.AdVarChar, 32, info.BankCard)
	statement.AddParamter("@Mobile", database.AdParamInput, database.AdVarChar, 32, info.Mobile)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return false
	}

	retCode = int((retRows[0][0]).(int64))
	return retCode == 1
}
