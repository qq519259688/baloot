package handler

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/money/proto"
)

// 金币日志
func cashLog(userId int, beginTime, endTime string, pageIndex, pageSize int) (int, []*pb.CashInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	recordCount := 0
	var list []*pb.CashInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Cash_GetLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 32, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 32, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return recordCount, list
	}

	if rowLen > 1 {

		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info pb.CashInfo

			info.Desc = (ret[0]).(string)
			info.Amount = int((ret[1]).(int64))
			info.Balance = int((ret[2]).(int64))
			info.Date = (ret[3]).(string)
			list = append(list, &info)
		}
	}

	recordCount = int((retRows[rowLen-1][0]).(int64))
	return recordCount, list
}

// 获取金币
func getMoney(userId int) (bool, int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("inventory.getMoney transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	gold, bank := 0, 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Money_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Gold", database.AdParamOutput, database.AdBigint, 8, gold)
	statement.AddParamter("@Bank", database.AdParamOutput, database.AdBigint, 8, bank)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return false, gold, bank
	}

	gold = int((retRows[0][0]).(int64))
	bank = int((retRows[0][1]).(int64))
	return true, gold, bank
}

// 加金币
func giveMoney(userId, amount, logType int, sourceName, remark, ipAddress string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("inventory.giveMoney transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Money_Give")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@LogType", database.AdParamInput, database.AdInteger, 4, logType)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 32, sourceName)
	statement.AddParamter("@Remark", database.AdParamInput, database.AdNVarChar, 128, remark)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return retCode
	}

	retCode = int((retRows[0][0]).(int64))
	return retCode
}

// 加金币
func modifyMoneyWithTax(userId, amount, tax, logType int, sourceName, remark, ipAddress string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("cash.modifyMoneyWithTax transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Money_WriteMoney")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 64, sourceName)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdVarChar, 64, 0)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@Tax", database.AdParamInput, database.AdBigint, 8, tax)
	statement.AddParamter("@Status", database.AdParamInput, database.AdTinyInt, 1, 0)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@Type", database.AdParamInput, database.AdInteger, 4, logType)
	var balance int
	statement.AddParamter("@Balance", database.AdParamOutput, database.AdBigint, 8, balance)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdTinyInt, 1, retCode)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return retCode
	}
	retCode = int((retRows[0][0]).(int64))
	return retCode
}

// 减金币
func reduceMoney(userId, amount, logType int, sourceName, remark, ipAddress string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("inventory.reduceMoney transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	retCode := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Money_Reduce")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@LogType", database.AdParamInput, database.AdInteger, 4, logType)
	statement.AddParamter("@SourceName", database.AdParamInput, database.AdNVarChar, 64, sourceName)
	statement.AddParamter("@Remark", database.AdParamInput, database.AdNVarChar, 128, remark)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return retCode
	}

	retCode = int((retRows[0][0]).(int64))
	return retCode
}

// 保险柜存入
func bankIn(userId, amount, gameId int, serverName, ipAddress string) (int, int, int, string) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	retCode, cash, bank, outMsg := 0, 0, 0, ""

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("p_bank_in")
	statement.AddParamter("@userid", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@servername", database.AdParamInput, database.AdNVarChar, 64, serverName)
	statement.AddParamter("@cashamount", database.AdParamOutput, database.AdBigint, 8, cash)
	statement.AddParamter("@bankamount", database.AdParamOutput, database.AdBigint, 8, bank)
	statement.AddParamter("@outmsg", database.AdParamOutput, database.AdNVarChar, 512, outMsg)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	statement.AddParamter("@EPassword", database.AdParamInput, database.AdVarChar, 32, "")
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return retCode, cash, bank, outMsg
	}

	ret := retRows[0]
	cash = int((ret[0]).(int64))
	bank = int((ret[1]).(int64))
	outMsg = (ret[2]).(string)
	retCode = int((ret[3]).(int64))
	return retCode, cash, bank, outMsg
}

// 保险柜取出
func bankOut(userId, amount, gameId int, serverName, ipAddress string) (int, int, int, string) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	retCode, cash, bank, outMsg := 0, 0, 0, ""
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("p_bank_out")
	statement.AddParamter("@userid", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@amount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@safeboxpwd", database.AdParamInput, database.AdVarChar, 16, "")
	statement.AddParamter("@servername", database.AdParamInput, database.AdNVarChar, 64, serverName)
	statement.AddParamter("@cashamount", database.AdParamOutput, database.AdBigint, 8, amount)
	statement.AddParamter("@bankamount", database.AdParamOutput, database.AdBigint, 8, bank)
	statement.AddParamter("@outmsg", database.AdParamOutput, database.AdVarChar, 512, outMsg)
	statement.AddParamter("@EPassword", database.AdParamInput, database.AdVarChar, 32, "")
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	statement.AddParamter("@GameID", database.AdParamInput, database.AdInteger, 4, gameId)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return retCode, cash, bank, outMsg
	}

	ret := retRows[0]
	cash = int((ret[0]).(int64))
	bank = int((ret[1]).(int64))
	outMsg = (ret[2]).(string)
	retCode = int((ret[3]).(int64))
	return retCode, cash, bank, outMsg
}

// 保险柜查询
func bankQuery(userId int) (int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	bank := 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("p_bank_query")
	statement.AddParamter("@userid", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@bankamount", database.AdParamOutput, database.AdBigint, 8, bank)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return 0, bank
	}

	bank = int((retRows[0][0]).(int64))
	return 1, bank
}

// 保险柜转账
func bankTransfer(userId, toUserId, amount int, ipAddress string) (int, int, string) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	retCode, stillAmount, outMsg := 0, 0, ""
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_SafeBox_Transfer")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@AcceptUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	statement.AddParamter("@WantAmount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@Esafeboxpwd", database.AdParamInput, database.AdVarChar, 32, "")
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@ErrMsg", database.AdParamOutput, database.AdNVarChar, 128, outMsg)
	statement.AddParamter("@StillAmount", database.AdParamOutput, database.AdBigint, 8, stillAmount)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, retCode)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	if len(retRows) <= 0 {
		return retCode, stillAmount, outMsg
	}

	ret := retRows[0]
	outMsg = (ret[0]).(string)
	stillAmount = int((ret[1]).(int64))
	retCode = int((ret[2]).(int64))

	return retCode, stillAmount, outMsg
}

// 保险柜记录
func bankLog(userId int, beginTime, endTime string, pageIndex, pageSize int) (int, []*pb.BankLogInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	recordCount := 0
	var list []*pb.BankLogInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_bank_GetLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@BeginTime", database.AdParamInput, database.AdVarChar, 32, beginTime)
	statement.AddParamter("@EndTime", database.AdParamInput, database.AdVarChar, 32, endTime)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	rowsLen := len(retRows)
	if rowsLen <= 0 {
		return recordCount, list
	}

	// 这里带输出和结果集，最后一行是输出参数
	if rowsLen > 1 {
		//这里是结果集
		for i := 0; i < rowsLen-1; i++ {
			ret := retRows[i]
			var info pb.BankLogInfo

			info.From = (ret[0]).(string)
			info.To = (ret[1]).(string)
			info.WantedAmount = int((ret[2]).(int64))
			info.StillAmount = int((ret[3]).(int64))
			info.Crdate = (ret[4]).(string)

			list = append(list, &info)
		}
	}

	//这里是输出参数
	recordCount = int((retRows[rowsLen-1][0]).(int64))
	return recordCount, list
}

// 充值、提现记录
func financeLog(userId, pageIndex, pageSize int) (int, []*pb.FinanceInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	recordCount := 0
	var list []*pb.FinanceInfo

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_GetFinanceList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlstring := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return recordCount, list
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var out pb.FinanceInfo

			out.Crdate = (ret[0]).(string)
			out.Type = int((ret[1]).(int64))
			out.Amount = int((ret[2]).(int64))
			out.State = (ret[3]).(string)

			list = append(list, &out)
		}
	}
	recordCount = int((retRows[rowLen-1][0]).(int64))

	return recordCount, list
}

// 转账(操作结果/操作描述/剩余金额/返还金额)
func cashTransfer(userId, toUserId, amount int, ipAddress string) (int, string, int, int) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover %v", err)
			log.Release("%s", debug.Stack())
		}
	}()

	retCode, outMsg, stillAmount, refund := 0, "", 0, 0
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Cash_Transfer")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@AcceptUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	statement.AddParamter("@WantAmount", database.AdParamInput, database.AdBigint, 8, amount)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	sqlString := statement.GenSql()
	retRows := dbengine.ExecuteRs(sqlString)
	if len(retRows) <= 0 {
		return retCode, outMsg, stillAmount, refund
	}

	ret := retRows[0]
	retCode = int((ret[0]).(int64))
	outMsg = (ret[1]).(string)
	stillAmount = int((ret[2]).(int64))
	refund = int((ret[3]).(int64))
	return retCode, outMsg, stillAmount, refund
}

// 赠送记录
func cashTransferLog(userId, days int) []*pb.TransferInfo {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Game_Cash_TransferLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Days", database.AdParamInput, database.AdInteger, 4, days)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	var list []*pb.TransferInfo
	if err := json.Unmarshal([]byte(jsonData), &list); err != nil {
		log.Error("cash.transferLog json unmarshal err %v", err)
	}
	return list
}

// 添加赠送记录
func addCashTransferLog(userId int, nickName string, toUserId int, toNickName string, wantAmount, taxAmount int, ipAddress string) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_Cash_AddTransferLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, nickName)
	statement.AddParamter("@AcceptUserID", database.AdParamInput, database.AdInteger, 4, toUserId)
	statement.AddParamter("@AcceptNickName", database.AdParamInput, database.AdNVarChar, 32, toNickName)
	statement.AddParamter("@WantAmount", database.AdParamInput, database.AdBigint, 8, wantAmount)
	statement.AddParamter("@TaxAmount", database.AdParamInput, database.AdBigint, 8, taxAmount)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}
