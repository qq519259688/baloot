package handler

import (
	"encoding/json"
	"sync"
	"time"

	"bet24.com/log"
	"bet24.com/redis"
	pb "bet24.com/servers/micros/money/proto"
	notification "bet24.com/servers/micros/notification/proto"
)

type chipmgr struct {
	lock *sync.RWMutex
	cfg  *pb.TransferCfg
}

func newChipMgr() *chipmgr {
	mgr := new(chipmgr)
	mgr.lock = &sync.RWMutex{}
	mgr.refresh()
	log.Debug("chip manager running")
	return mgr
}

func (this *chipmgr) refresh() {
	go this.loadCfg()
	time.AfterFunc(time.Minute*1, this.refresh)
}

func (this *chipmgr) loadCfg() {
	info := getTransferCfg()
	this.lock.Lock()
	defer this.lock.Unlock()
	this.cfg = info
}

// 筹码日志
func (this *chipmgr) chipLog(userId int, beginTime, endTime string, pageIndex, pageSize int) (int, []*pb.CashInfo) {
	return chipLog(userId, beginTime, endTime, pageIndex, pageSize)
}

// 获取筹码
func (this *chipmgr) getChip(userId int) (bool, int, int) {
	return getChip(userId)
}

// 加筹码并通知客户端
func (this *chipmgr) giveChip(userId, amount, logType int, sourceName, remark, ipAddress string) int {
	retCode := giveChip(userId, amount, logType, sourceName, remark, ipAddress)
	if retCode != 1 {
		log.Debug("chip.giveChip fail userId=%d amount=%d logType=%d sourceName=%s remark=%s",
			userId, amount, logType, sourceName, remark)
		return retCode
	}

	// 通知客户端
	notification.AddNotification(userId, notification.Notification_Chip, "")
	go this.notifyChanged(userId)
	return retCode
}

// 减筹码并通知客户端
func (this *chipmgr) reduceChip(userId, amount, logType int, sourceName, remark, ipAddress string) int {
	retCode := reduceChip(userId, amount, logType, sourceName, remark, ipAddress)
	if retCode != 1 {
		log.Debug("chip.reduceChip fail userId=%d amount=%d logType=%d sourceName=%s remark=%s",
			userId, amount, logType, sourceName, remark)
		return retCode
	}

	// 通知客户端
	go notification.AddNotification(userId, notification.Notification_Chip, "")
	go this.notifyChanged(userId)
	return retCode
}

func (this *chipmgr) notifyChanged(userId int) {
	var d redis.Channel_msg
	d.Message = "RefreshChip"
	d.UserID = userId
	js, _ := json.Marshal(d)
	redis.Publish(string(js))
}

// 保险柜存入并通知客户端
func (this *chipmgr) bankIn(userId, amount, gameId int, serverName, ipAddress string) (int, int, int, string) {
	retCode, cash, bank, outMsg := chipBankIn(userId, amount, gameId, serverName, ipAddress)
	if retCode != 1 {
		log.Debug("chip.BankIn fail userId=%d amount=%d gameId=%d serverName=%s ipAddress=%s",
			userId, amount, gameId, serverName, ipAddress)
		return retCode, cash, bank, outMsg
	}

	// 通知客户端
	go notification.AddNotification(userId, notification.Notification_Chip, "")
	go this.notifyChanged(userId)
	return retCode, cash, bank, outMsg
}

// 保险柜取出并通知客户端
func (this *chipmgr) bankOut(userId, amount, gameId int, serverName, ipAddress string) (int, int, int, string) {
	retCode, cash, bank, outMsg := chipBankOut(userId, amount, gameId, serverName, ipAddress)
	if retCode != 1 {
		log.Debug("chip.BankOut fail userId=%d amount=%d serverName=%s ipAddress=%s",
			userId, amount, serverName, ipAddress)
		return retCode, cash, bank, outMsg
	}

	// 通知客户端
	go notification.AddNotification(userId, notification.Notification_Chip, "")
	go this.notifyChanged(userId)
	return retCode, cash, bank, outMsg
}

// 保险柜查询
func (this *chipmgr) bankQuery(userId int) (int, int) {
	return chipBankQuery(userId)
}

// 转账(操作结果/操作描述/剩余金额/返还金额)
func (this *chipmgr) bankTransfer(userId, toUserId, amount int, ipAddress string) (int, string, int, int) {
	retCode, stillAmount, outMsg, refund := chipBankTransfer(userId, toUserId, amount, ipAddress)
	if retCode != 1 {
		log.Debug("chip.BankTransfer fail userId=%d toUserId=%d amount=%d ipAddress=%s",
			userId, toUserId, amount, ipAddress)
		return retCode, outMsg, stillAmount, refund
	}
	go this.notifyChanged(userId)
	go notification.AddNotification(toUserId, notification.Notification_Chip, "")
	go notification.AddNotification(userId, notification.Notification_Chip, "")
	return retCode, outMsg, stillAmount, refund
}

func (this *chipmgr) getTransferCfg() *pb.TransferCfg {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.cfg
}

// 赠送记录
func (this *chipmgr) transferLog(userId int) []*pb.TransferInfo {
	return chipTransferLog(userId)
}

// 保险柜记录
func (this *chipmgr) bankLog(userId int, beginTime, endTime string, pageIndex, pageSize int) (int, []*pb.BankLogInfo) {
	return chipBankLog(userId, beginTime, endTime, pageIndex, pageSize)
}

// 获取银行信息
func (this *chipmgr) getBankInfo(userId int) *pb.BankInfo {
	return getBankInfo(userId)
}

// 保存银行信息
func (this *chipmgr) saveBankInfo(userId int, realName, bankName, bankCode, bankCard, mobile string) bool {
	info := &pb.BankInfo{
		RealName: realName,
		BankName: bankName,
		BankCode: bankCode,
		BankCard: bankCard,
		Mobile:   mobile,
	}
	return saveBankInfo(userId, info)
}
