package handler

import (
	"context"

	pb "bet24.com/servers/micros/money/proto"
)

var chip *chipmgr

func RunChip() {
	chip = newChipMgr()
}

// 发放筹码
func (this *Money) GiveChip(ctx context.Context, req *pb.Request_GiveReduceMoney, rsp *pb.Response_GiveReduceMoney) error {
	rsp.Gold = chip.giveChip(req.UserId, req.Amount, req.LogType, req.SourceName, req.Remark, req.IpAddress)
	return nil
}

// 扣减筹码
func (this *Money) ReduceChip(ctx context.Context, req *pb.Request_GiveReduceMoney, rsp *pb.Response_GiveReduceMoney) error {
	rsp.RetCode = chip.reduceChip(req.UserId, req.Amount, req.LogType, req.SourceName, req.Remark, req.IpAddress)
	return nil
}

// 获取筹码
func (this *Money) GetUserChip(ctx context.Context, req *pb.Request_base, rsp *pb.Response_GetMoney) error {
	_, rsp.Gold, _ = chip.getChip(req.UserId)
	return nil
}

// 筹码日志
func (this *Money) UserChipLog(ctx context.Context, req *pb.Request_CashLog, rsp *pb.Response_CashLog) error {
	rsp.RecordCount, rsp.List = chip.chipLog(req.UserId, req.BeginTime, req.EndTime, req.PageIndex, req.PageSize)
	return nil
}

// 转账(操作结果/操作描述/剩余金额/返还金额)
func (this *Money) ChipBankTransfer(ctx context.Context, req *pb.Request_Transfer, rsp *pb.Response_Transfer) error {
	// 转账
	rsp.RetCode, rsp.ErrMsg, rsp.StillAmount, rsp.Refund = chip.bankTransfer(req.UserId, req.ToUserId, req.Amount, req.IpAddress)

	return nil
}

func (this *Money) GetTransferCfg(ctx context.Context, req *pb.Request, rsp *pb.Response_GetTransferCfg) error {
	rsp.Info = chip.getTransferCfg()
	return nil
}

// 转账日志
func (this *Money) ChipTransferLog(ctx context.Context, req *pb.Request_base, rsp *pb.Response_GoldTransferLog) error {
	rsp.List = chip.transferLog(req.UserId)
	return nil
}

// 获取银行信息
func (this *Money) GetBankInfo(ctx context.Context, req *pb.Request_base, rsp *pb.Response_GetBankInfo) error {
	rsp.Info = chip.getBankInfo(req.UserId)
	return nil
}

// 保险柜存入并通知客户端
func (this *Money) SaveBankInfo(ctx context.Context, req *pb.Request_SaveBankInfo, rsp *pb.Response_SaveBankInfo) error {
	rsp.Success = chip.saveBankInfo(req.UserId, req.RealName, req.BankName, req.BankCode, req.BankCard, req.Mobile)
	return nil
}
