package handler

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/money/proto"

	"context"
	"fmt"
)

var instance *Money

type Money struct {
}

func newHandler() *Money {
	ret := new(Money)
	ret.ctor()
	return ret
}

func (g *Money) ctor() {
	RunCash()
	RunChip()
}

func GetInstance() *Money {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func (this *Money) dump(cmd, param1, param2 string) {
	switch cmd {
	case "transfer":
		getTransferManager().dump(param1, param2)
	default:
		log.Release("Money.Dump unhandled cmd %s", cmd)
	}
}

func (this *Money) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}
