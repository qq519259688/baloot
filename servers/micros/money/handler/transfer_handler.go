package handler

import (
	pb "bet24.com/servers/micros/money/proto"
	"context"
)

// 转账(操作结果/操作描述/剩余金额/返还金额)
func (this *Money) GoldTransfer(ctx context.Context, req *pb.Request_Transfer, rsp *pb.Response_Transfer) error {
	// 转账
	rsp.Success, rsp.ErrMsg = getTransferManager().transferGold(req.UserId, req.ToUserId, req.Amount, req.IpAddress)
	return nil
}

// 转账日志
func (this *Money) GetGoldTransferLog(ctx context.Context, req *pb.Request_GoldTransferLog, rsp *pb.Response_GoldTransferLog) error {
	rsp.Data = getTransferManager().getGoldTransferLog(req.UserId)
	return nil
}

// 转账配置
func (this *Money) GetGoldTransferConfig(ctx context.Context, req *pb.Request_GoldTransferLog, rsp *pb.Response_GoldTransferLog) error {
	rsp.Data = getTransferManager().getGoldTransferConfig(req.UserId)
	return nil
}
