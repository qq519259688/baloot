package handler

import (
	"encoding/json"

	"bet24.com/log"
	"bet24.com/redis"
	pb "bet24.com/servers/micros/money/proto"
	notification "bet24.com/servers/micros/notification/proto"
)

type cashmgr struct {
}

func newCashMgr() *cashmgr {
	log.Debug("cash manager running")
	return &cashmgr{}
}

// 金币日志
func (bm *cashmgr) cashLog(userId int, beginTime, endTime string, pageIndex, pageSize int) (int, []*pb.CashInfo) {
	return cashLog(userId, beginTime, endTime, pageIndex, pageSize)
}

// 获取金币
func (bm *cashmgr) getMoney(userId int) (bool, int, int) {
	return getMoney(userId)
}

// 加金币并通知客户端
func (bm *cashmgr) giveMoney(userId, amount, logType int, sourceName, remark, ipAddress string) int {
	retCode := giveMoney(userId, amount, logType, sourceName, remark, ipAddress)
	if retCode != 1 {
		log.Debug("cash.giveMoney fail userId=%d amount=%d logType=%d sourceName=%s remark=%s",
			userId, amount, logType, sourceName, remark)
		return retCode
	}

	//通知客户端
	notification.AddNotification(userId, notification.Notification_Gold, "")
	go bm.notifyGoldChanged(userId)
	return retCode
}

// 加金币并通知客户端
func (bm *cashmgr) modifyMoneyWithTax(userId, amount, tax, logType int, sourceName, remark, ipAddress string) int {
	retCode := modifyMoneyWithTax(userId, amount, tax, logType, sourceName, remark, ipAddress)
	if retCode != 1 {
		log.Debug("cash.modifyMoneyWithTax fail userId=%d amount=%d logType=%d sourceName=%s remark=%s",
			userId, amount, logType, sourceName, remark)
		return retCode
	}

	//通知客户端
	notification.AddNotification(userId, notification.Notification_Gold, "")
	go bm.notifyGoldChanged(userId)
	return retCode
}

// 减金币并通知客户端
func (bm *cashmgr) reduceMoney(userId, amount, logType int, sourceName, remark, ipAddress string) int {
	retCode := reduceMoney(userId, amount, logType, sourceName, remark, ipAddress)
	if retCode != 1 {
		log.Debug("cash.reduceMoney fail userId=%d amount=%d logType=%d sourceName=%s remark=%s",
			userId, amount, logType, sourceName, remark)
		return retCode
	}
	//通知客户端
	go notification.AddNotification(userId, notification.Notification_Gold, "")
	go bm.notifyGoldChanged(userId)
	return retCode
}

func (bm *cashmgr) notifyGoldChanged(userId int) {
	var d redis.Channel_msg
	d.Message = "RefreshGold"
	d.UserID = userId
	js, _ := json.Marshal(d)
	redis.Publish(string(js))
}

// 保险柜存入并通知客户端
func (bm *cashmgr) BankIn(userId, amount, gameId int, serverName, ipAddress string) (int, int, int, string) {
	retCode, cash, bank, outMsg := bankIn(userId, amount, gameId, serverName, ipAddress)
	if retCode != 1 {
		log.Debug("cash.BankIn fail userId=%d amount=%d gameId=%d serverName=%s ipAddress=%s",
			userId, amount, gameId, serverName, ipAddress)
		return retCode, cash, bank, outMsg
	}

	//通知客户端
	go notification.AddNotification(userId, notification.Notification_Gold, "")
	go bm.notifyGoldChanged(userId)
	return retCode, cash, bank, outMsg
}

// 保险柜取出并通知客户端
func (bm *cashmgr) BankOut(userId, amount, gameId int, serverName, ipAddress string) (int, int, int, string) {
	retCode, cash, bank, outMsg := bankOut(userId, amount, gameId, serverName, ipAddress)
	if retCode != 1 {
		log.Debug("cash.BankOut fail userId=%d amount=%d serverName=%s ipAddress=%s",
			userId, amount, serverName, ipAddress)
		return retCode, cash, bank, outMsg
	}

	//通知客户端
	go notification.AddNotification(userId, notification.Notification_Gold, "")
	go bm.notifyGoldChanged(userId)
	return retCode, cash, bank, outMsg
}

// 保险柜查询
func (bm *cashmgr) BankQuery(userId int) (int, int) {
	return bankQuery(userId)
}

// 保险柜转账
func (bm *cashmgr) BankTransfer(userId, toUserId, amount int, ipAddress string) (int, int, string) {
	retCode, stillAmount, outMsg := bankTransfer(userId, toUserId, amount, ipAddress)
	if retCode != 1 {
		log.Debug("cash.BankTransfer fail userId=%d toUserId=%d amount=%d ipAddress=%s",
			userId, toUserId, amount, ipAddress)
		return retCode, stillAmount, outMsg
	}
	go bm.notifyGoldChanged(userId)
	return retCode, stillAmount, outMsg
}

// 转账(操作结果/操作描述/剩余金额/返还金额)
func (bm *cashmgr) Transfer(userId, toUserId, amount int, ipAddress string) (int, string, int, int) {
	retCode, outMsg, stillAmount, refund := cashTransfer(userId, toUserId, amount, ipAddress)
	if retCode != 1 {
		log.Debug("cash.Transfer fail userId=%d toUserId=%d amount=%d ipAddress=%s",
			userId, toUserId, amount, ipAddress)
		return retCode, outMsg, stillAmount, refund
	}
	go bm.notifyGoldChanged(userId)
	go notification.AddNotification(toUserId, notification.Notification_Gold, "")
	go notification.AddNotification(userId, notification.Notification_Gold, "")
	return retCode, outMsg, stillAmount, refund
}

// 赠送记录
func (this *cashmgr) getTransferLog(userId, days int) []*pb.TransferInfo {
	return cashTransferLog(userId, days)
}

// 添加赠送记录
func (this *cashmgr) addTransferLog(userId int, nickName string, toUserId int, toNickName string, wantAmount, taxAmount int, ipAddress string) {
	addCashTransferLog(userId, nickName, toUserId, toNickName, wantAmount, taxAmount, ipAddress)
}

// 保险柜记录
func (bm *cashmgr) BankLog(userId int, beginTime, endTime string, pageIndex, pageSize int) (int, []*pb.BankLogInfo) {
	return bankLog(userId, beginTime, endTime, pageIndex, pageSize)
}

// 充值、提现记录
func (bm *cashmgr) FinanceLog(userId int, pageIndex, pageSize int) (int, []*pb.FinanceInfo) {
	return financeLog(userId, pageIndex, pageSize)
}
