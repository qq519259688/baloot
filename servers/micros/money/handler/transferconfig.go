package handler

type GoldTransferConfig struct {
	DailyTransferCount int    // 每天限制次数 -1不限制,0不能转
	TaxRate            int    // 税收百分比，负数表示固定值
	ReservedAmount     int    // 转账后保留额度
	MinAmount          int    // 最小转账额度
	MailTitle          string // 邮件赠送标题
	MailSource         string // 邮件源
	MailContent        string // 邮件赠送内容
}

type UserGoldTransferConfig struct {
	TotalCount int // 每天总次数，包含VIP,-1表示无限制
	UsedCount  int // 已使用次数
	MinAmount  int // 最小转账金额
	MaxAmount  int // 最大转账金额
	TaxRate    int // 税收百分比，负数表示固定值
}
