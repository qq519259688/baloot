package handler

import (
	"bet24.com/log"
	"bet24.com/redis"
	"encoding/json"
	"sync"
	"time"
)

const redis_key = "money_transfercount"

var transferCountMgr *transferCountManager

type transferCountManager struct {
	lockUser          *sync.RWMutex
	UserTransferCount map[int]int
	LastDay           int
	isDirty           bool
}

func getTransferCountMgr() *transferCountManager {
	if transferCountMgr == nil {
		transferCountMgr = new(transferCountManager)
		transferCountMgr.ctor()
	}

	return transferCountMgr
}

func (tcm *transferCountManager) ctor() {
	tcm.lockUser = &sync.RWMutex{}
	tcm.UserTransferCount = make(map[int]int)
	tcm.LastDay = time.Now().Day()
	tcm.loadTransferCountInfo()

	time.AfterFunc(10*time.Minute, tcm.saveTransferCountInfo)
}

func (tcm *transferCountManager) checkDayChanged() {
	nowDay := time.Now().Day()
	if nowDay == tcm.LastDay {
		return
	}

	tcm.isDirty = true

	tcm.LastDay = nowDay
	tcm.lockUser.Lock()
	tcm.UserTransferCount = make(map[int]int)
	tcm.lockUser.Unlock()
}

func (tcm *transferCountManager) getTransferCount(userId int) int {
	tcm.checkDayChanged()
	tcm.lockUser.RLock()
	defer tcm.lockUser.RUnlock()
	count, ok := tcm.UserTransferCount[userId]
	if !ok {
		return 0
	}
	return count
}

func (tcm *transferCountManager) addTransferCount(userId int) {
	tcm.lockUser.Lock()
	tcm.UserTransferCount[userId] += 1
	tcm.lockUser.Unlock()
	tcm.isDirty = true
}

func (tcm *transferCountManager) loadTransferCountInfo() {
	data, ok := redis.String_Get(redis_key)
	if data == "" || !ok {
		return
	}

	err := json.Unmarshal([]byte(data), &tcm)
	if err != nil {
		log.Release("transferCountManager.loadTransferCountInfo Unmarshal faied %s", data)
		return
	}

	tcm.checkDayChanged()
}

func (tcm *transferCountManager) saveTransferCountInfo() {
	time.AfterFunc(10*time.Minute, tcm.saveTransferCountInfo)
	if !tcm.isDirty {
		return
	}

	d, _ := json.Marshal(tcm)
	redis.String_SetEx(redis_key, string(d), 86400)
}

func (tcm *transferCountManager) dump(param string) {
	log.Release("-------------------------------")
	log.Release("transferCountManager.dump %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	d, _ := json.Marshal(tcm)
	log.Release(string(d))
}
