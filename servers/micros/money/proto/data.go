package proto

type CashInfo struct {
	Desc    string // 备注
	Amount  int    // 操作金额
	Balance int    // 剩余金额
	Date    string // 时间
}

type BankLogInfo struct {
	From         string
	To           string
	WantedAmount int
	StillAmount  int
	Crdate       string
}

type FinanceInfo struct {
	Crdate string // 时间
	Type   int    // 类型 1=充值  2=提现
	Amount int    // 金额
	State  string // 状态 00-成功 01-处理中 02-处理失败
}

type TransferInfo struct {
	UserID         int    // 用户ID
	NickName       string // 昵称
	AcceptUserID   int    // 接收用户id
	AcceptNickName string // 接收用户昵称
	Amount         int    // 金额
	TaxAmount      int    // 费用
	Crdate         string // 时间
}

type BankInfo struct {
	RealName string // 真实姓名
	BankName string // 银行名称
	BankCode string // 银行码
	BankCard string // 银行卡号
	Mobile   string // 手机号
}

type TransferCfg struct {
	Fee             int    // 手续费率(百分制)
	FreeBeginTime   string // 免费开启时间
	FreeEndTime     string // 免费截止时间
	FreeFlag        int    // 免费状态(1=开启  其他关闭)
	RefundBeginTime string // 返额开始时间
	RefundEndTime   string // 返额截止时间
	RefundRatio     int    // 返额比率(百分制)
	RefundMaxAmount int    // 单笔返额上限(首笔)
	RefundFlag      int    // 返额状态(1=开启  其他关闭)
	MinBeginTime    string // 转账最低开始时间
	MinEndTime      string // 转账最低截止时间
	MinAmount       int    // 转账最低金额
	MinFlag         int    // 转账最低状态(1=开启  其他关闭)
}
