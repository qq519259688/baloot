package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

func GoldTransfer(userId, toUserId, amount int, ipAddress string) (bool, string) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_Transfer{
		UserId:    userId,
		ToUserId:  toUserId,
		Amount:    amount,
		IpAddress: ipAddress,
	}

	reply := &Response_Transfer{}

	err := xclient.Call(context.Background(), "GoldTransfer", args, reply)
	if err != nil {
		log.Debug("GoldTransfer failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false, "server error"
	}
	return reply.Success, reply.ErrMsg
}

// 赠送日志
func GetGoldTransferLog(userId int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GoldTransferLog{
		UserId: userId,
	}

	reply := &Response_GoldTransferLog{}

	err := xclient.Call(context.Background(), "GetGoldTransferLog", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("GetGoldTransferLog failed to call: %v", err)
		return ""
	}
	return reply.Data
}

// 赠送配置
func GetGoldTransferConfig(userId int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GoldTransferLog{
		UserId: userId,
	}

	reply := &Response_GoldTransferLog{}

	err := xclient.Call(context.Background(), "GetGoldTransferConfig", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("GetGoldTransferConfig failed to call: %v", err)
		return ""
	}
	return reply.Data
}
