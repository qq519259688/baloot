package proto

type Request_base struct {
	UserId    int
	IpAddress string
}

type Request_CashLog struct {
	UserId    int
	ToUserId  int
	BeginTime string
	EndTime   string
	PageIndex int
	PageSize  int
}

type Response_CashLog struct {
	RecordCount int
	List        []*CashInfo
}

type Response_GetMoney struct {
	Success bool
	Gold    int
}

type Request_BankInOrOut struct {
	UserId     int
	Amount     int
	GameID     int
	ServerName string
	IpAddress  string
}

type Response_BankInOrOut struct {
	RetCode    int
	GameID     int `json:"GameID,omitempty"`
	Gold       int
	BankAmount int //保险柜金币
	OutMsg     string
}

type Response_FinanceLog struct {
	RecordCount int
	List        []*FinanceInfo
}

type Request_Transfer struct {
	UserId    int
	ToUserId  int
	Amount    int
	IpAddress string
}

type Response_Transfer struct {
	RetCode     int
	StillAmount int
	Refund      int
	Success     bool
	ErrMsg      string
}

type Request_GoldTransferLog struct {
	UserId int
	Days   int
}

type Response_GoldTransferLog struct {
	List []*TransferInfo
	Data string
}

type Request_GiveReduceMoney struct {
	UserId     int
	Amount     int
	LogType    int
	SourceName string
	Remark     string
	Tax        int
	IpAddress  string
}

type Response_GiveReduceMoney struct {
	Success bool
	Gold    int
	RetCode int
}

type Response_BankLog struct {
	RecordCount int
	List        []*BankLogInfo
}

type Response_GetTransferCfg struct {
	Info *TransferCfg
}

type Response_GetBankInfo struct {
	Info *BankInfo
}

type Request_SaveBankInfo struct {
	UserId   int
	RealName string // 真实姓名
	BankName string // 银行名称
	BankCode string // 银行码
	BankCard string // 银行卡号
	Mobile   string // 手机号
}

type Response_SaveBankInfo struct {
	Success bool
}
