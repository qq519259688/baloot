package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

// 发放筹码
func GiveChip(userId, amount, logType int, sourceName, remark, ipAddress string) int {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GiveReduceMoney{
		UserId:     userId,
		Amount:     amount,
		LogType:    logType,
		SourceName: sourceName,
		Remark:     remark,
		IpAddress:  ipAddress,
	}

	reply := &Response_GiveReduceMoney{}

	err := xclient.Call(context.Background(), "GiveChip", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.Gold
}

// 扣减筹码
func ReduceChip(userId, amount, logType int, sourceName, remark, ipAddress string) int {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GiveReduceMoney{
		UserId:     userId,
		Amount:     amount,
		LogType:    logType,
		SourceName: sourceName,
		Remark:     remark,
		IpAddress:  ipAddress,
	}

	reply := &Response_GiveReduceMoney{}

	err := xclient.Call(context.Background(), "ReduceChip", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return reply.RetCode
	}
	return reply.RetCode
}

func GetUserChip(userId int) (bool, int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_base{
		UserId: userId,
	}

	reply := &Response_GetMoney{}

	err := xclient.Call(context.Background(), "GetUserChip", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return reply.Success, reply.Gold
	}
	return reply.Success, reply.Gold
}

// 金币日志
func UserChipLog(userId int, beginTime, endTime string, pageIndex, pageSize int) (int, []*CashInfo) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_CashLog{
		UserId:    userId,
		BeginTime: beginTime,
		EndTime:   endTime,
		PageIndex: pageIndex,
		PageSize:  pageSize,
	}

	reply := &Response_CashLog{}

	err := xclient.Call(context.Background(), "UserChipLog", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return reply.RecordCount, reply.List
	}
	return reply.RecordCount, reply.List
}

func ChipBankTransfer(userId, toUserId, amount int, ipAddress string) *Response_Transfer {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_Transfer{
		UserId:    userId,
		ToUserId:  toUserId,
		Amount:    amount,
		IpAddress: ipAddress,
	}

	reply := &Response_Transfer{}

	err := xclient.Call(context.Background(), "ChipBankTransfer", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply
}

func GetTransferCfg() *TransferCfg {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{}

	reply := &Response_GetTransferCfg{}

	err := xclient.Call(context.Background(), "GetTransferCfg", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.Info
}

// 赠送日志
func ChipTransferLog(userId int) []*TransferInfo {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_base{
		UserId: userId,
	}

	reply := &Response_GoldTransferLog{}

	err := xclient.Call(context.Background(), "ChipTransferLog", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.List
}

// 获取银行信息
func GetBankInfo(userId int) *BankInfo {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_base{
		UserId: userId,
	}

	reply := &Response_GetBankInfo{}

	err := xclient.Call(context.Background(), "GetBankInfo", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply.Info
}

// 保存银行信息
func SaveBankInfo(userId int, realName, bankName, bankCode, bankCard, mobile string) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_SaveBankInfo{
		UserId:   userId,
		RealName: realName,
		BankName: bankName,
		BankCode: bankCode,
		BankCard: bankCard,
		Mobile:   mobile,
	}

	reply := &Response_SaveBankInfo{}

	err := xclient.Call(context.Background(), "SaveBankInfo", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.Success
}
