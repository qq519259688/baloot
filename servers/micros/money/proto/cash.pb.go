package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

// 金币日志
func CashLog(userId int, beginTime, endTime string, pageIndex, pageSize int) (int, []*CashInfo) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_CashLog{
		UserId:    userId,
		BeginTime: beginTime,
		EndTime:   endTime,
		PageIndex: pageIndex,
		PageSize:  pageSize,
	}

	reply := &Response_CashLog{}

	err := xclient.Call(context.Background(), "CashLog", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, nil
	}
	return reply.RecordCount, reply.List
}

func GetMoney(userId int) (bool, int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_base{
		UserId: userId,
	}

	reply := &Response_GetMoney{}

	err := xclient.Call(context.Background(), "GetMoney", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false, 0
	}
	return reply.Success, reply.Gold
}

// 获取保险柜金额
func BankQuery(userId int) (int, int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_BankInOrOut{
		UserId: userId,
	}

	reply := &Response_BankInOrOut{}

	err := xclient.Call(context.Background(), "BankQuery", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return reply.RetCode, reply.BankAmount
	}
	return reply.RetCode, reply.BankAmount
}

func BankIn(userId, amount, gameId int, serverName, ipAddress string) (int, int, int, string) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_BankInOrOut{
		UserId:     userId,
		Amount:     amount,
		GameID:     gameId,
		ServerName: serverName,
		IpAddress:  ipAddress,
	}

	reply := &Response_BankInOrOut{}

	err := xclient.Call(context.Background(), "BankIn", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return reply.RetCode, reply.Gold, reply.BankAmount, reply.OutMsg
	}
	return reply.RetCode, reply.Gold, reply.BankAmount, reply.OutMsg
}

func BankOut(userId, amount, gameId int, serverName, ipAddress string) (int, int, int, string) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_BankInOrOut{
		UserId:     userId,
		Amount:     amount,
		GameID:     gameId,
		ServerName: serverName,
		IpAddress:  ipAddress,
	}

	reply := &Response_BankInOrOut{}

	err := xclient.Call(context.Background(), "BankOut", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return reply.RetCode, reply.Gold, reply.BankAmount, reply.OutMsg
	}
	return reply.RetCode, reply.Gold, reply.BankAmount, reply.OutMsg
}

func FinanceLog(userId, pageIndex, pageSize int) (int, []*FinanceInfo) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_CashLog{
		UserId:    userId,
		PageIndex: pageIndex,
		PageSize:  pageSize,
	}

	reply := &Response_FinanceLog{}

	err := xclient.Call(context.Background(), "FinanceLog", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return reply.RecordCount, reply.List
	}
	return reply.RecordCount, reply.List
}

// 加金币
func GiveMoney(userId, amount, logType int, sourceName, remark, ipAddress string) int {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GiveReduceMoney{
		UserId:     userId,
		Amount:     amount,
		LogType:    logType,
		SourceName: sourceName,
		Remark:     remark,
		IpAddress:  ipAddress,
	}

	reply := &Response_GiveReduceMoney{}

	err := xclient.Call(context.Background(), "GiveMoney", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.Gold
}

// 扣减金币
func ReduceMoney(userId, amount, logType int, sourceName, remark, ipAddress string) bool {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GiveReduceMoney{
		UserId:     userId,
		Amount:     amount,
		LogType:    logType,
		SourceName: sourceName,
		Remark:     remark,
		IpAddress:  ipAddress,
	}

	reply := &Response_GiveReduceMoney{}

	err := xclient.Call(context.Background(), "ReduceMoney", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}
	return reply.Success
}

func ModifyMoneyWithTax(userId, amount, tax, logType int, sourceName, remark, ipAddress string) int {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GiveReduceMoney{
		UserId:     userId,
		Amount:     amount,
		Tax:        tax,
		LogType:    logType,
		SourceName: sourceName,
		Remark:     remark,
		IpAddress:  ipAddress,
	}

	reply := &Response_GiveReduceMoney{}

	err := xclient.Call(context.Background(), "ModifyMoneyWithTax", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		return 0
	}
	return reply.Gold
}

func BankTransfer(userId, toUserId, amount int, ipAddress string) (int, string, int, int) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_Transfer{
		UserId:    userId,
		ToUserId:  toUserId,
		Amount:    amount,
		IpAddress: ipAddress,
	}

	reply := &Response_Transfer{}

	err := xclient.Call(context.Background(), "BankTransfer", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return reply.RetCode, reply.ErrMsg, reply.StillAmount, reply.Refund
	}
	return reply.RetCode, reply.ErrMsg, reply.StillAmount, reply.Refund
}

func BankLog(userId int, beginTime, endTime string, pageIndex, pageSize int) *Response_BankLog {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_CashLog{
		UserId:    userId,
		BeginTime: beginTime,
		EndTime:   endTime,
		PageIndex: pageIndex,
		PageSize:  pageSize,
	}

	reply := &Response_BankLog{}

	err := xclient.Call(context.Background(), "BankLog", args, reply)
	if err != nil {
		log.Debug("money failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}
	return reply
}
