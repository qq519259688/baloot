package handler

import (
	"bet24.com/servers/micros/highly_profitable/handler/highly_profitable"
	pb "bet24.com/servers/micros/highly_profitable/proto"
	"context"
)

func (h *highlyProfitableService) GetHighlyProfitableActivity(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.List = highly_profitable.GetHighlyProfitableActivity(req.UserId)
	return nil
}

func (h *highlyProfitableService) ClaimHighlyProfitableAward(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success, rsp.Items = highly_profitable.ClaimHighlyProfitableAward(req.UserId)
	return nil
}

func (h *highlyProfitableService) ActivationHighlyProfitableNotification(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = highly_profitable.ActivationHighlyProfitableNotification(req.UserId)
	return nil
}

func (h *highlyProfitableService) OnUserFinishedRelativeTask(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	highly_profitable.OnUserFinishedRelativeTask(req.UserId)
	return nil
}
