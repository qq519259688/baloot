package highly_profitable

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	pb "bet24.com/servers/micros/highly_profitable/proto"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	notification "bet24.com/servers/micros/notification/proto"
	task "bet24.com/servers/micros/task/proto"
	"encoding/json"
)

const idle_seconds = 600 // 过期时长(秒)

type userStage struct {
	userId    int
	timeStamp int // 当前时间戳
	pb.UserStageTask
}

func newUserStage(userId int) *userStage {
	ret := new(userStage)
	ret.userId = userId
	ret.loadData()
	ret.updateTimeStamp()
	return ret
}

func (ut *userStage) loadData() {
	stage := transHighlyProfitableGetUserStageInfo(ut.userId)
	if stage.Schedule == 0 {
		go transHighlyProfitableSaveStageRecord(ut.userId, 1, pb.ClaimStatus_NotActive)
		stage.Schedule = 1
	}
	ut.Schedule = stage.Schedule
	for k, v := range mgr.sysConfig {
		res := v
		if stage.Status == pb.ClaimStatus_Active {
			if k+1 == ut.Schedule {
				//ut.createUserTasks(v.Tasks)
				v.Status = stage.Status
			}
		}
		taskRewards := task.GetTasksRewards(res.Tasks)
		paidItems := item.GroupItems(res.PaidRewards)
		successItems := item.GroupItems(res.SuccessRewards)

		// 合并所有奖励项
		allItems := append(taskRewards, append(paidItems, successItems...)...)
		res.TotalRewards = item.GroupItems(allItems)
		ut.Tasks = append(ut.Tasks, &res)
	}
	return
}

// 创建用户任务
func (ut *userStage) createUserTasks(tasks []int) {
	if len(tasks) == 0 {
		return
	}
	if !task.CreateUserTasks(ut.userId, tasks) {
		log.Debug("highly_profitable.userStage.loadData userId[%d] create task fail. taskIds[%v]", ut.userId, tasks)
	}
}

// 更新时间戳
func (ut *userStage) updateTimeStamp() {
	ut.timeStamp = common.GetTimeStamp() + idle_seconds
	return
}

// 是否过期
func (ut *userStage) isExpire() bool {
	return ut.timeStamp < common.GetTimeStamp()
}

// 获取当前阶段任务
func (ut *userStage) getCurrentStageTask() *pb.HighlyProfitable {
	for k := range ut.Tasks {
		if ut.Schedule == k+1 {
			return ut.Tasks[k]
		}
	}
	return nil
}

// 领取一本万利奖励
func (ut *userStage) claimHighlyProfitableAward() (bool, []inventory.ItemPack) {
	info := ut.getCurrentStageTask()
	if info == nil {
		log.Debug("highly_profitable.userStage.activationNotification userId[%d] task not exist!", ut.userId)
		return false, nil
	}
	if info.Status != pb.ClaimStatus_Complete {
		log.Debug("highly_profitable.userStage.activationNotification userId[%d] not complete!", ut.userId)
		return false, nil
	}

	// 把已完成未领取的任务奖励先领取了
	items := task.ClaimTaskRewards(ut.userId, info.Tasks)

	ut.Schedule++
	info.Status = pb.ClaimStatus_End
	go transHighlyProfitableSaveStageRecord(ut.userId, ut.Schedule, info.Status)
	go inventory.AddItems(ut.userId, info.SuccessRewards, "highly profitable award", common.LOGTYPE_HIGHLY_PROFITABLE_AWARD)
	if len(items) > 0 {
		log.Debug("userStage.claimHighlyProfitableAward userId[%d] auto claim task items %v", ut.userId, items)

	}

	items = append(items, info.SuccessRewards...)
	item.GroupItems(items)
	return true, items
}

// 激活通知
func (ut *userStage) activationHighlyProfitableNotification() bool {
	info := ut.getCurrentStageTask()
	if info == nil {
		log.Debug("highly_profitable.userStage.activationNotification userId[%d] task not exist!", ut.userId)
		return false
	}
	if info.Status > pb.ClaimStatus_NotActive {
		log.Debug("highly_profitable.userStage.activationNotification userId[%d] It's already activated!", ut.userId)
		return false
	}

	info.Status = pb.ClaimStatus_Active
	d, _ := json.Marshal(ut.UserStageTask)
	log.Debug("highly_profitable.userStage.activationNotification send award UserId[%d] Items%v", ut.userId, info.PaidRewards)

	ut.createUserTasks(info.Tasks)
	go transHighlyProfitableSaveStageRecord(ut.userId, ut.Schedule, info.Status)
	go inventory.AddItems(ut.userId, info.PaidRewards, "highly profitable award", common.LOGTYPE_HIGHLY_PROFITABLE_AWARD)
	go notification.AddNotification(ut.userId, notification.Notification_HighlyProfitableChage, string(d))
	return true
}

func (ut *userStage) onUserFinishedRelativeTask() {
	info := ut.getCurrentStageTask()
	if info == nil {
		log.Debug("highly_profitable.userStage.onUserFinishedRelativeTask userId[%d] task not exist!", ut.userId)
		return
	}
	if info.Status != pb.ClaimStatus_Active {
		return
	}
	if !task.IsTasksFinished(ut.userId, info.Tasks) {
		return
	}

	info.Status = pb.ClaimStatus_Complete
	go transHighlyProfitableSaveStageRecord(ut.userId, ut.Schedule, info.Status)
	d, _ := json.Marshal(ut.UserStageTask)
	go notification.AddNotification(ut.userId, notification.Notification_HighlyProfitableChage, string(d))
}

// 用户打印
func (ut *userStage) dumpUser() {
	log.Debug("当前查询的用户ID：%d\r\n"+
		"  当前时间戳：%s\r\n"+
		"  当前阶段：%d\r\n"+
		"  任务列表\r\n", ut.userId, common.TimeStampToString(int64(ut.timeStamp)), ut.Schedule)
	for _, v := range ut.Tasks {
		log.Debug("\r\n"+
			"-----当前产品ID：%s\r\n"+
			"-----产品价格：%.2f\r\n"+
			"-----状态：%d （0 未激活， 1 已激活， 2 完成， 3 结束）\r\n"+
			"-----任务列表：%v\r\n"+
			"-----激活的奖励：%v\r\n"+
			"-----完成的奖励：%v\r\n"+
			"-----总奖励：%v\r\n",
			v.ProductId, v.Price, v.Status, v.Tasks, v.PaidRewards, v.SuccessRewards, v.TotalRewards)
	}
}
