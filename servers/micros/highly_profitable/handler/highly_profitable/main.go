package highly_profitable

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/highly_profitable/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
)

func Run() {
	getManager()
}

func Dump(cmd, param1 string) {
	switch cmd {
	case "sys":
		getManager().dumpSys(param1)
	case "user":
		getManager().dumpUser(param1)
	default:
		log.Release("highly_profitable.Dump unhandled cmd %s", cmd)
	}
}

// 获取一本万利活动
func GetHighlyProfitableActivity(userId int) pb.UserStageTask {
	return getManager().getHighlyProfitableActivity(userId)
}

// 领取一本万利奖励
func ClaimHighlyProfitableAward(userId int) (bool, []item.ItemPack) {
	return getManager().claimHighlyProfitableAward(userId)
}

// 激活通知
func ActivationHighlyProfitableNotification(userId int) bool {
	return getManager().activationHighlyProfitableNotification(userId)
}

func OnUserFinishedRelativeTask(userId int) {
	getManager().onUserFinishedRelativeTask(userId)
}
