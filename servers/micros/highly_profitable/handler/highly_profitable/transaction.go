package highly_profitable

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/highly_profitable/proto"
	"encoding/json"
)

// 获取用户当前阶段信息
func transHighlyProfitableGetUserStageInfo(userId int) pb.UserStageInfo {
	var out []pb.UserStageInfo
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_HighlyProfitable_GetUserStageInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)
	if err := json.Unmarshal([]byte(jsonData), &out); err != nil {
		log.Error("transHighlyProfitableGetUserStageInfo json unmarshal UserID=%d err %v", userId, err)
	}
	if len(out) <= 0 {
		return pb.UserStageInfo{}
	}

	return out[0]
}

// 修改阶段记录
func transHighlyProfitableSaveStageRecord(userId, schedule, status int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_HighlyProfitable_SaveStageRecord")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Schedule", database.AdParamInput, database.AdInteger, 4, schedule)
	statement.AddParamter("@Status", database.AdParamInput, database.AdTinyInt, 1, status)
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdVarChar, 20, common.GetNowTimeStr())
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
}
