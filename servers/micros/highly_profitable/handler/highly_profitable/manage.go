package highly_profitable

import (
	"encoding/json"
	"os"
	"strconv"
	"sync"
	"time"

	"bet24.com/log"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/shop"
	pb "bet24.com/servers/micros/highly_profitable/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

var mgr *manager

const config_key = "activity_highly_profitable"

func getManager() *manager {
	if mgr == nil {
		mgr = new(manager)
		mgr.ctor()
	}
	return mgr
}

type manager struct {
	configString string
	lock         *sync.RWMutex
	userList     map[int]*userStage
	sysConfig    []pb.HighlyProfitable
}

func (m *manager) ctor() {
	log.Debug("highly_profitable running")
	m.lock = &sync.RWMutex{}
	m.userList = make(map[int]*userStage)
	m.readConf()
}

// 读取配置
func (m *manager) readConf() {
	time.AfterFunc(time.Second*600, m.readConf)
	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("serviceconf/activity_highly_profitable.json")
		if err != nil {
			log.Release("highly_profitable.manager.loadData read config failed")
			return
		}
		configString = string(data)
		if configString != "" {
			platformconfig.SetConfig(config_key, configString)
		}
	}

	if configString == m.configString {
		return
	}

	m.configString = configString
	// 读取配置

	err := json.Unmarshal([]byte(configString), &m.sysConfig)
	if err != nil {
		log.Release("highly_profitable.manager.loadData Unmarshal failed %s", configString)
	}
	m.loadProductItem()
}

// 加载对应产品的道具
func (m *manager) loadProductItem() {
	for key, value := range m.sysConfig {
		respProduct := coreClient.GetProduct(0, 0, value.ProductId)
		if respProduct.RetCode != 1 {
			continue
		}
		var items shop.Shop_Item
		if err := json.Unmarshal([]byte(respProduct.Data), &items); err != nil {
			log.Error("%s GetProduct json unmarshal err %v", "highly_profitable.manager.loadProductItem", err)
			continue
		}
		m.sysConfig[key].Price = items.Price
		//m.sysConfig[key].Items = items.Extra
	}
}

// 检查过期用户
func (m *manager) checkExpiredUsers() {
	time.AfterFunc(time.Second*600, m.checkExpiredUsers)
	var toRemove []int
	m.lock.RLock()
	for k, v := range m.userList {
		if v.isExpire() {
			toRemove = append(toRemove, k)
		}
	}
	m.lock.RUnlock()
	if len(toRemove) == 0 {
		return
	}

	log.Debug("highly_profitable.manager.checkExpiredUsers removing %v", toRemove)
	m.lock.Lock()
	for _, v := range toRemove {
		delete(m.userList, v)
	}
	m.lock.Unlock()
}

// 用户打印
func (m *manager) dumpUser(param string) {
	log.Release("-------------------------------")
	log.Release("highly_profitable.manager.dumpUser %s", param)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	if param == "" {
		m.lock.RLock()
		for _, v := range m.userList {
			go v.dumpUser()
		}
		m.lock.RUnlock()
		return
	}

	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}
	m.lock.RLock()
	val, ok := m.userList[userId]
	m.lock.RUnlock()
	if !ok {
		log.Release("The user does not exist.", userId)
		return
	}
	val.dumpUser()
}

// 系统打印
func (m *manager) dumpSys(param string) {
	log.Release("-------------------------------")
	log.Release("highly_profitable.manager.dumpSys")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	for k, v := range m.sysConfig {
		log.Release("当前阶段：%d\r\n"+
			"  当前产品ID：%s\r\n"+
			"  当前产品价格：%.2f\r\n"+
			"  当前阶段状态：%d (0 未激活， 1 已激活， 2 未完成， 3 结束)\r\n"+
			"  当前任务列表：%v\r\n"+
			"  当前激活奖励：%v\r\n"+
			"  当前完成奖励：%v\r\n"+
			"  当前总奖励：%v\r\n", k+1, v.ProductId, 0.99, v.Status, v.Tasks, v.PaidRewards, v.SuccessRewards, v.TotalRewards)
	}
}

// 获取用户任务
func (m *manager) getUserStage(userId int) *userStage {
	m.lock.RLock()
	u, ok := m.userList[userId]
	m.lock.RUnlock()
	if ok {
		u.updateTimeStamp()
		return u
	}
	u = newUserStage(userId)
	m.lock.Lock()
	m.userList[userId] = u
	m.lock.Unlock()
	return u
}

// 获取一本万利活动
func (m *manager) getHighlyProfitableActivity(userId int) pb.UserStageTask {
	u := m.getUserStage(userId)
	return u.UserStageTask
}

// 领取一本万利奖励
func (m *manager) claimHighlyProfitableAward(userId int) (bool, []item.ItemPack) {
	u := m.getUserStage(userId)
	return u.claimHighlyProfitableAward()
}

// 激活通知
func (m *manager) activationHighlyProfitableNotification(userId int) bool {
	u := m.getUserStage(userId)
	return u.activationHighlyProfitableNotification()
}

func (m *manager) onUserFinishedRelativeTask(userId int) {
	u := m.getUserStage(userId)
	u.onUserFinishedRelativeTask()
}
