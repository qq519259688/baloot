package handler

import (
	"bet24.com/log"
	"bet24.com/servers/micros/highly_profitable/handler/highly_profitable"
	pb "bet24.com/servers/micros/highly_profitable/proto"
	"context"
	"fmt"
)

var instance *highlyProfitableService

type highlyProfitableService struct {
}

func GetInstance() *highlyProfitableService {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func newHandler() *highlyProfitableService {
	ret := new(highlyProfitableService)
	ret.ctor()
	return ret
}

func (h *highlyProfitableService) ctor() {
	highly_profitable.Run()
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func (h *highlyProfitableService) dump(cmd, param1, param2 string) {
	switch cmd {
	case "highly_profitable":
		highly_profitable.Dump(param1, param2)
	default:
		log.Release("highlyProfitableService.Dump unhandled cmd %s", cmd)
	}
}

func (h *highlyProfitableService) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}
