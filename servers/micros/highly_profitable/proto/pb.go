package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	"context"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "highly_profitable"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name   string
	UserId int
}

type Response struct {
	Success bool
	Data    string
	Items   []item.ItemPack
	List    UserStageTask
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("task failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}
