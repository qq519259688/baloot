package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	"context"
)

func GetHighlyProfitableActivity(userId int) UserStageTask {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "GetHighlyProfitableActivity", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("highly_profitable.GetHighlyProfitableActivity failed to call: %v", err)
		return UserStageTask{}
	}

	return reply.List
}

func ClaimHighlyProfitableAward(userId int) (bool, []item.ItemPack) {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "ClaimHighlyProfitableAward", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("highly_profitable.ClaimHighlyProfitableAward failed to call: %v", err)
		return false, nil
	}
	return reply.Success, reply.Items
}

func ActivationHighlyProfitableNotification(userId int) bool {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	reply := &Response{}
	err := xclient.Call(context.Background(), "ActivationHighlyProfitableNotification", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("highly_profitable.ActivationHighlyProfitableNotification failed to call: %v", err)
		return false
	}
	return reply.Success
}

func OnUserFinishedRelativeTask(userId int) {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}
	err := xclient.Call(context.Background(), "OnUserFinishedRelativeTask", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("highly_profitable.OnUserFinishedRelativeTask failed to call: %v", err)
	}
}
