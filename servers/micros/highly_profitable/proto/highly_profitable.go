package proto

import item "bet24.com/servers/micros/item_inventory/proto"

const (
	ClaimStatus_NotActive = iota // 0 未激活
	ClaimStatus_Active           // 1 已激活
	ClaimStatus_Complete         // 2 完成
	ClaimStatus_End              // 3 结束
)

// 用户阶段任务
type UserStageTask struct {
	Schedule int                 // 当前阶段
	Tasks    []*HighlyProfitable // 阶段任务
}

// 一本万利
type HighlyProfitable struct {
	ProductId      string          // 产品ID
	Price          float64         `json:",omitempty"` // 价格
	Status         int             `json:",omitempty"` // 礼包的状态（0 未激活， 1 已激活， 2 完成， 3 结束）【注：左上角激活就送与完成所有任务可领礼包状态】
	Tasks          []int           // 任务列表
	PaidRewards    []item.ItemPack // 激活的奖励
	SuccessRewards []item.ItemPack // 完成的奖励
	TotalRewards   []item.ItemPack `json:",omitempty"` // 总奖励
}

// 用户的阶段信息
type UserStageInfo struct {
	Schedule int // 当前阶段
	Status   int // 礼包的状态（0 未激活， 1 已激活， 2 未完成， 3 结束）【注：左上角激活就送与完成所有任务可领礼包状态】
}
