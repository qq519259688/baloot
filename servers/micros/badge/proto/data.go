package proto

import (
	"bet24.com/servers/common"
	item "bet24.com/servers/micros/item_inventory/proto"
	task "bet24.com/servers/micros/task/proto"
)

const (
	UsageLevel = 1 // 使用等级
	MaxLevel   = 3 // 黄金 等级3（最高等级）
)

const (
	_                     = iota // 0=无效
	BadgeType_Achievement        // 1=成就徽章
	BadgeType_Honor              // 2=荣誉徽章
)

const (
	Action_Recharge                       = iota + 1000 // 1000=充值
	Action_Game_Kill                                    // 1001=游戏击杀
	Action_Game_CallPoints                              // 1002=累计叫分次数
	Action_AudioRoom_RoomGrade                          // 1003=语聊房房间等级
	Action_AudioRoom_SendGivingDiamond                  // 1004=赠送的礼物价值钻石
	Action_AudioRoom_SendGivingNum                      // 1005=赠送礼物数量
	Action_AudioRoom_ReceiveGivingDiamond               // 1006=收到礼物价值钻石
	Action_AudioRoom_ReceiveGivingNum                   // 1007=收到礼物数量
	Action_Ranking                                      // 1008=排行榜
	Action_PropConsumeGold                              // 1009=购买道具消耗金币
	Action_CompleteTheTask                              // 1010=完成任务
	Action_SignInCount                                  // 1011=签到次数

	Action_ConsumeGold         = task.TaskAction_fire            // 5=消耗金币
	Action_Game_WinTimes       = task.TaskAction_wingame         // 3=胜利次数
	Action_Match_ChampionTimes = task.TaskAction_matchchampion   // 23=冠军次数、赛事冠军次数
	Action_NoviceGuide         = task.TaskAction_novice_guidance // 25=新手引导
	Action_CollectGold         = task.TaskAction_earn            // 2=赢金
)

// 徽章
type Badge struct {
	BadgeId      int           // 徽章ID
	BadgeName    string        // 徽章名称
	BadgeType    int           // 徽章类型（1=成就徽章，2=荣誉徽章）
	Action       int           // 触发动作
	Levels       []Badge_Level // 徽章等级
	BadgeScope                 // 适用范围
	Duration     int           // 持续时间【秒】（0=不限制）
	Model        int           // 显示的模式（0：所有，1：语聊房，2：...）
	IsShow       bool          // 是否显示
	IsCollection bool          // 是否收集进度
}

// 等级信息
type Badge_Level struct {
	Level  int `json:",omitempty"` // 徽章等级
	Exps   int `json:",omitempty"` // 经验值
	Points int `json:",omitempty"` // 徽章点数（用于排行榜），荣耀徽章始终为0
}

// 适用范围
type BadgeScope struct {
	GameNames []string   `json:",omitempty"` // 游戏名
	ItemIds   []int      `json:",omitempty"` // 道具id
	GiftIds   []int      `json:",omitempty"` // 礼物id
	TaskIds   []int      `json:",omitempty"` // 任务id
	Ranks     []RankInfo `json:",omitempty"` // 榜单
}

// 应用调用时传入参数
type Scope struct {
	GameName   string          `json:",omitempty"` // 游戏名
	Items      []item.ItemPack `json:",omitempty"` // 实体道具
	GiftId     int             `json:",omitempty"` // 礼物ID
	TaskId     int             `json:",omitempty"` // 任务id
	RankCrdate string          `json:",omitempty"` // 榜单的创建开始日期
	RankInfo                   // 榜单
}

// 是否使用
func (b *Badge) IsApplicative(param Scope) bool {
	for _, v := range b.GameNames {
		if v == param.GameName {
			return true
		}
	}
	return false
}

// 榜单信息
type RankInfo struct {
	Type  int `json:",omitempty"` // 榜单类型
	Rank  int `json:",omitempty"` // 榜单名次
	Score int `json:",omitempty"` // 分数
}

// 用户徽章
type UserBadge struct {
	BadgeId     int    // 徽章ID
	Badge_Level        // 等级经验(从数据库获取重新计算等级)
	Times       int    `json:",omitempty"` // 获取次数
	Position    int    `json:",omitempty"` // 位置
	Wearable    bool   // 是否可佩戴
	ExpiresTime int    `json:",omitempty"` // 到期时间戳
	ObtainDate  string `json:",omitempty"` // 徽章多个等级的获得日期
	UnlockTime  []int  `json:",omitempty"` // 每个徽章的解锁时间，多个
	Crdate      string // 创建时间
}

// 徽章是否过期
func (ub *UserBadge) IsExpires() bool {
	return ub.ExpiresTime <= common.GetTimeStamp()
}

// 是否能佩戴
func (ub *UserBadge) IsWearable() bool {
	if ub.ExpiresTime > 0 {
		return ub.ExpiresTime > common.GetTimeStamp()
	}
	return ub.Level >= UsageLevel
}

// 徽章位置
type BadgePosition struct {
	Position int // 穿戴的位置（返回结果集始终是从小到大排列）【-1：展示类，0以上是是佩戴的位置】
	BadgeId  int // 徽章的id
	Level    int // 徽章的等级
}

// 获取等级
func GetLevel(exps int, levels []Badge_Level) (int, map[int]int) {
	var level int
	lvPoints := make(map[int]int, 0)
	for k, v := range levels {
		lvPoints[levels[k].Level] = levels[k].Points
		if v.Exps <= exps {
			level = levels[k].Level
		}
	}
	return level, lvPoints
}

// 检查排名分
func CheckRankingScore(sysRank []RankInfo, param RankInfo) bool {
	for _, r := range sysRank {
		if r.Score > 0 && param.Score >= r.Score {
			return true
		}
		if r.Type == 0 {
			continue
		}
		if r.Type == param.Type && r.Rank == param.Rank {
			return true
		}
	}
	return false
}

// 游戏匹配
func GameMatch(sysIds []int, id int) bool {
	for _, s := range sysIds {
		if s == id {
			return true
		}
	}
	return false
}

// 礼物id匹配
func GiftIdMatch(sysIds []int, items []item.ItemPack) bool {
	for _, v := range sysIds {
		for _, i := range items {
			if v == i.ItemId {
				return true
			}
		}
	}
	return false
}
