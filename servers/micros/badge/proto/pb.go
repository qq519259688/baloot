package proto

import (
	"context"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "badgeservice"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	UserId    int    `json:",omitempty"`
	Model     int    `json:",omitempty"`
	BadgeId   int    `json:",omitempty"`
	Position  int    `json:",omitempty"`
	Name      string `json:",omitempty"`
	IpAddress string `json:",omitempty"`
	Action    int    `json:",omitempty"`
	Progress  int    `json:",omitempty"`
	Param     Scope  `json:",omitempty"`
}

type Response struct {
	RetCode int
	Data    string
	Badges  []BadgePosition
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("%s failed to call: %v", ServiceName, err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("userLabel return %s", reply.Data)
	return reply.Data
}

// 获取系统成就的徽章列表
func GetSysBadgeList() string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetSysBadgeList", args, reply)
	if err != nil {
		log.Debug("%s failed to call: %v", ServiceName, err)
		common.GetClientPool().RemoveClient(ServiceName)
		return "[]"
	}

	return reply.Data
}

// 获取用户徽章佩戴与展示
func GetBadgeWearAndShow(userId int) []BadgePosition {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetBadgeWearAndShow", args, reply)
	if err != nil {
		log.Debug("%s failed to call: %v", ServiceName, err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}

	return reply.Badges
}

// 获取用户徽章列表
func GetUserBadgeList(userId int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserBadgeList", args, reply)
	if err != nil {
		log.Debug("%s failed to call: %v", ServiceName, err)
		common.GetClientPool().RemoveClient(ServiceName)
		return "[]"
	}

	if reply.Data == "" {
		return "[]"
	}
	return reply.Data
}

// 佩戴成就徽章
func WearBadge(userId, badgeId, position int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId:   userId,
		BadgeId:  badgeId,
		Position: position,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "WearBadge", args, reply)
	if err != nil {
		log.Debug("%s failed to call: %v", ServiceName, err)
		common.GetClientPool().RemoveClient(ServiceName)
		return "[]"
	}

	if reply.Data == "" {
		return "[]"
	}
	return reply.Data
}

// 触发动作（徽章进度）
func DoAction(userId, action, progress int, param Scope) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		UserId:   userId,
		Action:   action,
		Progress: progress,
		Param:    param,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "DoAction", args, reply)
	if err != nil {
		log.Debug("%s failed to call: %v", ServiceName, err)
		common.GetClientPool().RemoveClient(ServiceName)
	}

	return
}
