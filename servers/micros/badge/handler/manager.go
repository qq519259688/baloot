package handler

import (
	"encoding/json"
	"os"
	"strconv"
	"sync"
	"time"

	"bet24.com/log"
	"bet24.com/servers/common"
	pb "bet24.com/servers/micros/badge/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "badge_config"

var mgr *badgeMgr

type badgeMgr struct {
	lock     *sync.RWMutex     // 锁
	sysList  []*pb.Badge       // 系统徽章列表
	userList map[int]*userInfo // 用户徽章列表

	lastConfigString string
}

func newBadgeMgr() {
	mgr = new(badgeMgr)
	mgr.lock = &sync.RWMutex{}
	mgr.userList = make(map[int]*userInfo)
	mgr.loadSysBadgeList()
	mgr.doCheck()
	return
}

// 加载系统徽章
func (b *badgeMgr) loadSysBadgeList() {
	go time.AfterFunc(10*time.Minute, mgr.loadSysBadgeList)
	configString := platformconfig.GetConfig(config_key)
	if configString == "" {
		data, err := os.ReadFile("serviceconf/badge_config.json")
		if err != nil {
			log.Release("read config failed serviceconf/badge_config.json %v", err)
			return
		}
		configString = string(data)
		platformconfig.SetConfig(config_key, configString)
	}

	if configString == b.lastConfigString {
		return
	}
	b.lastConfigString = configString
	err := json.Unmarshal([]byte(configString), &b.sysList)
	if err != nil {
		log.Release("badgeMgr.loadSysBadgeList Unmarshal config [%s] err:%v", configString, err)
		return
	}
	// 刷新榜单数据
	transRefreshRankData()
	return
}

// 轮询
func (b *badgeMgr) doCheck() {
	ticker := time.NewTicker(1 * time.Minute)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				b.checkUserExpire()
			}
		}
	}(ticker)
}

// 检查用户过期
func (b *badgeMgr) checkUserExpire() {
	var toRemove []int
	b.lock.RLock()
	// 遍历所有用户
	for _, v := range b.userList {
		// 判断是否过期
		if !v.isExpire() {
			continue
		}
		toRemove = append(toRemove, v.userId)
		v.destructor()
	}
	b.lock.RUnlock()

	if len(toRemove) == 0 {
		return
	}

	b.lock.Lock()
	defer b.lock.Unlock()
	for _, u := range toRemove {
		// 删除
		delete(b.userList, u)
		log.Debug("badgeMgr.checkUserExpire userId=%d 已完成清理", u)
	}
	return
}

// 获取系统成就的徽章列表
func (b *badgeMgr) getSysBadgeList() string {
	d, _ := json.Marshal(b.sysList)
	return string(d)
}

// 获取用户
func (b *badgeMgr) getUser(userId int) *userInfo {
	b.lock.RLock()
	u, ok := b.userList[userId]
	b.lock.RUnlock()
	if ok {
		u.updateTimeStamp()
		return u
	}

	u = newUserInfo(userId)
	b.lock.Lock()
	b.userList[userId] = u
	b.lock.Unlock()
	return u
}

// 获取系统成就的徽章信息
func (b *badgeMgr) getSysBadgeInfo(badgeId int) *pb.Badge {
	for k, v := range b.sysList {
		if v.BadgeId == badgeId {
			return b.sysList[k]
		}
	}
	return nil
}

// 获取用户徽章列表
func (b *badgeMgr) getUserBadgeList(userId int) string {
	u := b.getUser(userId)
	if u == nil {
		log.Debug("badgeMgr.getUserBadgeInfo userId=%d not exist", userId)
		return ""
	}
	list := u.getUserBadgeList()
	d, _ := json.Marshal(list)
	return string(d)
}

// 获取用户佩戴与展示的徽章
func (b *badgeMgr) getBadgeWearAndShow(userId int) []pb.BadgePosition {
	u := b.getUser(userId)
	if u == nil {
		log.Debug("badgeMgr.getBadgeWearAndShow userId=%d not exist", userId)
		return []pb.BadgePosition{}
	}
	return u.getBadgeWearAndShow()
}

// 佩戴成就徽章
func (b *badgeMgr) wearBadge(userId, badgeId, position int) string {
	u := b.getUser(userId)
	if u == nil {
		log.Debug("badgeMgr.wearBadge userId=%d not exist", userId)
		return ""
	}
	list := u.wearBadge(badgeId, position)
	d, _ := json.Marshal(list)
	return string(d)
}

// 触发动作（徽章进度）
func (b *badgeMgr) doAction(userId, action, progress int, param pb.Scope) {
	u := b.getUser(userId)
	if u == nil {
		log.Debug("badgeMgr.doAction userId=%d not exist", userId)
		return
	}
	u.doAction(action, progress, param)
	return
}

// 打印用户
func (b *badgeMgr) dumpUser(param1 string) {
	log.Release("-------------------------------")
	log.Release("badgeMgr.dumpSys success")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	if param1 == "" {
		b.lock.RLock()
		log.Release("  Total User Count:%d", len(b.userList))
		b.lock.RUnlock()
		return
	}

	var userId int
	var err error
	if param1 != "" {
		userId, err = strconv.Atoi(param1)
		if err != nil {
			log.Release("badgeMgr.dumpUser Parameter invalid. param1[%s]", param1)
			return
		}
	}
	v := b.getUser(userId)
	if v == nil {
		log.Release("badgeMgr.dumpUser user[%d] not exist", userId)
		return
	}

	log.Release("当前用户ID：%d", v.userId)
	for _, info := range v.badgeList {
		log.Release("徽章id：%d", info.BadgeId)
		log.Release("徽章等级：%d", info.Level)
		log.Release("经验值：%d", info.Exps)
		log.Release("成就点数：%d", info.Points)
		log.Release("领取次数：%d", info.Times)
		log.Release("当前位置：%d", info.Position)
		log.Release("是否能佩戴：%v", info.Wearable)
		if info.ExpiresTime > 0 {
			log.Release("过期时间：%s", common.TimeStampToString(int64(info.ExpiresTime)))
		}
		for k, t := range info.UnlockTime {
			log.Release("  等级[%d]，解锁时间：%s", k+1, common.TimeStampToString(int64(t)))
		}
		log.Release("")
	}
}

// 打印系统徽章
func (b *badgeMgr) dumpSys() {
	log.Release("-------------------------------")
	log.Release("badgeMgr.dumpSys success")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	sysList := b.getSysBadgeList()
	log.Release(sysList)
}
