package handler

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	pb "bet24.com/servers/micros/badge/proto"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	"encoding/json"
	"fmt"
	"time"
)

// 获取用户徽章列表
func transGetUserBadgeList(userId int) []*pb.UserBadge {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserBadge_GetBadgeList")
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	jsonData := dbengine.Execute(sqlString)

	var out []*pb.UserBadge
	if err := json.Unmarshal([]byte(jsonData), &out); err != nil {
		log.Error("transaction.transGetUserBadgeList json unmarshal UserID=%d err %v", userId, err)
		return out
	}
	for k, v := range out {
		t, err := time.Parse(time.RFC3339Nano, v.Crdate)
		if err != nil {
			fmt.Println("WS_UserBadge_GetBadgeList 解析时间错误:", err)
			continue
		}
		out[k].Crdate = t.Format(common.Layout)
	}
	return out
}

// 保存用户徽章
func transSaveUserBadge(userId int, u *pb.UserBadge) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserBadge_SaveBadge")
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@BadgeId", database.AdParamInput, database.AdInteger, 4, u.BadgeId)
	statement.AddParamter("@Level", database.AdParamInput, database.AdSmallInt, 4, u.Level)
	statement.AddParamter("@Exps", database.AdParamInput, database.AdInteger, 4, u.Exps)
	statement.AddParamter("@Points", database.AdParamInput, database.AdTinyInt, 4, u.Points)
	statement.AddParamter("@Times", database.AdParamInput, database.AdInteger, 4, u.Times)
	statement.AddParamter("@Position", database.AdParamInput, database.AdSmallInt, 1, u.Position)
	statement.AddParamter("@ExpiresTime", database.AdParamInput, database.AdInteger, 16, u.ExpiresTime)
	statement.AddParamter("@ObtainDate", database.AdParamInput, database.AdVarChar, 128, u.ObtainDate)
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdVarChar, 20, u.Crdate)
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}

// 更新榜单数据
func transRefreshRankData() {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_Rank_ReloadBadgeRanking")
	sqlString := statement.GenSql()
	dbengine.Execute(sqlString)
	return
}
