package handler

import (
	"context"
	"fmt"

	"bet24.com/log"
	pb "bet24.com/servers/micros/badge/proto"
)

var instance *badge

type badge struct {
}

func GetInstance() *badge {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func newHandler() *badge {
	ret := new(badge)
	ret.ctor()
	return ret
}

func (g *badge) ctor() {
	newBadgeMgr()
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func (g *badge) dump(cmd, param1, param2 string) {
	switch cmd {
	case "config":
		mgr.dumpSys()
	case "user":
		mgr.dumpUser(param1)
	default:
		log.Release("badge.Dump unhandled cmd %s", cmd)
	}
}

func (g *badge) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (g *badge) GetSysBadgeList(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = mgr.getSysBadgeList()
	return nil
}

func (g *badge) GetBadgeWearAndShow(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Badges = mgr.getBadgeWearAndShow(req.UserId)
	return nil
}

func (g *badge) GetUserBadgeList(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = mgr.getUserBadgeList(req.UserId)
	return nil
}

func (g *badge) WearBadge(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = mgr.wearBadge(req.UserId, req.BadgeId, req.Position)
	return nil
}

func (g *badge) DoAction(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	mgr.doAction(req.UserId, req.Action, req.Progress, req.Param)
	return nil
}
