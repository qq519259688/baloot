package main

import (
	"fmt"

	"bet24.com/servers/micros/badge/config"
	"bet24.com/servers/micros/badge/handler"
	pb "bet24.com/servers/micros/badge/proto"
	"bet24.com/servers/micros/common"
)

func main() {
	config.Run(pb.ServiceName)
	go waitInput()

	common.RunService(pb.ServiceName, handler.GetInstance(), fmt.Sprintf("localhost:%d", config.Server.ConsulPort))
}

func waitInput() {
	for {
		var cmd string
		var param1 string
		var param2 string
		fmt.Scanf("%s %s %s", &cmd, &param1, &param2)
		switch cmd {
		case "exit":
			common.DeregisterService(pb.ServiceName)
		default:
			handler.Dump(cmd, param1, param2)
		}
	}
}
