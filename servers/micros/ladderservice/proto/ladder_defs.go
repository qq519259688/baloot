package proto

// 连胜加分
type ConsecutiveWinPoint struct {
	Count            int // 连胜次数
	AddictionalPoint int // 额外分数
}

type LadderInfo struct {
	Ladder int // 所在段位
	Level  int // 段位等级
	Star   int // 星星数量
}

func (li *LadderInfo) IsChanged(old LadderInfo) bool {
	if li.Ladder != old.Ladder {
		return true
	}
	if li.Level != old.Level {
		return true
	}
	if li.Star != old.Star {
		return true
	}
	return false
}

type UserLadderInfo struct {
	LadderInfo
	Point     int             // 分数
	MaxConWin int             // 最大连胜
	WinCount  int             // 赢
	LoseCount int             // 输
	DrawCount int             // 平
	CurConWin int             // 当前累计连胜
	Records   []WinningStreak // 结算的连胜记录
}

type LadderConfig struct {
	Ladder int    // id，用于前端标记
	Name   string // 名称
	Levels []LevelConfig
}

type LevelConfig struct {
	Level int
	Stars []StarConfig
}

type StarConfig struct {
	Star  int
	Point int
}

type GameRoomInfo struct {
	GameId            int
	BaseScore         int    // 底分
	MinGold           int    // 最小携带
	MaxGold           int    // 最大携带 0不限制
	AdditionalPercent int    // 加成
	ServerAddr        string // 服务器地址
	RoomName          string // 坐下规则名称
	GameRule          string // 详细规则
}

// 结算记录
type SettlementRecord struct {
	TotalPoint        int             // 总分数
	WinNumber         int             // 胜利次数
	WinPoint          int             // 赢的分数
	LoseNumber        int             // 失败次数
	LosePoint         int             // 输的分数
	Records           []WinningStreak // 连胜记录
	AdditionalPercent int             // 加成
}

// 连胜记录
type WinningStreak struct {
	Point    int // 分数
	WinCount int // 胜利次数
	WinType  int // 连胜类型
}
