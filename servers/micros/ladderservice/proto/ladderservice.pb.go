package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "ladderservice"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	Name       string
	UserId     int
	GameId     int
	Score      int
	ServerAddr string
	GameRoomInfo
}

type Response struct {
	Data       string
	UL         *UserLadderInfo
	Settlement SettlementRecord
	Success    bool
	Value      int
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("ladderservice failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("SayHello return %s", reply.Data)
	return reply.Data
}

func GetSystemLadderInfo() string {
	xclient := getClient()

	args := &Request{}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetSystemLadderInfo", args, reply)
	if err != nil {
		log.Debug("ladderservice.GetSystemLadderInfo failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	return reply.Data
}

func GetUserLadderInfo(userId int, gameId int) *UserLadderInfo {
	xclient := getClient()

	args := &Request{
		UserId: userId,
		GameId: gameId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserLadderInfo", args, reply)
	if err != nil {
		log.Debug("ladderservice.GetUserLadderInfo failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return nil
	}

	return reply.UL
}

func GetUserConsecutiveWinCount(userId int, gameId int) int {
	xclient := getClient()
	args := &Request{
		UserId: userId,
		GameId: gameId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserConsecutiveWinCount", args, reply)
	if err != nil {
		log.Debug("ladderservice.GetUserConsecutiveWinCount failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}

	return reply.Value
}

func UseConsecutiveCard(userId int) bool {
	xclient := getClient()

	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "UseConsecutiveCard", args, reply)
	if err != nil {
		log.Debug("ladderservice.UseConsecutiveCard failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return false
	}

	return reply.Success
}

// 房间相关
func RegisterLadderRoom(gameId int, roomName, gameRule string, base, min, max int, additionalPercent int, serverAddr string) {
	xclient := getClient()
	args := &Request{
		GameRoomInfo: GameRoomInfo{
			GameId:            gameId,
			RoomName:          roomName,
			BaseScore:         base,
			MinGold:           min,
			MaxGold:           max,
			AdditionalPercent: additionalPercent,
			ServerAddr:        serverAddr,
			GameRule:          gameRule,
		},
	}

	err := xclient.Call(context.Background(), "RegisterLadderRoom", args, nil)
	if err != nil {
		log.Debug("ladderservice.RegisterLadderRoom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func DeregisterLadderRoom(serverAddr string) {
	log.Debug("DeregisterLadderRoom serverAddr = %s", serverAddr)
	xclient := getClient()
	args := &Request{}
	args.ServerAddr = serverAddr

	err := xclient.Call(context.Background(), "DeregisterLadderRoom", args, nil)
	if err != nil {
		log.Debug("ladderservice.DeregisterLadderRoom failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func GetLadderRoomList() string {
	xclient := getClient()
	args := &Request{}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetLadderRoomList", args, reply)
	if err != nil {
		log.Debug("ladderservice.GetLadderRoomList failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}
	return reply.Data
}

func AddUserLadderScore(userId int, gameId int, roomName string, score int) (int, bool) {
	xclient := getClient()
	args := &Request{
		UserId: userId,
		GameId: gameId,
		Score:  score,
	}
	args.RoomName = roomName

	reply := &Response{}

	err := xclient.Call(context.Background(), "AddUserLadderScore", args, reply)
	if err != nil {
		log.Debug("ladderservice.AddUserLadderScore failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0, false
	}
	return reply.Value, reply.Success
}

func GetUserSettlementRecord(userId int) SettlementRecord {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserSettlementRecord", args, reply)
	if err != nil {
		log.Debug("ladderservice.GetUserSettlementRecord failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return SettlementRecord{}
	}
	return reply.Settlement
}

func GetUserHistoricalRecord(userId int) []WinningStreak {
	xclient := getClient()
	args := &Request{
		UserId: userId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetUserHistoricalRecord", args, reply)
	if err != nil {
		log.Debug("ladderservice.GetUserHistoricalRecord failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return []WinningStreak{}
	}
	return reply.Settlement.Records
}

func GetLadderRoomAdditionalPercent(gameId int, ruleName string) int {
	xclient := getClient()
	args := &Request{
		Name:   ruleName,
		GameId: gameId,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "GetLadderRoomAdditionalPercent", args, reply)
	if err != nil {
		log.Debug("ladderservice.GetLadderRoomAdditionalPercent failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return 0
	}
	return reply.Value
}
