package handler

import (
	"bet24.com/log"
	"bet24.com/servers/micros/ladderservice/handler/ladder"
	pb "bet24.com/servers/micros/ladderservice/proto"
	"context"
	"fmt"
)

var instance *MainHandler

func GetInstance() *MainHandler {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *MainHandler {
	ret := new(MainHandler)
	ret.ctor()
	return ret
}

type MainHandler struct{}

func (h *MainHandler) ctor() {
	ladder.Run()
}

func (d *MainHandler) dump(cmd, param1, param2 string) {
	switch cmd {
	case "ladder":
		ladder.Dump(param1, param2)
	default:
		log.Release("ladderservice.MainHandler.dump unhandled cmd")
	}
}

func (h *MainHandler) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (h *MainHandler) GetSystemLadderInfo(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = ladder.GetSystemLadderInfo()
	return nil
}

func (h *MainHandler) GetUserLadderInfo(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.UL = ladder.GetUserLadderInfo(req.UserId)
	return nil
}

func (h *MainHandler) GetUserConsecutiveWinCount(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Value = ladder.GetUserConsecutiveWinCount(req.UserId, req.GameId)
	return nil
}

func (h *MainHandler) UseConsecutiveCard(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Success = ladder.UseConsecutiveCard(req.UserId, req.GameId)
	return nil
}

func (h *MainHandler) RegisterLadderRoom(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	ladder.RegisterLadderRoom(req.GameRoomInfo)
	return nil
}

func (h *MainHandler) DeregisterLadderRoom(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	ladder.DeregisterLadderRoom(req.ServerAddr)
	return nil
}

func (h *MainHandler) GetLadderRoomList(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = ladder.GetLadderRoomList()
	return nil
}

func (h *MainHandler) AddUserLadderScore(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Value, rsp.Success = ladder.AddUserScore(req.UserId, req.GameId, req.Score, req.RoomName)
	return nil
}

func (h *MainHandler) GetUserSettlementRecord(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Settlement = ladder.GetSettlementRecord(req.UserId)
	return nil
}

func (h *MainHandler) GetUserHistoricalRecord(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Settlement.Records = ladder.GetHistoricalRecord(req.UserId)
	return nil
}

func (h *MainHandler) GetLadderRoomAdditionalPercent(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Value = ladder.GetLadderRoomAdditionalPercent(req.GameId, req.Name)
	return nil
}
