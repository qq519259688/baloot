package ladder

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	inventory "bet24.com/servers/micros/item_inventory/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	"strconv"
	"sync"
	"time"
)

var conMgr *consecutiveMgr

func getConsecutiveMgr() *consecutiveMgr {
	if conMgr == nil {
		conMgr = new(consecutiveMgr)
		conMgr.ctor()
	}
	return conMgr
}

type consecutiveMgr struct {
	lock        *sync.RWMutex
	userRecords map[int]*userConsecutiveRecord
}

func (cm *consecutiveMgr) ctor() {
	log.Release("ladder.consecutiveMgr.ctor()")
	cm.lock = &sync.RWMutex{}
	cm.userRecords = make(map[int]*userConsecutiveRecord)
}

func (cm *consecutiveMgr) checkExpireUser() {
	time.AfterFunc(10*time.Minute, cm.checkExpireUser)
	var toRemove []int

	cm.lock.RLock()
	for k, v := range cm.userRecords {
		if v.isExpired() {
			toRemove = append(toRemove, k)
		}
	}
	cm.lock.RUnlock()

	if len(toRemove) == 0 {
		return
	}

	log.Release("consecutiveMgr.checkExpireUser removing %v", toRemove)

	cm.lock.Lock()
	for _, v := range toRemove {
		delete(cm.userRecords, v)
	}
	cm.lock.Unlock()
}

func (cm *consecutiveMgr) loadUserRecord(userId int) *userConsecutiveRecord {
	return newUserConsecutiveRecord(userId)
}

func (cm *consecutiveMgr) getUserRecord(userId int) *userConsecutiveRecord {
	cm.lock.RLock()
	ur, ok := cm.userRecords[userId]
	cm.lock.RUnlock()
	if !ok {
		ur = cm.loadUserRecord(userId)
		cm.lock.Lock()
		cm.userRecords[userId] = ur
		cm.lock.Unlock()
	}
	return ur
}

func (cm *consecutiveMgr) getUserConsecutiveWinCount(userId int, gameId int) int {
	ur := cm.getUserRecord(userId)
	if ur == nil {
		log.Release("consecutiveMgr.getUserConsecutiveWinCount[%d] user not exist", userId)
		return 0
	}
	return ur.getConsecutiveWinCount(gameId)
}

func (cm *consecutiveMgr) addUserRecord(userId int, gameId int, score int) {
	ur := cm.getUserRecord(userId)
	if ur == nil {
		log.Release("consecutiveMgr.addUserRecord[%d] user not exist", userId)
		return
	}
	ur.addRecord(score, gameId)
}

func (cm *consecutiveMgr) removeLastLost(userId int, gameId int) bool {
	ur := cm.getUserRecord(userId)
	if ur == nil {
		log.Release("consecutiveMgr.removeLastLost[%d] user not exist", userId)
		return false
	}
	// 检查道具是否有
	price := getLadderConfig().ConsecutiveCardPrice
	if price.Count > 0 {
		ok, errMsg := inventory.Consume(userId, price.ItemId, common.LOGTYPE_LADDER_CONSECUTIVE_CARD, price.Count, 0)
		if !ok {
			log.Release("consecutiveMgr.removeLastLost[%d] Consume failed %s", userId, errMsg)
			return false
		}
	}

	removeOk := ur.removeLastLost(gameId)
	if !removeOk {
		// 道具加回去
		if price.Count > 0 {
			inventory.AddItems(userId, []item.ItemPack{price}, "CONSECUTIVE_CARD return", common.LOGTYPE_LADDER_CONSECUTIVE_CARD)
		}
	}

	return removeOk
}

func (cm *consecutiveMgr) dump(param string) {
	log.Release("-------------------------------")
	log.Release("consecutiveMgr.dump")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	var userId int
	var err error
	if userId, err = strconv.Atoi(param); err != nil {
		log.Release("atoi error %v", err)
		return
	}

	ur := cm.getUserRecord(userId)
	if ur == nil {
		log.Release("  no record")
		return
	}

	ur.dump()
}
