package ladder

import (
	"bet24.com/database"
	"bet24.com/log"
	"bet24.com/servers/common"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/ladderservice/proto"
	"encoding/json"
	_ "runtime/debug"
)

func getUserConsecutiveRecord(userId int) string {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetProcName("WS_LadderService_GetConsecutiveRecord")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	retRows := dbengine.ExecuteRs(statement.GenSql())
	rowLen := len(retRows)
	if rowLen <= 0 {
		return ""
	}
	return retRows[0][0].(string)
}

func setUserConsecutiveRecord(userId int, record string) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetProcName("WS_LadderService_SetConsecutiveRecord")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Record", database.AdParamInput, database.AdVarChar, 1024, record)
	lock := getLadderManager().getUserLock(userId)
	if lock != nil {
		lock.Lock()
		defer lock.Unlock()
	}
	dbengine.Execute(statement.GenSql())
}

func getUserLadderInfo(userId int) pb.UserLadderInfo {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetProcName("WS_LadderService_GetUserLadderInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	jsonData := dbengine.Execute(statement.GenSql())
	var ret []pb.UserLadderInfo
	err := json.Unmarshal([]byte(jsonData), &ret)
	if err != nil || len(ret) == 0 {
		log.Release("getUserLadderInfo failed err = %s", jsonData)
		// 没有记录，初始化分数
		uli := pb.UserLadderInfo{}
		uli.Point = getLadderConfig().InitialPoint
		return uli
	}
	return ret[0]
}

func setUserLadderInfo(userId int, userLadderInfo pb.UserLadderInfo) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetProcName("WS_LadderService_SetUserLadderInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Point", database.AdParamInput, database.AdInteger, 4, userLadderInfo.Point)
	statement.AddParamter("@MaxConWin", database.AdParamInput, database.AdInteger, 4, userLadderInfo.MaxConWin)
	statement.AddParamter("@WinCount", database.AdParamInput, database.AdInteger, 4, userLadderInfo.WinCount)
	statement.AddParamter("@LoseCount", database.AdParamInput, database.AdInteger, 4, userLadderInfo.LoseCount)
	statement.AddParamter("@DrawCount", database.AdParamInput, database.AdInteger, 4, userLadderInfo.DrawCount)
	lock := getLadderManager().getUserLock(userId)
	if lock != nil {
		lock.Lock()
		defer lock.Unlock()
	}
	dbengine.Execute(statement.GenSql())
}

func getUserLadderSettlementRecord(userId int) []pb.WinningStreak {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_LadderService_GetUserSettlementRecord")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	//log.Debug(sqlstring)
	jsonData := dbengine.Execute(sqlString)

	var out []pb.WinningStreak
	if err := json.Unmarshal([]byte(jsonData), &out); err != nil {
		log.Error("transaction.getUserLadderSettlementRecord json unmarshal UserID=%d err %v", userId, err)
	}
	return out
}

func setUserLadderSettlementRecord(userId, point, winCount, winType int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_LadderService_SetUserSettlementRecord")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@Point", database.AdParamInput, database.AdInteger, 4, point)
	statement.AddParamter("@WinCount", database.AdParamInput, database.AdInteger, 4, winCount)
	statement.AddParamter("@WinType", database.AdParamInput, database.AdInteger, 4, winType)
	statement.AddParamter("@Crdate", database.AdParamInput, database.AdVarChar, 20, common.GetNowTimeStr())
	lock := getLadderManager().getUserLock(userId)
	if lock != nil {
		lock.Lock()
		defer lock.Unlock()
	}
	dbengine.Execute(statement.GenSql())
}
