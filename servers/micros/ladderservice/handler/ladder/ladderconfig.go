package ladder

import (
	"encoding/json"
	"os"
	"time"

	"bet24.com/log"
	item "bet24.com/servers/micros/item_inventory/proto"
	pb "bet24.com/servers/micros/ladderservice/proto"
	platformconfig "bet24.com/servers/micros/platformconfig/proto"
)

const config_key = "ladderservice_config"

var ladderConf *ladderconfig

func getLadderConfig() *ladderconfig {
	if ladderConf == nil {
		ladderConf = new(ladderconfig)
		ladderConf.ctor()
	}
	return ladderConf
}

type ladderconfig struct {
	ConsecutiveWinPoints []pb.ConsecutiveWinPoint // 连胜额外点数配置
	WinPoint             int                      // 赢点数
	LosePoint            int                      // 输点数
	ConsecutiveCardPrice item.ItemPack            // 连胜卡配置
	LadderConfigs        []pb.LadderConfig        // 段位详细配置
	InitialPoint         int                      // 初始点数

	lastConfigString string
}

func (lc *ladderconfig) ctor() {
	log.Debug("ladder.ladderconfig.ctor()")
	lc.loadConfig()
}

func (lc *ladderconfig) loadConfig() {
	time.AfterFunc(10*time.Minute, lc.loadConfig)
	configString := platformconfig.GetConfig(config_key)

	if configString == "" {
		data, err := os.ReadFile("serviceconf/ladderservice_config.json")
		if err != nil {
			log.Release("ladderconfig.loadConfig.loadData read json failed")
			return
		}
		configString = string(data)
		platformconfig.SetConfig(config_key, configString)
	}
	if configString == lc.lastConfigString {
		return
	}
	lc.lastConfigString = configString

	err := json.Unmarshal([]byte(configString), &lc)
	if err != nil {
		log.Release("ladderconfig.loadConfig Unmarshal config [%s] err:%v", configString, err)
		return
	}
}

func (lc *ladderconfig) dump() {
	log.Release("-------------------------------")
	log.Release("ladderconfig.dump")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	log.Release(lc.lastConfigString)
}

func (lc *ladderconfig) getConsecutiveWinAdditionalPoint(count int) int {
	configCount := len(lc.ConsecutiveWinPoints)
	if configCount == 0 {
		return 0
	}
	if count >= lc.ConsecutiveWinPoints[configCount-1].Count {
		return lc.ConsecutiveWinPoints[configCount-1].AddictionalPoint
	}
	for _, v := range lc.ConsecutiveWinPoints {
		if v.Count == count {
			return v.AddictionalPoint
		}
	}

	return 0
}

func (lc *ladderconfig) getMaxConsecutiveWinCount() int {
	count := len(lc.ConsecutiveWinPoints)
	if count == 0 {
		log.Release("ladderconfig.getMaxConsecutiveWinCount no config")
		return 0
	}

	return lc.ConsecutiveWinPoints[count-1].Count
}

func (lc *ladderconfig) getLadderInfo() string {
	return lc.lastConfigString
}
