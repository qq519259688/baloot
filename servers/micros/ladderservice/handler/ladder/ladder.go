package ladder

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/ladderservice/proto"
)

func Run() {
	getLadderConfig()
	getLadderManager()
	getConsecutiveMgr()
}

func Dump(cmd, param1 string) {
	switch cmd {
	case "config":
		getLadderConfig().dump()
		return
	case "con":
		getConsecutiveMgr().dump(param1)
		return
	case "user":
		getLadderManager().dumpUser(param1)
	case "room":
		getRoomManager().dump()
	case "cal":
		getLadderManager().dumpCal(param1)
	default:
		log.Release("ladder.Dump unhandled cmd %s", cmd)
		break
	}

}

func GetSystemLadderInfo() string {
	return getLadderConfig().getLadderInfo()
}

func GetUserConsecutiveWinCount(userId int, gameId int) int {
	if userId <= 0 {
		log.Release("ladder.GetUserConsecutiveWinCount userId == %d", userId)
		return 0
	}
	return getConsecutiveMgr().getUserConsecutiveWinCount(userId, gameId)
}

func GetUserLadderInfo(userId int) *pb.UserLadderInfo {
	if userId <= 0 {
		log.Release("ladder.GetUserLadderInfo userId == %d", userId)
		return nil
	}
	return getLadderManager().getUserLadderInfo(userId)
}

func AddUserScore(userId int, gameId int, score int, roomName string) (int, bool) {
	if userId <= 0 {
		log.Release("ladder.AddUserScore userId == %d", userId)
		return 0, false
	}
	return getLadderManager().addUserScore(userId, gameId, score, roomName)
}

func UseConsecutiveCard(userId int, gameId int) bool {
	if userId <= 0 {
		log.Release("ladder.UseConsecutiveCard userId == %d", userId)
		return false
	}

	if getConsecutiveMgr().removeLastLost(userId, gameId) {
		getLadderManager().refreshCurConWin(userId)
		return true
	}
	return false
}

func RegisterLadderRoom(roomInfo pb.GameRoomInfo) {
	getRoomManager().addRoom(roomInfo)
}

func DeregisterLadderRoom(serverAddr string) {
	log.Debug("DeregisterLadderRoom %s", serverAddr)
	getRoomManager().removeRooms(serverAddr)
}

func GetLadderRoomList() string {
	return getRoomManager().getRoomList()
}

func GetSettlementRecord(userId int) pb.SettlementRecord {
	return getLadderManager().getSettlementRecord(userId)
}

func GetHistoricalRecord(userId int) []pb.WinningStreak {
	return getLadderManager().getHistoricalRecord(userId)
}

func GetLadderRoomAdditionalPercent(gameId int, ruleName string) int {
	return getRoomManager().getRoomAdditionalPercent(gameId, ruleName)
}
