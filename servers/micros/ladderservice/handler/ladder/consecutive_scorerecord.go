package ladder

import (
	"bet24.com/log"
	"time"
)

const max_score_record = 30

type ScoreRecord struct {
	Score   int   `json:"s"`
	Removed bool  `json:"r,omitempty"` // 使用连胜卡移除
	Time    int64 `json:"t"`
}

type consecutiveRecord struct {
	GameId int           `json:"g"`
	Scores []ScoreRecord `json:"ss"`
}

func newConsecutiveRecord(gameId int) *consecutiveRecord {
	ret := new(consecutiveRecord)
	ret.GameId = gameId
	return ret
}

func (cr *consecutiveRecord) getConsecutiveWinCount() int {
	ret := 0
	for i := 0; i < len(cr.Scores); i++ {
		if cr.Scores[i].Score < 0 && !cr.Scores[i].Removed {
			break
		}
		if cr.Scores[i].Score > 0 {
			ret++
		}
	}
	return ret
}

func (cr *consecutiveRecord) addRecord(score int) {
	cr.Scores = append([]ScoreRecord{{Score: score, Time: time.Now().Unix()}}, cr.Scores...)
	// 处理溢出数据
	if len(cr.Scores) >= max_score_record {
		cr.Scores = cr.Scores[:max_score_record]
	}
}

func (cr *consecutiveRecord) removeLastLost() bool {
	if len(cr.Scores) == 0 {
		log.Release("consecutiveRecord.removeLastLost failed, no record")
		return false
	}

	if cr.Scores[0].Removed {
		log.Release("consecutiveRecord.removeLastLost failed, already removed")
		return false
	}

	if cr.Scores[0].Score >= 0 {
		log.Release("consecutiveRecord.removeLastLost failed, not lost")
		return false
	}

	cr.Scores[0].Removed = true
	return true
}
