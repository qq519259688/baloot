package ladder

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/ladderservice/proto"
	"sort"
	"sync"
	"time"
)

const ladderuser_expire_seconds = 1800

type ladderUser struct {
	userId int
	pb.UserLadderInfo
	pb.SettlementRecord
	ping     int64
	userLock *sync.RWMutex
}

func newLadderUser(userId int) *ladderUser {
	ret := &ladderUser{userId: userId}
	ret.UserLadderInfo = getUserLadderInfo(userId)
	ret.UserLadderInfo.Records = getUserLadderSettlementRecord(userId)
	ret.LadderInfo = getLadderInfoByPoint(ret.Point)
	ret.CurConWin = getConsecutiveMgr().getUserConsecutiveWinCount(userId, 0)
	ret.SettlementRecord.Records = nil
	ret.userLock = &sync.RWMutex{}
	ret.refreshPing()
	return ret
}

func (lu *ladderUser) refreshPing() {
	lu.ping = time.Now().Unix()
}

func (lu *ladderUser) isExpired() bool {
	return time.Now().Unix()-lu.ping > ladderuser_expire_seconds
}

func (lu *ladderUser) dump() {
	log.Release("    UserId[%d]MC[%d]W[%d]L[%d]D[%d]", lu.userId, lu.MaxConWin, lu.WinCount, lu.LoseCount, lu.DrawCount)
	log.Release("              Point[%d]Ld[%d]Lv[%d]St[%d]", lu.Point, lu.Ladder, lu.Level, lu.Star)
	log.Release("              SettlementRecord[%d]Ld[%d]Lv[%d]St[%d]", lu.Point, lu.Ladder, lu.Level, lu.Star)
	log.Release("    TotalPoint[%d] WinNumber[%d] WinPoint[%d] LoseNumber[%d] LosePoint[%d] AdditionalPercent[%d]",
		lu.TotalPoint, lu.WinNumber, lu.WinPoint, lu.LoseNumber, lu.LosePoint, lu.AdditionalPercent)
	log.Release("    Records[%v]", lu.SettlementRecord.Records)

}

// 添加点数，返回是否升级或降级
func (lu *ladderUser) addPoint(point, gameId, winStreakScore, additionalPercent int) bool {
	if point == 0 {
		return false
	}
	lu.refreshPing()
	oldLadderInfo := lu.LadderInfo

	// 刷新局数
	if point == 0 {
		lu.DrawCount++
	} else if point > 0 {
		lu.WinCount++
		lu.Point += (winStreakScore + point) * (100 + additionalPercent) / 100
	} else {
		lu.LoseCount++
		lu.Point = lu.Point + point
	}

	lu.LadderInfo = getLadderInfoByPoint(lu.Point)
	if lu.LadderInfo.Ladder == 1 && lu.Point < 100 {
		lu.Point = 100
	}

	// 刷新最大连胜
	lu.CurConWin = getConsecutiveMgr().getUserConsecutiveWinCount(lu.userId, gameId)

	if lu.CurConWin > lu.MaxConWin {
		lu.MaxConWin = lu.CurConWin
	}

	go setUserLadderInfo(lu.userId, lu.UserLadderInfo)
	return lu.IsChanged(oldLadderInfo)
}

func (lu *ladderUser) refreshCurConWin() {
	lu.CurConWin = getConsecutiveMgr().getUserConsecutiveWinCount(lu.userId, 0)
}

// 获取用户结算记录
func (lu *ladderUser) getSettlementRecord() pb.SettlementRecord {
	defer lu.clearRecord()
	sort.SliceStable(lu.SettlementRecord.Records, func(i, j int) bool {
		return lu.SettlementRecord.Records[i].WinType > lu.SettlementRecord.Records[j].WinType
	})
	return lu.SettlementRecord
}

func (lu *ladderUser) clearRecord() {
	lu.refreshPing()
	lu.SettlementRecord = pb.SettlementRecord{}
}

func (lu *ladderUser) addRecord(point, winStreakScore, additionalPercent int) {
	lu.SettlementRecord.AdditionalPercent = additionalPercent
	if point > 0 {
		lu.SettlementRecord.WinNumber++
		lu.SettlementRecord.WinPoint += point
		lu.SettlementRecord.TotalPoint += (winStreakScore + point) * (100 + additionalPercent) / 100
	}
	if point < 0 {
		lu.SettlementRecord.LoseNumber++
		lu.SettlementRecord.LosePoint += point
		lu.SettlementRecord.TotalPoint = lu.SettlementRecord.TotalPoint + point
	}

	// 小于二连胜不进入记录
	if lu.CurConWin < 2 {
		return
	}

	defer lu.setHistoricalRecord(winStreakScore, lu.CurConWin)
	for k, v := range lu.SettlementRecord.Records {
		if v.WinType == lu.CurConWin {
			lu.SettlementRecord.Records[k].WinCount++
			lu.SettlementRecord.Records[k].Point *= lu.SettlementRecord.Records[k].WinCount
			return
		}
	}

	lu.SettlementRecord.Records = append(lu.SettlementRecord.Records, pb.WinningStreak{
		Point:    winStreakScore,
		WinCount: 1,
		WinType:  lu.CurConWin,
	})
}

func (lu *ladderUser) setHistoricalRecord(winStreakScore, winType int) {
	for k, v := range lu.UserLadderInfo.Records {
		if v.WinType == winType {
			lu.UserLadderInfo.Records[k].WinCount++
			lu.UserLadderInfo.Records[k].Point *= lu.UserLadderInfo.Records[k].WinCount
			setUserLadderSettlementRecord(lu.userId, lu.UserLadderInfo.Records[k].Point, lu.UserLadderInfo.Records[k].WinCount, winType)
			return
		}
	}
	lu.UserLadderInfo.Records = append(lu.UserLadderInfo.Records, pb.WinningStreak{
		Point:    winStreakScore,
		WinCount: 1,
		WinType:  winType,
	})
	setUserLadderSettlementRecord(lu.userId, winStreakScore, 1, winType)
}

func (lu *ladderUser) getHistoricalRecord() []pb.WinningStreak {
	lu.refreshPing()
	sort.SliceStable(lu.UserLadderInfo.Records, func(i, j int) bool {
		return lu.UserLadderInfo.Records[i].WinType > lu.UserLadderInfo.Records[j].WinType
	})
	return lu.UserLadderInfo.Records
}
