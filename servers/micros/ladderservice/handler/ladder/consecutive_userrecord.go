package ladder

import (
	"bet24.com/log"
	"bet24.com/servers/common"
	"encoding/json"
	"time"
)

const record_expire_seconds = 1800

type userConsecutiveRecord struct {
	UserId  int
	Records []*consecutiveRecord
	ping    int64
}

func newUserConsecutiveRecord(userId int) *userConsecutiveRecord {
	record := getUserConsecutiveRecord(userId)
	ur := new(userConsecutiveRecord)
	ur.UserId = userId
	ur.refreshPing()
	err := json.Unmarshal([]byte(record), &ur.Records)
	if err != nil {
		log.Release("newUserConsecutiveRecord[%d] Unmarshal failed %s", userId, record)
	}

	return ur
}

func (ucr *userConsecutiveRecord) refreshPing() {
	ucr.ping = time.Now().Unix()
}

func (ucr *userConsecutiveRecord) isExpired() bool {
	return time.Now().Unix()-ucr.ping > record_expire_seconds
}

func (ucr *userConsecutiveRecord) getConsecutiveWinCount(gameId int) int {
	ucr.refreshPing()
	for _, v := range ucr.Records {
		if v.GameId == gameId || gameId == 0 {
			return v.getConsecutiveWinCount()
		}
	}

	return 0
}

func (ucr *userConsecutiveRecord) addRecord(score int, gameId int) {
	ucr.refreshPing()
	for _, v := range ucr.Records {
		if v.GameId == gameId || gameId == 0 {
			v.addRecord(score)
			// 写入数据库
			go ucr.updateRecordToDB()
			return
		}
	}
	// 没找到
	record := newConsecutiveRecord(gameId)
	record.addRecord(score)
	ucr.Records = append(ucr.Records, record)

	// 写入数据库
	go ucr.updateRecordToDB()
}

func (ucr *userConsecutiveRecord) getRecordByGameId(gameId int) *consecutiveRecord {
	for k, v := range ucr.Records {
		if v.GameId == gameId || gameId == 0 {
			return ucr.Records[k]
		}
	}
	log.Debug("ucr.userConsecutiveRecord.getRecordByGameId(%d) userId[%d] not found", gameId, ucr.UserId)
	return nil
}

// 移除最后一局输的
func (ucr *userConsecutiveRecord) removeLastLost(gameId int) bool {
	ucr.refreshPing()
	r := ucr.getRecordByGameId(gameId)
	if r == nil {
		log.Release("userConsecutiveRecord.removeLastLost UserId [%d] gameId[%d] not found", ucr.UserId, gameId)
		return false
	}
	if r.removeLastLost() {
		// 写入数据库
		go ucr.updateRecordToDB()
		return true
	}
	return false
}

func (ucr *userConsecutiveRecord) updateRecordToDB() {
	d, _ := json.Marshal(ucr.Records)
	setUserConsecutiveRecord(ucr.UserId, string(d))
}

func (ucr *userConsecutiveRecord) dump() {
	for _, v := range ucr.Records {
		log.Release("   GameId:%d", v.GameId)
		for _, v1 := range v.Scores {
			log.Release("        Score[%d]Removed[%v]Time[%s]", v1.Score, v1.Removed, common.TimeStampToShortString(v1.Time))
		}
	}
}
