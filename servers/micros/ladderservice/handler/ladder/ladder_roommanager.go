package ladder

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/ladderservice/proto"
	"encoding/json"
)

var roomMgr *roomManager

func getRoomManager() *roomManager {
	if roomMgr == nil {
		roomMgr = new(roomManager)
	}
	return roomMgr
}

type roomManager struct {
	roomList []pb.GameRoomInfo
}

func (rm *roomManager) dump() {
	log.Release("-------------------------------")
	log.Release("roomManager.dump")
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()
	for _, v := range rm.roomList {
		log.Release("  RoomName[%s]BS[%d]MinMax[%d-%d]Add[%d]Server[%s]", v.RoomName, v.BaseScore, v.MinGold, v.MaxGold, v.AdditionalPercent, v.ServerAddr)
	}
}

func (rm *roomManager) addRoom(room pb.GameRoomInfo) {
	for k, v := range rm.roomList {
		if v.GameId == room.GameId && v.RoomName == room.RoomName {
			rm.roomList[k] = room
			return
		}
	}
	rm.roomList = append(rm.roomList, room)
}

func (rm *roomManager) removeRooms(serverAddr string) {
	for k := 0; k < len(rm.roomList); {
		if rm.roomList[k].ServerAddr == serverAddr {
			rm.roomList = append(rm.roomList[:k], rm.roomList[k+1:]...)
		} else {
			k++
		}
	}
}

func (rm *roomManager) getRoomAdditionalPercent(gameId int, roomName string) int {
	for _, v := range rm.roomList {
		if v.GameId == gameId && v.RoomName == roomName {
			return v.AdditionalPercent
		}
	}
	log.Release("roomManager.getRoomAdditionalPercent gameId[%d] roomName[%s] not found", gameId, roomName)
	return 0
}

func (rm *roomManager) getRoomList() string {
	d, _ := json.Marshal(rm.roomList)
	return string(d)
}
