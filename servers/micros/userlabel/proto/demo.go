package proto

/* 调用样例（demo）
// 1=活跃时长(大厅模块)(已植入)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_ActiveTime,
	Param:      "",
	Value:      20, // 在线时长
	TotalValue: 0,
})

// 2=付费金额(充值模块)(已植入)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_ChargeAmount,
	Param:      "",
	Value:      30, // 充值金额
	TotalValue: 0,
})

// 3 = 游戏喜好(游戏模块)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_GamePreferences,
	Param:      "37", // 游戏ID
	Value:      200,  // 游戏时长
	TotalValue: 0,
})

// 4=好友房(游戏模块)(已植入)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_FriendRoom,
	Param:      "",
	Value:      80, // 好友局数
	TotalValue: 0,
})

// 5=互动类(语聊房模块)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_Interactive,
	Param:      "",
	Value:      50000, // 互动道具和礼物价值金币数
	TotalValue: 0,
})

// 6=任务类(任务模块)(已植入)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_Task,
	Param:      "",
	Value:      8, // 任务数
	TotalValue: 0,
})

// 7=聊天类(聊天模块)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_Chat,
	Param:      "",
	Value:      57, // 聊天信息条数
	TotalValue: 0,
})

// 8=交际类(好友模块)(已植入)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_Communication,
	Param:      "",
	Value:      23, // 好友数量
	TotalValue: 0, // 好友数量
})

// 9=IP危险(注册模块)(已植入)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_IpDanger,
	Param:      "183.17.230.102", // IP地址
	Value:      0,
	TotalValue: 0,
})

// 10=IMei危险(注册模块)(已植入)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_IMeiDanger,
	Param:      "47f6306f-a9f6-f022-17a7-7eef2eb20d28", // 设备号(deviceid)
	Value:      0,
	TotalValue: 0,
})

// 11=输金危险(游戏模块、充值模块)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_LoseDanger,
	Param:      "",
	Value:      5000000, // 输金币数
	TotalValue: 0,       // 充值金额(美元)
})

// 12=作弊危险(游戏模块)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_CheatingDanger,
	Param:      "",
	Value:      1000, // 好友房赢金币数
	TotalValue: 1000, // 所有房间赢金币数（好友场、比赛场等等）
})

// 13=潜在付费(充值模块)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_PotentialCharge,
	Param:      "",
	Value:      5,   // 点击商城次数
	TotalValue: 100, // 充值金额
})

// 14=被动付费(充值模块)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_PassiveCharge,
	Param:      "",
	Value:      5, // 破产或者濒临破产时充值次数
	TotalValue: 5, // 总充值次数
})

// 15=储蓄付费(充值模块)(已植入)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_SavingsCharge,
	Param:      "",
	Value:      500000, // 充值时剩余金币
	TotalValue: 5,      // 充值金额
})

// 16=折扣付费(充值模块,主要用于礼包购买)
PostTest("triggerEvent", struct {
	TypeId     int    // 类型ID
	Param      string // 参数
	Value      int    // 值
	TotalValue int    // 累计值
}{
	TypeId:     userlabel.Type_DiscountCharge,
	Param:      "",
	Value:      1,   // 充值礼包次数
	TotalValue: 100, // 充值次数
})
*/
