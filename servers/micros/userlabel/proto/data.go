package proto

const (
	_               = iota // 0=无效
	Type_Login             // 1=登录
	Type_Charge            // 2=充值
	Type_Game              // 3=游戏
	Type_Task              // 4=任务
	Type_Chat              // 5=聊天
	Type_AudioRoom         // 6=语聊房
	Type_Friend            // 7=好友
	Type_Bankruptcy        // 8=破产
)

// 标签范围
type Scope struct {
	GameId        int     `json:",omitempty"` // 游戏id
	IsFriendRoom  bool    `json:",omitempty"` // 是否好友房
	OnlineSeconds int     `json:",omitempty"` // 在线时长
	GoldAmount    int     `json:",omitempty"` // 金币数量
	PayPrice      float64 `json:",omitempty"` // 充值金额(美元)
	IsDiscount    bool    `json:",omitempty"` // 是否折扣
	Num           int     `json:",omitempty"` // 数量
	IPAddress     string  `json:",omitempty"` // IP地址
	IMei          string  `json:",omitempty"` // IMei
	UTMSource     string  `json:",omitempty"` // 流量渠道
	IsRegister    bool    `json:",omitempty"` // 是否注册
	IsReturn      bool    `json:",omitempty"` // 是否回归
}

// LabelConfig 标签
type LabelConfig struct {
	TypeId   int    `json:"TypeId"`         // 类别ID
	TypeName string `json:"TypeName"`       // 类别名称
	Color    string `json:"Color"`          // 颜色
	Days     int    `json:"Days,omitempty"` // 天数
	Content  []struct {
		LabelId    string `json:"LabelId"`              // 标签Id
		LabelName  string `json:"LabelName"`            // 标签名称
		Memo       string `json:"Memo"`                 // 描述
		Min        int    `json:"Min,omitempty"`        // 最低值
		Max        int    `json:"Max,omitempty"`        // 最高值
		Percentage int    `json:"Percentage,omitempty"` // 百分比
		Ext        int    `json:"Ext,omitempty"`        // 扩展参数
		PoolValue  int    `json:"PoolValue,omitempty"`  // 奖池金额
	} `json:"Content"` // 具体内容
}

// 标签信息
type LabelInfo struct {
	TypeId     int        `json:"TypeId,omitempty"`     // 类型ID
	LabelId    string     `json:"LabelId,omitempty"`    // 标签Id
	TotalValue int        `json:"TotalValue,omitempty"` // 累计数值
	Days       []*DayInfo `json:"Days,omitempty"`       // 每天数据
}

// 日信息
type DayInfo struct {
	Index    int    `json:"Index,omitempty"`    // 日期索引
	Param    string `json:"Param,omitempty"`    // 参数
	Value    int    `json:"Value,omitempty"`    // 数值
	ExtValue int    `json:"ExtValue,omitempty"` // 扩展数值
}

// 计算标签结果
type CalResult struct {
	Success   bool   // 操作结果
	LabelId   string // 标签id
	LabelName string // 标签名称
	PoolValue int    // 奖池
	Days      int    // 天数
}

// ConfigSimpleInfo 标签
type ConfigSimpleInfo struct {
	TypeId    int    `json:"TypeId"`    // 类别ID
	TypeName  string `json:"TypeName"`  // 类别名称
	Color     string `json:"Color"`     // 颜色
	LabelId   string `json:"LabelId"`   // 标签Id
	LabelName string `json:"LabelName"` // 标签名称
}
