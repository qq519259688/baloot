package proto

import (
	"context"
	"encoding/json"

	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"github.com/smallnest/rpcx/client"
)

const ServiceName = "userlabel"

var consulAddr = common.Default_Consul_Addr

func getClient() client.XClient {
	return common.GetClientPool().GetClient(ServiceName, consulAddr)
}

type Request struct {
	UserId    int    `json:",omitempty"`
	Name      string `json:",omitempty"`
	IpAddress string `json:",omitempty"`
}

type Response struct {
	RetCode int
	Data    string
}

func SetConsulAddr(addr string) {
	consulAddr = addr
}

func SayHello(name string) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{
		Name: name,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SayHello", args, reply)
	if err != nil {
		log.Debug("userLabel failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
		return ""
	}

	log.Debug("userLabel return %s", reply.Data)
	return reply.Data
}

type Request_GetLabel struct {
	UserId int // 用户id
	TypeId int // 类型id
}

type Response_GetLabel struct {
	Labels []LabelColor
}

type LabelColor struct {
	TypeId    int    // 类型id
	TypeName  string // 类型名称
	LabelId   string // 标签ID
	LabelName string // 标签名称
	Color     string // 色值
	PoolValue int    // 奖池金额
}

type Request_TriggerEvent struct {
	UserId int // 用户ID
	TypeId int // 类型ID
	Scope      // 范围
}

// 用户标签触发事件
func TriggerEvent(userId, typeId int, scope Scope) {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_TriggerEvent{
		UserId: userId,
		TypeId: typeId,
		Scope:  scope,
	}

	err := xclient.Call(context.Background(), "TriggerEvent", args, nil)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("userLabel failed to call: %v", err)
	}

	return
}

// 获取标签列表
func GetLabel(userId, typeId int) []LabelColor {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GetLabel{
		UserId: userId,
		TypeId: typeId,
	}

	reply := &Response_GetLabel{}

	err := xclient.Call(context.Background(), "GetLabel", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("userLabel failed to call: %v", err)
	}

	return reply.Labels
}

type LabelUser struct {
	UserId     int    // 用户id
	NickName   string // 昵称
	TypeId     int    // 类型id
	TypeName   string // 类型名称
	LabelId    string // 标签ID
	LabelName  string // 标签名称
	UpdateTime string // 更新时间
}

type Request_GetListByLabelId struct {
	LabelId   int
	PageIndex int
	PageSize  int
}

type Response_GetListByLabelId struct {
	RecordCount int
	List        []LabelUser
}

// 获取标签列表
func GetListByLabelId(labelId, pageIndex, pageSize int) string {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request_GetListByLabelId{
		LabelId:   labelId,
		PageIndex: pageIndex,
		PageSize:  pageSize,
	}

	reply := &Response_GetListByLabelId{}

	err := xclient.Call(context.Background(), "GetListByLabelId", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("userLabel failed to call: %v", err)
	}

	buf, _ := json.Marshal(reply)
	return string(buf)
}

type Response_GetConfigSimpleInfo struct {
	List []ConfigSimpleInfo
}

// 获取标签配置简单信息
func GetConfigSimpleInfo() []ConfigSimpleInfo {
	xclient := getClient()
	//defer xclient.Close()

	args := &Request{}

	reply := &Response_GetConfigSimpleInfo{}

	err := xclient.Call(context.Background(), "GetConfigSimpleInfo", args, reply)
	if err != nil {
		common.GetClientPool().RemoveClient(ServiceName)
		log.Debug("userLabel failed to call: %v", err)
	}

	return reply.List
}
