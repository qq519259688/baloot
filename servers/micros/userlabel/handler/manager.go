package handler

import (
	"sort"
	"strconv"
	"sync"
	"time"

	"bet24.com/servers/common"

	"bet24.com/log"
	pb "bet24.com/servers/micros/userlabel/proto"
)

var mgr *labelMgr

type labelMgr struct {
	config_list []*pb.LabelConfig // 标签配置
	user_list   map[int]*userInfo // 用户列表
	lock        *sync.RWMutex     // 用户锁
}

func newLabelMgr() {
	mgr = new(labelMgr)
	mgr.lock = &sync.RWMutex{}
	mgr.user_list = make(map[int]*userInfo)
	mgr.loadConfig()
	mgr.doCheck()
	return
}

// 轮询
func (this *labelMgr) doCheck() {
	ticker := time.NewTicker(2 * time.Minute)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				this.checkExpire()
			}
		}
	}(ticker)
}

// 检查用户
func (this *labelMgr) checkExpire() {
	this.lock.Lock()
	defer this.lock.Unlock()

	// 遍历所有用户
	for _, u := range this.user_list {
		// 判断是否过期
		if !u.isExpire() {
			continue
		}

		// 删除
		delete(this.user_list, u.userId)
		log.Debug("manager.checkExpire userId=%d 已完成清理", u.userId)
	}

	return
}

// 获取用户
func (this *labelMgr) getUser(userId int) *userInfo {
	this.lock.RLock()
	u, ok := this.user_list[userId]
	this.lock.RUnlock()
	if ok {
		u.updateTimeStamp()
		return u
	}

	u = newUserInfo(userId)
	this.lock.Lock()
	this.user_list[userId] = u
	this.lock.Unlock()
	return u
}

// 获取标签列表
func (this *labelMgr) getLabel(userId, typeId int) []pb.LabelColor {
	u := this.getUser(userId)
	if u == nil {
		log.Debug("manager.getLabel userId=%d", userId)
		return nil
	}

	var ret []pb.LabelColor
	for _, v := range u.getLabelList() {
		if typeId > 0 && v.TypeId != typeId {
			continue
		}

		cfg := this.getConfigInfo(v.TypeId)
		if cfg == nil {
			log.Error("manager.getLabel userId=%d typeId=%d", userId, v.TypeId)
			continue
		}

		labelName, poolValue := this.getLabelName(v.TypeId, v.LabelId)
		if labelName == "" {
			labelName = v.LabelId
		}

		ret = append(ret, pb.LabelColor{
			TypeId:    v.TypeId,
			TypeName:  cfg.TypeName,
			LabelId:   v.LabelId,
			LabelName: labelName,
			Color:     cfg.Color,
			PoolValue: poolValue,
		})
	}

	sort.SliceStable(ret, func(i, j int) bool {
		return ret[i].TypeId < ret[j].TypeId
	})

	return ret
}

// 触发事件
func (this *labelMgr) triggerEvent(userId, typeId int, scope pb.Scope) {
	if userId <= 0 {
		log.Debug("manager.triggerEvent userId=%d typeId=%d scope=%+v", userId, typeId, scope)
		return
	}

	log.Debug("manager.triggerEvent userId=%d typeId=%d scope=%+v", userId, typeId, scope)

	u := this.getUser(userId)
	if u == nil {
		log.Debug("manager.triggerCounter userId=%d typeId=%d scope=%+v", userId, typeId, scope)
		return
	}

	// 触发事件
	u.dispatch(typeId, scope)
}

// 用户标签列表
func (this *labelMgr) GetListByLabelId(labelId, pageIndex, pageSize int) (int, []pb.LabelUser) {
	recordCount, list := trans_GetListByLabelId(labelId, pageIndex, pageSize)
	for i := 0; i < len(list); i++ {
		cfg := this.getConfigInfo(list[i].TypeId)
		if cfg == nil {
			continue
		}

		labelName, _ := this.getLabelName(list[i].TypeId, list[i].LabelId)
		if labelName == "" {
			labelName = list[i].LabelId
		}

		list[i].TypeName = cfg.TypeName
		list[i].LabelName = labelName
	}
	return recordCount, list
}

// 打印用户
func (this *labelMgr) dumpUser(param1 string) {
	userId := 0
	if param1 != "" {
		userId, _ = strconv.Atoi(param1)
	}

	log.Debug(" ^_^ 开始打印用户数据，用户(%d)人", len(this.user_list))

	this.lock.RLock()
	for uid, v := range this.user_list {
		if userId > 0 && userId != uid {
			continue
		}

		log.Debug("用户[%d]信息,过期时间[%s],标签数据(%d)个:", uid, time.Unix(int64(v.timeStamp), 0).Format(common.Layout), len(v.label_list))
		for _, labelInfo := range v.label_list {
			log.Debug("TypeId=%d LabelId=%s TotalValue=%d 共(%d)条数据", labelInfo.TypeId, labelInfo.LabelId, labelInfo.TotalValue, len(labelInfo.Days))
			for _, dayInfo := range labelInfo.Days {
				log.Debug("	Index=%d Param=%s value=%d extValue=%d", dayInfo.Index, dayInfo.Param, dayInfo.Value, dayInfo.ExtValue)
			}
		}
		log.Debug("++++++++++++++++++++++++++++++++++++++++++++++")

	}
	this.lock.RUnlock()

	log.Debug("完成用户数据打印 ^_^")
}

// 打印配置
func (this *labelMgr) dumpConfig() {

	log.Debug(" ^_^ 开始打印配置数据，共(%d)条记录", len(this.config_list))

	this.lock.RLock()
	for _, v := range this.config_list {
		log.Debug("配置信息:%+v", v)
		for _, c := range v.Content {
			log.Debug("	%+v", c)
		}
		log.Debug("++++++++++++++++++++++++++++++++++++++++++++++")

	}
	this.lock.RUnlock()

	log.Debug("完成配置数据打印 ^_^")
}
