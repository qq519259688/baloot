package handler

import (
	"context"
	"fmt"

	"bet24.com/log"
	pb "bet24.com/servers/micros/userlabel/proto"
)

var instance *userLabel

type userLabel struct {
}

func GetInstance() *userLabel {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func newHandler() *userLabel {
	ret := new(userLabel)
	ret.ctor()
	return ret
}

func (g *userLabel) ctor() {
	newLabelMgr()
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func (g *userLabel) dump(cmd, param1, param2 string) {
	switch cmd {
	case "config":
		mgr.dumpConfig()
	case "user":
		mgr.dumpUser(param1)
	default:
		log.Release("userLabel.Dump unhandled cmd %s", cmd)
	}
}

func (g *userLabel) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

// 获取标签列表
func (g *userLabel) GetLabel(ctx context.Context, req *pb.Request_GetLabel, rsp *pb.Response_GetLabel) error {
	rsp.Labels = mgr.getLabel(req.UserId, req.TypeId)
	return nil
}

// 触发事件
func (g *userLabel) TriggerEvent(ctx context.Context, req *pb.Request_TriggerEvent, rsp *pb.Response) error {
	mgr.triggerEvent(req.UserId, req.TypeId, req.Scope)
	return nil
}

// 获取标签列表
func (g *userLabel) GetListByLabelId(ctx context.Context, req *pb.Request_GetListByLabelId, rsp *pb.Response_GetListByLabelId) error {
	rsp.RecordCount, rsp.List = mgr.GetListByLabelId(req.LabelId, req.PageIndex, req.PageSize)
	return nil
}

// 获取标签配置简单信息
func (g *userLabel) GetConfigSimpleInfo(ctx context.Context, req *pb.Request, rsp *pb.Response_GetConfigSimpleInfo) error {
	rsp.List = mgr.getConfigSimpleInfo()
	return nil
}
