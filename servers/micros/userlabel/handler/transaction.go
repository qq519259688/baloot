package handler

import (
	"encoding/json"
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/userlabel/proto"
)

// 保存
func trans_Save(userId, days int, info *pb.LabelInfo) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	buf, _ := json.Marshal(info.Days)

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserLabel_Save")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@TypeID", database.AdParamInput, database.AdInteger, 4, info.TypeId)
	statement.AddParamter("@LabelID", database.AdParamInput, database.AdVarChar, 32, info.LabelId)
	statement.AddParamter("@TotalValue", database.AdParamInput, database.AdInteger, 4, info.TotalValue)
	statement.AddParamter("@JsonData", database.AdParamInput, database.AdVarChar, 4096, string(buf))
	statement.AddParamter("@Days", database.AdParamInput, database.AdInteger, 4, days)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	dbengine.Execute(sqlString)
}

// 获取
func trans_GetInfo(userId int) []pb.LabelInfo {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserLabel_GetInfo")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, userId)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []pb.LabelInfo
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		var info pb.LabelInfo
		info.TypeId = int(ret[0].(int64))
		info.LabelId = ret[1].(string)
		info.TotalValue = int(ret[2].(int64))
		jsonData := ret[3].(string)
		if err := json.Unmarshal([]byte(jsonData), &info.Days); err != nil {
			log.Error("transaction.trans_GetInfo json unmarshal err %v", err)
		}
		list = append(list, info)
	}

	return list
}

// 获取IP、IMei用户
func trans_GetIPAndIMei(ipAddress, iMei string, days int) []int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_GetIPAndIMei")
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, ipAddress)
	statement.AddParamter("@IMei", database.AdParamInput, database.AdVarChar, 64, iMei)
	statement.AddParamter("@Days", database.AdParamInput, database.AdInteger, 4, days)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return nil
	}

	var list []int
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		list = append(list, int(ret[0].(int64)))
	}
	return list
}

// 获取标签列表
func trans_GetListByLabelId(labelId, pageIndex, pageSize int) (int, []pb.LabelUser) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var (
		recordCount int
		list        []pb.LabelUser
	)

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Manage_UserLabel_GetListByLabelId")
	statement.AddParamter("@LabelID", database.AdParamInput, database.AdInteger, 4, labelId)
	statement.AddParamter("@PageIndex", database.AdParamInput, database.AdInteger, 4, pageIndex)
	statement.AddParamter("@PageSize", database.AdParamInput, database.AdInteger, 4, pageSize)
	statement.AddParamter("@RecordCount", database.AdParamOutput, database.AdInteger, 4, recordCount)
	sqlString := statement.GenSql()
	//log.Debug(sqlString)
	retRows := dbengine.ExecuteRs(sqlString)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return 0, nil
	}

	if rowLen > 1 {
		for i := 0; i < rowLen-1; i++ {
			ret := retRows[i]
			var info pb.LabelUser
			info.UserId = int(ret[0].(int64))
			info.NickName = ret[1].(string)
			info.TypeId = int(ret[2].(int64))
			info.UpdateTime = ret[3].(string)
			list = append(list, info)
		}
	}

	recordCount = int(retRows[rowLen-1][0].(int64))
	return recordCount, list
}
