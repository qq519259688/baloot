package handler

import (
	"encoding/json"
	"os"
	"time"

	game "bet24.com/servers/micros/game/proto"

	platformconfig "bet24.com/servers/micros/platformconfig/proto"
	pb "bet24.com/servers/micros/userlabel/proto"

	"bet24.com/log"
)

const config_key = "userlabel_config"

func (this *labelMgr) loadConfig() {
	this.loadRedisConfig()
	go time.AfterFunc(5*time.Minute, this.loadConfig)
}

func (this *labelMgr) loadRedisConfig() {
	if data := platformconfig.GetConfig(config_key); data != "" {
		err := json.Unmarshal([]byte(data), &this.config_list)
		if err == nil {
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
		return
	}

	if data, err := os.ReadFile("serviceconf/userLabelConfig.json"); err == nil {
		err = json.Unmarshal([]byte(data), &this.config_list)
		if err == nil {
			platformconfig.SetConfig(config_key, string(data))
			return
		}
		log.Release("Unmarshal config [%s] err:%v", string(data), err)
	} else {
		log.Release("read config failed serviceconf/userLabelConfig.json %v", err)
	}

	return
}

// 获取标签配置列表
func (this *labelMgr) getConfigList() []*pb.LabelConfig {
	return this.config_list
}

// 获取标签配置
func (this *labelMgr) getConfigInfo(typeId int) *pb.LabelConfig {
	for _, v := range this.config_list {
		if v.TypeId == typeId {
			return v
		}
	}

	return nil
}

// 获取标签信息
func (this *labelMgr) getLabelName(typeId int, labelId string) (string, int) {
	cfg := this.getConfigInfo(typeId)
	if cfg == nil {
		log.Debug("getLabelName typeId=%d labelId=%s cfg is nil", typeId, labelId)
		return "", 0
	}

	for _, v := range cfg.Content {
		if v.LabelId == labelId {
			return v.LabelName, v.PoolValue
		}
	}

	return "", 0
}

// 获取标签配置简单信息
func (this *labelMgr) getConfigSimpleInfo() []pb.ConfigSimpleInfo {
	var list []pb.ConfigSimpleInfo
	for _, v := range this.config_list {
		for _, c := range v.Content {
			// 普通游戏
			if v.TypeId == pb.Type_Game && c.LabelId == "302" {
				for _, g := range game.GetGameList() {
					list = append(list, pb.ConfigSimpleInfo{
						TypeId:    v.TypeId,
						TypeName:  v.TypeName,
						Color:     v.Color,
						LabelId:   g.EnglishName,
						LabelName: g.EnglishName,
					})
				}
				continue
			}

			list = append(list, pb.ConfigSimpleInfo{
				TypeId:    v.TypeId,
				TypeName:  v.TypeName,
				Color:     v.Color,
				LabelId:   c.LabelId,
				LabelName: c.LabelName,
			})
		}
	}
	return list
}
