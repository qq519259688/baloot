package handler

import (
	"bet24.com/servers/micros/remotemessage/handler/remotemessage"
	pb "bet24.com/servers/micros/remotemessage/proto"
	"context"
	"fmt"
)

var instance *ServiceHandler

func GetInstance() *ServiceHandler {
	if instance == nil {
		instance = newHandler()
	}
	return instance
}

func Dump(cmd, param1, param2 string) {
	GetInstance().dump(cmd, param1, param2)
}

func newHandler() *ServiceHandler {
	ret := new(ServiceHandler)
	ret.ctor()
	return ret
}

type ServiceHandler struct{}

func (h *ServiceHandler) ctor() {

}

func (d *ServiceHandler) dump(cmd, param1, param2 string) {
	remotemessage.Dump(cmd, param1)
}

func (h *ServiceHandler) SayHello(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	rsp.Data = fmt.Sprintf("Hello from %s:%s", pb.ServiceName, req.Name)
	return nil
}

func (h *ServiceHandler) SetUserToken(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	remotemessage.SetUserToken(req.UserId, req.DeviceType, req.Token)
	return nil
}
func (h *ServiceHandler) SendRemoteMessage(ctx context.Context, req *pb.Request, rsp *pb.Response) error {
	remotemessage.SendRemoteMessage(req.UserIds, req.Title, req.Content)
	return nil
}
