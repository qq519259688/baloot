package remotemessage

import (
	"bet24.com/log"
	"context"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"google.golang.org/api/option"
)

type firebaseMgr struct {
	client *messaging.Client
}

var firebase_mgr *firebaseMgr

func getFireBaseInstance() *firebaseMgr {
	if firebase_mgr == nil {
		firebase_mgr = new(firebaseMgr)
		firebase_mgr.ctor()
	}
	return firebase_mgr
}

func (fm *firebaseMgr) ctor() bool {
	opt := option.WithCredentialsFile("serviceconfig/firebaseaccount.json")
	if opt == nil {
		log.Release("firebaseMgr.ctor fail to init creedential file firebaseaccount.json")
		return false
	}
	ctx := context.Background()
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Release("firebaseMgr.ctor initializing app: %v", err)
		return false
	}
	client, err := app.Messaging(ctx)
	if err != nil {
		log.Release("firebaseMgr.ctor app.Messaging failed: %v", err)
		return false
	}
	fm.client = client
	return true
}

func (fm *firebaseMgr) sendMessage(tokens []string, title, content string) {
	if fm.client == nil {
		fm.ctor()
	}
	if fm.client == nil {
		log.Release("firebaseMgr.sendMessage fm.client == nil")
		return
	}
	message := &messaging.MulticastMessage{
		Notification: &messaging.Notification{
			Title: title,
			Body:  content,
		},
		Tokens: tokens,
	}
	_, err := fm.client.SendMulticast(context.Background(), message)
	if err != nil {
		log.Release("firebaseMgr.sendMessage failed %v", err)
	}
}
