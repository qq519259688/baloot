package remotemessage

func Run() {

}

func Dump(param1, param2 string) {
	getInstance().dump(param1, param2)
}

func SetUserToken(userId int, deviceType int, token string) {
	getInstance().setUserToken(userId, deviceType, token)
}

func SendRemoteMessage(userIds []int, title string, content string) {
	getInstance().sendRemoteMessage(userIds, title, content)
}
