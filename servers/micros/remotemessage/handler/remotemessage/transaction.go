package remotemessage

import (
	"bet24.com/database"
	"bet24.com/log"
	dbengine "bet24.com/servers/micros/dbengine/proto"
	pb "bet24.com/servers/micros/remotemessage/proto"
	"encoding/json"
)

func updateUserToken(userId int, token string, deviceType int) {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("WS_UserRemoteToken_Update")
	statement.AddParamter("@UserId", database.AdParamInput, database.AdInteger, 4, userId)
	statement.AddParamter("@DeviceType", database.AdParamInput, database.AdInteger, 4, deviceType)
	statement.AddParamter("@Token", database.AdParamInput, database.AdVarChar, 128, token)
	sqlstring := statement.GenSql()
	dbengine.Execute(sqlstring)
}

func getUserTokenList() []pb.UserToken {
	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_UserRemoteToken_GetList")
	sqlstring := statement.GenSql()
	jsonData := dbengine.Execute(sqlstring)
	var ret []pb.UserToken
	if err := json.Unmarshal([]byte(jsonData), &ret); err != nil {
		log.Error("remotemessage.transaction.getUserTokenList json unmarshal err %s", jsonData)
	}
	return ret
}
