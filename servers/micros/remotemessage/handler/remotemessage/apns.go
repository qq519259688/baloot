package remotemessage

import (
	"bet24.com/log"
	"context"
	"github.com/sideshow/apns2"
	"github.com/sideshow/apns2/certificate"
	"github.com/sideshow/apns2/payload"
	"time"
)

type apnsMgr struct {
	client *apns2.Client
}

var apns_mgr *apnsMgr

func getApnsInstance() *apnsMgr {
	if apns_mgr == nil {
		apns_mgr = new(apnsMgr)
		apns_mgr.ctor()
	}
	return apns_mgr
}

func (fm *apnsMgr) ctor() bool {
	cert, err := certificate.FromP12File("anps.p12", "123456")
	if err != nil {
		log.Release("apnsMgr.ctor fail to init creedential file apns.p12")
		return false
	}
	client := apns2.NewClient(cert).Production()
	if err != nil {
		log.Release("apnsMgr.ctor app.client failed: %v", err)
		return false
	}
	fm.client = client
	return true
}

func (fm *apnsMgr) sendMessage(tokens []string, title, content string) {
	if fm.client == nil {
		fm.ctor()
	}
	if fm.client == nil {
		log.Release("apnsMgr.sendMessage fm.client == nil")
		return
	}
	notification := &apns2.Notification{}
	notification.Payload = payload.NewPayload().AlertTitle(title).AlertBody(content)
	// 包名需要配置
	notification.Topic = "chior.mideast.thirdd"
	for _, v := range tokens {
		notification.DeviceToken = v
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		_, err := fm.client.PushWithContext(ctx, notification)
		defer cancel()
		if err != nil {
			log.Release("apnsMgr.sendMessage failed %v", err)
		}
	}

}
