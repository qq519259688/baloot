package remotemessage

import (
	"bet24.com/log"
	pb "bet24.com/servers/micros/remotemessage/proto"
	"strconv"
	"sync"
)

var mgr *message_manager

func getInstance() *message_manager {
	if mgr == nil {
		mgr = new(message_manager)
		mgr.ctor()
	}
	return mgr
}

type message_manager struct {
	userlist map[int]*pb.UserToken
	lock     *sync.RWMutex
}

func (mm *message_manager) ctor() {
	log.Release("remotemessage.Run()")
	mm.userlist = make(map[int]*pb.UserToken)
	mm.lock = &sync.RWMutex{}

	ul := getUserTokenList()
	mm.lock.Lock()
	for k, v := range ul {
		mm.userlist[v.UserId] = &ul[k]
	}
	mm.lock.Unlock()
}

func (mm *message_manager) getUser(userId int) *pb.UserToken {
	mm.lock.RLock()
	defer mm.lock.RUnlock()
	ret, ok := mm.userlist[userId]
	if !ok {
		return nil
	}
	return ret
}

func (mm *message_manager) dump(param1, param2 string) {
	log.Release("-------------------------------")
	log.Release("message_manager.dump %s,%s", param1, param2)
	defer func() {
		log.Release("+++++++++++++++++++++++++++++++")
		log.Release("")
	}()

	userId, err := strconv.Atoi(param1)
	if err != nil {
		log.Release("message_manager.dump param error. err:%v", err)
		return
	}

	usr := mm.getUser(userId)
	if usr == nil {
		log.Release("message_manager.dump user not exist:%d", userId)
		return
	}

	log.Release("  UserId[%d]Type[%d]Token[%s]", usr.UserId, usr.DeviceType, usr.Token)
}

func (mm *message_manager) setUserToken(userId int, deviceType int, token string) {
	usr := mm.getUser(userId)
	if usr == nil {
		usr = &pb.UserToken{
			UserId:     userId,
			DeviceType: deviceType,
			Token:      token,
		}
		mm.lock.Lock()
		mm.userlist[userId] = usr
		mm.lock.Unlock()
	} else {
		usr.DeviceType = deviceType
		usr.Token = token
	}

	updateUserToken(userId, token, deviceType)
}

func (mm *message_manager) sendRemoteMessage(userIds []int, title string, content string) {
	var firebaseTokens []string
	var apnsTokens []string
	for _, v := range userIds {
		u := mm.getUser(v)
		if u == nil {
			continue
		}
		if u.Token == "" {
			continue
		}

		if u.DeviceType == pb.DeviceType_Android {
			firebaseTokens = append(firebaseTokens, u.Token)
		} else if u.DeviceType == pb.DeviceType_IOS {
			apnsTokens = append(apnsTokens, u.Token)
		}
	}

	if len(firebaseTokens) > 0 {
		getFireBaseInstance().sendMessage(firebaseTokens, title, content)
	}
	if len(apnsTokens) > 0 {
		getApnsInstance().sendMessage(apnsTokens, title, content)
	}
}
