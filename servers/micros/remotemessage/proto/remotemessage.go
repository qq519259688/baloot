package proto

import (
	"bet24.com/log"
	"bet24.com/servers/micros/common"
	"context"
)

const (
	DeviceType_Android = iota
	DeviceType_IOS
)

type UserToken struct {
	UserId     int
	DeviceType int
	Token      string
}

func SetUserToken(userId int, deviceType int, token string) {
	xclient := getClient()

	args := &Request{
		UserId:     userId,
		DeviceType: deviceType,
		Token:      token,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SetUserToken", args, reply)
	if err != nil {
		log.Debug("remotemessage.SetUserToken failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}

func SendRemoteMessage(userIds []int, title string, content string) {
	xclient := getClient()
	args := &Request{
		UserIds: userIds,
		Title:   title,
		Content: content,
	}

	reply := &Response{}

	err := xclient.Call(context.Background(), "SendRemoteMessage", args, reply)
	if err != nil {
		log.Debug("remotemessage.SendRemoteMessage failed to call: %v", err)
		common.GetClientPool().RemoveClient(ServiceName)
	}
}
