package main

import (
	"fmt"

	pb "bet24.com/servers/micros/remotemessage/proto"
)

func main() {
	fmt.Println(pb.SayHello("client"))
}
