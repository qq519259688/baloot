package coda

type ret struct {
	Code string `json:"code"` // 响应 ‘SUCCESS’
	Msg  string `json:"msg"`  // 响应 ‘Success’
}

type (
	init_req struct {
		InitRequest initRequest `json:"initRequest" form:"initRequest"`
	}

	initRequest struct {
		ApiKey   string        `json:"apiKey" form:"apiKey"`
		OrderId  string        `json:"orderId" form:"orderId"`
		Country  int           `json:"country" form:"country"`
		Currency int           `json:"currency" form:"currency"`
		PayType  int           `json:"payType" form:"payType"`
		Items    []item_req    `json:"items" form:"items"`
		Profiles []profile_req `json:"profile" form:"profile"`
	}

	item_req struct {
		Code  string  `json:"code" form:"code"`
		Name  string  `json:"name" form:"name"`
		Price float64 `json:"price" form:"price"`
		Type  int     `json:"type" form:"type"`
	}

	profile_req struct {
		Entry       []entry_req `json:"entry" form:"entry"`
		Need_mno_id string      `json:"need_mno_id,omitempty" form:"need_mno_id,omitempty"`
		User_id     string      `json:"user_id,omitempty" form:"user_id,omitempty"`
	}

	entry_req struct {
		Key   string `json:"key" form:"key"`
		Value string `json:"value" form:"value"`
	}
)

type (
	init_resp struct {
		InitResult initResult `json:"initResult" form:"initResult"`
	}

	initResult struct {
		ResultCode int `json:"resultCode" form:"resultCode"`
		TxnId      int `json:"txnId" form:"txnId"`
	}
)

type payNotify struct {
	TxnId       int     `json:"TxnId" form:"TxnId"`
	OrderId     string  `json:"OrderId" form:"OrderId"`
	ResultCode  int     `json:"ResultCode" form:"ResultCode"`
	TotalPrice  float64 `json:"TotalPrice" form:"TotalPrice"`
	PaymentType int     `json:"PaymentType" form:"PaymentType"`
	Checksum    string  `json:"Checksum" form:"Checksum"`
}

type orderItem struct {
	ItemCode string  `json:"itemCode" form:"itemCode"`
	Price    float64 `json:"price" form:"price"`
	Quantity int     `json:"quantity" form:"quantity"`
}
