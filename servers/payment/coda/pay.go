package coda

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"bet24.com/log"
	"bet24.com/public"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"bet24.com/servers/payment/shop"
	"github.com/gin-gonic/gin"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_coda_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "coda.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	//// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "coda.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "coda.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "coda.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "coda.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "coda.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	var itemList []item_req
	itemList = append(itemList, item_req{
		Code:  obj.In.ProductID,
		Name:  obj.In.ProductID,
		Price: obj.In.Price,
		Type:  1,
	})

	var entryList []entry_req
	entryList = append(entryList, entry_req{
		Key:   "user_id",
		Value: strconv.Itoa(obj.In.UserID),
	})

	var profileList []profile_req
	profileList = append(profileList, profile_req{
		Entry: entryList,
	})

	country := 0
	currency := 0
	apiKey := ""

	for _, v := range config.Server.Coda.ApiKeys {
		if v.Currency != obj.In.Currency {
			continue
		}

		country = v.CountryCode
		currency = v.CurrencyCode
		apiKey = v.ApiKey
		break
	}

	// 判断是否无效
	if country == 0 || currency == 0 || apiKey == "" {
		log.Error("coda.PayOrder obj.In=%+v is invalid", obj.In)
		return
	}

	// 请求payOrder的代码
	req := init_req{InitRequest: initRequest{
		ApiKey:   apiKey,
		OrderId:  obj.Out.OrderID,
		Country:  country,
		Currency: currency,
		PayType:  0,
		Items:    itemList,
		Profiles: profileList,
	}}

	// 生成签名
	checkContent, err := json.Marshal(req)
	if err != nil {
		log.Error("coda.PayOrder json marshal fail %v", err)
		return
	}

	// POST请求
	respBody := public.HttpPostByJson(config.Server.Coda.Url_pay_init, string(checkContent))
	log.Debug("coda.payOrder req ==> %+v  resp ==> %+v", string(checkContent), respBody)

	var resp init_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("coda.payOrder json unmarshal err %v", err)
		return
	}

	if resp.InitResult.ResultCode != 0 {
		log.Error("coda.payOrder init_resp %v", resp)
		return
	}

	begin_url := fmt.Sprintf(config.Server.Coda.Url_pay_begin, resp.InitResult.TxnId)

	// 跳转到支付页面，以便持卡人完成支付过程
	c.Redirect(http.StatusMovedPermanently, begin_url)
	return
}

// 回调通知
func PayNotify(c *gin.Context) {
	// bodyData, _ := io.ReadAll(c.Request.Body)
	// log.Debug("coda.PayNotify ==> ctx.Request.body: %v", string(bodyData))

	var resp payNotify
	if err := c.ShouldBind(&resp); err != nil {
		log.Debug("%s query params err %v", "coda.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("coda.PayNotify resp ==> %+v", resp)

	apiKey := ""

	for _, v := range config.Server.Coda.ApiKeys {
		for _, k := range v.PaymentTypes {
			if k != resp.PaymentType {
				continue
			}

			apiKey = v.ApiKey
			break
		}

		if apiKey != "" {
			break
		}
	}

	// TxnID + API key + OrderId + ResultCode
	data := fmt.Sprintf("%d%s%s%d", resp.TxnId, apiKey, resp.OrderId, resp.ResultCode)
	sign := public.GetMd5String(data)

	// 验证签名
	if sign != resp.Checksum {
		log.Error("coda.PayNotify 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("coda.PayNotify 签名成功")

	if resp.ResultCode != 0 {
		log.Debug("coda.PayNotify resp ==> %+v 接收响应", resp)
		c.String(http.StatusOK, "ResultCode=0")
		return
	}

	// 数据库操作
	obj := db.NewNotify(db.SP_coda_NOTIFY)
	obj.In.OrderID = resp.OrderId
	obj.In.TradeID = strconv.Itoa(resp.TxnId)
	obj.DoAction(nil)

	// 操作成功,给道具
	if obj.Out.RetCode == 1 {
		// 充值
		resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
		log.Debug("%s 充值成功 %+v", "coda.PayNotify", resp)
	}

	c.String(http.StatusOK, "ResultCode=0")
	return
}

// 支付完成跳转处理
func PayCallback(c *gin.Context) {
	var resp payNotify
	if err := c.ShouldBind(&resp); err != nil {
		log.Debug("%s query params err %v", "coda.PayCallback", err)
		c.String(http.StatusOK, "")
		return
	}
	log.Debug("coda.PayCallback ==> %+v", resp)

	c.HTML(http.StatusOK, "coda.html", gin.H{
		"title":      "Coda Recharge Result",
		"outTradeNo": resp.OrderId,
		"tradeToken": resp.TxnId,
		"status":     "SUCCESS",
	})
	return
}
