package crush

type (
	pay_req struct {
		MerchantOrderId string `json:"merchantOrderId,omitempty"` // 商户订单ID
		OrderAmount     string `json:"orderAmount,omitempty"`     // 订单金额，单位：元，带两位小数
		NotifyUrl       string `json:"notifyUrl,omitempty"`       // 异步回调地址
	}

	pay_resp struct {
		Success bool        `json:"success,omitempty"`
		Code    string      `json:"code,omitempty"`
		Msg     string      `json:"msg,omitempty"`
		Data    payRespData `json:"data,omitempty"` // 返回数据体
	}

	payRespData struct {
		MerchantOrderId string `json:"merchantOrderId,omitempty"` // 商户提交的订单号
		OrderId         string `json:"orderId,omitempty"`         // 系统订单号
		Status          string `json:"status,omitempty"`          // 交易状态
		PayUrl          string `json:"payUrl,omitempty"`          // 支付链接地址
		Message         string `json:"message,omitempty"`         // 跳转地址
	}
)

type (
	payNotify struct {
		OrderId         string `json:"orderId,omitempty"`         // 平台订单ID
		MerchantOrderId string `json:"merchantOrderId,omitempty"` // 商户订单ID
		PayAmount       string `json:"payAmount,omitempty"`       // 实际支付金额
		Status          string `json:"status,omitempty"`          // 支付状态：SUCCESS - 成功；FAILURE - 失败
	}
)
