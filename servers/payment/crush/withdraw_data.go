package crush

type (
	withdraw_req struct {
		MerchantOrderId string `json:"merchantOrderId,omitempty"` // 商户订单ID
		OrderAmount     string `json:"orderAmount,omitempty"`     // 订单金额，单位：元，带两位小数（埃及代付需填整数，如23.00）
		UserName        string `json:"userName,omitempty"`        // 收款人名字
		BankName        string `json:"bankName,omitempty"`        // 收款银行名称（秘鲁支持银行BCP、Interbank、BBVA、scotiabank）
		AccountNumber   string `json:"accountNumber,omitempty"`   // 收款银行账户
		MobileNumber    string `json:"mobileNumber,omitempty"`    // 收款人手机号码(乌干达必填)
		IdNumber        string `json:"idNumber,omitempty"`        // 证件号码(委内瑞拉必填)
		Cci             string `json:"cci,omitempty"`             // 收款CCI帐号(秘鲁必填)
		NotifyUrl       string `json:"notifyUrl,omitempty"`       // 商户提供的异步回调地址
	}

	withdraw_resp struct {
		Success bool   `json:"success" form:"success"` // 0：成功，1：失败
		Code    string `json:"code" form:"code"`       // 错误信息，code为0时，本值为空
		Msg     string `json:"msg" form:"msg"`
	}
)
