package crush

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"bet24.com/log"
	"bet24.com/public"
	notification "bet24.com/servers/micros/notification/proto"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 提现请求
func WithdrawOrder(c *gin.Context) {
	obj := db.NewCrushWithdrawReq()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "crush.WithdrawOrder", err)
		c.String(http.StatusOK, "fail")
		return
	}

	// 判断提现金额足购
	/*if !coreClient.TryWithdraw(obj.In.UserID, obj.In.Amount, true) {
		c.String(http.StatusOK, "Insufficient amount")
		return
	}*/

	obj.In.IPAddress = strings.Split(c.Request.RemoteAddr, ":")[0]
	obj.DoAction()
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v obj.Out=%+v", "crush.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	if obj.Out.GetStatus != 0 {
		log.Debug("%s GenOrder Audit obj.In=%+v obj.Out=%+v", "crush.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	// 请求时，没有传手机号
	if obj.In.Mobile == "" {
		obj.In.Mobile = obj.Out.Tel
	}

	// 请求withdrawOrder的代码
	req := withdraw_req{
		MerchantOrderId: obj.Out.OrderID,
		OrderAmount:     fmt.Sprintf("%d.00", obj.In.Amount),
		UserName:        obj.In.RealName,
		BankName:        obj.In.BankName,
		AccountNumber:   obj.In.BankCard,
		MobileNumber:    obj.In.Mobile,
		IdNumber:        "",
		Cci:             "",
		NotifyUrl:       config.Server.CrushPay.Url_withdraw_Notify,
	}

	// 生成签名
	checkContent, err := json.Marshal(req)
	if err != nil {
		log.Error("crush.WithdrawOrder checkContent json marshal fail %v", err)
		return
	}

	buf, err := public.AesEncrypt(checkContent, []byte(config.Server.CrushPay.MerchantKey))
	if err != nil {
		log.Error("crush.WithdrawOrder AESEncrypt err %v", err)
		return
	}

	sign := strings.ToUpper(hex.EncodeToString(buf))

	// 生成请求的 body
	bodyBuf, _ := json.Marshal(struct {
		Data string `json:"data"`
	}{
		Data: sign,
	})
	body := string(bodyBuf)

	log.Debug(body)

	respBody := httpPostByJson(config.Server.CrushPay.Url_withdraw_order, body, config.Server.CrushPay.MerchantAppId)
	log.Debug("crush.WithdrawOrder req ==> %+v  resp ==> %+v", string(checkContent), respBody)

	var resp withdraw_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("crush.WithdrawOrder json unmarshal req ==> %+v resp ==> %+v fail %v", req, respBody, err)
		c.String(http.StatusOK, "fail")
		return
	}

	log.Debug("crush.WithdrawOrder resp ==> %+v", resp)

	// 接口响应码，’APPLY_SUCCESS’代表成功
	if !resp.Success {
		log.Error("crush.withdrawRequest post return resp fail ==> %+v", resp)

		// 余额不足,退款
		// if resp.Data.Status != "PENDING" {
		// 	obj := db.NewPayerMaxWithdrawNotify()
		// 	obj.In.OrderID = req.Data.OutTradeNo
		// 	obj.In.DfTransactionId = resp.Data.TradeNo
		// 	obj.In.Status = 0
		// 	obj.In.DfDesc = fmt.Sprintf("%s", resp.Data.Status)
		// 	obj.In.Balance = 0
		// 	obj.DoAction()
		//
		// 	log.Debug("payermax.WithdrawOrder obj.In=%+v obj.Out=%+v", obj.In, obj.Out)
		// }

		c.String(http.StatusOK, "fail")
		return
	}

	go func() {
		//coreClient.TryWithdraw(obj.In.UserID, obj.In.Amount, false)

		// 通知
		notification.AddNotification(obj.In.UserID, notification.Notification_Gold, "")
	}()

	// 更新银行信息
	// objBank := db.NewBankInfoUp()
	// objBank.In.UserID = obj.In.UserID
	// objBank.In.RealName = req.Name
	// objBank.In.BankCard = req.Number
	// objBank.In.BankName = req.BankCode
	// objBank.In.Mobile = obj.In.Mobile
	// objBank.In.EMail = obj.In.Email
	// objBank.In.Address = obj.In.Address
	// objBank.DoAction()

	// log.Debug("payermax.WithdrawNotify need save bankInfo")

	c.String(http.StatusOK, "SUCCESS")
	return
}

// 提现通知
func WithdrawNotify(c *gin.Context) {
	var Req struct {
		Data string `json:"data" form:"data"`
	}
	if err := c.ShouldBind(&Req); err != nil {
		log.Debug("%s query params err %v", "crush.WithdrawNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("crush.WithdrawNotify resp ==> %+v", Req)

	data, err := hex.DecodeString(Req.Data)
	if err != nil {
		log.Debug("crush.WithdrawNotify hex.DecodeString %s ==> err %v", Req.Data, err)
		c.String(http.StatusOK, "")
		return
	}

	buf, err := public.AesDecrypt(data, []byte(config.Server.CrushPay.MerchantKey))
	if err != nil {
		log.Debug("crush.WithdrawNotify AESDecrypt err %v", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("crush.WithdrawNotify 解析成功  ==> %s", string(buf))

	var resp payNotify
	if err := json.Unmarshal(buf, &resp); err != nil {
		log.Debug("%s query params err %v", "crush.WithdrawNotify", err)
		c.String(http.StatusOK, "")
		return
	}
	log.Debug("crush.WithdrawNotify resp ==> %+v", resp)

	// 返回码，’APPLY_SUCCESS’代表成功
	if resp.Status != "SUCCESS" {
		log.Error("crush.WithdrawNotify resp ==> %+v 失败", resp)
		c.String(http.StatusOK, "SUCCESS")
		return
	}

	payAmount, err := strconv.Atoi(strings.ReplaceAll(resp.PayAmount, ".", ""))
	if err != nil {
		log.Error("crush.WithdrawNotify balance err %v", err)
		return
	}
	payAmount = payAmount / 100

	// 交易状态，SUCCESS成功, FAILD失败 ，PENDING进行中，BOUNCEBACK退票
	// 0=Failure 1=Success  2=Pending(Success)  3=BOUNCEBACK退票
	status := 0
	switch resp.Status {
	case "SUCCESS": // 1=成功
		status = 1
	default:
		status = 0
	}

	obj := db.NewCrushWithdrawNotify()
	obj.In.OrderID = resp.MerchantOrderId
	obj.In.DfTransactionId = resp.OrderId
	obj.In.Status = status
	obj.In.DfDesc = ""
	obj.In.Balance = 0
	obj.DoAction()

	log.Debug("crush.WithdrawNotify obj.In=%+v obj.Out=%+v", obj.In, obj.Out)

	// 提现失败,退款
	// if resp.Status == WITHDRAW_FAIL {
	// 	go coreClient.AddNotification(obj.Out.UserID, notification.Notification_Gold, "")
	// }

	c.String(http.StatusOK, "SUCCESS")
	return
}
