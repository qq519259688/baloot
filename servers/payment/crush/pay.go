package crush

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"bet24.com/servers/payment/shop"

	"bet24.com/log"
	"bet24.com/public"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_Crush_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "crush.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	//// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "crush.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "crush.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "crush.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "crush.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "crush.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	// 请求payOrder的代码
	req := pay_req{
		MerchantOrderId: obj.Out.OrderID,
		OrderAmount:     fmt.Sprintf("%f", obj.In.Price),
		NotifyUrl:       config.Server.CrushPay.Url_pay_Notify,
	}

	// 生成签名
	checkContent, err := json.Marshal(req)
	if err != nil {
		log.Error("crush.PayOrder json marshal fail %v", err)
		return
	}

	buf, err := public.AesEncrypt(checkContent, []byte(config.Server.CrushPay.MerchantKey))
	if err != nil {
		log.Error("crush.PayOrder AESEncrypt err %v", err)
		return
	}

	sign := strings.ToUpper(hex.EncodeToString(buf))

	// 生成请求的 body
	bodyBuf, _ := json.Marshal(struct {
		Data string `json:"data"`
	}{
		Data: sign,
	})
	body := string(bodyBuf)

	// POST请求
	respBody := httpPostByJson(config.Server.CrushPay.Url_pay_order, body, config.Server.CrushPay.MerchantAppId)
	log.Debug("crush.payOrder req ==> %+v  resp ==> %+v", string(checkContent), respBody)

	var resp pay_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("crush.payOrder json unmarshal req ==> %+v resp ==> %+v fail %v", body, respBody, err)
		return
	}

	log.Debug("crush.payOrder resp ==> %+v", resp)

	// 返回码，’APPLY_SUCCESS’代表成功
	if !resp.Success {
		log.Error("crush.payOrder post return resp fail ==> %+v", resp)
		return
	}

	// 跳转到支付页面，以便持卡人完成支付过程
	c.Redirect(http.StatusMovedPermanently, resp.Data.PayUrl)
	// c.String(http.StatusOK, "Success")
	return
}

// 回调通知
func PayNotify(c *gin.Context) {

	var Req struct {
		Data string `json:"data" form:"data"`
	}
	if err := c.ShouldBind(&Req); err != nil {
		log.Debug("%s query params err %v", "crush.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("crush.PayNotify resp ==> %+v", Req)

	data, err := hex.DecodeString(Req.Data)
	if err != nil {
		log.Debug("crush.PayNotify hex.DecodeString %s ==> err %v", Req.Data, err)
		c.String(http.StatusOK, "")
		return
	}

	buf, err := public.AesDecrypt(data, []byte(config.Server.CrushPay.MerchantKey))
	if err != nil {
		log.Debug("crush.PayNotify AESDecrypt err %v", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("crush.PayNotify 解析成功  ==> %s", string(buf))

	var resp payNotify
	if err := json.Unmarshal(buf, &resp); err != nil {
		log.Debug("%s query params err %v", "crush.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}
	log.Debug("crush.PayNotify resp ==> %+v", resp)

	// 返回码，’APPLY_SUCCESS’代表成功
	if resp.Status != "SUCCESS" {
		log.Error("crush.PayNotify resp ==> %+v 失败", resp)
		c.String(http.StatusOK, "SUCCESS")
		return
	}

	payAmount, err := strconv.Atoi(strings.ReplaceAll(resp.PayAmount, ".", ""))
	if err != nil {
		log.Error("crush.WithdrawNotify balance err %v", err)
		return
	}
	payAmount = payAmount / 100

	// 数据库操作
	obj := db.NewNotify(db.SP_Crush_NOTIFY)
	obj.In.OrderID = resp.MerchantOrderId
	obj.In.TradeID = resp.OrderId
	// obj.In.Price = payAmount
	obj.DoAction(nil)

	// 操作成功,给道具
	if obj.Out.RetCode == 1 {
		// 充值
		resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
		log.Debug("%s 充值成功 %+v", "crush.PayNotify", resp)
	}

	c.String(http.StatusOK, "SUCCESS")
	return
}

// 支付完成跳转处理
func PayCallback(c *gin.Context) {
	c.String(http.StatusOK, "SUCCESS")
	log.Debug("crush.payCallback")
}
