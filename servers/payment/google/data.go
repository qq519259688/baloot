package google

/*
   consumptionState    : The consumption state of the inapp product. Possible values are: (0) Yet to be consumed (1) Consumed
   developerPayload    : A developer-specified string that contains supplemental information about an order.
   kind                : This kind represents an inappPurchase object in the androidpublisher service.
   orderId             : The order id associated with the purchase of the inapp product.
   purchaseState       : The purchase state of the order. Possible values are: (0) Purchased (1) Cancelled
   purchaseTimeMillis  : The time the product was purchased, in milliseconds since the epoch (Jan 1, 1970).
*/

type GoogleIAP struct {
	Kind                        string `json:"kind"`
	PurchaseTimeMillis          string `json:"purchaseTimeMillis"`
	PurchaseState               int    `json:"purchaseState"`
	ConsumptionState            int    `json:"consumptionState"`
	OrderId                     string `json:"orderId"`
	DeveloperPayload            string `json:"developerPayload"`
	AcknowledgementState        int    `json:"acknowledgementState"`
	ObfuscatedExternalAccountId string `json:"obfuscatedExternalAccountId"`
	RegionCode                  string `json:"regionCode"`
}

type Req_Google struct {
	UserID      int    `json:"UserID"`      //用户ID
	ProductId   string `json:"ProductId"`   //产品ID
	Price       int    `json:"Price"`       //价格
	PartnerID   int    `json:"PartnerID"`   //渠道ID
	Token       string `json:"Token"`       //token
	PackageName string `json:"PackageName"` //PackeName
}
