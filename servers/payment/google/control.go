package google

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"bet24.com/log"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"

	coreClient "bet24.com/servers/coreservice/client"
)

func Verify(c *gin.Context) {
	var req Req_Google
	if err := c.ShouldBind(&req); err != nil {
		log.Error("google.verify get params fail %v", err)
		c.String(http.StatusOK, "google.Verify get params fail %v", err)
		return
	}

	ipAddress := strings.Split(c.Request.RemoteAddr, ":")[0]

	objLog := db.NewGoogleAddLog()
	objLog.In.UserID = req.UserID
	objLog.In.PartnerID = req.PartnerID
	objLog.In.ProductID = req.ProductId
	objLog.In.Price = req.Price
	objLog.In.Token = req.Token
	objLog.In.IpAddress = ipAddress
	go objLog.DoAction()

	packageName := req.PackageName
	if packageName == "" {
		packageName = config.Server.PackageName
	}

	googleApiFile := "conf/google-api.json"
	if packageName == "forest.leaf.bigdog" {
		googleApiFile = "conf/google-bigdog.json"
	} else {
		packageFile := fmt.Sprintf("conf/%s.json", packageName)
		_, err := os.ReadFile(packageFile) // PUT your credential json file
		if err == nil {
			googleApiFile = packageFile
		}
	}

	data, err := os.ReadFile(googleApiFile) // PUT your credential json file
	if err != nil {
		log.Error("google.verify ReadFile %v", err)
		c.String(http.StatusOK, "google.verify ReadFile %v", err)
		return
	}

	log.Debug("google.Verify using api file %s", googleApiFile)

	confJSON, err := google.JWTConfigFromJSON(data, "https://www.googleapis.com/auth/androidpublisher")
	if err != nil {
		log.Error("google.verify JWTConfigFromJson conf %v", err)
		c.String(http.StatusOK, "google.verify JWTConfigFromJson conf %v", err)
		return
	}

	client := confJSON.Client(oauth2.NoContext)

	resp, err := client.Get(
		fmt.Sprintf("https://www.googleapis.com/androidpublisher/v3/applications/%s/purchases/products/%s/tokens/%s",
			packageName,
			req.ProductId,
			req.Token))

	if err != nil {
		log.Error("google.Verify Get err: %v", err)
		c.String(http.StatusOK, "google.verify Get err: %v", err)
		return
	}

	body, err := io.ReadAll(resp.Body)
	log.Debug("response:%v ", string(body))

	appResult := &GoogleIAP{}
	err = json.Unmarshal(body, &appResult)

	if len(appResult.OrderId) <= 0 {
		log.Error("google.verify orderId is empty")
		c.String(http.StatusOK, "google.verify orderId is empty")
		return
	}

	objSuccess := db.NewGoogleAddSuccessLog()
	objSuccess.In.UserID = req.UserID
	objSuccess.In.Token = appResult.OrderId
	objSuccess.In.ProductID = req.ProductId
	objSuccess.In.Price = req.Price
	objSuccess.In.PartnerID = req.PartnerID
	objSuccess.In.ResponseIAP = string(body)
	objSuccess.In.IpAddress = ipAddress
	objSuccess.DoAction()

	//操作成功,给道具
	if objSuccess.Out.RetCode == 1 {
		//充值
		resp := coreClient.Recharge(req.UserID, req.ProductId)
		log.Debug("%s 充值成功 %+v", "google.Verify", resp)
	}

	c.String(http.StatusOK, "success")
	return
}

func RewardAD(c *gin.Context) {
	//log.Debug("google.RewardAD %v", *c)
	c.String(http.StatusOK, "OK")
}
