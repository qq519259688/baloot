package hall

import (
	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/redis"
	"bet24.com/servers/common"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/coreservice/shop"
	audioroom "bet24.com/servers/micros/audioroom/proto"
	dotservice "bet24.com/servers/micros/dotservice/proto"
	game "bet24.com/servers/micros/game/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	notification "bet24.com/servers/micros/notification/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/transaction"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"image"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// H5注册
func Register(c *gin.Context) {
	var req register_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "hall.Register", err)
		return
	}

	var (
		ret struct {
			RetCode int
			Items   []item.ItemPack
		}

		info struct {
			Name string `json:"name"`
			Id   string `json:"id"`
		}
	)

	id := strings.ReplaceAll(req.IMei, "fb.", "")

	// 去校验
	url := fmt.Sprintf("https://graph.facebook.com/%s?access_token=%s", id, req.AccessToken)
	respData := public.HttpGet(url)
	if err := json.Unmarshal([]byte(respData), &info); err != nil {
		log.Error("hall.Register respData=%s unmarshal err %v", respData, err)
		ret.RetCode = 15
		c.JSON(http.StatusOK, ret)
		return
	}

	log.Debug("hall.Register respData ==> %s", respData)

	// 校验失败
	if info.Name != req.NickName {
		ret.RetCode = 15
		c.JSON(http.StatusOK, ret)
		return
	}

	req.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	regResp := register(req)

	// 注册成功
	if regResp.RetCode == 1 && regResp.UserID > 0 {

		// 去绑定师徒
		resp := client.BindTeacher(regResp.UserID, req.TeacherID, 1)

		if resp.RetCode == 1 {
			if err := json.Unmarshal([]byte(resp.Data), &ret); err != nil {
				log.Error("hall.Register unmarshal err %v", err)
			}
		}
	}

	c.JSON(http.StatusOK, ret)
	return
}

// 绑定代理
func Bind(c *gin.Context) {
	var req register_req
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "hall.Bind", err)
		return
	}

	var ret struct {
		RetCode int
		Items   []item.ItemPack
	}

	if strings.HasPrefix(req.IMei, "fb.") {
		// facebook校验
		if ok := verifyFacebook(req.IMei, req.NickName, req.AccessToken); ok {
			ret.RetCode = 1
		}
	} else if strings.HasPrefix(req.IMei, "gg.") {
		// google校验
		if ok := verifyGoogle(req.NickName, req.AccessToken); ok {
			ret.RetCode = 1
		}
	}

	// 验证失败
	if ret.RetCode != 1 {
		ret.RetCode = 15
		c.JSON(http.StatusOK, ret)
		return
	}

	req.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	regResp := register(req)

	// 注册成功
	if regResp.RetCode == 1 && regResp.UserID > 0 {

		// 去绑定师徒
		resp := client.AgentBind(regResp.UserID, req.TeacherID, req.IpAddress)

		if resp.RetCode == 1 {
			if err := json.Unmarshal([]byte(resp.Data), &ret); err != nil {
				log.Error("hall.Bind unmarshal err %v", err)
			}
		}
	}

	c.JSON(http.StatusOK, ret)
	return
}

// facebook 验证
func verifyFacebook(iMei, nickName, accessToken string) bool {
	var info struct {
		Name string `json:"name"`
		Id   string `json:"id"`
	}

	id := strings.ReplaceAll(iMei, "fb.", "")

	// 去校验
	url := fmt.Sprintf("https://graph.facebook.com/%s?access_token=%s", id, accessToken)

	respData := public.HttpGet(url)

	log.Debug("hall.verifyFacebook respData ==> %s", respData)

	if err := json.Unmarshal([]byte(respData), &info); err != nil {
		log.Error("hall.verifyFacebook respData=%s unmarshal err %v", respData, err)
		return false
	}

	// 校验失败
	if info.Name != nickName {
		return false
	}

	return true
}

// google 验证
func verifyGoogle(nickName, accessToken string) bool {
	var info struct {
		Name string `json:"name"`
		Id   string `json:"id"`
	}

	// 去校验
	// url := fmt.Sprintf("https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=%s", accessToken)
	url := fmt.Sprintf("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=%s", accessToken)

	respData := public.HttpGet(url)

	log.Debug("hall.verifyGoogle respData ==> %s", respData)

	if err := json.Unmarshal([]byte(respData), &info); err != nil {
		log.Error("hall.verifyGoogle respData=%s unmarshal err %v", respData, err)
		return false
	}

	// 校验失败
	if info.Name != nickName {
		return false
	}

	return true
}

// 币种列表
func GetExchangeRateList(c *gin.Context) {
	resp := client.GetExchangeRateList()
	if resp.RetCode != 1 {
		c.JSON(http.StatusOK, "")
		return
	}
	c.String(http.StatusOK, resp.Data)
	return
}

// 商城列表
func GetShopList(c *gin.Context) {
	var req struct {
		Currency string
	}
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "hall.GetShopList", err)
		return
	}

	if req.Currency == "" {
		req.Currency = "EGP"
	}

	resp := client.GetShopList(0, shop.ShopType_Web, "")
	if resp.RetCode != 1 {
		c.JSON(http.StatusOK, "")
		return
	}

	c.String(http.StatusOK, resp.Data)
	return
}

// 用户信息
func GetUserInfo(c *gin.Context) {
	var req struct {
		UserID int
	}
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "hall.GetUserInfo", err)
		return
	}

	if req.UserID <= 0 {
		c.String(http.StatusOK, "fail")
		return
	}

	resp := userservices.GetUserInfo(req.UserID)
	d, _ := json.Marshal(resp)
	c.String(http.StatusOK, string(d))
	return
}

func IsInReview(c *gin.Context) {
	var req struct {
		VersionCode int
		PartnerId   int
	}
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "hall.IsInReview", err)
		return
	}
	reviewGame := ""
	done := make(chan int)
	go func() {
		reviewGame = game.GetReviewGame(c.ClientIP(), req.PartnerId, req.VersionCode)
		done <- 1
	}()
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	select {
	case <-done:
		cancel()
		break
	case <-ctx.Done():
		log.Release("game.GetReviewGame timeout")
		cancel()
		break
	}

	// 打点
	go dotservice.AddDot(0, dotservice.DotScope{
		Scene:     dotservice.Scene_HotUpdate,
		Action:    dotservice.Action_Click,
		Extra:     strconv.Itoa(common.GetTimeStamp()),
		IpAddress: c.ClientIP(),
	})
	c.String(http.StatusOK, reviewGame)
	return
}

func checkSection(userID int, token string) bool {
	if token == "" {
		return false
	}
	sectionKey := getSectionKey(userID)
	t, ok := redis.String_Get(sectionKey)
	if !ok {
		log.Release("redis error")
		return false
	}
	if t != token {
		return false
	}
	// 刷新有效期
	redis.String_SetEx(sectionKey, t, 1800)
	return true
}

func getSectionKey(userID int) string {
	return fmt.Sprintf("Section:%d", userID)
}

func getImageFormat(filename string) string {
	f, err := os.Open(filename)
	if err != nil {
		return ""
	}
	defer f.Close()

	_, format, err := image.Decode(f)
	if err != nil {
		return ""
	}

	return format
}

func isImageFile(fileName string) bool {
	f := getImageFormat(fileName)
	return f == "jpeg" || f == "png"
}

func deleteUserFace(userId int) {
	faceUrl := userservices.GetUserFaceUrl(userId)
	if faceUrl == "" {
		return
	}

	if !strings.HasPrefix(faceUrl, config.Server.UploadUrl) {
		return
	}

	fileName := strings.ReplaceAll(faceUrl, config.Server.UploadUrl, "")

	fileName = fmt.Sprintf("%s%s", config.Server.UploadDir, fileName)

	log.Debug("deleteUserFace %s", fileName)
	os.Remove(fileName)
}

func deleteUserRoomIamage(userId int) {
	roomUrl := audioroom.OnAudioRoomMsg(userId, "AudioRoomGetRoomImg", "")
	if roomUrl == "" {
		return
	}
	// 如果是头像复制的
	if strings.Contains(roomUrl, "user") {
		return
	}

	if !strings.HasPrefix(roomUrl, config.Server.UploadUrl) {
		return
	}

	fileName := strings.ReplaceAll(roomUrl, config.Server.UploadUrl, "")

	fileName = fmt.Sprintf("%s%s", config.Server.UploadDir, fileName)

	log.Debug("deleteUserRoomIamage %s", fileName)
	os.Remove(fileName)
}

func UploadFile(c *gin.Context) {
	_, err := c.MultipartForm()
	if err != nil {
		log.Debug("UploadFile failed MultipartForm %v", err)
		c.String(http.StatusBadRequest, "Bad request")
		return
	}
	log.Debug("headers %v", c.Request.Header)

	// 获得token
	token, ok := c.Request.Header["Authorization"]
	if !ok {
		log.Debug("UploadFile no Authorization")
		c.String(http.StatusBadRequest, "Bad request")
		return
	}
	var userToken UserToken
	err = json.Unmarshal([]byte(token[0]), &userToken)
	if err != nil {
		log.Debug("UploadFile failed usertoken %v", err)
		c.String(http.StatusBadRequest, "Bad request")
		return
	}
	log.Debug("UploadFile UserId :%d token :%s", userToken.UserId, userToken.Token)

	// 校验token
	if !checkSection(userToken.UserId, userToken.Token) {
		log.Debug("UploadFile failed checkSection failed")
		c.String(http.StatusBadRequest, "Bad request")
		return
	}

	file, err := c.FormFile("fileName")
	if err != nil {
		log.Debug("UploadFile failed %v", err)
		c.String(http.StatusBadRequest, "Bad request")
		return
	}
	tmpFileName := fmt.Sprintf("temp/%d_%s", userToken.UserId, file.Filename)
	log.Debug("UploadFile UserId :%d file save to :%s", userToken.UserId, tmpFileName)

	// Save the file to disk
	err = c.SaveUploadedFile(file, tmpFileName)
	if err != nil {
		c.String(http.StatusInternalServerError, "Internal server error")
		log.Debug("UploadFile save to file %s error %v", tmpFileName, err)
		return
	}

	/*format := getImageFormat(tmpFileName)
	if format != "jpeg" && format != "png" {
		c.String(http.StatusInternalServerError, "Internal server error")
		log.Debug("UploadFile checkIsImageFormat %s failed  format[%s]", tmpFileName, format)
		os.Remove(tmpFileName)
		return
	}*/
	isRoomImage := strings.Contains(file.Filename, "roomImage")

	var faceFile string
	// 找出玩家之前头像，尝试删除
	if isRoomImage {
		log.Debug("UploadFile UserId :%d file is roomImage", userToken.UserId)
		deleteUserRoomIamage(userToken.UserId)
		faceFile = fmt.Sprintf("%d_roomImage_%d.jpg", userToken.UserId, rand.Intn(10000))
	} else {
		deleteUserFace(userToken.UserId)
		faceFile = fmt.Sprintf("%d_face_%d.jpg", userToken.UserId, rand.Intn(10000))
	}

	finalFileName := fmt.Sprintf("%s/%s", config.Server.UploadDir, faceFile)
	os.Rename(tmpFileName, finalFileName)

	url := fmt.Sprintf("%s/%s", config.Server.UploadUrl, faceFile)
	// update db
	if !isRoomImage {
		if transaction.ChangeUserFace(userToken.UserId, -1, url) {
			log.Debug("UploadFile changed user[%d] face[%s]", userToken.UserId, url)
			notification.AddNotification(userToken.UserId, notification.Notification_UserInfoChanged, fmt.Sprintf(`{"FaceUrl":"%s"}`, url))
			// 刷新coreservice中的用户信息
			userservices.UpdateUserInfo(userToken.UserId)
		}
	} else {
		audioroom.OnAudioRoomMsg(userToken.UserId, "updateRoomImg", url)
		notification.AddNotification(userToken.UserId, notification.Notification_RoomImageChanged, fmt.Sprintf(`{"RoomImageUrl":"%s"}`, url))

	}

	c.String(http.StatusOK, "File uploaded successfully")
}

func Feedback(c *gin.Context) {
	var req struct {
		Email string
		Msg   string
	}

	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s shouldBind err %v", "hall.Feedback", err)
		return
	}

	if req.Email == "" || req.Msg == "" {
		c.String(http.StatusBadRequest, "failed")
		return
	}

	obj := transaction.NewFeedback()
	obj.In.IPAddress = strings.Split(c.Request.RemoteAddr, ":")[0]
	obj.In.Email = req.Email
	obj.In.Msg = req.Msg
	obj.DoAction()
	c.String(http.StatusOK, "ok")
}
