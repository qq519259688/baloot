package hall

import (
	"runtime/debug"

	"bet24.com/servers/payment/db"

	"bet24.com/database"
	"bet24.com/log"
)

// 师徒登录（绑定）
func register(req register_req) register_resp {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	var resp register_resp

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("H5_AllUser_Register")
	statement.AddParamter("@IMei", database.AdParamInput, database.AdVarChar, 64, req.IMei)
	statement.AddParamter("@NickName", database.AdParamInput, database.AdNVarChar, 32, req.NickName)
	statement.AddParamter("@Sex", database.AdParamInput, database.AdInteger, 4, req.Sex)
	statement.AddParamter("@FaceID", database.AdParamInput, database.AdInteger, 4, req.FaceID)
	statement.AddParamter("@FaceUrl", database.AdParamInput, database.AdVarChar, 1024, req.FaceUrl)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, req.PartnerID)
	statement.AddParamter("@Version", database.AdParamInput, database.AdInteger, 4, req.Version)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, req.IpAddress)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	retRows := db.CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return resp
	}

	ret := retRows[0]
	resp.RetCode = int((*ret[0].(*interface{})).(int64))
	resp.UserID = int((*ret[1].(*interface{})).(int64))
	resp.NickName = (*ret[2].(*interface{})).(string)
	// log.Debug("register.resp==>%+v", resp)
	return resp
}
