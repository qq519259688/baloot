package hall

// H5注册
type (
	register_req struct {
		IMei        string // IMei
		NickName    string // 昵称
		Sex         int    // 性别 0=默认(无)  1=男  2=女
		FaceID      int    // 头像ID
		FaceUrl     string // 头像地址
		PartnerID   int    // 渠道ID
		Version     int    // 版本
		TeacherID   int    // 师父ID
		IpAddress   string // IP地址
		AccessToken string // token
	}

	register_resp struct {
		RetCode  int    // 操作结果
		UserID   int    // 用户ID
		NickName string // 昵称
	}

	UserToken struct {
		UserId int
		Token  string
	}
)
