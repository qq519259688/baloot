package toppay

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"bet24.com/servers/payment/shop"

	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_Kaya_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "toppay.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	//// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "toppay.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "toppay.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "toppay.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "toppay.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "toppay.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	// 请求payOrder的代码
	req := pay_req{
		MerchantCode:  config.Server.TopPay.MerchantCode,
		Method:        "", // DANA
		OrderNum:      obj.Out.OrderID,
		PayMoney:      int(math.Ceil(obj.In.Price)),
		ProductDetail: obj.In.ProductID,
		Name:          strconv.Itoa(obj.In.UserID),
		// Name:         obj.In.Name,
		Email:        obj.In.Email,
		Phone:        obj.In.Tel,
		NotifyUrl:    config.Server.TopPay.Url_pay_notify,
		ExpiryPeriod: 30,
		DateTime:     time.Now().Format(TIME_FORMAT),
	}

	params := url.Values{}
	params.Set("merchantCode", req.MerchantCode)
	params.Set("method", req.Method)
	params.Set("orderNum", req.OrderNum)
	params.Set("payMoney", strconv.Itoa(req.PayMoney))
	params.Set("productDetail", req.ProductDetail)
	params.Set("name", req.Name)
	params.Set("email", req.Email)
	params.Set("phone", req.Phone)
	params.Set("notifyUrl", req.NotifyUrl)
	params.Set("expiryPeriod", strconv.Itoa(req.ExpiryPeriod))
	params.Set("dateTime", req.DateTime)

	// 生成签名
	checkContent := createEncryptStr(params)
	// log.Debug("order.checkContent ==> %s", checkContent)

	// 商户请求我们接口时使用商户私钥对请求参数 进行加密
	mchPriKey := fmt.Sprintf(`-----BEGIN PRIVATE KEY-----
%s
-----END PRIVATE KEY-----
`, config.Server.TopPay.MCH_PRIVATE_KEY)

	if err := public.RSA.SetPrivateKey(mchPriKey); err != nil {
		log.Error("toppay.payOrder set private key :%v ==> %+v", err, params)
		return
	}

	// 私钥加密
	prienctypt, err := public.RSA.PriKeyENCTYPT([]byte(checkContent))
	if err != nil {
		log.Error("toppay.payOrder RSA.PriKeyENCTYPT err %v", err)
		return
	}

	req.Sign = base64.StdEncoding.EncodeToString(prienctypt)
	// log.Debug("kaya.payOrder req ==> %+v ", params)

	// POST请求
	buf, _ := json.Marshal(req)
	respBody := public.HttpPostByJson(config.Server.TopPay.Url_pay_order, string(buf))
	log.Debug("toppay.payOrder req ==> %+v  resp ==> %+v", params, respBody)

	var resp pay_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("toppay.payOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		return
	}

	// log.Debug("kaya.payOrder resp ==> %+v", resp)

	// 请求响应码，00000表示成功，其他失败
	if resp.PlatRespCode != RESP_CODE_SUCCESS {
		log.Error("toppay.payOrder post return resp fail ==> %+v", resp)
		return
	}

	// 跳转到支付页面，以便持卡人完成支付过程
	c.Redirect(http.StatusMovedPermanently, resp.Url)

	// c.String(http.StatusOK, "Success")
	return
}

// 回调通知
func PayNotify(c *gin.Context) {
	var resp payNotify
	if err := c.ShouldBind(&resp); err != nil {
		log.Debug("%s query params err %v", "toppay.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("toppay.PayNotify resp ==> %+v", resp)

	params := url.Values{}
	params.Set("code", resp.Code)
	params.Set("email", resp.Email)
	params.Set("method", resp.Method)
	params.Set("msg", resp.Msg)
	params.Set("name", resp.Name)
	params.Set("orderNum", resp.OrderNum)
	params.Set("payFee", strconv.Itoa(resp.PayFee))
	params.Set("payMoney", strconv.Itoa(resp.PayMoney))
	params.Set("platOrderNum", resp.PlatOrderNum)
	params.Set("vaNumber", resp.VaNumber)

	// 生成签名
	checkContent := createEncryptStr(params)
	// log.Debug("kaya.PayNotify checkContent ==> %s", checkContent)

	// 商户使用 商户后台显示的平台公钥 进行解密
	platPubKey := fmt.Sprintf(`-----BEGIN PUBLIC KEY-----
%s
-----END PUBLIC KEY-----
`, config.Server.TopPay.PLAT_PUBLIC_KEY)

	if err := public.RSA.SetPublicKey(platPubKey); err != nil {
		log.Error("toppay.PayNotify set public key :%v ==> %+v", err, params)
		return
	}

	data, err := base64.StdEncoding.DecodeString(resp.PlatSign)
	if err != nil {
		log.Error("toppay.PayNotify base64.StdEncoding.DecodeString err %v", err)
		return
	}

	// 公钥解密
	pubdecrypt, err := public.RSA.PubKeyDECRYPT(data)
	if err != nil {
		log.Error("toppay.PayNotify RSA.PubKeyDECRYPT err %v", err)
		return
	}

	if checkContent != string(pubdecrypt) {
		log.Error("toppay.PayNotify 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("toppay.PayNotify 签名成功")

	// 0-成功，其他失败
	if resp.Code != "00" {
		log.Error("toppay.PayNotify resp ==> %+v 失败", resp)
		return
	}

	// 数据库操作
	obj := db.NewNotify(db.SP_Kaya_NOTIFY)
	obj.In.OrderID = resp.OrderNum
	obj.In.TradeID = resp.PlatOrderNum
	//obj.In.Price = resp.PayMoney
	obj.DoAction(nil)

	// 操作成功,给道具
	// if obj.Out.RetCode == 1 {
	// 	// 充值
	// 	resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
	// 	log.Debug("%s 充值成功 %+v", "toppay.PayNotify", resp)
	// }

	c.String(http.StatusOK, "success")
	return
}
