package toppay

const (
	RESP_CODE_SUCCESS  = "SUCCESS"
	RESP_CODE_FAIL     = "FAIL"
	RESP_CODE_UNKNOWN  = "UNKNOWN"
	RESP_CODE_NOTEXIST = "NOTEXIST"
	RESP_CODE_ERROR    = "ERROR"
	TIME_FORMAT        = "20060102150405"
	WITHDRAW_SUCCESS   = "2"
	WITHDRAW_FAIL      = "4"
)

// 代收下单
type (
	pay_req struct {
		MerchantCode  string `json:"merchantCode" form:"merchantCode"`       // 唯一商户代码
		Method        string `json:"method" form:"method"`                   // 银行类型
		OrderNum      string `json:"orderNum" form:"orderNum"`               // 这个号码对于商家来说应该是唯一的。平台不接受重复的号码。
		PayMoney      int    `json:"payMoney" form:"payMoney"`               // 支付数量。只接受整数。
		ProductDetail string `json:"productDetail" form:"productDetail"`     // 账单说明
		Name          string `json:"name" form:"name"`                       // 当用户向 VA 付款时，此名称将显示在账单页面上
		Email         string `json:"email" form:"email"`                     // 客户电子邮件
		Phone         string `json:"phone" form:"phone"`                     // 用户电话号码
		NotifyUrl     string `json:"notifyUrl" form:"notifyUrl"`             // 当用户向 va 付款时，商家将在此 url 上收到通知
		ExpiryPeriod  int    `json:"expiryPeriod" form:"expiryPeriod"`       // 以分钟为单位的订单/账单到期时间
		DateTime      string `json:"dateTime" form:"dateTime"`               // 格式 yyyyMMddHHmmss
		AppId         string `json:"appId,omitempty" form:"appId,omitempty"` // 应用程序 ID。商家可以根据业务创建自己的应用程序。该字段是可选的。
		Sign          string `json:"sign" form:"sign"`
	}

	pay_resp struct {
		PlatRespCode    string `json:"platRespCode" form:"platRespCode"`       // 结果代码： "SUCCESS" "FAIL" "UNKNOWN" "NOTEXIST" "ERROR"
		PlatRespMessage string `json:"platRespMessage" form:"platRespMessage"` // 响应代码说明
		PlatOrderNum    string `json:"platOrderNum" form:"platOrderNum"`       // 平台范围内的唯一订单号。
		OrderNum        string `json:"orderNum" form:"orderNum"`               // 商户范围内的唯一订单号。与请求正文中的 orderNum 相同。
		Amount          string `json:"amount" form:"amount"`                   // 单位为印尼盾，正整数
		ProductDetail   string `json:"productDetail" form:"productDetail"`     // 账单的描述。与请求正文中的字段相同。
		Name            string `json:"name" form:"name"`                       // 顾客的姓名，同请求name
		Email           string `json:"email" form:"email"`                     // 顾客的邮箱，不做校验，同请求emai
		Url             string `json:"url" form:"url"`                         // 顾客付款的支付链接
		PlatSign        string `json:"platSign" form:"platSign"`               // 加密的响应数据
	}
)

// 代收回调通知
type payNotify struct {
	Code         string `json:"code" form:"code"`                 // 00代表成功, 其他失败
	Email        string `json:"email" form:"email"`               // 邮箱
	Method       string `json:"method" form:"method"`             // 渠道code
	Msg          string `json:"msg" form:"msg"`                   // 描述
	Name         string `json:"name" form:"name"`                 // 名称
	OrderNum     string `json:"orderNum" form:"orderNum"`         // 商户订单号
	PayFee       int    `json:"payFee" form:"payFee"`             // 手续费
	PayMoney     int    `json:"payMoney" form:"payMoney"`         // va金额
	PlatOrderNum string `json:"platOrderNum" form:"platOrderNum"` // 平台订单号
	PlatSign     string `json:"platSign" form:"platSign"`         // 签名一致则代表是平台下发,否则不是
	VaNumber     string `json:"vaNumber" form:"vaNumber"`         // va number
}
