package toppay

// 代付下单
type (
	withdraw_req struct {
		MerchantCode string `json:"merchantCode" form:"merchantCode"`       // 唯一商户代码
		OrderNum     string `json:"orderNum" form:"orderNum"`               // 这个号码对于商家来说应该是唯一的。平台不接受重复的号码。
		Money        int    `json:"money" form:"money"`                     // 转账金额。只接受整数。
		Description  string `json:"description" form:"description"`         // 账单说明
		Name         string `json:"name" form:"name"`                       // 留在银行的用户的真实姓名。
		BankCode     string `json:"bankCode" form:"bankCode"`               // 银行简称
		Number       string `json:"number" form:"number"`                   // 用户的银行卡号。
		NotifyUrl    string `json:"notifyUrl" form:"notifyUrl"`             // 异步通知地址，用于接收转账成功的异步通知
		FeeType      int    `json:"feeType" form:"feeType"`                 // 枚举：0 1 如何收取交易费用：- 0 从转账金额中扣除 - 1 从商家余额中扣除
		DateTime     string `json:"dateTime" form:"dateTime"`               // 格式 yyyyMMddHHmmss
		AppId        string `json:"appId,omitempty" form:"appId,omitempty"` // 应用程序 ID。商家可以根据业务创建自己的应用程序。该字段是可选的。
		Sign         string `json:"sign" form:"sign"`
	}

	withdraw_resp struct {
		PlatRespCode    string `json:"platRespCode" form:"platRespCode"`       // 结果代码： "SUCCESS" "FAIL" "UNKNOWN" "NOTEXIST" "ERROR"
		PlatRespMessage string `json:"platRespMessage" form:"platRespMessage"` // 响应代码说明
		PlatOrderNum    string `json:"platOrderNum" form:"platOrderNum"`       // 平台范围内的唯一订单号。
		OrderNum        string `json:"orderNum" form:"orderNum"`               // 商户范围内的唯一订单号。与请求正文中的 orderNum 相同。
		Name            string `json:"name" form:"name"`                       // 当用户向 VA 付款时，此名称将显示在账单页面上
		BankCode        string `json:"bankCode" form:"bankCode"`               // 银行简称。
		Number          string `json:"number" form:"number"`                   // 用户的银行卡号。
		Money           string `json:"money" form:"money"`                     // 转账金额。只接受整数。
		Fee             string `json:"fee" form:"fee"`                         // 平台收取的费用
		NotifyUrl       string `json:"notifyUrl" form:"notifyUrl"`             // 异步通知地址，用于接收转账成功的异步通知
		FeeType         string `json:"feeType" form:"feeType"`                 // 枚举：0 1	如何收取交易费用：- 0 从转账金额中扣除 - 1 从商家余额中扣除
		PlatSign        string `json:"platSign" form:"platSign"`               // 加密的响应数据
	}
)

// 代付回调通知
type withdrawNotify struct {
	BankCode     string `json:"bankCode" form:"bankCode"`         // 014 与用户请求一致
	Description  string `json:"description" form:"description"`   // 描述与用户请求一致
	Fee          int    `json:"fee" form:"fee"`                   // 手续费
	FeeType      int    `json:"feeType" form:"feeType"`           // 0-商户转账金额扣款 1-商户余额扣费
	Money        int    `json:"money" form:"money"`               // 代付金额
	Name         string `json:"name" form:"name"`                 // 收款名称
	Number       string `json:"number" form:"number"`             // 收款号码
	OrderNum     string `json:"orderNum" form:"orderNum"`         // 商户订单号
	PlatOrderNum string `json:"platOrderNum" form:"platOrderNum"` // 平台订单号
	PlatSign     string `json:"platSign" form:"platSign"`         // 签名一致则代表是平台下发,否则不是
	Status       string `json:"status" form:"status"`             // 状态:2-成功 4-失败
	StatusMsg    string `json:"statusMsg" form:"statusMsg"`       // 状态描述
}

// 账户余额查询请求、响应
type (
	queryAccount_req struct {
		MerchantCode string `json:"merchantCode" form:"merchantCode"`       // 唯一商户代码
		DateTime     string `json:"dateTime" form:"dateTime"`               // 格式 yyyyMMddHHmmss
		AppId        string `json:"appId,omitempty" form:"appId,omitempty"` // 应用程序 ID。商家可以根据业务创建自己的应用程序。该字段是可选的。
		Sign         string `json:"sign" form:"sign"`
	}

	queryAccount_resp struct {
		PlatRespCode    string `json:"platRespCode" form:"platRespCode"`       // 结果代码： "SUCCESS" "FAIL" "UNKNOWN" "NOTEXIST" "ERROR"
		PlatRespMessage string `json:"platRespMessage" form:"platRespMessage"` // 响应代码说明
		Balance         string `json:"balance" form:"balance"`                 // 商户账户余额。
		Freeze          string `json:"freeze" form:"freeze"`                   // 冻结金额
		PendMoney       string `json:"pendMoney" form:"pendMoney"`             // 处理中的金额（解冻或冻结）
		PlatSign        string `json:"platSign" form:"platSign"`               // 加密的响应数据
	}
)
