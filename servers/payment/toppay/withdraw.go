package toppay

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"bet24.com/log"
	"bet24.com/public"
	notification "bet24.com/servers/micros/notification/proto"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 提现请求
func WithdrawOrder(c *gin.Context) {
	obj := db.NewKayaWithdrawReq()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "toppay.WithdrawOrder", err)
		c.String(http.StatusOK, "fail")
		return
	}

	obj.In.IPAddress = strings.Split(c.Request.RemoteAddr, ":")[0]
	obj.DoAction()
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v obj.Out=%+v", "toppay.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	if obj.Out.GetStatus != 0 {
		log.Debug("%s GenOrder Audit obj.In=%+v obj.Out=%+v", "toppay.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	// 请求时，没有传手机号
	if obj.In.Mobile == "" {
		obj.In.Mobile = obj.Out.Tel
	}

	// 请求withdrawOrder的代码
	req := withdraw_req{
		MerchantCode: config.Server.TopPay.MerchantCode,
		OrderNum:     obj.Out.OrderID,
		Money:        obj.In.Amount,
		Description:  "withdraw",
		Name:         obj.In.RealName,
		BankCode:     obj.In.BankCode,
		Number:       obj.In.BankCard,
		NotifyUrl:    config.Server.TopPay.Url_withdraw_notify,
		FeeType:      1,
		DateTime:     TIME_FORMAT,
	}

	params := url.Values{}
	params.Set("merchantCode", req.MerchantCode)
	params.Set("orderNum", req.OrderNum)
	params.Set("money", strconv.Itoa(req.Money))
	params.Set("description", req.Description)
	params.Set("name", req.Name)
	params.Set("bankCode", req.BankCode)
	params.Set("number", req.Number)
	params.Set("notifyUrl", req.NotifyUrl)
	params.Set("feeType", strconv.Itoa(req.FeeType))
	params.Set("dateTime", req.DateTime)

	// 生成签名
	checkContent := createEncryptStr(params)
	log.Debug("toppay.WithdrawOrder.checkContent ==> %s", checkContent)

	// 商户请求我们接口时使用商户私钥对请求参数 进行加密
	mchPriKey := fmt.Sprintf(`-----BEGIN PRIVATE KEY-----
%s
-----END PRIVATE KEY-----
`, config.Server.TopPay.MCH_PRIVATE_KEY)

	if err := public.RSA.SetPrivateKey(mchPriKey); err != nil {
		log.Error("toppay.WithdrawOrder set private key :%v ==> %+v", err, params)
		return
	}

	// 私钥加密
	prienctypt, err := public.RSA.PriKeyENCTYPT([]byte(checkContent))
	if err != nil {
		log.Error("toppay.WithdrawOrder RSA.PriKeyENCTYPT err %v", err)
		return
	}

	req.Sign = base64.StdEncoding.EncodeToString(prienctypt)
	// log.Debug("toppay.payOrder req ==> %+v ", params)

	buf, _ := json.Marshal(req)
	respBody := public.HttpPostByJson(config.Server.TopPay.Url_withdraw_order, string(buf))
	log.Debug("toppay.WithdrawOrder req ==> %+v  resp ==> %+v", string(buf), respBody)

	var resp withdraw_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("toppay.WithdrawOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		c.String(http.StatusOK, "fail")
		return
	}

	log.Debug("toppay.WithdrawOrder resp ==> %+v", resp)

	// 请求响应码，00000表示成功，其他失败
	if resp.PlatRespCode != RESP_CODE_SUCCESS {
		log.Error("toppay.withdrawRequest post return resp fail ==> %+v", resp)

		// // 余额不足,退款
		// if resp.PlatRespCode == RESP_CODE_FAIL && resp.PlatRespMessage == "insufficient account balance" {
		// 	obj := db.NewKayaWithdrawNotify()
		// 	obj.In.OrderID = req.OrderNum
		// 	obj.In.DfTransactionId = resp.PlatOrderNum
		// 	obj.In.Status = 0
		// 	obj.In.DfDesc = fmt.Sprintf("%s", resp.PlatRespMessage)
		// 	obj.In.Balance = 0
		// 	obj.DoAction()
		//
		// 	log.Debug("kaya.WithdrawOrder obj.In=%+v obj.Out=%+v", obj.In, obj.Out)
		// }

		c.String(http.StatusOK, "fail")
		return
	}

	// go coreClient.AddNotification(obj.In.UserID, notification.Notification_Gold, "")

	// 更新银行信息
	// objBank := db.NewBankInfoUp()
	// objBank.In.UserID = obj.In.UserID
	// objBank.In.RealName = req.Name
	// objBank.In.BankCard = req.Number
	// objBank.In.BankName = req.BankCode
	// objBank.In.Mobile = obj.In.Mobile
	// objBank.In.EMail = obj.In.Email
	// objBank.In.Address = obj.In.Address
	// objBank.DoAction()

	// log.Debug("toppay.WithdrawNotify need save bankInfo")

	c.String(http.StatusOK, "success")
	return
}

// 提现通知
func WithdrawNotify(c *gin.Context) {
	var resp withdrawNotify
	if err := c.ShouldBind(&resp); err != nil {
		log.Debug("%s query params err %v", "toppay.WithdrawNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("toppay.WithdrawNotify resp ==> %+v", resp)

	params := url.Values{}
	params.Set("bankCode", resp.BankCode)
	params.Set("description", resp.Description)
	params.Set("fee", strconv.Itoa(resp.Fee))
	params.Set("feeType", strconv.Itoa(resp.FeeType))
	params.Set("money", strconv.Itoa(resp.Money))
	params.Set("name", resp.Name)
	params.Set("number", resp.Number)
	params.Set("orderNum", resp.OrderNum)
	params.Set("platOrderNum", resp.PlatOrderNum)
	params.Set("status", resp.Status)
	params.Set("statusMsg", resp.StatusMsg)

	// 生成签名
	checkContent := createEncryptStr(params)
	log.Debug("toppay.WithdrawNotify checkContent ==> %s", checkContent)

	// 商户使用 商户后台显示的平台公钥 进行解密
	platPubKey := fmt.Sprintf(`-----BEGIN PUBLIC KEY-----
%s
-----END PUBLIC KEY-----
`, config.Server.TopPay.PLAT_PUBLIC_KEY)

	if err := public.RSA.SetPublicKey(platPubKey); err != nil {
		log.Error("toppay.WithdrawNotify set public key :%v ==> %+v", err, params)
		return
	}

	data, err := base64.StdEncoding.DecodeString(resp.PlatSign)
	if err != nil {
		log.Error("toppay.WithdrawNotify base64.StdEncoding.DecodeString err %v", err)
		return
	}

	// 公钥解密
	pubdecrypt, err := public.RSA.PubKeyDECRYPT(data)
	if err != nil {
		log.Error("toppay.WithdrawNotify RSA.PubKeyDECRYPT err %v", err)
		return
	}

	// 校验信息
	if checkContent != string(pubdecrypt) {
		log.Error("toppay.payOrder 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("toppay.PayNotify 签名成功")

	// 状态:2-成功 4-失败
	if resp.Status != WITHDRAW_SUCCESS && resp.Status != WITHDRAW_FAIL {
		log.Error("toppay.WithdrawNotify req ==> %+v", resp)
		c.String(http.StatusOK, "")
		return
	}

	// 查询账户余额
	balance := queryAccount()

	// 状态(0=Failure 1=Success  2=Pending(Success))
	status := 0
	switch resp.Status {
	case "2": // 2=成功
		status = 1
	case "4": // 4=失败
		status = 0
	default:
		status = 2
	}

	obj := db.NewKayaWithdrawNotify()
	obj.In.OrderID = resp.OrderNum
	obj.In.DfTransactionId = resp.PlatOrderNum
	obj.In.Status = status
	obj.In.DfDesc = fmt.Sprintf("%s，Fee: %d", strings.ReplaceAll(resp.StatusMsg, "'", ""), resp.Fee)
	obj.In.Balance = balance
	obj.DoAction()

	log.Debug("toppay.WithdrawNotify obj.In=%+v obj.Out=%+v", obj.In, obj.Out)

	// 提现失败,退款
	// if resp.Status == WITHDRAW_FAIL {
	// 	go coreClient.AddNotification(obj.Out.UserID, notification.Notification_Gold, "")
	// }

	c.String(http.StatusOK, "success")
	return
}

// 账户余额
func queryAccount() int {
	req := queryAccount_req{
		MerchantCode: config.Server.TopPay.MerchantCode,
		DateTime:     time.Now().Format(TIME_FORMAT),
	}

	params := url.Values{}
	params.Set("merchantCode", req.MerchantCode)
	params.Set("dateTime", req.DateTime)

	// 生成签名
	checkContent := createEncryptStr(params)
	log.Debug("toppay.queryAccount.checkContent ==> %s", checkContent)

	// 商户请求我们接口时使用商户私钥对请求参数 进行加密
	mchPriKey := fmt.Sprintf(`-----BEGIN PRIVATE KEY-----
%s
-----END PRIVATE KEY-----
`, config.Server.TopPay.MCH_PRIVATE_KEY)

	if err := public.RSA.SetPrivateKey(mchPriKey); err != nil {
		log.Error("toppay.queryAccount set private key :%v ==> %+v", err, params)
		return -1
	}

	// 私钥加密
	prienctypt, err := public.RSA.PriKeyENCTYPT([]byte(checkContent))
	if err != nil {
		log.Error("kaya.queryAccount RSA.PriKeyENCTYPT err %v", err)
		return -1
	}

	req.Sign = base64.StdEncoding.EncodeToString(prienctypt)
	// log.Debug("toppay.queryAccount req ==> %+v ", params)

	buf, _ := json.Marshal(req)
	respBody := public.HttpPostByJson(config.Server.TopPay.Url_queryAccount, string(buf))
	log.Debug("toppay.queryAccount req ==> %+v  resp ==> %+v", params, respBody)

	var resp queryAccount_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("toppay.queryAccount json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		return -1
	}

	log.Debug("toppay.queryAccount resp ==> %+v", resp)

	// 请求响应码
	if resp.PlatRespCode != RESP_CODE_SUCCESS {
		log.Error("toppay.queryAccount post return resp fail ==> %+v", resp)
		return -1
	}

	balance, err := strconv.Atoi(resp.Balance)
	if err != nil {
		log.Error("toppay.queryAccount get balance %v", err)
		return -1
	}

	return balance
}

// 提现审核
func WithdrawAudit(c *gin.Context) {
	obj := db.NewKayaWithdrawAudit()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "toppay.WithdrawAudit", err)
		return
	}

	obj.DoAction(nil)

	// 审核失败
	if obj.Out.RetCode != 1 {
		c.JSON(http.StatusOK, obj.Out)
		return
	}

	log.Debug("toppay.WithdrawAudit obj.In=%+v obj.Out=%+v", obj.In, obj.Out)

	// 审核成功
	// 状态  2=同意  11=拒绝
	if obj.In.Status != 2 {
		go notification.AddNotification(obj.Out.UserID, notification.Notification_Gold, "")
		c.JSON(http.StatusOK, obj.Out)
		return
	}

	// 请求withdrawOrder的代码
	req := withdraw_req{
		MerchantCode: config.Server.TopPay.MerchantCode,
		OrderNum:     obj.Out.OrderID,
		Money:        obj.Out.Amount,
		Description:  "withdraw",
		Name:         obj.Out.RealName,
		BankCode:     obj.Out.BankName,
		Number:       obj.Out.BankCard,
		NotifyUrl:    config.Server.TopPay.Url_withdraw_notify,
		FeeType:      0,
		DateTime:     TIME_FORMAT,
	}

	params := url.Values{}
	params.Set("merchantCode", req.MerchantCode)
	params.Set("orderNum", req.OrderNum)
	params.Set("money", strconv.Itoa(req.Money))
	params.Set("description", req.Description)
	params.Set("name", req.Name)
	params.Set("bankCode", req.BankCode)
	params.Set("number", req.Number)
	params.Set("notifyUrl", req.NotifyUrl)
	params.Set("feeType", strconv.Itoa(req.FeeType))
	params.Set("dateTime", req.DateTime)

	// 生成签名
	checkContent := createEncryptStr(params)
	log.Debug("toppay.WithdrawAudit.checkContent ==> %s", checkContent)

	// 商户请求我们接口时使用商户私钥对请求参数 进行加密
	mchPriKey := fmt.Sprintf(`-----BEGIN PRIVATE KEY-----
%s
-----END PRIVATE KEY-----
`, config.Server.TopPay.MCH_PRIVATE_KEY)

	if err := public.RSA.SetPrivateKey(mchPriKey); err != nil {
		log.Error("toppay.WithdrawAudit set private key :%v ==> %+v", err, params)
		return
	}

	// 私钥加密
	prienctypt, err := public.RSA.PriKeyENCTYPT([]byte(checkContent))
	if err != nil {
		log.Error("toppay.WithdrawAudit RSA.PriKeyENCTYPT err %v", err)
		return
	}

	req.Sign = base64.StdEncoding.EncodeToString(prienctypt)
	// log.Debug("toppay.payOrder req ==> %+v ", params)

	buf, _ := json.Marshal(req)
	respBody := public.HttpPostByJson(config.Server.TopPay.Url_withdraw_order, string(buf))
	log.Debug("toppay.WithdrawAudit req ==> %+v  resp ==> %+v", string(buf), respBody)

	var resp withdraw_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("toppay.WithdrawAudit json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		c.String(http.StatusOK, "fail")
		return
	}

	log.Debug("toppay.WithdrawAudit resp ==> %+v", resp)

	// 请求响应码，00000表示成功，其他失败
	if resp.PlatRespCode != RESP_CODE_SUCCESS {
		log.Error("toppay.WithdrawAudit post return resp fail ==> %+v", resp)

		// 余额不足,退款
		if resp.PlatRespCode == RESP_CODE_FAIL && resp.PlatRespMessage == "insufficient account balance" {
			obj := db.NewKayaWithdrawNotify()
			obj.In.OrderID = req.OrderNum
			obj.In.DfTransactionId = resp.PlatOrderNum
			obj.In.Status = 0
			obj.In.DfDesc = fmt.Sprintf("%s", resp.PlatRespMessage)
			obj.In.Balance = 0
			obj.DoAction()

			log.Debug("toppay.WithdrawAudit obj.In=%+v obj.Out=%+v", obj.In, obj.Out)
		}

		c.JSON(http.StatusOK, obj.Out)
		return
	}

	// 更新银行信息
	objBank := db.NewBankInfoUp()
	objBank.In.UserID = obj.Out.UserID
	objBank.In.RealName = req.Name
	objBank.In.BankCard = req.Number
	objBank.In.BankName = req.BankCode
	objBank.In.Mobile = obj.Out.Mobile
	objBank.In.EMail = obj.Out.EMail
	objBank.In.Address = obj.Out.Address
	objBank.DoAction()

	log.Debug("toppay.WithdrawAudit need save bankInfo")

	c.JSON(http.StatusOK, obj.Out)
	return
}
