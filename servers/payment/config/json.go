package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	slog "bet24.com/log"
	uuid "github.com/satori/go.uuid"
)

var Server struct {
	LogLevel        string
	FileLevel       string
	LogPath         string
	Port            int
	Login           string
	Password        string
	Database        string
	Datasource      string
	ChannelUrl      string
	ChannelPassword string
	RedisDB         int
	PaymentID       string
	MonitorPort     int

	WSAddr     string
	CertFile   string
	KeyFile    string
	MaxConnNum int
	LastDay    int

	WebPort int
	WebKey  string

	TlsPort int    // HTTPS 端口
	TlsCert string // HTTPS 证书FILE
	TlsKey  string // HTTPS 证书KEY

	ServiceAddr string

	Updates []UpdateInfo

	Cropay     // 第三方支付信息
	ZShuiPay   // 聚合支付信息
	KayaPay    // Kaya支付信息
	LetsPay    // lets支付信息
	LiuPay     // liu支付信息
	TopPay     // toppay 支付信息
	FlashPay   // flash 支付信息
	PayerMax   // payerMax 支付信息
	DingpeiPay // dingpei 支付信息
	CrushPay   // crush 支付信息
	OPay       // oPay 支付信息
	PayMob     // payMob 支付信息
	Fawry      // fawry 支付信息
	Coda       // coda 支付信息
	LaPay      // la 支付信息
	Huawei     //华为支付配置信息

	PackageName string

	Facebooks           []Facebook `json:"facebook"` // facebook 信息
	Facebook_KEY        string     `json:"facebook.key"`
	Facebook_STATUS_URL string     `json:"facebook.status_url"`

	// 头像
	UploadDir string // 上传存储路径 "d:/wwwroot/domino.ss2007.com/face"
	UploadUrl string // 访问路径 "http://domino.ss2007.com/face"
}

type UpdateInfo struct {
	Version   int
	PartnerId string
	Title     string
	Desc      string
	Url       string
}

// cropay 第三方支付信息
type Cropay struct {
	Merchant_id         int    `json:"cropay.merchant_id"`
	Order_hash          string `json:"cropay.order_hash"`
	Url_pay_order       string `json:"cropay.url_pay_order"`
	Url_sync            string `json:"cropay.url_sync"`
	Url_succ_back       string `json:"cropay.url_succ_back"`
	Url_fail_back       string `json:"cropay.url_fail_back"`
	Url_referrer_domain string `json:"cropay.url_referrer_domain"`
	Url_resultPay       string `json:"cropay.url_resultPay"`
}

// 聚合支付
type ZShuiPay struct {
	GymchtId                  string `json:"zshuipay.gymchtId"`
	GymKey                    string `json:"zshuipay.gymKey"`
	Url_pay_order             string `json:"zshuipay.url_pay_order"`
	Url_pay_callback          string `json:"zshuipay.url_pay_callback"`
	Url_pay_notify            string `json:"zshuipay.url_pay_notify"`
	Url_withdraw_order        string `json:"zshuipay.url_withdraw_order"`
	Url_withdraw_notify       string `json:"zshuipay.url_withdraw_notify"`
	Url_withdraw_queryAccount string `json:"zshuipay.url_withdraw_queryAccount"`
}

// kaya支付
type KayaPay struct {
	MerchantCode        string `json:"kayapay.merchantCode"`
	PLAT_PUBLIC_KEY     string `json:"kayapay.platform_pubKey"`
	MCH_PRIVATE_KEY     string `json:"kayapay.merchant_priKey"`
	Url_pay_order       string `json:"kayapay.url_pay_order"`
	Url_pay_notify      string `json:"kayapay.url_pay_notify"`
	Url_withdraw_order  string `json:"kayapay.url_withdraw_order"`
	Url_withdraw_notify string `json:"kayapay.url_withdraw_notify"`
	Url_queryAccount    string `json:"kayapay.url_queryAccount"`
}

// lets支付
type LetsPay struct {
	MchId              string `json:"letspay.mchId"`
	MD5Key             string `json:"letspay.key"`
	Url_pay_order      string `json:"letspay.url_pay_order"`
	Url_pay_notify     string `json:"letspay.url_pay_notify"`
	Url_pay_return     string `json:"letspay.url_pay_return"`
	Url_withdraw_order string `json:"letspay.url_withdraw_order"`
	Url_witdraw_notify string `json:"letspay.url_withdraw_notify"`
	Url_queryAccount   string `json:"letspay.url_queryAccount"`
}

// liu 支付
type LiuPay struct {
	MerchantCode        string `json:"liupay.merchantCode"`
	MD5Key              string `json:"liupay.key"`
	Url_pay_order       string `json:"liupay.url_pay_order"`
	Url_pay_notify      string `json:"liupay.url_pay_notify"`
	Url_pay_page        string `json:"liupay.url_pay_page"`
	Url_withdraw_order  string `json:"liupay.url_withdraw_order"`
	Url_withdraw_search string `json:"liupay.url_withdraw_search"`
}

// topPay 支付
type TopPay struct {
	MerchantCode        string `json:"toppay.merchantCode"`
	PLAT_PUBLIC_KEY     string `json:"toppay.platform_pubKey"`
	MCH_PRIVATE_KEY     string `json:"toppay.merchant_priKey"`
	Url_pay_order       string `json:"toppay.url_pay_order"`
	Url_pay_notify      string `json:"toppay.url_pay_notify"`
	Url_withdraw_order  string `json:"toppay.url_withdraw_order"`
	Url_withdraw_notify string `json:"toppay.url_withdraw_notify"`
	Url_queryAccount    string `json:"toppay.url_queryAccount"`
}

// facebook
type Facebook struct {
	APP_ID     string `json:"app_id"`
	APP_SECRET string `json:"app_secret"`
}

// Flash支付
type FlashPay struct {
	MerchantCode        string `json:"flashpay.merchantCode"`
	PLAT_PUBLIC_KEY     string `json:"flashpay.platform_pubKey"`
	MCH_PRIVATE_KEY     string `json:"flashpay.merchant_priKey"`
	Url_pay_order       string `json:"flashpay.url_pay_order"`
	Url_pay_notify      string `json:"flashpay.url_pay_notify"`
	Url_withdraw_order  string `json:"flashpay.url_withdraw_order"`
	Url_withdraw_notify string `json:"flashpay.url_withdraw_notify"`
	Url_queryAccount    string `json:"flashpay.url_queryAccount"`
}

// payerMax支付
type PayerMax struct {
	MerchantAppId       string `json:"payerMax.merchantAppId"`
	MerchantNo          string `json:"payerMax.merchantNo"`
	PLAT_PUBLIC_KEY     string `json:"payerMax.platform_pubKey"`
	MCH_PRIVATE_KEY     string `json:"payerMax.merchant_priKey"`
	Url_pay_order       string `json:"payerMax.url_pay_order"`
	Url_pay_Notify      string `json:"payerMax.url_pay_notify"`
	Url_withdraw_order  string `json:"payerMax.url_withdraw_order"`
	Url_withdraw_Notify string `json:"payerMax.url_withdraw_notify"`
	FrontCallbackUrl    string `json:"payerMax.frontCallbackUrl"`
}

// Dingpei 支付
type DingpeiPay struct {
	MerchantNo          string `json:"dingpeipay.merchantNo"`
	MD5Key              string `json:"dingpeipay.key"`
	Type                string `json:"dingpeipay.type"`
	Url_pay_order       string `json:"dingpeipay.url_pay_order"`
	Url_pay_notify      string `json:"dingpeipay.url_pay_notify"`
	Url_pay_page        string `json:"dingpeipay.url_pay_page"`
	Url_withdraw_order  string `json:"dingpeipay.url_withdraw_order"`
	Url_withdraw_notify string `json:"dingpeipay.url_withdraw_notify"`
	Url_withdraw_search string `json:"dingpeipay.url_withdraw_search"`
}

// crush支付
type CrushPay struct {
	MerchantAppId       string `json:"crushpay.merchantAppId"`
	MerchantKey         string `json:"crushpay.merchantKey"`
	Url_pay_order       string `json:"crushpay.url_pay_order"`
	Url_pay_Notify      string `json:"crushpay.url_pay_notify"`
	Url_withdraw_order  string `json:"crushpay.url_withdraw_order"`
	Url_withdraw_Notify string `json:"crushpay.url_withdraw_notify"`
}

// OPay 支付
type OPay struct {
	MerchantId     string `json:"oPay.merchantId"`
	SecretKey      string `json:"oPay.secretKey"`
	PublicKey      string `json:"oPay.publicKey"`
	Url_pay_order  string `json:"oPay.url_pay_order"`
	Url_pay_Notify string `json:"oPay.url_pay_notify"`
	Url_pay_Return string `json:"oPay.url_pay_return"`
}

// PayMob 支付
type PayMob struct {
	HMAC                string `json:"payMob.HMAC"`
	Api_Key             string `json:"payMob.api_key"`
	Integration_Id      int    `json:"payMob.integration_id"`
	Url_Auth_Token      string `json:"payMob.url_auth_token"`
	Url_Order_Register  string `json:"payMob.url_order_register"`
	Url_Payment_Token   string `json:"payMob.url_payment_token"`
	Url_Payment_CashPay string `json:"payMob.url_payment_cashPay"`
}

// fawry 支付
type Fawry struct {
	MerchantCode  string `json:"fawry.merchantCode"`
	SecureKey     string `json:"fawry.secureKey"`
	Url_pay_order string `json:"fawry.url_pay_order"`
}

// coda 支付
type Coda struct {
	ApiKeys        []CodaKey `json:"coda.apiKeys"`
	Url_pay_init   string    `json:"coda.url_pay_init"`
	Url_pay_begin  string    `json:"coda.url_pay_begin"`
	Url_pay_Notify string    `json:"coda.url_pay_notify"`
	Url_pay_Return string    `json:"coda.url_pay_return"`
}

type CodaKey struct {
	Currency     string `json:"currency"`
	CountryCode  int    `json:"countryCode"`
	CurrencyCode int    `json:"currencyCode"`
	ApiKey       string `json:"apiKey"`
	PaymentTypes []int  `json:"paymentTypes"`
}

// laPay 支付
type LaPay struct {
	MerchantNo    string `json:"lapay.merchantNo"`
	MerchantCode  string `json:"lapay.merchantCode"`
	PlatPublicKey string `json:"lapay.platPublicKey"`
	MchPrivateKey string `json:"lapay.mchPrivateKey"`
	UrlPayOrder   string `json:"lapay.urlPayOrder"`
	UrlPayNotify  string `json:"lapay.urlPayNotify"`
	CallbackUrl   string `json:"LaPay.CallbackUrl"`
}

// 华为支付配置信息
type Huawei struct {
	ClientSecret         string `json:"huawei.ClientSecret"`
	ClientId             string `json:"huawei.ClientId"`
	TokenUrl             string `json:"huawei.TokenUrl"`
	ApplicationPublicKey string `json:"huawei.ApplicationPublicKey"`
}

func Run() {
	data, err := os.ReadFile("fishconf/payment.json")
	if err != nil {
		log.Fatalf("%v", err)
	}
	fmt.Println(string(data))
	err = json.Unmarshal(data, &Server)
	if err != nil {
		log.Fatalf("%v", err)
	}

	logger, err := slog.New(Server.LogLevel, Server.FileLevel, Server.LogPath, log.LstdFlags)
	if err == nil {
		slog.Export(logger)
	}

	id, _ := uuid.NewV4()
	Server.PaymentID = id.String()
	//slog.Debug("Server.PaymentID = %v", Server.PaymentID)
	if Server.MonitorPort == 0 {
		Server.MonitorPort = Server.WebPort + 100
	}

	if Server.PackageName == "" {
		Server.PackageName = "com.shengsheng.domino"
	}
	now := time.Now()
	Server.LastDay = now.Day()
	refreshLogFile()
}

func refreshLogFile() {
	time.AfterFunc(10*time.Minute, refreshLogFile)
	doRefreshLogFile()
}

func doRefreshLogFile() {
	if Server.LogPath != "" {
		now := time.Now()
		if now.Day() != Server.LastDay {
			Server.LastDay = now.Day()
			slog.RecreateFileLog(Server.LogPath, log.LstdFlags)
		}
	}
}
