package huawei

type purchaseData struct {
	AutoRenewing       bool   `json:"autoRenewing"`
	OrderId            string `json:"orderId"`
	PackageName        string `json:"packageName"`
	ApplicationId      int    `json:"applicationId"`
	Kind               int    `json:"kind"`
	ProductId          string `json:"productId"`
	ProductName        string `json:"productName"`
	PurchaseTime       int    `json:"purchaseTime"`
	PurchaseTimeMillis int    `json:"purchaseTimeMillis"`
	PurchaseState      int    `json:"purchaseState"`
	PurchaseToken      string `json:"purchaseToken"`
	ConsumptionState   int    `json:"consumptionState"`
	Confirmed          int    `json:"confirmed"`
	Currency           string `json:"currency"`
	Price              int    `json:"price"`
	Country            string `json:"country"`
	PayOrderId         string `json:"payOrderId"`
	PayType            string `json:"payType"`
	ResponseCode       string `json:"responseCode,omitempty"`
	DeveloperPayload   string `json:"developerPayload,omitempty"`
}

type data struct {
	PurchaseTokenData string `json:"purchaseTokenData"`
	ResponseCode      string `json:"responseCode"`
	DataSignature     string `json:"dataSignature"`
}
