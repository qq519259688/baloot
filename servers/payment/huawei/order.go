/*
 * Copyright 2020. Huawei Technologies Co., Ltd. All rights reserved.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */
package huawei

import (
	"bet24.com/log"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"encoding/json"
	"fmt"
)

type OrderClient struct {
}

var Client = &OrderClient{}

func getOrderUrl(accountFlag int) string {
	if accountFlag == 1 {
		// site for telecom carrier
		return "https://orders-at-dre.iap.dbankcloud.com"
	} else {
		// TODO: replace the (ip:port) to the real one
		return "https://orders-drcn.iap.hicloud.com"
	}

}

func (oc *OrderClient) VerifyToken(purchaseToken, productId string, accountFlag int) (int, error) {
	bodyMap := map[string]string{"purchaseToken": purchaseToken, "productId": productId}
	url := getOrderUrl(accountFlag) + "/applications/purchases/tokens/verify"
	bodyBytes, err := SendRequest(url, bodyMap)
	if err != nil {
		log.Error("huawei.VerifyToken.SendRequest err is %s", err)
		return 0, err
	}
	// TODO: display the response as string in console, you can replace it with your business logic.
	log.Debug("order.VerifyToken ==>%s", bodyBytes)

	resp := &data{}
	err = json.Unmarshal([]byte(bodyBytes), resp)
	if err != nil {
		log.Error("huawei.VerifyToken.Unmarshal err is %v", err)
		return 0, err
	}

	//验签
	err = VerifyRsaSign(resp.PurchaseTokenData, resp.DataSignature, config.Server.Huawei.ApplicationPublicKey)
	if err != nil {
		log.Error("huawei.VerifyToken.VerifyRsaSign err is %v", err)
		return 0, err
	}

	log.Debug("huawei.VerifyToken Verify Success!")

	purchase := purchaseData{}
	err = json.Unmarshal([]byte(resp.PurchaseTokenData), &purchase)
	if err != nil {
		log.Error("huawei.VerifyToken.VerifyRsaSign.Unmarshal err is %v", err)
		return 0, err
	}

	notify := db.NewNotify(db.SP_HuaweiPay_NOTIFY)
	notify.In.OrderID = purchase.DeveloperPayload
	notify.In.Price = float64(purchase.Price)
	notify.In.TradeID = purchase.OrderId
	notify.DoAction(nil)

	// 操作成功,给道具
	if notify.Out.RetCode == 1 {
		// 充值
		resp := coreClient.Recharge(notify.Out.UserID, notify.Out.ProductID)
		log.Debug("%s 充值成功 %+v", "huawei.VerifyToken.Notify", resp)
	}

	return notify.Out.RetCode, nil
}

func (oc *OrderClient) CancelledListPurchase(endAt int64, startAt int64, maxRows int, productType int, continuationToken string, accountFlag int) {
	bodyMap := map[string]string{
		"endAt":             fmt.Sprintf("%v", endAt),
		"startAt":           fmt.Sprintf("%v", startAt),
		"maxRows":           fmt.Sprintf("%v", maxRows),
		"type":              fmt.Sprintf("%v", productType),
		"continuationToken": continuationToken,
	}
	url := getOrderUrl(accountFlag) + "/applications/v2/purchases/cancelledList"
	bodyBytes, err := SendRequest(url, bodyMap)
	if err != nil {
		log.Error("err is %s", err)
	}
	// TODO: display the response as string in console, you can replace it with your business logic.
	log.Debug("%s", bodyBytes)
}

func (oc *OrderClient) confirmPurchase(productId, purchaseToken string, accountFlag int) {
	bodyMap := map[string]string{
		"purchaseToken": purchaseToken,
		"productId":     productId,
	}
	url := getOrderUrl(accountFlag) + "/applications/v2/purchases/confirm"
	bodyBytes, err := SendRequest(url, bodyMap)
	if err != nil {
		log.Error("err is %s", err)
	}
	// TODO: display the response as string in console, you can replace it with your business logic.
	log.Debug("%s", bodyBytes)
}
