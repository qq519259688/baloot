package huawei

import (
	"encoding/json"
	"net/http"
	"strings"

	"bet24.com/log"
	"bet24.com/servers/payment/db"
	"bet24.com/servers/payment/shop"
	"github.com/gin-gonic/gin"
)

// 下单
func Order(c *gin.Context) {
	obj := db.NewOrder(db.SP_HuaweiPay_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "huawei.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "huawei.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "huawei.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "huawei.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "huawei.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "huawei.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	c.String(http.StatusOK, obj.Out.OrderID)
	return
}

// 回调
func Notify(c *gin.Context) {
	_, err := AtDemo.GetAppAt()
	if err != nil {
		log.Error("%s GetAppAt err is %v", "huawei.Notify", err)
		return
	}

	//获取参数
	reqPurchaseData := strings.Join(c.Request.Form["inAppPurchaseData"], "")
	//log.Debug("reqPurChaseData ==> %s", reqPurchaseData)

	//解析参数
	req := purchaseData{}
	if err := json.Unmarshal([]byte(reqPurchaseData), &req); err != nil {
		log.Debug("%s Query Params reqPurchaseData Unmarshal err %v", "huawei.Notify", err)
		return
	}

	//log.Debug("解析参数：%+v", req)

	//验证token有效性
	retCode, _ := Client.VerifyToken(req.PurchaseToken, req.ProductId, 0)
	c.JSON(http.StatusOK, struct{ RetCode int }{RetCode: retCode})
	return
}
