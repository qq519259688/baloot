package update

type checkUpdateReq struct {
	Version   int    `form:"version"`
	PartnerID string `form:"PartnerID"`
}
