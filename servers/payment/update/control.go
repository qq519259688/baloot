package update

import (
	"fmt"
	"net/http"

	"bet24.com/log"
	"bet24.com/servers/payment/config"
	"github.com/gin-gonic/gin"
)

//检查更新
func CheckUpdate(c *gin.Context) {
	req := checkUpdateReq{}
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s query params err %v", "update.CheckUpdate", err)
		return
	}

	log.Debug("update.CheckUpdate ==> %+v", req)

	for _, v := range config.Server.Updates {
		if v.Version <= req.Version {
			continue
		}
		if v.PartnerId != req.PartnerID {
			continue
		}

		c.String(http.StatusOK, fmt.Sprintf("1|%s|%s|%s", v.Title, v.Desc, v.Url))
		return
	}
	c.String(http.StatusOK, "")
	return
}
