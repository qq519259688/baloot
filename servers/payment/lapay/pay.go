package lapay

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"bet24.com/servers/payment/shop"

	"bet24.com/log"
	"bet24.com/public"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_LaPay_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "laPay.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	//// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "lapay.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "lapay.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "lapay.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "lapay.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "laPay.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	t := strconv.Itoa(int(time.Now().UnixNano() / 1e6))

	obj.In.Name = returnDefaultVal(obj.In.Name, public.RandStr(12))

	out := inputParameter{
		OrderID:   obj.Out.OrderID,
		PayModel:  obj.In.PayModel,
		Price:     obj.In.Price,
		Tel:       obj.In.Tel,
		Country:   obj.In.Country,
		Email:     obj.In.Email,
		Name:      obj.In.Name,
		CardNum:   obj.In.CardNum,
		IpAddress: obj.In.IpAddress,
		Currency:  obj.In.Currency,
		T:         t,
	}

	params := url.Values{}
	var err error
	//if out.PayModel == PAY_MODEL {
	//	err = walletMode(params, out)
	//} else {
	out.PayModel = PAY_MODEL2
	err = bankcardMode(params, out)
	//}
	if err != nil {
		return
	}

	respBody := public.HttpPostForm(config.Server.LaPay.UrlPayOrder, params)

	log.Debug("laPay.PayOrder post respBody ==> %+v", respBody)

	var resp respApiReturn
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("laPay.PayOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		return
	}

	// 返回码，’0000’代表成功
	if resp.Meta.Code != "0000" {
		log.Error("laPay.PayOrder post return resp fail ==> %+v params ==> %+v", resp, params)
		return
	}

	// 跳转到支付页面，以便持卡人完成支付过程
	if resp.Data.WebUrl != "" {
		c.Redirect(http.StatusMovedPermanently, resp.Data.WebUrl)
		return
	}
	c.Redirect(http.StatusMovedPermanently, config.Server.LaPay.CallbackUrl)
	return
}

// 返回默认值
func returnDefaultVal(val, randStr string) string {
	if val == "" {
		return randStr
	}
	return val
}

// todo 后续有新增直接复制银行卡模式的数据进行调整
// 钱包模式
//func walletMode(params url.Values, out inputParameter) error {
//	return nil
//}

// 银行卡模式
func bankcardMode(params url.Values, out inputParameter) error {
	params.Add("merchantNo", config.Server.LaPay.MerchantNo)
	params.Add("timeStamp", out.T)
	params.Add("merchantOrderNo", out.OrderID)
	params.Add("payModel", out.PayModel)
	params.Add("orderAmount", fmt.Sprintf("%d", int(out.Price*100))) // 订单金额，正整数，单位：分
	params.Add("cardNo", out.CardNum)
	params.Add("cardType", CARD_TYPE)
	params.Add("cardCcv", strconv.Itoa(rand.Intn(1000)))
	params.Add("cardExpMonth", time.Now().Format("01"))
	params.Add("cardExpYear", time.Now().Format("2006"))
	params.Add("phone", out.Tel)
	params.Add("country", out.Country)
	params.Add("email", out.Email)
	params.Add("callbackUrl", config.Server.LaPay.UrlPayNotify)
	params.Add("payType", PAY_TYPE)

	params.Add("productDetail", "This is a commodity detail")
	params.Add("address", out.IpAddress)
	params.Add("firstName", out.Name)
	params.Add("lastName", out.Name)
	params.Add("city", out.Country)

	if err := public.MD5withRSASign(params, config.Server.LaPay.MchPrivateKey); err != nil {
		log.Error("laPay.PayOrder MD5withRSASign req ==> %+v fail ==> %+v", params, err)
		return err
	}

	key := config.Server.LaPay.MerchantCode

	nos, err := public.Sha1Encrypt([]byte(out.CardNum), []byte(key))
	if err != nil {
		log.Error("laPay.PayOrder createRequest Sha1Encrypt out.CardNum req ==> %+v fail ==> %+v",
			out.CardNum, err)
		return err
	}

	ccv, err := public.Sha1Encrypt([]byte(params.Get("cardCcv")), []byte(key))
	if err != nil {
		log.Error("laPay.PayOrder createRequest Sha1Encrypt cardCcv req ==> %+v fail ==> %+v",
			params.Get("cardCcv"), err)
		return err
	}

	params.Set("cardNo", nos)
	params.Set("cardCcv", ccv)

	params.Set("productDetail", url.QueryEscape(params.Get("productDetail")))
	params.Set("firstName", url.QueryEscape(params.Get("firstName")))
	params.Set("lastName", url.QueryEscape(params.Get("lastName")))
	params.Set("city", url.QueryEscape(params.Get("city")))

	params.Add("idendifyNumber", "27010200202071")
	params.Add("version", "1")

	params.Add("state", "NY")
	params.Add("postcode", "056400")
	params.Add("userIp", out.IpAddress)

	// 以下字段暂不需要
	//params.Add("currency", out.Currency)
	//params.Add("validPeriod", "")
	//params.Add("language", "en")
	//params.Add("userAgent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36")
	//params.Add("noticeUrl", "")
	//params.Add("customParam", "")
	//params.Add("expandField", "")
	return nil
}

// 回调通知
func PayNotify(c *gin.Context) {
	respJson := strings.Join(c.Request.Form["data"], "")
	if respJson == "" {
		log.Debug("%s strings.Join c.Request.Form['data'] is empty %+v",
			"laPay.PayNotify", c.Request.Form)
		c.String(http.StatusOK, "")
		return
	}

	var resp payNotifyData
	if err := json.Unmarshal([]byte(respJson), &resp); err != nil {
		log.Debug("%s json.Unmarshal respJson err %v", "laPay.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("laPay.PayNotify resp ==> %+v", resp)

	// 返回码，’SUCCESS’代表成功
	if resp.OrderStatus != "SUCCESS" {
		log.Error("laPay.PayNotify resp ==> %+v 失败", resp)
		c.JSON(http.StatusOK, ret{
			Code: "SUCCESS",
			Msg:  "Success",
		})
		return
	}

	params := url.Values{}
	params.Add("merchantNo", resp.MerchantNo)
	params.Add("merchantOrderNo", resp.MerchantOrderNo)
	params.Add("orderNo", resp.OrderNo)
	params.Add("currency", resp.Currency)
	params.Add("orderAmount", strconv.Itoa(resp.OrderAmount))
	params.Add("orderFee", strconv.Itoa(resp.OrderFee))
	params.Add("payType", resp.PayType)
	params.Add("payModel", resp.PayModel)
	params.Add("orderStatus", resp.OrderStatus)

	joint := createEncryptStr(params)
	resDecode := public.MD5withRSADecrypt(resp.Sign, config.Server.LaPay.MchPrivateKey)
	if joint != resDecode {
		log.Debug("%s public.MD5DecryptLong decryption failure ==> %s", "laPay.PayNotify", resp.Sign)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("laPay.PayNotify 解密成功")

	// 数据库操作
	obj := db.NewNotify(db.SP_LaPay_NOTIFY)
	obj.In.OrderID = resp.MerchantOrderNo
	obj.In.TradeID = resp.OrderNo
	//obj.In.Price = resp.OrderAmount
	obj.DoAction(nil)

	// 操作成功,给道具
	if obj.Out.RetCode == 1 {
		// 充值
		resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
		log.Debug("%s 充值成功 %+v", "laPay.PayNotify", resp)
	}

	c.JSON(http.StatusOK, ret{
		Code: "SUCCESS",
		Msg:  "Success",
	})
	return
}

// 支付完成跳转处理
func PayCallback(c *gin.Context) {
	c.String(http.StatusOK, "success")
	log.Debug("lapay.payCallback")
}
