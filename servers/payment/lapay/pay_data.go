package lapay

const (
	PAY_TYPE = "EGYPT_PAY" // 支付类型：埃及，默认是INTERNATIONAL_CARD_PAY国际卡支付

	PAY_MODEL  = "PAYMOB_WALLET" // 支付模式：Paymob Wallet 钱包
	PAY_MODEL2 = "CARDPAYMENT"   // 支付模式：（埃及收单）银行卡支付，所以默认国际卡

	CARD_TYPE = "MASTERCARD" // payType为“国际卡支付”时需要：【VISA、MASTERCARD、JCB、AMEX、UPI、DISCOVER、MIR、DinersClub、RuPay、BCcard、DinaCard、WEB】 备注：为WEB时，卡信息非必传
)

type ret struct {
	Code string `json:"code" form:"code"` // 响应 ‘SUCCESS’
	Msg  string `json:"msg" form:"msg"`   // 响应 ‘Success’
}

// 文档格式
// R    该参数为必填项
// O    参数可选
// C    该参数在一定条件下出现（有条件）
// Y/N  签署

// 下单接口（网关模式）
type (
	payReqData struct {
		MerchantNo      string `json:"merchantNo" form:"merchantNo"`           // R  Y  系统分配的唯一标识符
		MerchantOrderNo string `json:"merchantOrderNo" form:"merchantOrderNo"` // R  Y  商户订单号
		TimeStamp       string `json:"timeStamp" form:"timeStamp"`             // R  Y  标准时间 (GMT) 的当前时间戳。见备注 【Timestamp】
		OrderAmount     string `json:"orderAmount" form:"orderAmount"`         // R  Y  订单金额，正整数， 单位：分
		Currency        string `json:"currency" form:"currency"`               // R  Y  币种，见备注 【币种】
		ProductDetail   string `json:"productDetail" form:"productDetail"`     // R  Y  商品详情
		CallbackUrl     string `json:"callbackUrl" form:"callbackUrl"`         // R  Y  系统异步回调地址
		NoticeUrl       string `json:"noticeUrl" form:"noticeUrl"`             // R  N  页面通知地址
		CustomParam     string `json:"customParam" form:"customParam"`         // O  N  当我们需要重定向到 noticeUrl 时，会将此参数返回给您，最多 500 位
		Country         string `json:"country" form:"country"`                 // C  Y  非必传，国家
		City            string `json:"city" form:"city"`                       // C  Y  非必传，市
		Phone           string `json:"phone" form:"phone"`                     // C  Y  非必传，手机号
		Sign            string `json:"sign" form:"sign"`                       // R  N  签名，根据签名步骤生成加密字符串，并使用加密方式[RsaUtil. Sign] .
		Version         string `json:"version" form:"version"`                 // R  N  1
		MerType         string `json:"merType" form:"merType"`                 // C  N  非必传，值为0/1，默认为0，当值为1时，收银台phone、address、city、country、state、postCode字段不收集,反之需收集
	}
)

// api下单
type (
	respApiReturn struct {
		Meta respApiMeta `json:"meta" form:"meta"`
		Data respApiData `json:"data" form:"data"`
	}

	respApiMeta struct {
		Success bool   `json:"success" form:"success"`
		Code    string `json:"code" form:"code"`
		Message string `json:"message" form:"message"`
	}

	respApiData struct {
		MerchantNo      string `json:"merchantNo" form:"MerchantNo"`           // Y 系统中商户的唯一标识
		MerchantOrderNo string `json:"merchantOrderNo" form:"MerchantOrderNo"` // Y 自定义系统订单号（不能重复，否则会被拒绝）
		OrderNo         string `json:"orderNo" form:"OrderNo"`                 // Y 业务订单号，系统订单的唯一标识
		Currency        string `json:"currency" form:"Currency"`               // Y 币种，见备注 【币种】
		OrderAmount     int    `json:"orderAmount" form:"OrderAmount"`         // Y 订单金额，正整数，单位：分
		OrderFee        int    `json:"orderFee" form:"OrderFee"`               // Y 订单费用金额，正整数，单位：分
		PayModel        string `json:"payModel" form:"PayModel"`               // Y 见备注 【PayModel】
		OrderStatus     string `json:"orderStatus" form:"OrderStatus"`         // Y 订单状态，见备注 【订单状态】
		WebUrl          string `json:"webUrl" form:"WebUrl"`                   // Y 需要访问/重定向的url，当pauType为“印尼支付”时，注意：1、印尼支付中直连（payModel为DIRECT），此字段返回VA ，具体情况以实际沟通为准
		Page            string `json:"page" form:"Page"`                       // N 显示给用户的 html 页面
		OrderTime       int    `json:"orderTime" form:"OrderTime"`             // N N
		FinishTime      int    `json:"finishTime" form:"FinishTime"`           // N
		Sign            string `json:"sign" form:"Sign"`                       // N 该字段记录了交易通道的返回信息，例如:descriptor
		Remark          string `json:"remark" form:"Remark"`                   // Y 签名，根据签名步骤生成加密字符串，使用加密方式 [RsaUtil. Sign] .
	}

	// 入参
	inputParameter struct {
		OrderID   string  `json:"orderId" form:"orderId" desc:"订单号"`
		PayModel  string  `json:"payModel" form:"payModel" desc:"支付模式"`
		Price     float64 `json:"price" form:"price" desc:"价格"`
		Tel       string  `json:"tel" form:"tel" desc:"电话"`
		Country   string  `json:"country" form:"country" desc:"国家"`
		Email     string  `json:"email" form:"email" desc:"邮箱"`
		Name      string  `json:"name" form:"name" desc:"姓名"`
		CardNum   string  `json:"cardNum" form:"cardNum" desc:"银行卡号"`
		IpAddress string  `json:"ipAddress" form:"ipAddress" desc:"IP地址"`
		Currency  string  `json:"currency" form:"currency" desc:"币种"`
		T         string  `json:"t" form:"t" desc:"当前时间，单位：毫秒"`
	}
)

// 回调通知
type (
	payNotifyData struct {
		Msg             string `json:"msg" form:"msg"`
		FinishTime      int64  `json:"finishTime" form:"finishTime"`
		OrderNo         string `json:"orderNo" form:"orderNo"`                 // Y 业务订单号，系统订单的唯一标识
		Sign            string `json:"sign" form:"sign"`                       // N 签名，根据签名步骤生成加密字符串，使用加密方式 [RsaUtil. Sign] .
		OrderFee        int    `json:"orderFee" form:"orderFee"`               // Y 手续费，正整数，单位：分
		OrderStatus     string `json:"orderStatus" form:"orderStatus"`         // Y 订单状态，见备注 【订单状态】
		PayModel        string `json:"payModel" form:"payModel"`               // Y
		MerchantOrderNo string `json:"merchantOrderNo" form:"merchantOrderNo"` // Y 自定义系统订单号（不能重复，否则会被拒绝）
		OrderAmount     int    `json:"orderAmount" form:"orderAmount"`         // Y 订单金额，正整数，单位：分
		PayType         string `json:"payType" form:"payType"`                 // Y
		OrderTime       int64  `json:"orderTime" form:"orderTime"`
		Currency        string `json:"currency" form:"currency"`     // Y 币种，见备注 【币种】
		MerchantNo      string `json:"merchantNo" form:"merchantNo"` // Y 系统中商户的唯一标识
	}
)
