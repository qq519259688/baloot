package flash

const (
	// ARRIVED，SUCCESS，CLEARED都可以认为是收款成功
	PAY_STATUS_CREATED = "CREATED" // 订单初始化
	PAY_STATUS_ARRIVED = "ARRIVED" // 已收款未结算
	PAY_STATUS_SUCCESS = "SUCCESS" // 已收款，入账冻结账户
	PAY_STATUS_CLEARED = "CLEARED" // 收款成功
	PAY_STATUS_FAILED  = "FAILED"  // 失败

)

// 代收下单
type (
	pay_req struct {
		MerchantNo      string  `json:"merchantNo" form:"merchantNo"`                         // 支付系统提供给合作商户的唯一标识
		MerchantOrderNo string  `json:"merchantOrderNo" form:"merchantOrderNo"`               // 商户唯一订单
		PayAmount       float64 `json:"payAmount" form:"payAmount"`                           // 支付金额
		Mobile          string  `json:"mobile" form:"mobile"`                                 // 付款人手机号，越真实，成功率越高，触发风控系数越低
		Name            string  `json:"name" form:"name"`                                     // 付款人姓名，越真实，成功率越高，触发风控系数越低
		Method          string  `json:"method" form:"method"`                                 // 渠道
		ExpiryPeriod    string  `json:"expiryPeriod,omitempty" form:"expiryPeriod,omitempty"` // 有效期分钟
		Email           string  `json:"email,omitempty" form:"email,omitempty"`               // 付款人邮箱，越真实，成功率越高，触发风控系数越低
		NotifyUrl       string  `json:"notifyUrl,omitempty" form:"notifyUrl,omitempty"`       // 回调地址，如果不传，则读取商户后台配置, 去掉转义字符“\”,参考格式 https://www.abc.com/
		ClientNo        string  `json:"clientNo,omitempty" form:"clientNo,omitempty"`         // 多商户调用区分值，非必传
		Description     string  `json:"description" form:"description"`                       // 描述
		Sign            string  `json:"sign" form:"sign"`
	}

	pay_resp struct {
		Status  string        `json:"status" form:"status"`   // 请求状态
		Message string        `json:"message" form:"message"` // 请求信息
		Data    pay_resp_data `json:"data" form:"data"`       // 数据体（以下为数据体属性）
	}

	pay_resp_data struct {
		OrderStatus     string `json:"orderStatus" form:"orderStatus"`                     // 订单状态
		OrderMessage    string `json:"orderMessage" form:"orderMessage"`                   // 订单状态描述
		PlatOrderNo     string `json:"platOrderNo" form:"platOrderNo"`                     // FlashPay订单号
		MerchantOrderNo string `json:"merchantOrderNo" form:"merchantOrderNo"`             // 商户订单号回传
		Method          string `json:"method" form:"method"`                               // 渠道
		Name            string `json:"name" form:"name"`                                   // 用户名
		Email           string `json:"email" form:"email"`                                 // 邮箱
		AccountNumber   string `json:"accountNumber" form:"accountNumber"`                 // 付款va码，或者付款链接
		PaymentUrl      string `json:"paymentUrl" form:"paymentUrl"`                       // 付款链接
		PayAmount       int    `json:"payAmount" form:"payAmount"`                         // 商户订单金额
		MerchantFee     string `json:"merchantFee,omitempty" form:"merchantFee,omitempty"` // 商户订单手续费
		Description     string `json:"description" form:"description"`                     // 商户订单描述
	}
)

// 代收回调通知
type payNotify struct {
	Amount          string `json:"amount" form:"amount"`                   // 付款金额
	FactAmount      string `json:"factAmount" form:"factAmount"`           // 付款实际金额
	MerchantOrderNo string `json:"merchantOrderNo" form:"merchantOrderNo"` // 商户唯一订单号
	PlatOrderNo     string `json:"platOrderNo" form:"platOrderNo"`         // flashPay订单号
	OrderStatus     string `json:"orderStatus" form:"orderStatus"`         // 订单状态
	OrderMessage    string `json:"orderMessage" form:"orderMessage"`       // 订单状态描述
	Sign            string `json:"sign" form:"sign"`                       // 签名
}
