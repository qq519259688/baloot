package flash

const (
	// 除了SUCCESS是成功，FAILED失败，其他状态请调用方标记为处理中，等待回调或者查询接口返回最终状态
	WITHDRAW_STATUS_CREATED = "CREATED" // 订单初始化
	WITHDRAW_STATUS_PENDING = "PENDING" // 订单处理中
	WITHDRAW_STATUS_SUCCESS = "SUCCESS" // 成功
	WITHDRAW_STATUS_FAILED  = "FAILED"  // 失败
)

// 代付下单
type (
	withdraw_req struct {
		MerchantNo      string  `json:"merchantNo" form:"merchantNo"`                   // 支付系统提供给合作商户的唯一标识
		MerchantOrderNo string  `json:"merchantOrderNo" form:"merchantOrderNo"`         // 商户唯一订单
		Description     string  `json:"description" form:"description"`                 // 订单描述
		PayAmount       float64 `json:"payAmount" form:"payAmount"`                     // 支付金额
		BankCode        string  `json:"bankCode" form:"bankCode"`                       // 渠道
		Mobile          string  `json:"mobile" form:"mobile"`                           // 收款人手机号，越真实，成功率越高，触发风控系数越低
		BankNumber      string  `json:"bankNumber" form:"bankNumber"`                   // 收款银行卡号
		AccountHoldName string  `json:"accountHoldName" form:"accountHoldName"`         // 收款人姓名，越真实，成功率越高，触发风控系数越低
		NotifyUrl       string  `json:"notifyUrl,omitempty" form:"notifyUrl,omitempty"` // 回调地址，如果不传，则读取商户后台配置, 不要有转义字符
		ClientNo        string  `json:"clientNo,omitempty" form:"clientNo,omitempty"`   // 多商户调用区分值，非必传
		Sign            string  `json:"sign" form:"sign"`
	}

	withdraw_resp struct {
		Status  string             `json:"status" form:"status"`   // 请求状态
		Message string             `json:"message" form:"message"` // 请求信息
		Data    withdraw_resp_data `json:"data" form:"data"`       // 数据体（以下为数据体属性）
	}

	withdraw_resp_data struct {
		OrderStatus     string `json:"orderStatus" form:"orderStatus"`         // 订单状态
		OrderMessage    string `json:"orderMessage" form:"orderMessage"`       // 订单状态描述
		MerchantOrderNo string `json:"merchantOrderNo" form:"merchantOrderNo"` // 商户订单号回传
		PlatOrderNo     string `json:"platOrderNo" form:"platOrderNo"`         // FlashPay订单号
		MerchantFee     string `json:"merchantFee" form:"merchantFee"`         // 手续费
	}
)

// 代付回调通知
type withdrawNotify struct {
	Amount          string `json:"amount" form:"amount"`                   // 付款金额
	MerchantNo      string `json:"merchantNo" form:"merchantNo"`           // 支付系统提供给合作商户的唯一标识
	MerchantOrderNo string `json:"merchantOrderNo" form:"merchantOrderNo"` // 商户唯一订单号
	PlatOrderNo     string `json:"platOrderNo" form:"platOrderNo"`         // flashPay订单号
	OrderStatus     string `json:"orderStatus" form:"orderStatus"`         // 订单状态
	OrderMessage    string `json:"orderMessage" form:"orderMessage"`       // 订单状态描述
	Sign            string `json:"sign" form:"sign"`                       // 签名
}

// 账户余额查询请求、响应
type (
	queryAccount_req struct {
		MerchantNo string `json:"merchantNo" form:"merchantNo"` // 商户号
		Timestamp  string `json:"timestamp" form:"timestamp"`   // 时间戳
		Sign       string `json:"sign" form:"sign"`             // 签名
	}

	queryAccount_resp struct {
		Status  string                 `json:"status" form:"status"`   // 结果代码： "SUCCESS" "FAIL" "UNKNOWN" "NOTEXIST" "ERROR"
		Message string                 `json:"message" form:"message"` // 响应代码说明
		Data    queryAccount_resp_data `json:"data" form:"data"`       // 数据体（以下为数据体属性）
	}

	queryAccount_resp_data struct {
		AvailableAmount float64 `json:"availableAmount" form:"availableAmount"` // 可用余额
		FreezeAmount    float64 `json:"freezeAmount" form:"freezeAmount"`       // 冻结余额
		TotalAmount     float64 `json:"totalAmount" form:"totalAmount"`         // 总额，可用+冻结
		Sign            string  `json:"sign,omitempty" form:"sign,omitempty"`   // 签名
	}
)
