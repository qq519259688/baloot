package flash

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 提现请求
func WithdrawOrder(c *gin.Context) {
	obj := db.NewFlashWithdrawReq()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "flash.WithdrawOrder", err)
		c.String(http.StatusOK, "fail")
		return
	}

	obj.In.IPAddress = strings.Split(c.Request.RemoteAddr, ":")[0]
	obj.DoAction()
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v obj.Out=%+v", "flash.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	if obj.Out.GetStatus != 0 {
		log.Debug("%s GenOrder Audit obj.In=%+v obj.Out=%+v", "flash.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	// 请求时，没有传手机号
	if obj.In.Mobile == "" {
		obj.In.Mobile = obj.Out.Tel
	}

	// 请求withdrawOrder的代码
	req := withdraw_req{
		MerchantNo:      config.Server.FlashPay.MerchantCode,
		MerchantOrderNo: obj.Out.OrderID,
		Description:     "withdraw",
		PayAmount:       float64(obj.In.Amount),
		BankCode:        obj.In.BankCode,
		Mobile:          obj.In.Mobile,
		BankNumber:      obj.In.BankCard,
		AccountHoldName: obj.In.RealName,
		NotifyUrl:       config.Server.FlashPay.Url_withdraw_notify,
		// ClientNo:        "",
	}

	payAmount := strconv.FormatFloat(req.PayAmount, 'f', -1, 64)

	params := url.Values{}
	params.Set("merchantNo", req.MerchantNo)
	params.Set("merchantOrderNo", req.MerchantOrderNo)
	params.Set("description", req.Description)
	params.Set("payAmount", payAmount)
	params.Set("bankCode", req.BankCode)
	params.Set("mobile", req.Mobile)
	params.Set("bankNumber", req.BankNumber)
	params.Set("accountHoldName", req.AccountHoldName)
	params.Set("notifyUrl", req.NotifyUrl)

	// 生成签名
	checkContent := createEncryptStr(params)
	// log.Debug("flash.WithdrawOrder.checkContent ==> %s", checkContent)

	// 商户请求我们接口时使用商户私钥对请求参数 进行加密
	mchPriKey := fmt.Sprintf(`-----BEGIN PRIVATE KEY-----
%s
-----END PRIVATE KEY-----
`, config.Server.FlashPay.MCH_PRIVATE_KEY)

	if err := public.RSA.SetPrivateKey(mchPriKey); err != nil {
		log.Error("flashpay.WithdrawOrder set private key :%v ==> %+v", err, params)
		return
	}

	// 私钥加密
	prienctypt, err := public.RSA.PriKeyENCTYPT([]byte(checkContent))
	if err != nil {
		log.Error("flashpay.WithdrawOrder RSA.PriKeyENCTYPT err %v", err)
		return
	}

	req.Sign = base64.StdEncoding.EncodeToString(prienctypt)
	// log.Debug("flashpay.payOrder req ==> %+v ", params)

	buf, _ := json.Marshal(req)
	respBody := public.HttpPostByJson(config.Server.FlashPay.Url_withdraw_order, string(buf))
	log.Debug("flashpay.WithdrawOrder req ==> %+v  resp ==> %+v", string(buf), respBody)

	var resp withdraw_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("flashpay.WithdrawOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		c.String(http.StatusOK, "fail")
		return
	}

	log.Debug("flashpay.WithdrawOrder resp ==> %+v", resp)

	if resp.Status != "200" {
		log.Error("flashpay.withdrawRequest post return resp fail ==> %+v", resp)
	}

	// 请求响应码，00000表示成功，其他失败
	if resp.Data.OrderStatus == WITHDRAW_STATUS_FAILED {
		log.Error("flashpay.withdrawRequest post return resp fail ==> %+v", resp)
		c.String(http.StatusOK, "fail")
		return
	}

	c.String(http.StatusOK, "success")
	return
}

// 提现通知
func WithdrawNotify(c *gin.Context) {
	bodyData, _ := io.ReadAll(c.Request.Body)
	// log.Debug("flash.WithdrawNotify ==> ctx.Request.body: %v", string(bodyData))

	var resp withdrawNotify
	err := json.Unmarshal(bodyData, &resp)
	if err != nil {
		log.Debug("%s query params err %v", "flashpay.WithdrawNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("flashpay.WithdrawNotify resp ==> %+v", resp)

	params := url.Values{}
	params.Set("amount", resp.Amount)
	params.Set("merchantNo", resp.MerchantNo)
	params.Set("merchantOrderNo", resp.MerchantOrderNo)
	params.Set("platOrderNo", resp.PlatOrderNo)
	params.Set("orderStatus", resp.OrderStatus)
	params.Set("orderMessage", resp.OrderMessage)

	// 生成签名
	checkContent := createEncryptStr(params)
	log.Debug("flashpay.WithdrawNotify checkContent ==> %s", checkContent)

	// 商户使用 商户后台显示的平台公钥 进行解密
	platPubKey := fmt.Sprintf(`-----BEGIN PUBLIC KEY-----
%s
-----END PUBLIC KEY-----
`, config.Server.FlashPay.PLAT_PUBLIC_KEY)

	if err := public.RSA.SetPublicKey(platPubKey); err != nil {
		log.Error("flashpay.WithdrawNotify set public key :%v ==> %+v", err, params)
		return
	}

	data, err := base64.RawURLEncoding.DecodeString(resp.Sign)
	if err != nil {
		log.Error("flashpay.WithdrawNotify base64.StdEncoding.DecodeString err %v", err)
		return
	}

	// 公钥解密
	pubdecrypt, err := public.RSA.PubKeyDECRYPT(data)
	if err != nil {
		log.Error("flashpay.WithdrawNotify RSA.PubKeyDECRYPT err %v", err)
		return
	}

	// 校验信息
	if checkContent != string(pubdecrypt) {
		log.Error("flashpay.WithdrawNotify 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("flashpay.WithdrawNotify 签名成功")

	// 除了SUCCESS是成功，FAILED失败，其他状态请调用方标记为处理中，等待回调或者查询接口返回最终状态
	if resp.OrderStatus != WITHDRAW_STATUS_SUCCESS {
		log.Error("flashpay.WithdrawNotify req ==> %+v", resp)
		c.String(http.StatusOK, "")
		return
	}

	// 查询账户余额
	balance := queryAccount()

	// 除了SUCCESS是成功，FAILED失败，其他状态请调用方标记为处理中，等待回调或者查询接口返回最终状态
	// 状态(0=Failure 1=Success  2=Pending(Success))
	status := 0
	switch resp.OrderStatus {
	case WITHDRAW_STATUS_SUCCESS:
		status = 1
	case WITHDRAW_STATUS_FAILED:
		status = 0
	default:
		status = 1
	}

	obj := db.NewFlashWithdrawNotify()
	obj.In.OrderID = resp.MerchantOrderNo
	obj.In.DfTransactionId = resp.PlatOrderNo
	obj.In.Status = status
	obj.In.DfDesc = fmt.Sprintf("%s", strings.ReplaceAll(resp.OrderMessage, "'", ""))
	obj.In.Balance = balance
	obj.DoAction()

	log.Debug("flashpay.WithdrawNotify obj.In=%+v obj.Out=%+v", obj.In, obj.Out)

	c.String(http.StatusOK, "success")
	return
}

// 账户余额
func queryAccount() int {
	req := queryAccount_req{
		MerchantNo: config.Server.FlashPay.MerchantCode,
		Timestamp:  time.Now().Format(TIME_FORMAT),
	}

	params := url.Values{}
	params.Set("merchantNo", req.MerchantNo)
	params.Set("timestamp", req.Timestamp)

	// 生成签名
	checkContent := createEncryptStr(params)
	log.Debug("flashpay.queryAccount.checkContent ==> %s", checkContent)

	// 商户请求我们接口时使用商户私钥对请求参数 进行加密
	mchPriKey := fmt.Sprintf(`-----BEGIN PRIVATE KEY-----
%s
-----END PRIVATE KEY-----
`, config.Server.FlashPay.MCH_PRIVATE_KEY)

	if err := public.RSA.SetPrivateKey(mchPriKey); err != nil {
		log.Error("flashpay.queryAccount set private key :%v ==> %+v", err, params)
		return -1
	}

	// 私钥加密
	prienctypt, err := public.RSA.PriKeyENCTYPT([]byte(checkContent))
	if err != nil {
		log.Error("flash.queryAccount RSA.PriKeyENCTYPT err %v", err)
		return -1
	}

	req.Sign = base64.StdEncoding.EncodeToString(prienctypt)
	// log.Debug("flashpay.queryAccount req ==> %+v ", params)

	buf, _ := json.Marshal(req)
	respBody := public.HttpPostByJson(config.Server.FlashPay.Url_queryAccount, string(buf))
	log.Debug("flashpay.queryAccount req ==> %+v  resp ==> %+v", params, respBody)

	var resp queryAccount_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("flashpay.queryAccount json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		return -1
	}

	log.Debug("flashpay.queryAccount resp ==> %+v", resp)

	// 请求响应码
	if resp.Status != "200" {
		log.Error("flashpay.queryAccount post return resp fail ==> %+v", resp)
		return -1
	}

	balance := int(resp.Data.AvailableAmount)
	return balance
}
