package flash

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bet24.com/servers/payment/shop"

	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_Flash_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "flash.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	//// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "flash.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "flash.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "flash.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "flash.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "flash.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	// 请求payOrder的代码
	req := pay_req{
		MerchantNo:      config.Server.FlashPay.MerchantCode,
		MerchantOrderNo: obj.Out.OrderID,
		PayAmount:       obj.In.Price,
		Mobile:          obj.In.Tel,
		Name:            obj.In.Name,
		Method:          "CHECKOUT",
		// ExpiryPeriod:    "",
		Email:     obj.In.Email,
		NotifyUrl: config.Server.FlashPay.Url_pay_notify,
		// ClientNo:        "",
		Description: obj.In.ProductID,
	}

	payAmount := strconv.FormatFloat(req.PayAmount, 'f', -1, 64)

	params := url.Values{}
	params.Set("merchantNo", req.MerchantNo)
	params.Set("merchantOrderNo", req.MerchantOrderNo)
	params.Set("payAmount", payAmount)
	params.Set("mobile", req.Mobile)
	params.Set("name", req.Name)
	params.Set("method", req.Method)
	params.Set("notifyUrl", req.NotifyUrl)
	params.Set("description", req.Description)

	// 生成签名
	checkContent := createEncryptStr(params)
	log.Debug("order.checkContent ==> %s", checkContent)

	// 商户请求我们接口时使用商户私钥对请求参数 进行加密
	mchPriKey := fmt.Sprintf(`-----BEGIN PRIVATE KEY-----
%s
-----END PRIVATE KEY-----
`, config.Server.FlashPay.MCH_PRIVATE_KEY)

	if err := public.RSA.SetPrivateKey(mchPriKey); err != nil {
		log.Error("flash.payOrder set private key :%v ==> %+v", err, params)
		return
	}

	// 私钥加密
	prienctypt, err := public.RSA.PriKeyENCTYPT([]byte(checkContent))
	if err != nil {
		log.Error("flash.payOrder RSA.PriKeyENCTYPT err %v", err)
		return
	}

	req.Sign = base64.StdEncoding.EncodeToString(prienctypt)
	log.Debug("flash.payOrder req ==> %+v ", params)

	// POST请求
	buf, _ := json.Marshal(req)
	respBody := public.HttpPostByJson(config.Server.FlashPay.Url_pay_order, string(buf))
	log.Debug("flash.payOrder req ==> %+v  resp ==> %+v", params, respBody)

	var resp pay_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("flash.payOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		return
	}

	log.Debug("flash.payOrder resp ==> %+v", resp)

	// 请求响应码
	if resp.Status != "200" {
		log.Error("flash.payOrder post return resp fail ==> %+v", resp)
		return
	}

	// ARRIVED，SUCCESS，CLEARED都可以认为是收款成功
	if resp.Data.OrderStatus != PAY_STATUS_ARRIVED &&
		resp.Data.OrderStatus != PAY_STATUS_SUCCESS &&
		resp.Data.OrderStatus != PAY_STATUS_CLEARED {
		log.Error("flash.payOrder post return resp fail ==> %+v", resp)
		return
	}

	// 跳转到支付页面，以便持卡人完成支付过程
	c.Redirect(http.StatusMovedPermanently, resp.Data.PaymentUrl)

	// c.String(http.StatusOK, "Success")
	return
}

// 回调通知
func PayNotify(c *gin.Context) {
	bodyData, _ := io.ReadAll(c.Request.Body)
	log.Debug("flash.PayNotify ==> ctx.Request.body: %v", string(bodyData))

	var resp payNotify
	err := json.Unmarshal(bodyData, &resp)
	if err != nil {
		log.Debug("%s query params err %v", "flash.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("flash.PayNotify resp ==> %+v", resp)

	params := url.Values{}
	params.Set("amount", resp.Amount)
	params.Set("factAmount", resp.FactAmount)
	params.Set("merchantOrderNo", resp.MerchantOrderNo)
	params.Set("platOrderNo", resp.PlatOrderNo)
	params.Set("orderStatus", resp.OrderStatus)
	params.Set("orderMessage", resp.OrderMessage)

	// 生成签名
	checkContent := createEncryptStr(params)
	// log.Debug("flash.PayNotify checkContent ==> %s", checkContent)

	// 商户使用 商户后台显示的平台公钥 进行解密
	platPubKey := fmt.Sprintf(`-----BEGIN PUBLIC KEY-----
%s
-----END PUBLIC KEY-----
`, config.Server.FlashPay.PLAT_PUBLIC_KEY)

	if err := public.RSA.SetPublicKey(platPubKey); err != nil {
		log.Error("flash.PayNotify set public key :%v ==> %+v", err, params)
		return
	}

	data, err := base64.RawURLEncoding.DecodeString(resp.Sign)
	if err != nil {
		log.Error("flash.PayNotify base64.StdEncoding.DecodeString err %v", err)
		return
	}

	// 公钥解密
	pubdecrypt, err := public.RSA.PubKeyDECRYPT(data)
	if err != nil {
		log.Error("flash.PayNotify RSA.PubKeyDECRYPT err %v", err)
		return
	}

	if checkContent != string(pubdecrypt) {
		log.Error("flash.PayNotify 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("flash.PayNotify 签名成功")

	// ARRIVED，SUCCESS，CLEARED都可以认为是收款成功
	if resp.OrderStatus != PAY_STATUS_ARRIVED &&
		resp.OrderStatus != PAY_STATUS_SUCCESS &&
		resp.OrderStatus != PAY_STATUS_CLEARED {
		log.Error("flash.PayNotify resp ==> %+v 失败", resp)
		return
	}

	//faceAmount, _ := strconv.Atoi(resp.FactAmount)

	// 数据库操作
	obj := db.NewNotify(db.SP_Flash_NOTIFY)
	obj.In.OrderID = resp.MerchantOrderNo
	obj.In.TradeID = resp.PlatOrderNo
	//obj.In.Price = faceAmount
	obj.DoAction(nil)

	// 操作成功,给道具
	// if obj.Out.RetCode == 1 {
	// 	// 充值
	// 	resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
	// 	log.Debug("%s 充值成功 %+v", "flash.PayNotify", resp)
	// }

	c.String(http.StatusOK, "success")
	return
}
