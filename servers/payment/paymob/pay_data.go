package paymob

type ret struct {
	Code string `json:"code"` // 响应 ‘SUCCESS’
	Msg  string `json:"msg"`  // 响应 ‘Success’
}

// 身份验证token
type (
	auth_token_req struct {
		Api_Key string `json:"api_key"`
	}

	auth_token_resp struct {
		Token string `json:"token"`
	}
)

// 订单注册
type (
	order_register_req struct {
		Auth_token        string                   `json:"auth_token"`
		Delivery_needed   string                   `json:"delivery_needed"`
		Amount_cents      string                   `json:"amount_cents"`
		Merchant_order_id string                   `json:"merchant_order_id"`
		Items             []order_register_product `json:"items"`
	}

	order_register_product struct {
		Name         string `json:"name"`
		Amount_cents string `json:"amount_cents"`
		Description  string `json:"description"`
		Quantity     string `json:"quantity"`
	}

	order_register_resp struct {
		Id int `json:"id"`
	}
)

// 支付token
type (
	payment_toke_req struct {
		Auth_token     string                         `json:"auth_token"`
		Amount_cents   string                         `json:"amount_cents"`
		Expiration     int                            `json:"expiration"`
		Order_id       string                         `json:"order_id"`
		Billing_data   payment_token_req_billing_data `json:"billing_data"`
		Currency       string                         `json:"currency"`
		Integration_id int                            `json:"integration_id"`
	}

	payment_token_req_billing_data struct {
		Apartment       string `json:"apartment"`
		Email           string `json:"email"`
		Floor           string `json:"floor"`
		First_name      string `json:"first_name"`
		Street          string `json:"street"`
		Building        string `json:"building"`
		Phone_number    string `json:"phone_number"`
		Shipping_method string `json:"shipping_method"`
		Postal_code     string `json:"postal_code"`
		City            string `json:"city"`
		Country         string `json:"country"`
		Last_name       string `json:"last_name"`
		State           string `json:"state"`
	}

	payment_token_resp struct {
		Token string `json:"token"`
	}
)

// 现金支付
type (
	cash_pay_req struct {
		Source        cash_pay_source `json:"source"`
		Payment_token string          `json:"payment_token"`
	}

	cash_pay_source struct {
		Identifier string `json:"identifier"`
		Subtype    string `json:"subtype"`
	}

	cash_pay_resp struct {
		Id                     int        `json:"id"`
		Pending                bool       `json:"pending"`
		Amount_cents           int        `json:"amount_cents"`
		Success                bool       `json:"success"`
		Is_auth                bool       `json:"is_auth"`
		Is_capture             bool       `json:"is_capture"`
		Is_standalone_payment  bool       `json:"is_standalone_payment"`
		Is_voided              bool       `json:"is_voided"`
		Is_refunded            bool       `json:"is_refunded"`
		Is_3d_secure           bool       `json:"is_3d_secure"`
		Integration_id         int        `json:"integration_id"`
		Profile_id             int        `json:"profile_id"`
		Has_parent_transaction bool       `json:"has_parent_transaction"`
		Order                  cash_order `json:"order"`
		Created_at             string     `json:"created_at"`
		Currency               string     `json:"currency"`
		Api_source             string     `json:"api_source"`
		Is_void                bool       `json:"is_void"`
		Is_refund              bool       `json:"is_refund"`
		Error_occured          bool       `json:"error_occured"`
		Refunded_amount_cents  int        `json:"refunded_amount_cents"`
		Captured_amount        int        `json:"captured_amount"`
		Merchant_staff_tag     string     `json:"merchant_staff_tag"`
		Owner                  int        `json:"owner"`
		Parent_transaction     string     `json:"parent_transaction"`
		Merchant_order_id      string     `json:"merchant_order_id"`
		Redirect_url           string     `json:"redirect_url"`
	}

	cash_order struct {
		Order_url string `json:"order_url"`
	}
)

// 回调通知
type (
	payNotify_req struct {
		Amount_cents           int    `json:"amount_cents" form:"amount_cents"`
		Captured_amount        int    `json:"captured_amount" form:"captured_amount"`
		Created_at             string `json:"created_at" form:"created_at"`
		Currency               string `json:"currency" form:"currency"`
		Data_message           string `json:"data.message" form:"data.message"`
		Error_occured          bool   `json:"error_occured" form:"error_occured"`
		Has_parent_transaction bool   `json:"has_parent_transaction" form:"has_parent_transaction"`
		Hmac                   string `json:"hmac" form:"hmac"`
		Id                     int    `json:"id" form:"id"`
		Integration_id         int    `json:"integration_id" form:"integration_id"`
		Is_3d_secure           bool   `json:"is_3d_secure" form:"is_3d_secure"`
		Is_auth                bool   `json:"is_auth" form:"is_auth"`
		Is_capture             bool   `json:"is_capture" form:"is_capture"`
		Is_refund              bool   `json:"is_refund" form:"is_refund"`
		Is_refunded            bool   `json:"is_refunded" form:"is_refunded"`
		Is_standalone_payment  bool   `json:"is_standalone_payment" form:"is_standalone_payment"`
		Is_void                bool   `json:"is_void" form:"is_void"`
		Is_voided              bool   `json:"is_voided" form:"is_voided"`
		Merchant_commission    int    `json:"merchant_commission" form:"merchant_commission"`
		Merchant_order_id      string `json:"merchant_order_id" form:"merchant_order_id"`
		Order                  int    `json:"order" form:"order"`
		Owner                  int    `json:"owner" form:"owner"`
		Pending                bool   `json:"pending" form:"pending"`
		Profile_id             int    `json:"profile_id" form:"profile_id"`
		Refunded_amount_cents  int    `json:"refunded_amount_cents" form:"refunded_amount_cents"`
		Source_data_pan        string `json:"source_data.pan" form:"source_data.pan"`
		Source_data_sub_type   string `json:"source_data.sub_type" form:"source_data.sub_type"`
		Source_data_type       string `json:"source_data.type" form:"source_data.type"`
		Success                bool   `json:"success" form:"success"`
		Txn_response_code      int    `json:"txn_response_code" form:"txn_response_code"`
		Updated_at             string `json:"updated_at" form:"updated_at"`
	}

	payNotify_resp struct {
	}
)
