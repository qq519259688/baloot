package paymob

import (
	"io"
	"net/http"
	"strings"

	"bet24.com/log"
)

const dateFormat = "2006-01-02T15:04:05.000Z07:00"

// http post json 请求
func httpPostByJson(url string, data string, sign string) string {
	req, err := http.NewRequest("POST", url, strings.NewReader(data))
	if err != nil {
		log.Error("HttpPostByJson NewRequest error %v", err)
		return ""
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Cache-Control", "no-cache")
	req.Header.Set("sign", sign)

	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		log.Error("HttpPostByJson Request Do error %v", err)
		return ""
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error("HttpPostByJson Response error %v", err)
		return ""
	}

	// log.Debug("HttpPost Send：%v", string(body))
	return string(body)
}
