package lets

// 提现请求、响应
type (
	withdraw_req struct {
		Type        string `json:"type" form:"type"`               // 转账类型,必填字符小写固定字符 api
		MchId       string `json:"mchId" form:"mchId"`             // 商户号,商户的开户号
		MchTransNo  string `json:"mchTransNo" form:"mchTransNo"`   // 转账订单号,至少 6 位字符，最多 22 位
		Amount      int    `json:"amount" form:"amount"`           // 金额,单位为元,保留两位小数
		NotifyUrl   string `json:"notifyUrl" form:"notifyUrl"`     // 通知地址,支持 http 和 https 通知,通知方式为 post
		AccountName string `json:"accountName" form:"accountName"` // 账户名,持卡人姓名
		AccountNo   string `json:"accountNo" form:"accountNo"`     // 账号,持卡人卡号
		BankCode    string `json:"bankCode" form:"bankCode"`       // 银行代号(驼峰法),章节五各个国家银行代号
		// 备注
		// 提交 email,phone 参数，格式举例：
		// email:520155@gmail.com/phone:1234567892
		// 以上仅为格式举例，具体内容用户自己填写
		// 印度代付支持 bank 和 upi 代付
		// email:520155@gmail.com/phone:1234567892/mode:bank(或者 upi)
		RemarkInfo string `json:"remarkInfo" form:"remarkInfo"`
	}

	withdraw_resp struct {
		RetCode    string `json:"retCode" form:"retCode"`
		RetMsg     string `json:"retMsg" form:"retMsg"`
		MchTransNo string `json:"mchTransNo" form:"mchTransNo"`
		PlatOrder  string `json:"platOrder" form:"platOrder"`
	}
)

// 提现通知
type withdrawNotify struct {
	MchId         string  `json:"mchId" form:"mchId"`                 // 商户号,商户的开户号
	MchTransNo    string  `json:"mchTransNo" form:"mchTransNo"`       // 转账订单号,至少 6 位字符，最多 22 位
	Amount        float64 `json:"amount" form:"amount"`               // 金额,单位为元,保留两位小数
	Status        string  `json:"status" form:"status"`               // 状态,1 处理中,2 成功,3 失败
	TransSuccTime string  `json:"transSuccTime" form:"transSuccTime"` // 时间,成功时间
	Sign          string  `json:"sign" form:"sign"`                   // 签名,商户返回数据得到签名与返回的签名进行验签名
}

// 账户余额查询请求、响应
type (
	queryAccount_req struct {
		MchId string `json:"mchId" form:"mchId"` //商户号,商户的开户号
	}

	queryAccount_resp struct {
		MchId   string  `json:"mchId" form:"mchId"`     // 商户号,商户的开户号
		Balance float64 `json:"balance" form:"balance"` // 账户余额,单位元
		Sign    string  `json:"sign" form:"sign"`       // 签名,商户返回数据得到签名与返回的签名进行验签名
	}
)
