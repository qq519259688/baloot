package lets

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bet24.com/log"
	"bet24.com/public"
	notification "bet24.com/servers/micros/notification/proto"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 提现请求
func WithdrawOrder(c *gin.Context) {
	obj := db.NewLetsWithdrawReq()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "lets.WithdrawOrder", err)
		c.String(http.StatusOK, "fail")
		return
	}

	obj.In.IPAddress = strings.Split(c.Request.RemoteAddr, ":")[0]
	obj.DoAction()
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v obj.Out=%+v", "lets.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	// 请求时，没有传手机号
	if obj.In.Mobile == "" {
		obj.In.Mobile = obj.Out.Tel
	}

	// 请求withdrawOrder的代码
	req := withdraw_req{
		Type:        "api",
		MchId:       config.Server.LetsPay.MchId,
		MchTransNo:  obj.Out.OrderID,
		Amount:      obj.In.Amount,
		NotifyUrl:   config.Server.LetsPay.Url_witdraw_notify,
		AccountName: obj.In.RealName,
		AccountNo:   obj.In.BankCard,
		BankCode:    obj.In.BankName,
		RemarkInfo:  fmt.Sprintf("email:%s/phone:%s", obj.In.Email, obj.In.Mobile),
	}

	params := url.Values{}
	params.Set("type", req.Type)
	params.Set("mchId", req.MchId)
	params.Set("mchTransNo", req.MchTransNo)
	params.Set("amount", fmt.Sprintf("%d.00", req.Amount))
	params.Set("notifyUrl", req.NotifyUrl)
	params.Set("accountName", req.AccountName)
	params.Set("accountNo", req.AccountNo)
	params.Set("bankCode", req.BankCode)
	params.Set("remarkInfo", req.RemarkInfo)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.LetsPay.MD5Key
	// log.Debug("lets.WithdrawOrder.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	params.Set("sign", signature)

	respBody := public.HttpPostForm(config.Server.LetsPay.Url_withdraw_order, params)

	log.Debug("lets.WithdrawOrder req ==> %+v  resp ==> %+v", params, respBody)

	var resp withdraw_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("lets.WithdrawOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		c.String(http.StatusOK, "fail")
		return
	}

	log.Debug("lets.WithdrawOrder resp ==> %+v", resp)

	// 请求响应码
	if resp.RetCode != "SUCCESS" {
		log.Error("lets.withdrawRequest post return resp fail ==> %+v", resp)
		c.String(http.StatusOK, "fail")
		return
	}

	go notification.AddNotification(obj.In.UserID, notification.Notification_Gold, "")

	// 更新银行信息
	objBank := db.NewBankInfoUp()
	objBank.In.UserID = obj.In.UserID
	objBank.In.RealName = req.AccountName
	objBank.In.BankCard = req.AccountNo
	objBank.In.BankName = req.BankCode
	objBank.In.Mobile = obj.In.Mobile
	objBank.In.EMail = obj.In.Email
	objBank.In.Address = obj.In.Address
	objBank.DoAction()

	log.Debug("lets.WithdrawNotify need save bankInfo")

	c.String(http.StatusOK, "success")
	return
}

// 提现通知
func WithdrawNotify(c *gin.Context) {
	var req withdrawNotify
	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s query params err %v", "lets.WithdrawNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("lets.WithdrawNotify ==> %+v", req)

	// 请求响应码
	if req.Status != "2" {
		log.Error("lets.WithdrawNotify req ==> %+v", req)
		c.String(http.StatusOK, "")
		return
	}

	amount := strconv.FormatFloat(req.Amount, 'f', 2, 64)

	params := url.Values{}
	params.Set("mchId", config.Server.LetsPay.MchId)
	params.Set("mchTransNo", req.MchTransNo)
	params.Set("amount", amount)
	params.Set("status", req.Status)
	params.Set("transSuccTime", req.TransSuccTime)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.LetsPay.MD5Key
	// log.Debug("lets.WithdrawNotify.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))

	// 校验签名
	if req.Sign != signature {
		log.Error("lets.WithdrawNotify 签名失败 post req ==> %+v  signature ==> %s", req, signature)
		return
	}

	log.Debug("lets.WithdrawNotify 签名成功")

	// 代付状态 1 处理中,2 成功,3 失败
	if req.Status != "2" && req.Status != "3" {
		log.Error("lets.WithdrawNotify response fail ==> %+v", req)
		return
	}

	// 查询账户余额
	// balance := queryAccount()

	obj := db.NewLetsWithdrawNotify()
	obj.In.OrderID = req.MchTransNo
	obj.In.BankCard = ""
	obj.In.DfTransactionId = req.TransSuccTime
	obj.In.DfState = req.Status
	obj.In.DfDesc = ""
	obj.In.Balance = 0
	obj.DoAction()

	log.Debug("lets.WithdrawNotify obj.In=%+v obj.Out=%+v", obj.In, obj.Out)

	// 提现失败,退款
	if req.Status == "3" {
		go notification.AddNotification(obj.Out.UserID, notification.Notification_Gold, "")
	}

	c.String(http.StatusOK, "success")
	return
}

// 账户余额
func queryAccount() int {
	req := queryAccount_req{
		MchId: config.Server.LetsPay.MchId,
	}

	params := url.Values{}
	params.Set("mchId", req.MchId)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.LetsPay.MD5Key
	// log.Debug("lets.queryAccount.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	params.Set("sign", signature)

	respBody := public.HttpPostForm(config.Server.LetsPay.Url_queryAccount, params)
	log.Debug("lets.queryAccount req ==> %+v  resp ==> %+v", params, respBody)

	var resp queryAccount_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("lets.queryAccount json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		return -1
	}

	log.Debug("lets.queryAccount resp ==> %+v", resp)

	amount := strconv.FormatFloat(resp.Balance, 'f', 2, 64)
	balance, err := strconv.Atoi(strings.ReplaceAll(amount, ".00", ""))
	if err != nil {
		log.Error("lets.queryAccount price err %v", err)
		return -1
	}

	return balance
}
