package lets

// 支付下单请求、响应
type (
	pay_req struct {
		MchId    string `json:"mchId" form:"mchId"`       // 商户号
		OrderNo  string `json:"orderNo" form:"orderNo"`   // 订单号,至少 6 位字符，最多 22 位
		Amount   int    `json:"amount" form:"amount"`     // 金额,单位为元,保留两位小数
		Product  string `json:"product" form:"product"`   // 产品号,支付产品说明
		Bankcode string `json:"bankcode" form:"bankcode"` // 银行代号(小写), all
		// 物品说明,本字段扩展字段参考后面说明进行对应格式要求进行字符串拼接
		// 提交 email,name,phone 参数，格式举例：
		// email:520155@gmail.com/name:tom/phone:1234567892
		// 印度电话是 10 位数,不需要加区号,以 7,8,9 开头; 巴西支付填写如下字段
		// email:520155@gmail.com/name:san zhang/phone:1234567892
		// /cpf:1234567892
		// 巴西 cpf 必须正确，否则无法拉起支付; 以上仅为格式举例，具体内容用户自己填写
		Goods     string `json:"goods" form:"goods"`
		NotifyUrl string `json:"notifyUrl" form:"notifyUrl"` // 异步通知,支持 http 和 https 通知,通知方式为 post
		ReturnUrl string `json:"returnUrl" form:"returnUrl"` // 同步通知,支持 http 和 https 通知,通知方式为 post，不生效，不能为空
	}

	pay_resp struct {
		RetCode   string `json:"retCode" form:"retCode"`
		PayUrl    string `json:"payUrl" form:"payUrl"`
		OrderNo   string `json:"orderNo" form:"orderNo"`
		PlatOrder string `json:"platOrder" form:"platOrder"`
		Code      string `json:"code" form:"code"`
	}
)

// 充值通知
type payNotify struct {
	MchId       string  `json:"mchId" form:"mchId"`             // 商户号
	OrderNo     string  `json:"orderNo" form:"orderNo"`         // 订单号,至少 6 位字符，最多 22 位
	Amount      float64 `json:"amount" form:"amount"`           // 金额,单位为元,保留两位小数
	Product     string  `json:"product" form:"product"`         // 产品号,参考 product 支付产品说明
	PaySuccTime string  `json:"paySuccTime" form:"paySuccTime"` // 成功时间,支付成功时间
	Status      string  `json:"status" form:"status"`           // 成功状态,1 支付中,2 成功,5 失效
	Sign        string  `json:"sign" form:"sign"`               // 签名,商户返回数据得到签名与返回的签名进行验签名
}
