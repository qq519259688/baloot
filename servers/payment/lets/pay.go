package lets

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bet24.com/servers/payment/shop"

	"bet24.com/log"
	"bet24.com/public"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_Lets_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "lets.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	//// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "lets.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "lets.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "lets.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "lets.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "lets.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	// 请求payOrder的代码
	req := pay_req{
		MchId:     config.Server.LetsPay.MchId,
		OrderNo:   obj.Out.OrderID,
		Amount:    int(math.Ceil(obj.In.Price)),
		Product:   "indoovo",
		Bankcode:  "all",
		Goods:     fmt.Sprintf("email:%s/name:%s/phone:%s", obj.In.Email, obj.In.Name, obj.In.Email),
		NotifyUrl: config.Server.LetsPay.Url_pay_notify,
		ReturnUrl: config.Server.LetsPay.Url_pay_return,
	}

	params := url.Values{}
	params.Set("mchId", req.MchId)
	params.Set("orderNo", req.OrderNo)
	params.Set("amount", fmt.Sprintf("%d.00", req.Amount))
	params.Set("product", req.Product)
	params.Set("bankcode", req.Bankcode)
	params.Set("goods", req.Goods)
	params.Set("notifyUrl", req.NotifyUrl)
	params.Set("returnUrl", req.ReturnUrl)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.LetsPay.MD5Key
	// log.Debug("lets.payOrder checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	params.Set("sign", signature)

	// POST请求
	respBody := public.HttpPostForm(config.Server.LetsPay.Url_pay_order, params)
	log.Debug("lets.payOrder req ==> %+v  resp ==> %+v", params, respBody)

	var resp pay_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("lets.payOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		return
	}

	log.Debug("lets.payOrder resp ==> %+v", resp)

	// 请求响应码
	if resp.RetCode != "SUCCESS" || resp.PayUrl == "" {
		log.Error("lets.payOrder post return resp fail ==> %+v", resp)
		return
	}

	// 跳转到支付页面，以便持卡人完成支付过程
	c.Redirect(http.StatusMovedPermanently, resp.PayUrl)
	return
}

// 回调通知
func PayNotify(c *gin.Context) {
	var resp payNotify
	if err := c.ShouldBind(&resp); err != nil {
		log.Debug("%s query params err %v", "lets.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("lets.PayNotify resp ==> %+v", resp)

	amount := strconv.FormatFloat(resp.Amount, 'f', 2, 64)

	params := url.Values{}
	params.Set("mchId", config.Server.LetsPay.MchId)
	params.Set("orderNo", resp.OrderNo)
	params.Set("amount", amount)
	params.Set("product", resp.Product)
	params.Set("paySuccTime", resp.PaySuccTime)
	params.Set("status", resp.Status)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.LetsPay.MD5Key
	// log.Debug("lets.PayNotify checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))

	// 校验签名
	if resp.Sign != signature {
		log.Error("lets.PayNotify 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("lets.PayNotify 签名成功")

	// 1 支付中,2 成功,5 失效
	if resp.Status != "2" {
		log.Error("lets.PayNotify resp ==> %+v 失败", resp)
		return
	}

	//price, err := strconv.Atoi(strings.ReplaceAll(amount, ".00", ""))
	//if err != nil {
	//	log.Error("lets.PayNotify price err %v", err)
	//	return
	//}

	// 数据库操作
	obj := db.NewNotify(db.SP_Lets_NOTIFY)
	obj.In.OrderID = resp.OrderNo
	obj.In.TradeID = resp.PaySuccTime
	//obj.In.Price = price
	obj.DoAction(nil)

	// 操作成功,给道具
	if obj.Out.RetCode == 1 {
		// 充值
		resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
		log.Debug("%s 充值成功 %+v", "lets.PayNotify", resp)
	}

	c.String(http.StatusOK, "success")
	return
}

// 支付完成跳转处理
func PayCallback(c *gin.Context) {
	log.Debug("lets.payCallback:%v", c)
	// c.Redirect(http.StatusMovedPermanently, config.Server.Cropay.Url_resultPay)
}
