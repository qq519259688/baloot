package cropay

import "encoding/xml"

// 支付下单参数
type payOrder_req struct {
	baseInfo                  // 基本信息
	billInfo                  // 账单信息
	deliveryInfo              // 送货信息
	productInfo               // 购物信息
	Signature          string `form:"signature"`          //订单签名=md5(order_hash+merchant_id+merch_order_id +price_currency+price_amount) order_hash:商户的交易证书，可从“商户后台”获取	注意:不同字符集中生成的证书可能不同,应使用utf-8
	Client_finger_cybs string `form:"client_finger_cybs"` //用于调用风控接口的唯一字符串，一般是32位的guid字符串
}

// 基本信息
type baseInfo struct {
	Merchant_id         int     `form:"merchant_id"`         // 商户号  (由Cropay支付系统分配) 备注：实际提交时，该值需要进行转换。转换方法为：商户号 * 818 + 5201314
	Order_type          string  `form:"order_type"`          // 支付方式 0：跳转支付 3：内嵌支付 4：站内支付  (必须提交信用卡信息) 5：控制台支付(包括信用卡和国际本地化支付) 7：国际本地化支付(必须提交debit_id,issue_id)
	Gw_version          string  `form:"gw_version"`          // 接口版本号 本公司开发的接口版本号统一命名。例如：zencart(1.0)	商户自行开发的接口，该“版本号”值为空,或根据开发平台分别命名为:asp(1.0),php(1.0),jsp(1.0),asp.net(1.0)
	Language            string  `form:"language"`            // 浏览器语言。可选值为：en-us：英语 zh-cn：中文 zh-tw：繁体中文 de-de：德语 fr-fr：法语 it-it：意大利语 es-es：西班牙语 ja-jp：日语 ko-kr：韩语 pt-pt：葡萄牙语 若此字段传空，则默认为英文。
	Merch_order_ori_id  string  `form:"merch_order_ori_id"`  // 商户原始订单号 (例如：791 , 10320 ...) 由商户网站生成,在商户网站后台查询该订单的订单号
	Merch_order_id      string  `form:"merch_order_id"`      // 商户订单号  (例如：141017114556-3694 , ...) 由商户网站生成,用来唯一标识该订单,不应当重复。
	Merch_order_date    string  `form:"merch_order_date"`    // 商户订单时间 格式：YYYYMMDDHHMMSS。例如：20180731235959
	Price_currency      string  `form:"price_currency"`      // 标价币种 (具体支持的币种需要向销售确认)	例如：USD:美元,CNY:人民币,EUR:欧元,GBP:英镑,JPY:日元,KRW:韩元,RUR:俄罗斯卢布,HKD:港币,AUD:澳元
	Price_amount        float64 `form:"price_amount"`        // 标价金额 格式：保留两位小数的金额。例如：263.50
	Ip                  string  `form:"ip"`                  // 持卡人的IP地址
	Url_sync            string  `form:"url_sync"`            // 订单状态同步地 址/异步回传地址 例如：http://www.abc.com/glb-callback.php
	Url_succ_back       string  `form:"url_succ_back"`       // 成功订单返回地址 (待处理状态的订单也返回该网址)
	Url_fail_back       string  `form:"url_fail_back"`       // 失败订单返回地址
	Url_referrer_domain string  `form:"url_referrer_domain"` // 商户交易网址(如果网站放在子目录，则也需要带上子目录) 例如 www.abc.com  或sale.abc.com/website1
}

// 账单信息
type billInfo struct {
	Bill_address  string `form:"bill_address"`  // 账单地址
	Bill_country  string `form:"bill_country"`  // 账单国家(符合ISO标准的国家代码,参考“附录A”)
	Bill_province string `form:"bill_province"` // 账单地区
	Bill_city     string `form:"bill_city"`     // 账单城市
	Bill_email    string `form:"bill_email"`    // 账单e_mail 备注：必须符合e_mail格式
	Bill_phone    string `form:"bill_phone"`    // 账单电话
	Bill_post     string `form:"bill_post"`     // 账单邮编
}

// 送货信息
type deliveryInfo struct {
	Delivery_name     string `form:"delivery_name"`     // 收货姓名
	Delivery_address  string `form:"delivery_address"`  // 收货地址
	Delivery_country  string `form:"delivery_country"`  // 收货国家(符合ISO标准的国家代码,参考“附录A”)
	Delivery_province string `form:"delivery_province"` // 收货地区
	Delivery_city     string `form:"delivery_city"`     // 收货城市
	Delivery_email    string `form:"delivery_email"`    // 收货e_mail  备注：必须符合e_mail格式
	Delivery_phone    string `form:"delivery_phone"`    // 收货电话
	Delivery_post     string `form:"delivery_post"`     // 收货邮编
}

// 购物信息
type productInfo struct {
	Product_name string  `form:"product_name"` // 商品名称 (可以传多个产品信息，至少1个，最多50个)
	Product_sn   string  `form:"product_sn"`   // 商品货号 (同上)
	Quantity     int     `form:"quantity"`     // 商品数量 (同上)
	Unit         float64 `form:"unit"`         // 商品单价 (同上)
}

// 支付下单响应
type payOrder_resp struct {
	XMLName            xml.Name `xml:"payments"`           // 指定最外层的标签
	Merchant_id        int      `xml:"merchant_id"`        // 商户号 (由Cropay支付系统分配)
	Merch_order_ori_id string   `xml:"merch_order_ori_id"` // 商户原始订单号 由商户网站生成,在商户网站后台查询该订单的订单号
	Merch_order_id     string   `xml:"merch_order_id"`     // 商户订单号 由商户网站生成,用来唯一标识该订单,不应当重复。
	Price_currency     string   `xml:"price_currency"`     // 标价币种
	Price_amount       float64  `xml:"price_amount"`       // 标价金额
	Order_remark       string   `xml:"order_remark"`       // 订单备注
	Order_id           string   `xml:"order_id"`           // 系统订单号  (例如：1410302359590000)	本公司的系统生成的唯一标识该订单的订单号
	Status             string   `xml:"status"`             // 订单状态 T-待处理  N-失败  E-出错 备注：status不会返回Y(成功)状态；如果系统正常，会返回T(待处理)状态。E代表异常订单
	Message            string   `xml:"message"`            // 订单结果的描述性信息 备注：对于Y/N/E状态，都可能返回message信息。如果message不为空，该信息需要显示给持卡人看。
	Payment_url        string   `xml:"payment_url"`        // 该值是一个URL。如果status=T，商户网站需要跳转到该页面，以便持卡人完成支付过程。 备注：该参数经过了base64转换。
	Signature          string   `xml:"signature"`          // 签名=md5(order_hash+merchant_id+merch_order_id+ price_currency+price_amount+order_id+status) order_hash:商户的交易证书 注意:不同字符集中生成的证书可能不同,应使用utf-8
	Pay_title          string   `xml:"pay_title"`          // 支付方式的标题。比如：Payment Gateway
	Pay_logo           string   `xml:"pay_logo"`           // 支付方式的Logo文件名/URL。如果有多个文件名/URL，则用;分隔。例如： http://www.html5.com/vmj.png http://www.html5.com/vmj.png;http://www.html5.com/201.png vmj.png;201.png
}

// 通知响应
type notify_resp struct {
	Merchant_id        int    `form:"merchant_id"`        // 商户号   (由Cropay支付系统分配)
	Merch_order_ori_id string `form:"merch_order_ori_id"` // 商户原始订单号 由商户网站生成,在商户网站后台查询该订单的订单号
	Merch_order_id     string `form:"merch_order_id"`     // 商户订单号 由商户网站生成,用来唯一标识该订单,不应当重复。
	Bill_email         string `form:"bill_email"`         // 账单邮箱
	Price_currency     string `form:"price_currency"`     // 标价币种
	Price_amount       string `form:"price_amount"`       // 标价金额
	Order_remark       string `form:"order_remark"`       // 备注信息
	Order_id           string `form:"order_id"`           // 系统订单号  (例如：1410302359590000) 本公司的系统生成的唯一标识该订单的订单号
	Status             string `form:"status"`             // 订单状态 Y-成功 N-失败 备注：系统异步回传接口不会返回“T-待处理”和“E-错误”这两种状态，只会返回“Y-成功”和“N-失败”
	Message            string `form:"message"`            // 订单结果的描述性信息
	Card_holder        string `form:"card_holder"`        // 持卡人姓名(Full Name)
	Card_type_id       string `form:"card_type_id"`       // 卡种 1：Visa，2：MasterCard，3：JCB，4:AE运通卡
	Card_num_mask      string `form:"card_num_mask"`      // 卡号(掩码)
	Bin_country        string `form:"bin_country"`        // 发卡行所在国家的编码。参考附录A：国家代码表
	Signature          string `form:"signature"`          // 签名=md5(order_hash+merchant_id+merch_order_id+ price_currency+price_amount+order_id+status) order_hash:商户的交易证书 注意:不同字符集中生成的证书可能不同,应使用utf-8
}
