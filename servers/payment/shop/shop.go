package shop

import (
	"encoding/json"

	"bet24.com/log"
	coreClient "bet24.com/servers/coreservice/client"
	item "bet24.com/servers/micros/item_inventory/proto"
)

type Shop_Item struct {
	ProductId   string          // 产品ID
	ProductName string          // 产品名称
	Price       float64         // 基础价格(充值时是美元)
	PayType     int             // 支付类型(1=RMB  0=钻石 2=金币)
	ShopType    int             // 产品类型(1=金币  2=钻石  3=炮台  4=礼包)
	IsHot       int             // 是否热销 1=热销
	Bonus       int             // 额外赠送(多送百分比)
	Extra       []item.ItemPack // 扩展信息
	Status      int             // 状态(1=正常  2=不可用或没有购买次数)
	Rate        int             // 比率
}

func GetProduct(productId string) *Shop_Item {
	var item *Shop_Item
	respProduct := coreClient.GetProduct(0, 0, productId)
	if respProduct.RetCode != 1 {
		log.Error("%s GenOrder client ==> %+v", "GetProduct.GetProduct", respProduct)
		return item
	}

	if err := json.Unmarshal([]byte(respProduct.Data), &item); err != nil {
		log.Error("%s GenOrder json unmarshal err %v", "GetProduct.GetProduct", err)
		return item
	}

	return item
}

// 汇率
type exchangeRate struct {
	Currency    string  // 币种
	CountryName string  // 国家名称
	CountryCode string  // 国家编码
	Rate        float64 // 汇率
}

func GetExchangeRate(currency string) *exchangeRate {
	var list []*exchangeRate

	respExchange := coreClient.GetExchangeRateList()
	if respExchange.RetCode != 1 {
		log.Error("%s PayRequest client ==> %+v", "GetExchangeRate.GetExchangeRateList", respExchange)
		return nil
	}

	if err := json.Unmarshal([]byte(respExchange.Data), &list); err != nil {
		log.Error("%s PayRequest json unmarshal err %v", "GetExchangeRate.GetExchangeRateList", err)
		return nil
	}

	var info *exchangeRate

	for _, v := range list {
		info = v
		if info.Currency == currency {
			return info
		}
	}

	return info
}
