package liu

import (
	"encoding/json"
	"math"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bet24.com/servers/payment/shop"

	"bet24.com/log"
	"bet24.com/public"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_Liu_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "liu.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	//// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "liu.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "liu.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "liu.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "liu.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "liu.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	// 请求payOrder的代码
	req := pay_req{
		MerchantCode: config.Server.LiuPay.MerchantCode,
		Ccy_no:       "IDR",
		OrderNo:      obj.Out.OrderID,
		Menty:        int(math.Ceil(obj.In.Price)),
		TypeCode:     "yinnixunihubniva",
		Wares:        obj.In.ProductID,
		NotifyUrl:    config.Server.LiuPay.Url_pay_notify,
		PageUrl:      config.Server.LiuPay.Url_pay_page,
	}

	params := url.Values{}
	params.Set("merchantCode", req.MerchantCode)
	params.Set("ccy_no", req.Ccy_no)
	params.Set("orderNo", req.OrderNo)
	params.Set("menty", strconv.Itoa(req.Menty))
	params.Set("typeCode", req.TypeCode)
	params.Set("wares", req.Wares)
	params.Set("notifyUrl", req.NotifyUrl)
	params.Set("pageUrl", req.PageUrl)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.LiuPay.MD5Key
	// log.Debug("liu.payOrder checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	req.Signature = signature

	// POST请求
	buf, _ := json.Marshal(req)
	respBody := public.HttpPostByJson(config.Server.LiuPay.Url_pay_order, string(buf))
	log.Debug("liu.payOrder req ==> %+v  resp ==> %+v", string(buf), respBody)

	var resp pay_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("liu.payOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		return
	}

	log.Debug("liu.payOrder resp ==> %+v", resp)

	// 请求响应码
	if resp.Err_code != "000002" || resp.Status != "SUCCESS" || resp.Order_data == "" {
		log.Error("liu.payOrder post return resp fail ==> %+v", resp)
		return
	}

	// 跳转到支付页面，以便持卡人完成支付过程
	c.Redirect(http.StatusMovedPermanently, resp.Order_data)
	return
}

// 回调通知
func PayNotify(c *gin.Context) {
	var resp payNotify
	if err := c.ShouldBind(&resp); err != nil {
		log.Debug("%s query params err %v", "liu.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("liu.PayNotify resp ==> %+v", resp)

	if resp.Err_code != "000000" {
		log.Error("liu.PayNotify resp ==> %+v 失败", resp)
		return
	}

	params := url.Values{}
	params.Set("typeCode", resp.TypeCode)
	params.Set("err_code", resp.Err_code)
	params.Set("err_msg", resp.Err_msg)
	params.Set("merchantCode", config.Server.LiuPay.MerchantCode)
	params.Set("orderNo", resp.OrderNo)
	params.Set("amount", strconv.Itoa(resp.Amount))
	params.Set("terraceNo", resp.TerraceNo)
	params.Set("orderTime", resp.OrderTime)
	params.Set("payAmount", resp.PayAmount)
	params.Set("payTime", resp.PayTime)
	params.Set("reserver", resp.Reserver)
	params.Set("status", resp.Status)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.LiuPay.MD5Key
	// log.Debug("liu.PayNotify checkContent ==> %s", checkContent)
	signature := strings.ToLower(public.GetMd5String(checkContent))

	// 校验签名
	if resp.Signature != signature {
		log.Error("liu.PayNotify 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("liu.PayNotify 签名成功")

	if resp.Status != "SUCCESS" {
		log.Error("liu.PayNotify resp ==> %+v 失败", resp)
		return
	}

	// 数据库操作
	obj := db.NewNotify(db.SP_Liu_NOTIFY)
	obj.In.OrderID = resp.OrderNo
	obj.In.TradeID = resp.TerraceNo
	//obj.In.Price = resp.Amount
	obj.DoAction(nil)

	// 操作成功,给道具
	if obj.Out.RetCode == 1 {
		// 充值
		resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
		log.Debug("%s 充值成功 %+v", "liu.PayNotify", resp)
	}

	c.String(http.StatusOK, "SUCCESS")
	return
}

// 支付完成跳转处理
func PayCallback(c *gin.Context) {
	log.Debug("lets.payCallback")
	// c.Redirect(http.StatusMovedPermanently, config.Server.Cropay.Url_resultPay)
}
