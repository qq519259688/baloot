package liu

// 支付下单请求、响应
type (
	pay_req struct {
		MerchantCode string `json:"merchantCode" form:"merchantCode"` // 商户号,商户编号
		Ccy_no       string `json:"ccy_no" form:"ccy_no"`             // 币种,人民币：CNY（默认）  美国：USD（美元）  印度：INR(卢比)  越南：VND(盾)  巴西：BRL(雷亚尔)  印尼：IDR（印尼卢比）  印尼：THB（泰铢） 墨西哥：MXN(比索)
		OrderNo      string `json:"orderNo" form:"orderNo"`           // 订单号,必须唯一
		Menty        int    `json:"menty" form:"menty"`               // 交易金额,元为单位，整数
		TypeCode     string `json:"typeCode" form:"typeCode"`         // 业务编码
		Wares        string `json:"wares" form:"wares"`               // 商品详情
		NotifyUrl    string `json:"notifyUrl" form:"notifyUrl"`       // 异步通知地址,支付成功后，平台主动通知商家系统，商家系统必须指定接收通知的地址。
		PageUrl      string `json:"pageUrl" form:"pageUrl"`           // 支付成功，页面跳转地址
		Signature    string `json:"signature" form:"signature"`       // 签名
	}

	pay_resp struct {
		MerchantCode string `json:"merchantCode" form:"merchantCode"` // 商户号,商户编号
		OrderNo      string `json:"orderNo" form:"orderNo"`           // 订单号,商户必须保证唯一
		Amount       int    `json:"amount" form:"amount"`             // 交易金额,元为单位,整数,金额校检,以支付金额为准。
		TypeCode     string `json:"typeCode" form:"typeCode"`         // 业务编码
		Wares        string `json:"wares" form:"wares"`               // 商品详情
		NotifyUrl    string `json:"notifyUrl" form:"notifyUrl"`       // 异步通知地址,支付成功后，平台主动通知商家系统，商家系统必须指定接收通知的地址。
		PageUrl      string `json:"pageUrl" form:"pageUrl"`           // 支付成功页面跳转地址,成功跳转地址
		Order_no     string `json:"order_no" form:"order_no"`         // 平台订单号,平台唯一
		Order_time   string `json:"order_time" form:"order_time"`     // 订单时间,时间戳：格式为yyyyMMddHHmmss
		Status       string `json:"status" form:"status"`             // 状态,成功：SUCCESS 失败：FAIL
		Err_code     string `json:"err_code" form:"err_code"`         // 错误码
		Err_msg      string `json:"err_msg" form:"err_msg"`           // 错误信息
		Order_data   string `json:"order_data" form:"order_data"`     // 支付链接, 状态成功才有
		Signature    string `json:"signature" form:"signature"`       // 签名
	}
)

// 充值通知
type payNotify struct {
	TypeCode     string `json:"typeCode" form:"typeCode"`         // 业务编码
	Err_code     string `json:"err_code" form:"err_code"`         // 错误码
	Err_msg      string `json:"err_msg" form:"err_msg"`           // 错误信息
	MerchantCode string `json:"merchantCode" form:"merchantCode"` // 商户号,商户编号
	OrderNo      string `json:"orderNo" form:"orderNo"`           // 商户订单号,商户唯一订单号
	Amount       int    `json:"amount" form:"amount"`             // 订单金额,元为单位,整数,金额校检,以支付金额为准。
	TerraceNo    string `json:"terraceNo" form:"terraceNo"`       // 平台订单号,平台唯一订单号
	OrderTime    string `json:"orderTime" form:"orderTime"`       // 订单时间
	PayAmount    string `json:"payAmount" form:"payAmount"`       // 支付金额
	PayTime      string `json:"payTime" form:"payTime"`           // 支付时间
	Reserver     string `json:"reserver" form:"reserver"`         // 订单保留信息
	Status       string `json:"status" form:"status"`             // 订单状态
	Signature    string `json:"signature" form:"signature"`       // 签名
}
