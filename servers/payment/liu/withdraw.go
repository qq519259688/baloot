package liu

import (
	"bet24.com/log"
	"bet24.com/public"
	notification "bet24.com/servers/micros/notification/proto"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"encoding/json"
	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// 提现请求
func WithdrawOrder(c *gin.Context) {
	obj := db.NewLiuWithdrawReq()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "liu.WithdrawOrder", err)
		c.String(http.StatusOK, "fail")
		return
	}

	obj.In.IPAddress = strings.Split(c.Request.RemoteAddr, ":")[0]
	obj.DoAction()
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder OrderId is empty fail obj.In=%+v obj.Out=%+v", "liu.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	// 下单失败
	if obj.Out.RetCode != 1 {
		log.Debug("%s GenOrder fail obj.In=%+v obj.Out=%+v", "liu.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	// 下单成功，待审核
	if obj.Out.GetStatus != 0 {
		log.Debug("%s GenOrder success obj.In=%+v obj.Out=%+v", "liu.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "success")
		return
	}

	// 请求时，没有传手机号
	if obj.In.Mobile == "" {
		obj.In.Mobile = obj.Out.Tel
	}

	// 请求withdrawOrder的代码
	req := withdraw_req{
		MerchantCode: config.Server.LiuPay.MerchantCode,
		OrderNo:      obj.Out.OrderID,
		Type:         1,
		AccNo:        obj.In.BankCard,
		AccName:      obj.In.RealName,
		Amount:       obj.In.Amount,
		BankName:     obj.In.BankName,
		BankBranch:   "1",
		Province:     "2",
		City:         "3",
		Ccy_no:       "IDR",
	}

	params := url.Values{}
	params.Set("merchantCode", req.MerchantCode)
	params.Set("orderNo", req.OrderNo)
	params.Set("type", strconv.Itoa(req.Type))
	params.Set("accNo", req.AccNo)
	params.Set("accName", req.AccName)
	params.Set("amount", strconv.Itoa(req.Amount))
	params.Set("bankName", req.BankName)
	params.Set("bankBranch", req.BankBranch)
	params.Set("province", req.Province)
	params.Set("city", req.City)
	params.Set("ccy_no", req.Ccy_no)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.LiuPay.MD5Key
	// log.Debug("liu.WithdrawOrder.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	req.Signature = signature

	// POST请求
	buf, _ := json.Marshal(req)
	respBody := public.HttpPostByJson(config.Server.LiuPay.Url_withdraw_order, string(buf))
	log.Debug("liu.WithdrawOrder req ==> %+v  resp ==> %+v", string(buf), respBody)

	var resp withdraw_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("liu.WithdrawOrder json unmarshal req ==> %+v resp ==> %+v fail %v", string(buf), respBody, err)
		c.String(http.StatusOK, "fail")
		return
	}

	log.Debug("liu.WithdrawOrder resp ==> %+v", resp)

	// 请求响应码
	if resp.Err_code != "000002" {
		log.Error("liu.withdrawRequest post return resp fail ==> %+v", resp)
		c.String(http.StatusOK, "fail")
		return
	}

	// 请求响应码
	if resp.Status != "SUCCESS" {
		log.Error("liu.withdrawRequest post return resp fail ==> %+v", resp)
		c.String(http.StatusOK, "fail")
		return
	}

	go notification.AddNotification(obj.In.UserID, notification.Notification_Gold, "")

	// 更新银行信息
	objBank := db.NewBankInfoUp()
	objBank.In.UserID = obj.In.UserID
	objBank.In.RealName = req.AccName
	objBank.In.BankCard = req.AccNo
	objBank.In.BankName = req.BankName
	objBank.In.Mobile = obj.In.Mobile
	objBank.In.EMail = obj.In.Email
	objBank.In.Address = obj.In.Address
	objBank.DoAction()

	log.Debug("liu.WithdrawNotify need save bankInfo")

	// 加到队列
	go withdrawQueues.push(req.OrderNo)

	c.String(http.StatusOK, "success")
}

// 提现审核
func WithdrawAudit(c *gin.Context) {
	obj := db.NewLiuWithdrawAudit()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s shouldBind err %v", "WithdrawAudit", err)
		return
	}

	obj.DoAction(nil)

	// 审核失败
	if obj.Out.RetCode != 1 {
		c.JSON(http.StatusOK, obj.Out)
		return
	}

	log.Debug("liu.WithdrawAudit obj.In=%+v obj.Out=%+v", obj.In, obj.Out)

	// 审核成功
	// 状态  2=同意  11=拒绝
	if obj.In.Status != 2 {
		go notification.AddNotification(obj.Out.UserID, notification.Notification_Gold, "")
		c.JSON(http.StatusOK, obj.Out)
		return
	}

	// 请求withdrawOrder的代码
	req := withdraw_req{
		MerchantCode: config.Server.LiuPay.MerchantCode,
		OrderNo:      obj.Out.OrderID,
		Type:         1,
		AccNo:        obj.Out.BankCard,
		AccName:      obj.Out.RealName,
		Amount:       obj.Out.Amount,
		BankName:     obj.Out.BankName,
		BankBranch:   "1",
		Province:     "2",
		City:         "3",
		Ccy_no:       "IDR",
	}

	params := url.Values{}
	params.Set("merchantCode", req.MerchantCode)
	params.Set("orderNo", req.OrderNo)
	params.Set("type", strconv.Itoa(req.Type))
	params.Set("accNo", req.AccNo)
	params.Set("accName", req.AccName)
	params.Set("amount", strconv.Itoa(req.Amount))
	params.Set("bankName", req.BankName)
	params.Set("bankBranch", req.BankBranch)
	params.Set("province", req.Province)
	params.Set("city", req.City)
	params.Set("ccy_no", req.Ccy_no)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.LiuPay.MD5Key
	// log.Debug("liu.WithdrawAudit.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	req.Signature = signature

	// POST请求
	buf, _ := json.Marshal(req)
	respBody := public.HttpPostByJson(config.Server.LiuPay.Url_withdraw_order, string(buf))
	log.Debug("liu.WithdrawAudit req ==> %+v  resp ==> %+v", string(buf), respBody)

	var resp withdraw_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("liu.WithdrawAudit json unmarshal req ==> %+v resp ==> %+v fail %v", string(buf), respBody, err)
		c.String(http.StatusOK, "fail")
		return
	}

	log.Debug("liu.WithdrawAudit resp ==> %+v", resp)

	// 请求响应码
	if resp.Err_code != "000000" {
		log.Error("liu.WithdrawAudit post return resp fail ==> %+v", resp)
		c.String(http.StatusOK, "fail")
		return
	}

	// 请求响应码
	if resp.Status != "SUCCESS" {
		log.Error("liu.WithdrawAudit post return resp fail ==> %+v", resp)
		c.String(http.StatusOK, "fail")
		return
	}

	// 加到队列
	go withdrawQueues.push(req.OrderNo)

	c.JSON(http.StatusOK, obj.Out)
	return
}

// 提现查询
func withdrawSearch(orderId string) bool {
	id, _ := uuid.NewV4()

	// 请求withdrawOrder的代码
	req := withdrawSearch_req{
		RequestNo:    id.String(),
		RequestTime:  time.Now().Add(1 * time.Hour).Format("20060102150405"), // 北京时间，所以这里需要+1小时
		MerchantCode: config.Server.LiuPay.MerchantCode,
		OrderNo:      orderId,
		TerraceNo:    "",
	}

	params := url.Values{}
	params.Set("requestNo", req.RequestNo)
	params.Set("requestTime", req.RequestTime)
	params.Set("merchantCode", req.MerchantCode)
	params.Set("orderNo", req.OrderNo)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.LiuPay.MD5Key
	// log.Debug("liu.withdrawSearch.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	req.Signature = signature

	// POST请求
	buf, _ := json.Marshal(req)
	respBody := public.HttpPostByJson(config.Server.LiuPay.Url_withdraw_search, string(buf))
	log.Debug("liu.withdrawSearch req ==> %+v  resp ==> %+v", string(buf), respBody)

	var resp withdrawSearch_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("liu.withdrawSearch json unmarshal req ==> %+v resp ==> %+v fail %v", string(buf), respBody, err)
		return false
	}

	log.Debug("liu.withdrawSearch resp ==> %+v", resp)

	// 请求响应码
	if resp.Query_Status != "SUCCESS" {
		log.Error("liu.withdrawSearch post return resp fail ==> %+v", resp)
		return false
	}

	// 既没有成功，也没有失败,继续查询
	if resp.Status != "SUCCESS" && resp.Status != "FAIL" {
		return false
	}

	status := 0
	switch resp.Status {
	case "FAIL":
		status = Withdraw_Status_Failure
	case "SUCCESS":
		status = Withdraw_Status_Success
	default:
		status = Withdraw_Status_Pending_Success
	}

	obj := db.NewLiuWithdrawNotify()
	obj.In.OrderID = req.OrderNo
	obj.In.DfTransactionId = resp.TerraceNo
	obj.In.Status = status
	obj.In.DfDesc = resp.Status
	obj.In.Balance = 0
	obj.DoAction()

	log.Debug("lets.withdrawSearch obj.In=%+v obj.Out=%+v", obj.In, obj.Out)

	// 提现失败,退款
	if resp.Status == "FAIL" {
		go notification.AddNotification(obj.Out.UserID, notification.Notification_Gold, "")
	}

	return true
}
