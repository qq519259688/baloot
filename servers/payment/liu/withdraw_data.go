package liu

const (
	Withdraw_Status_Failure         = 0
	Withdraw_Status_Success         = 1
	Withdraw_Status_Pending_Success = 2
)

// 提现请求、响应
type (
	withdraw_req struct {
		MerchantCode string `json:"merchantCode" form:"merchantCode"` // 商户号,平台分配的唯一商户编号
		OrderNo      string `json:"orderNo" form:"orderNo"`           // 商户订单号,商户必须保证唯一
		Type         int    `json:"type" form:"type"`                 // 账户类型,1:对私 2：对公
		AccNo        string `json:"accNo" form:"accNo"`               // 收款账号
		AccName      string `json:"accName" form:"accName"`           // 收款户名
		Amount       int    `json:"amount" form:"amount"`             // 金额,元为单位；整数
		BankName     string `json:"bankName" form:"bankName"`         // 银行名称
		BankBranch   string `json:"bankBranch" form:"bankBranch"`     // 支行
		Province     string `json:"province" form:"province"`         // 省
		City         string `json:"city" form:"city"`                 // 城市
		Ccy_no       string `json:"ccy_no" form:"ccy_no"`             // 币种
		Signature    string `json:"signature" form:"signature"`       // 签名
	}

	withdraw_resp struct {
		Status       string `json:"status" form:"status"`             // 状态,仅表示订单受理状态，非订单代付状态; SUCCESS：成功 FAIL:失败
		Err_code     string `json:"err_code" form:"err_code"`         // 错误码,商户必须保证唯一
		Err_msg      string `json:"err_msg" form:"err_msg"`           // 错误信息,1:对私 2：对公
		MerchantCode string `json:"merchantCode" form:"merchantCode"` // 商户号
		OrderNo      string `json:"orderNo" form:"orderNo"`           // 商户订单号
		AccNo        string `json:"accNo" form:"accNo"`               // 收款账号
		AccName      string `json:"accName" form:"accName"`           // 收款户名
		Amount       int    `json:"amount" form:"amount"`             // 金额
		BankBranch   string `json:"bankBranch" form:"bankBranch"`     // 支行
		Province     string `json:"province" form:"province"`         // 省份
		City         string `json:"city" form:"city"`                 // 城市
	}
)

// 提现查询
type (
	withdrawSearch_req struct {
		RequestNo    string `json:"requestNo" form:"requestNo"`       // 请求流水号,商户必须保证唯一
		RequestTime  string `json:"requestTime" form:"requestTime"`   // 请求时间戳,时间戳：yyyyMMddHHmmss
		MerchantCode string `json:"merchantCode" form:"merchantCode"` // 商户号,平台分配的唯一商户号
		OrderNo      string `json:"orderNo" form:"orderNo"`           // 商户订单号,商户必须保证唯一
		TerraceNo    string `json:"terraceNo" form:"terraceNo"`       // 平台订单号,平台唯一
		Signature    string `json:"signature" form:"signature"`       // 签名
	}

	withdrawSearch_resp struct {
		Query_Status string `json:"query_Status" form:"query_Status"` // 查询状态,SUCCESS 成功；FAIL 失败；只表示本次查询状态，不表示原业务状态
		Err_code     string `json:"err_code" form:"err_code"`         // 错误码
		Err_msg      string `json:"err_msg" form:"err_msg"`           // 错误信息
		RequestNo    string `json:"requestNo" form:"requestNo"`       // 请求流水号
		RequestTime  string `json:"requestTime" form:"requestTime"`   // 请求时间戳
		MerchantCode string `json:"merchantCode" form:"merchantCode"` // 商户号
		OrderNo      string `json:"orderNo" form:"orderNo"`           // 商户订单号
		TerraceNo    string `json:"terraceNo" form:"terraceNo"`       // 平台订单号
		Type         string `json:"type" form:"type"`                 // 账户类型
		AccNo        string `json:"accNo" form:"accNo"`               // 收款账号
		AccName      string `json:"accName" form:"accName"`           // 收款户名
		CcyNo        string `json:"ccyNo" form:"ccyNo"`               // 币种
		Amount       int    `json:"amount" form:"amount"`             // 金额,元为单位；整数
		Fee          int    `json:"fee" form:"fee"`                   // 手续费,元为单位；整数
		Status       string `json:"status" form:"status"`             // 代付状态,SUCCESS:成功 FAIL:失败 UNKNOW:未知
	}
)
