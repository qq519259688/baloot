package liu

import (
	"fmt"
	"net/url"
	"sort"
)

// 创建加密串
func createEncryptStr(params url.Values) string {
	var key []string
	var str = ""
	for k := range params {
		if k != "Sign" && k != "sign" {
			key = append(key, k)
		}
	}
	sort.Strings(key)
	for i := 0; i < len(key); i++ {
		if params.Get(key[i]) == "" {
			continue
		}

		if str != "" {
			str = str + "&"
		}

		//log.Debug("key[%v]=%v", i, str)

		str = str + fmt.Sprintf("%v=%v", key[i], params.Get(key[i]))
	}

	return str
}

func getSeconds(reqTimes int) int {
	seconds := 0
	reqTimes++
	switch reqTimes {
	case 1: // 第1次请求
		seconds = 10
	case 2: // 第2次请求
		seconds = 30
	case 3: // 第3次请求
		seconds = 60
	case 4: // 第4次请求
		seconds = 120
	case 5: // 第5次请求
		seconds = 300
	case 6: // 第6次请求
		seconds = 600
	case 7: // 第7次请求
	case 8: // 第8次请求
	case 9: // 第9次请求
		seconds = 3000
	default:
		seconds = 6000
	}

	return seconds
}
