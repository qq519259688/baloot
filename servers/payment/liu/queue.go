package liu

import (
	"sync"
	"time"

	"bet24.com/log"

	"bet24.com/servers/common"

	"bet24.com/servers/payment/db"
)

var withdrawQueues *withdrawQueue

// 请求队列
type withdrawQueue struct {
	queues []*withdrawInfo
	lock   *sync.RWMutex
}

// 提现信息
type withdrawInfo struct {
	OrderId  string // 订单号
	ReqTimes int    // 请求次数
	ts       int    // 时间戳
}

func Run() {
	withdrawQueues = new(withdrawQueue)
	withdrawQueues.lock = &sync.RWMutex{}

	// 加载数据
	withdrawQueues.loadQueue()

	// 去检查
	withdrawQueues.check()

	log.Debug("liu.queue withdrawQueues run...")
}

// 加载数据
func (wq *withdrawQueue) loadQueue() {
	obj := db.NewLiuWithdrawQueue()
	obj.DoAction()
	var list []*withdrawInfo
	for _, v := range obj.Out.List {
		list = append(list, &withdrawInfo{
			OrderId:  v,
			ReqTimes: 0,
			ts:       0,
		})
	}
	wq.lock.Lock()
	defer wq.lock.Unlock()
	wq.queues = list
}

// 检查数据
func (wq *withdrawQueue) check() {
	ticker := time.NewTicker(1 * time.Second)
	go func(t *time.Ticker) {
		for {
			select {
			case <-t.C:
				// 查询提现订单并请求
				wq.search()
			}
		}
	}(ticker)
}

// 查询提现订单并请求
func (wq *withdrawQueue) search() {
	for i := 0; i < len(wq.queues); i++ {
		// 还没到时间
		if wq.queues[i].ts > common.GetTimeStamp() {
			continue
		}

		log.Debug("liu.queue search idx=[%d] ts=%d ==> %+v", i, common.GetTimeStamp(), wq.queues[i])

		// 失败,查询次数+1
		if ok := withdrawSearch(wq.queues[i].OrderId); !ok {
			wq.queues[i].ts = common.GetTimeStamp() + getSeconds(wq.queues[i].ReqTimes)
			wq.queues[i].ReqTimes++
			continue
		}

		// 成功，从队列里删除
		wq.lock.Lock()
		wq.queues = append(wq.queues[:i], wq.queues[i+1:]...)
		wq.lock.Unlock()
	}
}

// 添加数据(压数据)
func (wq *withdrawQueue) push(orderId string) {
	info := &withdrawInfo{
		OrderId:  orderId,
		ReqTimes: 0,
		ts:       common.GetTimeStamp(),
	}

	wq.lock.Lock()
	defer wq.lock.Unlock()
	wq.queues = append(wq.queues, info)
}

func Dump(param1, param2 string) {
	switch param1 {
	case "sys":
		withdrawQueues.dumpOrder(param2)
	default:
		log.Debug("withdrawQueues.Dump unhandled %s:%s", param1, param2)
	}
}

func (wq *withdrawQueue) dumpOrder(param string) {
	log.Debug("-------------------------------")
	log.Debug("withdrawQueue.dumpOrder %s %d", param, common.GetTimeStamp())
	defer func() {
		log.Debug("+++++++++++++++++++++++++++++++")
		log.Debug("")
	}()

	for k, v := range wq.queues {
		if param != "" && param == v.OrderId {
			log.Debug("idx[%d] ==> %+v", k, v)
			return
		}

		log.Debug("idx[%d] ==> %+v", k, v)
	}
	return
}
