package zhongshui

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"bet24.com/log"
	"bet24.com/public"
	notification "bet24.com/servers/micros/notification/proto"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
)

// 提现请求
func WithdrawOrder(c *gin.Context) {
	obj := db.NewZShuiWithdrawReq()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "zhongshui.WithdrawOrder", err)
		c.String(http.StatusOK, "fail")
		return
	}

	obj.In.IPAddress = strings.Split(c.Request.RemoteAddr, ":")[0]
	obj.DoAction()
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v obj.Out=%+v", "zhongshui.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	// 请求时，没有传手机号
	if obj.In.Mobile == "" {
		obj.In.Mobile = obj.Out.Tel
	}

	id, _ := uuid.NewV4()

	// 请求withdrawOrder的代码
	req := withdraw_req{
		GymchtId:           config.Server.ZShuiPay.GymchtId,
		DfSn:               obj.Out.OrderID,
		ReceiptAmount:      obj.In.Amount,
		CurType:            "1",
		PayType:            "1",
		PaymentModes:       "IMPS",
		ReceiptName:        obj.In.RealName,
		ReceiptPan:         obj.In.BankCard,
		ReceiptBankNm:      obj.In.BankName,
		SettleNo:           "PYTM000001",
		BeneficiaryVPA:     "VPA",
		BeneficiaryPhoneNo: obj.In.Mobile, // PS
		BeneficiaryEmail:   obj.In.Email,
		BeneficiaryAddress: obj.In.Address,
		Mobile:             obj.In.Mobile,
		AcctType:           "0",
		Nonce:              id.String(),
		NotifyUrl:          config.Server.ZShuiPay.Url_withdraw_notify,
	}

	params := url.Values{}
	params.Set("gymchtId", req.GymchtId)
	params.Set("dfSn", req.DfSn)
	params.Set("receiptAmount", strconv.Itoa(req.ReceiptAmount))
	params.Set("curType", req.CurType)
	params.Set("payType", req.PayType)
	params.Set("paymentModes", req.PaymentModes)
	params.Set("receiptName", req.ReceiptName)
	params.Set("receiptPan", req.ReceiptPan)
	params.Set("receiptBankNm", req.ReceiptBankNm)
	params.Set("settleNo", req.SettleNo)
	params.Set("beneficiaryVPA", req.BeneficiaryVPA)
	params.Set("beneficiaryPhoneNo", req.BeneficiaryPhoneNo)
	params.Set("beneficiaryEmail", req.BeneficiaryEmail)
	params.Set("beneficiaryAddress", req.BeneficiaryAddress)
	params.Set("mobile", req.Mobile)
	params.Set("acctType", req.AcctType)
	params.Set("nonce", req.Nonce)
	params.Set("notifyUrl", req.NotifyUrl)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.ZShuiPay.GymKey
	// log.Debug("zhongshui.WithdrawOrder.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	params.Set("sign", signature)

	respBody := public.HttpPostForm(config.Server.ZShuiPay.Url_withdraw_order, params)

	log.Debug("zhongshui.WithdrawOrder req ==> %+v  resp ==> %+v", params, respBody)

	var resp withdraw_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("zhongshui.WithdrawOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		c.String(http.StatusOK, "fail")
		return
	}

	// log.Debug("zhongshui.WithdrawOrder resp ==> %+v", resp)

	// 请求响应码，00000表示成功，其他失败
	if resp.ResultCode != "00000" {
		log.Error("zhongshui.withdrawRequest post return resp fail ==> %+v", resp)
		c.String(http.StatusOK, "fail")
		return
	}

	paramsResp := url.Values{}
	paramsResp.Set("resultCode", resp.ResultCode)
	paramsResp.Set("message", resp.Message)
	paramsResp.Set("gymchtId", req.GymchtId)
	paramsResp.Set("dfSn", resp.DfSn)
	paramsResp.Set("dfTransactionId", resp.DfTransactionId)
	paramsResp.Set("dfState", resp.DfState)
	paramsResp.Set("dfDesc", resp.DfDesc)
	paramsResp.Set("timeEnd", resp.TimeEnd)
	paramsResp.Set("nonce", resp.Nonce)

	// 生成签名
	checkContent_resp := createEncryptStr(paramsResp) + "&key=" + config.Server.ZShuiPay.GymKey
	// log.Debug("zhongshui.WithdrawOrder checkContent_resp ==> %s", checkContent_resp)
	signature_resp := strings.ToUpper(public.GetMd5String(checkContent_resp))

	// 校验签名
	if resp.Sign != signature_resp {
		log.Error("zhongshui.WithdrawOrder 签名失败 post return resp ==> %+v", resp)
		c.String(http.StatusOK, "fail")
		return
	}

	log.Debug("zhongshui.WithdrawOrder 签名成功")

	go notification.AddNotification(obj.In.UserID, notification.Notification_Gold, "")

	// 更新银行信息
	objBank := db.NewBankInfoUp()
	objBank.In.UserID = obj.In.UserID
	objBank.In.RealName = req.ReceiptName
	objBank.In.BankCard = req.ReceiptPan
	objBank.In.BankName = req.ReceiptBankNm
	objBank.In.Mobile = req.Mobile
	objBank.In.EMail = req.BeneficiaryEmail
	objBank.In.Address = req.BeneficiaryAddress
	objBank.DoAction()

	// log.Debug("zhongshui.WithdrawNotify need save bankInfo")

	c.String(http.StatusOK, "success")
	return
}

// 提现通知
func WithdrawNotify(c *gin.Context) {

	data, _ := io.ReadAll(c.Request.Body)
	log.Debug("zhongshui.WithdrawNotify ==> ctx.Request.body: %v", string(data))

	var req withdrawNotify
	err := json.Unmarshal(data, &req)
	if err != nil {
		log.Debug("%s query params err %v", "zhongshui.WithdrawNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	// log.Debug("zhongshui.WithdrawNotify ==> %+v", req)

	// 请求响应码，00000表示成功
	if req.ResultCode != "00000" {
		log.Error("zhongshui.WithdrawNotify req ==> %+v", req)
		c.String(http.StatusOK, "")
		return
	}

	params := url.Values{}
	params.Set("resultCode", req.ResultCode)
	params.Set("message", req.Message)
	params.Set("gymchtId", req.GymchtId)
	params.Set("dfTransactionId", req.DfTransactionId)
	params.Set("dfSn", req.DfSn)
	params.Set("dfState", req.DfState)
	params.Set("receiptAmount", strconv.Itoa(req.ReceiptAmount))
	params.Set("dfDesc", req.DfDesc)
	params.Set("receiptName", req.ReceiptName)
	params.Set("receiptPan", req.ReceiptPan)
	params.Set("receiptBankNm", req.ReceiptBankNm)
	params.Set("mobile", req.Mobile)
	params.Set("timeEnd", req.TimeEnd)
	params.Set("memo", req.Memo)
	params.Set("nonce", req.Nonce)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.ZShuiPay.GymKey
	// log.Debug("zhongshui.WithdrawNotify.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))

	// 校验签名
	if req.Sign != signature {
		log.Error("zhongshui.WithdrawNotify 签名失败 post req ==> %+v  signature ==> %s", req, signature)
		return
	}

	log.Debug("zhongshui.WithdrawNotify 签名成功")

	// 代付状态 00-成功   01-处理中  02-处理失败
	if req.DfState != "00" && req.DfState != "02" {
		log.Error("zhongshui.WithdrawNotify response fail ==> %+v", req)
		return
	}

	// 查询账户余额
	balance := queryAccount()

	obj := db.NewZShuiWithdrawNotify()
	obj.In.OrderID = req.DfSn
	obj.In.BankCard = req.ReceiptPan
	obj.In.DfTransactionId = req.DfTransactionId
	obj.In.DfState = req.DfState
	obj.In.DfDesc = req.DfDesc
	obj.In.Balance = balance
	obj.DoAction()

	log.Debug("zhongshui.WithdrawNotify obj.In=%+v obj.Out=%+v", obj.In, obj.Out)

	// 提现失败,退款
	if req.DfState == "02" {
		go notification.AddNotification(obj.Out.UserID, notification.Notification_Gold, "")
	}

	c.String(http.StatusOK, "success")
	return
}

// 账户余额
func queryAccount() int {
	id, _ := uuid.NewV4()
	req := queryAccount_req{
		GymchtId: config.Server.ZShuiPay.GymchtId,
		QryTime:  time.Now().Format("20060102150405"),
		QryType:  "1",
		Nonce:    id.String(),
	}

	params := url.Values{}
	params.Set("gymchtId", req.GymchtId)
	params.Set("qryTime", req.QryTime)
	params.Set("qryType", req.QryType)
	params.Set("nonce", req.Nonce)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.ZShuiPay.GymKey
	// log.Debug("zhongshui.queryAccount.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	params.Set("sign", signature)

	respBody := public.HttpPostForm(config.Server.ZShuiPay.Url_withdraw_queryAccount, params)
	log.Debug("zhongshui.queryAccount req ==> %+v  resp ==> %+v", params, respBody)

	var resp queryAccount_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("zhongshui.queryAccount json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		return -1
	}

	// log.Debug("zhongshui.queryAccount resp ==> %+v", resp)

	// 请求响应码，00000表示成功，其他失败
	if resp.ResultCode != "00000" {
		log.Error("zhongshui.withdrawRequest post return resp fail ==> %+v", resp)
		return -1
	}

	return resp.Balance
}
