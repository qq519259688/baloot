package zhongshui

// 充值请求、响应
type (
	pay_req struct {
		GymchtId     string `json:"gymchtId"`               // 平台分配商户号
		TradeSn      string `json:"tradeSn"`                // 商户系统内容订单号，32字符以内，可含字母，确保商户系统内唯一
		OrderAmount  int    `json:"orderAmount"`            // 交易金额,单位分，只允许数字
		GoodsName    string `json:"goodsName"`              // 商品名称
		ExpirySecond int    `json:"expirySecond,omitempty"` // 否, 订单有效时间，单位秒
		TradeSource  string `json:"tradeSource"`            // 支付来源,upi,imps,upiquick,PayTm,CashFree,PayU,Razorpay,Easebuzz,ShuXin,Gaga,Panda,DFY,cc,dc,nb,paypal,wallet,webstaging
		Realname     string `json:"realname"`               // 付款人名字
		UserMobile   string `json:"userMobile"`             // 付款人电话
		UserEmail    string `json:"userEmail"`              // 付款人Email
		Callback_url string `json:"callback_url"`           // 支付完跳转地址,付完成后跳转地址的url，255字符以内，确保通过互联网能访问该地址,部分支付方式无用,可空，url必须以http或https协议开头
		NotifyUrl    string `json:"notifyUrl"`              // 通知地址,接收交易结果通知的url，255字符以内，确保通过互联网能访问该地址，url必须以http或https协议开头
	}

	pay_resp struct {
		ResultCode string `json:"resultCode"` // 响应码,请求响应码，00000表示成功，其他失败
		Message    string `json:"message"`    // 响应码描述
		GymchtId   string `json:"gymchtId"`   // 商户号,平台分配商户号
		Code_url   string `json:"code_url"`   // 响应内容,支付链接，可以直接使用,如为空，则不能使用
		Sign       string `json:"sign"`       // 签名,Md5签名结果
	}
)

// 充值通知
type payNotify struct {
	GymchtId       string `json:"gymchtId"`       // 商户号, 平台商户号
	Transaction_id string `json:"transaction_id"` // 平台订单号，32字符以内
	TradeSn        string `json:"tradeSn"`        // 商户订单号,商户系统内容订单号，32字符以内，可含字母，确保商户系统内唯一
	Pay_result     string `json:"pay_result"`     // 支付结果,0-成功，其他失败
	Pay_info       string `json:"pay_info"`       // 否 支付结果信息，支付成功时为空
	OrderAmount    int    `json:"orderAmount"`    // 交易金额,单位分，只允许数字
	BankType       string `json:"bankType"`       // 否 付款银行, 银行类型
	TimeEnd        string `json:"timeEnd"`        // 支付完成时间，格式为yyyyMMddHHmmss
	Sign           string `json:"sign"`           // 签名, Md5签名结果
	T0Flag         string `json:"t0Flag"`         // 是否T0交易，1-是，0-否
}

// 充值结果
type payResult struct {
	GymchtId       string `json:"gymchtId"`       // 商户号,平台商户号
	TradeSn        string `json:"tradeSn"`        // 商户订单号,商户系统内容订单号，32字符以内，可含字母，确保商户系统内唯一
	Transaction_id string `json:"transaction_id"` // 平台订单号，32字符以内
	OrderAmount    int    `json:"orderAmount"`    // 交易金额, 单位分，只允许数字
	TradeState     string `json:"tradeState"`     // 交易状态， SUCCESS—支付成功 	NOTPAY—未支付	CLOSED—已关闭	PAYING-订单处理中	PAYERROR—支付失败
	Nonce          string `json:"nonce"`          // 32位随机字符串
	Sign           string `json:"sign"`           // Md5签名结果
}
