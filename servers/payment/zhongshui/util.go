package zhongshui

import (
	"fmt"
	"net/url"
	"sort"
)

// 创建加密串
func createEncryptStr(params url.Values) string {
	var key []string
	var str = ""
	for k := range params {
		if k != "Sign" && k != "sign" {
			key = append(key, k)
		}
	}
	sort.Strings(key)
	for i := 0; i < len(key); i++ {
		if params.Get(key[i]) == "" {
			continue
		}

		if str != "" {
			str = str + "&"
		}

		//log.Debug("key[%v]=%v", i, str)

		str = str + fmt.Sprintf("%v=%v", key[i], params.Get(key[i]))
	}

	return str
}
