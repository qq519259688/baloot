package zhongshui

import (
	"encoding/json"
	"math"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bet24.com/servers/payment/shop"

	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"

	coreClient "bet24.com/servers/coreservice/client"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_ZShui_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "zhongshui.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "zhongshui.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "zhongshui.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "zhongshui.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "zhongshui.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "zhongshui.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	//请求payOrder的代码
	req := pay_req{
		GymchtId:     config.Server.ZShuiPay.GymchtId,
		TradeSn:      obj.Out.OrderID,
		OrderAmount:  int(math.Ceil(obj.In.Price)),
		GoodsName:    obj.In.ProductID,
		TradeSource:  "webstaging",
		Realname:     obj.In.Name,
		UserMobile:   obj.In.Tel,
		UserEmail:    obj.In.Email,
		Callback_url: config.Server.ZShuiPay.Url_pay_callback,
		NotifyUrl:    config.Server.ZShuiPay.Url_pay_notify,
	}

	params := url.Values{}
	params.Set("gymchtId", req.GymchtId)
	params.Set("tradeSn", req.TradeSn)
	params.Set("orderAmount", strconv.Itoa(req.OrderAmount))
	params.Set("goodsName", req.GoodsName)
	params.Set("tradeSource", req.TradeSource)
	params.Set("realname", req.Realname)
	params.Set("userMobile", req.UserMobile)
	params.Set("userEmail", req.UserEmail)
	params.Set("callback_url", req.Callback_url)
	params.Set("notifyUrl", req.NotifyUrl)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.ZShuiPay.GymKey
	// log.Debug("order.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	params.Set("sign", signature)

	// POST请求
	respBody := public.HttpPostForm(config.Server.ZShuiPay.Url_pay_order, params)
	log.Debug("zhongshui.payOrder req ==> %+v  resp ==> %+v", params, respBody)

	var resp pay_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("zhongshui.payOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		return
	}

	// log.Debug("zhongshui.payOrder resp ==> %+v", resp)

	// 请求响应码，00000表示成功，其他失败
	if resp.ResultCode != "00000" {
		log.Error("zhongshui.payOrder post return resp fail ==> %+v", resp)
		return
	}

	paramsResp := url.Values{}
	paramsResp.Set("resultCode", resp.ResultCode)
	paramsResp.Set("message", resp.Message)
	paramsResp.Set("gymchtId", config.Server.ZShuiPay.GymchtId)
	paramsResp.Set("code_url", resp.Code_url)

	// 生成签名
	checkContent_resp := createEncryptStr(paramsResp) + "&key=" + config.Server.ZShuiPay.GymKey
	// log.Debug("order.checkContent_resp ==> %s", checkContent_resp)
	signature_resp := strings.ToUpper(public.GetMd5String(checkContent_resp))

	// 校验签名
	if resp.Sign != signature_resp {
		log.Error("zhongshui.payOrder 签名失败 post return resp ==> %+v", resp)
		return
	}

	log.Debug("zhongshui.payOrder 签名成功")

	// 判断支付链接是否为空
	if resp.Code_url == "" {
		log.Error("zhongshui.payOrder response Code_url is empty %+v", resp)
		return
	}

	// log.Debug("paymentUrl ==> %s", resp.Code_url)

	// 跳转到支付页面，以便持卡人完成支付过程
	c.Redirect(http.StatusMovedPermanently, resp.Code_url)
	return
}

// 回调通知
func PayNotify(c *gin.Context) {
	var resp payNotify
	if err := c.ShouldBind(&resp); err != nil {
		log.Debug("%s query params err %v", "zhongshui.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("zhongshui.PayNotify resp ==> %+v", resp)

	params := url.Values{}
	params.Set("gymchtId", config.Server.ZShuiPay.GymchtId)
	params.Set("transaction_id", resp.Transaction_id)
	params.Set("tradeSn", resp.TradeSn)
	params.Set("pay_result", resp.Pay_result)
	params.Set("pay_info", resp.Pay_info)
	params.Set("orderAmount", strconv.Itoa(resp.OrderAmount))
	params.Set("bankType", resp.BankType)
	params.Set("timeEnd", resp.TimeEnd)
	params.Set("t0Flag", resp.T0Flag)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.ZShuiPay.GymKey
	// log.Debug("order.OrderNotify checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))

	// 校验签名
	if resp.Sign != signature {
		log.Error("zhongshui.PayNotify 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("zhongshui.PayNotify 签名成功")

	// 0-成功，其他失败
	if resp.Pay_result != "0" {
		log.Error("zhongshui.PayNotify resp ==> %+v 失败", resp)
		return
	}

	// 数据库操作
	obj := db.NewNotify(db.SP_ZShui_NOTIFY)
	obj.In.OrderID = resp.TradeSn
	obj.In.TradeID = resp.Transaction_id
	//obj.In.Price = resp.OrderAmount
	obj.DoAction(nil)

	//操作成功,给道具
	if obj.Out.RetCode == 1 {
		//充值
		resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
		log.Debug("%s 充值成功 %+v", "zhongshui.PayNotify", resp)
	}

	c.String(http.StatusOK, "success")
	return
}

// 支付完成跳转处理
func PayCallback(c *gin.Context) {
	log.Debug("zhongshui.payCallback:%v", c)
	// c.Redirect(http.StatusMovedPermanently, config.Server.Cropay.Url_resultPay)
}
