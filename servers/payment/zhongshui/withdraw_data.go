package zhongshui

// 提现请求、响应
type (
	withdraw_req struct {
		GymchtId           string `json:"gymchtId"`                     // 平台分配商户号
		DfSn               string `json:"dfSn"`                         // 商户代付订单号，32字符以内，可含字母，确保商户系统内唯一
		ReceiptAmount      int    `json:"receiptAmount"`                // 代付金额,单位分，只允许数字
		CurType            string `json:"curType"`                      // 金额类型 金额类型 1-卢布（单位：分）
		PayType            string `json:"payType"`                      // 付款方式	1-余额代付	缺损值为1
		PaymentModes       string `json:"paymentModes,omitempty"`       // 否 使用支付模式,UPI,NEFT,IMPS,RTGS,BankCard,Paytm。UPI最大10万,IMPS最大20万,NEFT不限额,当前NEFT强制走IMPS,限额20万
		ReceiptName        string `json:"receiptName"`                  // 收款人姓名,账户名，UPI.NEFT和IMPS模式必填,建议都传
		ReceiptPan         string `json:"receiptPan"`                   // 收款人卡号,银行账户（UPI-ID或者PayTm账号或者银行卡账号），NEFT和IMPS模式必填,建议都传
		ReceiptBankNm      string `json:"receiptBankNm,omitempty"`      // 否 收款银行,BankCard模式必填
		SettleNo           string `json:"settleNo,omitempty"`           // 否 银行代码,Accounts IFSC，NEFT和IMPS和BankCard模式必填,建议都传，PYTM000001
		BeneficiaryVPA     string `json:"beneficiaryVPA"`               // UPI-VPA,UPI模式必填,和手机号二选一,建议都传, 优先选择VPA, 建议使用VPA,UPI-手机号错误率高(88799788@PYTM)
		BeneficiaryPhoneNo string `json:"beneficiaryPhoneNo,omitempty"` // 否 UPI-手机号,UPI模式必填,和VPA二选一,建议都传, 优先选择VPA(987654321)
		BeneficiaryEmail   string `json:"beneficiaryEmail"`             // 收款人Email
		BeneficiaryAddress string `json:"beneficiaryAddress"`           // 收款人地址
		Mobile             string `json:"mobile"`                       // 收款人联系电话
		AcctType           string `json:"acctType"`                     // 帐号类型, 默认传0
		Memo               string `json:"memo,omitempty"`               // 否 摘要,简单的附言
		Nonce              string `json:"nonce"`                        // 32位随机字符串
		NotifyUrl          string `json:"notifyUrl,omitempty"`          // 否 通知地址,接收代付结果通知的url，255字符以内，确保通过互联网能访问该地址
	}

	withdraw_resp struct {
		ResultCode string `json:"resultCode"` // 请求响应码，00000表示成功，其他失败
		Message    string `json:"message"`    // 响应码描述
		GymchtId   string `json:"gymchtId"`   // 否 商户号,平台分配商户号
		DfSn       string `json:"dfSn"`       // 商户代付订单号

		DfTransactionId string `json:"dfTransactionId"` // 平台代付单号，32字符以内
		DfState         string `json:"dfState"`         // 代付订单状态 代付状态 	00-成功	01-处理中	02-处理失败
		DfDesc          string `json:"dfDesc"`          // 代付状态描述
		TimeEnd         string `json:"timeEnd"`         // 代付完成时间，格式：YYYYMMDDhhmmss
		Nonce           string `json:"nonce"`           // 32位随机字符串
		Sign            string `json:"sign"`            // Md5签名结果
	}
)

// 提现通知
type withdrawNotify struct {
	ResultCode      string `json:"resultCode"`      // 响应码, 请求响应码，00000表示成功
	Message         string `json:"message"`         // 响应码描述
	GymchtId        string `json:"gymchtId"`        // 否 商户号,平台分配商户号
	DfTransactionId string `json:"dfTransactionId"` // 平台代付单号，32字符以内
	DfSn            string `json:"dfSn"`            // 商户代付订单号
	DfState         string `json:"dfState"`         // 代付状态	00-成功，02-失败
	ReceiptAmount   int    `json:"receiptAmount"`   // 代付金额, 单位分，只允许数字
	DfDesc          string `json:"dfDesc"`          // 代付状态描述
	ReceiptName     string `json:"receiptName"`     // 收款人姓名
	ReceiptPan      string `json:"receiptPan"`      // 收款人卡号
	ReceiptBankNm   string `json:"receiptBankNm"`   // 收款银行
	Mobile          string `json:"mobile"`          // 否 收款人手机号码
	TimeEnd         string `json:"timeEnd"`         // 代付完成时间，格式：YYYYMMDDhhmmss
	Memo            string `json:"memo"`            // 否 简单的附言
	Nonce           string `json:"nonce"`           // 32位随机字符串
	Sign            string `json:"sign"`            // Md5签名结果
}

// 账户余额查询请求、响应
type (
	queryAccount_req struct {
		GymchtId string `json:"gymchtId"` // 商户号，平台分配商户号
		QryTime  string `json:"qryTime"`  // 查询时间，格式为yyyyMMddHHmmss
		QryType  string `json:"qryType"`  // 查询类型	1-余额	2-垫资可用额度	缺损值为1
		Nonce    string `json:"nonce"`    // 32位随机字符串
	}

	queryAccount_resp struct {
		ResultCode string `json:"resultCode"` // 请求响应码，00000表示成功，其他失败
		Message    string `json:"message"`    // 响应码描述

		GymchtId      string `json:"gymchtId"`      // 平台商户号
		AccountStatus string `json:"accountStatus"` // 账户状态，00-正常
		Balance       int    `json:"balance"`       // 账户余额,单位分，只允许数字
		Nonce         string `json:"nonce"`         // 32位随机字符串
		Sign          string `json:"sign"`          // Md5签名结果
	}
)
