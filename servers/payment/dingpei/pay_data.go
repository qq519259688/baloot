package dingpei

type (
	pay_req struct {
		MerchantNo      string `json:"merchantNo" form:"merchantNo"`                       // 商户号
		Type            string `json:"type" form:"type"`                                   // 支付类型, 详见通道编码
		Amount          string `json:"amount" form:"amount"`                               // 充值金额,精确到两位小数点，单位为元。如充值100.50元，请传递100.50
		MerchantOrderNo string `json:"merchantOrderNo" form:"merchantOrderNo"`             // 商户唯一订单号
		ClientIp        string `json:"clientIp" form:"clientIp"`                           // 用户的客户端IP地址
		ReturnUrl       string `json:"returnUrl" form:"returnUrl"`                         // 充值成功后，支付页面跳转地址
		NotifyUrl       string `json:"notifyUrl" form:"notifyUrl"`                         // 充值成功后，异步通知地址
		PayUserName     string `json:"payUserName,omitempty" form:"payUserName,omitempty"` // 付款人真实姓名，不能有特殊符号和空格。在某些通道中该参数必填，具体请联系客服
		Lang            string `json:"lang,omitempty" form:"lang,omitempty"`               // 使用的语言,默认为en,可选值ar:阿拉伯语,en:英文;填写错误或未填写均使用英文
		Sign            string `json:"sign" form:"sign"`                                   // 签名
	}

	pay_resp struct {
		Code int         `json:"code" form:"code"` // 0：成功，1：失败
		Msg  string      `json:"msg" form:"msg"`   // 错误信息，code为0时，本值为空
		Data payRespData `json:"data" form:"data"`
	}

	payRespData struct {
		Amount          int    `json:"amount" form:"amount"`                   // 订单金额
		RealAmount      int    `json:"realAmount" form:"realAmount"`           // 用户需要支付的实际金额（某些充值方式需要金额浮动）
		MerchantOrderNo string `json:"merchantOrderNo" form:"merchantOrderNo"` // 商户唯一订单号
		SystemOrderNo   string `json:"systemOrderNo" form:"systemOrderNo"`     // 接口生成的系统唯一订单号
		PayUrl          string `json:"payUrl" form:"payUrl"`                   // 支付网址（商户需要自行将本网址生成二维码或跳转）
	}
)

type payNotify struct {
	SystemOrderNo   string `json:"systemOrderNo" form:"systemOrderNo"`     // 系统唯一订单号
	MerchantOrderNo string `json:"merchantOrderNo" form:"merchantOrderNo"` // 商户唯一订单号
	Amount          string `json:"amount" form:"amount"`                   // 订单金额
	RealAmount      string `json:"realAmount" form:"realAmount"`           // 用户实际支付金额
	Status          int    `json:"status" form:"status"`                   // 状态，0：失败；1：成功
	Sign            string `json:"sign" form:"sign"`                       // 签名
}
