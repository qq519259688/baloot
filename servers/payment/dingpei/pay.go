package dingpei

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bet24.com/servers/payment/shop"

	"bet24.com/log"
	"bet24.com/public"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_Dingpei_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "Dingpei.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	//// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "dingpei.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "dingpei.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "dingpei.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "dingpei.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "Dingpei.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	// 请求payOrder的代码
	req := pay_req{
		MerchantNo:      config.Server.DingpeiPay.MerchantNo,
		Type:            config.Server.DingpeiPay.Type,
		Amount:          fmt.Sprintf("%f.00", obj.In.Price),
		MerchantOrderNo: obj.Out.OrderID,
		ClientIp:        obj.In.IpAddress,
		ReturnUrl:       config.Server.DingpeiPay.Url_pay_page,
		NotifyUrl:       config.Server.DingpeiPay.Url_pay_notify,
		PayUserName:     "",
		Lang:            "",
		Sign:            "",
	}

	params := url.Values{}
	params.Set("merchantNo", req.MerchantNo)
	params.Set("type", req.Type)
	params.Set("amount", req.Amount)
	params.Set("merchantOrderNo", req.MerchantOrderNo)
	params.Set("clientIp", req.ClientIp)
	params.Set("returnUrl", req.ReturnUrl)
	params.Set("notifyUrl", req.NotifyUrl)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.DingpeiPay.MD5Key
	// log.Debug("DingpeiPay.payOrder checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	req.Sign = signature
	params.Set("sign", req.Sign)

	// POST请求
	respBody := public.HttpPostForm(config.Server.DingpeiPay.Url_pay_order, params)
	log.Debug("DingpeiPay.payOrder req ==> %+v  resp ==> %+v", params, respBody)

	var resp pay_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("DingpeiPay.payOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		return
	}

	log.Debug("DingpeiPay.payOrder resp ==> %+v", resp)

	// 请求响应码 0：成功，1：失败
	if resp.Code != 0 {
		log.Error("DingpeiPay.payOrder post return resp fail ==> %+v", resp)
		return
	}

	// 跳转到支付页面，以便持卡人完成支付过程
	c.Redirect(http.StatusMovedPermanently, resp.Data.PayUrl)
	return
}

// 回调通知
func PayNotify(c *gin.Context) {
	bodyData, _ := io.ReadAll(c.Request.Body)
	log.Debug("DingpeiPay.PayNotify ==> ctx.Request.body: %v", string(bodyData))

	var resp payNotify
	if err := json.Unmarshal(bodyData, &resp); err != nil {
		log.Debug("%s query params err %v", "DingpeiPay.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("DingpeiPay.PayNotify resp ==> %+v", resp)

	// 状态，0：失败；1：成功
	if resp.Status != 1 {
		log.Error("DingpeiPay.PayNotify resp ==> %+v 失败", resp)
		return
	}

	params := url.Values{}
	params.Set("systemOrderNo", resp.SystemOrderNo)
	params.Set("merchantOrderNo", resp.MerchantOrderNo)
	params.Set("amount", resp.Amount)
	params.Set("realAmount", resp.RealAmount)
	params.Set("status", strconv.Itoa(resp.Status))

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.DingpeiPay.MD5Key
	// log.Debug("DingpeiPay.PayNotify checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))

	// 校验签名
	if resp.Sign != signature {
		log.Error("DingpeiPay.PayNotify 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("DingpeiPay.PayNotify 签名成功")

	//price, err := strconv.Atoi(strings.ReplaceAll(resp.Amount, ".00", ""))
	//if err != nil {
	//	log.Error("DingpeiPay.PayNotify price err %v", err)
	//	return
	//}

	// 数据库操作
	obj := db.NewNotify(db.SP_Dingpei_NOTIFY)
	obj.In.OrderID = resp.MerchantOrderNo
	obj.In.TradeID = resp.SystemOrderNo
	// obj.In.Price = price
	obj.DoAction(nil)

	// 操作成功,给道具
	if obj.Out.RetCode == 1 {
		// 充值
		resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
		log.Debug("%s 充值成功 %+v", "liu.PayNotify", resp)
	}

	c.String(http.StatusOK, "success")
	return
}

// 支付完成跳转处理
func PayCallback(c *gin.Context) {
	c.String(http.StatusOK, "success")
	log.Debug("Dingpei.payCallback")
	// c.Redirect(http.StatusMovedPermanently, config.Server.DingpeiPay.Url_resultPay)
}
