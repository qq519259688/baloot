package dingpei

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bet24.com/log"
	"bet24.com/public"
	notification "bet24.com/servers/micros/notification/proto"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 提现请求
func WithdrawOrder(c *gin.Context) {
	obj := db.NewDingpeiWithdrawReq()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "DingpeiPay.WithdrawOrder", err)
		c.String(http.StatusOK, "fail")
		return
	}

	obj.In.IPAddress = strings.Split(c.Request.RemoteAddr, ":")[0]
	obj.DoAction()
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder OrderId is empty fail obj.In=%+v obj.Out=%+v", "DingpeiPay.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	// 下单失败
	if obj.Out.RetCode != 1 {
		log.Debug("%s GenOrder fail obj.In=%+v obj.Out=%+v", "DingpeiPay.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	// 下单成功，待审核
	if obj.Out.GetStatus != 0 {
		log.Debug("%s GenOrder success obj.In=%+v obj.Out=%+v", "DingpeiPay.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "success")
		return
	}

	// 请求时，没有传手机号
	if obj.In.Mobile == "" {
		obj.In.Mobile = obj.Out.Tel
	}

	// 请求withdrawOrder的代码
	req := withdraw_req{
		MerchantNo:        config.Server.DingpeiPay.MerchantNo,
		MerchantOrderNo:   obj.Out.OrderID,
		Amount:            fmt.Sprintf("%d.00", obj.In.Amount),
		BankCode:          obj.In.BankCode,
		BankAccountNumber: obj.In.BankCard,
		BankAccountName:   obj.In.RealName,
		BankBranch:        "",
		CnapsNo:           "",
		Province:          0,
		City:              "",
		NotifyUrl:         config.Server.DingpeiPay.Url_withdraw_notify,
		ClientIp:          obj.In.IPAddress,
		Sign:              "",
	}

	params := url.Values{}
	params.Set("merchantNo", req.MerchantNo)
	params.Set("merchantOrderNo", req.MerchantOrderNo)
	params.Set("amount", req.Amount)
	params.Set("bankCode", req.BankCode)
	params.Set("bankAccountNumber", req.BankAccountNumber)
	params.Set("bankAccountName", req.BankAccountName)
	params.Set("notifyUrl", req.NotifyUrl)
	params.Set("clientIp", req.ClientIp)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.DingpeiPay.MD5Key
	// log.Debug("DingpeiPay.WithdrawOrder.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))
	req.Sign = signature
	params.Set("sign", req.Sign)

	// POST请求
	respBody := public.HttpPostForm(config.Server.DingpeiPay.Url_withdraw_order, params)
	log.Debug("DingpeiPay.WithdrawOrder req ==> %+v  resp ==> %+v", params, respBody)

	var resp withdraw_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("DingpeiPay.WithdrawOrder json unmarshal req ==> %+v resp ==> %+v fail %v", params, respBody, err)
		c.String(http.StatusOK, "fail")
		return
	}

	log.Debug("DingpeiPay.WithdrawOrder resp ==> %+v", resp)

	// 请求响应码 0：成功，1：失败
	if resp.Code != 0 {
		log.Error("DingpeiPay.withdrawRequest post return resp fail ==> %+v", resp)
		c.String(http.StatusOK, "fail")
		return
	}

	go notification.AddNotification(obj.In.UserID, notification.Notification_Gold, "")

	c.String(http.StatusOK, "success")
}

// 提现通知
func WithdrawNotify(c *gin.Context) {
	data, _ := io.ReadAll(c.Request.Body)
	log.Debug("DingpeiPay.WithdrawNotify ==> ctx.Request.body: %v", string(data))

	var req withdrawNotify
	if err := json.Unmarshal(data, &req); err != nil {
		log.Debug("%s query params err %v", "DingpeiPay.WithdrawNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	// log.Debug("DingpeiPay.WithdrawNotify ==> %+v", req)

	// 订单状态：0:待处理 1:代付成功 2:代付失败
	if req.Status == 2 {
		log.Error("DingpeiPay.WithdrawNotify req ==> %+v", req)
		c.String(http.StatusOK, "")
		return
	}

	params := url.Values{}
	params.Set("systemOrderNo", req.SystemOrderNo)
	params.Set("merchantOrderNo", req.MerchantOrderNo)
	params.Set("amount", req.Amount)
	params.Set("balance", req.Balance)
	params.Set("status", strconv.Itoa(req.Status))
	params.Set("errorMsg", req.ErrorMsg)

	// 生成签名
	checkContent := createEncryptStr(params) + "&key=" + config.Server.DingpeiPay.MD5Key
	// log.Debug("DingpeiPay.WithdrawNotify.checkContent ==> %s", checkContent)
	signature := strings.ToUpper(public.GetMd5String(checkContent))

	// 校验签名
	if req.Sign != signature {
		log.Error("DingpeiPay.WithdrawNotify 签名失败 post req ==> %+v  signature ==> %s", req, signature)
		return
	}

	log.Debug("DingpeiPay.WithdrawNotify 签名成功")

	// 订单状态：0:待处理 1:代付成功 2:代付失败
	if req.Status == 2 {
		log.Error("DingpeiPay.WithdrawNotify response fail ==> %+v", req)
		return
	}

	balance, err := strconv.Atoi(strings.ReplaceAll(req.Balance, ".", ""))
	if err != nil {
		log.Error("DingpeiPay.WithdrawNotify balance err %v", err)
		return
	}

	balance = balance / 100

	obj := db.NewDingpeiWithdrawNotify()
	obj.In.OrderID = req.MerchantOrderNo
	obj.In.DfTransactionId = req.SystemOrderNo
	obj.In.Status = req.Status
	obj.In.DfDesc = ""
	obj.In.Balance = balance
	obj.DoAction()

	log.Debug("DingpeiPay.WithdrawNotify obj.In=%+v obj.Out=%+v", obj.In, obj.Out)

	c.String(http.StatusOK, "success")
	return
}
