package dingpei

// 代付下单
type (
	withdraw_req struct {
		MerchantNo        string `json:"merchantNo" form:"merchantNo"`                     // 商户号
		MerchantOrderNo   string `json:"merchantOrderNo" form:"merchantOrderNo"`           // 商户唯一订单号
		Amount            string `json:"amount" form:"amount"`                             // 代付金额
		BankCode          string `json:"bankCode" form:"bankCode"`                         // 银行编码,详见《银行编码》
		BankAccountNumber string `json:"bankAccountNumber" form:"bankAccountNumber"`       // 银行或沃达丰钱包卡号
		BankAccountName   string `json:"bankAccountName" form:"bankAccountName"`           // 银行户名，如果使用沃达丰，此处固定值：Vodafone
		BankBranch        string `json:"bankBranch,omitempty" form:"bankBranch,omitempty"` // 支行信息 , 接入私转私通道可填写任意分行地址
		CnapsNo           string `json:"cnapsNo,omitempty" form:"cnapsNo,omitempty"`       // 银行联行号，银行唯一识别编号，对公账户时该项必填
		Province          int    `json:"province,omitempty" form:"province,omitempty"`     // 省份 , 接入私转私通道可填写任意省份
		City              string `json:"city,omitempty" form:"city,omitempty"`             // 城市 , 接入私转私通道可填写任意城市
		NotifyUrl         string `json:"notifyUrl" form:"notifyUrl"`                       // 系统会将代付结果异步通知到该地址
		ClientIp          string `json:"clientIp" form:"clientIp"`                         // 发起代付的玩家IP，例如：123.123.123.123
		Sign              string `json:"sign" form:"sign"`                                 // 签名，签名方法详见《签名算法》
	}

	withdraw_resp struct {
		Code int             `json:"code" form:"code"` // 0：成功，1：失败
		Msg  string          `json:"msg" form:"msg"`   // 错误信息，code为0时，本值为空
		Data withdrawReqData `json:"data" form:"data"`
	}

	withdrawReqData struct {
		RateAmount      string `json:"rateAmount" form:"rateAmount"`           // 下发手续费
		ToAmount        string `json:"toAmount" form:"toAmount"`               // 到账金额
		ReduceAmount    string `json:"reduceAmount" form:"reduceAmount"`       // 商户余额总扣除金额
		Balance         string `json:"balance" form:"balance"`                 // 商户余额
		MerchantOrderNo string `json:"merchantOrderNo" form:"merchantOrderNo"` // 商户订单号
		SystemOrderNo   string `json:"systemOrderNo" form:"systemOrderNo"`     // 接口生成的系统唯一订单号
		BankAccount     string `json:"bankAccount" form:"bankAccount"`         // 银行账户户名
		CardNo          string `json:"cardNo" form:"cardNo"`                   // 银行卡号
		BankAddress     string `json:"bankAddress" form:"bankAddress"`         // 分行地址
	}
)

// 代付回调通知
type withdrawNotify struct {
	SystemOrderNo   string `json:"systemOrderNo" form:"systemOrderNo"`     // 系统唯一订单号
	MerchantOrderNo string `json:"merchantOrderNo" form:"merchantOrderNo"` // 商户唯一订单号
	Amount          string `json:"amount" form:"amount"`                   // 金额
	Balance         string `json:"balance" form:"balance"`                 // 商户余额
	Status          int    `json:"status" form:"status"`                   // 订单状态：0:待处理 1:代付成功 2:代付失败
	ErrorMsg        string `json:"errorMsg" form:"errorMsg"`               // 代付失败原因
	Sign            string `json:"sign" form:"sign"`                       // 签名 ，签名方法详见《签名算法》
}

// 账户余额查询请求、响应
type (
	queryAccount_req struct {
		MerchantNo string `json:"merchantNo" form:"merchantNo"` // 商户号
		Sign       string `json:"sign" form:"sign"`             // 签名，签名方法详见《签名算法》
	}

	queryAccount_resp struct {
		Code string               `json:"code" form:"code"` // 0：成功，1：失败
		Msg  string               `json:"msg" form:"msg"`   // 错误信息，code为0时，本值为空
		Data queryAccountRespData `json:"data" form:"data"`
	}

	queryAccountRespData struct {
		Amount string `json:"amount" form:"amount"` // 商户当前代付余额
		Frozen string `json:"frozen" form:"frozen"` // 商户当前代付冻结金额
	}
)
