package opay

type ret struct {
	Code string `json:"code"` // 响应 ‘SUCCESS’
	Msg  string `json:"msg"`  // 响应 ‘Success’
}

type (
	pay_req struct {
		Amount      amountData    `json:"amount"`      // 金额（分单位）。
		CallbackUrl string        `json:"callbackUrl"` // 如果您通过API发送了callbackUrl，OPay将向该callbackUrl发送回调通知。如果您没有通过API发送callbackUrl，则需要在商家仪表板上配置webhookurl，OPay将向该webhook url发送回调通知。
		Country     string        `json:"country"`     // 国家代码。在此处查看完整列表
		ProductList []productInfo `json:"productList"`
		Reference   string        `json:"reference"` // 唯一的商户支付订单号。
		ReturnUrl   string        `json:"returnUrl"` // OPay收银员在结账后应将客户返回的URL。
	}

	amountData struct {
		Currency string `json:"currency"` // 货币类型
		Total    int    `json:"total"`    // 金额（分单位）。
	}

	productInfo struct {
		Description string `json:"description"` // 订单产品描述
		Name        string `json:"name"`        // 订单产品名称
		Price       int    `json:"price"`       // 订单产品价格
		ProductId   string `json:"productId"`   // 订单产品ID
		Quantity    int    `json:"quantity"`    // 订单产品数量
	}

	pay_resp struct {
		Code    string  `json:"code"`
		Message string  `json:"message"`
		Data    payData `json:"data"`
	}

	payData struct {
		Amount     amountData `json:"amount"`     // 金额（分单位）。
		CashierUrl string     `json:"cashierUrl"` // 为客户当前交易生成的OPay出纳URl
		OrderNo    string     `json:"orderNo"`    // 唯一的Opay付款订单号。
		Reference  string     `json:"reference"`  // 唯一商户付款订单号
		Status     string     `json:"status"`     // [初始、挂起、成功、失败、关闭]
		Vat        vatData    `json:"vat"`
	}

	vatData struct {
		Total    int    `json:"total"`    // 金额（分单位）。
		Currency string `json:"currency"` // 订单标题
	}
)

type (
	payNotify struct {
		Payload payloadData `json:"payload"` // 有效载荷
		Sha512  string      `json:"sha512"`  // 这本质上是回调有效载荷的HMAC SHA512签名。使用您的密钥签名。
		Type    string      `json:"type"`    // 交易状态。
	}

	payloadData struct {
		Amount           string `json:"amount"`           // 交易金额（EGP）。
		Channel          string `json:"channel"`          // 渠道
		Country          string `json:"country"`          // 交易所属国家
		Currency         string `json:"currency"`         // 交易货币
		DisplayedFailure string `json:"displayedFailure"` // 失败的事务处理原因。
		ErrorCode        string `json:"errorCode"`
		ErrorMsg         string `json:"errorMsg"`
		Fee              string `json:"fee"` // 交易费金额（EGP）
		FeeCurrency      string `json:"feeCurrency"`
		InstrumentType   string `json:"instrumentType"`
		// PayChannel       string `json:"payChannel"` // 支付方式（银行卡余额支付OWealth FlexiFixed BonusPayment）
		Reference     string `json:"reference"` // 合作伙伴交易记录编号。
		Refunded      bool   `json:"refunded"`  // 交易是否为退款类型（真假）
		Status        string `json:"status"`    // 事务状态（成功失败）。
		Timestamp     string `json:"timestamp"` // 交易时间
		Token         string `json:"token"`
		TransactionId string `json:"transactionId"` // Opay交易编号
		Updated_at    string `json:"updated_at"`    // 交易更新时间
	}
)
