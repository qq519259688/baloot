package opay

import (
	"encoding/json"
	"fmt"
	"io"
	"math"
	"net/http"
	"strings"

	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"bet24.com/servers/payment/shop"
	"github.com/gin-gonic/gin"

	coreClient "bet24.com/servers/coreservice/client"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_OPay_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "oPay.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	//// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "opay.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "opay.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "opay.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "opay.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "oPay.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	var productList []productInfo
	productList = append(productList, productInfo{
		ProductId:   obj.In.ProductID,
		Name:        obj.In.ProductID,
		Description: obj.In.ProductID,
		Price:       int(math.Ceil(obj.In.Price)),
		Quantity:    1,
	})

	price := int(obj.In.Price)

	// 请求payOrder的代码
	req := pay_req{
		Reference: obj.Out.OrderID,
		Country:   obj.In.Country,
		Amount: amountData{
			Total:    price,
			Currency: obj.In.Currency,
		},
		CallbackUrl: config.Server.OPay.Url_pay_Notify,
		ReturnUrl:   config.Server.OPay.Url_pay_Return,
		ProductList: productList,
	}

	// 生成签名
	checkContent, err := json.Marshal(req)
	if err != nil {
		log.Error("oPay.PayOrder json marshal fail %v", err)
		return
	}

	// sign := public.HmacSHA512(config.Server.OPay.PublicKey, string(checkContent))
	// log.Debug("oPay.payOrder checkContent=%s sign=%s", string(checkContent), sign)

	headers := make(map[string]string)
	headers["Authorization"] = fmt.Sprintf("Bearer " + config.Server.OPay.PublicKey)
	headers["MerchantId"] = config.Server.OPay.MerchantId

	// POST请求
	respBody := httpPostByJson(config.Server.OPay.Url_pay_order, string(checkContent), headers)
	log.Debug("oPay.payOrder req ==> %+v  resp ==> %+v", req, respBody)

	var resp pay_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("oPay.payOrder json unmarshal req ==> %+v resp ==> %+v fail %v", req, respBody, err)
		return
	}

	log.Debug("oPay.payOrder resp ==> %+v", resp)

	// 返回码，’APPLY_SUCCESS’代表成功
	if resp.Data.Status == "FAIL" || resp.Data.Status == "CLOSE" {
		log.Error("oPay.payOrder post return resp fail ==> %+v", resp)
		return
	}

	// 跳转到支付页面，以便持卡人完成支付过程
	c.Redirect(http.StatusMovedPermanently, resp.Data.CashierUrl)

	// c.String(http.StatusOK, "Success")
	return
}

// 回调通知
func PayNotify(c *gin.Context) {
	bodyData, _ := io.ReadAll(c.Request.Body)
	log.Debug("oPay.PayNotify ==> ctx.Request.body: %v", string(bodyData))

	var resp payNotify
	if err := json.Unmarshal(bodyData, &resp); err != nil {
		log.Debug("%s query params err %v", "oPay.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	// log.Debug("oPay.PayNotify resp ==> %+v", resp)

	// 生成签名
	sign := genNotifySign(resp, config.Server.OPay.SecretKey)

	// 验证签名
	if resp.Sha512 != sign {
		log.Error("oPay.PayNotify 签名失败 sign=%s ==> %+v", sign, resp)
		return
	}

	log.Debug("oPay.PayNotify 签名成功")

	// 返回码，’APPLY_SUCCESS’代表成功
	if resp.Payload.Status != "SUCCESS" {
		log.Error("oPay.PayNotify resp ==> %+v 失败", resp)
		c.JSON(http.StatusOK, ret{Code: "SUCCESS", Msg: "Success"})
		return
	}

	// 数据库操作
	obj := db.NewNotify(db.SP_OPay_NOTIFY)
	obj.In.OrderID = resp.Payload.Reference
	obj.In.TradeID = resp.Payload.TransactionId
	obj.In.Price = 0
	obj.DoAction(nil)

	// 操作成功,给道具
	if obj.Out.RetCode == 1 {
		// 充值
		resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
		log.Debug("%s 充值成功 %+v", "oPay.PayNotify", resp)
	}

	c.JSON(http.StatusOK, ret{Code: "SUCCESS", Msg: "Success"})
	return
}

// 生成签名
func genNotifySign(resp payNotify, privateKey string) string {
	checkContent := "{Amount:\"%s\",Currency:\"%s\",Reference:\"%s\",Refunded:%s,Status:\"%s\",Timestamp:\"%s\",Token:\"%s\",TransactionID:\"%s\"}"

	refunded := "f"
	if resp.Payload.Refunded {
		refunded = "t"
	}

	signContent := fmt.Sprintf(checkContent,
		resp.Payload.Amount,
		resp.Payload.Currency,
		resp.Payload.Reference,
		refunded,
		resp.Payload.Status,
		resp.Payload.Timestamp,
		resp.Payload.Token,
		resp.Payload.TransactionId)

	// 生成签名
	sign := public.Hmac_SHA3_SHA512(privateKey, signContent)

	log.Debug("oPay.PayNotify 程序加密 resp=%+v signContent=%s sign=%s", resp, signContent, sign)

	return sign
}

// 支付完成跳转处理
func PayCallback(c *gin.Context) {
	bodyData, _ := io.ReadAll(c.Request.Body)
	log.Debug("oPay.PayCallback ==> ctx.Request.body: %v", string(bodyData))
	c.String(http.StatusOK, "success")
}
