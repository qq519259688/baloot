package main

import (
	"bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/monitor"
	"bet24.com/servers/payment/config"
	_ "bet24.com/servers/payment/crons"
	"bet24.com/servers/payment/db"
	"bet24.com/servers/payment/liu"
	"bet24.com/servers/payment/router"
	"bet24.com/utils"
	"fmt"
	"time"

	coreservice "bet24.com/servers/coreservice/client"
)

func main() {
	//defer log.PanicHandler("payment")
	config.Run()
	redis.InitPool(config.Server.ChannelUrl, config.Server.ChannelPassword, config.Server.RedisDB)
	coreservice.SetServiceAddr(config.Server.ServiceAddr)

	db.Run()
	// liu.Run()

	go waitInput()

	//监控
	monitor.Run(config.Server.MonitorPort, config.Server.LogPath)

	//定时器
	//go crons.Run()

	log.Release("Server started %v", time.Now())
	utils.SetConsoleTitle(fmt.Sprintf("payment port:%d monitor:%d", config.Server.TlsPort, config.Server.MonitorPort))

	//gin 路由
	router.Run()
}

func waitInput() {
	for {
		var cmd string
		var param1 string
		var param2 string
		fmt.Scanf("%s %s %s", &cmd, &param1, &param2)
		switch cmd {
		case "":
		case "queue":
			liu.Dump(param1, param2)
		case "exit":
			return
		default:
			log.Release("unknown command1")
		}
	}
}
