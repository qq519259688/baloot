package apple

import (
	"encoding/base64"
	"net/http"
	"strconv"
	"strings"

	"bet24.com/log"
	"bet24.com/servers/coreservice/client"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

func Verify(c *gin.Context) {

	//获取参数
	userDBID := strings.Join(c.Request.Form["userDBID"], "")
	userId, err := strconv.Atoi(userDBID)
	if err != nil {
		log.Error("apple.Verify get UserDBID fail %v", err)
		return
	}

	szReceipt := strings.Join(c.Request.Form["szReceipt"], "")
	szReceipt = base64.StdEncoding.EncodeToString([]byte(szReceipt))
	if szReceipt == "" {
		log.Error("apple.Verify get szReceipt is null")
		return
	}

	ipAddress := strings.Split(c.Request.RemoteAddr, ":")[0]

	objLog := db.NewAddLog()
	objLog.In.UserID = userId
	objLog.In.ReceiptBuffer = szReceipt
	objLog.In.IPAddress = ipAddress
	objLog.DoAction(nil)

	isSandBox := 0
	receipt, err, receiptData := VerifyReceipt(szReceipt, false)

	goiapErr, ok := err.(ErrorWithCode)

	if ok && goiapErr.Code() == SandboxReceiptOnProd {
		isSandBox = 1
		receipt, err, receiptData = VerifyReceipt(szReceipt, true)
	}

	if err != nil {
		log.Error("%+v", err)
	}

	log.Debug("Got receipt %+v", receipt)
	// 新版appstore带slot前缀
	productId := strings.Replace(receipt.ProductId, "cof_", "", -1)

	objSuccess := db.NewAddSuccessLog()
	objSuccess.In.UserID = userId
	objSuccess.In.TransactionID = receipt.TransactionId
	objSuccess.In.ProductID = productId
	objSuccess.In.Memo = receiptData
	objSuccess.In.IPAddress = ipAddress
	objSuccess.In.IsSandBox = isSandBox
	objSuccess.DoAction(nil)

	//操作成功,给道具
	if objSuccess.Out.RetCode == 1 {
		//充值
		resp := client.Recharge(userId, productId)
		log.Debug("%s 充值成功 %+v", "apple.Verify", resp)
	}

	c.String(http.StatusOK, strconv.Itoa(objSuccess.Out.RetCode))
	return
}
