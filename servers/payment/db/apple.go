package db

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 添加日志(苹果)
type (
	addLog_in struct {
		UserID        int    //用户ID
		ReceiptBuffer string //接收的收据
		IPAddress     string //IP地址
	}

	addLog struct {
		database.Trans_base
		In addLog_in
	}
)

func NewAddLog() *addLog {
	return &addLog{}
}

func (this *addLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("apple.addLog transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Pay_Verify_AddLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@ReceiptBuffer", database.AdParamInput, database.AdVarChar, 4096, this.In.ReceiptBuffer)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IPAddress)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	CenterDB.ExecSql(sqlstring)
	this.State = true
}

// 添加校验成功日志(苹果)
type (
	addSuccessLog_in struct {
		UserID        int    //用户ID
		TransactionID string //事务ID
		ProductID     string //产品ID
		Memo          string //结果字符串
		IPAddress     string //IP地址
		IsSandBox     int    // 是否沙箱
	}

	addSuccessLog_out struct {
		RetCode int //操作结果
	}

	addSuccessLog struct {
		database.Trans_base
		In  addSuccessLog_in
		Out addSuccessLog_out
	}
)

func NewAddSuccessLog() *addSuccessLog {
	return &addSuccessLog{}
}

func (this *addSuccessLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("apple.addSuccessLog transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_Verify_AddSuccessLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@TransactionID", database.AdParamInput, database.AdVarChar, 64, this.In.TransactionID)
	statement.AddParamter("@ProductID", database.AdParamInput, database.AdVarChar, 64, this.In.ProductID)
	statement.AddParamter("@Memo", database.AdParamInput, database.AdVarChar, 1024, this.In.Memo)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IPAddress)
	statement.AddParamter("@IsSandBox", database.AdParamInput, database.AdInteger, 4, this.In.IsSandBox)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}

	this.State = true
	this.Out.RetCode = int(retRows[0][0].(int64))
}

// 添加校验错误日志(苹果)
type (
	addErrorLog_in struct {
		UserID    int    //用户ID
		ErrorMsg  string //错误消息
		IPAddress string //IP地址
	}

	addErrorLog struct {
		database.Trans_base
		In addErrorLog_in
	}
)

func NewAddErrorLog() *addErrorLog {
	return &addErrorLog{}
}

func (this *addErrorLog) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Error("apple.addErrorLog transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}

		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_Verify_AddErrorLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@ErrorMsg", database.AdParamInput, database.AdVarChar, 1024, this.In.ErrorMsg)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IPAddress)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	CenterDB.ExecSql(sqlstring)
	this.State = true
}
