package db

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 银行信息
type BankInfo struct {
	RealName string // 真实姓名
	BankCard string // 银行卡号
	BankName string // 银行名称
	Mobile   string // 手机号
	EMail    string // email
	Address  string // 地址
}

// 银行信息列表
type (
	bankInfoList_in struct {
		UserID int // 用户ID
	}

	bankInfoList_out struct {
		List []BankInfo
	}

	bankInfoList struct {
		database.Trans_base
		In  bankInfoList_in
		Out bankInfoList_out
	}
)

func newBankInfoList() *bankInfoList {
	return &bankInfoList{}
}

func (this *bankInfoList) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_BankInfo_GetList")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	rowLen := len(retRows)
	if rowLen <= 0 {
		return
	}
	this.Out.List = make([]BankInfo, rowLen)
	for i := 0; i < rowLen; i++ {
		ret := retRows[i]
		out := &this.Out.List[i]

		out.RealName = ret[0].(string)
		out.BankCard = ret[1].(string)
		out.BankName = ret[2].(string)
		out.Mobile = ret[3].(string)
		out.EMail = ret[4].(string)
		out.Address = ret[5].(string)
	}
}

// 修改银行信息
type (
	bankInfoUp_in struct {
		UserID int
		BankInfo
	}

	bankInfoUp struct {
		database.Trans_base
		In bankInfoUp_in
	}
)

func NewBankInfoUp() *bankInfoUp {
	return &bankInfoUp{}
}

func (this *bankInfoUp) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_BankInfo_Update")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@RealName", database.AdParamInput, database.AdVarChar, 128, this.In.RealName)
	statement.AddParamter("@BankCard", database.AdParamInput, database.AdVarChar, 32, this.In.BankCard)
	statement.AddParamter("@BankName", database.AdParamInput, database.AdVarChar, 128, this.In.BankName)
	statement.AddParamter("@Mobile", database.AdParamInput, database.AdVarChar, 32, this.In.Mobile)
	statement.AddParamter("@EMail", database.AdParamInput, database.AdVarChar, 64, this.In.EMail)
	statement.AddParamter("@Address", database.AdParamInput, database.AdVarChar, 256, this.In.Address)
	sqlstring := statement.GenSql()
	// log.Debug(sqlstring)
	CenterDB.ExecSql(sqlstring)
}
