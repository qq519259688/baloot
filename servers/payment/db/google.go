package db

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

//添加日志(google)
type (
	googleAddLog_in struct {
		UserID    int    //用户ID
		ProductID string //产品ID
		Price     int    //价格
		PartnerID int    //渠道ID
		Token     string //token
		IpAddress string //ip地址
	}

	googleAddLog struct {
		database.Trans_base
		In googleAddLog_in
	}
)

func NewGoogleAddLog() *googleAddLog {
	return &googleAddLog{}
}

func (this *googleAddLog) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_Google_AddLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@ProductID", database.AdParamInput, database.AdVarChar, 32, this.In.ProductID)
	statement.AddParamter("@Price", database.AdParamInput, database.AdInteger, 4, this.In.Price)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@Token", database.AdParamInput, database.AdVarChar, 1024, this.In.Token)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	CenterDB.ExecSql(sqlstring)
	return
}

//添加成功日志(google)
type (
	googleAddSuccessLog_in struct {
		UserID      int    //用户ID
		ProductID   string //产品ID
		Price       int    //价格
		PartnerID   int    //渠道ID
		Token       string //token
		ResponseIAP string //响应信息
		IpAddress   string //IP地址
	}

	googleAddSuccessLog_out struct {
		RetCode int //操作结果
	}

	googleAddSuccessLog struct {
		database.Trans_base
		In  googleAddSuccessLog_in
		Out googleAddSuccessLog_out
	}
)

func NewGoogleAddSuccessLog() *googleAddSuccessLog {
	return &googleAddSuccessLog{}
}

func (this *googleAddSuccessLog) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_Google_AddSuccessLog")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@ProductID", database.AdParamInput, database.AdVarChar, 32, this.In.ProductID)
	statement.AddParamter("@Price", database.AdParamInput, database.AdInteger, 4, this.In.Price)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@Token", database.AdParamInput, database.AdVarChar, 1024, this.In.Token)
	statement.AddParamter("@ResponseIAP", database.AdParamInput, database.AdVarChar, 1024, this.In.ResponseIAP)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	this.Out.RetCode = int(retRows[0][0].(int64))
}
