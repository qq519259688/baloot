package db

import (
	dbengine "bet24.com/servers/micros/dbengine/proto"
)

var CenterDB *dboperator

func Run() {
	if CenterDB == nil {
		CenterDB = new(dboperator)
	}
}

type dboperator struct {
}

func (d *dboperator) ExecSql(sql string) [][]interface{} {
	return dbengine.ExecuteRs(sql)
}

func (d *dboperator) ExecSqlJson(sql string) string {
	return dbengine.Execute(sql)
}
