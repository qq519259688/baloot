package db

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// crush支付代付(提现请求)
type (
	crushWithdrawReq_in struct {
		UserID    int    // 用户ID
		Amount    int    // 提现金额
		IPAddress string // IP地址
		RealName  string // 真实姓名
		BankCard  string // 银行卡
		BankCode  string // 银行编码
		BankName  string // 银行名称
		Mobile    string // 电话
		Email     string // email
		Address   string // 地址
	}

	crushWithdrawReq_out struct {
		RetCode    int    // 操作结果 1=下单成功  11=参数无效  12=提现金额太小  13=提现次数超过限制  14=提现金额超过限制  15=扣减金币失败
		OrderID    string // 订单号
		GetStatus  int    // 状态 (0=下单(无审核)  1=待审核  2=已审核  3=提现成功(审核)  4=提成成功(无审核)  11=拒绝)
		RealAmount int    // 实际提现的金额(扣除费率)
		Tel        string // 手机号
	}

	crushWithdrawReq struct {
		database.Trans_base
		In  crushWithdrawReq_in
		Out crushWithdrawReq_out
	}
)

func NewCrushWithdrawReq() *crushWithdrawReq {
	return &crushWithdrawReq{}
}

func (this *crushWithdrawReq) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_CrushWithdraw_Req")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdInteger, 4, this.In.Amount)
	statement.AddParamter("@RealName", database.AdParamInput, database.AdVarChar, 128, this.In.RealName)
	statement.AddParamter("@BankCard", database.AdParamInput, database.AdVarChar, 32, this.In.BankCard)
	statement.AddParamter("@BankName", database.AdParamInput, database.AdVarChar, 128, this.In.BankCode)
	statement.AddParamter("@Mobile", database.AdParamInput, database.AdVarChar, 32, this.In.Mobile)
	statement.AddParamter("@EMail", database.AdParamInput, database.AdVarChar, 64, this.In.Email)
	statement.AddParamter("@Address", database.AdParamInput, database.AdVarChar, 256, this.In.Address)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IPAddress)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	ret := retRows[0]
	this.Out.RetCode = int(ret[0].(int64))
	this.Out.OrderID = ret[1].(string)
	this.Out.GetStatus = int(ret[2].(int64))
	this.Out.RealAmount = int(ret[3].(int64))
	this.Out.Tel = ret[4].(string)
}

// crush支付代付(提现通知)
type (
	crushWithdrawNotify_in struct {
		OrderID         string // 订单号
		DfTransactionId string // 平台代付单号，32字符以内
		Status          int    // 0=Failure 1=Success  2=Pending(Success)  3=BOUNCEBACK退票
		DfDesc          string // 代付状态描述
		Balance         int    // 余额
	}

	crushWithdrawNotify_out struct {
		RetCode int // 操作结果  1=操作成功  2=操作失败,返还金币  11=操作失败
		UserID  int // 用户ID
	}

	crushWithdrawNotify struct {
		database.Trans_base
		In  crushWithdrawNotify_in
		Out crushWithdrawNotify_out
	}
)

func NewCrushWithdrawNotify() *crushWithdrawNotify {
	return &crushWithdrawNotify{}
}

func (this *crushWithdrawNotify) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_CrushWithdraw_Notify")
	statement.AddParamter("@OrderID", database.AdParamInput, database.AdVarChar, 32, this.In.OrderID)
	statement.AddParamter("@DfTransactionId", database.AdParamInput, database.AdVarChar, 32, this.In.DfTransactionId)
	statement.AddParamter("@Status", database.AdParamInput, database.AdInteger, 4, this.In.Status)
	statement.AddParamter("@DfDesc", database.AdParamInput, database.AdVarChar, 256, this.In.DfDesc)
	statement.AddParamter("@Balance", database.AdParamInput, database.AdInteger, 4, this.In.Balance)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if (len(retRows)) <= 0 {
		return
	}

	ret := retRows[0]

	this.Out.RetCode = int(ret[0].(int64))
	this.Out.UserID = int(ret[1].(int64))
}
