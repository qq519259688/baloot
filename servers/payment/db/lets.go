package db

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// lets支付代付(提现请求)
type (
	letsWithdrawReq_in struct {
		UserID    int    // 用户ID
		Amount    int    // 提现金额
		IPAddress string // IP地址
		RealName  string // 真实姓名
		BankCard  string // 银行卡
		BankName  string // 银行名称
		Mobile    string // 电话
		Email     string // email
		Address   string // 地址
	}

	letsWithdrawReq_out struct {
		RetCode    int    // 操作结果 1=下单成功  11=参数无效  12=扣减金币失败
		OrderID    string // 订单号
		RealAmount int    // 实际提现的金额(扣除费率)
		Tel        string // 手机号
	}

	letsWithdrawReq struct {
		database.Trans_base
		In  letsWithdrawReq_in
		Out letsWithdrawReq_out
	}
)

func NewLetsWithdrawReq() *letsWithdrawReq {
	return &letsWithdrawReq{}
}

func (this *letsWithdrawReq) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_LetsWithdraw_Req")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdInteger, 4, this.In.Amount)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IPAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	statement.AddParamter("@OrderID", database.AdParamOutput, database.AdVarChar, 32, this.Out.OrderID)
	statement.AddParamter("@RealAmount", database.AdParamOutput, database.AdInteger, 4, this.Out.RealAmount)
	statement.AddParamter("@Tel", database.AdParamOutput, database.AdVarChar, 32, this.Out.Tel)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	ret := retRows[0]
	this.Out.RetCode = int(ret[0].(int64))
	this.Out.OrderID = ret[1].(string)
	this.Out.RealAmount = int(ret[2].(int64))
	this.Out.Tel = ret[3].(string)
}

// lets支付代付(提现通知)
type (
	letsWithdrawNotify_in struct {
		OrderID         string // 订单号
		BankCard        string // 银行卡号
		DfTransactionId string // 平台代付单号，32字符以内
		DfState         string // 代付状态 00-成功 01-处理中 02-处理失败
		DfDesc          string // 代付状态描述
		Balance         int    // 余额
	}

	letsWithdrawNotify_out struct {
		RetCode int // 操作结果  1=操作成功  2=操作失败,返还金币  11=操作失败
		UserID  int // 用户ID
	}

	letsWithdrawNotify struct {
		database.Trans_base
		In  letsWithdrawNotify_in
		Out letsWithdrawNotify_out
	}
)

func NewLetsWithdrawNotify() *letsWithdrawNotify {
	return &letsWithdrawNotify{}
}

func (this *letsWithdrawNotify) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_LetsWithdraw_Notify")
	statement.AddParamter("@OrderID", database.AdParamInput, database.AdVarChar, 32, this.In.OrderID)
	statement.AddParamter("@BankCard", database.AdParamInput, database.AdVarChar, 32, this.In.BankCard)
	statement.AddParamter("@DfTransactionId", database.AdParamInput, database.AdVarChar, 32, this.In.DfTransactionId)
	statement.AddParamter("@DfState", database.AdParamInput, database.AdVarChar, 16, this.In.DfState)
	statement.AddParamter("@DfDesc", database.AdParamInput, database.AdVarChar, 256, this.In.DfDesc)
	statement.AddParamter("@Balance", database.AdParamInput, database.AdInteger, 4, this.In.Balance)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if (len(retRows)) <= 0 {
		return
	}

	ret := retRows[0]

	this.Out.RetCode = int(ret[0].(int64))
	this.Out.UserID = int(ret[1].(int64))
}
