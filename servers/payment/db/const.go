package db

const (
	SP_CRO_ORDER  = "Pay_CroPay_GenOrder" // Cropay 订单生成
	SP_CRO_NOTIFY = "Pay_CroPay_OrderReq" // Cropay 回调通知

	SP_ZShui_ORDER  = "Pay_ZShui_GenOrder"    // 聚合支付 订单生成
	SP_ZShui_NOTIFY = "Pay_ZShuiPay_OrderReq" // 聚合支付 回调通知

	SP_Kaya_ORDER  = "Pay_Kaya_GenOrder" // Kaya支付 订单生成
	SP_Kaya_NOTIFY = "Pay_Kaya_OrderReq" // Kaya支付 回调通知

	SP_Lets_ORDER  = "Pay_Lets_GenOrder" // Lets支付 订单生成
	SP_Lets_NOTIFY = "Pay_Lets_OrderReq" // Lets支付 回调通知

	SP_Liu_ORDER  = "Pay_Liu_GenOrder" // Liu支付 订单生成
	SP_Liu_NOTIFY = "Pay_Liu_OrderReq" // Liu支付 回调通知

	SP_Flash_ORDER  = "Pay_Flash_GenOrder" // Flash 支付 订单生成
	SP_Flash_NOTIFY = "Pay_Flash_OrderReq" // Flash 支付 回调通知

	SP_PayerMax_ORDER  = "Pay_PayerMax_GenOrder" // PayerMax支付 订单生成
	SP_PayerMax_NOTIFY = "Pay_PayerMax_OrderReq" // PayerMax支付 回调通知

	SP_Dingpei_ORDER  = "Pay_Dingpei_GenOrder" // Dingpei支付 订单生成
	SP_Dingpei_NOTIFY = "Pay_Dingpei_OrderReq" // Dingpei支付 回调通知

	SP_Crush_ORDER  = "Pay_Crush_GenOrder" // Crush支付 订单生成
	SP_Crush_NOTIFY = "Pay_Crush_OrderReq" // Crush支付 回调通知

	SP_OPay_ORDER  = "Pay_OPay_GenOrder" // OPay支付 订单生成
	SP_OPay_NOTIFY = "Pay_OPay_OrderReq" // OPay支付 回调通知

	SP_paymob_ORDER  = "Pay_paymob_GenOrder" // paymob支付 订单生成
	SP_paymob_NOTIFY = "Pay_paymob_OrderReq" // paymob支付 回调通知

	SP_fawry_ORDER  = "Pay_fawry_GenOrder" // fawry支付 订单生成
	SP_fawry_NOTIFY = "Pay_fawry_OrderReq" // fawry支付 回调通知

	SP_coda_ORDER  = "Pay_coda_GenOrder" // coda支付 订单生成
	SP_coda_NOTIFY = "Pay_coda_OrderReq" // coda支付 回调通知

	SP_LaPay_ORDER  = "Pay_LaPay_GenOrder" // 订单生成
	SP_LaPay_NOTIFY = "Pay_LaPay_OrderReq" // 回调通知

	SP_HuaweiPay_ORDER  = "Pay_HuaweiPay_GenOrder" // 华为订单生成
	SP_HuaweiPay_NOTIFY = "Pay_HuaweiPay_OrderReq" // 华为回调通知
)
