package db

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 下单
type (
	order_in struct {
		UserID    int     // 用户ID
		Price     float64 // 价格
		ProductID string  // 商品ID
		PartnerID int     // 渠道ID
		IpAddress string  // IP地址
		Name      string  // 姓名
		Tel       string  // 电话
		Email     string  // 邮箱
		Currency  string  // 币种
		Country   string  // 国家
		PayMethod string  // 支付方式
		TargetOrg string  // 目标机构
		CardNum   string  // 银行卡号
		PayModel  string  // 支付模式
	}

	order_out struct {
		OrderID string //订单号
	}

	order struct {
		database.Trans_base
		procName string //存储过程
		In       order_in
		Out      order_out
	}
)

func NewOrder(procName string) *order {
	return &order{
		procName: procName,
	}
}

func (this *order) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName(this.procName)
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@Price", database.AdParamInput, database.AdFloat, 20, this.In.Price)
	statement.AddParamter("@ProductID", database.AdParamInput, database.AdVarChar, 32, this.In.ProductID)
	statement.AddParamter("@PartnerID", database.AdParamInput, database.AdInteger, 4, this.In.PartnerID)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IpAddress)
	statement.AddParamter("@OrderID", database.AdParamOutput, database.AdVarChar, 32, this.Out.OrderID)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}
	this.State = true
	this.Out.OrderID = retRows[0][0].(string)
}

// 回调
type (
	notify_in struct {
		OrderID string  //订单号
		TradeID string  //业务流水号
		Price   float64 //价格
	}

	notify_out struct {
		RetCode   int    //操作结果
		UserID    int    //用户ID
		ProductID string //产品ID
	}

	notify struct {
		database.Trans_base
		ProcName string //存储过程
		In       notify_in
		Out      notify_out
	}
)

func NewNotify(procName string) *notify {
	return &notify{
		ProcName: procName,
	}
}

func (this *notify) DoAction(ch chan<- interface{}) {
	defer func() {
		if err := recover(); err != nil {
			log.Release("transaction recover err %v", err)
			log.Release("%s", debug.Stack())
		}
		if ch != nil {
			ch <- this
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName(this.ProcName)
	statement.AddParamter("@OrderID", database.AdParamInput, database.AdVarChar, 32, this.In.OrderID)
	statement.AddParamter("@TradeID", database.AdParamInput, database.AdVarChar, 64, this.In.TradeID)
	statement.AddParamter("@Price", database.AdParamInput, database.AdFloat, 20, this.In.Price)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	statement.AddParamter("@UserID", database.AdParamOutput, database.AdInteger, 4, this.Out.UserID)
	statement.AddParamter("@ProductID", database.AdParamOutput, database.AdVarChar, 32, this.Out.ProductID)
	sqlstring := statement.GenSql()
	//log.Debug(sqlstring)
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		this.State = false
		return
	}
	this.State = true
	this.Out.RetCode = int(retRows[0][0].(int64))
	this.Out.UserID = int(retRows[0][1].(int64))
	this.Out.ProductID = (retRows[0][2].(string))
}
