package db

import (
	"runtime/debug"

	"bet24.com/database"

	"bet24.com/log"
)

// 删除facebook账号
func DelFacebook(facebookId string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_DelFacebook")
	statement.AddParamter("@FacebookID", database.AdParamInput, database.AdVarChar, 64, facebookId)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}

	return int(retRows[0][0].(int64))
}

// 获取facebook账号信息
func GetFacebook(facebookId string) int {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("WS_AllUser_GetFacebookInfo")
	statement.AddParamter("@FacebookID", database.AdParamInput, database.AdVarChar, 64, facebookId)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return 0
	}

	return int(retRows[0][0].(int64))
}
