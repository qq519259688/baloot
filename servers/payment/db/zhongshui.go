package db

import (
	"runtime/debug"

	"bet24.com/database"
	"bet24.com/log"
)

// 聚合支付--通知失败
type (
	zshuiNotifyFail_in struct {
		OrderID string // 订单号
		TradeID string // 业务流水号
		PayInfo string // 支付结果信息
	}

	zshuiNOtifyFail struct {
		database.Trans_base
		In zshuiNotifyFail_in
	}
)

func NewZShuiNotifyFail() *zshuiNOtifyFail {
	return &zshuiNOtifyFail{}
}

func (this *zshuiNOtifyFail) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(false)
	statement.SetProcName("Pay_ZShuiPay_NotifyFail")
	statement.AddParamter("@OrderID", database.AdParamInput, database.AdVarChar, 32, this.In.OrderID)
	statement.AddParamter("@TradeID", database.AdParamInput, database.AdVarChar, 64, this.In.TradeID)
	statement.AddParamter("@PayInfo", database.AdParamInput, database.AdVarChar, 64, this.In.PayInfo)
	sqlstring := statement.GenSql()
	log.Debug(sqlstring)
	CenterDB.ExecSql(sqlstring)
}

// 聚合支付代付(提现请求)
type (
	zshuiWithdrawReq_in struct {
		UserID    int    // 用户ID
		Amount    int    // 提现金额
		IPAddress string // IP地址
		RealName  string // 真实姓名
		BankCard  string // 银行卡
		BankName  string // 银行名称
		Mobile    string // 电话
		Email     string // email
		Address   string // 地址
	}

	zshuiWithdrawReq_out struct {
		RetCode    int    // 操作结果 1=下单成功  11=参数无效  12=扣减金币失败
		OrderID    string // 订单号
		RealAmount int    // 实际提现的金额(扣除费率)
		Tel        string // 手机号
	}

	zshuiWithdrawReq struct {
		database.Trans_base
		In  zshuiWithdrawReq_in
		Out zshuiWithdrawReq_out
	}
)

func NewZShuiWithdrawReq() *zshuiWithdrawReq {
	return &zshuiWithdrawReq{}
}

func (this *zshuiWithdrawReq) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_ZShuiWithdraw_Req")
	statement.AddParamter("@UserID", database.AdParamInput, database.AdInteger, 4, this.In.UserID)
	statement.AddParamter("@Amount", database.AdParamInput, database.AdInteger, 4, this.In.Amount)
	statement.AddParamter("@IPAddress", database.AdParamInput, database.AdVarChar, 16, this.In.IPAddress)
	statement.AddParamter("@RetCode", database.AdParamOutput, database.AdInteger, 4, this.Out.RetCode)
	statement.AddParamter("@OrderID", database.AdParamOutput, database.AdVarChar, 32, this.Out.OrderID)
	statement.AddParamter("@RealAmount", database.AdParamOutput, database.AdInteger, 4, this.Out.RealAmount)
	statement.AddParamter("@Tel", database.AdParamOutput, database.AdVarChar, 32, this.Out.Tel)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if len(retRows) <= 0 {
		return
	}

	ret := retRows[0]
	this.Out.RetCode = int(ret[0].(int64))
	this.Out.OrderID = ret[1].(string)
	this.Out.RealAmount = int(ret[2].(int64))
	this.Out.Tel = ret[3].(string)
}

// 聚合支付代付(提现通知)
type (
	zshuiWithdrawNotify_in struct {
		OrderID         string // 订单号
		BankCard        string // 银行卡号
		DfTransactionId string // 平台代付单号，32字符以内
		DfState         string // 代付状态 00-成功 01-处理中 02-处理失败
		DfDesc          string // 代付状态描述
		Balance         int    // 余额
	}

	zshuiWithdrawNotify_out struct {
		RetCode int // 操作结果  1=操作成功  2=操作失败,返还金币  11=操作失败
		UserID  int // 用户ID
	}

	zshuiWithdrawNotify struct {
		database.Trans_base
		In  zshuiWithdrawNotify_in
		Out zshuiWithdrawNotify_out
	}
)

func NewZShuiWithdrawNotify() *zshuiWithdrawNotify {
	return &zshuiWithdrawNotify{}
}

func (this *zshuiWithdrawNotify) DoAction() {
	defer func() {
		if err := recover(); err != nil {
			log.Error("transaction recover err %v", err)
			log.Error("%s", debug.Stack())
		}
	}()

	statement := database.NewStatement()
	statement.SetNeedReturnValue(false)
	statement.SetOpenRecordSet(true)
	statement.SetProcName("Pay_ZShuiWithdraw_Notify")
	statement.AddParamter("@OrderID", database.AdParamInput, database.AdVarChar, 32, this.In.OrderID)
	statement.AddParamter("@BankCard", database.AdParamInput, database.AdVarChar, 32, this.In.BankCard)
	statement.AddParamter("@DfTransactionId", database.AdParamInput, database.AdVarChar, 32, this.In.DfTransactionId)
	statement.AddParamter("@DfState", database.AdParamInput, database.AdVarChar, 16, this.In.DfState)
	statement.AddParamter("@DfDesc", database.AdParamInput, database.AdVarChar, 256, this.In.DfDesc)
	statement.AddParamter("@Balance", database.AdParamInput, database.AdInteger, 4, this.In.Balance)
	sqlstring := statement.GenSql()
	retRows := CenterDB.ExecSql(sqlstring)
	if (len(retRows)) <= 0 {
		return
	}

	ret := retRows[0]

	this.Out.RetCode = int(ret[0].(int64))
	this.Out.UserID = int(ret[1].(int64))
}
