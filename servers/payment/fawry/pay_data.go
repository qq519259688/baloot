package fawry

type ret struct {
	Code string `json:"code"` // 响应 ‘SUCCESS’
	Msg  string `json:"msg"`  // 响应 ‘Success’
}

type pay_req struct {
	MerchantCode           string        `json:"merchantCode" form:"merchantCode"`
	MerchantRefNum         string        `json:"merchantRefNum" form:"merchantRefNum"`
	PaymentMethod          string        `json:"paymentMethod" form:"paymentMethod"`
	CardNumber             string        `json:"cardNumber,omitempty" form:"cardNumber,omitempty"`
	CardExpiryYear         string        `json:"cardExpiryYear,omitempty" form:"cardExpiryYear,omitempty"`
	CardExpiryMonth        string        `json:"cardExpiryMonth,omitempty" form:"cardExpiryMonth,omitempty"`
	Cvv                    string        `json:"cvv,omitempty" form:"cvv,omitempty"`
	CustomerMobile         string        `json:"customerMobile" form:"customerMobile"`
	CustomerEmail          string        `json:"customerEmail" form:"customerEmail"`
	Amount                 string        `json:"amount" form:"amount"`
	CurrencyCode           string        `json:"currencyCode" form:"currencyCode"`
	Description            string        `json:"description" form:"description"`
	Language               string        `json:"language" form:"language"`
	ChargeItems            []chargeItems `json:"chargeItems" form:"chargeItems"`
	Enable3DS              bool          `json:"enable3DS,omitempty" form:"enable3DS,omitempty"`
	AuthCaptureModePayment bool          `json:"authCaptureModePayment,omitempty" form:"authCaptureModePayment,omitempty"`
	ReturnUrl              string        `json:"returnUrl,omitempty" form:"returnUrl,omitempty"`
	Signature              string        `json:"signature" form:"signature"`
}

type pay_resp struct {
	Type              string `json:"type" form:"type"`
	ReferenceNumber   string `json:"referenceNumber" form:"referenceNumber"`
	MerchantRefNumber string `json:"merchantRefNumber" form:"merchantRefNumber"`
	WalletQr          string `json:"walletQr" form:"walletQr"`
	StatusCode        int    `json:"statusCode" form:"statusCode"`
	StatusDescription string `json:"statusDescription" form:"statusDescription"`
}

type chargeItems struct {
	ItemId      string  `json:"itemId" form:"itemId"`
	Description string  `json:"description" form:"description"`
	Price       string  `json:"price" form:"price"`
	Quantity    float64 `json:"quantity" form:"quantity"`
}

type payNotify struct {
	RequestId             string      `json:"requestId" form:"requestId"`
	FawryRefNumber        string      `json:"fawryRefNumber" form:"fawryRefNumber"`
	MerchantRefNumber     string      `json:"merchantRefNumber" form:"merchantRefNumber"`
	CustomerName          string      `json:"customerName" form:"customerName"`
	CustomerMobile        string      `json:"customerMobile" form:"customerMobile"`
	CustomerMail          string      `json:"customerMail" form:"customerMail"`
	CustomerMerchantId    string      `json:"customerMerchantId" form:"customerMerchantId"`
	PaymentAmount         float64     `json:"paymentAmount" form:"paymentAmount"`
	OrderAmount           float64     `json:"orderAmount" form:"orderAmount"`
	FawryFees             float64     `json:"fawryFees" form:"fawryFees"`
	ShippingFees          float64     `json:"shippingFees" form:"shippingFees"`
	OrderStatus           string      `json:"orderStatus" form:"orderStatus"`
	PaymentMethod         string      `json:"paymentMethod" form:"paymentMethod"`
	PaymentTime           int         `json:"paymentTime" form:"paymentTime"`
	AuthNumber            int         `json:"authNumber" form:"authNumber"`
	PaymentRefrenceNumber string      `json:"paymentRefrenceNumber" form:"paymentRefrenceNumber"`
	OrderExpiryDate       int         `json:"orderExpiryDate" form:"orderExpiryDate"`
	OrderItems            []orderItem `json:"orderItems" form:"orderItems"`
	FailureErrorCode      string      `json:"failureErrorCode" form:"failureErrorCode"`
	FailureReason         string      `json:"failureReason" form:"failureReason"`
	MessageSignature      string      `json:"messageSignature" form:"messageSignature"`
}

type orderItem struct {
	ItemCode string  `json:"itemCode" form:"itemCode"`
	Price    float64 `json:"price" form:"price"`
	Quantity int     `json:"quantity" form:"quantity"`
}
