package fawry

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"bet24.com/log"
	"bet24.com/public"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"bet24.com/servers/payment/shop"
	"github.com/gin-gonic/gin"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_fawry_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "fawry.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	//// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "fawry.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "fawry.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "fawry.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "fawry.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "fawry.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	var items []chargeItems
	items = append(items, chargeItems{
		ItemId:      obj.In.ProductID,
		Description: obj.In.ProductID,
		Price:       fmt.Sprintf("%.2f", obj.In.Price),
		Quantity:    1,
	})

	// 请求payOrder的代码
	req := pay_req{
		MerchantCode:   config.Server.Fawry.MerchantCode,
		MerchantRefNum: obj.Out.OrderID,
		PaymentMethod:  "MWALLET",
		CustomerMobile: obj.In.Tel,
		CustomerEmail:  obj.In.Email,
		Amount:         fmt.Sprintf("%.2f", obj.In.Price),
		CurrencyCode:   obj.In.Currency,
		Description:    obj.In.ProductID,
		Language:       "en-gb",
		ChargeItems:    items,
		// CardNumber:      "4543474002259998",
		// CardExpiryYear:  "25",
		// CardExpiryMonth: "05",
		// Cvv:             "123",
		// ReturnUrl:       "https://meweb.metagame.cyou:8088/fawry/payNotify",
		// Enable3DS:       true,
	}

	// The SHA-256 digested for the following concatenated string
	var data string
	if req.PaymentMethod == "PAYATFAWRY" || req.PaymentMethod == "MWALLET" {
		data = fmt.Sprintf("%s%s%s%s%s", req.MerchantCode, req.MerchantRefNum, req.PaymentMethod, req.Amount, config.Server.Fawry.SecureKey)
	} else if req.PaymentMethod == "CARD" {
		data = fmt.Sprintf("%s%s%s%s%s%s%s%s%s%s",
			req.MerchantCode, req.MerchantRefNum, req.PaymentMethod, req.Amount,
			req.CardNumber, req.CardExpiryYear, req.CardExpiryMonth, req.Cvv, req.ReturnUrl, config.Server.Fawry.SecureKey)
	}

	// log.Debug("fawry.payOrder data ==> %v", data)
	req.Signature = public.SHA256(data)

	// 生成签名
	checkContent, err := json.Marshal(req)
	if err != nil {
		log.Error("fawry.PayOrder json marshal fail %v", err)
		return
	}

	// POST请求
	respBody := public.HttpPostByJson(config.Server.Fawry.Url_pay_order, string(checkContent))
	log.Debug("fawry.payOrder req ==> %+v  resp ==> %+v", string(checkContent), respBody)

	var resp pay_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("fawry.PayOrder respBody json unmarshal fail %v", err)
		return
	}

	if resp.StatusCode != 200 {
		log.Error("fawry.payOrder 失败 ==> %+v", resp)
		return
	}

	c.HTML(http.StatusOK, "fawryQr.html", gin.H{
		"title":    "E-Wallet QRCode",
		"walletQr": strings.ReplaceAll(resp.WalletQr, "data:image/PNG;base64,", ""),
		// "walletQr": resp.WalletQr,
	})

	// 显示二维码
	// html := fmt.Sprintf("<img src=\"%s\" alt=\"QR_Code\"/>", resp.WalletQr)
	// c.Header("content-Type", "text/html; charset=utf-8")
	// c.String(http.StatusOK, html)

	// 跳转到支付页面，以便持卡人完成支付过程
	// c.Redirect(http.StatusMovedPermanently, resp.Redirect_url)
	return
}

// 回调通知
func PayNotify(c *gin.Context) {
	bodyData, _ := io.ReadAll(c.Request.Body)
	log.Debug("fawry.PayNotify ==> ctx.Request.body: %v", string(bodyData))

	var resp payNotify
	if err := json.Unmarshal(bodyData, &resp); err != nil {
		log.Debug("%s query params err %v", "fawry.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("fawry.PayNotify resp ==> %+v", resp)

	data := fmt.Sprintf("%s%s%.2f%.2f%s%s%s%s",
		resp.FawryRefNumber, resp.MerchantRefNumber, resp.PaymentAmount, resp.OrderAmount,
		resp.OrderStatus, resp.PaymentMethod, resp.PaymentRefrenceNumber,
		config.Server.Fawry.SecureKey)

	hmac := public.SHA256(data)

	// 验证签名
	if hmac != resp.MessageSignature {
		log.Error("fawry.PayNotify 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("fawry.PayNotify 签名成功")

	// New, PAID, CANCELED
	// DELIVERED, REFUNDED, EXPIRED
	// PARTIAL_REFUNDED, FAILED
	if resp.OrderStatus != "PAID" {
		log.Debug("fawry.PayNotify resp ==> %+v 接收响应", resp)
		c.JSON(http.StatusOK, ret{
			Code: "SUCCESS",
			Msg:  "Success",
		})
		return
	}

	// 数据库操作
	obj := db.NewNotify(db.SP_fawry_NOTIFY)
	obj.In.OrderID = resp.MerchantRefNumber
	obj.In.TradeID = resp.FawryRefNumber
	obj.DoAction(nil)

	// 操作成功,给道具
	if obj.Out.RetCode == 1 {
		// 充值
		resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
		log.Debug("%s 充值成功 %+v", "fawry.PayNotify", resp)
	}

	c.JSON(http.StatusOK, ret{
		Code: "SUCCESS",
		Msg:  "Success",
	})
	return
}
