package facebook

type RespData struct {
	Url               string `json:"url"`
	Confirmation_code string `json:"confirmation_code"`
}
