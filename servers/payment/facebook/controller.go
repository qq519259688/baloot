package facebook

import (
	"fmt"
	"net/http"
	"strconv"

	"bet24.com/public"

	"bet24.com/servers/payment/db"

	"bet24.com/log"
	"bet24.com/servers/payment/config"
	"github.com/gin-gonic/gin"
	"github.com/huandu/facebook"
)

// 获取密钥
func getSecret(appId string) string {
	for _, v := range config.Server.Facebooks {
		if v.APP_ID == appId {
			return v.APP_SECRET
		}
	}

	return ""
}

// 删除 facebook 账号
func Del(c *gin.Context) {
	appId := c.Param("id")
	appSecret := getSecret(appId)
	if appSecret == "" {
		log.Error("facebook.del appId=%s appSecret=%s is invalid", appId, appSecret)
		return
	}

	var req struct {
		Signed_Request string `json:"signed_request" form:"signed_request"`
	}

	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s query params err %v", "facebook.controller.DelFacebook", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("facebook.controller.DelFacebook appId=%s appSecret=%s req ==> %+v", appId, appSecret, req)

	if req.Signed_Request == "" {
		log.Debug("skip this case as we don't have a valid signed request.")
		return
	}

	app := facebook.New(appId, appSecret)
	res, err := app.ParseSignedRequest(req.Signed_Request)
	if err != nil {
		log.Error("cannot parse signed request. [e:%v]", err)
		return
	}

	log.Debug("signed request is '%v'.", res)

	facebookId := fmt.Sprintf("fb.%s", res.Get("user_id"))
	userId := db.DelFacebook(facebookId)
	if userId <= 0 {
		log.Error("facebook.Del facebookId=%s userId=%d", facebookId, userId)
	}

	// 生成md5加密串
	checkContent := fmt.Sprintf("userId=fb.%s&key=%s", res.Get("user_id"), config.Server.Facebook_KEY)
	sign := public.GetMd5String(checkContent)

	status_url := fmt.Sprintf("%s?id=%s&sign=%s", config.Server.Facebook_STATUS_URL, res.Get("user_id"), sign)
	confirmation_code := strconv.Itoa(userId)

	c.JSON(http.StatusOK, RespData{
		Url:               status_url,
		Confirmation_code: confirmation_code,
	})

	log.Debug("facebook.Del 删除成功 userId=%d facebookId=%s", userId, facebookId)
	return
}

func Info(c *gin.Context) {
	var req struct {
		UserID int    `json:"id" form:"id"`
		Sign   string `json:"sign" form:"sign"`
	}

	if err := c.ShouldBind(&req); err != nil {
		log.Debug("%s query params err %v", "hall.controller.Info", err)
		c.String(http.StatusOK, "")
		return
	}

	log.Debug("facebook.controller.Info req ==> %+v", req)

	if req.UserID <= 0 {
		c.String(http.StatusOK, "Akun tidak valid")
		return
	}

	// 生成md5加密串
	checkContent := fmt.Sprintf("userId=fb.%d&key=%s", req.UserID, config.Server.Facebook_KEY)
	sign := public.GetMd5String(checkContent)
	if sign != req.Sign {
		c.String(http.StatusOK, "data ilegal")
		return
	}

	facebookId := fmt.Sprintf("fb.%d", req.UserID)
	userId := db.GetFacebook(facebookId)
	if userId > 0 {
		c.String(http.StatusOK, "Akun ini valid")
		return
	}

	c.String(http.StatusOK, "Akun telah dibatalkan")
	return
}
