package router

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bet24.com/servers/payment/huawei"

	"bet24.com/servers/payment/payermax"

	"bet24.com/servers/payment/adjust"
	"bet24.com/servers/payment/apple"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/controller"
	"bet24.com/servers/payment/facebook"
	"bet24.com/servers/payment/google"
	"bet24.com/servers/payment/hall"
	"bet24.com/servers/payment/middleware"
	"bet24.com/servers/payment/update"
	"github.com/gin-gonic/gin"
	"github.com/mattn/go-colorable"
)

func Run() {

	// 强制日志颜色化
	gin.ForceConsoleColor()

	// 设置颜色输出,识别 console 色值
	gin.DefaultWriter = colorable.NewColorableStdout()

	// 设置日志模式
	gin.SetMode(gin.DebugMode)

	// 创建一个默认的路由
	r := gin.Default()
	r.Static("/static", "./html/static")
	r.LoadHTMLGlob("html/templates/*")
	r.Use(middleware.CheckValid(), middleware.Cors())

	// 创建一个默认的路由(HTTPS)
	rTls := gin.Default()
	rTls.Static("/static", "./html/static")
	rTls.LoadHTMLGlob("html/templates/*")
	rTls.Use(middleware.TlsHandler(), middleware.CheckValid(), middleware.Cors()) // 处理SSL的中间件

	// ------------------------------苹果支付------------------------------
	appleGroup := rTls.Group("/apple")
	{
		// 苹果验证
		appleGroup.POST("/verify", apple.Verify)
	}

	adjustGroup := rTls.Group("/adjust")
	{
		adjustGroup.POST("/notify", adjust.Notify)
		adjustGroup.GET("/notify", adjust.Notify)
	}

	// ------------------------------谷歌支付------------------------------
	googleGroup := rTls.Group("/google")
	{
		// 谷歌支付验证
		googleGroup.POST("/verify", google.Verify)
		googleGroup.GET("/rewardAD", google.RewardAD)
		googleGroup.POST("/rewardAD", google.RewardAD)
	}

	// ------------------------------facebook------------------------------
	facebookGroup := rTls.Group("/facebook")
	{
		// facebook
		facebookGroup.POST("/del/:id", facebook.Del)

		facebookGroup.POST("/info", facebook.Info)
		facebookGroup.GET("/info", facebook.Info)
	}

	// ------------------------------cropay支付------------------------------
	//cropayGroupTls := rTls.Group("/cropay")
	//{
	//	// 下单（POST）
	//	cropayGroupTls.POST("/order", cropay.Order)
	//	// 下单(GET)
	//	cropayGroupTls.GET("/order", cropay.Order)
	//
	//	// 回调(POST)
	//	cropayGroupTls.POST("/notify", cropay.Notify)
	//	// 回调(GET)
	//	cropayGroupTls.GET("/notify", cropay.Notify)
	//
	//	// 成功(POST)
	//	cropayGroupTls.POST("/succ_back", cropay.Succ)
	//	// 成功(GET)
	//	cropayGroupTls.GET("/succ_back", cropay.Succ)
	//
	//	// 失败(POST)
	//	cropayGroupTls.POST("/fail_back", cropay.Fail)
	//	// 失败(GET)
	//	cropayGroupTls.GET("/fail_back", cropay.Fail)
	//}

	// 聚合支付、提现
	//zhongshuiGroupTls := rTls.Group("/zshui")
	//{
	//	// 充值下单（POST）
	//	zhongshuiGroupTls.POST("/payOrder", zhongshui.PayOrder)
	//	// 充值下单(GET)
	//	zhongshuiGroupTls.GET("/payOrder", zhongshui.PayOrder)
	//
	//	// 支付完跳转处理（POST）
	//	zhongshuiGroupTls.POST("/payCallback", zhongshui.PayCallback)
	//	// 支付完跳转处理(GET)
	//	zhongshuiGroupTls.GET("/payCallback", zhongshui.PayCallback)
	//
	//	// 充值回调(POST)
	//	zhongshuiGroupTls.POST("/payNotify", zhongshui.PayNotify)
	//	// 充值回调(GET)
	//	zhongshuiGroupTls.GET("/payNotify", zhongshui.PayNotify)
	//
	//	// 提现请求(POST)
	//	zhongshuiGroupTls.POST("/withdrawOrder", zhongshui.WithdrawOrder)
	//	// 提现请求(GET)
	//	zhongshuiGroupTls.GET("/withdrawOrder", zhongshui.WithdrawOrder)
	//
	//	// 提现回调(POST)
	//	zhongshuiGroupTls.POST("/withdrawNotify", zhongshui.WithdrawNotify)
	//	// 提现回调(GET)
	//	zhongshuiGroupTls.GET("/withdrawNotify", zhongshui.WithdrawNotify)
	//}

	// Kaya支付、提现
	//kayaGroupTls := rTls.Group("/kaya")
	//{
	//	// 充值下单（POST）
	//	kayaGroupTls.POST("/payOrder", kaya.PayOrder)
	//	// 充值下单(GET)
	//	kayaGroupTls.GET("/payOrder", kaya.PayOrder)
	//
	//	// 充值回调(POST)
	//	kayaGroupTls.POST("/payNotify", kaya.PayNotify)
	//	// 充值回调(GET)
	//	kayaGroupTls.GET("/payNotify", kaya.PayNotify)
	//
	//	// 提现请求(POST)
	//	kayaGroupTls.POST("/withdrawOrder", kaya.WithdrawOrder)
	//	// 提现请求(GET)
	//	kayaGroupTls.GET("/withdrawOrder", kaya.WithdrawOrder)
	//
	//	// 提现回调(POST)
	//	kayaGroupTls.POST("/withdrawNotify", kaya.WithdrawNotify)
	//	// 提现回调(GET)
	//	kayaGroupTls.GET("/withdrawNotify", kaya.WithdrawNotify)
	//}

	// toppay 支付、提现
	//toppayGroupTls := rTls.Group("/toppay")
	//{
	//	// 充值下单（POST）
	//	toppayGroupTls.POST("/payOrder", toppay.PayOrder)
	//	// 充值下单(GET)
	//	toppayGroupTls.GET("/payOrder", toppay.PayOrder)
	//
	//	// 充值回调(POST)
	//	toppayGroupTls.POST("/payNotify", toppay.PayNotify)
	//	// 充值回调(GET)
	//	toppayGroupTls.GET("/payNotify", toppay.PayNotify)
	//
	//	// 提现请求(POST)
	//	toppayGroupTls.POST("/withdrawOrder", toppay.WithdrawOrder)
	//	// 提现请求(GET)
	//	toppayGroupTls.GET("/withdrawOrder", toppay.WithdrawOrder)
	//
	//	// 提现回调(POST)
	//	toppayGroupTls.POST("/withdrawNotify", toppay.WithdrawNotify)
	//	// 提现回调(GET)
	//	toppayGroupTls.GET("/withdrawNotify", toppay.WithdrawNotify)
	//}

	// toppay 支付、提现
	//toppayGroup := r.Group("/toppay")
	//{
	//	// 充值下单（POST）
	//	toppayGroup.POST("/payOrder", toppay.PayOrder)
	//	// 充值下单(GET)
	//	toppayGroup.GET("/payOrder", toppay.PayOrder)
	//
	//	// 充值回调(POST)
	//	toppayGroup.POST("/payNotify", toppay.PayNotify)
	//	// 充值回调(GET)
	//	toppayGroup.GET("/payNotify", toppay.PayNotify)
	//
	//	// 提现请求(POST)
	//	toppayGroup.POST("/withdrawOrder", toppay.WithdrawOrder)
	//	// 提现请求(GET)
	//	toppayGroup.GET("/withdrawOrder", toppay.WithdrawOrder)
	//
	//	// 提现回调(POST)
	//	toppayGroup.POST("/withdrawNotify", toppay.WithdrawNotify)
	//	// 提现回调(GET)
	//	toppayGroup.GET("/withdrawNotify", toppay.WithdrawNotify)
	//}

	// flashpay 支付、提现
	//flashpayGroup := r.Group("/flashpay")
	//{
	//	// 充值下单（POST）
	//	flashpayGroup.POST("/payOrder", flash.PayOrder)
	//	// 充值下单(GET)
	//	flashpayGroup.GET("/payOrder", flash.PayOrder)
	//
	//	// 充值回调(POST)
	//	flashpayGroup.POST("/payNotify", flash.PayNotify)
	//	// 充值回调(GET)
	//	flashpayGroup.GET("/payNotify", flash.PayNotify)
	//
	//	// 提现请求(POST)
	//	flashpayGroup.POST("/withdrawOrder", flash.WithdrawOrder)
	//	// 提现请求(GET)
	//	flashpayGroup.GET("/withdrawOrder", flash.WithdrawOrder)
	//
	//	// 提现回调(POST)
	//	flashpayGroup.POST("/withdrawNotify", flash.WithdrawNotify)
	//	// 提现回调(GET)
	//	flashpayGroup.GET("/withdrawNotify", flash.WithdrawNotify)
	//}

	// Lets支付、提现
	//letsGroupTls := rTls.Group("/lets")
	//{
	//	// 充值下单（POST）
	//	letsGroupTls.POST("/payOrder", lets.PayOrder)
	//	// 充值下单(GET)
	//	letsGroupTls.GET("/payOrder", lets.PayOrder)
	//
	//	// 充值回调(POST)
	//	letsGroupTls.POST("/payNotify", lets.PayNotify)
	//	// 充值回调(GET)
	//	letsGroupTls.GET("/payNotify", lets.PayNotify)
	//
	//	// 支付完跳转处理（POST）
	//	letsGroupTls.POST("/return", lets.PayCallback)
	//	// 支付完跳转处理(GET)
	//	letsGroupTls.GET("/return", lets.PayCallback)
	//
	//	// 提现请求(POST)
	//	letsGroupTls.POST("/withdrawOrder", lets.WithdrawOrder)
	//	// 提现请求(GET)
	//	letsGroupTls.GET("/withdrawOrder", lets.WithdrawOrder)
	//
	//	// 提现回调(POST)
	//	letsGroupTls.POST("/withdrawNotify", lets.WithdrawNotify)
	//	// 提现回调(GET)
	//	letsGroupTls.GET("/withdrawNotify", lets.WithdrawNotify)
	//}

	// Liu支付、提现
	//liuGroupTls := rTls.Group("/liu")
	//{
	//	// 充值下单（POST）
	//	liuGroupTls.POST("/payOrder", liu.PayOrder)
	//	// 充值下单(GET)
	//	liuGroupTls.GET("/payOrder", liu.PayOrder)
	//
	//	// 充值回调(POST)
	//	liuGroupTls.POST("/payNotify", liu.PayNotify)
	//	// 充值回调(GET)
	//	liuGroupTls.GET("/payNotify", liu.PayNotify)
	//
	//	// 支付完跳转处理（POST）
	//	liuGroupTls.POST("/return", liu.PayCallback)
	//	// 支付完跳转处理(GET)
	//	liuGroupTls.GET("/return", liu.PayCallback)
	//
	//	// 提现请求(POST)
	//	liuGroupTls.POST("/withdrawOrder", liu.WithdrawOrder)
	//	// 提现请求(GET)
	//	liuGroupTls.GET("/withdrawOrder", liu.WithdrawOrder)
	//
	//	// 提现审核
	//	liuGroupTls.POST("/withdrawAudit", liu.WithdrawAudit)
	//}

	// dingpei支付、提现
	//dingpeiGroupTls := rTls.Group("/dingpei")
	//{
	//	// 充值下单（POST）
	//	dingpeiGroupTls.POST("/payOrder", dingpei.PayOrder)
	//	// 充值下单(GET)
	//	dingpeiGroupTls.GET("/payOrder", dingpei.PayOrder)
	//
	//	// 充值回调(POST)
	//	dingpeiGroupTls.POST("/payNotify", dingpei.PayNotify)
	//	// 充值回调(GET)
	//	dingpeiGroupTls.GET("/payNotify", dingpei.PayNotify)
	//
	//	// 提现请求(POST)
	//	dingpeiGroupTls.POST("/withdrawOrder", dingpei.WithdrawOrder)
	//	// 提现请求(GET)
	//	dingpeiGroupTls.GET("/withdrawOrder", dingpei.WithdrawOrder)
	//
	//	// 提现回调(POST)
	//	dingpeiGroupTls.POST("/withdrawNotify", dingpei.WithdrawNotify)
	//	// 提现回调(GET)
	//	dingpeiGroupTls.GET("/withdrawNotify", dingpei.WithdrawNotify)
	//
	//	// 支付完跳转处理（POST）
	//	dingpeiGroupTls.POST("/return", dingpei.PayCallback)
	//	// 支付完跳转处理(GET)
	//	dingpeiGroupTls.GET("/return", dingpei.PayCallback)
	//}

	// payermax支付、提现
	payermaxGroupTls := rTls.Group("/payermax")
	{
		// 充值下单（POST）
		payermaxGroupTls.POST("/payOrder", payermax.PayOrder)
		// 充值下单(GET)
		payermaxGroupTls.GET("/payOrder", payermax.PayOrder)

		// 充值回调(POST)
		payermaxGroupTls.POST("/payNotify", payermax.PayNotify)
		// 充值回调(GET)
		payermaxGroupTls.GET("/payNotify", payermax.PayNotify)

		// 提现请求(POST)
		payermaxGroupTls.POST("/withdrawOrder", payermax.WithdrawOrder)
		// 提现请求(GET)
		payermaxGroupTls.GET("/withdrawOrder", payermax.WithdrawOrder)

		// 提现回调(POST)
		payermaxGroupTls.POST("/withdrawNotify", payermax.WithdrawNotify)
		// 提现回调(GET)
		payermaxGroupTls.GET("/withdrawNotify", payermax.WithdrawNotify)

		// 支付完跳转处理（POST）
		payermaxGroupTls.POST("/return", payermax.PayCallback)
		// 支付完跳转处理(GET)
		payermaxGroupTls.GET("/return", payermax.PayCallback)
	}

	// crush支付、提现
	//crushGroupTls := rTls.Group("/crush")
	//{
	//	// 充值下单（POST）
	//	crushGroupTls.POST("/payOrder", crush.PayOrder)
	//	// 充值下单(GET)
	//	crushGroupTls.GET("/payOrder", crush.PayOrder)
	//
	//	// 充值回调(POST)
	//	crushGroupTls.POST("/payNotify", crush.PayNotify)
	//	// 充值回调(GET)
	//	crushGroupTls.GET("/payNotify", crush.PayNotify)
	//
	//	// 提现请求(POST)
	//	crushGroupTls.POST("/withdrawOrder", crush.WithdrawOrder)
	//	// 提现请求(GET)
	//	crushGroupTls.GET("/withdrawOrder", crush.WithdrawOrder)
	//
	//	// 提现回调(POST)
	//	crushGroupTls.POST("/withdrawNotify", crush.WithdrawNotify)
	//	// 提现回调(GET)
	//	crushGroupTls.GET("/withdrawNotify", crush.WithdrawNotify)
	//
	//	// 支付完跳转处理（POST）
	//	crushGroupTls.POST("/return", crush.PayCallback)
	//	// 支付完跳转处理(GET)
	//	crushGroupTls.GET("/return", crush.PayCallback)
	//}

	// oPay支付、提现
	//oPayGroupTls := rTls.Group("/opay")
	//{
	//	// 充值下单（POST）
	//	oPayGroupTls.POST("/payOrder", opay.PayOrder)
	//	// 充值下单(GET)
	//	oPayGroupTls.GET("/payOrder", opay.PayOrder)
	//
	//	// 充值回调(POST)
	//	oPayGroupTls.POST("/payNotify", opay.PayNotify)
	//	// 充值回调(GET)
	//	oPayGroupTls.GET("/payNotify", opay.PayNotify)
	//
	//	// 支付完跳转处理（POST）
	//	oPayGroupTls.POST("/return", opay.PayCallback)
	//	// 支付完跳转处理(GET)
	//	oPayGroupTls.GET("/return", opay.PayCallback)
	//}

	// paymob支付、提现
	//paymobGroupTls := rTls.Group("/paymob")
	//{
	//	// 充值下单（POST）
	//	paymobGroupTls.POST("/payOrder", paymob.PayOrder)
	//	// 充值下单(GET)
	//	paymobGroupTls.GET("/payOrder", paymob.PayOrder)
	//
	//	// 充值回调(POST)
	//	paymobGroupTls.POST("/payNotify", paymob.PayNotify)
	//	// 充值回调(GET)
	//	paymobGroupTls.GET("/payNotify", paymob.PayNotify)
	//
	//	// 支付完跳转处理（POST）
	//	paymobGroupTls.POST("/return", paymob.PayCallback)
	//	// 支付完跳转处理(GET)
	//	paymobGroupTls.GET("/return", paymob.PayCallback)
	//}

	// fawry支付、提现
	//fawryGroupTls := rTls.Group("/fawry")
	//{
	//	// 充值下单（POST）
	//	fawryGroupTls.POST("/payOrder", fawry.PayOrder)
	//	// 充值下单(GET)
	//	fawryGroupTls.GET("/payOrder", fawry.PayOrder)
	//
	//	// 充值回调(POST)
	//	fawryGroupTls.POST("/payNotify", fawry.PayNotify)
	//	// 充值回调(GET)
	//	fawryGroupTls.GET("/payNotify", fawry.PayNotify)
	//}

	// la支付、提现
	//laGroupTls := rTls.Group("/la")
	//{
	//	// 充值下单（POST）
	//	laGroupTls.POST("/payOrder", lapay.PayOrder)
	//	// 充值下单(GET)
	//	laGroupTls.GET("/payOrder", lapay.PayOrder)
	//
	//	// 充值回调(POST)
	//	laGroupTls.POST("/payNotify", lapay.PayNotify)
	//	// 充值回调(GET)
	//	laGroupTls.GET("/payNotify", lapay.PayNotify)
	//
	//	// 支付完跳转处理（POST）
	//	laGroupTls.POST("/return", lapay.PayCallback)
	//	// 支付完跳转处理(GET)
	//	laGroupTls.GET("/return", lapay.PayCallback)
	//}

	// coda支付、提现
	//codaGroupTls := rTls.Group("/coda")
	//{
	//	// 充值下单（POST）
	//	codaGroupTls.POST("/payOrder", coda.PayOrder)
	//	// 充值下单(GET)
	//	codaGroupTls.GET("/payOrder", coda.PayOrder)
	//
	//	// 充值回调(POST)
	//	codaGroupTls.POST("/payNotify", coda.PayNotify)
	//	// 充值回调(GET)
	//	codaGroupTls.GET("/payNotify", coda.PayNotify)
	//
	//	// 支付完跳转处理（POST）
	//	codaGroupTls.POST("/return", coda.PayCallback)
	//	// 支付完跳转处理(GET)
	//	codaGroupTls.GET("/return", coda.PayCallback)
	//
	//}

	// 华为支付
	huaweiGroupTls := rTls.Group("/huawei")
	{
		// 充值下单（POST）
		huaweiGroupTls.POST("/order", huawei.Order)
		// 充值下单(GET)
		huaweiGroupTls.GET("/order", huawei.Order)

		// 充值回调(POST)
		huaweiGroupTls.POST("/notify", huawei.Notify)
		// 充值回调(GET)
		huaweiGroupTls.GET("/notify", huawei.Notify)
	}

	// 检查更新
	checkUpdate := r.Group("/update")
	{
		// 查询余额
		checkUpdate.GET("/checkupdate", update.CheckUpdate)
	}

	// 大厅功能
	hallTls := rTls.Group("/hall")
	{
		// 师徒登录（绑定）
		hallTls.POST("/register", hall.Register)
		// 代理绑定
		hallTls.POST("/bind", hall.Bind)
		// 汇率列表
		hallTls.POST("/getExchangeRate", hall.GetExchangeRateList)
		// 网页商城列表
		hallTls.POST("/getShopList", hall.GetShopList)
		// 用户信息
		hallTls.POST("/userInfo", hall.GetUserInfo)

		hallTls.POST("/isInReview", hall.IsInReview)
		hallTls.GET("/isInReview", hall.IsInReview)

		hallTls.POST("/uploadFile", hall.UploadFile)

		hallTls.POST("/feedback", hall.Feedback)
		hallTls.GET("/feedback", hall.Feedback)
	}

	// ------------------------------404错误------------------------------
	// 404错误
	r.NoRoute(controller.NotFoundRoute)
	r.NoMethod(controller.NotFoundMethod)

	// 启动HTTPS服务(gin默认启动服务)
	go rTls.RunTLS(fmt.Sprintf(":%d", config.Server.TlsPort), config.Server.TlsCert, config.Server.TlsKey)

	// 启动HTTP服务(gin默认启动服务)
	r.Run(fmt.Sprintf(":%d", config.Server.WebPort))

	// ------------------------------优雅关机------------------------------
	// 实现优雅的关机
	srv := &http.Server{
		Addr:    fmt.Sprintf("%d", config.Server.WebPort),
		Handler: r,
	}

	go func() {

		fmt.Printf("Listening and serving HTTP on %v\n", srv.Addr)

		// 启动HTTP服务
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// 等待中断信号来优雅地关闭服务器，为关闭服务器操作设置一个超时时长
	quit := make(chan os.Signal, 1) // 创建一个接收信号的通道

	// kill 默认会发送 syscall.SIGTERM 信号
	// kill -2 发送 syscall.SIGINT 信号,我们常用的 Ctrl+C 就是触发系统SIGINT信号
	// kill -9 发送 syscall.SIGKILL 信号，但是不能被捕获，所以不需要添加它
	// signal.Notify 把收到的 syscall.SIGINT 或 syscall.SIGTERM 信号转发给 quit
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM) // 此处不会阻塞

	<-quit // 阻塞在此，当接收到上述两种信号时才会往下执行

	fmt.Println("Shutdown Server!")

	// 创建一个30秒超时的context
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	// 优雅关闭服务（将未处理完的请求处理完再关闭服务），超时就退出
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	fmt.Println("payment server closed ...")
}
