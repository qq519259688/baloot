package payermax

type (
	withdraw_req struct {
		Version       string          `json:"version,omitempty"`       // 接口版本。当前值为：1.0
		KeyVersion    string          `json:"keyVersion,omitempty"`    // 密钥版本。当前值为：1
		RequestTime   string          `json:"requestTime,omitempty"`   // 请求时间，符合rfc3339规范，格式：yyyy-MM-dd’T’HH:mm:ss.SSSXXX
		MerchantAppId string          `json:"merchantAppId,omitempty"` // 商户应用Id，PayerMax分配给商户应用的唯一标识
		MerchantNo    string          `json:"merchantNo,omitempty"`    // 商户号，商户与PayerMax业务签约时生成的唯一标识
		Data          withdrawReqData `json:"data,omitempty"`          // 请求数据体
	}

	withdrawReqData struct {
		OutTradeNo  string         `json:"outTradeNo,omitempty"` // 商户订单号，唯一标识商户的一笔交易，不能重复，只能包含字母、数字、下划线。
		Country     string         `json:"country,omitempty"`    // 国家代码，大写字母，参见【交易支持国家/地区与币种】
		Trade       *tradeData     `json:"trade,omitempty"`      // 交易信息
		PayeeInfo   *payeeInfoData `json:"payeeInfo,omitempty"`  // 收款方信息
		Document    *documentData  `json:"document,omitempty"`
		Address     *addressData   `json:"address,omitempty"`
		PayeePhone  string         `json:"payeePhone,omitempty"`  // 收款方移动电话号码，在不同国家-支付方式下填写规范不同
		BirthDate   string         `json:"birthDate,omitempty"`   // 收款方出生日期，yyyy-MM-dd格式
		Email       string         `json:"email,omitempty"`       // 收款方邮箱
		PayerInfo   *payerInfoData `json:"payerInfo,omitempty"`   // 付款方信息
		ExpiryDays  string         `json:"expiryDays,omitempty"`  // 取款码有效天数，当前仅在FawryCash中有效，支持传入1~15整数（1=24Hours），其他值或不传则默认为7
		Remark      string         `json:"remark,omitempty"`      // 出款附言或备注，允许英文，数字，中划线，空格，点；受渠道约束，该字段可能会进行特殊处理，如超长截断或填充默认值；
		Reference   string         `json:"reference,omitempty"`   // 透传数据，在付款异步回调通知中原样返回，该字段主要用于商户携带订单的自定义数据
		NotifyUrl   string         `json:"notifyUrl,omitempty"`   // 商户接收付款结果的后台回调地址，以http/https开头
		NotifyEmail string         `json:"notifyEmail,omitempty"` // 收款方通知邮箱
		NotifyPhone string         `json:"notifyPhone,omitempty"` // 收款方通知电话
	}

	tradeData struct {
		Amount   string `json:"amount,omitempty"`   // 交易金额 , 注意：印尼，越南，韩国，智利，巴基斯坦，哥伦比亚地区，本币不支持带小数金额
		Currency string `json:"currency,omitempty"` // 交易币种
	}

	payeeInfoData struct {
		PaymentMethod string           `json:"paymentMethod,omitempty"` // 支付方式，参见【支付方式及目标机构说明】
		TargetOrg     string           `json:"targetOrg,omitempty"`     // 目标机构，参见【支付方式及目标机构说明】
		PayeeType     string           `json:"payeeType,omitempty"`     // 收款方类型（PERSONAL/CORPORATION），默认PERSONAL
		AccountInfo   *accountInfoData `json:"accountInfo,omitempty"`   // 收款账户信息
		BankInfo      *bankInfoData    `json:"bankInfo,omitempty"`
		Name          *nameData        `json:"name,omitempty"` // 收款方姓名，支持英文、空格、逗号、中划线、点，在不同国家-支付方式下填写规范不同
	}

	accountInfoData struct {
		AccountNo   string `json:"accountNo,omitempty"`   // 收款方账号，根据付款方式不同填入相应账号
		AccountType string `json:"accountType,omitempty"` // 收款账户类型,在不同国家-支付方式下规范不同
		CheckDigit  string `json:"checkDigit,omitempty"`  // 收款方账户验证数字
	}

	bankInfoData struct {
		BankCode     string `json:"bankCode,omitempty"`     // 用于识别特定金融机构下的收款方银行代码，在不同国家下填写规范不同，参见《付款申请模板及填写规范》以及【支持银行范围列表】
		BankName     string `json:"bankName,omitempty"`     // 收款方银行名称
		BankBranch   string `json:"bankBranch,omitempty"`   // 收款人银行网点号/分行号
		BankCity     string `json:"bankCity,omitempty"`     // 收款方银行所在城市
		CorAccountNo string `json:"corAccountNo,omitempty"` // 收款方银行的代理账号（correspondent account）
	}

	nameData struct {
		FirstName  string `json:"firstName,omitempty"`  // 收款方名
		MiddleName string `json:"middleName,omitempty"` // 收款方中间名，多段中间名时，使用空格进行分隔
		LastName   string `json:"lastName,omitempty"`   // 收款方姓
		FullName   string `json:"fullName,omitempty"`   // 收款方全名
	}

	documentData struct {
		DocumentType       string `json:"documentType,omitempty"`       // 收款方个人身份证明类型，在不同国家下可选类型不同，参见【身份证明】
		DocumentId         string `json:"documentId,omitempty"`         // 收款方个人识别号码，在不同国家下可选类型不同，参见【身份证明】
		DocumentIssueDate  string `json:"documentIssueDate,omitempty"`  // 收款方个人识别号码生效日期，在不同国家下可选类型不同，yyyy-MM-dd格式
		DocumentExpireDate string `json:"documentExpireDate,omitempty"` // 收款方个人识别号码失效日期，在不同国家下可选类型不同，yyyy-MM-dd格式
	}

	addressData struct {
		Address string `json:"address,omitempty"` // 收款方地址
		City    string `json:"city,omitempty"`    // 收款方城市
		ZipCode string `json:"zipCode,omitempty"` // 收款方邮编
	}

	payerInfoData struct {
		Name *payerInfoNameData `json:"name,omitempty"` // 付款方姓名，支持英文、空格、逗号、中划线、点，在不同国家-支付方式下填写规范
	}

	payerInfoNameData struct {
		FullName string `json:"fullName,omitempty"` // 付款方全名
	}

	withdraw_resp struct {
		Code string            `json:"code,omitempty"` // 接口响应码，’APPLY_SUCCESS’代表成功
		Msg  string            `json:"msg,omitempty"`  // 响应描述，’Success.’
		Data *withdrawRespData `json:"data,omitempty"` // 返回数据体
	}

	withdrawRespData struct {
		OutTradeNo string `json:"outTradeNo,omitempty"` // 商户订单号
		TradeNo    string `json:"tradeNo,omitempty"`    // PayerMax流水号
		Status     string `json:"status,omitempty"`     // 交易状态
	}
)

type (
	withdrawNotify struct {
		Code          string              `json:"code,omitempty"`          // 接口响应码，’APPLY_SUCCESS’代表接口调用成功
		Msg           string              `json:"msg,omitempty"`           // 响应描述，’Success.’
		Version       string              `json:"version,omitempty"`       // 接口版本。当前值为：1.0
		KeyVersion    string              `json:"keyVersion,omitempty"`    // 密钥版本。当前值为：1
		MerchantAppId string              `json:"merchantAppId,omitempty"` // 商户应用Id，PayerMax分配给商户应用的唯一标识
		MerchantNo    string              `json:"merchantNo,omitempty"`    // 商户号，商户与PayerMax业务签约时生成的唯一标识
		NotifyTime    string              `json:"notifyTime,omitempty"`    // 通知时间，符合rfc3339规范，格式：yyyy-MM-dd’T’HH:mm:ss.SSSXXX
		NotifyType    string              `json:"notifyType,omitempty"`    // 通知类型，PAYOUT
		Data          *withdrawNotifyData `json:"data,omitempty"`          // 返回数据体
		NotifyPhone   string              `json:"notifyPhone,omitempty"`   // 收款方通知邮件手机号
		NotifyAddress string              `json:"notifyAddress,omitempty"` // 收款方通知邮件地址，
		ResponseCode  string              `json:"responseCode,omitempty"`  // 订单失败错误码,详见[错误码列表]
		ResponseMsg   string              `json:"responseMsg,omitempty"`   // 订单失败错误码描述
	}

	withdrawNotifyData struct {
		OutTradeNo         string                 `json:"outTradeNo,omitempty"`         // 商户订单号
		TradeNo            string                 `json:"tradeNo,omitempty"`            // PayerMax交易流水号
		Status             string                 `json:"status,omitempty"`             // 交易状态，SUCCESS成功, FAILD失败 ，PENDING进行中，BOUNCEBACK退票
		TransactionUtcTime string                 `json:"transactionUtcTime,omitempty"` // 交易创建时间，符合rfc3339规范，格式：yyyy-MM-dd’T’HH:mm:ss.SSSXXX
		Trade              *notifyTradeData       `json:"trade,omitempty"`              // 交易信息
		Source             *notifySourceData      `json:"source,omitempty"`             // 付款信息（商户）
		Destination        *notifyDestinationData `json:"destination,omitempty"`        // 收款信息（用户）
	}

	notifyTradeData struct {
		Amount   string `json:"amount,omitempty"`   // 商户在请求中传入的付款金额，单位元
		Currency string `json:"currency,omitempty"` // 商户在请求中传入的付款金额对应货币代码
	}

	notifySourceData struct {
		Amount   string `json:"amount,omitempty"`   // 付款方-扣款金额
		Currency string `json:"currency,omitempty"` // 付款方-扣款币种
		Fee      string `json:"fee,omitempty"`
	}

	notifyDestinationData struct {
		Amount   string `json:"amount,omitempty"`   // 收款方-到账金额
		Currency string `json:"currency,omitempty"` // 收款方-到账币种
	}
)
