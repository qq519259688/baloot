package payermax

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"

	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/common"
	notification "bet24.com/servers/micros/notification/proto"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"github.com/gin-gonic/gin"
)

// 提现请求
func WithdrawOrder(c *gin.Context) {
	obj := db.NewPayerMaxWithdrawReq()
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "payermax.WithdrawOrder", err)
		c.String(http.StatusOK, "fail")
		return
	}

	// 判断提现金额足购
	/*if !coreClient.TryWithdraw(obj.In.UserID, obj.In.Amount, true) {
		c.String(http.StatusOK, "Insufficient amount")
		return
	}*/

	obj.In.IPAddress = strings.Split(c.Request.RemoteAddr, ":")[0]
	obj.DoAction()
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v obj.Out=%+v", "payermax.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	if obj.Out.GetStatus != 0 {
		log.Debug("%s GenOrder Audit obj.In=%+v obj.Out=%+v", "payermax.WithdrawOrder", obj.In, obj.Out)
		c.String(http.StatusOK, "fail")
		return
	}

	// 请求时，没有传手机号
	if obj.In.Mobile == "" {
		obj.In.Mobile = obj.Out.Tel
	}

	// 请求withdrawOrder的代码
	req := withdraw_req{
		Version:       "1.0",
		KeyVersion:    "1",
		RequestTime:   common.GetNowTime().Format(dateFormat),
		MerchantAppId: config.Server.PayerMax.MerchantAppId,
		MerchantNo:    config.Server.PayerMax.MerchantNo,
		Data: withdrawReqData{
			OutTradeNo: obj.Out.OrderID,
			Country:    "SA",
			Trade: &tradeData{
				Amount:   strconv.Itoa(obj.In.Amount),
				Currency: "SAR",
			},
			PayeeInfo: &payeeInfoData{
				PaymentMethod: "WALLET",
				TargetOrg:     "STCPAY",
				AccountInfo: &accountInfoData{
					AccountNo: obj.In.BankCard,
				},
				BankInfo: &bankInfoData{
					BankCode: obj.In.BankCode,
					BankName: obj.In.BankName,
				},
				Name: &nameData{
					FullName: obj.In.RealName,
				},
			},
			NotifyUrl: config.Server.PayerMax.Url_withdraw_Notify,
		},
	}

	// 生成签名
	buf, err := json.Marshal(req)
	if err != nil {
		log.Error("payermax.WithdrawOrder checkContent json marshal fail %v", err)
		return
	}

	checkContent := string(buf)
	sign := public.Sha256WithRsa(checkContent, config.Server.PayerMax.MCH_PRIVATE_KEY)
	log.Debug("payermax.WithdrawOrder checkContent=%s sign=%s", checkContent, sign)

	respBody := httpPostByJson(config.Server.PayerMax.Url_withdraw_order, checkContent, sign)
	log.Debug("payermax.WithdrawOrder req ==> %+v  resp ==> %+v", checkContent, respBody)

	var resp withdraw_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("payermax.WithdrawOrder json unmarshal req ==> %+v resp ==> %+v fail %v", req, respBody, err)
		c.String(http.StatusOK, "fail")
		return
	}

	log.Debug("payermax.WithdrawOrder resp ==> %+v", resp)

	// 接口响应码，’APPLY_SUCCESS’代表成功
	if resp.Code != "APPLY_SUCCESS" {
		log.Error("payermax.withdrawRequest post return resp fail ==> %+v", resp)

		// 余额不足,退款
		// if resp.Data.Status != "PENDING" {
		// 	obj := db.NewPayerMaxWithdrawNotify()
		// 	obj.In.OrderID = req.Data.OutTradeNo
		// 	obj.In.DfTransactionId = resp.Data.TradeNo
		// 	obj.In.Status = 0
		// 	obj.In.DfDesc = fmt.Sprintf("%s", resp.Data.Status)
		// 	obj.In.Balance = 0
		// 	obj.DoAction()
		//
		// 	log.Debug("payermax.WithdrawOrder obj.In=%+v obj.Out=%+v", obj.In, obj.Out)
		// }

		c.String(http.StatusOK, "fail")
		return
	}

	go func() {
		//coreClient.TryWithdraw(obj.In.UserID, obj.In.Amount, false)

		// 通知
		notification.AddNotification(obj.In.UserID, notification.Notification_Gold, "")
	}()

	// 更新银行信息
	// objBank := db.NewBankInfoUp()
	// objBank.In.UserID = obj.In.UserID
	// objBank.In.RealName = req.Name
	// objBank.In.BankCard = req.Number
	// objBank.In.BankName = req.BankCode
	// objBank.In.Mobile = obj.In.Mobile
	// objBank.In.EMail = obj.In.Email
	// objBank.In.Address = obj.In.Address
	// objBank.DoAction()

	// log.Debug("payermax.WithdrawNotify need save bankInfo")

	c.String(http.StatusOK, "success")
	return
}

// 提现通知
func WithdrawNotify(c *gin.Context) {
	bodyData, _ := io.ReadAll(c.Request.Body)
	log.Debug("payermax.WithdrawNotify ==> ctx.Request.body: %v", string(bodyData))

	// 获取签名
	sign := c.Request.Header.Get("sign")
	log.Debug("payermax.WithdrawNotify sign=%s", sign)

	var resp withdrawNotify
	if err := json.Unmarshal(bodyData, &resp); err != nil {
		log.Debug("%s query params err %v", "payermax.WithdrawNotify", err)
		c.String(http.StatusOK, "")
		return
	}
	log.Debug("payermax.WithdrawNotify resp ==> %+v", resp)

	// 验证签名
	if public.VerifyRsaSignBySHA256(string(bodyData), sign, config.Server.PayerMax.PLAT_PUBLIC_KEY) != nil {
		log.Error("payermax.WithdrawNotify 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("payermax.WithdrawNotify 签名成功")

	// 返回码，’APPLY_SUCCESS’代表成功
	if resp.Code != "APPLY_SUCCESS" {
		log.Error("payermax.WithdrawNotify req ==> %+v", resp)
		c.String(http.StatusOK, "")
		return
	}

	// 交易状态，SUCCESS成功, FAILD失败 ，PENDING进行中，BOUNCEBACK退票
	// 0=Failure 1=Success  2=Pending(Success)  3=BOUNCEBACK退票
	status := 0
	switch resp.Data.Status {
	case "FAILD": // 0=失败
		status = 0
	case "SUCCESS": // 1=成功
		status = 1
	case "PENDING": // 2=进行中
		status = 2
	case "BOUNCEBACK": // 3=退票
		status = 3
	default:
		status = 2
	}

	obj := db.NewPayerMaxWithdrawNotify()
	obj.In.OrderID = resp.Data.OutTradeNo
	obj.In.DfTransactionId = resp.Data.TradeNo
	obj.In.Status = status
	obj.In.DfDesc = fmt.Sprintf("%s，Fee: %s", strings.ReplaceAll(resp.Data.Status, "'", ""), resp.Data.Source.Fee)
	obj.In.Balance = 0
	obj.DoAction()

	log.Debug("payermax.WithdrawNotify obj.In=%+v obj.Out=%+v", obj.In, obj.Out)

	// 提现失败,退款
	// if resp.Status == WITHDRAW_FAIL {
	// 	go coreClient.AddNotification(obj.Out.UserID, notification.Notification_Gold, "")
	// }

	c.JSON(http.StatusOK, ret{
		Code: "SUCCESS",
		Msg:  "Success",
	})
	return
}
