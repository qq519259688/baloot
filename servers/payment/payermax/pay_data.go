package payermax

type ret struct {
	Code string `json:"code"` // 响应 ‘SUCCESS’
	Msg  string `json:"msg"`  // 响应 ‘Success’
}

type (
	pay_req struct {
		Version       string     `json:"version,omitempty"`       // 接口版本。当前值为：1.0
		KeyVersion    string     `json:"keyVersion,omitempty"`    // 密钥版本。当前值为：1
		RequestTime   string     `json:"requestTime,omitempty"`   // 请求时间，符合rfc3339规范，格式：yyyy-MM-dd’T’HH:mm:ss.SSSXXX
		MerchantAppId string     `json:"merchantAppId,omitempty"` // 商户应用Id，PayerMax分配给商户应用的唯一标识
		MerchantNo    string     `json:"merchantNo,omitempty"`    // 商户号，商户与PayerMax业务签约时生成的唯一标识
		Data          payReqData `json:"data,omitempty"`          // 请求数据体
	}

	payReqData struct {
		OutTradeNo       string             `json:"outTradeNo,omitempty"`    // 商户订单号，唯一标识商户的一笔交易，不能重复，只能包含字母、数字、下划线。
		Subject          string             `json:"subject,omitempty"`       // 订单标题
		TotalAmount      string             `json:"totalAmount,omitempty"`   // 商户传入的订单金额，金额的单位为元。除以下国家外按照各国币种支持的小数点位上送。注意：巴林、科威特、伊拉克，约旦、突尼斯、利比亚、奥马尔地区，本币只支持两位小数；		印尼、中国台湾、巴基斯坦、哥伦比亚地区，本币不支持带小数金额。
		Currency         string             `json:"currency,omitempty"`      // 币种代码，大写字母，参见【交易支持国家/地区与币种】
		Country          string             `json:"country,omitempty"`       // 国家代码，大写字母，如果所传的国家代码与币种不匹配，则以货币代码对应的地区展示收银台。如指定了支付方式，则国家必须上送，参见【交易支持国家/地区与币种】
		UserId           string             `json:"userId,omitempty"`        // 商户内部的用户Id，需要保证每个ID唯一性
		PaymentDetail    *paymentDetailData `json:"paymentDetail,omitempty"` // 支付信息，非必填	1、支持仅指定支付方式，收银台会拉取该支付方式支持的所有目标机构  2、支持指定支付方式+目标机构
		CardInfo         *cardInfoData      `json:"cardInfo,omitempty"`
		FrontCallbackUrl string             `json:"frontCallbackUrl,omitempty"` // 商户指定的跳转URL，用户完成支付后会被跳转到该地址，以http/https开头或者商户应用的scheme地址（目前仅支持Andriod）
		NotifyUrl        string             `json:"notifyUrl,omitempty"`        // 服务端回调通知URL，以http/https开头可以通过MerchantDashboard平台配置商户通知地址，如果交易中上送，则以交易为准
	}

	paymentDetailData struct {
		PaymentTokenID string `json:"paymentTokenID,omitempty"` // 卡token支付时，此字段为必填
		PaymentMethod  string `json:"paymentMethod,omitempty"`  // 支付方式，可以为空，参见【收银台支付-支付方式列表】
		TargetOrg      string `json:"targetOrg,omitempty"`      // 目标机构，可以为空，如果指定目标机构，则支付方式也必须指定。
	}

	cardInfoData struct {
		CardOrg     string `json:"cardOrg,omitempty"`
		CardOrgType string `json:"cardOrgType,omitempty"`
	}

	pay_resp struct {
		Code string      `json:"code,omitempty"` // 返回码，’APPLY_SUCCESS’代表成功
		Msg  string      `json:"msg,omitempty"`  // 返回描述，’Success.’
		Data payRespData `json:"data,omitempty"` // 返回数据体
	}

	payRespData struct {
		OutTradeNo  string `json:"outTradeNo,omitempty"`  // 商户订单号
		TradeToken  string `json:"tradeToken,omitempty"`  // PayerMax流水号
		Status      string `json:"status,omitempty"`      // 交易状态
		RedirectUrl string `json:"redirectUrl,omitempty"` // 跳转地址
	}
)

type (
	payNotify struct {
		Code          string        `json:"code,omitempty"`          // 返回码，’APPLY_SUCCESS’代表成功
		Msg           string        `json:"msg,omitempty"`           // 返回描述，’Success.’
		KeyVersion    string        `json:"keyVersion,omitempty"`    // 密钥版本。当前值为：1
		MerchantAppId string        `json:"merchantAppId,omitempty"` // 商户app id
		MerchantNo    string        `json:"merchantNo,omitempty"`    // 商户Id
		NotifyTime    string        `json:"notifyTime,omitempty"`    // 通知时间，符合rfc3339规范，格式：yyyy-MM-dd’T’HH:mm:ss.SSSXXX
		NotifyType    string        `json:"notifyType,omitempty"`    // 通知类型 PAYMENT
		Data          payNotifyData `json:"data,omitempty"`
	}

	payNotifyData struct {
		OutTradeNo  string `json:"outTradeNo,omitempty"`  // 商户订单号
		TradeToken  string `json:"tradeToken,omitempty"`  // PayerMax交易流水号
		TotalAmount int    `json:"totalAmount,omitempty"` // 商户传入的订单金额，金额的单位为元
		Currency    string `json:"currency,omitempty"`    // 订单币种
		Status      string `json:"status,omitempty"`      // 交易状态
	}
)

type payReturn struct {
	OutTradeNo string `json:"outTradeNo" form:"outTradeNo"`
	Status     string `json:"status" form:"status"`
	TradeToken string `json:"tradeToken" form:"tradeToken"`
}
