package payermax

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"

	"bet24.com/log"
	"bet24.com/public"
	"bet24.com/servers/common"
	coreClient "bet24.com/servers/coreservice/client"
	"bet24.com/servers/payment/config"
	"bet24.com/servers/payment/db"
	"bet24.com/servers/payment/shop"
	"github.com/gin-gonic/gin"
)

// 下单
func PayOrder(c *gin.Context) {
	obj := db.NewOrder(db.SP_PayerMax_ORDER)
	if err := c.ShouldBind(&obj.In); err != nil {
		log.Debug("%s query params err %v", "payermax.PayOrder", err)
		c.String(http.StatusOK, "")
		return
	}

	obj.In.IpAddress = strings.Split(c.Request.RemoteAddr, ":")[0]

	//// 币种为空,根据ip获取
	//if obj.In.Currency == "" {
	//	currency := shop.GetCurrencyRateByIp(obj.In.UserID, obj.In.IpAddress)
	//	obj.In.Currency = currency
	//}

	// 获取产品信息
	item := shop.GetProduct(obj.In.ProductID)
	if item == nil {
		log.Error("%s query GetProduct productId=%s currency=%s is nil", "payermax.PayOrder", obj.In.ProductID, obj.In.Currency)
		c.String(http.StatusOK, "")
		return
	}

	//// 获取当前汇率信息
	//info := shop.GetExchangeRate(obj.In.Currency)
	//if info == nil {
	//	log.Error("%s query GetExchangeRate obj.In.Currency=%s is nil", "payermax.PayOrder", obj.In.Currency)
	//	c.String(http.StatusOK, "")
	//	return
	//}
	//
	//// 计算价格
	//calPrice := info.Rate * item.Price
	//
	//// 检查价格是否篡改
	//if calPrice != obj.In.Price {
	//	log.Error("%s obj.In.Price=%v info.Rate=%v calPrice=%v is invalid", "payermax.PayOrder", obj.In.Price, info.Rate, calPrice)
	//	c.String(http.StatusOK, "")
	//	return
	//}

	// 检查价格是否篡改
	if item.Price != obj.In.Price {
		log.Error("%s obj.In.Price=%v calPrice=%v is invalid", "payermax.PayOrder", obj.In.Price, item.Price)
		c.String(http.StatusOK, "")
		return
	}

	obj.DoAction(nil)
	if obj.Out.OrderID == "" {
		log.Debug("%s GenOrder fail obj.In=%+v", "payermax.PayOrder", obj.In)
		c.String(http.StatusOK, "")
		return
	}

	// 请求payOrder的代码
	req := pay_req{
		Version:       "1.0",
		KeyVersion:    "1",
		RequestTime:   common.GetNowTime().Format(dateFormat),
		MerchantAppId: config.Server.PayerMax.MerchantAppId,
		MerchantNo:    config.Server.PayerMax.MerchantNo,
		Data: payReqData{
			OutTradeNo:       obj.Out.OrderID,
			Subject:          obj.In.ProductID,
			TotalAmount:      fmt.Sprintf("%d", int(obj.In.Price)),
			Currency:         obj.In.Currency,
			Country:          obj.In.Country,
			UserId:           strconv.Itoa(obj.In.UserID),
			FrontCallbackUrl: config.Server.PayerMax.FrontCallbackUrl,
			NotifyUrl:        config.Server.PayerMax.Url_pay_Notify,
		},
	}

	// 生成签名
	checkContent, err := json.Marshal(req)
	if err != nil {
		log.Error("payermax.PayOrder json marshal fail %v", err)
		return
	}

	sign := public.Sha256WithRsa(string(checkContent), config.Server.PayerMax.MCH_PRIVATE_KEY)
	log.Debug("payermax.payOrder checkContent=%s sign=%s", string(checkContent), sign)

	// POST请求
	respBody := httpPostByJson(config.Server.PayerMax.Url_pay_order, string(checkContent), sign)
	log.Debug("payermax.payOrder req ==> %+v  resp ==> %+v", req, respBody)

	var resp pay_resp
	if err := json.Unmarshal([]byte(respBody), &resp); err != nil {
		log.Error("payermax.payOrder json unmarshal req ==> %+v resp ==> %+v fail %v", req, respBody, err)
		return
	}

	log.Debug("payermax.payOrder resp ==> %+v", resp)

	// 返回码，’APPLY_SUCCESS’代表成功
	if resp.Code != "APPLY_SUCCESS" {
		log.Error("payermax.payOrder post return resp fail ==> %+v", resp)
		return
	}

	// 跳转到支付页面，以便持卡人完成支付过程
	c.Redirect(http.StatusMovedPermanently, resp.Data.RedirectUrl)

	// c.String(http.StatusOK, "Success")
	return
}

// 回调通知
func PayNotify(c *gin.Context) {
	bodyData, _ := io.ReadAll(c.Request.Body)
	log.Debug("payermax.PayNotify ==> ctx.Request.body: %v", string(bodyData))

	// 获取签名
	sign := c.Request.Header.Get("sign")
	log.Debug("payermax.PayNotify sign=%s", sign)

	var resp payNotify
	if err := json.Unmarshal(bodyData, &resp); err != nil {
		log.Debug("%s query params err %v", "payermax.PayNotify", err)
		c.String(http.StatusOK, "")
		return
	}
	log.Debug("payermax.PayNotify resp ==> %+v", resp)

	// 验证签名
	if public.VerifyRsaSignBySHA256(string(bodyData), sign, config.Server.PayerMax.PLAT_PUBLIC_KEY) != nil {
		log.Error("payermax.PayNotify 签名失败 ==> %+v", resp)
		return
	}

	log.Debug("payermax.PayNotify 签名成功")

	// 返回码，’APPLY_SUCCESS’代表成功
	if resp.Code != "APPLY_SUCCESS" {
		log.Error("payermax.PayNotify resp ==> %+v 失败", resp)
		c.JSON(http.StatusOK, ret{
			Code: "SUCCESS",
			Msg:  "Success",
		})
		return
	}

	// 数据库操作
	obj := db.NewNotify(db.SP_PayerMax_NOTIFY)
	obj.In.OrderID = resp.Data.OutTradeNo
	obj.In.TradeID = resp.Data.TradeToken
	// obj.In.Price = resp.Data.TotalAmount
	obj.DoAction(nil)

	// 操作成功,给道具
	if obj.Out.RetCode == 1 {
		// 充值
		resp := coreClient.Recharge(obj.Out.UserID, obj.Out.ProductID)
		log.Debug("%s 充值成功 %+v", "payermax.PayNotify", resp)
	}

	c.JSON(http.StatusOK, ret{
		Code: "SUCCESS",
		Msg:  "Success",
	})
	return
}

// 支付完成跳转处理
func PayCallback(c *gin.Context) {
	var resp payReturn
	if err := c.ShouldBind(&resp); err != nil {
		log.Debug("%s query params err %v", "payermax.PayCallback", err)
		c.String(http.StatusOK, "")
		return
	}
	log.Debug("payermax.PayCallback ==> %+v", resp)

	c.HTML(http.StatusOK, "payermax.html", gin.H{
		"title":      "PayerMax Recharge Result",
		"outTradeNo": resp.OutTradeNo,
		"tradeToken": resp.TradeToken,
		"status":     resp.Status,
	})
	return
}
