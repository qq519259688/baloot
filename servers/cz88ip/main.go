package cz88ip

type IpInfo struct {
	Country    string // 国家
	Region     string // 省，市，区
	LastUpdate int64  // 上次刷新时间
}

func GetCountryAndRegion(ipAddress string) (country string, region string) {
	ipInfo := GetIpManager().getIpInfo(ipAddress)
	if ipInfo == nil {
		return
	}
	country = ipInfo.Country
	region = ipInfo.Region
	return
}
