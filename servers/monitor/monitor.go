package monitor

import (
	"bet24.com/log"
	"expvar"
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"os"
	"path/filepath"
	"runtime/pprof"
)

func metricsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	first := true
	report := func(key string, value interface{}) {
		if !first {
			fmt.Fprintf(w, ",\n")
		}
		first = false
		if str, ok := value.(string); ok {
			fmt.Fprintf(w, "%q: %q", key, str)
		} else {
			fmt.Fprintf(w, "%q: %v", key, value)
		}
	}

	fmt.Fprintf(w, "{\n")
	expvar.Do(func(kv expvar.KeyValue) {
		report(kv.Key, kv.Value)
	})
	fmt.Fprintf(w, "\n}\n")
}

func Run(port int, logPath string) {
	go func() {
		if logPath != "" {
			p, _ := filepath.Abs(filepath.Dir(logPath + "\\"))
			http.Handle("/logs/", http.StripPrefix("/logs/", http.FileServer(http.Dir(p))))
		}
		http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	}()
}

func logCPU(port int) {
	// 如果文件夹不存在则创建
	os.MkdirAll("./tmp", 0777)
	f, err := os.OpenFile(fmt.Sprintf("./tmp/cpu_%d.prof", port), os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Release("%v", err)
	}
	defer f.Close()
	pprof.StartCPUProfile(f)
	defer pprof.StopCPUProfile()
}

func logMem(port int) {
	os.MkdirAll("./tmp", 0777)
	fm, err := os.OpenFile(fmt.Sprintf("./tmp/mem_%d.out", port), os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Release("%v", err)
	}
	pprof.WriteHeapProfile(fm)
	fm.Close()
}
