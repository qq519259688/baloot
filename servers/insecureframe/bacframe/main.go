package bacframe

import (
	"bet24.com/log"
	"bet24.com/servers/insecureframe"
	"bet24.com/servers/insecureframe/gate"
	"bet24.com/servers/insecureframe/robot"
	"strconv"
)

var gameFrame *GameFrame

func RunFrame(gameSink GameSink, redisUrl, redisPsw string, redisDb int) BacFrame {
	return run(gameSink, redisUrl, redisPsw, redisDb, "")
}

func RunFrameWithLogServer(gameSink GameSink, redisUrl, redisPsw string, redisDb int, logPath string) *GameFrame {
	return run(gameSink, redisUrl, redisPsw, redisDb, logPath)
}

func run(gameSink GameSink, redisUrl, redisPsw string, redisDb int, logPath string) *GameFrame {
	gameFrame = NewGameFrame(gameSink)
	insecureframe.Run(gameFrame, redisUrl, redisPsw, redisDb, logPath)
	log.Release("bacframe running")
	return gameFrame
}

func Dump(cmd, param1, param2 string) bool {
	switch cmd {
	case "user":
		log.Debug("----------bacframe.Dump user ----------")
		userId, _ := strconv.Atoi(param1)
		usr := gate.GetUserByUserId(userId)
		if usr == nil {
			log.Debug("    userId %d not exist", userId)
		} else {
			log.Debug("    %s", usr.DumpUserInfo())
		}
		log.Debug("++++++++++bacframe.Dump user ++++++++++")
	case "robot":
		robot.Dump()
	case "frame":
		getFrameManager().dump(param1, param2)
	default:
		return false
	}
	return true
}

func StopServer() {
	gate.StopServer()
}
