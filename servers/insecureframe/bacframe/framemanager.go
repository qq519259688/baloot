package bacframe

import (
	"strconv"
	"sync"

	"bet24.com/log"
)

type frame_manager struct {
	lock      *sync.RWMutex
	frameList map[int]*GameFrame
}

var frameMgr *frame_manager

func getFrameManager() *frame_manager {
	if frameMgr == nil {
		frameMgr = new(frame_manager)
		frameMgr.ctor()
	}
	return frameMgr
}

func (fm *frame_manager) ctor() {
	fm.lock = &sync.RWMutex{}
	fm.frameList = make(map[int]*GameFrame)
}

func (fm *frame_manager) getGameFrame(index int) *GameFrame {
	fm.lock.RLock()
	gf := fm.frameList[index]
	fm.lock.RUnlock()
	if gf == nil {
		log.Debug("frame_manager.getFrame[%d] not found", index)
		return nil
	}
	return gf
}

func (fm *frame_manager) dump(param1, param2 string) {
	log.Release("----------frame_manager.dump %s.%s ----------", param1, param2)
	defer func() {
		log.Release("++++++++++frame_manager.dump++++++++++")
	}()
	if param1 == "" {
		fm.lock.RLock()
		log.Release("  frame count:%d", len(fm.frameList))
		for k := range fm.frameList {
			log.Release("    %d", k)
		}
		fm.lock.RUnlock()
		return
	}

	tableId, err := strconv.Atoi(param1)
	if err != nil {
		log.Release("  invalid argument %v", err)
		return
	}

	gf := fm.getGameFrame(tableId)
	if gf == nil {
		return
	}

	gf.dump(param2)
}
