package bacframe

import (
	"bet24.com/servers/user"
)

type GameSink interface {
	GetServerPort() int
	GetGameID() int
	GetGameName() string
	GetRoomName() string
	OnUserEnter(userIndex int32)
	OnUserExit(userIndex int32)
	OnGetGameScene() string
	OnGameMessage(userIndex int32, userID int, msg, data string) bool
	GetCertFile() string
	GetKeyFile() string
	//机器人配置
	GetRobotCount() int
	GetRobotGoldLimit() (min, max int)
	GetRobotOnlineSec() int
	IsChipRoom() bool
	OnPlatformConfig(key string)
}

type BacFrame interface {
	// 刷新场景 userIndex == -1表示所有人
	NotifySceneChanged(userIndex int32)
	KickUser(userIndex int32) bool

	// 如果userIndex == -1 则表示广播给所有人
	SendGameData(userIndex int32, msg, data string)
	GetUser(userIndex int32) *user.UserInfo
	GetUserByUserId(userId int) *user.UserInfo
	WriteUserMoney(userId int, amount, tax int, status, scoreType int, sourceName string) (bool, int)
	// 写分并返回实际修改数量
	WriteUserMoneyWithModifyAmount(userId int, amount, tax int, status, scoreType int, sourceName string) int
	WriteBetRecord(userId int, betAmount int, winAmount int, tax int, winRate float64, betDesc string, resultDesc string, roomName string)

	GetUserList() []*user.UserInfo

	GetVirtualUserCount(userCount int, roomType int) int

	SendBroadcast(userId int, userName string, score int)

	// 获取用户的金币或筹码，根据房间类型自动返回
	GetUserChipOrGold(userIndex int32) int
	GetUserChipOrGoldByUserId(userId int) int
	GetUserChipOrGoldByUser(usr *user.UserInfo) int

	// 更新房间列表
	UpdateRoomList(roomName, addr string)
}
type GameSink_LadderRoom interface {
	IsLadderRoom() bool
	GetMinGold(roomName string) int
	GetMaxGold(roomName string) int
	GetAdditionalPercent(roomName string) int
	GetBaseScore(roomName string) int
}