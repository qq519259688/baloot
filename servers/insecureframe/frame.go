package insecureframe

import (
	"bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/insecureframe/gate"
	"bet24.com/servers/insecureframe/robot"
	"bet24.com/servers/transaction"
	"bet24.com/servers/user"
	"math/rand"
	"time"
)

func Run(sink gate.GateSink,
	redisUrl, redisPsw string, redisDb int, logPath string) {
	log.Debug("insecureframe.Run")
	rand.Seed(time.Now().UnixNano())
	if redisUrl != "" {
		redis.InitPool(redisUrl, redisPsw, redisDb)
	}
	transaction.Run()

	gate.Run(sink, logPath)

	go redis.Subscribe(gate.RecvChannelData)

	if sink.GetRobotCount() > 0 {
		robot.RunRobot(sink, gate.GetGate())
	}
}

func GetUserByUserId(userId int) *user.UserInfo {
	return gate.GetUserByUserId(userId)
}

func WriteUserMoney(userId int, amount, tax int, status, scoreType int, sourceName string, isChip bool) (bool, int) {
	return gate.WriteUserMoney(userId, amount, tax, status, scoreType, sourceName, isChip)
}
