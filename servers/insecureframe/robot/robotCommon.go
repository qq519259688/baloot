package robot

import (
	"bet24.com/log"
	"bet24.com/servers/insecureframe/gate"
)

func RunRobot(s gate.GateSink, g *gate.Gate) {
	robotMMgr = NewRobotManager(s, g)
}

func IsRunning() bool {
	return robotMMgr != nil
}

func GetOneRobotEnterTable(tableID, min, max int) bool {
	if robotMMgr == nil {
		return false
	}
	return robotMMgr.getOneRobotEnterTable(tableID, min, max, false, false)
}

func GetOneRobotEnterTableAndReady(tableID, min, max int) bool {
	if robotMMgr == nil {
		return false
	}
	return robotMMgr.getOneRobotEnterTable(tableID, min, max, false, true)
}

func RobotOutTable(userIndex int32, tableID int) {
	if robotMMgr == nil {
		return
	}
	robotMMgr.robotOutTable(userIndex, tableID)
}

func OneRobotLogin() bool {
	if robotMMgr == nil {
		return false
	}
	return robotMMgr.oneRobotLogin()
}

func OneRobotLogout(userIndex int32) {
	if robotMMgr == nil {
		return
	}
	robotMMgr.oneRobotLogout(userIndex)
}

func Exit() {
	if robotMMgr == nil {
		return
	}
	robotMMgr.Exit()
}

func Dump() {
	if robotMMgr == nil {
		log.Debug("Robot Manager not running")
		return
	}
	robotMMgr.Dump()
}

func UserExit(userIndex int32) {
	if robotMMgr == nil {
		return
	}
	robotMMgr.userExit(userIndex)
}

func IsRetired(userIndex int32) bool {
	if robotMMgr == nil {
		return false
	}
	return robotMMgr.isRetired(userIndex)
}
