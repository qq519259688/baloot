package frame

import (
	"bet24.com/log"
	"bet24.com/servers/insecureframe/message"
	privateroom "bet24.com/servers/micros/privateroom/proto"
	"bet24.com/servers/user"
	"context"
	"encoding/json"
	"time"
)

const (
	dismiss_none = iota
	dismiss_yes
	dismiss_no
)

const dimiss_wait_sec = 120

func (t *ThreadsafeTable) sendMessageToMainThread(msg *threadsafe_message) {
	if t.stopped {
		if msg.Callback != nil {
			msg.Callback <- 1
		}
		log.Release("sendMessageToMainThread tableId[%d] [%s] stopped", t.tableId, msg.Msg)
		return
	}
	for {
		c, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		select {
		case t.messageChan <- msg:
			cancel()
			return
		case <-c.Done():
			log.Release("sendMessageToMainThread tableId[%d] [%s] [%s]timeout", t.tableId, msg.Msg, msg.Data)
			//gameFrame.dump()
			if msg.Callback != nil {
				msg.Callback <- 1
			}
			cancel()
			return
		}
	}
}

func (t *ThreadsafeTable) onPrivateRoomStatusChanged(old, new int) {
	data := msg_statusChanged{OldStatus: old, NewStatus: new}
	d, _ := json.Marshal(data)
	callBack := make(chan byte)
	msg := &threadsafe_message{Msg: "privateRoomStatusChanged", Data: string(d), Callback: callBack}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
	<-callBack
}

func (t *ThreadsafeTable) onPrivateRoomDismissed() {
	callBack := make(chan byte)
	msg := &threadsafe_message{Msg: "privateRoomDismissed", Data: "", Callback: callBack}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
	<-callBack
}

func (t *ThreadsafeTable) onPrivateRoomRequestDismiss(usr *user.UserInfo, msg, data string) bool {
	if t.isPrivateRoomDismissing() {
		log.Release("ThreadsafeTable.onPrivateRoomRequestDismiss dismissing")
		return false
	}
	chairId := usr.GetUserChairId()
	if chairId < 0 || chairId >= len(t.dismissRequest) {
		log.Release("ThreadsafeTable.onPrivateRoomRequestDismiss user chair invalid %d", chairId)
		return false
	}

	if !t.IsPlaying() {
		if usr.GetUserId() == t.owner {
			go privateroom.DismissPrivateRoom(t.roomNo)
			return true
		}
		log.Release("ThreadsafeTable.onPrivateRoomRequestDismiss not playing and not owner")
		return false
	}

	t.dismissRequest[chairId] = dismiss_yes
	t.broadcastPrivateRoomDismissStatus()
	if t.dismissTimer != nil {
		t.dismissTimer.Stop()
	}
	t.dismissTimer = time.AfterFunc(time.Second*dimiss_wait_sec, t.onDimissTimout)
	return true
}

func (t *ThreadsafeTable) onPrivateRoomDismissResp(usr *user.UserInfo, msg, data string) bool {
	if !t.isPrivateRoomDismissing() {
		log.Release("ThreadsafeTable.onPrivateRoomDismissResp not dismissing")
		return false
	}
	chairId := usr.GetUserChairId()
	if chairId < 0 || chairId >= len(t.dismissRequest) {
		log.Release("ThreadsafeTable.onPrivateRoomDismissResp user chair invalid %d", chairId)
		return false
	}
	if t.dismissRequest[chairId] != dismiss_none {
		log.Release("ThreadsafeTable.onPrivateRoomDismissResp user already responsed")
		return false
	}
	if data == "1" {
		t.dismissRequest[chairId] = dismiss_yes
	} else {
		t.dismissRequest[chairId] = dismiss_no
	}
	t.broadcastPrivateRoomDismissStatus()
	// 检查是否足够解散了
	t.checkPrivateRoomDismiss()
	return true
}

func (t *ThreadsafeTable) onDimissTimout() {
	for i := 0; i < len(t.dismissRequest); i++ {
		if t.dismissRequest[i] == dismiss_none {
			t.dismissRequest[i] = dismiss_yes
		}
	}

	t.checkPrivateRoomDismiss()
}

func (t *ThreadsafeTable) checkPrivateRoomDismiss() {
	yesCount := 0
	noCount := 0
	userCount := t.getPlayerCount()

	for _, v := range t.dismissRequest {
		if v == dismiss_yes {
			yesCount++
		}

		if v == dismiss_no {
			noCount++
		}
	}

	if yesCount == userCount {
		go privateroom.DismissPrivateRoom(t.roomNo)
		t.resetPrivateRoomDismiss()
		return
	}

	if noCount > 0 {
		t.resetPrivateRoomDismiss()
		return
	}
}

func (t *ThreadsafeTable) broadcastPrivateRoomDismissStatus() {
	d, _ := json.Marshal(t.dismissRequest)
	t.broadcastData(message.Frame_PrivateRoom_DimiissStatus, string(d))
}

func (t *ThreadsafeTable) isPrivateRoomDismissing() bool {
	for _, v := range t.dismissRequest {
		if v != dismiss_none {
			return true
		}
	}
	return false
}

func (t *ThreadsafeTable) resetPrivateRoomDismiss() {
	t.dismissTimer.Stop()
	t.dismissRequest = make([]int, t.tableSink.OnGetChairCount())
}

func (t *ThreadsafeTable) getPrivateRoomDismissYesCount() int {
	count := 0
	for _, v := range t.dismissRequest {
		if v != dismiss_yes {
			count++
		}
	}
	return count
}
