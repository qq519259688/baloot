package frame

import (
	"bet24.com/log"
	"bet24.com/servers/insecureframe/gate"
	"bet24.com/servers/insecureframe/message"
	"bet24.com/servers/user"
	"encoding/json"
)

func (t *ThreadsafeTable) sendData(userIndex int32, msg, data string) bool {
	if msg == message.Table_GameScene && data == "" {
		log.Debug("ThreadsafeTable sendData 空场景不发")
		return true
	}
	return gate.SendMessage(userIndex, msg, data)
}

func (t *ThreadsafeTable) broadcastData(msg, data string) {
	//log.Debug("ThreadsafeTable broadcastData[%d] %s", t.tableId, msg)
	for i := 0; i < t.chairCount; i++ {
		if t.users[i] == -1 {
			continue
		}
		t.sendData(t.users[i], msg, data)
	}

	for _, v := range t.watchUsers {
		t.sendData(v.UserIndex, msg, data)
	}
}

func (t *ThreadsafeTable) sendUserEnter(usr *user.UserInfo, toUser int32) {
	if usr == nil {
		log.Debug("ThreadsafeTable.sendUserEnter usr == nil")
		return
	}
	info := usr.GetUserInfo_Table(toUser == usr.GetUserIndex())
	d, _ := json.Marshal(info)
	gate.SendMessage(toUser, message.Table_UserEnter, string(d))
}

func (t *ThreadsafeTable) sendUserExit(userId, chairId int, toUser int32, toWatch bool) {
	info := message.TableUserExit{UserId: userId, ChairId: chairId, ToWatch: toWatch}
	d, _ := json.Marshal(info)

	gate.SendMessage(toUser, message.Table_UserExit, string(d))
}

func (t *ThreadsafeTable) broadcastUserExit(userId, chairId int, toWatch bool) {
	// 发其他人
	for i := 0; i < t.chairCount; i++ {
		if t.users[i] == -1 {
			continue
		}
		go t.sendUserExit(userId, chairId, t.users[i], toWatch)
	}

}

func (t *ThreadsafeTable) sendGameScene(chairId int) bool {
	for _, v := range t.watchUsers {
		if v.ChairId != chairId {
			continue
		}
		if t.isPrivate() && v.UserId == t.owner {
			t.sendData(v.UserIndex, message.Table_GameScene,
				t.tableSink_privateRoom.OnGetPrivateRoomScene(chairId))
		} else {
			t.sendData(v.UserIndex, message.Table_GameScene,
				t.tableSink.OnGetChairScene(chairId, false))
		}

	}

	usr := t.GetUserByChair(chairId)
	if usr == nil {
		return false
	}
	userStatus := usr.GetUserStatus()
	isPlayer := (userStatus == user.UserStatus_Play || userStatus == user.UserStatus_Ready)
	t.sendData(usr.GetUserIndex(), message.Table_GameScene,
		t.tableSink.OnGetChairScene(chairId, isPlayer))
	return true
}

func (t *ThreadsafeTable) sendGameSceneToUser(userIndex int32) {
	usr, isPlayer := t.GetUser(userIndex)
	if usr == nil {
		log.Debug("ThreadsafeTable.sendGameSceneToUser user not exist %d", userIndex)
		return
	}
	chairId := usr.GetUserChairId()
	userStatus := usr.GetUserStatus()
	log.Debug("ThreadsafeTable.sendGameSceneToUser userId[%d],status[%d],owner[%d]", usr.GetUserId(), userStatus, t.owner)

	if userStatus == user.UserStatus_Watch && usr.GetUserId() == t.owner {
		t.sendData(userIndex, message.Table_GameScene,
			t.tableSink_privateRoom.OnGetPrivateRoomScene(chairId))
	} else {
		t.sendData(userIndex, message.Table_GameScene,
			t.tableSink.OnGetChairScene(chairId, isPlayer))
	}
}

func (t *ThreadsafeTable) broadcastScene() {
	for i := 0; i < t.chairCount; i++ {
		if t.users[i] == -1 {
			continue
		}
		usr := t.getPlayer(t.users[i])
		if usr == nil {
			continue
		}
		if usr.GetUserStatus() < user.UserStatus_Sit {
			continue
		}
		userStatus := usr.GetUserStatus()
		isPlayer := (userStatus == user.UserStatus_Play || userStatus == user.UserStatus_Ready)
		t.sendData(t.users[i], message.Table_GameScene,
			t.tableSink.OnGetChairScene(i, isPlayer))
	}

	for _, v := range t.watchUsers {
		if t.isPrivate() && v.UserId == t.owner {
			t.sendData(v.UserIndex, message.Table_GameScene,
				t.tableSink_privateRoom.OnGetPrivateRoomScene(v.ChairId))
		} else {
			t.sendData(v.UserIndex, message.Table_GameScene,
				t.tableSink.OnGetChairScene(v.ChairId, false))
		}
	}
}
