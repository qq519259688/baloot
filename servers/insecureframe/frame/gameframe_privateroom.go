package frame

import (
	"bet24.com/log"
	"bet24.com/servers/insecureframe/gate"
	"bet24.com/servers/insecureframe/message"
	privateroom "bet24.com/servers/micros/privateroom/proto"
	robot_query "bet24.com/servers/micros/userservices/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
	"encoding/json"
	"time"
)

// IPrivateRoomSink
func (f *GameFrame) CreatePrivateRoom(creator int, ruleName string, target int, userCount int, playTimeout int) int {
	// 先创建一个table
	table := f.createPrivateTable(creator, ruleName, target, userCount, playTimeout)
	// 去coreservice判断是否能创建
	roomNo, errMsg := privateroom.CreatePrivateRoomByGameServer(creator,
		f.gameSink.GetGameID(),
		f.gameSink.GetGameName(),
		f.gameSink.GetServerAddr(),
		table.GetTableID())

	log.Release("GameFrame.createPrivateRoom %d roomNo:%d,errMsg:%s", creator, roomNo, errMsg)

	// 如果失败
	if roomNo <= 0 {
		// 解散桌子
		f.removeTable(table.GetTableID())
		return roomNo
	}

	table.setRoomNo(roomNo)
	return roomNo
}

func (f *GameFrame) getPrivateTable(roomNo int) *ThreadsafeTable {
	if roomNo <= 0 {
		return nil
	}
	f.lock_table.RLock()
	defer f.lock_table.RUnlock()
	for k, v := range f.tables {
		if v.roomNo != roomNo {
			continue

		}
		if v.stopped {
			log.Release("GameFrame.getPrivateTable [%d] stopped", v.tableId)
			return nil
		}
		return f.tables[k]
	}
	return nil
}

func (f *GameFrame) OnRoomStatusChanged(roomNo int, oldStatus, newStatus int) {
	// 找到房间
	t := f.getPrivateTable(roomNo)
	if t == nil {
		log.Release("GameFrame.OnRoomStatusChanged failed roomNo[%d] not exist", roomNo)
		return
	}
	t.onPrivateRoomStatusChanged(oldStatus, newStatus)
}

func (f *GameFrame) OnRoomDismissed(roomNo int) {
	// 找到房间
	t := f.getPrivateTable(roomNo)
	if t == nil {
		log.Release("GameFrame.OnRoomDismissed failed roomNo[%d] not exist", roomNo)
		return
	}
	t.onPrivateRoomDismissed()
	// 百人场可能需要等待一段时间后才删除
	if gameFrame.gameSink.GetChairCount() < 2 {
		time.Sleep(2 * time.Minute)
	}
	f.removeTable(t.tableId)
}

func (f *GameFrame) recvPrivateRoomRequestDismiss(userIndex int32, msg, data string) bool {
	if !f.gameSink.IsPrivateRoom() {
		return false
	}
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("recvPrivateRoomRequestDismiss user not exist,%d,%s", userIndex, msg)
		return false
	}

	table := f.getTable(usr.GetUserTableId())
	if table == nil {
		log.Debug("recvPrivateRoomRequestDismiss user not in table,%d:%d,%s", userIndex, usr.GetUserId(), msg)
		return false
	}
	return table.onPrivateRoomRequestDismiss(usr, msg, data)
}

func (f *GameFrame) ForceEnterUser(userId int, roomNo int, chairId int) {
	// 找到房间
	t := f.getPrivateTable(roomNo)
	if t == nil {
		log.Release("GameFrame.ForceEnterUser failed roomNo[%d] not exist", roomNo)
		return
	}
	// 已经在了？
	usr := gate.GetUserByUserId(userId)
	if usr != nil {
		if usr.GetUserTableId() == t.tableId && usr.GetUserChairId() == chairId {
			log.Release("GameFrame.ForceEnterUser UserId[%d] RoomNo[%d] user already sit", userId, roomNo)
			return
		}
		log.Release("GameFrame.ForceEnterUser UserId[%d] RoomNo[%d] [%d.%d] kicking exist user[%d.%d]",
			userId, roomNo, t.tableId, chairId, usr.GetUserTableId(), usr.GetUserChairId())
		gate.KickUser(usr.GetUserIndex())
	}
	// 把玩家登录进来
	usr = gate.LoginAUser(userId)

	if usr == nil {
		log.Release("GameFrame.ForceEnterUser LoginAUser failed UserId = %d roomNo = %d", userId, roomNo)
		return
	}

	t.AddTableUser_sync(usr.GetUserIndex(), chairId, false, false)
	// 如果加入失败
	if usr.GetUserTableId() != t.tableId {
		log.Release("GameFrame.ForceEnterUser %d to %d failed", userId, roomNo)
		gate.SilentRemoveUser(userId)
		return
	}

	if robot_query.IsRobot(userId) {
		usr.SetRobot()
	}

	// 举手
	t.setUserReadyStatus_sync(usr.GetUserIndex(), true)
	f.addOfflineUser(usr)
	gate.SilentRemoveUser(userId)
}

func (f *GameFrame) recvPrivateRoomDismissResp(userIndex int32, msg, data string) bool {
	if !f.gameSink.IsPrivateRoom() {
		return false
	}
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("recvPrivateRoomDismissResp user not exist,%d,%s", userIndex, msg)
		return false
	}

	table := f.getTable(usr.GetUserTableId())
	if table == nil {
		log.Debug("recvPrivateRoomDismissResp user not in table,%d:%d,%s", userIndex, usr.GetUserId(), msg)
		return false
	}
	return table.onPrivateRoomDismissResp(usr, msg, data)
}

func (f *GameFrame) SetExtraParam(roomNo int, userId int, paramId int, data string) (ret string) {
	t := f.getPrivateTable(roomNo)
	if t == nil {
		log.Release("GameFrame.SetExtraParam(%d) failed roomNo[%d] not exist", paramId, roomNo)
		return
	}
	ret = ""

	return
}

func (f *GameFrame) recvPrivateRoomKick(userIndex int32, msg, data string) bool {
	if !f.gameSink.IsPrivateRoom() {
		log.Debug("recvPrivateRoomKick not in privateroom")
		return false
	}
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("recvPrivateRoomKick user not exist,%d,%s", userIndex, msg)
		return false
	}
	table := f.getTable(usr.GetUserTableId())
	if table == nil {
		log.Debug("recvPrivateRoomKick user not in table,%d:%d,%s", userIndex, usr.GetUserId(), msg)
		f.sendKickResult(userIndex, false, "not in table")
		return false
	}

	var kick message.KickUser
	e := json.Unmarshal([]byte(data), &kick)
	if e != nil {
		log.Debug("GameFrame.recvPrivateRoomKick Unmarshal failed %s", data)
		f.sendKickResult(userIndex, false, "invalid argument")
		return false
	}

	toUser := gate.GetUserByUserId(kick.ToUserId)
	if toUser == nil {
		log.Release("GameFrame.recvPrivateRoomKick toUserId[%d] not exist", kick.ToUserId)
		return false
	}

	if !userservices.CanKickPrivateRoomUser(usr.GetUserId(), kick.ToUserId) {
		log.Debug("GameFrame.recvPrivateRoomKick [%d]no right to kick[%d]", usr.GetUserId(), kick.ToUserId)
		f.sendKickResult(userIndex, false, "no right")
		return false
	}

	// 可以踢人
	ret := table.kickUser(toUser.GetUserIndex())
	if ret {
		// 发送成功指令
		f.sendKickResult(userIndex, true, "")
		f.sendBeKicked(toUser.GetUserIndex(), usr.GetUserId())
	} else {
		f.sendKickResult(userIndex, false, "failed")
	}
	return ret
}

func (f *GameFrame) sendKickResult(userIndex int32, success bool, errMsg string) {
	ret := message.KickUser{
		Success: success,
		ErrMsg:  errMsg,
	}
	d, _ := json.Marshal(ret)
	gate.SendMessage(userIndex, message.Frame_PrivateRoom_KickUser, string(d))
}

func (f *GameFrame) sendBeKicked(userIndex int32, fromUserId int) {
	ret := message.KickUser{
		UserId: fromUserId,
	}
	d, _ := json.Marshal(ret)
	gate.SendMessage(userIndex, message.Frame_PrivateRoom_BeKicked, string(d))

}
