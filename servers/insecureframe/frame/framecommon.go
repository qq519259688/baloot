package frame

import (
	"bet24.com/servers/user"
)

type GameSink interface {
	GetServerPort() int
	GetGameID() int
	GetGameName() string
	GetRoomName() string
	CreateTableSink(table Table, data string) TableSink
	GetOfflineSeconds() int64
	GetCertFile() string
	GetKeyFile() string
	GetServerAddr() string
	//机器人配置
	GetRobotCount() int
	GetRobotGoldLimit() (min, max int)
	GetRobotOnlineSec() int
	GetRoomDatas() []string
	// 获取最低房间数量，少于则创建机器人房间
	GetMinRoomCount(roomData string) int
	IsChipRoom() bool
	OnPlatformConfig(key string)
	IsPrivateRoom() bool

	// 获取椅子号，1可表示百人游戏
	GetChairCount() int
	GetRoomRobotCount(roomName string) int
	GetRoomRobotGoldLimit(roomName string) (min, max int)
	GetRoomLevel(data string) int
}

type GameSink_LadderRoom interface {
	IsLadderRoom() bool
	GetMinGold(roomName string) int
	GetMaxGold(roomName string) int
	GetAdditionalPercent(roomName string) int
	GetBaseScore(roomName string) int
}

type GameSink_FishRoom interface {
	IsTableCloseByLogic() bool
	IgnoreSameIP() bool
	EnterPlayingRoomFirst() bool
}

const (
	TableStatus_Free = iota
	TableStatus_Playing
)

const (
	MultiSetStatus_Free = iota
	MultiSetStatus_Playing
)

const (
	Param_Target = iota
	Param_UserCount
	Param_PlayTimeout
	Param_RoomType
	Param_Bet
	Param_Win
)

const (
	RoomType_normal = iota
	RoomType_private
	RoomType_match
)

type TableSink interface {
	OnGameMessage(userIndex int32, msg, data string) bool
	OnUserEnterTable(userIndex int32, chairId int)
	OnUserExitTable(userIndex int32, chairId int)
	OnUserOffline(chairId int)
	OnUserReplay(chairId int)
	OnUserReady(userIndex int32, chairId int)
	OnUserCancelReady(userIndex int32, chairId int)
	OnGetChairScene(chairId int, isPlayer bool) string
	OnGetChairCount() int
	Destroy()
	OnTimer(timerId int)
	DumpScene()
	GetGoldLimit() (min, max int)
	IsDual() bool
	OnBaseScoreChanged(baseScore int)
}

// 目前已知，后续可补充
type RoomExtraData struct {
	UserId int
	Param  int
}

type TableSink_PrivateRoom interface {
	SetPrivateRoomParam(param int, value string)
	OnPrivateRoomStatusChanged(oldStatus, newStatus int)
	OnPrivateRoomDismissed()
	OnGetPrivateRoomScene(chairId int) string
}

type Table interface {
	// 游戏准备好后，即可调用框架StartGame进行场景广播
	StartGame()
	// 游戏结算后，调用
	EndGame()

	// 多局开关
	MultiSetStart()
	MultiSetEnd()

	// 刷新场景 chairId == -1表示所有人
	NotifySceneChanged(chairId int)

	GetUserByChair(chairId int) *user.UserInfo
	KickUser(userIndex int32, force bool) bool
	KickUserByChair(chairId int, force bool) bool

	// 如果userIndex == -1 则表示广播给所有人
	SendGameData(userIndex int32, msg, data string)
	SendGameDataToChair(chairId int, msg, data string)
	BroadcastToWatcher(msg, data string)

	// 获取玩家，不算旁观的
	GetPlayer(userIndex int32) *user.UserInfo
	GetUserByUserId(userId int) *user.UserInfo
	GetPlayerByUserId(userId int) *user.UserInfo
	// 获取用户接口，返回是否player
	GetUser(userIndex int32) (*user.UserInfo, bool)

	SetUserReadyStatus(userIndex int32, isReady bool) bool

	// 写分并返回实际修改数量
	WriteUserMoneyWithModifyAmount(userId int, amount, tax int, status, scoreType int, sourceName string) int
	WriteUserMoney(userId int, amount, tax int, status, scoreType int, sourceName string) (bool, int)
	WriteBetRecord(userId int, betAmount int, winAmount int, winRate float64, betDesc string, resultDesc string, roomName string)
	WriteBetRecordWithPlayTime(userId int, betAmount int, winAmount int, winRate float64, betDesc string, resultDesc string, roomName string, secsBefore int)
	WriteBetRecordWithSetcount(userId int, betAmount int, winAmount int, winRate float64, betDesc string, resultDesc string, roomName string, setCount int)
	GetTableID() int
	// 用户转成旁观状态
	UserWatch(userIndex int32)

	IsPlaying() bool
	IsMultiSetPlaying() bool

	RequestADAward(userIndex int32, losingAmount int)
	LogWithTableId(format string, a ...interface{})

	// 玩家游戏已结束，设置成Sit状态可以离开
	SetUserEndGame(chairId int)

	// 框架定时器
	SetTimer(timerId int, delayMs int)
	KillTimer(timerId int)
	KillAllTimer()

	SendBroadcast(userId int, userName string, score int)
	Dismiss()
	FinishGame(userId int, baseScore, gameType, playerCount int)

	// 获取用户的金币或筹码，根据房间类型自动返回
	GetUserChipOrGold(userIndex int32) int
	GetUserChipOrGoldByUserId(userId int) int
	GetUserChipOrGoldByUser(usr *user.UserInfo) int

	IsPrivate() bool
	GetRoomNo() int
	GetOwner() int

	GetPlayerCount() int
	// 局间更新分数,
	UpdateGameScore(userId, scoreDelta int)
	// 协商解散房间
	DismissPrivateRoom()
	// 上报赢家
	PrivateRoomSetWinners(winners []int)

	// 增加经验值,返回实际增加数值
	AddExperience(userId, exp int) int

	GetUserList() []*user.UserInfo

	PrivateRoomGetRoomType() string
	PrivateRoomGetFeeAndPrize(userId int) (int, int)

	CloseTable()
}
