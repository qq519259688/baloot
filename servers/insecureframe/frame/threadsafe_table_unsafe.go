package frame

import (
	_ "bet24.com/log"
	"bet24.com/servers/user"
	"encoding/json"
)

type threadsafe_message struct {
	Msg      string
	Data     string
	Callback chan byte
}

type msg_AddUser struct {
	UserIndex int32
	ChairId   int
	Replay    bool
	ToWatch   bool
}

type msg_RemoveUser struct {
	UserIndex   int32
	ToWatch     bool
	ChangeTable bool
}

type msg_UserIndex struct {
	UserIndex int32
}

type msg_TableMessage struct {
	UserIndex int32
	Msg       string
	Data      string
}

type msg_SetReadyStatus struct {
	UserIndex int32
	IsReady   bool
}

type msg_UserReplay struct {
	OldUserIndex int32
	NewUserIndex int32
}

type msg_SetBaseScore struct {
	BaseScore int
}

type msg_statusChanged struct {
	OldStatus int
	NewStatus int
}

func (t *ThreadsafeTable) AddTableUser(userIndex int32, chairId int, replay bool, toWatch bool) bool {
	data := msg_AddUser{UserIndex: userIndex, ChairId: chairId, Replay: replay, ToWatch: toWatch}
	d, _ := json.Marshal(data)
	msg := &threadsafe_message{Msg: "AddUser", Data: string(d)}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
	return true
}

func (t *ThreadsafeTable) AddTableUser_sync(userIndex int32, chairId int, replay bool, toWatch bool) bool {
	data := msg_AddUser{UserIndex: userIndex, ChairId: chairId, Replay: replay, ToWatch: toWatch}
	d, _ := json.Marshal(data)
	callBack := make(chan byte)
	msg := &threadsafe_message{Msg: "AddUser", Data: string(d), Callback: callBack}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
	<-callBack
	return true
}

func (t *ThreadsafeTable) RemoveUser(userIndex int32, toWatch, changeTable bool) {
	data := msg_RemoveUser{UserIndex: userIndex, ToWatch: toWatch, ChangeTable: changeTable}
	d, _ := json.Marshal(data)
	callBack := make(chan byte)
	msg := &threadsafe_message{Msg: "RemoveUser", Data: string(d), Callback: callBack}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
	<-callBack
	//log.Debug("ThreadsafeTable.RemoveUser msg sent")
}

func (t *ThreadsafeTable) dismiss() {
	msg := &threadsafe_message{Msg: "dismiss", Data: ""}
	t.messageChan <- msg
}

func (t *ThreadsafeTable) userReplay(userIndex int32, usr *user.UserInfo) {
	data := msg_UserReplay{OldUserIndex: userIndex, NewUserIndex: usr.GetUserIndex()}
	d, _ := json.Marshal(data)
	callBack := make(chan byte)
	msg := &threadsafe_message{Msg: "userReplay", Data: string(d), Callback: callBack}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
	<-callBack
}

func (t *ThreadsafeTable) dumpScene() {
	msg := &threadsafe_message{Msg: "dumpScene", Data: ""}
	t.messageChan <- msg
}

func (t *ThreadsafeTable) dump() {
	msg := &threadsafe_message{Msg: "dump", Data: ""}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
}

func (t *ThreadsafeTable) dumpTimers() {
	msg := &threadsafe_message{Msg: "dumpTimers", Data: ""}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
}

func (t *ThreadsafeTable) onTableMessage(usr *user.UserInfo, msg, data string) bool {
	msgData := msg_TableMessage{UserIndex: usr.GetUserIndex(), Msg: msg, Data: data}
	d, _ := json.Marshal(msgData)
	msgInfo := &threadsafe_message{Msg: "TableMessage", Data: string(d)}
	//t.messageChan <- msgInfo
	go t.sendMessageToMainThread(msgInfo)
	return true
}

func (t *ThreadsafeTable) userWatch(userIndex int32) {
	data := msg_UserIndex{UserIndex: userIndex}
	d, _ := json.Marshal(data)
	msg := &threadsafe_message{Msg: "UserWatch", Data: string(d)}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
}

func (t *ThreadsafeTable) setUserReadyStatus(userIndex int32, isReady bool) bool {
	data := msg_SetReadyStatus{UserIndex: userIndex, IsReady: isReady}
	d, _ := json.Marshal(data)
	msg := &threadsafe_message{Msg: "SetUserReadyStatus", Data: string(d)}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
	return true
}

func (t *ThreadsafeTable) setUserReadyStatus_sync(userIndex int32, isReady bool) bool {
	data := msg_SetReadyStatus{UserIndex: userIndex, IsReady: isReady}
	d, _ := json.Marshal(data)
	callBack := make(chan byte)
	msg := &threadsafe_message{Msg: "SetUserReadyStatus", Data: string(d), Callback: callBack}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
	<-callBack
	return true
}

func (t *ThreadsafeTable) setBaseScore(data string) bool {
	d, _ := json.Marshal(data)
	msg := &threadsafe_message{Msg: "SetBaseScore", Data: string(d)}
	//t.messageChan <- msg
	go t.sendMessageToMainThread(msg)
	return true
}
