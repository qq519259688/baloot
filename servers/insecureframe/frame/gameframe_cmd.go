package frame

import (
	"encoding/json"
	"strconv"

	"bet24.com/log"
	coreservice "bet24.com/servers/coreservice/client"
	"bet24.com/servers/insecureframe/gate"
	"bet24.com/servers/insecureframe/message"
	privateroom "bet24.com/servers/micros/privateroom/proto"
	"bet24.com/servers/user"
)

func (f *GameFrame) recvAutoSit(userIndex int32, data string, changeTable bool) bool {
	// 判断自己状态
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("GameFrame.recvAutoSit user not exist %d", userIndex)
		return false
	}
	if usr.GetUserStatus() == user.UserStatus_Play || usr.GetUserStatus() == user.UserStatus_Offline {
		log.Debug("GameFrame.recvAutoSit user playing %d", userIndex)
		return false
	}

	roomLevel := f.gameSink.GetRoomLevel(data)
	if usr.GetUserLevel() < roomLevel {
		log.Debug("GameFrame.recvAutoSit User Level:%d < RoomLevel:%d", usr.GetUserLevel(), roomLevel)
		return false
	}

	lastTableId := usr.GetUserTableId()

	if lastTableId != -1 {
		// 从原先的桌子离开
		lastTable := f.getTable(lastTableId)
		if lastTable != nil {
			log.Debug("recvAutoSit remove from old table %d", lastTableId)
			lastTable.RemoveUser(userIndex, false, false)
		}
	}

	forceCreate := false

	if !changeTable {
		lastTableId = usr.GetUserLastTableId()
	} else {
		if f.isForceCreateTable(userIndex) {
			forceCreate = true
		}
	}
	// 找到一张桌子
	var table *ThreadsafeTable
	if !forceCreate {
		needFree := f.isNeedFreeTable(userIndex)
		table = f.getOrCreateTable(lastTableId, data, needFree, usr.GetUserIp())
	} else {
		table = f.createTable(data)
	}
	if table == nil {
		log.Debug("GameFrame.recvAutoSit can not get or create table")
		return false
	}
	log.Debug("recvAutoSit add to new table %d", table.GetTableID())
	if f.gameSink.GetChairCount() < 2 {
		table.AddTableUser(userIndex, -1, false, true)
	} else {
		table.AddTableUser(userIndex, -1, false, false)
	}
	return true
}

func (f *GameFrame) addChangeTable(userIndex int32) {
	f.lock_changeTable.Lock()
	f.changeTableCount[userIndex]++
	f.lock_changeTable.Unlock()
}

func (f *GameFrame) isForceCreateTable(userIndex int32) bool {
	ret := false
	f.lock_changeTable.Lock()
	if f.changeTableCount[userIndex]%3 == 0 {
		ret = true
		//f.changeTableCount[userIndex] = 0
	}
	f.lock_changeTable.Unlock()
	return ret
}

func (f *GameFrame) isNeedFreeTable(userIndex int32) bool {
	f.lock_changeTable.RLock()
	defer f.lock_changeTable.RUnlock()
	return f.changeTableCount[userIndex] == 0
}

func (f *GameFrame) recvChangeTable(userIndex int32, data string) bool {
	f.addChangeTable(userIndex)
	return f.recvAutoSit(userIndex, data, true)
}

func (f *GameFrame) recvSitTable(userIndex int32, data string) bool {
	if f.gameSink.GetChairCount() < 2 {
		return f.recvWatchTable(userIndex, data)
	}
	//log.Debug("GameFrame.recvSitTable,%s", data)
	// 判断自己状态
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("GameFrame.recvSitTable user not exist %d", userIndex)
		return false
	}
	var sit message.SitTable
	e := json.Unmarshal([]byte(data), &sit)
	if e != nil {
		log.Debug("GameFrame.recvSitTable Unmarshal failed %s", data)
		return false
	}
	user_status := usr.GetUserStatus()
	if user_status == user.UserStatus_Play {
		log.Debug("GameFrame.recvSitTable user playing %d", userIndex)
		return false
	}
	// 如果我的状态本来就是坐下的,
	/*if user_status == user.UserStatus_Sit && sit.TableId <= 0 {
		log.Debug("GameFrame.recvSitTable 已经坐下了")
		return false
	}*/
	// 如果我的状态是旁观的
	if user_status == user.UserStatus_Watch {
		// 直接转成坐下状态
		//f.setUserStatus(userIndex, user.UserStatus_Sit)
		sit.TableId = usr.GetUserTableId()
	}

	if sit.TableId < 0 {
		log.Debug("GameFrame.recvSitTable table not found %d", sit.TableId)
		return false
	}

	table := f.getTable(sit.TableId)
	if table == nil {
		log.Debug("GameFrame.recvSitTable table not found %d", sit.TableId)
		return false
	}

	// 如果多局已开始，不让坐下
	if table.IsMultiSetPlaying() {
		log.Debug("GameFrame.recvSitTable IsMultiSetPlaying %d", sit.TableId)
		return false
	}

	lastTableId := usr.GetUserTableId()
	if lastTableId != -1 && lastTableId != sit.TableId {
		// 从原先的桌子离开
		lastTable := f.getTable(lastTableId)
		if lastTable != nil {
			lastTable.RemoveUser(userIndex, false, false)
		}
	}

	/*
		可以旁观
		if table.GetTableStatus() == TableStatus_Playing {
			return false
		}
	*/
	log.Debug("GameFrame.recvSitTable,%s", data)

	// 如果是私人场，dual玩法
	sit.ChairId = table.adjustPrivateChairId(sit.ChairId)

	if sit.ChairId != -1 && !table.isChairEmpty(sit.ChairId) {
		return false
	}

	// 如果是私人场，并且是换位置
	if table.isPrivate() &&
		lastTableId == sit.TableId &&
		(user_status == user.UserStatus_Sit || user_status == user.UserStatus_Ready) &&
		usr.GetUserChairId() != sit.ChairId {
		if !privateroom.UserChangeChair(table.roomNo, usr.GetUserId(), sit.ChairId) {
			log.Release("GameFrame.recvSitTable UserChangeChair failed")
			return false
		}
		table.RemoveUser(userIndex, false, true)
	}
	return table.AddTableUser_sync(userIndex, sit.ChairId, false, false)
}

func (f *GameFrame) recvWatchTable(userIndex int32, data string) bool {
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("GameFrame.recvWatchTable user not exist")
		return false
	}
	// 如果是playing
	if usr.GetUserStatus() == user.UserStatus_Play {
		log.Debug("GameFrame.recvWatchTable user playing")
		gate.SendMessage(userIndex, message.Frame_Watch, "watch failed")
		return false
	}

	var sit message.SitTable
	e := json.Unmarshal([]byte(data), &sit)
	if e != nil {
		log.Debug("GameFrame.recvWatchTable Unmarshal failed %s", data)
		return false
	}

	table := f.getTable(sit.TableId)

	if table == nil {
		log.Debug("GameFrame.recvWatchTable table not found %d", usr.GetUserTableId())
		return false
	}
	return table.AddTableUser(userIndex, sit.ChairId, false, true)
}

func (f *GameFrame) recvStandup(userIndex int32, data string) bool {
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("GameFrame.recvStandup user not exist")
		return false
	}
	table := f.getTable(usr.GetUserTableId())

	if table == nil {
		log.Debug("GameFrame.recvStandup table not found %d", usr.GetUserTableId())
		return false
	}

	table.RemoveUser(userIndex, false, false)
	return true
}

func (f *GameFrame) recvWatch(userIndex int32, data string) bool {
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("GameFrame.recvWatch user not exist")
		return false
	}
	// 如果是playing
	if usr.GetUserStatus() == user.UserStatus_Play {
		log.Debug("GameFrame.recvWatch user playing")
		gate.SendMessage(userIndex, message.Frame_Watch, "watch failed")
		return false
	}
	table := f.getTable(usr.GetUserTableId())

	if table == nil {
		log.Debug("GameFrame.recvWatch table not found %d", usr.GetUserTableId())
		return false
	}
	table.userWatch(userIndex)
	return true
}

func (f *GameFrame) recvReady(userIndex int32, isReady bool) bool {
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("GameFrame.recvReady user not exist")
		return false
	}
	table := f.getTable(usr.GetUserTableId())

	if table == nil {
		log.Debug("GameFrame.recvReady table not found %d", usr.GetUserTableId())
		return false
	}

	return table.setUserReadyStatus(userIndex, isReady)
}

func (f *GameFrame) recvAdReward(userIndex int32, data string) bool {
	if f.gameSink.IsChipRoom() {
		return false
	}
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("GameFrame.recvAdReward user not exist")
		return false
	}
	serialNo, err := strconv.Atoi(data)
	if err != nil {
		log.Debug("GameFrame.recvAdReward invalid argument [%s]", data)
		return false
	}
	// 调用coreservice的视频奖励
	// 根据返回信息刷新金币，并通知用户
	var rewarded message.AdRewarded
	rewarded.UserId = usr.GetUserId()
	rewarded.SerialNo = serialNo
	rewarded.Gold = coreservice.VideoSettle(rewarded.UserId, serialNo)
	d, _ := json.Marshal(rewarded)
	// log.Debug("GameFrame.recvAdReward sending data %s", string(d))
	gate.SendMessage(userIndex, message.Frame_ADReward, string(d))
	if rewarded.Gold > 0 {
		gate.RefreshGold(userIndex)
	}
	return true
}

func (f *GameFrame) recvFramePing(userIndex int32, data string) bool {
	gate.SendMessage(userIndex, message.Frame_Ping, data)
	return true
}

func (f *GameFrame) recvFrameVoiceMessage(userIndex int32, userId int, msg, data string) bool {
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("recvFrameVoiceMessage user not exist,%d:%d,%s", userIndex, userId, msg)
		return false
	}
	table := f.getTable(usr.GetUserTableId())
	if table == nil {
		log.Debug("recvFrameVoiceMessage user not in table,%d:%d,%s", userIndex, userId, msg)
		return false
	}
	var voiceData struct {
		ChairId int
		Data    string
	}
	voiceData.ChairId = usr.GetUserChairId()
	voiceData.Data = data
	d, _ := json.Marshal(voiceData)
	table.broadcastData(msg, string(d))
	return true
}

func (f *GameFrame) recvTableMessage(userIndex int32, userID int, msg, data string) bool {
	usr := gate.GetUserInfo(userIndex)
	if usr == nil {
		log.Debug("recvTableMessage user not exist,%d:%d,%s", userIndex, userID, msg)
		return false
	}

	table := f.getTable(usr.GetUserTableId())
	if table == nil {
		log.Debug("recvTableMessage user not in table,%d:%d,%s", userIndex, userID, msg)
		return false
	}

	return table.onTableMessage(usr, msg, data)
}
