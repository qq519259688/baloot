package frame

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"

	"xorm.io/xorm"
	"xorm.io/xorm/names"
)

var engine *xorm.Engine

func init() {
	var err2 error
	engine, err2 = xorm.NewEngine("mysql", "test:P_test865952!sdwcx@tcp(114.132.71.177:3306)/chat_game?charset=utf8")
	//maxIdleCount 最大空闲连接数，默认不配置，是2个最大空闲连接
	engine.SetMaxIdleConns(20)
	//最大连接数，默认不配置，是不限制最大连接数
	engine.SetMaxOpenConns(100)
	//maxLifetime 连接最大存活时间
	engine.SetConnMaxLifetime(1000)
	
	if err2 != nil {
		fmt.Println("连接数据库失败", err2)
		return
	}
	// 会在控制台打印执行的sql

	//	XormDb.SetColumnMapper(core.SnakeMapper{})
	engine.SetTableMapper(names.SnakeMapper{})
	engine.SetColumnMapper(names.SnakeMapper{})
	engine.ShowSQL(true)
}