package frame

import (
	"bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/insecureframe"
	"bet24.com/servers/insecureframe/gate"
	"bet24.com/servers/insecureframe/robot"
	micro_common "bet24.com/servers/micros/common"
	ladderservice "bet24.com/servers/micros/ladderservice/proto"
	privateroom_server "bet24.com/servers/micros/privateroom/game/server"
	privateroom "bet24.com/servers/micros/privateroom/proto"
	"fmt"
	"os"
	"strconv"
	"time"
)

var gameFrame *GameFrame

var stopping bool

func RunFrame(gameSink GameSink, redisUrl, redisPsw string, redisDb int) *GameFrame {
	return run(gameSink, redisUrl, redisPsw, redisDb, "")
}

func RunFrameWithLogServer(gameSink GameSink, redisUrl, redisPsw string, redisDb int, logPath string) *GameFrame {
	return run(gameSink, redisUrl, redisPsw, redisDb, logPath)
}

func run(gameSink GameSink, redisUrl, redisPsw string, redisDb int, logPath string) *GameFrame {
	gameFrame = NewGameFrame(gameSink)
	insecureframe.Run(gameFrame, redisUrl, redisPsw, redisDb, logPath)
	stopping = false

	log.Release("frame running")
	if gameSink.IsPrivateRoom() {
		log.Release("frame running is private room")
		go privateroom_server.Run(gameSink.GetGameName(), os.Getpid(), 8500, gameFrame)
		fmt.Printf("启动完privateroom_server")
		privateRoomPing()
		fmt.Printf("启动完privateRoomPing")
	}

	//go gameFrame.CreateBacTable()
	return gameFrame
}

func privateRoomPing() {
	if stopping {
		return
	}

	privateroom.UpdateServerOnline(privateroom_server.GetAddr(), gate.GetUserCount())
	//privateroom.UpdateServerOnline(privateroom_server.GetAddr(), gate.GetUserCount())
	time.AfterFunc(30*time.Second, privateRoomPing)
}

func StopServer() {
	stopping = true
	gate.StopServer()
	if gameFrame.gameSink.IsPrivateRoom() {
		privateroom.UnregisterServer(privateroom_server.GetAddr())
	}
	if gameFrame.IsLadderRoom() {
		ladderservice.DeregisterLadderRoom(gameFrame.gameSink.GetServerAddr())
	}
}

func GetUserCount(privateData string) int {
	return gameFrame.getUserCount(privateData)
}

func GetVirtualUserCount(userCount int, roomType int) int {
	return gameFrame.GetVirtualUserCount(userCount, roomType)
}

func UpdateRoomList(roomName, ruleDesc string, gameRule string, roomType int) {
	skipRedisInfo := false
	if gameFrame.gameSink.IsPrivateRoom() {
		privateroom.RegisterGameRule(privateroom_server.GetAddr(),
			gameFrame.gameSink.GetGameID(),
			gameFrame.gameSink.GetGameName(), roomName, ruleDesc, gameRule)
		skipRedisInfo = true
	}

	gameSink_LadderRoom, ok := gameFrame.gameSink.(GameSink_LadderRoom)

	if ok && gameSink_LadderRoom.IsLadderRoom() {
		ladderservice.RegisterLadderRoom(gameFrame.gameSink.GetGameID(), roomName, gameRule,
			gameSink_LadderRoom.GetBaseScore(roomName),
			gameSink_LadderRoom.GetMinGold(roomName),
			gameSink_LadderRoom.GetMaxGold(roomName),
			gameSink_LadderRoom.GetAdditionalPercent(roomName),
			gameFrame.gameSink.GetServerAddr())
		skipRedisInfo = true
	}
	if skipRedisInfo {
		return
	}
	addrKey := "Addr"
	onlineKey := "Online"
	if gameFrame.gameSink.IsChipRoom() {
		addrKey = "ChipAddr"
		onlineKey = "ChipOnline"
	}
	gameName := gameFrame.gameSink.GetGameName()
	redis.String_SetEx(fmt.Sprintf("%s:%s:%v", gameName, addrKey, roomName), gameRule, 70)
	online := fmt.Sprintf("%d", GetVirtualUserCount(GetUserCount(roomName), roomType))
	redis.String_SetEx(fmt.Sprintf("%s:%s:%v", gameName, onlineKey, roomName), online, 70)
}

func UpdateRoomOnline(roomName string, roomType int) {
	//log.Release("UpdateRoomOnline %s,%d", roomName, roomType)
	if gameFrame.gameSink.IsPrivateRoom() {
		privateroom.UpdateServerOnline(privateroom_server.GetAddr(), gate.GetUserCount())
		//privateroom.UpdateServerOnline(privateroom_server.GetAddr(), gate.GetUserCount())
		return
	}
	onlineKey := "Online"
	if gameFrame.gameSink.IsChipRoom() {
		onlineKey = "ChipOnline"
	}
	gameName := gameFrame.gameSink.GetGameName()
	online := fmt.Sprintf("%d", GetVirtualUserCount(GetUserCount(roomName), roomType))
	redis.String_SetEx(fmt.Sprintf("%s:%s:%v", gameName, onlineKey, roomName), online, 70)
}

func Dump(cmd, param1, param2 string) bool {
	switch cmd {
	case "frame":
		gameFrame.dump()
	case "table":
		tableId, _ := strconv.Atoi(param1)
		gameFrame.dumpTable(tableId)
	case "scene":
		tableId, _ := strconv.Atoi(param1)
		gameFrame.dumpScene(tableId)
	case "dismiss":
		tableId, _ := strconv.Atoi(param1)
		gameFrame.dismissTable(tableId)
	case "offline":
		gameFrame.dumpOffline()
	case "timer":
		tableId, _ := strconv.Atoi(param1)
		gameFrame.dumpTimers(tableId)
	case "user":
		log.Debug("----------frame.Dump user ----------")

		userId, _ := strconv.Atoi(param1)
		usr := gate.GetUserByUserId(userId)
		if usr == nil {
			log.Debug("    userId %d not exist", userId)
		} else {
			log.Debug("    %s", usr.DumpUserInfo())
		}
		log.Debug("++++++++++frame.Dump user ++++++++++")
	case "robot":
		robot.Dump()
	case "messagerecord":
		userId, err := strconv.Atoi(param1)
		if err != nil {
			userId = 0
		}
		gate.DumpMessageRecord(userId)
	case "clearrecord":
		userId, err := strconv.Atoi(param1)
		if err != nil {
			userId = 0
		}
		gate.ClearMessageRecord(userId)
	case "clientpool":
		micro_common.DumpClientPools()
	default:
		return false
	}
	return true
}
