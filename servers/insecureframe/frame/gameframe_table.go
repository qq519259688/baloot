package frame

import (
	"bet24.com/log"
	"fmt"
	"sort"
)

// --GameTable++ 匹配建房
func (f *GameFrame) createTable(data string) *ThreadsafeTable {
	f.lock_table.Lock()
	table := newThreadSafeTable(f.tableIndex, 0)
	f.tableIndex++
	f.lock_table.Unlock()
	table.privateData = data
	table.setTableSink(f.gameSink.CreateTableSink(table, data))
	f.lock_table.Lock()
	f.tables[table.tableId] = table
	f.lock_table.Unlock()
	return table
}
//语聊房建房 
func (f *GameFrame) createPrivateTable(userId int, ruleName string, target int, userCount int, playTimeout int) *ThreadsafeTable {
	f.lock_table.Lock()
	table := newThreadSafeTable(f.tableIndex, userId)

	f.tableIndex++
	f.lock_table.Unlock()
	
	ts := f.gameSink.CreateTableSink(table, ruleName)
	table.setTableSink(ts)
	table.tableSink_privateRoom.SetPrivateRoomParam(Param_Target, fmt.Sprintf("%d", target))
	table.tableSink_privateRoom.SetPrivateRoomParam(Param_UserCount, fmt.Sprintf("%d", userCount))
	table.tableSink_privateRoom.SetPrivateRoomParam(Param_PlayTimeout, fmt.Sprintf("%d", playTimeout))
	table.privateData = ruleName
	f.lock_table.Lock()
	f.tables[table.tableId] = table
	f.lock_table.Unlock()
	return table
}

func (f *GameFrame) getTableCount(data string) int {
	f.lock_table.RLock()
	defer f.lock_table.RUnlock()
	ret := 0
	for _, v := range f.tables {
		if v.isIdled() || v.isFull() || !v.isSameRoom(data) {
			continue
		}
		ret++
	}
	return ret
}

// --GameTable++
func (f *GameFrame) getAvailableTable(exceptTableId int, data string, needFree bool, userIp string) *ThreadsafeTable {
	f.lock_table.RLock()

	var availableTables []*ThreadsafeTable
	for _, v := range f.tables {
		if v.isPrivate() {
			continue
		}
		if v.tableId == exceptTableId {
			// log.Debug("%d = %d", v.tableId, exceptTableId)
			continue
		}
		// 可以进入playing的房间
		if v.isIdled() {
			//log.Debug("getAvailableTable exceptTableId[%d] tableId[%d] isIdled", exceptTableId, v.tableId)
			continue
		}

		if v.stopped {
			continue
		}

		if !v.isSameRoom(data) {
			log.Debug("getAvailableTable exceptTableId[%d] tableId[%d] not same data [%s] != %s", exceptTableId, v.tableId, data, v.privateData)
			continue
		}

		// 如果是多局游戏已开始，也过滤
		if v.IsMultiSetPlaying() && !v.enterPlayingRoomFirst() {
			continue
		}
		if needFree && v.IsPlaying() && !v.enterPlayingRoomFirst() {
			log.Debug("getAvailableTable exceptTableId[%d] tableId[%d] ignore playing", exceptTableId, v.tableId)
			continue
		}

		// 有同IP
		if v.checkSameIp(userIp) {
			log.Debug("getAvailableTable checkSameIp failed %s", userIp)
			continue
		}

		if v.isFull() {
			log.Debug("getAvailableTable exceptTableId[%d] tableId[%d] full", exceptTableId, v.tableId)
			// 如果里面有机器人
			if !v.IsPlaying() && v.canRemoveOneRobot() {
				f.lock_table.RUnlock()
				return v
			}
			continue
		}
		//return v
		availableTables = append(availableTables, v)
	}
	f.lock_table.RUnlock()

	if len(availableTables) == 0 {
		return nil
	}

	if len(availableTables) > 1 {
		if availableTables[0].enterPlayingRoomFirst() {
			sort.Slice(availableTables, func(i, j int) bool {
				return availableTables[i].getPlayerCount() > availableTables[j].getPlayerCount()
			})
		} else {
			sort.Slice(availableTables, func(i, j int) bool {
				if availableTables[i].tableStatus == availableTables[j].tableStatus {
					return availableTables[i].getStartTime() > availableTables[j].getStartTime()
				}

				return availableTables[i].tableStatus < availableTables[j].tableStatus
			})

		}
	}
	return availableTables[0]

}

func (f *GameFrame) getOrCreateTable(exceptTableId int, data string, needFree bool, userIp string) *ThreadsafeTable {
	table := f.getAvailableTable(exceptTableId, data, needFree, userIp)
	if table != nil {
		return table
	}
	return f.createTable(data)
}

func (f *GameFrame) getTable(tableId int) *ThreadsafeTable {
	if tableId <= 0 {
		return nil
	}
	f.lock_table.RLock()
	defer f.lock_table.RUnlock()
	t, ok := f.tables[tableId]
	if !ok {
		log.Debug("GameFrame.getTable not exist %d", tableId)
		return nil
	}
	if t.stopped {
		log.Release("GameFrame.getTable [%d] stopped", tableId)
		return nil
	}
	return t
}

func (f *GameFrame) removeTable(tableId int) {
	f.removeOfflineByTableId(tableId)
	f.lock_table.Lock()
	defer f.lock_table.Unlock()
	t, ok := f.tables[tableId]
	delete(f.tables, tableId)
	if ok {
		t.Destroy()
	}
}
