package message

type BaseMsg struct {
	Msg  string
	Data string
}

// 大厅命令
const (
	Login         = "login"
	LoginByImei   = "loginByImei"
	LoginByToken  = "loginByToken"
	Ping          = "ping"
	IsChipRoomMsg = "isChipRoom"
)

// 框架命令
const (
	// S->C
	Frame_UserEnter = "Frame_UserEnter"
	//Frame_UserExit         = "Frame_UserExit"
	Frame_UserStatusChange = "Frame_UserStatusChange"
	Frame_UserScoreChange  = "Frame_UserScoreChange"

	// C->S
	Frame_AutoSit     = "Frame_AutoSit"
	Frame_ChangeTable = "Frame_ChangeTable"
	Frame_Sit         = "Frame_Sit"
	Frame_WatchTable  = "Frame_WatchTable"
	Frame_Standup     = "Frame_Standup"
	Frame_Watch       = "Frame_Watch"
	Frame_Ready       = "Frame_Ready"
	Frame_CancelReady = "Frame_CancelReady"
	//Frame_CreatePrivateRoom        = "Frame_CreatePrivateRoom"
	Frame_PrivateRoom_No = "Frame_PrivateRoom_No"
	//Frame_PrivateRoom_SetBaseScore = "Frame_PrivateRoom_SetBaseScore"
	Frame_PrivateRoom_Dismissed      = "Frame_PrivateRoom_Dismissed"
	Frame_PrivateRoom_RequestDismiss = "Frame_PrivateRoom_RequestDismiss"
	Frame_PrivateRoom_DismissResp    = "Frame_PrivateRoom_DismissResp"
	Frame_PrivateRoom_DimiissStatus  = "Frame_PrivateRoom_DimiissStatus"
	Frame_PrivateRoom_KickUser       = "Frame_PrivateRoom_KickUser" // 发起踢人动作，返回成功或者失败
	Frame_PrivateRoom_BeKicked       = "Frame_PrivateRoom_BeKicked" // 被踢了

	// 下行，通知用户可以看视频
	Frame_ADRewardInfo = "Frame_ADRewardInfo"
	// 广告免输协议，上下行通用
	// 如果参数为-1表示失败
	Frame_ADReward = "Frame_ADReward"

	// ping 测试网络延时用
	Frame_Ping  = "Frame_Ping"
	Frame_Voice = "Frame_Voice"
)

// 桌子命令
const (
	// S->C
	Table_UserEnter   = "Table_UserEnter"
	Table_UserExit    = "Table_UserExit"
	Table_GameScene   = "Table_GameScene"
	Table_UserOffline = "Table_UserOffline"
	Table_UserReplay  = "Table_UserReplay"
	Table_GameEnd     = "Table_GameEnd"
)

type UserStatusChange struct {
	UserId    int
	OldStatus int
	NewStatus int
	TableId   int
	ChairId   int
}

type TableUserExit struct {
	UserId  int
	ChairId int `json:"ChairId,omitempty"`
	ToWatch bool
}

type IsChipRoom struct {
	IsChipRoom   bool
	IsLadderRoom bool
}

type UserScoreChange struct {
	UserId int
	Gold   int
	Chip   int
}

type SitTable struct {
	TableId int
	ChairId int
}

type AdRewardInfo struct {
	UserId       int // 用户ID
	LosingGold   int // 本局结算金币
	SerialNo     int // 序列号
	ReturnGold   int // 总返还金币
	MaxTimes     int // 最多视频次数
	SettleAmount int // 当前返还金币
}

type AdRewarded struct {
	UserId   int
	Gold     int
	SerialNo int
}

type KickUser struct {
	ToUserId int `json:",omitempty"`
	Success  bool
	UserId   int    `json:",omitempty"`
	ErrMsg   string `json:",omitempty"`
}
