package gate

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	_ "runtime/debug"
	"strconv"
	"time"

	badge "bet24.com/servers/micros/badge/proto"

	"bet24.com/event"
	"bet24.com/log"
	"bet24.com/network"
	"bet24.com/redis"
	coreservice "bet24.com/servers/coreservice/client"
	utils2 "bet24.com/servers/insecureframe/gate/Utils"
	"bet24.com/servers/insecureframe/message"
	dotservice "bet24.com/servers/micros/dotservice/proto"
	item "bet24.com/servers/micros/item_inventory/proto"
	remotemessage "bet24.com/servers/micros/remotemessage/proto"
	userlabel "bet24.com/servers/micros/userlabel/proto"
	userservices "bet24.com/servers/micros/userservices/proto"
	vipservice "bet24.com/servers/micros/userservices/proto"
	"bet24.com/servers/transaction"
	"bet24.com/servers/user"
	"bet24.com/utils"
	uuid "github.com/satori/go.uuid"
)

const EXPIRE_TIME = 1800

func NewClient(userIndex int32, conn *network.WSConn, gate *Gate, isRobot bool) *client {
	ret := new(client)
	ret.userInfo = user.NewUserInfo(userIndex)
	ret.gate = gate
	ret.conn = conn
	if isRobot {
		ret.userInfo.SetRobot()
	} else {
		ret.userInfo.SetUserIp(ret.IP())
	}

	return ret
}

type client struct {
	conn     *network.WSConn
	gate     *Gate
	userInfo *user.UserInfo
	// 用户主线程退出信号
	onMainExit func()
	// 主控context
	m_ctx context.Context

	// 来自服务器的消息
	chan_channel_msg chan interface{}
	// event chan
	chan_event chan interface{}

	lastPing       time.Time
	Log_login_time time.Time

	destroyed bool

	loginGold int
}

func (this *client) GetEventChannel() chan interface{} {
	return this.chan_event
}

func (this *client) Run() {
	this.lastPing = time.Now()
	socket_channel := make(chan *message.BaseMsg)
	ctx, cancel := context.WithCancel(context.Background())
	this.onMainExit = cancel
	this.m_ctx = ctx
	this.destroyed = false
	go this.readSocketData(socket_channel)
	go this.checkPingTimeOut(ctx)
	this.chan_event = make(chan interface{})
	this.Log_login_time = time.Now()
	gate.addUser(this.userIndex(), this)
	fmt.Printf("client running.........")
	if this.userInfo.IsRobot() || this.conn == nil {
		return
	}

	for {
		select {
		case msg := <-socket_channel: //来自socket消息处理
			this.handleMsg(msg)
		case msg := <-this.chan_event: // event handler
			go this.handleEvent(msg)
		case <-ctx.Done():
			this.Destroy()
			return
		}
	}
}

func (this *client) userIndex() int32 {
	return this.userInfo.GetUserIndex()
}

func (this *client) isLogined() bool {
	return this.userInfo.GetUserId() > 0
}

func (this *client) isRobot() bool {
	return this.userInfo.IsRobot()
}

func (this *client) checkPingTimeOut(ctx context.Context) {
	if this.userInfo.IsRobot() || this.conn == nil {
		return
	}
	count := 0
	for {
		c, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		select {
		case <-ctx.Done():
			//log.Debug("exit ping ---------------------userID=%d err=%v", this.UserId, ctx.Err())
			cancel()
			return
		case <-c.Done():
			this.handlePing()
			count++
			if count == 100 {
				count = 0
				this.checkSection(this.userInfo.GetUserId(), this.userInfo.GetUserSession())
			}
		}
		cancel()
	}
}

func (this *client) handlePing() {
	// 很长时间没有ping了
	if this.isForceToCloseConnect() {
		this.Close()
		this.onMainExit()
	}
}

func (this *client) isForceToCloseConnect() bool {
	if this.userInfo.IsRobot() {
		return false
	}
	pastSeconds := int(time.Now().Sub(this.lastPing).Seconds())
	if pastSeconds > 180 {
		log.Debug("isForceToCloseConnect pastSeconds = %d,userid = %d", pastSeconds, this.userInfo.GetUserId())
		return true
	}
	return false
}

func (this *client) handleEvent(evt interface{}) {
	t := evt.(event.IEventChan)
	defer utils.TimeCost(fmt.Sprintf("client.handleEvent"))()
	t.OnEvent()
}

func (this *client) readSocketData(ch chan<- *message.BaseMsg) {
	if this.userInfo.IsRobot() || this.conn == nil {
		return
	}
	for {
		if this.conn == nil {
			return
		}
		data, err := this.conn.ReadMsg()
		if err != nil {
			log.Release("read message: %v", err)
			this.onMainExit()
			break
		}

		var msg *message.BaseMsg = new(message.BaseMsg)
		e := json.Unmarshal(data, msg)
		if e != nil {
			log.Release("readSocketData Unmarshal failed %v", string(data))
		} else {
			if msg.Msg == "" && msg.Data == "" {
				msg.Data = string(data)
			}
			ch <- msg
		}
	}
}

func (this *client) handleMsg(msg *message.BaseMsg) {
	defer utils.TimeCost(fmt.Sprintf("client.handleMsg %v", msg.Msg))()
	this.lastPing = time.Now()

	switch msg.Msg {
	case "ping":
		this.WriteMsg(msg.Msg, msg)

	//大厅功能类操作
	case "loginByIMei": // 登录
		this.loginByIMei(msg.Msg, msg.Data)
	case "login": // 到数据库查询
		this.Login(msg.Msg, msg.Data)
	case "loginByToken":
		this.loginByToken(msg.Msg, msg.Data)
	case "loginBySection":
		this.loginByToken(msg.Msg, msg.Data)
	default:
		gate.addMessageRecord(this.userInfo.GetUserId(), fmt.Sprintf("[%s]:{%s}", msg.Msg, msg.Data))
		if !sink.OnGameMessage(this.userIndex(), this.userInfo.GetUserId(), msg.Msg, msg.Data) {
			log.Debug("unhandled message %v  data %v", msg.Msg, msg.Data)
		}
	}
}

func (this *client) OnClose() {
	this.Destroy()
}

func (this *client) WriteMsg(cmdName string, msg interface{}) bool {
	buf, err := json.Marshal(msg)
	if err != nil {
		log.Release("WriteMsg Marshal faild cmdName:%v msg:%v", cmdName, msg)
		return false
	}

	if this.userInfo.IsRobot() || this.conn == nil || this.destroyed {
		return true
	}

	e := this.conn.WriteData(buf)
	if e != nil {
		log.Debug("client.WriteMsg %v", e)
	}
	return true
}

func (this *client) writeRawData(data string) {
	this.conn.WriteData([]byte(data))
}

func (this *client) LocalAddr() net.Addr {
	if this.conn == nil {
		return nil
	}
	return this.conn.LocalAddr()
}

func (this *client) RemoteAddr() net.Addr {
	if this.conn == nil {
		return nil
	}
	return this.conn.RemoteAddr()
}

func (this *client) IP() string {
	if this.userInfo.IsRobot() || this.conn == nil {
		return "127.0.0.1"
	}
	return this.RemoteAddr().(*net.TCPAddr).IP.String()
}

func (this *client) Close() {
	if this.conn != nil {
		this.conn.Close()
	} else {
		// 机器人
		this.Destroy()
	}
}

func (this *client) Destroy() {
	if this.destroyed {
		log.Debug("Destroy userID=%d this.destroyed", this.userIndex())
		return
	}
	this.destroyed = true
	userId := this.userInfo.GetUserId()
	log.Debug("Destroy userID=%d:%d err=%v", this.userIndex(), userId, this.m_ctx.Err())
	//log.Debug("%s", debug.Stack())
	sink.OnUserExit(this.userIndex())

	event.RemoveAllEvent(this.GetEventChannel())
	if userId > 0 {
		if sink.GetGameID() > 0 {
			go userlabel.TriggerEvent(userId, userlabel.Type_Game, userlabel.Scope{
				GameId:        sink.GetGameID(),
				IsFriendRoom:  sink.IsPrivateRoom(),
				OnlineSeconds: int(time.Now().Unix() - this.Log_login_time.Unix()),
				GoldAmount:    this.userInfo.GetUserGold() - this.loginGold,
			})
		}

		if sink.IsChipRoom() {
			go transaction.WriteChipSync(userId, sink.GetGameID(), 0, 0,
				0, sink.GetGameID()*100+99, sink.GetRoomName(), this.IP())
		} else {
			go transaction.WriteMoneySync(userId, sink.GetGameID(), 0, 0,
				0, sink.GetGameID()*100+99, sink.GetRoomName(), this.IP())
		}

	}
	if this.conn != nil {
		this.conn.Destroy()
		this.conn = nil
	}
	// 为保证上层应用在处理离开的时候数据还在，延迟删除
	time.AfterFunc(time.Duration(50)*time.Millisecond, func() {
		if userId > 0 {
			transaction.Trans_SetGameStatus(userId, sink.GetGameID(), 0, sink.GetRoomName(), sink.IsChipRoom())
			coreservice.FriendSetUserStatus(userId, 0, sink.GetRoomName())
		}
		this.gate.removeUser(this.userIndex())
		this.userInfo.Clear()
	})
}

func (this *client) Dump() {
	idle_time := int(time.Now().Sub(this.lastPing).Seconds())
	log.Release("Login[%v],Idle[%d 秒],%s",
		this.Log_login_time.Format("2006-01-02 15:04:05"), idle_time, this.userInfo.DumpUserInfo())
	if idle_time > 180 {
		log.Release("cleaning idle client")
		this.Close()
		this.onMainExit()
	}
}

func (this *client) logout() {
	// 为了防止新进的链接被清理掉，这里先把userid置0
	sink.OnUserExit(this.userIndex())
	this.userInfo.Clear()
	var retMsg message.BaseMsg
	retMsg.Msg = "logout"

	data, _ := json.Marshal(struct{ ErrorMsg string }{ErrorMsg: "账号已在其他地方登录，请留意账号安全!"})
	retMsg.Data = string(data)

	this.WriteMsg(retMsg.Msg, retMsg)
}

func (this *client) Login(msg, data string) {
	this.Log_login_time = time.Now()
	 obj := transaction.NewCheckUserPassword()
	 e := json.Unmarshal([]byte(data), &obj.IN)
	 if e != nil {
	 	this.onLoginFailed("login Unmarshal data failed")
		return
	}

	// obj.DoAction(nil)
	// ret := obj.State

	// if !ret || obj.Out.RetCode != 1 {
	// 	this.onLoginFailed("CheckUserPassword DoAction fail")
	// 	return
	// }
	
	user :=utils2.GetUserInfoData(obj.IN.UserID)
	
	this.userInfo.SetUserId(user.Data[0].UserId)
	this.userInfo.SetUserGold(user.Data[0].GoldCoins) 
	
	this.userInfo.SetUserBankamount(obj.Out.Bank)
	this.userInfo.SetUserNickName(user.Data[0].NickName)
	this.userInfo.SetUserFaceId(obj.Out.FaceID)
	this.userInfo.SetUserFaceUrl(user.Data[0].Avatar)
	this.userInfo.SetUserSex(user.Data[0].Gender)
	this.userInfo.SetUserSession(this.genSection())
	this.userInfo.SetUserWords(obj.Out.UserWords)
	this.userInfo.SetLogonTime(this.Log_login_time.Unix())
	this.userInfo.SetUserTeacher(obj.Out.TeacherID)
	this.userInfo.SetHigherUserID(obj.Out.HigherUserID)
	this.userInfo.SetGrade(obj.Out.Grade)
	this.userInfo.SetChip(obj.Out.ChipAmount)
	this.userInfo.SetChipBank(obj.Out.ChipBank)
	this.userInfo.SetCurrency(obj.Out.Currency)
	this.userInfo.SetCurrencyIsModify(obj.Out.CurrencyIsModify)
	this.userInfo.SetUTMSource(obj.Out.UTMSource)
	this.userInfo.SetPartnerId(obj.IN.PartnerId)
	this.userInfo.SetVersionCode(obj.IN.Version)
	this.userInfo.SetUserCharm(obj.Out.Charm)

	this.onLoginSuccess(msg, obj.Out.Code, obj.Out.IsGuest, 0, false)
}

func (this *client) loginByIMei(msg, data string) {
	this.Log_login_time = time.Now()
	obj := transaction.NewTransLogin()
	e := json.Unmarshal([]byte(data), &obj.In)
	if e != nil {
		this.onLoginFailed("loginByIMei Unmarshal data failed")
		return
	}

	log.Debug("client.loginByIMei %s", data)

	obj.In.IpAddress = this.IP()

	obj.In.NickName = coreservice.ParseKeyword(obj.In.NickName)

	obj.DoAction(nil)
	ret := obj.State

	if !ret || obj.Out.RetCode != 1 {
		if obj.Out.RetCode == 13 {
			this.onLoginFailed("13")
			return
		}
		log.Release("loginByIMei DoAction fail RetCode = %d", obj.Out.RetCode)
		this.onLoginFailed("13")
		return
	}

	// 非普通金币大厅不自动进入
	if sink.GetChipRoom() != 0 {
		obj.Out.AutoLoginChip = 0
		obj.Out.Chip = 0
		obj.Out.ChipBank = 0
	}

	log.Debug("client.loginByIMei [%d] logined", obj.Out.UserID)

	this.userInfo.SetUserId(obj.Out.UserID)
	this.userInfo.SetUserNickName(obj.Out.NickName)
	this.userInfo.SetUserFaceId(obj.Out.FaceID)
	this.userInfo.SetUserFaceUrl(obj.Out.FaceUrl)
	this.userInfo.SetUserSex(obj.Out.Sex)
	this.userInfo.SetUserGold(obj.Out.Money)
	this.userInfo.SetUserBankamount(obj.Out.Bank)
	this.userInfo.SetUserPayAmount(obj.Out.PayAmount)
	this.userInfo.SetUserSession(this.genSection())
	this.userInfo.SetUserWords(obj.Out.UserWords)
	this.userInfo.SetLogonTime(this.Log_login_time.Unix())
	this.userInfo.SetUserTeacher(obj.Out.TeacherId)
	this.userInfo.SetHigherUserID(obj.Out.HigherUserID)
	this.userInfo.SetGrade(obj.Out.Grade)
	this.userInfo.SetChip(obj.Out.Chip)
	this.userInfo.SetChipBank(obj.Out.ChipBank)
	this.userInfo.SetCurrency(obj.Out.Currency)
	this.userInfo.SetCurrencyIsModify(obj.Out.CurrencyIsModify)
	this.userInfo.SetUTMSource(obj.Out.UTMSource)
	this.userInfo.SetAutoLoginChip(obj.Out.AutoLoginChip)
	this.userInfo.SetPartnerId(obj.In.PartnerID)
	this.userInfo.SetVersionCode(obj.In.Version)
	this.userInfo.SetUserCharm(obj.Out.Charm)

	//头像有变化,更新头像
	if obj.In.FaceUrl != "" && obj.In.FaceUrl != obj.Out.FaceUrl {
		this.userInfo.SetUserFaceUrl(obj.In.FaceUrl)
		objFace := transaction.NewChangeFace()
		objFace.IN.UserID = obj.Out.UserID
		objFace.IN.Sex = obj.Out.Sex
		objFace.IN.FaceUrl = obj.In.FaceUrl
		go objFace.DoAction(nil)
	}

	gate.addMessageRecord(obj.Out.UserID, fmt.Sprintf("[%s]:{%s}", msg, data))

	// 新注册
	if obj.Out.IsRegister {

		// 昵称有变化
		if obj.In.NickName != "" && obj.In.NickName != obj.Out.NickName {
			this.userInfo.SetUserNickName(obj.In.NickName)
			objNick := transaction.NewTransChangeNickName()
			objNick.In.UserID = obj.Out.UserID
			objNick.In.NewNickName = obj.In.NickName
			go objNick.DoAction(nil)
		}

		// 记录渠道流量
		if source, content := obj.UTMInsert(); len(source) > 0 {
			this.userInfo.SetUTMSource(source)

			// 代理自动绑定
			if source == "agentinvite" {

				// 上级id
				higherUserId, err := strconv.Atoi(content)
				if err != nil {
					log.Error("loginByIMei UTM content error %v", err)
				}

				// 绑定代理
				if this.bindAgent(obj.Out.UserID, higherUserId, obj.In.IpAddress) {
					this.userInfo.SetHigherUserID(higherUserId)
				}
			}
		}
	}

	// 打点统计
	go dotservice.AddDot(obj.Out.UserID, dotservice.DotScope{
		Scene:  dotservice.Scene_Login,
		Action: dotservice.Action_Complete,
		Extra:  this.userInfo.GetUTMSource(),
	})

	// 触发用户标签
	go userlabel.TriggerEvent(obj.Out.UserID, userlabel.Type_Login, userlabel.Scope{
		IPAddress:  obj.In.IpAddress,
		IMei:       obj.In.Deviceid,
		IsRegister: obj.Out.IsRegister,
		UTMSource:  this.userInfo.GetUTMSource(),
		IsReturn:   obj.Out.IsReturn,
	})

	this.onLoginSuccess(msg, obj.Out.Code, obj.Out.IsGuest, obj.Out.ChipSend, obj.Out.IsRegister)
	log.Debug("client.loginByIMei [%d] loginSuccess", obj.Out.UserID)
	if obj.In.MessageToken != "" {
		deviceType := obj.In.PartnerID/10000 - 1
		remotemessage.SetUserToken(obj.Out.UserID, deviceType, obj.In.MessageToken)
	}
	// 领取回归奖励
	go func(userId int, isReturn bool) {
		if !isReturn {
			return
		}
		time.Sleep(1 * time.Second)
		coreservice.GiftReturnAward(userId)
	}(obj.Out.UserID, obj.Out.IsReturn)
}

func (this *client) bindAgent(userId, higherUserId int, ipAddress string) bool {
	var ret struct {
		RetCode int
		Items   []item.ItemPack
	}

	// 去绑定师徒
	resp := coreservice.AgentBind(userId, higherUserId, ipAddress)

	if resp.RetCode == 1 {
		if err := json.Unmarshal([]byte(resp.Data), &ret); err != nil {
			log.Error("client.bindAgent unmarshal err %v", err)
		}
	}

	log.Debug("client.bindAgent userId=%d higherUserId=%d ipAddress=%s resp.Data=%+v", userId, higherUserId, ipAddress, resp.Data)

	return ret.RetCode == 1
}

func (this *client) loginByToken(msg, data string) {
	var section struct {
		UserId      int
		Token       string
		PartnerId   int
		VersionCode int
	}

	e := json.Unmarshal([]byte(data), &section)
	if e != nil {
		errorstring := fmt.Sprintf("send session login return Marshal failed err=%v,data = %v", e, data)
		this.onLoginFailed(errorstring)
		return
	}

	if section.UserId == 0 {
		log.Release("client.loginByToken section.UserId == 0")
		return
	}

	if !this.checkSection(section.UserId, section.Token) {
		this.onLoginFailed("登录已超时，请重新登录")
		return
	}

	this.doLoginByToken(section.UserId, section.Token, msg, section.PartnerId, section.VersionCode)
}

func (this *client) doLoginByToken(userId int, token string, msg string, partnerId, versionCode int) {
	this.userInfo.SetUserId(userId)
	this.userInfo.SetUserSession(token)

	// 登陆成功，需要获取所有的登陆数据
	obj := transaction.NewTransGetMyInfo()
	obj.In.UserID = userId
	obj.DoAction(nil)
	ret := obj.State
	if !ret {
		this.onLoginFailed("登录失败，获取个人信息失败")
		return
	}

	this.userInfo.SetUserNickName(obj.Out.NickName)
	this.userInfo.SetUserFaceId(obj.Out.FaceID)
	this.userInfo.SetUserFaceUrl(obj.Out.FaceUrl)
	this.userInfo.SetUserSex(obj.Out.Sex)
	this.userInfo.SetUserGold(obj.Out.Amount)
	this.userInfo.SetUserBankamount(obj.Out.Bank)
	this.userInfo.SetUserPayAmount(obj.Out.PayMoney)
	this.userInfo.SetUserWords(obj.Out.UserWords)
	this.userInfo.SetLogonTime(this.Log_login_time.Unix())
	this.userInfo.SetUserTeacher(obj.Out.TeacherId)
	this.userInfo.SetHigherUserID(obj.Out.HigherUserID)
	this.userInfo.SetGrade(obj.Out.Grade)
	this.userInfo.SetChip(obj.Out.ChipAmount)
	this.userInfo.SetChipBank(obj.Out.ChipBank)
	this.userInfo.SetCurrency(obj.Out.Currency)
	this.userInfo.SetCurrencyIsModify(obj.Out.CurrencyIsModify)
	this.userInfo.SetUTMSource(obj.Out.UTMSource)
	this.userInfo.SetPartnerId(partnerId)
	this.userInfo.SetVersionCode(versionCode)
	this.userInfo.SetUserCharm(obj.Out.Charm)

	this.onLoginSuccess(msg, obj.Out.Code, obj.Out.IsGuest, 0, false)
}

func (this *client) forceLogin(userId int) {
	sectionKey := this.getSectionKey(userId)
	t, ok := redis.String_Get(sectionKey)
	if !ok {
		t = this.genSectionByUserId(userId)
	}
	this.doLoginByToken(userId, t, "loginByToken", 0, 0)
}

func (this *client) checkSection(userID int, token string) bool {
	if token == "" {
		return false
	}
	sectionKey := this.getSectionKey(userID)
	t, ok := redis.String_Get(sectionKey)
	if !ok {
		log.Release("redis error")
		return false
	}
	if t != token {
		return false
	}
	// 刷新有效期
	redis.String_SetEx(sectionKey, t, EXPIRE_TIME)
	return true
}

func (this *client) getSectionKey(userID int) string {
	return fmt.Sprintf("Section:%d", userID)
}

func (this *client) genSectionByUserId(userId int) string {
	id, err := uuid.NewV4()
	token := ""
	if err != nil {
		log.Release("genSection failed %v", err)
		token = fmt.Sprintf("%v", time.Now())
	} else {
		token = id.String()
	}
	redis.String_SetEx(this.getSectionKey(userId), token, EXPIRE_TIME)
	return token
}

func (this *client) genSection() string {
	userId := this.userInfo.GetUserId()
	if userId <= 0 {
		log.Debug("client.genSection UserId = %d", userId)
		return ""
	}
	id, err := uuid.NewV4()
	token := ""
	if err != nil {
		log.Release("genSection failed %v", err)
		token = fmt.Sprintf("%v", time.Now())
	} else {
		token = id.String()
	}
	redis.String_SetEx(this.getSectionKey(userId), token, EXPIRE_TIME)
	return token
}

func (this *client) onLoginFailed(errorstring string) {
	var retMsg message.BaseMsg
	retMsg.Msg = "login_failed"
	log.Release("client.onLoginFailed %s", errorstring)
	retMsg.Data = errorstring
	this.WriteMsg(retMsg.Msg, retMsg)
	// 因为发送数据为异步过程，延迟结束网络
	go func() {
		time.Sleep(100 * time.Millisecond)
		this.onMainExit()
	}()
}

func (this *client) onLoginSuccess(msg string, code, isGuest, chipSend int, isRegister bool) {
	userId := this.userInfo.GetUserId()
	this.loginGold = this.userInfo.GetUserGold()
	// 发送成功指令
	this.userInfo.SetUserStatus(user.UserStatus_Free)
	// 获取用户的装扮
	this.userInfo.SetDecorations(userservices.GetUserDecoration(userId))
	// 获取徽章
	this.userInfo.SetBadges(badge.GetBadgeWearAndShow(userId))
	userVip := vipservice.GetUserVipInfo(userId)
	if userVip != nil {
		this.userInfo.SetUserVipLevel(userVip.Level, userVip.Point, userVip.Expire)
	}

	loginAward, isWhite := this.getLevel(userId)
	if !this.isRobot() {
		this.checkRedPoint(userId)
	}

	// 数据库币种为空，允许修改
	if this.userInfo.GetCurrency() == "" {
		// 根据ip获取币种
		//currency := coreservice.GetCurrency(userId, this.IP())
		currency := "SAR"

		//log.Debug("getCurrency userId=%d ipAddress=%s currency=%s", userId, this.IP(), currency)
		this.userInfo.SetCurrency(currency)
	}

	var retMsg message.BaseMsg
	retMsg.Msg = msg
	ul := this.userInfo.GetUserInfo_Login(code, isGuest, chipSend, loginAward, isWhite, isRegister)
	d, _ := json.Marshal(ul)
	retMsg.Data = string(d)
	this.WriteMsg(retMsg.Msg, retMsg)

	info := message.IsChipRoom{IsChipRoom: sink.IsChipRoom(), IsLadderRoom: sink.IsLadderRoom()}
	retMsg.Msg = message.IsChipRoomMsg
	d, _ = json.Marshal(info)
	retMsg.Data = string(d)
	this.WriteMsg(retMsg.Msg, retMsg)

	go transaction.Trans_SetGameStatus(userId, sink.GetGameID(), 1, sink.GetRoomName(), sink.IsChipRoom())
	go coreservice.FriendSetUserStatus(userId, 1, sink.GetRoomName())
	this.gate.userLogined(this.userIndex(), userId)
}

func (this *client) getLevel(userId int) (int, int) {
	info := userservices.GetUserLevel(userId)
	this.userInfo.SetUserLevel(info.Level)
	this.userInfo.SetUserExperience(info.Experience)
	return info.LoginAward, info.IsWhite
}

func (this *client) refreshGold() {
	userId := this.userInfo.GetUserId()
	if userId == 0 {
		log.Debug("user.refreshGold userId = 0")
		return
	}
	obj := transaction.NewTransGetMyInfo()
	obj.In.UserID = userId
	obj.DoAction(nil)

	ret := obj.State

	if !ret {
		log.Debug("client.refreshGold failed")
		return
	}
	if sink.GetGameID() != 0 {
		this.userInfo.SetUserGold(obj.Out.Amount)
		this.userInfo.SetChip(obj.Out.ChipAmount)
		d, _ := json.Marshal(message.UserScoreChange{UserId: userId, Gold: obj.Out.Amount, Chip: obj.Out.ChipAmount})
		gate.broadcastData(message.Frame_UserScoreChange, string(d))
	}
}

func (this *client) checkRedPoint(userId int) {
	var info struct {
		MailVipTip bool // 重要邮件(含附件)
	}
	resp := coreservice.CheckRedPoint(userId)
	if err := json.Unmarshal([]byte(resp.Data), &info); err != nil {
		log.Error("checkRedPoint unmarshal data failed %v", err)
	}

	this.userInfo.SetMailVipTip(info.MailVipTip)
}
