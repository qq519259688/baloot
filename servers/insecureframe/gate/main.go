package gate

import (
	"bet24.com/log"
	"bet24.com/redis"
	"bet24.com/servers/insecureframe/message"
	notification "bet24.com/servers/micros/notification/proto"
	"bet24.com/servers/transaction"
	"bet24.com/servers/user"
	"encoding/json"
	utils2 "bet24.com/servers/insecureframe/gate/Utils"
)

var stopping bool

func Run(sink GateSink, logPath string) {
	stopping = false
	run(sink, logPath)
}

func DumpUsers() {
	//runtime.GC()
	log.Release("Totoal Connections = %d", gate.wsServer.GetConnectionCount())
	log.Release("Totoal UserCount = %d", len(gate.userlist))
	log.Release("Totoal Logined = %d", gate.getLoginedCount())
	for _, v := range gate.userlist {
		if !v.isRobot() {
			v.Dump()
		}
	}
}

func DumpRobots() {
	log.Release("Totoal Robot = %d", gate.getRobotCount())
	for _, v := range gate.userlist {
		if v.isRobot() {
			v.Dump()
		}
	}
}

func DumpMessageRecord(userId int) {
	gate.msgRecord.dump(userId)
}

func ClearMessageRecord(userId int) {
	gate.msgRecord.clear(userId)
}

func GetUserCount() int {
	return gate.getUserCount()
}

func GetPlayerCount() int {
	return gate.getPlayerCount()
}

func GetLoginedCount() int {
	return gate.getLoginedCount()
}

func GetUserInfo(userIndex int32) *user.UserInfo {
	return gate.getUserInfo(userIndex)
}

func GetUserList() []*user.UserInfo {
	return gate.getUserList()
}

func GetUserByUserId(userId int) *user.UserInfo {
	u := gate.getUserByUserID(userId)
	if u == nil {
		return nil
	}
	return u.userInfo
}

func SendMessage(userIndex int32, msg, data string) bool {
	u := gate.getUser(userIndex)
	if u == nil {
		log.Debug("gate.SendMessage failed %d,%s", userIndex, msg)
		sink.OnUserExit(userIndex)
		return false
	}

	if u.userInfo.IsRobot() {
		return true
	}
	m := message.BaseMsg{Data: data, Msg: msg}
	u.WriteMsg(msg, m)
	return true
}

func SendRawMessage(userIndex int32, rawData string) {
	u := gate.getUser(userIndex)
	if u == nil {
		log.Debug("gate.SendRawMessage failed %d,%s", userIndex, rawData)
		return
	}
	u.writeRawData(rawData)
}

func GetEventChannel(userIndex int32) chan interface{} {
	u := gate.getUser(userIndex)
	if u == nil {
		log.Debug("gate.GetEventChannel failed %d", userIndex)
		return nil
	}
	return u.GetEventChannel()
}

func GetUserIP(userIndex int32) string {
	u := gate.getUser(userIndex)
	if u == nil {
		log.Debug("gate.GetUserIP failed %d", userIndex)
		return ""
	}
	return u.IP()
}

func GetUserIPByUserID(userID int) string {
	u := gate.getUserByUserID(userID)
	if u == nil {
		log.Debug("gate.GetUserIPByUserID failed %d", userID)
		return ""
	}
	return u.IP()
}

func WriteUserMoneyWithModifyAmount(userId int, amount, tax int, status, scoreType int, sourceName string, isChip bool) int {
	if isChip {
		log.Release("frame.WriteUserMoneyWithModifyAmount not support chip room")
		return 0
	}
	u := gate.getUserByUserID(userId)
	if u == nil {
		_, ret := transaction.WriteMoneySyncWithModifyAmount(userId, sink.GetGameID(), amount, tax, status, scoreType, sourceName, "")
		return ret
	}

	// 机器人不写分
	if u.userInfo.IsRobot() {
		gold := u.userInfo.GetUserGold()
		if gold+amount < 0 {
			amount = -gold
		}
		u.userInfo.SetUserGold(gold + amount)

		return amount
	}

	balance, ret := transaction.WriteMoneySyncWithModifyAmount(userId, sink.GetGameID(), amount, tax, status,
		scoreType, sourceName, u.IP())
	if amount != 0 {
		go notification.AddNotification(userId, notification.Notification_Gold, "")
	}
	u.userInfo.SetUserGold(balance)

	// 游戏才需要广播
	if sink.GetGameID() != 0 {
		d, _ := json.Marshal(message.UserScoreChange{UserId: userId, Gold: u.userInfo.GetUserGold(), Chip: u.userInfo.GetChip()})
		gate.broadcastData(message.Frame_UserScoreChange, string(d))
	}
	return ret
}
type UserInfoReq struct {
	GameType    int        `json:"gameType"`    //游戏类型: 1.ludo 2.其他
	UserWallets []UserInfo `json:"userWallets"` //结算时的游戏排行
}
type UserInfo struct {
	GoldCoins int `json:"goldCoins"` //数量
	Type      int `json:"type"`      //类型: 1.加 2.减
	UserId    int `json:"userId"`    // ⽤户ID

}
func WriteUserMoney(userId int, amount, tax int, status, scoreType int, sourceName string, isChip bool) (bool, int) {
	u := gate.getUserByUserID(userId)

	//var userInfoReq = UserInfoReq{GameType: 1, UserWallets: userInfos}
	var userInfo = UserInfo{
		UserId:    userId,
		GoldCoins: amount-tax,
	//	Type:      1,
	}
	if amount>=0{
		userInfo.Type=1
	}else {
		userInfo.Type=2
	}


	var userInfos = []UserInfo{}
	userInfos = append(userInfos, userInfo)
	var userInfoReq = UserInfoReq{GameType: 1, UserWallets: userInfos}
		jsonData2, _ := json.Marshal(userInfoReq)
		utils2.PostData(jsonData2)

	if u == nil {
		// 离线写分
		if isChip {
			go transaction.WriteChipSync(userId, sink.GetGameID(), amount, tax,
				status, scoreType, sourceName, "127.0.0.1")
			if amount != 0 {
				go notification.AddNotification(userId, notification.Notification_Chip, "")
			}
		} else {
			go transaction.WriteMoneySync(userId, sink.GetGameID(), amount, tax,
				status, scoreType, sourceName, "127.0.0.1")
			if amount != 0 {
				go notification.AddNotification(userId, notification.Notification_Gold, "")
			}

		}

		if amount != 0 {
			var d redis.Channel_msg
			d.Message = "RefreshGold"
			d.UserID = userId
			js, _ := json.Marshal(d)
			redis.Publish(string(js))
		}
		return true, 0
	}
	balance := 0
	// 机器人不写分
	if u.userInfo.IsRobot() {
		gold := u.userInfo.GetUserGold()
		if isChip {
			gold = u.userInfo.GetChip()
		}
		if gold+amount < 0 {
			amount = -gold
		}
		if isChip {
			u.userInfo.SetChip(gold + amount)
		} else {
			u.userInfo.SetUserGold(gold + amount)
		}
		balance = gold + amount
	} else {
		var isSuceeded bool
		var retCode int
		var blc int
		if isChip {
			isSuceeded, retCode, blc = transaction.WriteChipSync(userId, sink.GetGameID(), amount, tax,
				status, scoreType, sourceName, u.IP())
		} else {
			isSuceeded, retCode, blc = transaction.WriteMoneySync(userId, sink.GetGameID(), amount, tax,
				status, scoreType, sourceName, u.IP())
		}

		if !isSuceeded {
			log.Release("WriteUserMoney failed,UserId[%d],amount[%d],retCode[%d]", userId, amount, retCode)
			return false, balance
		}
		balance = blc
		if isChip {
			if amount != 0 {
				go notification.AddNotification(userId, notification.Notification_Chip, "")
			}
			u.userInfo.SetChip(balance)
		} else {
			if amount != 0 {
				go notification.AddNotification(userId, notification.Notification_Gold, "")
			}
			u.userInfo.SetUserGold(balance)
		}
	}

	// 游戏才需要广播
	if sink.GetGameID() != 0 {
		d, _ := json.Marshal(message.UserScoreChange{UserId: userId, Gold: u.userInfo.GetUserGold(), Chip: u.userInfo.GetChip()})
		gate.broadcastData(message.Frame_UserScoreChange, string(d))
	}

	return true, balance
}

func KickUser(userIndex int32) bool {
	return gate.kickUser(userIndex)
}

func BroadcastData(msg, data string) {
	gate.broadcastData(msg, data)
}

func RefreshGold(userIndex int32) {
	gate.refreshGold(userIndex)
}

type GateSink interface {
	GetServerPort() int
	OnUserEnter(userIndex int32)
	OnUserLogined(userIndex int32)
	OnUserExit(userIndex int32)
	OnGameMessage(userIndex int32, userID int, msg, data string) bool
	GetGameID() int
	GetGameName() string
	GetRoomName() string
	GetCertFile() string
	GetKeyFile() string
	//机器人配置
	GetRobotCount() int
	GetRobotGoldLimit() (min, max int)
	GetRobotOnlineSec() int
	IsChipRoom() bool
	GetChipRoom() int
	OnPlatformConfig(key string)
	IsPrivateRoom() bool
	IsLadderRoom() bool
}

func StopServer() {
	stopping = true
	gate.removeUser(-1)
}

func LoginAUser(userId int) *user.UserInfo {
	index := gate.GenUserIndex()
	a := NewClient(index, nil, gate, false)
	go a.Run()
	a.forceLogin(userId)
	return GetUserInfo(index)
}

func SilentRemoveUser(userId int) {
	gate.silentRemoveUser(userId)
}
