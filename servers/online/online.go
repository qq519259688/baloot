package online

import (
	"fmt"
	"strconv"
	"time"

	_ "bet24.com/log"
	"bet24.com/redis"
)

/*
	redis缓存在线
	每隔1分钟上报在线，在线超时为1.5分钟
	缓存格式为：
	online_WebProxy_ServerID ,Online
	online_DataServer_ServerID,Online
*/

const Online_Expire_Seconds = 120

const (
	ServerType_WebProxy    = "WebProxy"
	ServerType_DataServer  = "DataServer"
	ServerType_MatchServer = "MatchServer"
	ServerType_ClubServer  = "ClubServer"
)

var Instance *online

type online struct {
	ServerID   string
	ServerType string
	OnlineFunc func() int
}

func (o *online) Run() {
	o.UpdateOnline()
}

func (o *online) UpdateOnline() {
	time.AfterFunc(time.Minute, o.UpdateOnline)
	onlineCount := o.OnlineFunc()
	o.writeOnlineToDB(onlineCount)
}

func (o *online) writeOnlineToDB(onlineCount int) {
	key := fmt.Sprintf("online:%s:%s", o.ServerType, o.ServerID)
	redis.String_SetEx(key, strconv.Itoa(onlineCount), Online_Expire_Seconds)
}

func Run(serverID, serverType string, onlineFunc func() int) {
	Instance = new(online)
	Instance.ServerID = serverID
	Instance.ServerType = serverType
	Instance.OnlineFunc = onlineFunc
	Instance.Run()
}

// 获取指定类型服务器的在线人数，如果serverType为空，则取所有在线
func GetOnlineCount(serverType string) int {
	var key string
	if serverType == "" {
		key = fmt.Sprintf("online:*")
	} else {
		key = fmt.Sprintf("online:%s:*", serverType)
	}
	// 模糊查询所有的在线服务器纪录
	servers := redis.Key_GetKeys(key)

	ret := 0
	for _, v := range servers {
		ret += redis.String_GetInt(v)
	}
	return ret
}
