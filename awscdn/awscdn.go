package main

import (
	"fmt"
	"os"
)

func main() {
	defer func() {
		fmt.Println("结束，按任意键退出")
		var endInput string
		fmt.Scanln(&endInput)
	}()

	if !loadConfig() {
		fmt.Println("加载配置文件失败，青检查awscdn.json文件")
		return
	}

	if len(os.Args) > 1 {
		if os.Args[1] == "invalidate" {
			if !doInvalidate() {
				fmt.Println("刷新cdn失败，请重新尝试")
				return
			}
			return
		}

		if os.Args[1] == "show" {
			doViewList()
			return
		}
	}

	if !doUpload() {
		fmt.Println("上传失败，请重新尝试")
		return
	}
	if !doInvalidate() {
		fmt.Println("刷新cdn失败，请重新尝试")
		return
	}
	flushCdn()
}
