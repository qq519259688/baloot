package main

import (
	"encoding/json"
	"fmt"
	"os"
)

var config struct {
	Key    string
	Secret string
	DistId string

	Ftp_url      string
	Ftp_user     string
	Ftp_password string
	Delta_path   string
	Version_file string

	Pattern string
}

func loadConfig() bool {
	data, err := os.ReadFile("awscdn.json")
	if err != nil {
		fmt.Println("awscdn.loadConfig.loadData read awscdn failed")
		return false
	}

	err = json.Unmarshal(data, &config)
	if err != nil {
		fmt.Println("awscdn.loadConfig.loadData Unmarshal failed", err)
		return false
	}

	fmt.Println("awscdn.loadConfig success")
	return true
}
