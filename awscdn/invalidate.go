package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudfront"
	"time"
)

func createSession() *cloudfront.CloudFront {
	customCreds := credentials.NewStaticCredentials(config.Key, config.Secret, "")
	s := session.New(&aws.Config{Credentials: customCreds})

	if e, a := customCreds, s.Config.Credentials; e != a {
		fmt.Printf("expect %v, got %v\n", e, a)
		return nil
	}
	return cloudfront.New(s)
}

func doViewList() {
	svc := createSession()
	if svc == nil {
		fmt.Println("doViewList createSession failed")
		return
	}
	if err := viewListInvalidations(svc); err != nil {
		fmt.Println(err)
	}
}

func doInvalidate() bool {
	svc := createSession()
	if svc == nil {
		return false
	}
	if err := viewListInvalidations(svc); err != nil {
		fmt.Println(err)
	}

	pattern := "/hotfix_asset*"
	if config.Pattern != "" {
		pattern = config.Pattern
	}
	if err := createInvalidationRequest(svc, pattern); err != nil {
		fmt.Println(err)
		return false
	}

	if err := viewListInvalidations(svc); err != nil {
		fmt.Println(err)
	}
	return true
}

func viewListInvalidations(svc *cloudfront.CloudFront) error {
	resp, err := svc.ListInvalidations(&cloudfront.ListInvalidationsInput{
		DistributionId: aws.String(config.DistId),
	})
	if err != nil {
		return fmt.Errorf("list.inval. err:%v", err.Error())
	}

	fmt.Printf("invs:%v\n", resp)
	return nil
}

func createInvalidationRequest(svc *cloudfront.CloudFront, pattern string) error {
	now := time.Now()
	resp, err := svc.CreateInvalidation(&cloudfront.CreateInvalidationInput{
		DistributionId: aws.String(config.DistId),
		InvalidationBatch: &cloudfront.InvalidationBatch{
			CallerReference: aws.String(
				fmt.Sprintf("goinvali%s", now.Format("2006/01/02,15:04:05"))),
			Paths: &cloudfront.Paths{
				Quantity: aws.Int64(1),
				Items: []*string{
					aws.String(pattern),
				},
			},
		},
	})

	if err != nil {
		return fmt.Errorf("create.inval. err:%v", err.Error())
	}

	fmt.Printf("invs:%v\n", resp)
	return nil
}
