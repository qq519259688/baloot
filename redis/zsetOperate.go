package redis

import (
	"github.com/gomodule/redigo/redis"
	"bet24.com/log"
	"strconv"
)

type Score_member struct {
	Score  string
	Member string
}

//执行一个有序zset插入
func Zset_ZAdd(key string, score int, member string, expire int) bool {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Zset_ZAdd no redis connection")
		return false
	}
	defer rc.Close()
	_, err := rc.Do("ZADD", key, score, member)
	if err != nil {
		log.Release("Zset_ZAdd key=%v score=%v memeber=%v expire=%v err=%v", key, score, member, expire, err)
		return false
	}
	if expire > 0 {
		rc.Do("expire", key, expire)
	}
	return true
}

func Zset_ZInc(key string, score int, member string) bool {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Zset_ZAdd no redis connection")
		return false
	}
	defer rc.Close()
	_, err := rc.Do("ZINCRBY", key, score, member)
	if err != nil {
		log.Release("Zset_ZInc key=%v score=%v member=%v err=%v", key, score, member, err)
		return false
	}
	return true
}

func Zset_ZRem(key string, member string) bool {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Zset_ZRem no redis connection")
		return false
	}
	defer rc.Close()
	_, err := rc.Do("ZREM", key, member)
	if err != nil {
		log.Release("ZREM key=%v member=%v err=%v", key, member, err)
		return false
	}
	return true
}

func Zset_ZScore(key string, member string) int {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Zset_ZAdd no redis connection")
		return 0
	}
	defer rc.Close()
	r, err := redis.String(rc.Do("ZSCORE", key, member))
	if err != nil {
		log.Debug("%v", err)
		return 0
	}

	ri, e := strconv.Atoi(r)
	if e != nil {
		log.Debug("%v", e)
		return 0
	}
	return ri
}

//读取指定zset
func Zset_ZRange(key string, start, stop int, desc bool, withScore bool) ([]Score_member, bool) {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Zset_ZRange no redis connection")
		return nil, false
	}
	defer rc.Close()
	cmd := "ZRANGE"
	if desc {
		cmd = "ZREVRANGE"
	}
	var rows []string
	var err error
	if withScore {
		rows, err = redis.Strings(rc.Do(cmd, key, start, stop, "WITHSCORES"))
	} else {
		rows, err = redis.Strings(rc.Do(cmd, key, start, stop))
	}

	if err != nil {
		log.Release("Zset_ZRange key=%v start=%v stop=%v desc=%v withScore=%v err=%v", key, start, stop, desc, withScore, err)
		return nil, false
	}

	rowsLen := len(rows) / 2
	if rowsLen <= 0 {
		log.Debug("Zset_ZRange no data")
		return nil, false
	}

	score_member_list := make([]Score_member, rowsLen)

	for i := 0; i < len(rows)-1; i += 2 {
		score_member_list[i/2].Member = string(rows[i])
		score_member_list[i/2].Score = string(rows[i+1])
	}

	return score_member_list, true
}

func Zset_ZRemByScore(key string, min, max int) {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Zset_ZRange no redis connection")
		return
	}
	defer rc.Close()
	rc.Do("ZREMRANGEBYSCORE", key, min, max)
}
