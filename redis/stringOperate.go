package redis

import (
	"strconv"

	"github.com/gomodule/redigo/redis"
	"bet24.com/log"
)

//写数据
func String_Set(key, value string) bool {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("String_Set no redis connection")
		return false
	}
	defer rc.Close()
	_, err := rc.Do("SET", key, value)
	if err != nil {
		log.Release("String_Set key=%v value=%v err=%v", key, value, err)
		return false
	}
	return true
}

//写过期数据
func String_SetEx(key, value string, seconds int) bool {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("String_SetEx no redis connection")
		return false
	}
	defer rc.Close()
	_, err := rc.Do("SETEX", key, seconds, value)
	if err != nil {
		log.Release("String_SetEx key=%v value=%v seconds=%v err=%v", key, value, seconds, err)
		return false
	}
	return true
}

//获取数据
func String_Get(key string) (string, bool) {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("String_Get no redis connection")
		return "", false
	}
	defer rc.Close()
	ret, err := redis.String(rc.Do("GET", key))
	if err != nil {
		//log.Release("String_Get key=%v err=%v", key, err)
		return "", true
	}
	return ret, true
}

//递增
func String_Incrby(key string, value int) (int, bool) {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("String_Incrby no redis connection")
		return 0, false
	}
	defer rc.Close()
	ret, err := redis.Int(rc.Do("INCRBY", key, value))
	if err != nil {
		log.Release("String_Incrby key=%v value=%v err=%v", key, value, err)
		return 0, false
	}

	return ret, true
}

func String_GetInt(key string) int {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("String_GetInt no redis connection")
		return 0
	}
	defer rc.Close()
	ret, err := redis.String(rc.Do("GET", key))
	if err != nil {
		//log.Debug("String_Get key=%v err=%v", key, err)
		return 0
	}
	r, e := strconv.Atoi(ret)
	if e != nil {
		return 0
	}
	return r
}

//在offset 位置写数据,返回: 指定偏移量原来储存的位
func String_SetBit(key string, offset int, value int) (int, bool) {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("String_SetBit no redis connection")
		return -1, false
	}
	defer rc.Close()
	//不是0, 1
	if value != 0 && value != 1 {
		return -1, false
	}

	i, err := redis.Int(rc.Do("SETBIT", key, offset, value))
	if err != nil {
		log.Release("String_SetBit key=%v offset=%v value=%v err=%v", key, offset, value, err)
		return -1, false
	}

	return i, true
}

//String_BITCOUNT 统计字符串被设置为1的bit数(注意: start, end 是指字节位置).
func String_BITCOUNT(key string, start int, end int) (int, bool) {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("String_BITCOUNT no redis connection")
		return -1, false
	}
	defer rc.Close()
	i, err := redis.Int(rc.Do("BITCOUNT", key, start, end))
	if err != nil {
		log.Release("String_BITCOUNT key=%v start=%v end=%v err=%v", key, start, end, err)
		return -1, false
	}

	return i, true
}

//String_GETSET 写数据并获取写前的旧数据
func String_GETSET(key string, value string) (string, bool) {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("String_Get no redis connection")
		return "", false
	}
	defer rc.Close()
	ret, err := redis.String(rc.Do("GETSET", key, value))

	if err != nil {
		//redis 返回是 nil, 还没有 key
		if err == redis.ErrNil {
			return "", true
		}

		//其他错误
		return "", false
	}
	return ret, true
}
