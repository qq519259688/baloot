package redis

type Channel_msg_base struct {
	Message string
	// 其他数据json信息
	Data string
}

type Channel_msg struct {
	Channel_msg_base
	UserID    int    // 用户ID
	IPAddress string //ip地址

}

const (

	//来自webproxy服务器
	UserEnterWebproxy = "UserEnterWebproxy"

	//来自DataServer服务器
	RefreshTask_From_Dataserver = "RefreshTask_From_Dataserver"
	RankingRise_From_DataServer = "RankingRise_From_DataServer"

	//其他
	UserEnterDataserver    = "UserEnterDataserver"
	Check_NewStatus_By_Web = "Check_NewStatus_By_Web"
	UserOutWebproxy        = "UserOutWebproxy"
	UserOutDataserver      = "UserOutDataserver"

	//来自邀请服务器
	InviteStart_From_InviteServer = "InviteStart_From_InviteServer"

	//来自WEB站点
	WhiteUsers_From_Web  = "WhiteUsers_From_Web"
	CreateRoom_From_Web  = "CreateRoom_From_Web"
	ApplyEnter_From_Web  = "ApplyEnter_From_Web"
	ApplyHandle_From_Web = "ApplyHandle_From_Web"
)

type UserInfo struct {
	NickName string
}
