package redis

import (
	"github.com/gomodule/redigo/redis"
	"bet24.com/log"
)

//向名称为key设置过期时间
func Key_EXPIRE(key string, seconds int) bool {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Key_EXPIRE no redis connection")
		return false
	}
	defer rc.Close()

	i, err := redis.Int(rc.Do("EXPIRE", key, seconds))

	if err != nil {
		log.Release("Key_EXPIRE key=%v seconds=%v err=%v", key, seconds, err)
		return false
	}

	if i == 1 {
		return true
	}

	return false
}

//是否存在某个key
func Key_EXISTS(key string) bool {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Key_EXISTS no redis connection")
		return false
	}
	defer rc.Close()

	i, err := redis.Int(rc.Do("EXISTS", key))

	if err != nil {
		log.Release("Key_EXISTS key=%v err=%v", key, err)
		return false
	}
	return i >= 1
}

func Key_Del(key string) {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Key_Del no redis connection")
		return
	}
	defer rc.Close()
	_, err := redis.Int(rc.Do("DEL", key))
	if err != nil {
		log.Release("Key_Del key=%v err=%v", key, err)
	}
}

func Key_GetKeys(key string) []string {
	rc := RedisClient.Get()
	var ret []string
	if rc == nil {
		log.Release("Key_GetKeys no redis connection")
		return ret
	}
	defer rc.Close()
	r, err := redis.Strings(rc.Do("KEYS", key))
	if err != nil {
		log.Release("Key_GetKeys key=%v err=%v", key, err)
		return ret
	}
	return r
}

func Key_DelKeys(keys string) int {
	ret := 0
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Key_DelKeys no redis connection")
		return ret
	}
	defer rc.Close()
	r, err := redis.Strings(rc.Do("KEYS", keys))
	if err != nil {
		log.Release("Key_DelKeys keys=%v err=%v", keys, err)
		return ret
	}
	for _, v := range r {
		rc.Do("DEL", v)
	}
	return len(r)
}
