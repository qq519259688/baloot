package redis

import (
	"time"

	"bet24.com/log"
	"github.com/gomodule/redigo/redis"
)

var (
	// 定义常量
	RedisClient    *redis.Pool
	REDIS_HOST     string
	REDIS_PASSWORD string
	REDIS_DB       int
)

func InitPool(serverUrl, password string, db int) {
	if RedisClient != nil {
		log.Release("redis.InitPool already inited")
		return
	}
	log.Release("redis.InitPool %v,%v,%d", serverUrl, password, db)
	REDIS_HOST = serverUrl
	REDIS_DB = db
	REDIS_PASSWORD = password

	// 建立连接池
	RedisClient = &redis.Pool{
		// 从配置文件获取maxidle以及maxactive，取不到则用后面的默认值
		MaxIdle:     3,
		MaxActive:   1000,
		Wait:        false,
		IdleTimeout: 300 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", REDIS_HOST)
			if err != nil {
				log.Release("redis.Dial failed %v", err)
				return nil, err
			}
			if len(REDIS_PASSWORD) > 0 {
				if _, err := c.Do("AUTH", REDIS_PASSWORD); err != nil {
					c.Close()
					log.Release("connect redis invalid password")
					return nil, err
				}
			}
			// 选择db
			if REDIS_DB > 0 {
				c.Do("SELECT", REDIS_DB)
			}
			return c, nil
		},
	}
}
