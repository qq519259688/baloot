package redis

import (
	"time"

	"bet24.com/log"
	"github.com/gomodule/redigo/redis"
)

const channel_name = "rbwar_channel"

func Subscribe(onCommand func(data string)) {
	conn := RedisClient.Get()
	if conn == nil {
		log.Release("Subscribe error conn == nil")
		time.AfterFunc(time.Minute, func() {
			Subscribe(onCommand)
		})
		return
	}
	psc := redis.PubSubConn{Conn: conn}
	psc.Subscribe(channel_name)
	for {
		switch v := psc.Receive().(type) {
		case redis.Message:
			//log.Debug("%s: message: %s\n", v.Channel, v.Data)
			onCommand(string(v.Data))
		case redis.Subscription:
			log.Release("%s: %s %d\n", v.Channel, v.Kind, v.Count)
		case error:
			log.Release("Subscribe error %v", v)
			time.AfterFunc(time.Minute, func() {
				Subscribe(onCommand)
			})
			return
		}
	}
}

func Publish(data string) {
	go func(d string) {
		conn := RedisClient.Get()
		if conn == nil {
			log.Release("Publish no pool connection")
			return
		}
		conn.Do("PUBLISH", channel_name, d)
		conn.Close()
	}(data)
}
