package redis

import (
	"github.com/gomodule/redigo/redis"
	"bet24.com/log"
)

//向名称为key的set中添加元素member
func Set_SAdd(key, member string, expire int) bool {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Set_SAdd no redis connection")
		return false
	}
	defer rc.Close()
	_, err := rc.Do("SADD", key, member)
	if err != nil {
		log.Release("Set_SAdd key=%v member=%v err=%v", key, member, err)
		return false
	}
	if expire > 0 {
		rc.Do("expire", key, expire)
	}
	return true
}

//返回名称为key的set的所有元素
func Set_SMembers(key string) ([]string, bool) {
	rc := RedisClient.Get()
	if rc == nil {
		log.Release("Set_SMembers no redis connection")
		return nil, false
	}
	defer rc.Close()
	ret, err := redis.Strings(rc.Do("SMEMBERS", key))
	if err != nil {
		//log.Release("Set_SMembers key=%v err=%v", key, err)
		var r []string
		return r, true
	}
	return ret, true
}
