package log

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime/debug"
	"time"
)

//捕获panic信息，输出到log文件
func PanicHandler(exeName string) {
	if err := recover(); err != nil {
		now := time.Now()  //获取当前时间
		pid := os.Getpid() //获取进程ID

		time_str := now.Format("200601021504")                            //设定时间格式
		fname := fmt.Sprintf("%s-%d-%s-dump.log", exeName, pid, time_str) //保存错误信息文件名:程序名-进程ID-当前时间（年月日时分秒）

		path := "./panicLog"
		os.MkdirAll(path, 0777)
		fn := filepath.Join(path, fname)
		f, osErr := os.Create(fn)
		if osErr != nil {
			return
		}
		defer f.Close()

		f.WriteString(fmt.Sprintf("%v\r\n", err)) //输出panic信息
		f.WriteString("========\r\n")
		f.WriteString(string(debug.Stack())) //输出堆栈信息
	}
}
