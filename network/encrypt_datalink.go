package network

import (
	"net"

	"bet24.com/log"
	"bet24.com/public"
)

const (
	MAX_USER_NUM       int    = 1024
	MAX_SENDBUF_LEN    int    = 16384
	MIN_FIRST_PACK_LEN int    = 7
	SEED_XOR_KEY       uint32 = 0x5CC55CC5
	LEN_EXCURSION      int    = 3
	SNAP_SIZE          int    = 4
	CMD_APPEND_SIZE    int    = 4
	CMD_HEAD_SIZE      int    = 8
)

var sendMap = []byte{
	0x70, 0x2F, 0x40, 0x5F, 0x44, 0x8E, 0x6E, 0x45, 0x7E, 0xAB, 0x2C, 0x1F, 0xB4, 0xAC, 0x9D, 0x91,
	0x0D, 0x36, 0x9B, 0x0B, 0xD4, 0xC4, 0x39, 0x74, 0xBF, 0x23, 0x16, 0x14, 0x06, 0xEB, 0x04, 0x3E,
	0x12, 0x5C, 0x8B, 0xBC, 0x61, 0x63, 0xF6, 0xA5, 0xE1, 0x65, 0xD8, 0xF5, 0x5A, 0x07, 0xF0, 0x13,
	0xF2, 0x20, 0x6B, 0x4A, 0x24, 0x59, 0x89, 0x64, 0xD7, 0x42, 0x6A, 0x5E, 0x3D, 0x0A, 0x77, 0xE0,
	0x80, 0x27, 0xB8, 0xC5, 0x8C, 0x0E, 0xFA, 0x8A, 0xD5, 0x29, 0x56, 0x57, 0x6C, 0x53, 0x67, 0x41,
	0xE8, 0x00, 0x1A, 0xCE, 0x86, 0x83, 0xB0, 0x22, 0x28, 0x4D, 0x3F, 0x26, 0x46, 0x4F, 0x6F, 0x2B,
	0x72, 0x3A, 0xF1, 0x8D, 0x97, 0x95, 0x49, 0x84, 0xE5, 0xE3, 0x79, 0x8F, 0x51, 0x10, 0xA8, 0x82,
	0xC6, 0xDD, 0xFF, 0xFC, 0xE4, 0xCF, 0xB3, 0x09, 0x5D, 0xEA, 0x9C, 0x34, 0xF9, 0x17, 0x9F, 0xDA,
	0x87, 0xF8, 0x15, 0x05, 0x3C, 0xD3, 0xA4, 0x85, 0x2E, 0xFB, 0xEE, 0x47, 0x3B, 0xEF, 0x37, 0x7F,
	0x93, 0xAF, 0x69, 0x0C, 0x71, 0x31, 0xDE, 0x21, 0x75, 0xA0, 0xAA, 0xBA, 0x7C, 0x38, 0x02, 0xB7,
	0x81, 0x01, 0xFD, 0xE7, 0x1D, 0xCC, 0xCD, 0xBD, 0x1B, 0x7A, 0x2A, 0xAD, 0x66, 0xBE, 0x55, 0x33,
	0x03, 0xDB, 0x88, 0xB2, 0x1E, 0x4E, 0xB9, 0xE6, 0xC2, 0xF7, 0xCB, 0x7D, 0xC9, 0x62, 0xC3, 0xA6,
	0xDC, 0xA7, 0x50, 0xB5, 0x4B, 0x94, 0xC0, 0x92, 0x4C, 0x11, 0x5B, 0x78, 0xD9, 0xB1, 0xED, 0x19,
	0xE9, 0xA1, 0x1C, 0xB6, 0x32, 0x99, 0xA3, 0x76, 0x9E, 0x7B, 0x6D, 0x9A, 0x30, 0xD6, 0xA9, 0x25,
	0xC7, 0xAE, 0x96, 0x35, 0xD0, 0xBB, 0xD2, 0xC8, 0xA2, 0x08, 0xF3, 0xD1, 0x73, 0xF4, 0x48, 0x2D,
	0x90, 0xCA, 0xE2, 0x58, 0xC1, 0x18, 0x52, 0xFE, 0xDF, 0x68, 0x98, 0x54, 0xEC, 0x60, 0x43, 0x0F}

var recvMap = []byte{
	0x51, 0xA1, 0x9E, 0xB0, 0x1E, 0x83, 0x1C, 0x2D, 0xE9, 0x77, 0x3D, 0x13, 0x93, 0x10, 0x45, 0xFF,
	0x6D, 0xC9, 0x20, 0x2F, 0x1B, 0x82, 0x1A, 0x7D, 0xF5, 0xCF, 0x52, 0xA8, 0xD2, 0xA4, 0xB4, 0x0B,
	0x31, 0x97, 0x57, 0x19, 0x34, 0xDF, 0x5B, 0x41, 0x58, 0x49, 0xAA, 0x5F, 0x0A, 0xEF, 0x88, 0x01,
	0xDC, 0x95, 0xD4, 0xAF, 0x7B, 0xE3, 0x11, 0x8E, 0x9D, 0x16, 0x61, 0x8C, 0x84, 0x3C, 0x1F, 0x5A,
	0x02, 0x4F, 0x39, 0xFE, 0x04, 0x07, 0x5C, 0x8B, 0xEE, 0x66, 0x33, 0xC4, 0xC8, 0x59, 0xB5, 0x5D,
	0xC2, 0x6C, 0xF6, 0x4D, 0xFB, 0xAE, 0x4A, 0x4B, 0xF3, 0x35, 0x2C, 0xCA, 0x21, 0x78, 0x3B, 0x03,
	0xFD, 0x24, 0xBD, 0x25, 0x37, 0x29, 0xAC, 0x4E, 0xF9, 0x92, 0x3A, 0x32, 0x4C, 0xDA, 0x06, 0x5E,
	0x00, 0x94, 0x60, 0xEC, 0x17, 0x98, 0xD7, 0x3E, 0xCB, 0x6A, 0xA9, 0xD9, 0x9C, 0xBB, 0x08, 0x8F,
	0x40, 0xA0, 0x6F, 0x55, 0x67, 0x87, 0x54, 0x80, 0xB2, 0x36, 0x47, 0x22, 0x44, 0x63, 0x05, 0x6B,
	0xF0, 0x0F, 0xC7, 0x90, 0xC5, 0x65, 0xE2, 0x64, 0xFA, 0xD5, 0xDB, 0x12, 0x7A, 0x0E, 0xD8, 0x7E,
	0x99, 0xD1, 0xE8, 0xD6, 0x86, 0x27, 0xBF, 0xC1, 0x6E, 0xDE, 0x9A, 0x09, 0x0D, 0xAB, 0xE1, 0x91,
	0x56, 0xCD, 0xB3, 0x76, 0x0C, 0xC3, 0xD3, 0x9F, 0x42, 0xB6, 0x9B, 0xE5, 0x23, 0xA7, 0xAD, 0x18,
	0xC6, 0xF4, 0xB8, 0xBE, 0x15, 0x43, 0x70, 0xE0, 0xE7, 0xBC, 0xF1, 0xBA, 0xA5, 0xA6, 0x53, 0x75,
	0xE4, 0xEB, 0xE6, 0x85, 0x14, 0x48, 0xDD, 0x38, 0x2A, 0xCC, 0x7F, 0xB1, 0xC0, 0x71, 0x96, 0xF8,
	0x3F, 0x28, 0xF2, 0x69, 0x74, 0x68, 0xB7, 0xA3, 0x50, 0xD0, 0x79, 0x1D, 0xFC, 0xCE, 0x8A, 0x8D,
	0x2E, 0x62, 0x30, 0xEA, 0xED, 0x2B, 0x26, 0xB9, 0x81, 0x7C, 0x46, 0x89, 0x73, 0xA2, 0xF7, 0x72}

type Encrypt_DataLink struct {
	recvBuffer        []byte
	m_bFirstPack      bool
	sendMap           []byte
	recvMap           []byte
	m_dwRecvSeed      uint32
	m_dwSendSeed      uint32
	m_cbSendRoundFrom uint8
	m_cbRecvRoundFrom uint8
}

func NewEncrypt_DataLink() *Encrypt_DataLink {
	var obj = new(Encrypt_DataLink)
	obj.clear()
	return obj
}

func (this *Encrypt_DataLink) clear() {
	this.recvBuffer = nil
	this.m_bFirstPack = true
	this.m_cbRecvRoundFrom = 0
	this.m_cbSendRoundFrom = 0
}

func (this *Encrypt_DataLink) OnDataArrive(buffer []byte, onCommand func([]byte)) bool {
	if buffer == nil {
		return true
	}
	if this.recvBuffer != nil {
		if buffer != nil {
			buffer = append(this.recvBuffer, buffer...)
		} else {
			buffer = this.recvBuffer
		}
		this.recvBuffer = nil
	}
	totalLen := len(buffer)
	if totalLen == 0 {
		return true
	}
	// 不够包头
	if totalLen < 4 {
		this.appendTemBuffer(buffer)
		return true
	}
	// 解析长度
	var ba *public.ByteArray = public.CreateByteArray(buffer)
	var cmdLen int
	_cl, _ := ba.ReadUInt8()
	cmdLen = int(_cl)
	if cmdLen == 0xFF {
		_cl, _ := ba.ReadInt16()
		cmdLen = int(_cl)
	}

	if ba.Available() < cmdLen {
		// 粘包
		this.appendTemBuffer(buffer)

		return true
	}

	// 是否第一个包
	cmdBuffer := make([]byte, cmdLen)
	ba.ReadBytes(cmdBuffer, cmdLen, 0)
	if this.m_bFirstPack {
		if cmdLen < MIN_FIRST_PACK_LEN {
			log.Release("DoRead() error,The FirstPack's Len= %d", cmdLen)
			return false
		}

		_, cmdBuffer = this.decodeFirstPack(cmdBuffer)
		this.m_bFirstPack = false
		cmdLen -= 4

	} else {

		_, cmdBuffer = this.decodeNormalPack(cmdBuffer)
	}

	if !this.checkData(cmdBuffer) {
		log.Release("RecvData() error,The CheckSum is wrong,cmdlen = %d,RecvedLen = %d\n", cmdLen, totalLen)
		this.clear()
		return false
	}

	cmdBuffer = cmdBuffer[1:]

	onCommand(cmdBuffer)

	nAvailable := ba.Available()
	if nAvailable > 0 {

		available := make([]byte, nAvailable)
		ba.ReadBytes(available, nAvailable, 0)
		this.appendTemBuffer(available)
		return this.OnDataArrive([]byte{}, onCommand)
	}
	return true
}

func (this *Encrypt_DataLink) appendTemBuffer(buffer []byte) {
	if this.recvBuffer == nil {
		this.recvBuffer = buffer[:]
		return
	}

	this.recvBuffer = append(this.recvBuffer, buffer...)
}

func (this *Encrypt_DataLink) SendData(data []byte, conn net.Conn) {
	isClient := false
	var buf []byte
	var ba *public.ByteArray = public.CreateByteArray(buf)
	dataLen := len(data)
	if isClient && this.m_bFirstPack {
		dataLen += 4
	}

	ba.WriteUInt8(0xff)
	ba.WriteUInt16(uint16(dataLen + 1))
	checkSum := this.getCheckSum(data)
	var dwXorkey uint32
	if isClient && this.m_bFirstPack {
		// 初始化key
		dwXorkey = uint32(this.seedMap(uint16(this.m_dwSendSeed)))
		dwXorkey |= uint32(uint32(this.seedMap(uint16(dwXorkey>>16))) << 16)
		dwXorkey ^= SEED_XOR_KEY
		this.m_dwSendSeed = dwXorkey
		this.m_dwRecvSeed = dwXorkey
	}

	var encryptData = []byte{checkSum}
	encryptData = append(encryptData, data...)
	encryptData = this.encryptData(encryptData)
	if isClient && this.m_bFirstPack {
		this.m_bFirstPack = false
		// 散存密钥
		var xorkey [4]byte
		indsertIndex := [4]int{1, 3, 6, 8}
		for i := 0; i < 4; i++ {
			xorkey[i] = byte(dwXorkey % 0x100)
			dwXorkey /= 0x100

			rear := append([]byte{}, encryptData[indsertIndex[i]:]...)
			encryptData = append(encryptData[0:indsertIndex[i]], xorkey[i])
			encryptData = append(encryptData, rear...)
		}
	}

	ba.WriteBytes(encryptData)
	conn.Write(ba.Bytes())
}

func (this *Encrypt_DataLink) decodeFirstPack(buffer []byte) (bool, []byte) {
	InitSeed := make([]byte, 4)
	InitSeed[0] = buffer[1]
	InitSeed[1] = buffer[3]
	InitSeed[2] = buffer[6]
	InitSeed[3] = buffer[8]
	buffer = append(buffer[:8], buffer[9:]...)
	buffer = append(buffer[:6], buffer[7:]...)
	buffer = append(buffer[:3], buffer[4:]...)
	buffer = append(buffer[:1], buffer[2:]...)

	ba := public.CreateByteArray(InitSeed)
	seed, _ := ba.ReadInt32()
	this.m_dwRecvSeed = uint32(seed)

	this.m_dwSendSeed = this.m_dwRecvSeed

	if !this.checkSeed(this.m_dwRecvSeed) {
		log.Release("Error at DeCodeFirstPack,CheckSeed() Faild")
		return false, buffer
	}
	return this.decodeNormalPack(buffer)
}

func (this *Encrypt_DataLink) decodeNormalPack(buffer []byte) (bool, []byte) {
	buf := public.CreateByteArray(buffer)
	inputlen := len(buffer)

	var bufTemp []byte
	var ret *public.ByteArray = public.CreateByteArray(bufTemp)
	//解第一层的XOR
	for nOff := 0; nOff < inputlen; nOff += 4 {
		if buf.Available() >= 4 {
			dwBuf, _ := buf.ReadUInt32()
			dwResult := dwBuf ^ this.m_dwRecvSeed
			ret.WriteInt32(int32(dwResult))
			wSeedIndex1 := uint16(dwBuf % 0x10000)
			wSeedIndex1 = this.seedMap(wSeedIndex1)
			wSeedIndex2 := uint16(dwBuf / 0x10000)
			wSeedIndex2 = this.seedMap(wSeedIndex2)
			this.m_dwRecvSeed = (uint32(wSeedIndex1) + uint32(wSeedIndex2)*0x10000) ^ SEED_XOR_KEY
		} else {
			// 补足长度
			complen := 4 - buf.Available()
			buf.SetWriteEnd()
			for i := 0; i < complen; i++ {
				buf.WriteInt8(0)
			}
			dwBuf, _ := buf.ReadUInt32()
			dwResult := dwBuf ^ this.m_dwRecvSeed
			ret.WriteInt32(int32(dwResult))
			var dwNewResult uint32 = 0
			var pos uint32 = 0
			for i := 0; i < 4-complen; i++ {
				tempInt := (dwResult % 0x100)
				tempInt = tempInt << pos
				dwNewResult += tempInt
				dwResult /= 0x100
				pos += 8
			}
			dwBuf = dwNewResult ^ this.m_dwRecvSeed
			wSeedIndex1 := uint16(dwBuf % 0x10000)
			wSeedIndex1 = this.seedMap(wSeedIndex1)
			wSeedIndex2 := uint16(dwBuf / 0x10000)
			wSeedIndex2 = this.seedMap(wSeedIndex2)
			this.m_dwRecvSeed = (uint32(wSeedIndex1) + uint32(wSeedIndex2)*0x10000) ^ SEED_XOR_KEY
		}
	}
	buffer = (ret.Bytes())[:inputlen]
	//字节映射表解码
	for nOff := 0; nOff < inputlen; nOff++ {
		buffer[nOff] = this.mapRecvByte(uint8(buffer[nOff]))
	}

	return true, buffer
}

func (this *Encrypt_DataLink) checkSeed(seed uint32) bool {
	return true
}

func (this *Encrypt_DataLink) mapSendByte(cbData uint8) byte {
	ret := sendMap[uint8(cbData+this.m_cbSendRoundFrom)]
	this.m_cbSendRoundFrom += 3
	return ret

}

func (this *Encrypt_DataLink) mapRecvByte(cbData uint8) byte {
	ret := recvMap[cbData] - this.m_cbRecvRoundFrom
	this.m_cbRecvRoundFrom += 3
	return ret
}

func (this *Encrypt_DataLink) checkData(buffer []byte) bool {
	bufLen := len(buffer)
	if bufLen < 2 {
		log.Release("checkData  error bufLen  = %d", bufLen)
		return false
	}
	var checkSum uint8 = 0
	for i := 0; i < bufLen; i++ {
		checkSum += uint8(buffer[i])
	}
	if checkSum != 0 {
		log.Release("checkData  error checkSum  = %d", checkSum)
		return false
	}
	return true
}

func (this *Encrypt_DataLink) getCheckSum(buffer []byte) uint8 {
	var checkSum uint8 = 0
	bufLen := len(buffer)
	for i := 0; i < bufLen; i++ {
		checkSum += uint8(buffer[i])
	}
	checkSum = 255 - checkSum + 1
	return checkSum
}

func (this *Encrypt_DataLink) encryptData(buffer []byte) []byte {
	buf := public.CreateByteArray(buffer)
	inputlen := len(buffer)

	//字节表映射
	for nOff := 0; nOff < inputlen; nOff++ {
		buffer[nOff] = this.mapSendByte(uint8(buffer[nOff]))
	}

	// 补足长度
	complen := (4 - inputlen%4) % 4
	buf.SetWriteEnd()
	for i := 0; i < complen; i++ {
		buf.WriteInt8(0)
	}

	var bufTemp []byte
	var ret *public.ByteArray = public.CreateByteArray(bufTemp)

	for nOff := 0; nOff < inputlen; nOff += 4 {
		dwBuf, _ := buf.ReadUInt32()
		dwResult := uint32(dwBuf) ^ this.m_dwSendSeed
		ret.WriteInt32(int32(dwResult))

		//得到下一个SendSeed
		wSeedIndex1 := uint16(dwResult % 0x10000)
		wSeedIndex1 = this.seedMap(wSeedIndex1)
		wSeedIndex2 := uint16(dwResult / 0x10000)
		wSeedIndex2 = this.seedMap(wSeedIndex2)
		this.m_dwSendSeed = (uint32(wSeedIndex1) + uint32(wSeedIndex2)*0x10000) ^ SEED_XOR_KEY
	}

	buffer = (ret.Bytes())[:inputlen]
	return buffer
}

func (this *Encrypt_DataLink) seedMap(seed uint16) uint16 {
	dwHold := uint32(seed)
	dwHold = dwHold*241103 + 2533101
	return uint16(dwHold >> 16)
}
