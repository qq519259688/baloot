package public

import (
	"strings"
	"time"
)

func GetMonthFirstLastDay(now time.Time) (firstOfMonth int, lastOfMonth int) {
	currentYear, currentMonth, _ := now.Date()
	currentLocation := now.Location()

	firstDate := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
	lastDate := firstDate.AddDate(0, 1, -1)

	return firstDate.Day(), lastDate.Day()
}

func GetMonthFirstLastTime(now time.Time) (time.Time, time.Time) {
	currentYear, currentMonth, _ := now.Date()
	currentLocation := now.Location()

	firstDate := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
	lastDate := firstDate.AddDate(0, 1, -1)

	return firstDate, lastDate
}

//NotPassToday 是否超过 今天凌晨
func NotPassToday(strTime string) (bool, error) {
	//获取转换后的时间
	compareTime, err := time.ParseInLocation("2006-01-02 15:04:05", strTime, time.Local)

	if err != nil {
		return false, err
	}

	//获取凌晨时间
	todayTime, err := time.ParseInLocation("2006-01-02", time.Now().Format("2006-01-02"), time.Local)

	if err != nil {
		return false, err
	}

	return compareTime.Before(todayTime), nil
}

//日期转字符串
func FormatDate(date time.Time, dateStyle string) string {
	dateStyle = strings.Replace(dateStyle, "yyyy", "2006", 1)
	dateStyle = strings.Replace(dateStyle, "yy", "06", 1)
	dateStyle = strings.Replace(dateStyle, "MM", "01", 1)
	dateStyle = strings.Replace(dateStyle, "dd", "02", 1)
	dateStyle = strings.Replace(dateStyle, "HH", "15", 1)
	dateStyle = strings.Replace(dateStyle, "mm", "04", 1)
	dateStyle = strings.Replace(dateStyle, "ss", "05", 1)
	dateStyle = strings.Replace(dateStyle, "SSS", "000", -1)
	return date.Format(dateStyle)
}
