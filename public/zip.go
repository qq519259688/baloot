package public

import (
	"bytes"
	"io"
	"log"
	"os"

	"github.com/alexmullins/zip"
)

//contents := []byte("Hello World")
//	fileName := "test.txt"
//	zipName := "./test.zip"
//	password := "golang"
//	Zip(contents, fileName, zipName, password)
//文本压缩加密
func Zip(contents []byte, fileName, zipName, password string) {
	fzip, err := os.Create(zipName)
	if err != nil {
		log.Fatalln(err)
	}

	zipw := zip.NewWriter(fzip)
	defer zipw.Close()

	w, err := zipw.Encrypt(fileName, password)
	if err != nil {
		log.Fatal(err)
	}

	_, err = io.Copy(w, bytes.NewReader(contents))
	if err != nil {
		log.Fatal(err)
	}

	zipw.Flush()
}
