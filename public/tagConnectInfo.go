package public

type ConnectInfo struct {
	Size           uint16
	GameInstallVer uint32
	GameBuildVer   uint32
	ConnectDelay   uint32
}

func NewConnectInfo(ba *ByteArray) *ConnectInfo {
	var this *ConnectInfo = new(ConnectInfo)
	if ba == nil {
		this.Size = 14
		this.GameInstallVer = 2018
		this.GameBuildVer = 1
		this.ConnectDelay = 0
		return this
	}
	this.Size, _ = ba.ReadUInt16()
	this.GameInstallVer, _ = ba.ReadUInt32()
	this.GameBuildVer, _ = ba.ReadUInt32()
	this.ConnectDelay, _ = ba.ReadUInt32()
	return this
}

func (this *ConnectInfo) Serialize(ba *ByteArray) {
	ba.WriteUInt16(this.Size)
	ba.WriteUInt32(this.GameInstallVer)
	ba.WriteUInt32(this.GameBuildVer)
	ba.WriteUInt32(this.ConnectDelay)
}
