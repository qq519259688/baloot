package public

import (
	"io"
	"net/http"
	"net/url"
	"strings"

	"bet24.com/log"
)

// http post json 请求
func HttpPostByJson(url string, data string) string {
	req, err := http.NewRequest("POST", url, strings.NewReader(data))
	if err != nil {
		log.Error("HttpPostByJson NewRequest error %v", err)
		return ""
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Cache-Control", "no-cache")

	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		log.Error("HttpPostByJson Request Do error %v", err)
		return ""
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error("HttpPostByJson Response error %v", err)
		return ""
	}

	// log.Debug("HttpPost Send：%v", string(body))
	return string(body)
}

// http post xml 请求
func HttpPostByXML(url string, data string) string {
	req, err := http.NewRequest("POST", url, strings.NewReader(data))
	if err != nil {
		log.Error("HttpPostByXML NewRequest error %v", err)
		return ""
	}

	req.Header.Set("Accept", "application/xml")
	//这里的http header的设置是必须设置的.
	req.Header.Set("Content-Type", "application/xml;charset=utf-8")

	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		log.Error("HttpPostByXML Request Do error %v", err)
		return ""
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error("HttpPostByXML Response error %v", err)
		return ""
	}

	//log.Debug("HttpPost Send：%v", string(body))
	return string(body)
}

// http get请求
func HttpGet(url string) string {
	//log.Debug("HttpGet url:%s", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Error("HttpGet NewRequest Error %v", err)
		return ""
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Cache-Control", "no-cache")

	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		log.Error("HttpGet request error %v", err)
		return ""
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error("HttpGet response error %v", err)
		return ""
	}
	return string(body)
}

// http.PostForm 方法的post请求
func HttpPostForm(url string, params url.Values) string {
	resp, err := http.PostForm(url, params)
	if err != nil {
		log.Error("HttpPostForm request error %v", err)
		return ""
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error("HttpPostForm response error %v", err)
		return ""
	}

	return string(body)
}
