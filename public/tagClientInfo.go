package public

import (
	"math/rand"
)

type ClientInfo struct {
	ComputerID   [3]uint32
	SystemVer    [2]uint32
	ComputerName string //12
}

func NewClientInfo(ba *ByteArray) *ClientInfo {
	var this *ClientInfo = new(ClientInfo)
	for i := 0; i < 3; i++ {
		if ba != nil {
			this.ComputerID[i], _ = ba.ReadUInt32()
		} else {
			this.ComputerID[i] = uint32(rand.Intn(65535))
		}
	}
	for i := 0; i < 2; i++ {
		if ba != nil {
			this.SystemVer[i], _ = ba.ReadUInt32()
		} else {
			this.SystemVer[i] = 2018
		}
	}
	if ba != nil {
		this.ComputerName, _ = ba.ReadString(12)
	} else {
		this.ComputerName = "golang test"
	}
	return this
}

func (this *ClientInfo) Serialize(ba *ByteArray) {
	for i := 0; i < 3; i++ {
		ba.WriteUInt32(this.ComputerID[i])
	}
	for i := 0; i < 2; i++ {
		ba.WriteUInt32(this.SystemVer[i])
	}

	ba.WriteStringWithLen(this.ComputerName, 12)
}

func (this *ClientInfo) GetMachineNum() []byte {
	var tmpBuf []byte
	ba := CreateByteArray(tmpBuf)
	for i := 0; i < 3; i++ {
		ba.WriteUInt32(this.ComputerID[i])
	}
	return ba.Bytes()
}
