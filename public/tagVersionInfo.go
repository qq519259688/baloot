package public

type VersionInfo struct {
	InstallVer  uint32
	BuildVer    uint32
	SubBuildVer uint32
}

func NewVersionInfo(ba *ByteArray) *VersionInfo {
	var this *VersionInfo = new(VersionInfo)
	if ba == nil {
		this.InstallVer = 2018
		this.BuildVer = 1
		this.SubBuildVer = 1
		return this
	}
	this.InstallVer, _ = ba.ReadUInt32()
	this.BuildVer, _ = ba.ReadUInt32()
	this.SubBuildVer, _ = ba.ReadUInt32()
	return this
}

func (this *VersionInfo) Serialize(ba *ByteArray) {
	ba.WriteUInt32(this.InstallVer)
	ba.WriteUInt32(this.BuildVer)
	ba.WriteUInt32(this.SubBuildVer)
}
