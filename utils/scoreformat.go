package utils

import (
	"fmt"
)

func FormatScore(score int) string {
	scoreStr := ""
	if score >= 1000000000000 {
		scoreStr = fmt.Sprintf("%0.2fT", float64(score)/1000000000000.0)
	} else if score >= 1000000000 {
		scoreStr = fmt.Sprintf("%0.2fB", float64(score)/1000000000.0)
	} else if score >= 1000000 {
		scoreStr = fmt.Sprintf("%0.2fM", float64(score)/1000000.0)
	} else if score >= 1000 {
		scoreStr = fmt.Sprintf("%0.2fK", float64(score)/1000.0)
	} else {
		scoreStr = fmt.Sprintf("%d", score)
	}
	return scoreStr
}
