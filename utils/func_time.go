package utils

import (
	"bet24.com/log"
	"fmt"
	"net"
	//"syscall"
	"time"
	//"unsafe"
)

var min_trace_time int64

func SetMinTraceTime(min int64) {
	min_trace_time = min
}

func TimeCost(func_name string) func() {
	if min_trace_time == 0 {
		min_trace_time = 200
	}
	start := time.Now()
	return func() {
		tc := time.Since(start)
		if tc.Milliseconds() >= min_trace_time {
			log.Release("%v run cost %v", func_name, tc)
		}
	}
}

func SetConsoleTitle(title string) {
	// handle, err := syscall.LoadLibrary("Kernel32.dll")
	// if err != nil {
	// 	return 0, err
	// }
	// defer syscall.FreeLibrary(handle)
	// proc, err := syscall.GetProcAddress(handle, "SetConsoleTitleW")
	// if err != nil {
	// 	return 0, err
	// }
	// r, _, err := syscall.Syscall(proc, 1, uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(title))), 0, 0)
	// return int(r), err
	//return 0,err
}

func CheckPortInUse(port int) bool {
	tcpAddress, err := net.ResolveTCPAddr("tcp4", fmt.Sprintf(":%d", port))
	if err != nil {
		return false
	}
	listener, err := net.ListenTCP("tcp", tcpAddress)
	if err != nil {
		return true
	}
	listener.Close()
	return false
}
