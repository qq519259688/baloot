package utils

import (
	"fmt"
	//"log"
	"os"
	//"syscall"
	"time"
)

var (
//	kernel32         = syscall.MustLoadDLL("kernel32.dll")
//	procSetStdHandle = kernel32.MustFindProc("SetStdHandle")
)

//func setStdHandle(stdhandle int32, handle syscall.Handle) error {
	// r0, _, e1 := syscall.Syscall(procSetStdHandle.Addr(), 2, uintptr(stdhandle), uintptr(handle), 0)
	// //r0, _, e1 := syscall.Syscall(procSetStdHandle.Addr(), 2, uintptr(handle), uintptr(stdhandle), 0)
	// if r0 == 0 {
	// 	if e1 != 0 {
	// 		return error(e1)
	// 	}
	// 	return syscall.EINVAL
	// }
//	return nil
//}

// redirectStderr to the file passed in
func RedirectStderr(f *os.File) {
	// err := setStdHandle(syscall.STD_ERROR_HANDLE, syscall.Handle(f.Fd()))
	// //err := setStdHandle(syscall.Handle(f.Fd()), syscall.STD_ERROR_HANDLE)
	// if err != nil {
	// 	log.Fatalf("Failed to redirect stderr to file: %v", err)
	// }
	// os.Stderr = f // this is required because the syscall on windows does not affect prior references to stderr
}

func SetErrorFile(logFilename string, startMsg string) {
	logFile, _ := os.OpenFile(logFilename, os.O_WRONLY|os.O_CREATE|os.O_SYNC|os.O_APPEND, 0644)
	RedirectStderr(logFile)
	os.Stderr.Write([]byte(fmt.Sprintf("Process created \n[%s]%s\n", time.Now().Format("2006-01-02 15:04:05"), startMsg)))
}
