package utils

import (
	"bet24.com/log"
	"strconv"
	"strings"
)

func HexToAscii(hex string) string {
	chars := strings.Split(hex, "\\")
	if len(chars) < 2 {
		log.Debug("HexToAscii invalid arg %s", hex)
		return ""
	}
	chars = chars[1:]
	ret := make([]byte, len(chars))
	for i := 0; i < len(chars); i++ {
		ret[i] = byte(hexToChar(chars[i]))
	}
	return string(ret)
}

func AsciiToHex(ascii string) string {
	return ""
}

func hexToChar(hex string) int {
	if strings.Index(hex, "x") != 0 {
		log.Debug("hexToChar %s not start with x", hex)
		return 0
	}
	hex = hex[1:]
	ret, err := strconv.ParseInt(hex, 16, 32)
	if err != nil {
		log.Debug("hexToChar %s convert failed", hex)
		return 0
	}
	return int(ret)
}
