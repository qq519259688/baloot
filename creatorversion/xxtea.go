package main

import (
	"bytes"
	"os"
	"strings"
	"sync"

	"github.com/xxtea/xxtea-go/xxtea"
)

func EncodeFile(srcFile string, destFile string, password string, sign string, sufixes []string) bool {
	//fmt.Println("EncodeFile " + srcFile)
	// 是否要加密的文件
	if isNeedEncode(srcFile, sufixes) {
		// 打开文件
		dat, err := os.ReadFile(srcFile)
		if err != nil {
			return false
		}
		// 已经加密过
		if strings.HasPrefix(string(dat), sign) {
			return false
		}
		encrypt_data := xxtea.Encrypt([]byte(dat), []byte(password))
		s := make([][]byte, 2)
		s[0] = []byte(sign)
		s[1] = encrypt_data
		sep := []byte("")
		os.WriteFile(destFile, bytes.Join(s, sep), 0777)
		return true
	}
	return false
}

func DecodeFile(srcFile string, destFile string, password string, sign string, sufixes []string) bool {
	// 是否要加密的文件
	if isNeedEncode(srcFile, sufixes) {
		// 打开文件
		dat, err := os.ReadFile(srcFile)
		if err != nil {
			return false
		}
		// 没有加密过
		if !strings.HasPrefix(string(dat), sign) {
			return false
		}

		decrypt_data := xxtea.Decrypt(([]byte(dat))[len(sign):], []byte(password))
		os.WriteFile(destFile, decrypt_data, 0777)
		return true
	}
	return false
}

func DoEncodeFile(srcFile string, destFile string, password string, sign string, wg *sync.WaitGroup, sufixes []string) {
	defer wg.Done()
	if !EncodeFile(srcFile, destFile, password, sign, sufixes) {
		CopyFile(destFile, srcFile)
	}
}

func DoDecodeFile(srcFile string, destFile string, password string, sign string, wg *sync.WaitGroup, sufixes []string) {
	defer wg.Done()
	if !DecodeFile(srcFile, destFile, password, sign, sufixes) {
		CopyFile(destFile, srcFile)
	}
}

func EncodeDir(srcPath string, destPath string, password string, sign string, wg *sync.WaitGroup, sufixes []string) {
	dir, err := os.ReadDir(srcPath)
	if err != nil {
		return
	}
	// 创建文件夹
	os.MkdirAll(destPath, 0777)

	for _, fi := range dir {
		if fi.IsDir() { // 忽略目录
			EncodeDir(srcPath+PthSep+fi.Name(), destPath+PthSep+fi.Name(), password, sign, wg, sufixes)
		} else {
			//fmt.Println(dirPth + PthSep + fi.Name())
			srcFile := srcPath + PthSep + fi.Name()
			destFile := destPath + PthSep + fi.Name()
			wg.Add(1)
			go DoEncodeFile(srcFile, destFile, password, sign, wg, sufixes)
		}
	}
}

func DecodeDir(srcPath string, destPath string, password string, sign string, wg *sync.WaitGroup, sufixes []string) {
	dir, err := os.ReadDir(srcPath)
	if err != nil {
		return
	}
	// 创建文件夹
	os.MkdirAll(destPath, 0777)

	for _, fi := range dir {
		if fi.IsDir() { // 忽略目录
			DecodeDir(srcPath+PthSep+fi.Name(), destPath+PthSep+fi.Name(), password, sign, wg, sufixes)
		} else {
			//fmt.Println(dirPth + PthSep + fi.Name())
			srcFile := srcPath + PthSep + fi.Name()
			destFile := destPath + PthSep + fi.Name()
			wg.Add(1)
			go DoDecodeFile(srcFile, destFile, password, sign, wg, sufixes)
		}
	}
}

func StartEncodeDir(srcPath string, destPath string, password string, sign string, sufixes []string) {
	var wg sync.WaitGroup
	EncodeDir(srcPath, destPath, password, sign, &wg, sufixes)
	wg.Wait()
}

func StartDecodeDir(srcPath string, destPath string, password string, sign string, sufixes []string) {
	var wg sync.WaitGroup
	DecodeDir(srcPath, destPath, password, sign, &wg, sufixes)
	wg.Wait()
}
