package database

// 未保证线程同步，当数据库操作需要输出的时候，对应的transaction需要继承ITransaction及trans_base
// 对应的DoAction也通过channel异步返回
// 如果该数据库操作无须返回，则不需要继承
type ITransaction interface {
	OnRet()
	DoAction(ch chan<- interface{})
}

type Trans_base struct {
	RetFunc func(state bool)
	State   bool
}

func (this *Trans_base) OnRet() {
	if this.RetFunc != nil {
		this.RetFunc(this.State)
	}
}
