package database

import (
	"bet24.com/log"
	_ "github.com/denisenkom/go-mssqldb"
)

type DataBase struct {
	sqlServer *SqlServer
}

func NewDataBase(datasource, database, login, password string) *DataBase {
	db := new(DataBase)
	db.start(datasource, database, login, password)
	return db
}

func (this *DataBase) start(datasource, database, login, password string) bool {
	this.sqlServer = NewSqlServer(datasource, database, login, password)
	if this.sqlServer == nil {
		log.Error("连接到数据库[%s : %s]失败\n", datasource, database)
		return false
	}

	log.Debug("datasource=%s database=%s 连接到数据成功...", datasource, database)
	return true
}

func (this *DataBase) ExecSql(sql string) [][]interface{} {
	return this.sqlServer.execSql(sql)
}

func (this *DataBase) ExecSqlJson(sql string) string {
	ret, err := this.sqlServer.execSqlJson(sql)
	if err != nil {
		log.Release("DataBase.ExecSqlJson error = %v", err)
		return ""
	}
	return ret
}
